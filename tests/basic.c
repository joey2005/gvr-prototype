/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.

  Basic functionality tests
 */
#include "string.h"
#include "math.h"
#include "common.h"

#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#ifdef GDS_CONFIG_USE_LRDS
#include <lrds.h>
#endif

#define NARRAYS 10

void test_allocation_impl(const char *test, MPI_Comm comm)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
  
    ASSERT_TEST_START_COMM(test, comm);
    
    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim,cts,min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            comm, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);
    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END_COMM(comm);
}

#undef  TEST_NAME
#define TEST_NAME "array allocation"
void test_allocation(void)
{
   test_allocation_impl(TEST_NAME, GDS_COMM_WORLD);
}

void test_array_rw_impl(const char *test, MPI_Comm comm)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo_ind[] = {0}, hi_ind[] = {0}, ld[] = {0};
    int put_buff[] = {432};
    int get_buff[1];
  
    ASSERT_TEST_START_COMM(test, comm);

    ASSERT_STAT_FATAL("GDS_alloc",
      GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
          comm, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    ASSERT_STAT_FATAL("GDS_put", GDS_put(put_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);

    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);

    ASSERT_EQUAL_INT(put_buff[0], get_buff[0]);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END_COMM(comm);
}

#undef  TEST_NAME
#define TEST_NAME "array read/write"
void test_array_rw(void)
{
    test_array_rw_impl(TEST_NAME, GDS_COMM_WORLD);
}

#undef  TEST_NAME
#define TEST_NAME "array accumulate"
void test_array_acc(void)
{
    const int init_val = 1;
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo_ind[] = {0}, hi_ind[] = {0}, ld[] = {0};
    int put_buff[] = {init_val}, acc_buff[] = {1};
    int get_buff[1];

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    ASSERT_STAT_FATAL("GDS_put", GDS_put(put_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(init_val, get_buff[0]);
    
    ASSERT_STAT_FATAL("GDS_acc", GDS_acc(acc_buff, ld, lo_ind, hi_ind, MPI_SUM, gds));
    GDS_fence(gds);
    
    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(nprocs + init_val, get_buff[0]);

    if (myrank == 0) {
        acc_buff[0] = 2;
        ASSERT_STAT_FATAL("GDS_acc",
            GDS_acc(acc_buff, ld, lo_ind, hi_ind, MPI_PROD, gds));
    }
    GDS_fence(gds);

    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT((nprocs + init_val) * 2, get_buff[0]);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "row major 2D array"
void test_array_2d_rowm(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {4, 5};
    GDS_size_t min_chunk[2] = {0, 0};
    int ndim = 2;
    GDS_size_t lo_ind[2], hi_ind[2], ld[1];
    static int buff[9];
    int i, j;
    MPI_Info info;

    ASSERT_TEST_START(TEST_NAME);

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_ROW_MAJOR);

    ASSERT_STAT_FATAL("GDS_alloc",
      GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    ASSERT_NOT_NULL_FATAL(gds);
    MPI_Info_free(&info);

    ld[0] = 1;
    for (j = 0; j < 5; j++) {
        for (i = 0; i < 4; i++) {
            buff[0] = 4 * j + i;
            lo_ind[0] = hi_ind[0] = i;
            lo_ind[1] = hi_ind[1] = j;
            ASSERT_STAT_FATAL("GDS_put", GDS_put(buff, ld, lo_ind, hi_ind, gds));
            GDS_wait_local(gds);
        }
    }

    GDS_fence(gds);

    ld[0] = 3;
    lo_ind[0] = 1; hi_ind[0] = 2;
    lo_ind[1] = 1; hi_ind[1] = 3;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    {
        int expected[] = {5, 9, 13, 6, 10, 14};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 2;
    lo_ind[0] = 0; hi_ind[0] = 1;
    lo_ind[1] = 3; hi_ind[1] = 4;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    {
        int expected[] = {12, 16, 13, 17};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 4;
    lo_ind[0] = 1; hi_ind[0] = 2;
    lo_ind[1] = 2; hi_ind[1] = 3;
    memset(buff, -1, sizeof(buff));
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    {
        int expected[] = {9, 13, -1, -1, 10, 14, -1, -1};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 5;

    lo_ind[0] = 0; hi_ind[0] = 0;
    lo_ind[1] = 0; hi_ind[1] = 4;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    {
        int expected[] = {0, 4, 8, 12, 16};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "column major 2D array"
void test_array_2d_colm(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {4, 5};
    GDS_size_t min_chunk[2] = {0, 0};
    int ndim = 2;
    GDS_size_t lo_ind[2], hi_ind[2], ld[1];
    int buff[12];
    int i, j;
    MPI_Info info;
  
    ASSERT_TEST_START(TEST_NAME);
    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    ASSERT_NOT_NULL_FATAL(gds);
    MPI_Info_free(&info);

    ld[0] = 1;
    for (j = 0; j < 5; j++) {
        for (i = 0; i < 4; i++) {
            buff[0] = 4*j + i;
            lo_ind[0] = hi_ind[0] = i;
            lo_ind[1] = hi_ind[1] = j;
            ASSERT_STAT_FATAL("GDS_put", GDS_put(buff, ld, lo_ind, hi_ind, gds));
            GDS_wait_local(gds);
        }
    }

    GDS_fence(gds);

    ld[0] = 2;
    lo_ind[0] = 1; hi_ind[0] = 2;
    lo_ind[1] = 1; hi_ind[1] = 3;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    {
        int expected[] = {5, 6, 9, 10, 13, 14};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    lo_ind[0] = 0; hi_ind[0] = 1;
    lo_ind[1] = 3; hi_ind[1] = 4;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    {
        int expected[] = {12, 13, 16, 17};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 4;
    lo_ind[0] = 1; hi_ind[0] = 2;
    lo_ind[1] = 1; hi_ind[1] = 2;
    memset(buff, -1, sizeof(buff));
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    {
        int expected[] = {5, 6, -1, -1, 9, 10, -1, -1};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 1;

    lo_ind[0] = 0; hi_ind[0] = 0;
    lo_ind[1] = 0; hi_ind[1] = 4;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    {
        int expected[] = {0, 4, 8, 12, 16};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "row major 3D array"
void test_array_3d_rowm(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {3, 4, 3};
    GDS_size_t min_chunk[3] = {0, 0, 0};
    int ndim = 3;
    GDS_size_t lo[3], hi[3], ld[2];
    static int buff[3*4*4];
    int i, j, k, n;;

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    ld[0] = 1; ld[1] = 1; n = 0;
    for (k = 0; k < 3; k++) {
        for (j = 0; j < 4; j++) {
            for (i = 0; i < 3; i++) {
                buff[0] = n++;
                lo[0] = hi[0] = k;
                lo[1] = hi[1] = j;
                lo[2] = hi[2] = i;
                ASSERT_STAT_FATAL("GDS_put", GDS_put(buff, ld, lo, hi, gds));
                GDS_wait_local(gds);
            }
        }
    }

    GDS_fence(gds);

    ld[0] = 3; ld[1] = 2;
    lo[0] = 0; hi[0] = 2;
    lo[1] = 1; hi[1] = 3;
    lo[2] = 1; hi[2] = 2;
    memset(buff, -1, sizeof(buff));
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo, hi, gds));
    GDS_fence(gds);
    {
        int expected[] = { 4,  5,  7,  8, 10, 11,
                          16, 17, 19, 20, 22, 23,
                          28, 29, 31, 32, 34, 35};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 4; ld[1] = 3;
    lo[0] = 1; hi[0] = 2;
    lo[1] = 1; hi[1] = 3;
    lo[2] = 1; hi[2] = 2;
    memset(buff, -1, sizeof(buff));
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo, hi, gds));
    GDS_fence(gds);
    {
        int expected[] = {16, 17, -1, 19, 20, -1, 22, 23, -1, -1, -1, -1,
                          28, 29, -1, 31, 32, -1, 34, 35, -1, -1, -1, -1};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "column major 3D array"
void test_array_3d_colm(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {4, 3, 3};
    GDS_size_t min_chunk[3] = {0, 0, 0};
    int ndim = 3;
    GDS_size_t lo[3], hi[3], ld[2];
    int buff[3*4*4];
    int i, j, k, n;;
    MPI_Info info;
    
    ASSERT_TEST_START(TEST_NAME);

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    ASSERT_NOT_NULL_FATAL(gds);
    MPI_Info_free(&info);

    ld[0] = 1; ld[1] = 1; n = 0;
    for (k = 0; k < 3; k++) {
        for (j = 0; j < 3; j++) {
            for (i = 0; i < 4; i++) {
                buff[0] = n++;
                lo[0] = hi[0] = i;
                lo[1] = hi[1] = j;
                lo[2] = hi[2] = k;
                ASSERT_STAT_FATAL("GDS_put", GDS_put(buff, ld, lo, hi, gds));
                GDS_wait_local(gds);
            }
        }
    }

    GDS_fence(gds);

    ld[0] = 2; ld[1] = 2;
    lo[0] = 1; lo[1] = 1; lo[2] = 0;
    hi[0] = 2; hi[1] = 2; hi[2] = 2;
    memset(buff, -1, sizeof(buff));
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo, hi, gds));
    GDS_fence(gds);
    {
        int expected[] = { 5, 6, 9, 10,
                           17, 18, 21, 22,
                           29, 30, 33, 34};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ld[0] = 4; ld[1] = 3;
    lo[0] = 1; lo[1] = 1; lo[2] = 0;
    hi[0] = 2; hi[1] = 2; hi[2] = 1;
    memset(buff, -1, sizeof(buff));
    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo, hi, gds));
    GDS_fence(gds);
    {
        int expected[] = {5, 6, -1, -1, 9, 10, -1, -1, -1, -1, -1, -1,
                          17, 18, -1, -1, 21, 22, -1, -1, -1, -1, -1, -1};
        ASSERT_EQUAL_INT(memcmp(expected, buff, sizeof(expected)), 0);
    }

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "multiple allocation"
void test_multi_alloc(void)
{
    GDS_gds_t gdss[NARRAYS];

    ASSERT_TEST_START(TEST_NAME);

    int ndim = 1, i;
    for (i = 0; i < NARRAYS; i++) {
        GDS_size_t cts[] = {1024};
        GDS_size_t min_chunk[] = {0};
        ASSERT_STAT_FATAL("GDS_alloc", 
            GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, 
                GDS_COMM_WORLD, MPI_INFO_NULL, &gdss[i]));
    }

    for (i = 0; i < NARRAYS; i++)
        ASSERT_STAT("GDS_free", GDS_free(&gdss[i]));
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "single read/write"
void test_single_rw(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, i;
    GDS_size_t lo_ind[] = {510};
    GDS_size_t hi_ind[] = {512};
    int put_buff[3] = {55, 143, 2098};
    int get_buff[3];
    GDS_size_t ld[] = {};

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, 
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);
    ASSERT_STAT_FATAL("GDS_put", GDS_put(put_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    for (i = 0; i < 3; i++) {
        ASSERT_EQUAL_INT(put_buff[i], get_buff[i]);
    }
    ASSERT_STAT("GDS_free", GDS_free(&gds));

    ASSERT_TEST_END();
}

void test_stride_impl(const char *test_name, MPI_Comm comm)
{
    const size_t ARRAY_SIZE = 64 * nprocs;
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {ARRAY_SIZE};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo_ind[] = {0};
    GDS_size_t hi_ind[] = {0};
    int put_buff[3] = {0, 143, 2098};
    int get_buff[3];
    GDS_size_t ld[] = {};
 
    if (nprocs < 2) return;

    ASSERT_TEST_START_COMM(test_name, comm);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            comm, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    if (myrank == 0) {
        GDS_put(put_buff, ld, lo_ind, hi_ind, gds);
        GDS_wait_local(gds);
    }

    put_buff[0] = 55;
    lo_ind[0] = ARRAY_SIZE/nprocs - 1;
    hi_ind[0] = lo_ind[0]+3-1;

    if (myrank == 0) {
        GDS_put(put_buff, ld, lo_ind, hi_ind, gds);
    }

    GDS_fence(gds);

    ASSERT_STAT("GDS_version_inc", GDS_version_inc(gds, 1, NULL, 0));

    ASSERT_STAT("GDS_get", GDS_get(get_buff, ld, lo_ind, hi_ind, gds));

    GDS_fence(gds);

    ASSERT_EQUAL_INT(memcmp(put_buff, get_buff, sizeof(get_buff)), 0);

    ASSERT_STAT("GDS_free", GDS_free(&gds));

    ASSERT_TEST_END_COMM(comm);
}

void test_stride(void)
{
    test_stride_impl("stride", MPI_COMM_WORLD);
}

#undef  TEST_NAME
#define TEST_NAME "iaxpy"
void test_iaxpy(void)
{
    ASSERT_TEST_START(TEST_NAME);

    /* Array size must be larger than or equal to
       the number of processes */
    int IAXPYM = 20;
    if (IAXPYM < nprocs)
        IAXPYM = 2 * nprocs;

    GDS_gds_t x;
    GDS_gds_t y;
    int alpha = 2;
    int ndim = 1;
    int inbufx[IAXPYM];
    int inbufy[IAXPYM];
    int outbuf[IAXPYM];
    int i;

    for (i=0;i<IAXPYM;++i) {
        inbufx[i] = i + 1;
        inbufy[i] = i + 1 + IAXPYM;
        outbuf[i] = 0;
    }

    GDS_size_t min_chunk[] = {0};
    GDS_size_t cts[] = {IAXPYM};
    GDS_size_t global_lo[] = {0};
    GDS_size_t global_hi[] = {IAXPYM - 1};
    size_t ld[] = {};

    GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &x);
    GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &y);

    size_t mysize = (IAXPYM)/nprocs;

    GDS_size_t my_lo[] = {myrank * mysize};
    GDS_size_t my_hi[] = {(myrank + 1) * mysize - 1};

    int tmpbufx[mysize];
    int tmpbufy[mysize];

    if (myrank == 0) {
        GDS_put((void *)inbufx, ld, global_lo, global_hi, x);
        GDS_put((void *)inbufy, ld, global_lo, global_hi, y);
    }

    GDS_fence(NULL);

    GDS_get((void*)tmpbufx, ld, my_lo, my_hi, x);
    GDS_get((void*)tmpbufy, ld, my_lo, my_hi, y);

    GDS_fence(NULL);

    GDS_version_inc(x, 1, NULL, 0);

    for (i=0; i < mysize; ++i) {
        tmpbufx[i] = alpha * tmpbufx[i] + tmpbufy[i];
    }

    GDS_put((void*)tmpbufx, ld, my_lo, my_hi, x);

    GDS_fence(x);

    if (myrank == 0) {
        GDS_get((void *)outbuf, ld, global_lo, global_hi, x);
        GDS_wait(x);
        for (i=0; i < mysize*nprocs; ++i) {
            ASSERT_EQUAL_INT(outbuf[i], (alpha * inbufx[i] + inbufy[i]));  
        }
    }

    GDS_fence(NULL);

    GDS_free(&x);
    GDS_free(&y);
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "version traverse"
#define NELEMS(a) (sizeof(a)/sizeof(a[0]))
void test_ver_traverse(void)
{
    GDS_gds_t gds;
    const size_t arraylen = 1024;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, i;

    GDS_size_t versions[] = {0, 1, 2, 5, 7, 10, 11};
    GDS_size_t lastver = versions[NELEMS(versions)-1];

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, 
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    /* create versions */
    for (i = 1; i < NELEMS(versions); i++) {
        int inc = versions[i] - versions[i-1];
        ASSERT_STAT("GDS_version_inc", GDS_version_inc(gds, inc, NULL, 0));
        ASSERT_EQUAL_INT(GDS_version(gds), versions[i]);
    }

    ASSERT_STAT("GDS_version_dec", GDS_version_dec(gds, lastver));
    ASSERT_EQUAL_INT(GDS_version(gds), 0);

    ASSERT_STAT("GDS_move_to_newest", GDS_move_to_newest(gds));
    ASSERT_EQUAL_INT(GDS_version(gds), lastver);

    /* Go backward */
    for (i = NELEMS(versions)-1;;i--) {
        if (i > 0) {
            ASSERT_STAT("GDS_move_to_prev", GDS_move_to_prev(gds));
            ASSERT_EQUAL_INT(GDS_version(gds), versions[i-1]);
        }
        else {
            ASSERT_EQUAL_INT(GDS_move_to_prev(gds), GDS_STATUS_INVALID);
            ASSERT_EQUAL_INT(GDS_version(gds), 0);
            break;
        }
    }

    /* Go forward */
    for (i = 0; ;i++) {
        if (i < NELEMS(versions)-1) {
            ASSERT_STAT("GDS_move_to_next", GDS_move_to_next(gds));
            ASSERT_EQUAL_INT(GDS_version(gds), versions[i+1]);
        }
        else {
            ASSERT_EQUAL_INT(GDS_move_to_next(gds), GDS_STATUS_INVALID);
            ASSERT_EQUAL_INT(GDS_version(gds), lastver);
            break;
        }
    }

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "version restore"
static void ver_restore_test_impl(MPI_Info info)
{
    GDS_gds_t gds;
    const GDS_size_t ct_per_proc = 10000;
    const int test_vers = 10;
    GDS_size_t cts[] = { ct_per_proc * nprocs };
    GDS_size_t ld[] = {}, lo[1], hi[1];
    int buff[ct_per_proc], i;
    GDS_status_t ret;

    for (i = 0; i < ct_per_proc; i++)
        buff[i] = ct_per_proc * myrank + i;

    ret = GDS_alloc(1, cts, NULL, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, info, &gds);
    ASSERT_STAT("GDS_alloc", ret);
    /* GDS_alloc may fail due to unsupported LRDS tracking scheme */
    if (ret != GDS_STATUS_OK)
        return;

    lo[0] = ct_per_proc * myrank;
    hi[0] = ct_per_proc * (myrank + 1) - 1;

    GDS_put(buff, ld, lo, hi, gds);
    GDS_fence(gds);

    for (i = 0; i < test_vers; i++) {
        int j;
        GDS_version_inc(gds, 1, NULL, 0);
        for (j = 0; j < ct_per_proc; j++)
            buff[j]++;
        GDS_put(buff, ld, lo, hi, gds);
    }
    GDS_fence(gds);

    lo[0] = ct_per_proc * myrank + ct_per_proc / 2;
    hi[0] = ct_per_proc * (myrank + 1) + ct_per_proc / 2 - 1;
    if (hi[0] >= ct_per_proc * nprocs)
        hi[0] = ct_per_proc * nprocs - 1;

    for (i = 0; i < test_vers; i++) {
        int off = 10, base;
        GDS_get(buff, ld, lo, hi, gds);
        GDS_wait_local(gds);
        base = ct_per_proc * myrank + ct_per_proc / 2 + off;
        ASSERT_EQUAL_INT(buff[off], base + test_vers - i);
        if (myrank != nprocs - 1) {
            off = ct_per_proc / 2 + 10;
            base = ct_per_proc * myrank + ct_per_proc / 2 + off;
            ASSERT_EQUAL_INT(buff[off], base + test_vers - i);
        }
        GDS_move_to_prev(gds);
    }

    GDS_free(&gds);
}

void test_ver_restore(void)
{
    MPI_Info info;

    ASSERT_TEST_START(TEST_NAME);

    /* Flat test */
    ver_restore_test_impl(MPI_INFO_NULL);

    /* Log test */
    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_REPR_KEY, GDS_REPR_LOG);
    GDS_info_set_int(info, GDS_LOG_BS_KEY, 32);
    GDS_info_set_int(info, GDS_LOG_PREALLOC_VERS_KEY, 12);
    ver_restore_test_impl(info);
    MPI_Info_free(&info);

    /* LRDS test */
#ifdef GDS_CONFIG_USE_LRDS
    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_REPR_KEY, GDS_REPR_LRDS);
    GDS_info_set_int(info, GDS_PUT_IMPL_KEY, GDS_PUT_IMPL_RMA);
    GDS_info_set_int(info, GDS_GET_IMPL_KEY, GDS_GET_IMPL_RMA);
    GDS_info_set_int(info, GDS_LRDS_PROP_DIRTY_TRACKING_KEY, LRDS_DIRTY_TRACKING_USER);
    GDS_info_set_int(info, GDS_LRDS_PROP_VERSIONING_KEY, LRDS_VERSIONING_INCREMENTAL);
    ver_restore_test_impl(info);
    MPI_Info_free(&info);

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_REPR_KEY, GDS_REPR_LRDS);
    GDS_info_set_int(info, GDS_PUT_IMPL_KEY, GDS_PUT_IMPL_RMA);
    GDS_info_set_int(info, GDS_GET_IMPL_KEY, GDS_GET_IMPL_RMA);
    GDS_info_set_int(info, GDS_LRDS_PROP_DIRTY_TRACKING_KEY, LRDS_DIRTY_TRACKING_KERNEL);
    GDS_info_set_int(info, GDS_LRDS_PROP_VERSIONING_KEY, LRDS_VERSIONING_INCREMENTAL);
    ver_restore_test_impl(info);
    MPI_Info_free(&info);

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_REPR_KEY, GDS_REPR_LRDS);
    GDS_info_set_int(info, GDS_PUT_IMPL_KEY, GDS_PUT_IMPL_RMA);
    GDS_info_set_int(info, GDS_GET_IMPL_KEY, GDS_GET_IMPL_RMA);
    GDS_info_set_int(info, GDS_LRDS_PROP_DIRTY_TRACKING_KEY, LRDS_DIRTY_TRACKING_HARDWARE);
    GDS_info_set_int(info, GDS_LRDS_PROP_VERSIONING_KEY, LRDS_VERSIONING_INCREMENTAL);
    ver_restore_test_impl(info);
    MPI_Info_free(&info);

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_REPR_KEY, GDS_REPR_LRDS);
    GDS_info_set_int(info, GDS_PUT_IMPL_KEY, GDS_PUT_IMPL_MSG);
    GDS_info_set_int(info, GDS_GET_IMPL_KEY, GDS_GET_IMPL_MSG);
    GDS_info_set_int(info, GDS_LRDS_PROP_DIRTY_TRACKING_KEY, LRDS_DIRTY_TRACKING_USER);
    GDS_info_set_int(info, GDS_LRDS_PROP_VERSIONING_KEY, LRDS_VERSIONING_INCREMENTAL);
    ver_restore_test_impl(info);
    MPI_Info_free(&info);

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_REPR_KEY, GDS_REPR_LRDS);
    GDS_info_set_int(info, GDS_PUT_IMPL_KEY, GDS_PUT_IMPL_MSG);
    GDS_info_set_int(info, GDS_GET_IMPL_KEY, GDS_GET_IMPL_MSG);
    GDS_info_set_int(info, GDS_LRDS_PROP_DIRTY_TRACKING_KEY, LRDS_DIRTY_TRACKING_KERNEL);
    GDS_info_set_int(info, GDS_LRDS_PROP_VERSIONING_KEY, LRDS_VERSIONING_INCREMENTAL);
    ver_restore_test_impl(info);
    MPI_Info_free(&info);
#endif
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "clone"
void test_clone(void)
{
    GDS_gds_t gds, gds_clone;
    const size_t arraylen = 4;
    
    int testdata[][4] = {{1,2,3,4}, {5,6,7,8}};
    int buff[4];
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo_ind[] = {0}, hi_ind[] = {3}, ld[] = {};
    
    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    if (myrank == 0)
        ASSERT_STAT_FATAL("GDS_put", GDS_put(testdata[0], ld, lo_ind, hi_ind, gds));

    GDS_fence(gds);

    ASSERT_STAT_FATAL("GDS_descriptor_clone", GDS_descriptor_clone(gds, &gds_clone));
    ASSERT_NOT_EQUAL_PTR(gds, gds_clone);
    ASSERT_STAT("GDS_version_inc", GDS_version_inc(gds, 1, NULL, 0));

    ASSERT_STAT_FATAL("GDS_put", GDS_put(testdata[1], ld, lo_ind, hi_ind, gds));

    GDS_fence(gds);

    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds));

    GDS_fence(gds);

    ASSERT_EQUAL_INT(memcmp(buff, testdata[1], 4), 0);

    ASSERT_EQUAL_INT(GDS_version(gds), 1);
    ASSERT_EQUAL_INT(GDS_version(gds_clone), 0);

    ASSERT_STAT_FATAL("GDS_get", GDS_get(buff, ld, lo_ind, hi_ind, gds_clone));

    GDS_fence(gds_clone);

    ASSERT_EQUAL_INT(memcmp(buff, testdata[0], 4), 0);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    ASSERT_STAT("GDS_free", GDS_free(&gds_clone));

    ASSERT_TEST_END();
}

/*
  Test case for Issue 18
  https://bitbucket.org/globalviewresilience/gvr-prototype/issue/18/
 */

#define I18_START 256
#define I18_SKIP 256
#define I18_STOP (I18_START+I18_SKIP*128)
#define TOL 1e-14

#undef  TEST_NAME
#define TEST_NAME "issue18"
void test_issue18(void)
{
    int i;
    GDS_size_t N;
    double x[I18_STOP], xgot[I18_STOP];
    GDS_gds_t gds;
    GDS_size_t count[1];
    GDS_size_t min_chunk[1] = {0};
    GDS_size_t ld[0];
    GDS_size_t zero = 0;
    GDS_size_t hi[1];

    ASSERT_TEST_START(TEST_NAME);

    for (i=0;i<I18_STOP;++i) x[i] = (double)(i);

    for (N=I18_START;N<=I18_STOP;N+=I18_SKIP) {
        count[0] = N;
        hi[0] = N-1;
        GDS_alloc(1, count, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds);
        GDS_put(x, ld, &zero, hi, gds);
        GDS_fence(gds);
        GDS_version_inc(gds, 1, "", 0);
        GDS_move_to_prev(gds);
        for (i=0;i<I18_STOP;++i) xgot[i] = 0.0;
        GDS_get(xgot, ld, &zero, hi, gds);
        GDS_fence(gds);
        double maxdiff = 0.0;
        for (i=0;i<N;++i) {
            double d = xgot[i] - x[i];
            maxdiff = MAX(maxdiff, ABS(d));
        }
        ASSERT_COND(maxdiff < TOL);
        GDS_free(&gds);
    }

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "GVR 2D"
/* Test case for Issue #26 */
void test_gvr_2d_test(void)
{
    GDS_gds_t gds;
    GDS_size_t dims[2], chunk[2], ld[1], lo[2], hi[2];
    int i, j, a[128][128*nprocs];
    MPI_Info info;
  
    ASSERT_TEST_START(TEST_NAME);

    dims[0] = dims[1] = 128*nprocs;
    chunk[0] = chunk[1] = 0;
    ld[0] = 128;

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);
    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(2, dims, chunk, GDS_DATA_INT, GDS_PRIORITY_LOW, GDS_COMM_WORLD, info, &gds));
    MPI_Info_free(&info);

    for (i = 0; i < 128; i++)
        for (j = 0; j < 128*nprocs; j++)
            a[i][j] = 0;

    lo[0] = 128*myrank;
    lo[1] = 0;
    hi[0] = 128*myrank + 128 - 1;
    hi[1] = 128*nprocs - 1;

    ASSERT_STAT("GDS_put", GDS_put(a, ld, lo, hi, gds));

    GDS_fence(gds);
    GDS_free(&gds);
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "large matrix"

#define TOL 1e-14
#define LMNONZEROS 13107200 /* 200MB = 131720 * (4B + 4B + 8B) */
void test_large_matrix(void) 
{
    int *rows, *cols;
    double *vals;
    int *rows_result, *cols_result;
    double *vals_result;
    int i, row, col;

    ASSERT_TEST_START(TEST_NAME);

    rows = (int *) malloc(LMNONZEROS * sizeof(int));
    cols = (int *) malloc(LMNONZEROS * sizeof(int));
    vals = (double *) malloc(LMNONZEROS * sizeof(double));
    rows_result = (int *) malloc(LMNONZEROS * sizeof(int));
    cols_result = (int *) malloc(LMNONZEROS * sizeof(int));
    vals_result = (double *) malloc(LMNONZEROS * sizeof(double));

    row = col = 0;

    //We construct something resembling a large, tridiagonal, sparse matrix
    for (i = 0; i < LMNONZEROS; ++i) {
        rows[i] = row;
        cols[i] = col;
        vals[i] = i / 3.0;
        if (i % 3 == 0) {
            ++row;
        } else {
            col = (col + 1) % 500000;
        }
    }

    GDS_gds_t gds_rows, gds_cols, gds_vals;
    GDS_size_t count[1] = {LMNONZEROS};
    GDS_size_t min_chunk[1] = {0};

    GDS_alloc(1, count, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds_rows);
    GDS_alloc(1, count, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds_cols);
    GDS_alloc(1, count, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds_vals);

    GDS_size_t lo[1] = {0};
    GDS_size_t hi[1] = {LMNONZEROS - 1};
    GDS_size_t ld[0];

    GDS_put(rows, ld, lo, hi, gds_rows);
    GDS_put(cols, ld, lo, hi, gds_cols);
    GDS_put(vals, ld, lo, hi, gds_vals);

    GDS_fence(NULL);

    GDS_get(rows_result, ld, lo, hi, gds_rows);
    GDS_get(cols_result, ld, lo, hi, gds_cols);
    GDS_get(vals_result, ld, lo, hi, gds_vals);

    GDS_fence(NULL);

    size_t eq = 0;
    for (i = 0; i < LMNONZEROS; ++i) {
        double d;

        /* To reduce the number of assertions, do not use CU_ASSERT macros */
        if (rows[i] == rows_result[i])
            eq++;

        if (cols[i] == cols_result[i])
            eq++;

        d = vals[i] - vals_result[i];
        if (d < 0)
            d = -d;
        if (d < TOL)
            eq++;
    }
    ASSERT_EQUAL_INT(eq, LMNONZEROS * 3);
    
    GDS_free(&gds_rows);
    GDS_free(&gds_cols);
    GDS_free(&gds_vals);
    free(rows);
    free(cols);
    free(vals);
    free(rows_result);
    free(cols_result);
    free(vals_result);
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "global/local error check and recovery"
#define ERROR_OFFSET 874
#define ERROR_COUNT 321
static GDS_status_t simple_global_error_handler(GDS_gds_t gds, GDS_error_t error_desc)
{    
    GDS_size_t offset, count, len;
    int flag;

    ASSERT_STAT("GDS_get_error_attr",
                GDS_get_error_attr(error_desc, GDS_EATTR_GDS_INDEX, &offset, &flag));

    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(offset, ERROR_OFFSET);
    ASSERT_STAT("GDS_get_error_attr_len",
                GDS_get_error_attr_len(error_desc, GDS_EATTR_GDS_INDEX, &len, &flag));
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(int64_t));

    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(offset, ERROR_OFFSET);

    ASSERT_STAT("GDS_get_error_attr",
                GDS_get_error_attr(error_desc, GDS_EATTR_GDS_COUNT, &count, &flag));

    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(count, ERROR_COUNT);
    ASSERT_STAT("GDS_get_error_attr_len",
                GDS_get_error_attr_len(error_desc, GDS_EATTR_GDS_INDEX, &len, &flag));
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(int64_t));

    GDS_resume_global(gds, error_desc);

    return GDS_STATUS_OK;
}

static GDS_status_t simple_local_error_handler(GDS_gds_t gds, GDS_error_t error_desc)
{    
    GDS_size_t offset, count, len;
    int flag;

    ASSERT_STAT("GDS_get_error_attr",
                GDS_get_error_attr(error_desc, GDS_EATTR_GDS_INDEX, &offset, &flag));

    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(offset, ERROR_OFFSET);
    ASSERT_STAT("GDS_get_error_attr_len",
                GDS_get_error_attr_len(error_desc, GDS_EATTR_GDS_INDEX, &len, &flag));
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(int64_t));


    ASSERT_STAT("GDS_get_error_attr",
                GDS_get_error_attr(error_desc, GDS_EATTR_GDS_COUNT, &count, &flag));

    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(count, ERROR_COUNT);
    ASSERT_STAT("GDS_get_error_attr_len",
                GDS_get_error_attr_len(error_desc, GDS_EATTR_GDS_INDEX, &len, &flag));
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(int64_t));

    GDS_resume_local(gds, error_desc);

    return GDS_STATUS_OK;
}

static GDS_error_t simple_check_error_desc(void)
{
    GDS_error_t err;
    GDS_size_t error_off = ERROR_OFFSET;
    GDS_size_t error_ct  = ERROR_COUNT;

    ASSERT_STAT_FATAL("GDS_create_error_descriptor",
                      GDS_create_error_descriptor(&err));
    ASSERT_NOT_NULL_FATAL(err);

    ASSERT_STAT("GDS_add_error_attr",
                GDS_add_error_attr(err,
                                   GDS_EATTR_GDS_INDEX,
                                   sizeof(error_off),
                                   &error_off));
    ASSERT_STAT("GDS_add_error_attr",
                GDS_add_error_attr(err,
                                   GDS_EATTR_GDS_COUNT,
                                   sizeof(error_ct),
                                   &error_ct));

    return err;
}

static int global_check_invocation = 0;
static int local_check_invocation = 0;

static GDS_status_t simple_global_error_check(GDS_gds_t gds, GDS_priority_t check_priority)
{
    GDS_error_t err = simple_check_error_desc();

    global_check_invocation++;

    /* Make sure this was collectively called */
    GDS_fence(gds);

    ASSERT_STAT("GDS_raise_local_error",
                GDS_raise_local_error(gds, err));

    return GDS_STATUS_OK;
}

static GDS_status_t simple_local_error_check(GDS_gds_t gds, GDS_priority_t check_priority)
{
    GDS_error_t err = simple_check_error_desc();

    local_check_invocation++;

    ASSERT_STAT("GDS_raise_local_error",
        GDS_raise_local_error(gds, err));

    return GDS_STATUS_OK;
}

void test_global_local_error_check_recovery_impl(const char *name, MPI_Comm comm)
{
    int rank;
    int size;

    ASSERT_TEST_START_COMM(name, comm);

    /* Initialize checking counter */
    local_check_invocation = global_check_invocation = 0;

    GDS_comm_rank(comm, &rank);
    GDS_comm_size(comm, &size);
    
    GDS_gds_t gds_p = NULL;
 
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;

    GDS_error_pred_t pred;
    GDS_error_pred_term_t term;
 
    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_BYTE, GDS_PRIORITY_HIGH,
            comm, MPI_INFO_NULL, &gds_p));

    ASSERT_NOT_NULL_FATAL(gds_p);

    ASSERT_STAT_FATAL("GDS_register_global_error_check",
        GDS_register_global_error_check(gds_p,
                                        simple_global_error_check,
                                        GDS_PRIORITY_HIGH));

    ASSERT_STAT_FATAL("GDS_register_local_error_check",
        GDS_register_local_error_check(gds_p,
                                       simple_local_error_check,
                                       GDS_PRIORITY_HIGH));

    ASSERT_STAT_FATAL("GDS_create_error_pred",
                      GDS_create_error_pred(&pred));
    ASSERT_STAT("GDS_create_error_pred_term",
                GDS_create_error_pred_term(GDS_EATTR_GDS_INDEX,
                                           GDS_EMEXP_ANY, 0, NULL, &term));
    ASSERT_NOT_NULL_FATAL(term);
    ASSERT_STAT("GDS_add_error_pred_term",
                GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT("GDS_create_error_pred_term",
                GDS_create_error_pred_term(GDS_EATTR_GDS_INDEX,
                                           GDS_EMEXP_ANY, 0, NULL, &term));
    ASSERT_NOT_NULL_FATAL(term);
    ASSERT_STAT("GDS_add_error_pred_term",
                GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT_FATAL("GDS_register_global_error_handler",
        GDS_register_global_error_handler(gds_p,
                                          pred,
                                          simple_global_error_handler));

    ASSERT_STAT_FATAL("GDS_register_local_error_handler",
        GDS_register_local_error_handler(gds_p,
                                         pred,
                                         simple_local_error_handler));

    ASSERT_STAT("GDS_free_error_pred",
                GDS_free_error_pred(&pred));

    ASSERT_STAT_FATAL("GDS_version_inc",
        GDS_version_inc(gds_p, 1, NULL, 0));

    ASSERT_EQUAL_INT(local_check_invocation, 1);
    ASSERT_EQUAL_INT(global_check_invocation, 1);

    ASSERT_STAT("GDS_check_all_errors",
        GDS_check_all_errors(gds_p));

    ASSERT_EQUAL_INT(local_check_invocation, 2);
    ASSERT_EQUAL_INT(global_check_invocation, 2);

    ASSERT_STAT("GDS_free", GDS_free(&gds_p));
    
    ASSERT_TEST_END_COMM(comm);
}

void test_global_local_error_check_recovery(void)
{
    test_global_local_error_check_recovery_impl(
        "global/local error check and recovery", MPI_COMM_WORLD);
}

#undef  TEST_NAME
#define TEST_NAME "minimum chunk size"
void test_minchunk(void)
{
    GDS_gds_t gds;
    int ndims = 2;
    GDS_size_t dims[2] = {4, 2 * nprocs};
    GDS_size_t min_chunks[2] = {4, 1};
    GDS_size_t ld[1] = {2 * nprocs};
    GDS_size_t lo[2] = {0, 0}, hi[2] = {3, 2 * nprocs - 1};
    int *buff;
    int i;

    ASSERT_TEST_START(TEST_NAME);

    buff = malloc(sizeof(int) * 4 * 2 * nprocs);
    ASSERT_NOT_NULL_FATAL(buff);

    for (i = 0; i < 4 * 2 * nprocs; i++)
        buff[i] = i;

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndims, dims, min_chunks, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));

    if (myrank == 0)
        GDS_put(buff, ld, lo, hi, gds);

    GDS_fence(gds);

    lo[0] = 1; hi[0] = 2;
    memset(buff + ld[0], 0, sizeof(int) * ld[0] * 2);
    GDS_get(buff + ld[0], ld, lo, hi, gds);
    GDS_wait_local(gds);

    for (i = ld[0]; i < 3 * ld[0]; i++)
        ASSERT_EQUAL_INT(buff[i], i);

    GDS_free(&gds);

    ld[0] = 2 * (nprocs + 1) / 2;
    dims[0] = 4; dims[1] = ld[0];
    min_chunks[0] = min_chunks[1] = 2;

    buff = realloc(buff, sizeof(int) * 4 * ld[0]);
    ASSERT_NOT_NULL_FATAL(buff);

    for (i = 0; i < 4 * ld[0]; i++)
        buff[i] = i;

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndims, dims, min_chunks, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));

    lo[0] = 0; lo[1] = 0;
    hi[0] = 3; hi[1] = ld[0] - 1;

    if (myrank == 0)
        GDS_put(buff, ld, lo, hi, gds);
    GDS_fence(gds);

    lo[0] = 1; hi[0] = 2;
    memset(buff + ld[0], 0, sizeof(int) * ld[0] * 2);
    GDS_get(buff + ld[0], ld, lo, hi, gds);
    GDS_wait_local(gds);

    for (i = ld[0]; i < 3 * ld[0]; i++)
        ASSERT_EQUAL_INT(buff[i], i);

    GDS_free(&gds);

    free(buff);
    
    ASSERT_TEST_END();
}

void test_subcomm_allocation(MPI_Comm comm)
{
    test_allocation_impl("subcommunicator allocation", comm);
}

void test_subcomm_rw(MPI_Comm comm)
{
    test_array_rw_impl("subcommunicator read/write", comm);
}

void test_subcomm_stride(MPI_Comm comm)
{
    test_stride_impl("subcommunicator stride", comm);
}

void test_subcomm_global_local_error_check_recovery(MPI_Comm comm)
{
    test_global_local_error_check_recovery_impl(
        "subcommunicator global/local error check and recovery", comm);
}

void test_subcomm(void)
{
    if (nprocs < 2)
        return;

    MPI_Group full, new;
    MPI_Comm new_comm;
    int new_rank[nprocs/2], i;
    for (i = 0; i < nprocs/2; i++)
        new_rank[i] = i;

    MPI_Comm_group(MPI_COMM_WORLD, &full);
    MPI_Group_incl(full, nprocs/2, new_rank, &new);

    MPI_Comm_create(MPI_COMM_WORLD, new, &new_comm);

    if (myrank >= nprocs/2)
        goto skip;

    test_subcomm_allocation(new_comm);
    test_subcomm_rw(new_comm);
    test_subcomm_stride(new_comm);
    test_subcomm_global_local_error_check_recovery(new_comm);
skip:
    MPI_Barrier(MPI_COMM_WORLD);

    if (new_comm != MPI_COMM_NULL)
        MPI_Comm_free(&new_comm);
    MPI_Group_free(&full);
    MPI_Group_free(&new);
}

#ifdef GDS_CONFIG_USE_LRDS
#undef  TEST_NAME
#define TEST_NAME "lrds callback"
static GDS_gds_t test_lrds_gds;
static size_t test_lrds_ndims;
static size_t *test_lrds_lo_local;
static size_t *test_lrds_hi_local;
static size_t test_lrds_handler_invocations;

static GDS_status_t local_error_recovery(GDS_gds_t gds, GDS_error_t desc) {

    ++test_lrds_handler_invocations;

    size_t ndims = test_lrds_ndims;
    int flag;
    long err_offset[ndims];
    long err_ct[ndims];
    long is_reusable;
    GDS_get_error_attr(desc, GDS_EATTR_GDS_INDEX, err_offset, &flag);
    ASSERT_COND(flag);
    GDS_get_error_attr(desc, GDS_EATTR_GDS_COUNT, err_ct, &flag);
    ASSERT_COND(flag);
    GDS_get_error_attr(desc, GDS_EATTR_MEMORY_LOCATION_REUSABLE, &is_reusable, &flag);
    ASSERT_COND(flag);

    size_t dim;
    for (dim = 0; dim < ndims; ++dim) {
        size_t lower_limit = test_lrds_lo_local[dim];
        size_t upper_limit = test_lrds_hi_local[dim];
        size_t err_start = err_offset[dim];
        size_t err_stop = err_offset[dim] + err_ct[dim] - 1;
        ASSERT_COND(err_start >= lower_limit);
        ASSERT_COND(err_stop >= lower_limit);
        ASSERT_COND(err_start <= upper_limit);
        ASSERT_COND(err_stop <= upper_limit);
    }
    return GDS_STATUS_OK;
}

void test_register_lrds_callback(void) {
    ASSERT_TEST_START(TEST_NAME);
    int rank, comm_size;
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);

    test_lrds_gds = NULL;
    test_lrds_handler_invocations = 0; 

    test_lrds_ndims = 1;
    size_t test_lrds_gds_ct = 4;
    GDS_size_t cts1[] = {test_lrds_gds_ct * comm_size};
    GDS_size_t min_chunk1[] = {0};
    GDS_alloc(test_lrds_ndims,cts1,min_chunk1, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, MPI_INFO_NULL, &test_lrds_gds);

    GDS_error_pred_t pred;
    GDS_error_pred_term_t term;
    GDS_create_error_pred(&pred);
    GDS_create_error_pred_term(GDS_EATTR_GDS_INDEX, GDS_EMEXP_ANY,
        0, NULL, &term);
    GDS_add_error_pred_term(pred, term);
    GDS_free_error_pred_term(&term);
    GDS_create_error_pred_term(GDS_EATTR_GDS_COUNT, GDS_EMEXP_ANY,
        0, NULL, &term);
    GDS_add_error_pred_term(pred, term);
    GDS_free_error_pred_term(&term);
    GDS_register_local_error_handler(test_lrds_gds, pred, local_error_recovery);
    GDS_free_error_pred(&pred);

    test_lrds_lo_local = malloc(test_lrds_ndims * sizeof(*test_lrds_lo_local));
    test_lrds_hi_local = malloc(test_lrds_ndims * sizeof(*test_lrds_hi_local));
    GDS_size_t local_start = test_lrds_gds_ct * rank;
    GDS_size_t local_stop = test_lrds_gds_ct * (rank + 1) - 1;
    test_lrds_lo_local[0] = local_start;
    test_lrds_hi_local[0] = local_stop; 

    kill(getpid(), SIGBUS);
    GDS_fence(test_lrds_gds);

    ASSERT_COND(test_lrds_handler_invocations > 0);
    GDS_free(&test_lrds_gds); 
    free(test_lrds_lo_local);
    free(test_lrds_hi_local);
    ASSERT_TEST_END();
}
#endif /* GDS_CONFIG_USE_LRDS */
