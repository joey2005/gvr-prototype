#!/usr/bin/python

import sys
import numpy

sizes = set([])
arraytypes = set([])
table = {}

kvdict = {}

if len(sys.argv) < 2:
    print "Usage: %s [interval]" % sys.argv[0]
    quit()

interval = int(sys.argv[1])

def array_type_string(kvdict):
    basetype = kvdict['type']
    if basetype == 'log':
        bs = '-bs' + kvdict['block-size']
    else:
        bs = ''
    r = '-r%d' % (int(float(kvdict['read-ratio']) * 100))
    k = '-k' + kvdict['k']
    return basetype + bs + k + r

for line in sys.stdin:
    if line[0] == '#':
        continue;

    kv = line.rstrip().split('=')
    if len(kv) < 2:
        kvdict.clear()
        continue
    if kv[0] == "DATA":
        if kv[1][0:2] != '0:':
            continue
        else:
            # commit
            if int(kvdict['interval']) != interval:
                continue
            arraytype = array_type_string(kvdict)
            arraytypes.add(arraytype)
            size = int(kvdict['array-size'])
            sizes.add(size)
            key = (size, arraytype)
            data_strs = kv[1][2:].rstrip('\n').rstrip(',').split(',')
            if not table.has_key(key):
                table[key] = []
            table[key].append(numpy.mean(map(float, data_strs)) / float(kvdict['num-versions']))
    kvdict[kv[0]] = kv[1]

fst_col = True
for i in sorted(sizes):
    if fst_col:
        print "size",
        for t in sorted(arraytypes):
            print "\t%s" % t,
        print ""
        fst_col = False

    print "%d" % i,
    for  t in sorted(arraytypes):
        key = (i, t)
        try:
            print "\t%f" % numpy.mean(table[key]),
        except KeyError:
            print "\t-1.0",
    print ""
