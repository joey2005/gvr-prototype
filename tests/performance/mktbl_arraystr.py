def lrds_subtype(kvdict):
    tracking = int(kvdict['lrds-tracking'])
    if tracking == 0:
        subtype = '-user'
    elif tracking == 1:
        subtype = '-kernel'
    elif tracking == 2:
        subtype = '-hw'
    else:
        subtype = '-??'

    versioning = int(kvdict['lrds-versioning'])
    if versioning == 0:
        subtype += '-inc'
    else:
        subtype += '-dec'

    return subtype

def array_type_string(kvdict):
    basetype = kvdict['type']
    bs = ''
    subtype = ''
    if basetype == 'flat' and int(kvdict['skip-versioning']) != 0:
        subtype = '-nover'
    elif basetype == 'log':
        bs = '-bs' + kvdict['block-size']
    elif basetype == 'lrds':
        subtype = lrds_subtype(kvdict)
        bs = '-bs' + kvdict['block-size']
    r = '-r%d' % (int(float(kvdict['read-ratio']) * 100))
    k = '-k' + kvdict['k']
    return basetype + subtype + bs + k + r
