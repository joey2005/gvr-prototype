#!/usr/bin/python

import sys
import numpy
import mktbl_arraystr

bss = set([])
arraytypes = set([])
table = {}

kvdict = {}

def array_type_string(kvdict):
    basetype = kvdict['type']
    if basetype == 'lrds':
        subtype = mktbl_arraystr.lrds_subtype(kvdict)
    else:
        subtype = ''
    r = '-r%d' % (int(float(kvdict['read-ratio']) * 100))
    i = '-i' + kvdict['interval']
    k = '-k' + kvdict['k']
    return basetype + subtype + k + r + i

for line in sys.stdin:
    if line[0] == '#':
        continue;

    kv = line.rstrip().split('=')
    if len(kv) < 2:
        kvdict.clear()
        continue
    if kv[0] == "DATA":
        if kv[1][0:2] != '0:':
            continue
        else:
            # commit
            arraytype = array_type_string(kvdict)
            arraytypes.add(arraytype)
            bs = int(kvdict['block-size'])
            bss.add(bs)
            key = (bs, arraytype)
            data_strs = kv[1][2:].rstrip('\n').rstrip(',').split(',')
            if not table.has_key(key):
                table[key] = []
            ops = int(kvdict['interval']) * int(kvdict['cycle'])
            table[key].append(ops / (numpy.mean(map(float, data_strs)) / float(kvdict['num-versions'])))
    kvdict[kv[0]] = kv[1]

fst_col = True
for i in sorted(bss):
    if fst_col:
        print "bs",
        for t in sorted(arraytypes):
            print "\t%s" % t,
        print ""
        fst_col = False

    print "%d" % i,
    for  t in sorted(arraytypes):
        key = (i, t)
        try:
            print "\t%f" % numpy.mean(table[key]),
        except KeyError:
            print "\t-1.0",
    print ""
