#!/usr/bin/python

import sys
import numpy
import mktbl_arraystr

n_versions = set([])
arraytypes = set([])
table = {}

kvdict = {}

def array_type_string(kvdict):
    basetype = kvdict['type']
    bs = ''
    subtype = ''
    if basetype == 'flat' and int(kvdict['skip-versioning']) != 0:
        subtype = '-nover'
    elif basetype == 'log':
        bs = '-bs' + kvdict['block-size']
    elif basetype == 'lrds':
        subtype = mktbl_arraystr.lrds_subtype(kvdict)
        bs = '-bs' + kvdict['block-size']
    fr = '-fr' + kvdict['restore-fill-ratio']
    return basetype + subtype + bs + fr

for line in sys.stdin:
    if line[0] == '#':
        continue;

    kv = line.rstrip().split('=')
    if len(kv) < 2:
        kvdict.clear()
        continue
    if kv[0] == "DATA":
        if kv[1][0:2] != '0:':
            continue
        else:
            # commit
            arraytype = array_type_string(kvdict)
            arraytypes.add(arraytype)
            n_ver = int(int(kvdict['restore-version']))
            n_versions.add(n_ver)
            key = (n_ver, arraytype)
            data_strs = kv[1][2:].rstrip('\n').rstrip(',').split(',')
            if not table.has_key(key):
                table[key] = []
            table[key].append(numpy.mean(map(float, data_strs)))
    kvdict[kv[0]] = kv[1]

fst_col = True
for i in sorted(n_versions):
    if fst_col:
        print "n_versions",
        for t in sorted(arraytypes):
            print "\t%s" % t,
        print ""
        fst_col = False

    print "%d" % i,
    for  t in sorted(arraytypes):
        try:
            key = (i, t)
            # show in milliseconds
            print "\t%f" % (numpy.mean(table[key]) * 1000),
        except KeyError:
            print "\t-1.0",
    print ""
