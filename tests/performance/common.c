/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <string.h>
#include <getopt.h>
#include <mpi.h>
#include <stdio.h>

#include "common.h"

void GDSB_params_init(GDSB_params_t *params) {
    *params = NULL;
}

void GDSB_params_free(GDSB_params_t *params) {
    struct GDSB_params *current = *params;
    while (current != NULL) {
        struct GDSB_params *next = current->next;
        free(current->name);
        free(current);
        current = next;
    }
    *params = NULL;
}

void GDSB_params_add(const char *name, const GDSB_val_t val, GDSB_params_t *params) {
    struct GDSB_params *new = malloc(sizeof(*new));
    new->name = strdup(name);
    new->val = val;
    new->next = *params;
    *params = new;
}

int GDSB_params_get(const char *name, GDSB_val_t *val, GDSB_params_t params) {
    while (params != NULL) {
        if (!strcmp(name, params->name)) {
            *val = params->val;
            return 1;
        }
        params = params->next;
    }
    return 0;
}

int GDSB_parse_cl(int argc, char **argv, GDSB_params_t *params) {
    int opt, idx, next = 1;
    static struct option opts[] = {
        {"message-size", required_argument, 0, 'm'},
        {"problem-size", required_argument, 0, 'p'},
        {"num-ops", required_argument, 0, 'o'},
        {"num-trials", required_argument, 0, 't'},
    };
    char *name;
    while ((opt = getopt_long(argc, argv,"m:p:o:t:", opts, &idx )) != -1) {
        int skip = 0;
        switch (opt) {
            case 'm' : 
                name = "MESSAGE_SIZE";
                break;
            case 'p' : 
                name = "PROBLEM_SIZE";
                break;
            case 'o' : 
                name = "NUM_OPS";
                break;
            case 't' :
                name = "NUM_TRIALS";
                break;
            default:  
                skip = 1;
                break;
        }
        if (!skip) {
            GDSB_params_add(name, atol(optarg), params);
        }
        ++next;
    }
    return next;
}

void GDSB_print_results(const char *testname, const size_t num_results, 
    const double *results, const char *comment, GDSB_params_t params) {
    int comm_rank, comm_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    double recv_buf[num_results * comm_size];
    MPI_Gather(results, num_results, MPI_DOUBLE, recv_buf, num_results, 
        MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (comm_rank == 0) {
        if (comment) {
            printf("#%s\n", comment);
        }
        printf("TESTNAME=%s\n", testname);
        printf("NUM_PROCS=%d\n", comm_size);
        while (params != NULL) {
            printf("%s=%ld\n", params->name, params->val);
            params = params->next;
        }
        int proc;
        size_t global_result_idx = 0;
        for (proc = 0; proc < comm_size; ++proc) {
            printf("DATA=%d:", proc);
            int local_result_idx;
            for (local_result_idx = 0; local_result_idx < num_results; 
                ++local_result_idx) {
                printf("%f,", recv_buf[global_result_idx]);
                ++global_result_idx;
            } 
            printf("\n");
        }
        printf("\n");
    }
}
