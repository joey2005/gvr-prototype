#!/usr/bin/python

import sys
import numpy

intervals = set([])
arraytypes = set([])
table = {}

kvdict = {}

def array_type_string(kvdict):
    basetype = kvdict['type']
    bs = ''
    subtype = ''
    if basetype == 'flat' and int(kvdict['skip-versioning']) != 0:
        subtype = '-nover'
    elif basetype == 'log':
        bs = '-bs' + kvdict['block-size']
    elif basetype == 'lrds':
        tracking = int(kvdict['lrds-tracking'])
        if tracking == 0:
            subtype = '-user'
        elif tracking == 1:
            subtype = '-kernel'
        elif tracking == 2:
            subtype = '-hw'
        else:
            subtype = '-??'

        versioning = int(kvdict['lrds-versioning'])
        if versioning == 0:
            subtype += '-inc'
        else:
            subtype += '-dec'

        bs = '-bs' + kvdict['block-size']
    r = '-r%d' % (int(float(kvdict['read-ratio']) * 100))
    k = '-k' + kvdict['k']
    return basetype + subtype + bs + k + r


for line in sys.stdin:
    if line[0] == '#':
        continue;

    kv = line.rstrip().split('=')
    if len(kv) < 2:
        kvdict.clear()
        continue
    if kv[0] == "DATA":
        if kv[1][0:2] != '0:':
            continue
        else:
            # commit
            arraytype = array_type_string(kvdict)
            arraytypes.add(arraytype)
            interval = int(kvdict['interval']) * int(kvdict['cycle'])
            intervals.add(interval)
            key = (interval, arraytype)
            data_strs = kv[1][2:].rstrip('\n').rstrip(',').split(',')
            if not table.has_key(key):
                table[key] = []
            table[key].append(numpy.mean(map(float, data_strs)) / float(kvdict['num-versions']))
    kvdict[kv[0]] = kv[1]

fst_col = True
for i in sorted(intervals):
    if fst_col:
        print "interval",
        for t in sorted(arraytypes):
            print "\t%s" % t,
        print ""
        fst_col = False

    print "%d" % i,
    for  t in sorted(arraytypes):
        try:
            key = (i, t)
            # show #ops/s
            print "\t%f" % (i / numpy.mean(table[key])),
        except KeyError:
            print "\t-1.0",
    print ""
