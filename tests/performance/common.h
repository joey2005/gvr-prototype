/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#ifndef COMMON_H
#define COMMON_H

#include <stdlib.h>

typedef long GDSB_val_t;

typedef struct GDSB_params {
    char *name;
    GDSB_val_t val;
    struct GDSB_params *next;
} *GDSB_params_t;

void GDSB_params_init(GDSB_params_t *params);
void GDSB_params_free(GDSB_params_t *params);
void GDSB_params_add(const char *name, const GDSB_val_t val,  GDSB_params_t *params);
// nonzero if param is found
int GDSB_params_get(const char *name, GDSB_val_t *val, GDSB_params_t params);
// returns index of first positional argument
int GDSB_parse_cl(int argc, char **argv, GDSB_params_t *params);
void GDSB_print_results(const char *testname, const size_t num_results, const
    double *results, const char *comment, GDSB_params_t params);

#endif
