/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include "common.h"

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <sched.h>
#include <gds.h>

int my_rank;
int n_procs;

static void do_fence(GDSB_val_t problem_size, GDSB_val_t num_ops, GDSB_val_t num_trials, double *data)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = { n_procs * problem_size };
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;

    int trial;

    for (trial = 0; trial < num_trials; ++trial) {
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_BYTE,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds);

        double tb, te;

        int i;
        tb = MPI_Wtime();
        for (i = 0; i < num_ops; i++)
            GDS_fence(gds);
        te = MPI_Wtime();

        data[trial] = te - tb;
        GDS_free(&gds);
    }
}

static void print_usage(void)
{
    if (my_rank == 0)
        fprintf(stderr, "Usage: fence --num-ops=NUM_OPS "
            "--problem-size=PROBLEM_SIZE "
            "--num-trials=NUM_TRIALS\n");
}

int main(int argc, char *argv[])
{
    GDS_thread_support_t prov;

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &prov);

    GDS_comm_size(GDS_COMM_WORLD, &n_procs);
    GDS_comm_rank(GDS_COMM_WORLD, &my_rank);

    GDSB_params_t params;
    GDSB_params_init(&params);

    GDSB_parse_cl(argc, argv, &params);

    GDSB_val_t num_ops, problem_size, num_trials;
    if (!GDSB_params_get("NUM_OPS", &num_ops, params)) {
        print_usage();
        exit(1);
    }
    if (!GDSB_params_get("PROBLEM_SIZE", &problem_size, params)) {
        print_usage();
        exit(1);
    }
    if (!GDSB_params_get("NUM_TRIALS", &num_trials, params)) {
        print_usage();
        exit(1);
    }

    double data[num_trials];

    do_fence(problem_size, num_ops, num_trials, data);

    GDSB_print_results("fence", num_trials, data, NULL, params);
    GDSB_params_free(&params);

    GDS_finalize();

    return 0;
}
