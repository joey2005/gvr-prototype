/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <mpi.h>

#include "common.h"

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    GDSB_params_t params;
    GDSB_params_init(&params);
    GDSB_parse_cl(argc, argv, &params);
    int num_trials = 3;
    GDSB_params_get("NUM_TRIALS", &num_trials, params);
    double data[num_trials];
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int op;
    for (op = 0; op < num_trials; ++op) {
        data[op] = (double)rank + 0.1 * op;
    }
    GDSB_print_results("sample", num_trials, data, "Sample GDSBench output", 
        params);
    GDSB_params_free(&params);
    MPI_Finalize();
    return 0;
}
