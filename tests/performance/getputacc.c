/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

//derived from mpimbench/acc.c

#include "common.h"

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <sched.h>
#include <gds.h>

int my_rank;
int n_procs;

typedef void (*rma_op_t)(void *buff, GDS_size_t *ld, GDS_size_t *lo, 
    GDS_size_t *hi, GDS_gds_t gds);

static void put_op(void *buff, GDS_size_t *ld, GDS_size_t *lo, 
    GDS_size_t *hi, GDS_gds_t gds)
{
    GDS_put(buff, ld, lo, hi, gds);
}

static void get_op(void *buff, GDS_size_t *ld, GDS_size_t *lo, 
    GDS_size_t *hi, GDS_gds_t gds)
{
    GDS_get(buff, ld, lo, hi, gds);
}

static void acc_op(void *buff, GDS_size_t *ld, GDS_size_t *lo, 
    GDS_size_t *hi, GDS_gds_t gds)
{
    GDS_acc(buff, ld, lo, hi, MPI_SUM, gds);
}

static void do_rma(rma_op_t rma_op, GDSB_val_t problem_size, GDSB_val_t message_size, 
    GDSB_val_t num_ops, GDSB_val_t num_trials, int rotate_target, double *data)
{
    int n_outstanding = 128;

    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = { n_procs * problem_size };
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;

    GDS_alloc(ndim, cts, min_chunk, GDS_DATA_BYTE,
        GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &gds);

    char *buff_local;
    buff_local = calloc(problem_size, sizeof(*buff_local));

    GDS_fence(gds);
    
    GDS_size_t lo[1];
    GDS_size_t hi[1];
    GDS_size_t ld[0];
    if (rotate_target) {
        lo[0] = problem_size * ((my_rank + 1) % n_procs);
    } else {
        lo[0] = 0;
    }
    hi[0] = lo[0] + message_size - 1;

    int trial;
    for (trial = 0; trial < num_trials; ++trial) {
        int i, n = 0;
        double tb, te; // td_max;

        tb = MPI_Wtime();
        for (i = 0; i < num_ops; i++) {
            rma_op(buff_local, ld, lo, hi, gds);
            if (++n % n_outstanding == 0)
                GDS_wait(gds);
        }
        GDS_wait(gds);
        te = MPI_Wtime();

        data[trial] = te - tb;
    }

    GDS_free(&gds);
    free(buff_local);
}

static void print_usage(void)
{
    if (my_rank == 0)
        fprintf(stderr, "Usage: getputacc --num-ops=NUM_OPS "
            "--message-size=MESSAGE_SIZE --problem-size=PROBLEM_SIZE "
            "--num-trials=NUM_TRIALS "
            "[get|put|acc]\n");
}

int main(int argc, char *argv[])
{
    GDS_thread_support_t prov;

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &prov);

    GDS_comm_size(GDS_COMM_WORLD, &n_procs);
    GDS_comm_rank(GDS_COMM_WORLD, &my_rank);

    GDSB_params_t params;
    GDSB_params_init(&params);
    int optind = GDSB_parse_cl(argc, argv, &params);

    GDSB_val_t num_ops, message_size, problem_size, num_trials;
    if (!GDSB_params_get("NUM_OPS", &num_ops, params)) {
        print_usage();
        exit(1);
    }
    if (!GDSB_params_get("MESSAGE_SIZE", &message_size, params)) {
        print_usage();
        exit(1);
    }
    if (!GDSB_params_get("PROBLEM_SIZE", &problem_size, params)) {
        print_usage();
        exit(1);
    }
    if (!GDSB_params_get("NUM_TRIALS", &num_trials, params)) {
        print_usage();
        exit(1);
    }

    rma_op_t rma_op = NULL;

    if (optind >= argc) {
        print_usage();
        return 1;
    }

    int rotate_target = 1;
    char *name;
    if (strcmp(argv[optind], "put") == 0) {
        rma_op = put_op;
        name = "put";
    } else if (strcmp(argv[optind], "get") == 0) {
        rma_op = get_op;
        name = "get";
    } else if (strcmp(argv[optind], "acc") == 0) {
        rma_op = acc_op;
        rotate_target = 0;
        name = "acc";
    } else {
        print_usage();
        return 1;
    }

    rma_op = put_op; 
    double data[num_trials];

    do_rma(rma_op, problem_size, message_size, num_ops, num_trials, rotate_target, 
        data);

    GDSB_print_results(name, num_trials, data, NULL, params);
    GDSB_params_free(&params);

    GDS_finalize();

    return 0;
}

