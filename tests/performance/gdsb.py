#!/usr/bin/env python
#
#  Global View Resilience (GVR)
#  http://gvr.cs.uchicago.edu
#
#  Copyright (C) 2014 University of Chicago.
#  See license.txt in top-level directory.
#
# A python library for parsing GDSB log files into an sqlite3 database
#

import sqlite3
import string
from math import sqrt

# Fields in the SQL table
FIELDS = (('TESTNAME', 'TEXT'),
    ('NUM_PROCS', 'INTEGER'),
    ('PROBLEM_SIZE', 'INTEGER'),
    ('MESSAGE_SIZE', 'INTEGER'),
    ('NUM_OPS', 'INTEGER'),
    ('NUM_TRIALS', 'INTEGER'),
    ('MEAN', 'REAL'),
    ('STDDEV', 'REAL'),
    ('RESULTS', 'DATA'))
    

# Schema for our SQL table
SCHEMA = ('CREATE TABLE IF NOT EXISTS GDSB (' + 
    string.join(map(lambda x: string.join(x, ' '), FIELDS), ', ') + ')')

# key is test name, value is a list of params required and their default values
TESTS = {'comp_gds': (('NUM_OPS', 10000), ('NUM_TRIALS', 5)),
         'comp_thread': (('NUM_OPS', 10000), ('NUM_TRIALS', 5)),
         'verinc': (('NUM_OPS', 1), ('NUM_TRIALS', 5), ('PROBLEM_SIZE', 1)),
         'alloc': (('NUM_OPS', 10), ('NUM_TRIALS', 5), ('PROBLEM_SIZE', 1)),
         'getputacc': (('NUM_OPS', 10), ('NUM_TRIALS', 5), ('PROBLEM_SIZE', 1),
             ('MESSAGE_SIZE', 1), ('GETPUTACC', 'get'))}
         

# Converts our data field (stored as sqlite TEXT into a list of lists of results
def __convert_data(s): 
    by_proc = s.split(';')
    with_proc_ids = map(lambda x: x.split(':'), by_proc)
    with_proc_ids.sort(key = lambda x: int(x[0]))
    no_proc_ids = map(lambda x: x[1], with_proc_ids)
    sep_results = map(lambda x: x.split(',')[:-1], no_proc_ids)
    as_float = map(lambda y: map(lambda x: float(x), y), sep_results)
    transposed = zip(*as_float)
    return transposed

sqlite3.register_converter('DATA', __convert_data)

# converts the results of __convert_data into a mean and a variance
def __process_results(r, num_ops):
    max_over_proc = map(lambda x: max(x)/num_ops, r)
    mean = sum(max_over_proc) / len(max_over_proc)
    stddev = sqrt(sum(map(lambda x: (x - mean)**2, max_over_proc)))
    return (mean, stddev)
     

# add an item to the table
def __add_entry(con, this_entry, this_data):
    this_entry['RESULTS'] = string.join(this_data, ';')
    (mean, stddev) = __process_results(__convert_data(this_entry['RESULTS']),
        float(this_entry['NUM_OPS']))
    this_entry['MEAN'] = mean
    this_entry['STDDEV'] = stddev
    entry_len = len(this_entry)
    items = this_entry.items()
    keys = [item[0] for item in items] 
    vals = [item[1] for item in items]
    sql = ('INSERT INTO GDSB (' + string.join(keys, ', ') + ') VALUES (' + 
        '?, ' * (entry_len - 1) + '?)')
    con.execute(sql, vals)
    con.commit()

# Create a new connection and initialize our tables
# GDSB is the table containing raw results
# GDSB_Processed has processed results instead
def init(db):
    con = sqlite3.connect(db, detect_types=sqlite3.PARSE_DECLTYPES)
    con.execute(SCHEMA)
    return con

# Add the results from a log file to the table
def add_log(con, filename):
    f = open(filename)
    this_entry = {}
    this_data = []
    for line in f:
        if not line.strip(): #blank line signifies the end of a record
            __add_entry(con, this_entry, this_data)
            this_entry = {}
            this_data = []
            continue
        if line[0] == '#': #comment
            continue
        (key, value) = line.strip().split('=')
        if key == 'DATA':
            this_data.append(value)
            continue
        this_entry[key] = value
    f.close()

def __make_task_dicts_help(args, param_dict):
    if not args:
        return [{}] 
    ret = []
    arg = args[0]
    param_name = arg[0]
    try:
        param_vals = param_dict[param_name] 
    except KeyError:
        param_vals = (arg[1],)
    for val in param_vals:
        to_add = map(lambda x: dict(x.items() + [(param_name, val)]),
            __make_task_dicts_help(args[1:], param_dict))
        ret += to_add
    return ret

# takes a dict of parameters for an experiment and makes them into tasks
def __make_task_dicts(param_dict):
    import os
    tasks = []
    try:
        nprocs = param_dict['NUM_PROCS']
    except KeyError:
        nprocs = (1,)
    try:
        tests = param_dict['TESTNAME']
    except KeyError:
        tests = TESTS.keys()
    for test_name in tests:
        args = TESTS[test_name]
        test_path = os.path.abspath(test_name)
        for nproc_num in nprocs:
            my_contribution = {'TEST_PATH' : test_path, 'NUM_PROCS' : nproc_num}
            toadd = map(lambda x: dict(my_contribution.items() + x.items()),
                 __make_task_dicts_help(args, param_dict))
            tasks += toadd
    tasks = filter(lambda x: (not x.has_key('MESSAGE_SIZE')) or 
        (x['MESSAGE_SIZE'] <= x['PROBLEM_SIZE']), tasks)
    return tasks

# takes a task and outputs the corresponding commandline
# params:
# task_dict: a dictionary specifying a task as output by
#   __make_task_dicts
# hosts: path for mpi hosts file. Use None if you don't want
#   to specify a hosts file
# outfile: name of the file to redirect output. Use None
#   if you don't want output redicrected.
def __task_dict_to_cl(task_dict, hosts, outfile):
    prefix = ''
    path = ''
    body = ''
    suffix = ''
    out = ''
    if outfile:
        out = '> {0}'.format(outfile)
    for param_name in task_dict:
        val = task_dict[param_name]
        if param_name == 'TEST_PATH':
            path = val + ' '
        elif param_name == 'NUM_PROCS':
            hosts_arg = ''
            if hosts:
                hosts_arg = '-f {0} '.format(hosts)
            prefix = 'mpiexec {0}-n {1:d} '.format(hosts_arg, val) 
        elif param_name == 'GETPUTACC':
            suffix = val + ' '
        else:
            cl_param_name = param_name.lower().replace('_', '-')
            body += '--{0}={1:d} '.format(cl_param_name, val)
    return prefix + path + body + suffix + out

# makes a list of tasks given a dictionary of parameters
# params:
# param_dict: a dictionary of parameters for this experiment. See
#   main function for an example
# hosts: path of mpi hosts file. Use None if there is no hosts file
# outfile_basename: Output will be redirected to a file called:
#   outfile_basenameSOME_NUMBER where the number is chosen in sequence.
#   Choose None if you don't want output to be redirected
def make_tasks(param_dict, hosts, outfile_basename):
    task_dicts = __make_task_dicts(param_dict)
    tasks = []
    task_no = 0
    for td in task_dicts:
        outfile = ''
        if outfile_basename:
            outfile = '{0}{1:05d}.log'.format(outfile_basename, task_no)
        tasks.append(__task_dict_to_cl(td, hosts, outfile))
        task_no += 1
    return tasks

if __name__ == '__main__':
    from os import system
    # This script doesn't have a commandline interface yet, so below are some
    # examples of what you can do with it. You can either import its public
    # functions into another python program, or you can uncomment and modify
    # one of the examples below and run with "python gdsb.py"

    # USAGE EXAMPLE: run experiment on lssg
    # param_dict = {
    #             'NUM_PROCS' : (1, 4, 8, 16),
    #             'PROBLEM_SIZE' : (1024**0, 1024**1, 1024**2, 1024**3),
    #             'MESSAGE_SIZE' : (1024**0, 1024**1, 1024**2),
    #             'GETPUTACC' : ('get', 'put', 'acc')}
    # tasks = make_tasks(param_dict, '/home/zar1/hosts', 'GDSB01_')
    # task_no = 0
    #     n_tasks = len(tasks)
    #     for task in tasks:
    #         print 'Performing task {0:d}/{1:d}\n{2}'.format(task_no, n_tasks, task)
    #         system(task)
    #         task_no += 1

    # USAGE EXAMPLE: output commands on midway to be included in an sbatch file
    # param_dict = {
    #              'NUM_PROCS' : (1, 8, 64, 256),
    #              'PROBLEM_SIZE' : (1000**0, 1000**1, 1000**2, 1000**3),
    #              'MESSAGE_SIZE' : (1000**0, 1000**1, 1000**2),
    #             'GETPUTACC' : ('get', 'put', 'acc')}
    # tasks = make_tasks(param_dict, '', '')
    # for task in tasks:
    #    print task

    # USAGE EXAMPLE: add two logfiles to a sqlite3 db and make a query
    # con = init(':memory:')
    # add_log(con, 'verinc.log')
    # add_log(con, 'comp_gds.log')
    # print con.execute('SELECT * FROM GDSB').fetchall()
    # con.close()

    # USAGE EXAMPLE: dump results to a csv file
    # con = init('GDSB_00001.db')
    # add_log(con, '/project/aachien/data/gdsb/results/GDSB_00001.log')
    # con.close()
    # this line borrowed from http://stackoverflow.com/questions/5776660/export-from-sqlite-to-csv-using-shell-script
    # cmd = 'sqlite3 -header -csv GDSB_00001.db "select TESTNAME, NUM_PROCS, PROBLEM_SIZE, MESSAGE_SIZE, NUM_OPS, NUM_TRIALS, MEAN, STDDEV from GDSB;" > GDSB_00001.csv'
    # system(cmd)
