/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>

#include "common.h"

#ifdef THREAD
#define _REENTRANT
#include <pthread.h>
#endif

#include <mpi.h>

#ifdef GDS
#include <gds.h>
#endif

#define ARRAY_SIZE (1024*1024*1024)
#define N_TRY 20
// num ops is 20

static double do_computation(double a, double b)
{
    int i;
    for (i = 0; i < ARRAY_SIZE; i++)
        a = a*3 + b;

    return a;
}

#ifdef THREAD

volatile int thread_flag = 1;

static void *thread_fun(void *arg)
{
    while (thread_flag) {
        int i;
        for (i = 0; i < 1000000; i++)
            __asm__ __volatile__("");
    }
    return NULL;
}
#endif

int main(int argc, char *argv[])
{
    double tb, te, t;
    int n;
    int rank, nprocs;

#ifdef GDS
    GDS_thread_support_t prov;
    GDS_init(&argc, &argv, GDS_THREAD_MULTIPLE, &prov);
#elif THREAD
    int req = MPI_THREAD_MULTIPLE, prov;
    pthread_t th;
    MPI_Init_thread(&argc, &argv, req, &prov);
    if (prov < req) {
        fprintf(stderr, "Thread level was not satisfied\n");
        return 1;
    }

    pthread_create(&th, NULL, thread_fun, NULL);
    sleep(1);
#else
    MPI_Init(&argc, &argv);
#endif

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    GDSB_params_t params;
    GDSB_params_init(&params);
    GDSB_parse_cl(argc, argv, &params);
    GDSB_val_t num_trials, num_ops;
    if (!GDSB_params_get("NUM_TRIALS", &num_trials, params)) {
        fprintf(stderr, "Requires --num-trials parameter\n");
        exit(1);
    }
    if (!GDSB_params_get("NUM_OPS", &num_ops, params)) {
        fprintf(stderr, "Requires --num-ops parameter\n");
        exit(1);
    }
    double data[num_trials];

    for (n = 0; n < num_trials; n++) {
        int op;
        tb = MPI_Wtime();
        for (op = 0; op < num_ops; ++op) {
            do_computation(rand(), rand());
        }
        te = MPI_Wtime();

        t = te - tb;
        data[n] = t;
    }
    char *test_name;
#ifdef GDS
    test_name = "comp_gds";
#else
    test_name = "comp_thread";
#endif

    GDSB_print_results(test_name, num_trials, data, NULL, params);
    GDSB_params_free(&params);

#ifdef GDS
    GDS_finalize();
#else
    MPI_Finalize();
#endif

    return 0;
}
