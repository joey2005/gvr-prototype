#!/usr/bin/python

import sys
import numpy
import mktbl_arraystr

arraytypes = set(['flat'])
versions = set([])
table = {}

kvdict = {}

def array_type_string(kvdict):
    basetype = kvdict['type']
    bs = ''
    subtype = ''
    if basetype == 'flat' and int(kvdict['skip-versioning']) != 0:
        subtype = '-nover'
    elif basetype == 'log':
        bs = '-bs' + kvdict['block-size']
    elif basetype == 'lrds':
        subtype = mktbl_arraystr.lrds_subtype(kvdict)
        bs = '-bs' + kvdict['block-size']
    r = '-r%d' % (int(float(kvdict['read-ratio']) * 100))
    k = '-k' + kvdict['k']
    i = '-i' + kvdict['interval']
    return basetype + subtype + bs + k + r + i

counter = {}
lrds_overhead = []
lrds_counter = {}

for line in sys.stdin:
    if line[0] == '#':
        continue;

    pad_head = len('[xxx] ')
    if line[pad_head:pad_head+len('counter.c')] == 'counter.c':
        cl = line.rstrip().split(',')
        if cl[2] == 'PUT_FIRST':
            ver = int(cl[1]) + 1 # make it 1-origin
            val = int(cl[3])
            versions.add(ver)
            if not counter.has_key(ver):
                counter[ver] = []
            counter[ver].append(val)
        continue
    elif line[pad_head:pad_head+len('target.c')] == 'target.c':
        cl = line[pad_head+len('target.c'):].rstrip().split(' ')
        # [':939:', 'LRDS', 'region', 'overhead', '1048648']
        # [':1112:', 'LRDS', 'version', '0,', 'size', '2973744']
        if cl[2] == 'region':
            lrds_overhead.append(int(cl[4]))
        elif cl[2] == 'version':
            ver = int(cl[3].rstrip(',')) + 1 # make it 1-origin
            val = int(cl[5])
            versions.add(ver)
            if not lrds_counter.has_key(ver):
                lrds_counter[ver] = []
            lrds_counter[ver].append(val)
        continue

    kv = line.rstrip().split('=')
    if len(kv) < 2:
        kvdict.clear()
        counter.clear()
        lrds_counter.clear()
        del lrds_overhead[:]
        continue
    if kv[0] == "DATA":
        if kv[1][0:2] != '0:':
            continue
        else:
            # commit
            arraytype = array_type_string(kvdict)
            arraytypes.add(arraytype)
            coeff = 1
            flat_pv_size = int(kvdict['array-size']) * 1024 * 1024
            log_md_ovhd = 0
            if kvdict['type'] == 'lrds':
                memuse = numpy.mean(lrds_overhead) + flat_pv_size
                c = lrds_counter
            else:
                # log
                memuse = int(kvdict['block-size'])
                log_md_ovhd = 4 * (flat_pv_size / int(kvdict['block-size']))
                c = counter
                coeff = int(kvdict['block-size'])
            flat_memuse = 0
            for ver in sorted(c.keys()):
                memuse += numpy.mean(c[ver]) * coeff + log_md_ovhd
                flat_memuse += flat_pv_size
                key = (ver, arraytype)
                table[key] = memuse
                table[(ver, 'flat')] = flat_memuse

    kvdict[kv[0]] = kv[1]

fst_row = True
for v in sorted(versions):
    if fst_row:
        print "version",
        for t in sorted(arraytypes):
            print "\t%s" % t,
        print ""
        fst_row = False

    print "%d" % v,
    for t in sorted(arraytypes):
        try:
            key = (v, t)
            print "\t%d" % table[key],
        except KeyError:
            print "\t0",
    print ""
