#!/usr/bin/python

import sys
import numpy
import mktbl_arraystr

n_versions = set([])
arraytypes = set([])
table = {}

kvdict = {}

def array_type_string(kvdict):
    basetype = kvdict['type']
    bs = ''
    subtype = ''
    if basetype == 'flat' and int(kvdict['skip-versioning']) != 0:
        subtype = '-nover'
    elif basetype == 'log':
        bs = '-bs' + kvdict['block-size']
    elif basetype == 'lrds':
        subtype = mktbl_arraystr.lrds_subtype(kvdict)
        bs = '-bs' + kvdict['block-size']
    k = '-k' + kvdict['k']
    return basetype + subtype + bs + k

step = 0.025

def mkhist(data):
    h = {}
    for v in data:
        idx = int(v / step)
        can_val = idx * step
        if h.has_key(can_val):
            h[can_val] = h[can_val] + 1
        else:
            h[can_val] = 1
    return h

for line in sys.stdin:
    if line[0] == '#':
        continue;

    kv = line.rstrip().split('=')
    if len(kv) < 2:
        kvdict.clear()
        continue
    if kv[0] == "DATA":
        if kv[1][0:2] != '0:':
            continue
        else:
            # commit
            arraytype = array_type_string(kvdict)
            n_ver = int(int(kvdict['restore-version']))
            data_strs = kvdict['restore-random-raw'].rstrip('\n').rstrip(',').split(',')
            data = map(lambda x: x*1000.0, map(float, data_strs))
            hist = mkhist(data)
            fname = "synth-restore-random-fr" + kvdict['restore-fill-ratio'] \
                    + '-' + arraytype + "-v" + str(n_ver) + ".dat"
            f = open(fname, "w")
            f.write("time cdf\n")
            f.write("0.0 0.0\n")
            sm = 0.0
            for k in sorted(hist.keys()):
                sm += (float(hist[k]) / len(data))
                f.write("%f %f\n" % (k, sm))
            f.close()
    kvdict[kv[0]] = kv[1]
