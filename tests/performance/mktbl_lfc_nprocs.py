#!/usr/bin/python

import sys
import numpy

nprocs = set([])
arraytypes = set([])
table = {}

kvdict = {}

def array_type_string(kvdict):
    basetype = kvdict['type']
    if basetype == 'log':
        bs = '-bs' + kvdict['block-size']
    else:
        bs = ''
    s = '-s' + kvdict['array-size']
    i = '-i' + kvdict['interval']
    r = '-r%d' % (int(float(kvdict['read-ratio']) * 100))
    k = '-k' + kvdict['k']
    return basetype + bs + s + i + k + r

for line in sys.stdin:
    if line[0] == '#':
        continue;

    kv = line.rstrip().split('=')
    if len(kv) < 2:
        kvdict.clear()
        continue
    if kv[0] == "DATA":
        if kv[1][0:2] != '0:':
            continue
        else:
            # commit
            arraytype = array_type_string(kvdict)
            arraytypes.add(arraytype)
            nproc = int(kvdict['NUM_PROCS'])
            nprocs.add(nproc)
            key = (nproc, arraytype)
            data_strs = kv[1][2:].rstrip('\n').rstrip(',').split(',')
            if not table.has_key(key):
                table[key] = []
            table[key].append(numpy.mean(map(float, data_strs)) / float(kvdict['num-versions']))
    kvdict[kv[0]] = kv[1]

fst_col = True
for i in sorted(nprocs):
    if fst_col:
        print "nprocs",
        for t in sorted(arraytypes):
            print "\t%s" % t,
        print ""
        fst_col = False

    print "%d" % i,
    for  t in sorted(arraytypes):
        key = (i, t)
        try:
            print "\t%f" % numpy.mean(table[key]),
        except KeyError:
            print "\t-1.0",
    print ""
