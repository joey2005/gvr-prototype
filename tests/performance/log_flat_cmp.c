/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include "common.h"

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <unistd.h>
#include <sched.h>
#include <gds.h>
#include <getopt.h>
#include <stdbool.h>
#include <float.h>
#include <ctype.h>

#define CYCLE 10

int my_rank;
int n_procs;

double apex_map_k = -1.0;

struct array_conf {
    int block_size;
    int msg;
    int lrds_tracking;
    int lrds_versioning; /* incremental or decremental */
};

struct benchmark {
    size_t array_size_each;
    size_t width;
    int cycle;
    int n_try;
    double read_ratio;
    int ver_interval;
    int n_versions;
    bool skip_free;
    int skip_versioning;
    int restore_mode;
    int restore_version;
    double restore_fill_ratio;
};

enum specific_options {
    OPT_LRDS_TRACKING = 256,
    OPT_LRDS_VERSIONING,
    OPT_RESTORE_AT_ONCE,
    OPT_RESTORE_RANDOM,
    OPT_RESTORE_FILL_RATIO,
};

struct time_stat {
    double min;
    double avg;
    double max;
    double stdev;

    double *raw;
};

static void calc_stat(const double *times, int n, struct time_stat *tstat)
{
    int i;
    double sum = 0.0, var = 0.0;
    tstat->min = DBL_MAX;
    tstat->max = 0.0;
    for (i = 0; i < n; i++) {
        sum += times[i];
        if (tstat->min > times[i]) tstat->min = times[i];
        if (tstat->max < times[i]) tstat->max = times[i];
    }
    tstat->avg = sum / n;
    for (i = 0; i < n; i++) {
        double d = times[i] - tstat->avg;
        var += d * d;
    }
    var /= (n-1);
    tstat->stdev = sqrt(var);
}

static void init_rand(long seed)
{
    srand48(seed);
}

/* Formula taken from APEX-Map apex-map-seq.c */
static ssize_t get_variance(const struct benchmark *bm)
{
    size_t half_size = bm->array_size_each * n_procs / 2;
    size_t base = pow(drand48(), 1/apex_map_k) * (half_size / bm->width - 1);
    ssize_t sign = mrand48() >= 0 ? 1 : -1;

    return sign * base * bm->width;
}

static size_t get_loindex(const struct benchmark *bm)
{
    GDS_size_t base_idx = my_rank * bm->array_size_each + bm->array_size_each / 2;
    size_t total_size = bm->array_size_each * n_procs;
    ssize_t v;
    GDS_size_t lo;

retry:
    v = get_variance(bm);
    lo = base_idx + v;

    if (v < 0 && lo > total_size)
        goto retry;

    if (lo + bm->width >= total_size)
        goto retry;

    return lo;
}

static MPI_Info setup_info(const char *array_repr,
    const struct array_conf *conf, const struct benchmark *bm)
{
    MPI_Info info;

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_REPR_KEY, array_repr);
    if (strcmp(array_repr, GDS_REPR_LOG) == 0) {
        GDS_info_set_int(info, GDS_LOG_BS_KEY, conf->block_size);
        if (getenv("GDS_LOG_PREALLOC_DEFAULT") == NULL)
            GDS_info_set_int(info, GDS_LOG_PREALLOC_VERS_KEY, bm->n_versions * 1.25);
    } else if (strcmp(array_repr, GDS_REPR_LRDS) == 0) {
        GDS_info_set_int(info, GDS_LRDS_PROP_DIRTY_TRACKING_KEY, conf->lrds_tracking);
        GDS_info_set_int(info, GDS_LRDS_PROP_VERSIONING_KEY, conf->lrds_versioning);
        GDS_info_set_int(info, GDS_LRDS_PROP_DIRTY_BLOCK_SIZE_KEY,
            conf->block_size);
    }

    if (conf->msg) {
        GDS_info_set_int(info, GDS_PUT_IMPL_KEY, GDS_PUT_IMPL_MSG);
        GDS_info_set_int(info, GDS_GET_IMPL_KEY, GDS_GET_IMPL_MSG);
    }

    return info;
}

static void run_bwbench(const char *array_repr, const struct array_conf *conf,
    const struct benchmark *bm, double total_time[])
{
    char *buff = NULL;
    int i;
    MPI_Info info;
    int ops_per_ver = bm->cycle * bm->ver_interval;
    int n_reads = (int) (ops_per_ver * bm->read_ratio);

    info = setup_info(array_repr, conf, bm);

    buff = malloc(bm->width * ops_per_ver);
    assert(buff);
    memset(buff, 1, bm->width * ops_per_ver);

    for (i = -1; i < bm->n_try; i++) {
        /* i == -1 for warm up */
        GDS_status_t s;
        GDS_gds_t gds;
        double tb, te;
        size_t count[1] = { bm->array_size_each * n_procs };
        size_t transferred_bytes = 0;

        s = GDS_alloc(1, count, NULL, GDS_DATA_BYTE, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, info, &gds);
        assert(s == GDS_STATUS_OK);

        /* No initialization needed */

        MPI_Barrier(MPI_COMM_WORLD);

        tb = MPI_Wtime();

        int n;
        for (n = 0; n < bm->n_versions; n++) {
            int p;

            for (p = 0; p < n_reads; p++) {
                GDS_size_t lo = get_loindex(bm);
                GDS_size_t hi = lo + bm->width - 1;
                if (hi >= bm->array_size_each * n_procs)
                    hi = bm->array_size_each * n_procs - 1;

                //printf("%d: get(%zu+%zu)\n", my_rank, lo, hi + 1 - lo);
                GDS_get(buff + bm->width * p, NULL, &lo, &hi, gds);
            }

            GDS_wait(gds);

            /* Do puts */
            for (p = n_reads; p < ops_per_ver; p++) {
                GDS_size_t lo = get_loindex(bm);
                GDS_size_t hi = lo + bm->width - 1;
                if (hi >= bm->array_size_each * n_procs)
                    hi = bm->array_size_each * n_procs - 1;

                //printf("%d: put(%zu+%zu)\n", my_rank, lo, hi + 1 - lo);
                GDS_put(buff + bm->width * p, NULL, &lo, &hi, gds);
            }
            transferred_bytes += bm->width * ops_per_ver;

            /* Version inc */
            //printf("%d: version_inc()\n", my_rank);
            if (bm->skip_versioning)
                continue;
            GDS_version_inc(gds, 1, NULL, 0);
        }

        if (bm->skip_versioning)
            GDS_fence(gds);

        te = MPI_Wtime();

        if (i >= 0)
            total_time[i] = te - tb;

        if (bm->skip_free) {
            total_time[0] = te - tb;
            break;
        }

        GDS_free(&gds);
    }

    MPI_Info_free(&info);

    free(buff);
}

static GDS_gds_t setup_gds_for_restore(const char *array_repr,
    const struct array_conf *conf, const struct benchmark *bm)
{
    MPI_Info info;
    info = setup_info(array_repr, conf, bm);

    GDS_status_t s;
    GDS_gds_t gds;
    size_t count[1] = { bm->array_size_each * n_procs };
    int buff[100];
    int v, j;

    s = GDS_alloc(1, count, NULL, GDS_DATA_BYTE, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, info, &gds);
    assert(s == GDS_STATUS_OK);

    for (v = 0; v < sizeof(buff) / sizeof(buff[0]); v++)
        buff[v] = v;

    /* Fill */
    for (v = 0; v < bm->n_versions; v++) {
        GDS_size_t lo = my_rank * bm->array_size_each, hi;
        j = 0;
        for (; lo + sizeof(int) <= (my_rank+1) * bm->array_size_each; lo += conf->block_size) {
            hi = lo + sizeof(int) - 1;
            if (drand48() < bm->restore_fill_ratio)
                GDS_put(buff+j, NULL, &lo, &hi, gds);
            if (++j >= sizeof(buff) / sizeof(buff[0])) {
                j = 0;
                GDS_wait_local(gds);
            }
        }
        GDS_version_inc(gds, 1, NULL, 0);
    }

    MPI_Info_free(&info);

    return gds;
}

static void run_restorebench(GDS_gds_t gds, const struct benchmark *bm,
    double total_time[], struct time_stat *time_stat)
{
    int i;
    int ops_per_ver = bm->cycle * bm->ver_interval;

    for (i = 0; i < bm->n_try; i++) {
        double tb = 0.0, te = 0.0;

        GDS_move_to_newest(gds);

        switch (bm->restore_mode) {
            int n;
        case OPT_RESTORE_AT_ONCE: {
            /* Measuring version restoration performance */
            char *restore_buff = malloc(bm->array_size_each);
            assert(restore_buff);
            memset(restore_buff, 1, bm->array_size_each);
            GDS_size_t lo = bm->array_size_each * my_rank;
            GDS_size_t hi = bm->array_size_each * (my_rank+1) - 1;
            for (n = 0; n < bm->restore_version; n++)
                GDS_move_to_prev(gds);
            tb = MPI_Wtime();
            GDS_get(restore_buff, NULL, &lo, &hi, gds);
            GDS_wait(gds);
            te = MPI_Wtime();
            free(restore_buff);
            break;
        }
        case OPT_RESTORE_RANDOM: {
            int p;
            double orig_k = apex_map_k;
            struct time_stat *ts = time_stat + (i < 0 ? 0 : i);
            for (n = 0; n < bm->restore_version; n++)
                GDS_move_to_prev(gds);
            apex_map_k = 1.0;
            tb = MPI_Wtime();
            for (p = 0; p < ops_per_ver; p++) {
                GDS_size_t lo = get_loindex(bm);
                GDS_size_t hi = lo + bm->width - 1;
                char buff[bm->width];
                double trb, tre;
                if (hi >= bm->array_size_each * n_procs)
                    hi = bm->array_size_each * n_procs - 1;
                trb = MPI_Wtime();
                GDS_get(buff, NULL, &lo, &hi, gds);
                GDS_wait(gds);
                tre = MPI_Wtime();
                ts->raw[p] = tre - trb;
            }
            te = MPI_Wtime();
            apex_map_k = orig_k;
            calc_stat(ts->raw, ops_per_ver, ts);
            break;
        }
        default:
            break;
        }

        if (i >= 0)
            total_time[i] = te - tb;
    }
}

static int *parse_restore_versions(const char *orig_buf, int *nvs)
{
    int alloc_max = 100, a, n = 0;
    int *ret_a = malloc(sizeof(int) * alloc_max);
    const char *strp = orig_buf;

    for (strp = orig_buf; *strp; strp++) {
        if (!isdigit(*strp))
            continue;
        a = strtol(strp, (char **) &strp, 10);
        ret_a[n++] = a;
        if (n >= alloc_max) {
            alloc_max *= 2;
            ret_a = realloc(ret_a, sizeof(int) * alloc_max);
        }
        if (*strp == '\0')
            break;
    }

    *nvs = n;
    return ret_a;
}

static void print_usage(void)
{
    if (my_rank == 0)
        fprintf(stderr, "Usage: [options]\n"
            "\n"
            "-b, --block-size\n"
            "-i, --interval\n"
            "-k\n"
            "-n, --num-versions\n"
            "-m, --num-trials\n"
            "-r, --read-ratio\n"
            "-s, --array-size\n"
            "-t, --type\n"
            "-w, --width\n"
            "    --msg\n"
            "    --lrds-tracking\n"
            "    --lrds-versioning\n"
            "    --skip-free\n"
            "    --skip-versioning\n"
            "    --restore\n"
            "    --restore-random\n"
            "    --restore-fill-ratio\n"
            );
}

int main(int argc, char *argv[])
{
    int opt, idx, i;
    int skip_free = 0;
    struct array_conf conf = { .block_size = 128, };
    struct benchmark bm = {
        .ver_interval = -1,
        .n_versions = 10,
        .n_try = 5,
        .array_size_each = 16 * 1024 * 1024,
        .cycle = CYCLE,
        .width = 8,
        .restore_fill_ratio = 0.5,
    };
    int *restore_versions = NULL;
    int n_restore_versions = 1;
    struct option opts[] = {
        {"block-size", required_argument, 0, 'b'},
        {"help", no_argument, 0, 'h'},
        {"interval", required_argument, 0, 'i'},
        {"num-versions", required_argument, 0, 'n'},
        {"num-trials", required_argument, 0, 'm'},
        {"read-ratio", required_argument, 0, 'r'},
        {"array-size", required_argument, 0, 's'},
        {"type", required_argument, 0, 't'},
        {"width", required_argument, 0, 'w'},
        {"msg", no_argument, &conf.msg, 1},
        {"lrds-tracking", required_argument, NULL, OPT_LRDS_TRACKING},
        {"lrds-versioning", required_argument, NULL, OPT_LRDS_VERSIONING},
        {"skip-free", no_argument, &skip_free, 1},
        {"skip-versioning", no_argument, &bm.skip_versioning, 1},
        {"restore", required_argument, NULL, OPT_RESTORE_AT_ONCE},
        {"restore-random", required_argument, NULL, OPT_RESTORE_RANDOM},
        {"restore-fill-ratio", required_argument, NULL, OPT_RESTORE_FILL_RATIO},
    };
    GDS_thread_support_t prov;
    const char *type = NULL;

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &prov);

    GDS_comm_size(GDS_COMM_WORLD, &n_procs);
    GDS_comm_rank(GDS_COMM_WORLD, &my_rank);

    init_rand(my_rank);

    GDSB_params_t params;

    while ((opt = getopt_long(argc, argv, "b:hi:k:m:n:r:s:t:w:", opts, &idx)) != -1) {
        switch (opt) {
        case 'b':
            conf.block_size = atoi(optarg);
            break;

        case 'h':
            print_usage();
            return 0;

        case 'i':
            bm.ver_interval = atoi(optarg);
            break;

        case 'k':
            apex_map_k = atof(optarg);
            break;

        case 'm':
            bm.n_try = atoi(optarg);
            break;

        case 'n':
            bm.n_versions = atoi(optarg);
            break;

        case 'r':
            bm.read_ratio = atof(optarg);
            break;

        case 's':
            bm.array_size_each = atoi(optarg) * 1024 * 1024;
            break;

        case 't':
            switch (optarg[0]) {
            case 'F':
            case 'f':
                type = GDS_REPR_FLAT;
                break;

            case 'L':
            case 'l':
                if (strcasecmp(optarg, "lrds") == 0)
                    type = GDS_REPR_LRDS;
                else
                    type = GDS_REPR_LOG;
                break;
            }
            break;

        case 'w':
            bm.width = atoi(optarg);
            break;

        case OPT_LRDS_TRACKING:
            conf.lrds_tracking = atoi(optarg);
            break;

        case OPT_LRDS_VERSIONING:
            conf.lrds_versioning = atoi(optarg);
            break;

        case OPT_RESTORE_AT_ONCE:
        case OPT_RESTORE_RANDOM:
            bm.restore_mode = opt;
            restore_versions = parse_restore_versions(optarg, &n_restore_versions);
            break;

        case OPT_RESTORE_FILL_RATIO:
            bm.restore_fill_ratio = atof(optarg);
            break;

        case 0:
            continue;

        default:
            fprintf(stderr, "Unknown option: %c\n", opt);
            print_usage();
            return 1;
        }
    }

    if (type == NULL) {
        fprintf(stderr, "type must be either log, flat, or lrds.\n");
        print_usage();
        return 1;
    }

    if (bm.ver_interval < 0.0) {
        fprintf(stderr, "interval must be positive.\n");
        print_usage();
        return 1;
    }

    if (apex_map_k < 0.0 || apex_map_k > 1.0) {
        fprintf(stderr, "k must satisfy 0 <= k <= 1.\n");
        print_usage();
        return 1;
    }

    if (bm.read_ratio < 0.0 || bm.read_ratio > 1.0) {
        fprintf(stderr, "r must satisfy 0 <= k <= 1.\n");
        print_usage();
        return 1;
    }

    if (bm.restore_version < 0 || bm.restore_version > bm.n_versions) {
        fprintf(stderr, "0 <= restore-version <= num-versions\n");
        print_usage();
        return 1;
    }

    if (bm.restore_fill_ratio < 0.0 || bm.restore_fill_ratio > 1.0) {
        fprintf(stderr, "restore-fill-ratio must satisfy 0 <= r <= 1.\n");
        print_usage();
        return 1;
    }

    if (skip_free && bm.n_try > 1) {
        fprintf(stderr, "num-trials must be 1 when --skip-free is present.\n");
        print_usage();
        return 1;
    }

    bm.skip_free = skip_free;

    double total_time[bm.n_try];
    struct time_stat time_stat[bm.n_try];
    GDS_gds_t gds_restore;

    for (i = 0; i < bm.n_try; i++) {
        int ops_per_ver = bm.ver_interval * bm.cycle;
        time_stat[i].raw = malloc(sizeof(double) * ops_per_ver);
        assert(time_stat[i].raw);
    }

    if (bm.restore_mode)
        gds_restore = setup_gds_for_restore(type, &conf, &bm);

    for (i = 0; i < n_restore_versions; i++) {
        if (restore_versions != NULL)
            /* Do restoration in reversal order */
            bm.restore_version = restore_versions[n_restore_versions - i - 1];

        GDSB_params_init(&params);

        GDSB_params_add("block-size", conf.block_size, &params);
        GDSB_params_add("msg", conf.msg, &params);
        GDSB_params_add("num-versions", bm.n_versions, &params);
        GDSB_params_add("num-trials", bm.n_try, &params);
        GDSB_params_add("array-size", bm.array_size_each / (1024*1024), &params);
        GDSB_params_add("cycle", bm.cycle, &params);
        GDSB_params_add("interval", bm.ver_interval, &params);
        GDSB_params_add("width", bm.width, &params);
        GDSB_params_add("lrds-tracking", conf.lrds_tracking, &params);
        GDSB_params_add("lrds-versioning", conf.lrds_versioning, &params);
        GDSB_params_add("skip-versioning", bm.skip_versioning, &params);
        GDSB_params_add("restore-mode", bm.restore_mode, &params);
        GDSB_params_add("restore-version", bm.restore_version, &params);

        if (bm.restore_mode)
            run_restorebench(gds_restore, &bm, total_time, time_stat);
        else
            run_bwbench(type, &conf, &bm, total_time);

        if (my_rank == 0) {
            printf("type=%s\n", type);
            printf("k=%.4f\n", apex_map_k);
            printf("read-ratio=%.2f\n", bm.read_ratio);
            if (bm.restore_mode == OPT_RESTORE_RANDOM) {
                int i, j;
                printf("restore-random-min=");
                for (i = 0; i < bm.n_try; i++) printf("%f,", time_stat[i].min);
                printf("\nrestore-random-max=");
                for (i = 0; i < bm.n_try; i++) printf("%f,", time_stat[i].max);
                printf("\nrestore-random-avg=");
                for (i = 0; i < bm.n_try; i++) printf("%f,", time_stat[i].avg);
                printf("\nrestore-random-stdev=");
                for (i = 0; i < bm.n_try; i++) printf("%f,", time_stat[i].stdev);
                printf("\nrestore-random-raw=");
                for (i = 0; i < bm.n_try; i++)
                    for (j = 0; j < bm.ver_interval * bm.cycle; j++)
                        printf("%.3e,", time_stat[i].raw[j]);
                printf("\n");
            }
            printf("restore-fill-ratio=%.2f\n", bm.restore_fill_ratio);
        }
        GDSB_print_results("log_flat_cmp", bm.n_try, total_time, NULL, params);
        GDSB_params_free(&params);
    }

    for (i = 0; i < bm.n_try; i++)
        free(time_stat[i].raw);

    if (skip_free) {
        fflush(stdout);
        MPI_Barrier(MPI_COMM_WORLD);
        return 0;
    }
    else if (bm.restore_mode)
        GDS_free(&gds_restore);

    GDS_finalize();

    return 0;
}
