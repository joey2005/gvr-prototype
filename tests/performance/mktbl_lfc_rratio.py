#!/usr/bin/python

import sys
import numpy
import mktbl_arraystr

rratios = set([])
arraytypes = set([])
table = {}

kvdict = {}

def array_type_string(kvdict):
    basetype = kvdict['type']
    subtype = ''
    bs = ''
    if basetype == 'flat' and int(kvdict['skip-versioning']) != 0:
        subtype = '-nover'
    elif basetype == 'log':
        bs = '-bs' + kvdict['block-size']
    elif basetype == 'lrds':
        subtype = mktbl_arraystr.lrds_subtype(kvdict)
        bs = '-bs' + kvdict['block-size']
    i = '-i' + kvdict['interval']
    k = '-k' + kvdict['k']
    return basetype + subtype + bs + k + i

for line in sys.stdin:
    if line[0] == '#':
        continue;

    kv = line.rstrip().split('=')
    if len(kv) < 2:
        kvdict.clear()
        continue
    if kv[0] == "DATA":
        if kv[1][0:2] != '0:':
            continue
        else:
            # commit
            arraytype = array_type_string(kvdict)
            arraytypes.add(arraytype)
            ops = int(kvdict['interval']) * int(kvdict['cycle'])
            rratio = int(float(kvdict['read-ratio']) * 100)
            rratios.add(rratio)
            key = (rratio, arraytype)
            data_strs = kv[1][2:].rstrip('\n').rstrip(',').split(',')
            mean_time = numpy.mean(map(float, data_strs)) / float(kvdict['num-versions'])
            throughput = ops / mean_time
            if not table.has_key(key):
                table[key] = []
            table[key].append(throughput)
    kvdict[kv[0]] = kv[1]

fst_col = True
for r in sorted(rratios):
    if fst_col:
        print "rratio",
        for t in sorted(arraytypes):
            print "\t%s" % t,
        print ""
        fst_col = False

    print "%d" % r,
    for t in sorted(arraytypes):
        key = (r, t)
        try:
            print "\t%f" % numpy.mean(table[key]),
        except KeyError:
            print "\t-1.0",
    print ""
