/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdio.h>
#include <memory.h>
#include <assert.h>

#include <gds.h>

#include "common.h"

extern int nprocs, myrank;

static void fill_int(int *buff, size_t nelems, int val)
{
    size_t i;
    for (i = 0; i < nelems; i++)
        buff[i] = val;
}

/*
  Log-structured array test
 */
void log_array_test_impl(const char *test_name, MPI_Info info, const size_t BLOCKSIZE)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {BLOCKSIZE * nprocs};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, m;
    GDS_size_t lo[1], hi[1], ld[] = {0};
    int put_buff[BLOCKSIZE];
    int get_buff[BLOCKSIZE];

    ASSERT_TEST_START(test_name);

    ASSERT_COND(BLOCKSIZE >= 10);

    ASSERT_STAT_FATAL("GDS_alloc", GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    pr_info("Local buffer test (bs=%zu)...\n", BLOCKSIZE);

    /* Local buffer */
    fill_int(put_buff, BLOCKSIZE, -1);
    lo[0] = myrank * BLOCKSIZE; hi[0] = (myrank+1) * BLOCKSIZE - 1;
    ASSERT_STAT("GDS_put", GDS_put(put_buff, ld, lo, hi, gds));
    GDS_wait(gds);
    ASSERT_STAT("GDS_get", GDS_get(get_buff, ld, lo, hi, gds));
    GDS_wait_local(gds);
    ASSERT_EQUAL_INT(memcmp(put_buff, get_buff, sizeof(put_buff)), 0);

    ASSERT_STAT_FATAL("GDS_version_inc", GDS_version_inc(gds, 1, NULL, 0));

    ASSERT_STAT("GDS_get", GDS_get(get_buff, ld, lo, hi, gds));
    GDS_wait_local(gds);
    ASSERT_EQUAL_INT(memcmp(put_buff, get_buff, sizeof(put_buff)), 0);

    GDS_fence(gds);

    pr_info("Neighbor buffer test (bs=%zu)...\n", BLOCKSIZE);

    /* Neighbor buffer */
    fill_int(put_buff, BLOCKSIZE, 1);
    m = (myrank+1) % nprocs;
    lo[0] = m * BLOCKSIZE; hi[0] = (m+1) * BLOCKSIZE - 1;
    ASSERT_STAT("GDS_put", GDS_put(put_buff, ld, lo, hi, gds));
    GDS_wait(gds);
    ASSERT_STAT("GDS_get", GDS_get(get_buff, ld, lo, hi, gds));
    GDS_wait_local(gds);
    ASSERT_EQUAL_INT(memcmp(put_buff, get_buff, sizeof(put_buff)), 0);

    ASSERT_STAT_FATAL("GDS_version_inc", GDS_version_inc(gds, 1, NULL, 0));

    ASSERT_STAT("GDS_get", GDS_get(get_buff, ld, lo, hi, gds));
    GDS_wait_local(gds);
    ASSERT_EQUAL_INT(memcmp(put_buff, get_buff, sizeof(put_buff)), 0);

    GDS_fence(gds);

    pr_info("Partial write test (bs=%zu)...\n", BLOCKSIZE);

    /* Partial write */
    put_buff[0] = myrank;
    lo[0] = hi[0] = myrank*BLOCKSIZE + 4;
    ASSERT_STAT("GDS_put", GDS_put(put_buff, ld, lo, hi, gds));
    GDS_wait(gds);
    m = (myrank+1) % nprocs;
    lo[0] = hi[0] = m*BLOCKSIZE + 10;
    ASSERT_STAT("GDS_put", GDS_put(put_buff, ld, lo, hi, gds));
    lo[0] = hi[0] = (m + 1) * BLOCKSIZE - 3;
    ASSERT_STAT("GDS_put", GDS_put(put_buff, ld, lo, hi, gds));

    GDS_fence(gds);

    lo[0] = myrank*BLOCKSIZE; hi[0] = (myrank+1) * BLOCKSIZE - 1;;
    ASSERT_STAT("GDS_get", GDS_get(get_buff, ld, lo, hi, gds));
    GDS_wait_local(gds);

    m = myrank > 0 ? (myrank - 1) % nprocs : nprocs - 1;
    size_t i;
    for (i = 0; i < BLOCKSIZE; i++) {
        switch (i) {
        case 4:
            if (get_buff[i] != myrank)
                pr_err("rank=%d get_buff[%zu]=%d != %d\n",
                    myrank, i, get_buff[i], myrank);
            ASSERT_EQUAL_INT(get_buff[i], myrank);
            break;
        case 10:
            if (get_buff[i] != m)
                pr_err("rank=%d get_buff[%zu]=%d m=%d\n",
                    myrank, i, get_buff[i], m);
            ASSERT_EQUAL_INT(get_buff[i], m);
            break;
        default:
            if (i == BLOCKSIZE - 3) {
                if (get_buff[i] != m)
                    pr_err("rank=%d get_buff[%zu]=%d != %d\n",
                        myrank, i, get_buff[i], m);
                ASSERT_EQUAL_INT(get_buff[i], m);
            } else {
                if (get_buff[i] != 1)
                    pr_err("rank=%d get_buff[%zu]=%d != 1\n",
                        myrank, i, get_buff[i]);
                ASSERT_EQUAL_INT(get_buff[i], 1);
            }
            break;
        }
    }

    GDS_fence(gds);

    pr_info("Partial read test (bs=%zu)...\n", BLOCKSIZE);

    lo[0] = hi[0] = myrank*BLOCKSIZE;
    GDS_get(get_buff, ld, lo, hi, gds);
    GDS_wait_local(gds);
    if (get_buff[0] != 1)
        pr_err("rank=%d got=%d != %d\n", myrank, get_buff[0], 1);
    ASSERT_EQUAL_INT(get_buff[0], 1);

    lo[0] = hi[0] = myrank*BLOCKSIZE + 4;
    GDS_get(get_buff, ld, lo, hi, gds);
    GDS_wait_local(gds);
    if (get_buff[0] != myrank)
        pr_err("rank=%d got=%d != %d\n", myrank, get_buff[0], myrank);
    ASSERT_EQUAL_INT(get_buff[0], myrank);

    lo[0] = hi[0] = myrank*BLOCKSIZE + 10;
    GDS_get(get_buff, ld, lo, hi, gds);
    m = myrank > 0 ? (myrank - 1) % nprocs : nprocs - 1;
    GDS_wait_local(gds);
    if (get_buff[0] != m)
        pr_err("rank=%d got=%d != %d\n", myrank, get_buff[0], m);
    ASSERT_EQUAL_INT(get_buff[0], m);

    lo[0] = hi[0] = (myrank + 1) * BLOCKSIZE - 3;
    GDS_get(get_buff, ld, lo, hi, gds);
    m = myrank > 0 ? (myrank - 1) % nprocs : nprocs - 1;
    GDS_wait_local(gds);
    if (get_buff[0] != m)
        pr_err("rank=%d got=%d != %d\n", myrank, get_buff[0], m);
    ASSERT_EQUAL_INT(get_buff[0], m);

    if (nprocs == 1)
        goto out;

    GDS_fence(gds);

    pr_info("Multi-process test (bs=%zu)...\n", BLOCKSIZE);

    if (myrank == 0) {
        size_t begin = BLOCKSIZE - 10;
        size_t end   = BLOCKSIZE * (nprocs - 1) + 10;

        for (i = 0; i < BLOCKSIZE; i++)
            put_buff[i] = i;

        for (i = begin; i < end;) {
            size_t len = BLOCKSIZE;
            if (end - i < len)
                len = end - i;
            lo[0] = i;
            hi[0] = i + len - 1;
            assert(len <= BLOCKSIZE);
            assert(hi[0] < end);
            ASSERT_STAT("GDS_put", GDS_put(put_buff, ld, lo, hi, gds));
            i += len;
        }
        GDS_wait(gds);
    }

    GDS_fence(gds);

    if (myrank == nprocs - 1) {
        size_t end;
        lo[0] = 0; hi[0] = BLOCKSIZE - 1;
        ASSERT_STAT("GDS_get", GDS_get(get_buff, ld, lo, hi, gds));
        GDS_wait_local(gds);
        for (i = 0; i < BLOCKSIZE - 10; i++) {
            switch (i) {
            case 4:
                ASSERT_EQUAL_INT(get_buff[i], 0);
                break;
            case 10:
                m = nprocs - 1;
                if (get_buff[i] != m)
                    pr_err("rank=%d get_buff[%zu]=%d m=%d\n",
                        myrank, i, get_buff[i], m);
                ASSERT_EQUAL_INT(get_buff[i], m);
                break;
            default:
                ASSERT_EQUAL_INT(get_buff[i], 1);
                break;
            }
        }

        for (;i < BLOCKSIZE; i++) {
            int expected = i + 10 - BLOCKSIZE;
            if (get_buff[i] != expected)
                pr_err("rank=%d get_buff[%zu]=%d != %d\n",
                    myrank, i, get_buff[i], expected);
            ASSERT_EQUAL_INT(get_buff[i], expected);
        }

        lo[0] = BLOCKSIZE; hi[0] = 2 * BLOCKSIZE - 1;
        ASSERT_STAT("GDS_get", GDS_get(get_buff, ld, lo, hi, gds));
        GDS_wait_local(gds);

        end = nprocs > 2 ? BLOCKSIZE - 10 : 10;

        for (i = 0; i < end; i++) {
            int expected = 10 + i;
            if (get_buff[i] != expected)
                pr_err("rank=%d get_buff[%zu]=%d != %d\n",
                    myrank, i, get_buff[i], expected);
            ASSERT_EQUAL_INT(get_buff[i], expected);
        }
        if (nprocs > 2) {
            for (;i < BLOCKSIZE; i++) {
                int expected = i + 10 - BLOCKSIZE;
                if (get_buff[i] != expected)
                    pr_err("rank=%d get_buff[%zu]=%d != %d\n",
                        myrank, i, get_buff[i], expected);
                ASSERT_EQUAL_INT(get_buff[i], expected);
            }
        }
    }

    GDS_version_inc(gds, 1, NULL, 0);

    pr_info("Multi-process conflicting write test (bs=%zu)...\n",
        BLOCKSIZE);

    if (myrank > BLOCKSIZE)
        goto pass;

    lo[0] = hi[0] = myrank;
    put_buff[0] = myrank * 137 - 5;
    GDS_put(put_buff, ld, lo, hi, gds);

pass:
    GDS_fence(gds);

    if (myrank == 0) {
        int max = nprocs < BLOCKSIZE ? nprocs : BLOCKSIZE;
        lo[0] = 0; hi[0] = max - 1;
        GDS_get(get_buff, ld, lo, hi, gds);
        GDS_wait_local(gds);
        for (i = 0; i < max; i++) {
            int e = i * 137 - 5;
            if (get_buff[i] != e)
                pr_err("rank=%d got[%zu]=%d != %d\n",
                    myrank, i, get_buff[i], e);
            ASSERT_EQUAL_INT(get_buff[i], e);
        }
    }

out:
    ASSERT_STAT("GDS_free", GDS_free(&gds));

    pr_info("exiting %s\n", test_name);

    ASSERT_TEST_END();
}

void log_array_acc_local_test_impl(const char *test_name, MPI_Info info, const size_t BLOCKSIZE)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {BLOCKSIZE * nprocs};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t acclo[1], acchi[1], getlo[1], gethi[1], ld[] = {0};
    int put_buff[BLOCKSIZE];
    int get_buff[BLOCKSIZE];

    ASSERT_TEST_START(test_name);

    ASSERT_COND(BLOCKSIZE >= 10);

    ASSERT_STAT_FATAL("GDS_alloc", GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    put_buff[0] = 1;

    acclo[0] = acchi[0] = BLOCKSIZE * myrank + BLOCKSIZE - 4;
    getlo[0] = BLOCKSIZE * myrank; gethi[0] = BLOCKSIZE * (1 + myrank) - 1;

    /* Accululate to zero page */
    GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_wait(gds);
    GDS_get(get_buff, ld, getlo, gethi, gds);
    GDS_wait_local(gds);

    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], 1);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);

    /* Accululate again */
    GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_wait(gds);
    GDS_get(get_buff, ld, getlo, gethi, gds);
    GDS_wait_local(gds);

    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], 2);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);

    GDS_version_inc(gds, 1, NULL, 0);

    /* Accumulate to new version */
    GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_wait(gds);
    GDS_get(get_buff, ld, getlo, gethi, gds);
    GDS_wait_local(gds);

    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], 3);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);

    GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_wait(gds);
    GDS_get(get_buff, ld, getlo, gethi, gds);
    GDS_wait_local(gds);

    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], 4);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);

    GDS_free(&gds);

    ASSERT_TEST_END();
}

void log_array_acc_remote_test_impl(const char *test_name, MPI_Info info, const size_t BLOCKSIZE)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {BLOCKSIZE * nprocs};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, r;
    GDS_size_t acclo[1], acchi[1], getlo[1], gethi[1], ld[] = {0};
    int put_buff[BLOCKSIZE];
    int get_buff[BLOCKSIZE];

    ASSERT_TEST_START(test_name);

    ASSERT_COND(BLOCKSIZE >= 10);

    ASSERT_STAT_FATAL("GDS_alloc", GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    put_buff[0] = 1;

    r = (myrank + 1) % nprocs;
    acclo[0] = acchi[0] = BLOCKSIZE * r + BLOCKSIZE - 4;
    getlo[0] = BLOCKSIZE * myrank; gethi[0] = BLOCKSIZE * (1 + myrank) - 1;

    /* Accululate to zero page */
    GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_fence(gds);
    GDS_get(get_buff, ld, getlo, gethi, gds);
    GDS_fence(gds);

    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], 1);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);

    /* Accululate again */
    GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_fence(gds);
    GDS_get(get_buff, ld, getlo, gethi, gds);
    GDS_fence(gds);

    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], 2);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);

    GDS_version_inc(gds, 1, NULL, 0);

    /* Accumulate to new version */
    GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_fence(gds);
    GDS_get(get_buff, ld, getlo, gethi, gds);
    GDS_fence(gds);

    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], 3);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);

    GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_fence(gds);
    GDS_get(get_buff, ld, getlo, gethi, gds);
    GDS_fence(gds);

    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], 4);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
    ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);

    GDS_free(&gds);

    ASSERT_TEST_END();
}

#define N_TRY 10000

void log_array_acc_concentrate_test_impl(const char *test_name, MPI_Info info, const size_t BLOCKSIZE)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {BLOCKSIZE * nprocs};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, i;
    GDS_size_t acclo[1], acchi[1], getlo[1], gethi[1], ld[] = {0};
    int put_buff[BLOCKSIZE];
    int get_buff[BLOCKSIZE];

    ASSERT_TEST_START(test_name);

    ASSERT_COND(BLOCKSIZE >= 10);

    ASSERT_STAT_FATAL("GDS_alloc", GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    put_buff[0] = 1;

    acclo[0] = acchi[0] = BLOCKSIZE - 4;

    for (i = 0; i < N_TRY; i++)
        GDS_acc(put_buff, ld, acclo, acchi, MPI_SUM, gds);
    GDS_fence(gds);

    if (myrank == 0) {
        getlo[0] = BLOCKSIZE * myrank; gethi[0] = BLOCKSIZE * (1 + myrank) - 1;
        GDS_get(get_buff, ld, getlo, gethi, gds);
        GDS_wait_local(gds);

        ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 4], N_TRY * nprocs);
        ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 3], 0);
        ASSERT_EQUAL_INT(get_buff[BLOCKSIZE - 5], 0);
    }

    GDS_free(&gds);

    ASSERT_TEST_END();
}
