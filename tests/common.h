/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  Common header
*/

#ifndef __COMMON_H__
#define __COMMON_H__

#include <gds.h>

#include <config.h>

/* Global variables */
int nprocs;
int myrank;

int asserts_n;
int asserts_ok;

#define ABS(_a_) ((_a_) < 0 ? -(_a_) : (_a_)) 
#define MAX(_a_, _b_) ((_a_) < _b_ ? (_b_) : (_a_))

#define ASSERT_STAT(a, b) ASSERT_STAT_(a, b, __func__, __FILE__, __LINE__)
#define ASSERT_STAT_FATAL(a, b) \
    ASSERT_STAT_FATAL_(a, b, __func__, __FILE__, __LINE__)
#define ASSERT_STAT_INVALID(a, b) \
    ASSERT_STAT_INVALID_(a, b, __func__, __FILE__, __LINE__)
#define ASSERT_NOT_NULL_FATAL(a) \
    ASSERT_NOT_NULL_FATAL_(a, __func__, __FILE__, __LINE__)
#define ASSERT_EQUAL_INT(a, b) \
    ASSERT_EQUAL_INT_(a, b, __func__, __FILE__, __LINE__)
#define ASSERT_NOT_EQUAL_INT(a, b) \
    ASSERT_NOT_EQUAL_INT_(a, b, __func__, __FILE__, __LINE__)
#define ASSERT_EQUAL_DOUBLE(a, b, c) \
    ASSERT_EQUAL_DOUBLE_(a, b, c, __func__, __FILE__, __LINE__)
#define ASSERT_EQUAL_PTR(a, b) \
    ASSERT_EQUAL_PTR_(a, b, __func__, __FILE__, __LINE__)
#define ASSERT_NOT_EQUAL_PTR(a, b) \
    ASSERT_NOT_EQUAL_PTR_(a, b, __func__, __FILE__, __LINE__)
#define ASSERT_EQUAL_STR(a, b, c) \
    ASSERT_EQUAL_STR_(a, b, c, __func__, __FILE__, __LINE__)
#define ASSERT_EQUAL_SIZE_ARR(a, b, c) \
    ASSERT_EQUAL_SIZE_ARR_(a, b, c, __func__, __FILE__, __LINE__)
#define ASSERT_COND(a) \
    ASSERT_COND_(a, __func__, __FILE__, __LINE__)
#define ASSERT_COND_FATAL(a) \
    ASSERT_COND_FATAL_(a, __func__, __FILE__, __LINE__)

/* Assert routines */
void ASSERT_INIT(void);
int ASSERT_REPORT(void);

void ASSERT_TEST_START(const char *test);
void ASSERT_TEST_START_COMM(const char *test, MPI_Comm comm);
void ASSERT_TEST_GOING(void);
void ASSERT_TEST_END(void);
void ASSERT_TEST_END_COMM(MPI_Comm comm);

void ASSERT_STAT_(const char *func, int stat, 
    const char *infunc, const char *infile, int lineno);
void ASSERT_STAT_FATAL_(const char *func, int stat,
    const char *infunc, const char *infile, int lineno);
void ASSERT_STAT_INVALID_(const char *func, int stat,
    const char *infunc, const char *infile, int lineno);
void ASSERT_NOT_NULL_FATAL_(const void *pointer,
    const char *infunc, const char *infile, int lineno);

void ASSERT_EQUAL_INT_(int a, int b,
    const char *infunc, const char *infile, int lineno);
void ASSERT_NOT_EQUAL_INT_(int a, int b,
    const char *infunc, const char *infile, int lineno);
void ASSERT_EQUAL_DOUBLE_(double a, double b, double epsilon,
    const char *infunc, const char *infile, int lineno);
void ASSERT_NOT_EQUAL_DOUBLE_(double a, double b, double epsilon,
    const char *infunc, const char *infile, int lineno);
void ASSERT_EQUAL_PTR_(void *a, void *b,
    const char *infunc, const char *infile, int lineno);
void ASSERT_NOT_EQUAL_PTR_(void *a, void *b,
    const char *infunc, const char *infile, int lineno);
void ASSERT_NOT_EQUAL_PTR_(void *a, void *b,
    const char *infunc, const char *infile, int lineno);
void ASSERT_NOT_EQUAL_PTR_(void *a, void *b,
    const char *infunc, const char *infile, int lineno);
void ASSERT_EQUAL_STR_(const char *a, const char *b, const size_t len,
    const char *infunc, const char *infile, int lineno);
void ASSERT_EQUAL_SIZE_ARR_(size_t *a, size_t *b, const size_t len,
    const char *infunc, const char *infile, int lineno);

void ASSERT_COND_(int cond,
    const char *infunc, const char *infile, int lineno);
void ASSERT_COND_FATAL_(int cond,
    const char *infunc, const char *infile, int lineno);

/* Debug print */

extern int dprint_level;

enum {
    DPRINT_ERR,
    DPRINT_WARN,
    DPRINT_INFO,
};

#define dbg_printf(_level, _fmt, ...)                                      \
    do {                                                                \
        if (_level <= dprint_level)                                     \
            fprintf(stderr, "%s:%s:%d: " _fmt, __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
    } while (0)

#define pr_err(_fmt, ...)                                 \
    do {                                                  \
        dbg_printf(DPRINT_ERR, _fmt, ##__VA_ARGS__);      \
    } while (0)

#define pr_warn(_fmt, ...)                                 \
    do {                                                   \
        dbg_printf(DPRINT_WARN, _fmt, ##__VA_ARGS__);      \
    } while (0)

#define pr_info(_fmt, ...)                                 \
    do {                                                   \
        dbg_printf(DPRINT_INFO, _fmt, ##__VA_ARGS__);      \
    } while (0)

#define dprintmatrix(_level, _rows, _cols, _buf, _fmt, ...)             \
    do {                                                                \
        if (_level <= dprint_level) {                                   \
            fprintf(stderr, "%s:%s:%d:Matrix: " _fmt "\n", __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
            int _i, _j;                                                 \
            for (_j = 0; _j < _cols; ++_j)                              \
                fprintf(stderr, "------");                              \
            fprintf(stderr, "\n");                                      \
            for (_i = 0; _i < _rows; ++_i) {                            \
                for (_j = 0; _j < _cols; ++_j)                          \
                    fprintf(stderr, "%5.2f ", _buf[_j * _rows + _i]);   \
                fprintf(stderr, "\n");                                  \
            }                                                           \
            for (_j = 0; _j < _cols; ++_j)                              \
                fprintf(stderr, "------");                              \
            fprintf(stderr, "\n\n");                                    \
        }                                                               \
    } while (0)

GDS_size_t GDS_version(GDS_gds_t gds);
#endif /* __COMMON_H__ */
