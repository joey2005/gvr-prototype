/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdio.h>
#include <unistd.h>

#include <gds.h>

#include "common.h"
#define FAILED_PROC 1

int dprint_level = 0;

static GDS_status_t handle_proc_failure(GDS_gds_t gds, GDS_error_t desc)
{
    GDS_comm_t comm_buff;
    MPI_Group group_buff, world_group, compare_group;
    int failed_proc = FAILED_PROC;
    int flag;
    int size;
    int64_t n_ranges;
    GDS_size_t count, offset;

    printf("[rank %d] handle_proc_failure() called\n", myrank);

    GDS_get_error_attr(desc, GDS_EATTR_LOST_COMMUNICATOR, &comm_buff, &flag);
    ASSERT_COND(flag);
    GDS_comm_size(comm_buff, &size);
    ASSERT_EQUAL_INT(size, nprocs);

    GDS_get_comm(gds, &comm_buff);
    GDS_comm_size(comm_buff, &size);
    ASSERT_EQUAL_INT(size, (myrank == FAILED_PROC) ? 1 : nprocs - 1);

    GDS_get_error_attr(desc, GDS_EATTR_LOST_PROCESSES,
        &group_buff, &flag);
    ASSERT_COND(flag);

    MPI_Comm_group(MPI_COMM_WORLD, &world_group);
    MPI_Group_incl(world_group, 1, &failed_proc, &compare_group);
    MPI_Group_compare(group_buff, compare_group, &flag);
    ASSERT_EQUAL_INT(flag, MPI_IDENT);
    MPI_Group_free(&world_group);
    MPI_Group_free(&compare_group);

    GDS_get_error_attr(desc, GDS_EATTR_MEMORY_N_RANGES,
        &n_ranges, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(n_ranges, 1);

    GDS_get_error_attr(desc, GDS_EATTR_GDS_INDEX,
        &offset, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(offset, FAILED_PROC);

    GDS_get_error_attr(desc, GDS_EATTR_GDS_COUNT,
        &count, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(count, 1);

    GDS_resume_global(gds, desc);

    return GDS_STATUS_OK;
}

void basic_test(void)
{
    pr_info("begin\n");

    int putval = 1 + myrank;
    int getval;
    int put_partner = (myrank + 1) % nprocs;
    int get_partner = (myrank + 2) % nprocs;
    int expect = (myrank + 1) % nprocs + 1;

    GDS_gds_t gds = NULL, gds1 = NULL;
    GDS_size_t cts[] = {nprocs};
    GDS_size_t min_chunk[] = {1};
    int ndim = 1;
    GDS_size_t put_lo_ind[] = {put_partner}, put_hi_ind[] = {put_partner};
    GDS_size_t get_lo_ind[] = {get_partner}, get_hi_ind[] = {get_partner};
    GDS_size_t ld[] = {0};
    GDS_error_pred_t pred;
    GDS_error_pred_term_t term;

    ASSERT_TEST_START("AMR");

    GDS_alloc(ndim,cts,min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, MPI_INFO_NULL, &gds);
    GDS_alloc(ndim,cts,min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, MPI_INFO_NULL, &gds1);

    ASSERT_STAT_FATAL("GDS_create_error_pred",
        GDS_create_error_pred(&pred));

    ASSERT_STAT_FATAL("GDS_create_error_pred_term",
        GDS_create_error_pred_term(GDS_EATTR_LOST_PROCESSES,
            GDS_EMEXP_ANY, 0, NULL,
            &term));
    ASSERT_STAT_FATAL("GDS_add_error_pred_term",
        GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT_FATAL("GDS_create_error_pred_term",
        GDS_create_error_pred_term(GDS_EATTR_LOST_COMMUNICATOR,
            GDS_EMEXP_ANY, 0, NULL,
            &term));
    ASSERT_STAT_FATAL("GDS_add_error_pred_term",
        GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    GDS_register_global_error_handler(gds, pred, handle_proc_failure);
    GDS_free_error_pred(&pred);

    printf("[rank %d] gds[%zu]<-%d\n", myrank, put_lo_ind[0], putval);
    GDS_put(&putval, ld, put_lo_ind, put_hi_ind, gds);

    if (myrank == FAILED_PROC)
        printf("Simulating failure for rank %d...\n", FAILED_PROC);

    GDS_simulate_proc_failure(FAILED_PROC);

    printf("[rank %d] After injecting failure\n", myrank);

    GDS_wait(gds);
    GDS_comm_t comm;
    int comm_size;
    ASSERT_STAT_FATAL("GDS_get_comm", GDS_get_comm(gds, &comm));
    GDS_comm_size(comm, &comm_size);
    ASSERT_EQUAL_INT(comm_size, (myrank == FAILED_PROC) ? 1 : nprocs - 1);
    ASSERT_STAT("GDS_get", GDS_get(&getval, ld, get_lo_ind, get_hi_ind, gds));
    GDS_wait_local(gds);
    printf("[rank %d] gds[%zu]->%d\n", myrank, get_lo_ind[0], getval);
    if (myrank != FAILED_PROC && get_lo_ind[0] != FAILED_PROC) {
        ASSERT_EQUAL_INT(getval, expect);
        if (getval != expect)
            printf("rank %d: 0: got=%d expected=%d\n", myrank, getval, expect);
    }
    putval += nprocs;
    if ((myrank + 1) % nprocs != FAILED_PROC)
        expect += nprocs;
    printf("[rank %d] Before version inc\n", myrank);
    GDS_version_inc(gds, 1, "", 0);
    printf("[rank %d] After version inc\n", myrank);
    printf("[rank %d] gds[%zu]<-%d\n", myrank, put_lo_ind[0], putval);
    GDS_put(&putval, ld, put_lo_ind, put_hi_ind, gds);
    GDS_fence(gds);
    GDS_get(&getval, ld, get_lo_ind, get_hi_ind, gds);
    GDS_fence(gds1);
    GDS_wait_local(gds);
    printf("[rank %d] gds[%zu]->%d\n", myrank, get_lo_ind[0], getval);
    if (myrank != FAILED_PROC) {
        ASSERT_EQUAL_INT(getval, expect);
        if (getval != expect)
            printf("rank %d: 1: got=%d expected=%d\n", myrank, getval, expect);
    }

    printf("[rank %d] All tests done, freeing arrays\n", myrank);

    GDS_free(&gds);
    GDS_free(&gds1);

    printf("[rank %d] Arrays freed\n", myrank);

    /* Comment out because this requires a collective call on MPI_COMM_WORLD */
    // ASSERT_TEST_END();
}


int main(int argc, char **argv)
{
    int ret = 0;
    GDS_thread_support_t provd_support;

    ASSERT_INIT();

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support);

    GDS_comm_size(GDS_COMM_WORLD, &nprocs);
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    basic_test();

    /* Comment out because this requires a collective call on MPI_COMM_WORLD */
    // ASSERT_REPORT();

    GDS_finalize();
    printf("[rank %d] Exiting\n", myrank);
    return ret;
}
