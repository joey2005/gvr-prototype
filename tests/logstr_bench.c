/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>

#include <gds.h>

static int my_rank;
static int n_procs;
static bool skip_gds_free = false;

#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define RWRATIO_TOTAL 4
#define BLOCKSIZE 32

struct param {
    size_t start;
    size_t access_size;

    /* significant_rank < 0 means it does not specify any ranks */
    int significant_rank;

    int read_ratio; /* 0-10; <0 means unspecified  */
};

struct benchmark {
    const char *desc;
    int width_min;
    int width_max;
    bool asymmetric;
    bool do_retry; /* Retry measurement for cache-hit case? */
    void (*init)(GDS_gds_t gds, size_t count_each,
        int *buff, const struct param *param);
    void (*run)(GDS_gds_t gds, size_t count_each,
        int *buff, const struct param *param);
    size_t (*transfer_bytes)(size_t count_each, size_t width);
};

static void neighbor_seq_w_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param);

static size_t one_way_transfer_bytes(size_t c, size_t width)
{
    if (width < BLOCKSIZE)
        return sizeof(int) * c / (BLOCKSIZE / width);
    else
        return sizeof(int) * c;
}

static void local_seq_w_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    size_t width = param->access_size;
    size_t start = my_rank * count_each;
    size_t s, step = MAX(width, BLOCKSIZE);
    size_t ld[0] = {};

    if (param->significant_rank >= 0 && param->significant_rank != my_rank)
        return;

    for (s = start; s + width <= start + count_each; s += step) {
        size_t lo[1] = { s };
        size_t hi[1] = { s + width - 1 };
        GDS_put(buff, ld, lo, hi, gds);
    }

    GDS_wait(gds);
}

static struct benchmark local_seq_w = {
    .desc = "local_seq_w",
    .run = local_seq_w_run,
    .do_retry = true,
    .transfer_bytes = one_way_transfer_bytes,
};

static struct benchmark local_seq_ow = {
    .desc = "local_seq_ow",
    .init = neighbor_seq_w_run,
    .run = local_seq_w_run,
    .transfer_bytes = one_way_transfer_bytes,
};

static void local_seq_r_init(GDS_gds_t gds, size_t count_each, int *buff,
    const struct param *param)
{
    local_seq_w_run(gds, count_each, buff, param);
}

static void local_seq_r_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    size_t width = param->access_size;
    size_t start = my_rank * count_each;
    size_t s, step = MAX(width, BLOCKSIZE);
    size_t ld[0] = {};
    size_t loff = 0;

    if (param->significant_rank >= 0 && param->significant_rank != my_rank)
        return;

    for (s = start; s + width <= start + count_each; s += step) {
        size_t lo[1] = { s };
        size_t hi[1] = { s + width - 1 };
        GDS_get(buff + loff, ld, lo, hi, gds);
        loff += width;
    }

    GDS_wait(gds);
}

static struct benchmark local_seq_r = {
    .desc = "local_seq_r",
    .init = neighbor_seq_w_run,
    .run = local_seq_r_run,
    .do_retry = true,
    .transfer_bytes = one_way_transfer_bytes,
};

static void neighbor_seq_w_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    int neigh = (my_rank + 1) % n_procs;
    size_t width = param->access_size;
    size_t start = neigh * count_each;
    size_t s, step = MAX(width, BLOCKSIZE);
    size_t ld[0] = {};
    size_t loff = 0;

    if (param->significant_rank >= 0 && param->significant_rank != my_rank)
        return;

    for (s = start; s + width <= start + count_each; s += step) {
        size_t lo[1] = { s };
        size_t hi[1] = { s + width - 1 };
        GDS_put(buff + loff, ld, lo, hi, gds);
        loff += width;
    }

    GDS_wait(gds);
}

static struct benchmark neighbor_seq_w = {
    .desc = "neighbor_seq_w",
    .run = neighbor_seq_w_run,
    .do_retry = true,
    .transfer_bytes = one_way_transfer_bytes,
};

static struct benchmark bw_warmup = {
    .desc = "bw_warmup",
    .run = neighbor_seq_w_run,
    .do_retry = true,
    .transfer_bytes = one_way_transfer_bytes,
};

static void neighbor_seq_a_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    int neigh = (my_rank + 1) % n_procs;
    size_t width = param->access_size;
    size_t start = neigh * count_each;
    size_t s, step = MAX(width, BLOCKSIZE);
    size_t ld[0] = {};
    size_t loff = 0;

    if (param->significant_rank >= 0 && param->significant_rank != my_rank)
        return;

    for (s = start; s + width <= start + count_each; s += step) {
        size_t lo[1] = { s };
        size_t hi[1] = { s + width - 1 };
        GDS_acc(buff + loff, ld, lo, hi, MPI_SUM, gds);
        loff += width;
    }

    GDS_wait(gds);
}

static struct benchmark neighbor_seq_a = {
    .desc = "neighbor_seq_a",
    .run = neighbor_seq_a_run,
    .do_retry = true,
    .transfer_bytes = one_way_transfer_bytes,
    .width_min = 1,
    .width_max = 262144,
};

static struct benchmark neighbor_seq_ow = {
    .desc = "neighbor_seq_ow",
    .init = local_seq_w_run,
    .run = neighbor_seq_w_run,
    .transfer_bytes = one_way_transfer_bytes,
};

static void neighbor_seq_r_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    int r = (my_rank + 1) % n_procs;
    size_t width = param->access_size;
    size_t start = r * count_each;
    size_t s, step = MAX(width, BLOCKSIZE);
    size_t ld[0] = {};
    size_t loff = 0;

    if (param->significant_rank >= 0 && param->significant_rank != my_rank)
        return;

    for (s = start; s + width <= start + count_each; s += step) {
        size_t lo[1] = { s };
        size_t hi[1] = { s + width - 1 };
        GDS_get(buff + loff, ld, lo, hi, gds);
        loff += width;
    }

    GDS_wait(gds);
}

static struct benchmark neighbor_seq_r = {
    .desc = "neighbor_seq_r",
    .init = local_seq_r_init,
    .run = neighbor_seq_r_run,
    .do_retry = true,
    .transfer_bytes = one_way_transfer_bytes,
};

static void local_init(GDS_gds_t gds, size_t count_each, int *buff,
    const struct param *param)
{
    size_t written = 0;
    size_t start = my_rank * count_each;
    size_t ld[0] = {};

    while (written < count_each) {
        size_t to_copy = param->access_size;
        size_t i;

        if (written + to_copy > count_each)
            to_copy = count_each - written;

        for (i = 0; i < param->access_size; i++)
            buff[i] = i + written;

        size_t lo[1] = { start + written };
        size_t hi[1] = { start + written + to_copy - 1 };

        GDS_put(buff, ld, lo, hi, gds);
        written += to_copy;
    }

    GDS_fence(gds);
}

typedef void (*gds_op_fun_t)(void *buff, size_t ld[],
    size_t lo[], size_t hi[], GDS_gds_t gds);

static void get_op(void *buff, size_t ld[], size_t lo[], size_t hi[], GDS_gds_t gds)
{
    GDS_get(buff, ld, lo, hi, gds);
}

static void put_op(void *buff, size_t ld[], size_t lo[], size_t hi[], GDS_gds_t gds)
{
    GDS_put(buff, ld, lo, hi, gds);
}

static void ms_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param, gds_op_fun_t gds_op)
{
    size_t width = param->access_size;
    size_t ce = count_each / n_procs;
    size_t step = MAX(width, BLOCKSIZE);
    size_t ld[0] = {};
    int t;

    if (my_rank != param->significant_rank)
        return;

    for (t = 0; t < n_procs; t++) {
        size_t s, start;

        if (t == param->significant_rank)
            continue;

        start = t * count_each;
        for (s = start; s + width <= start + ce; s += step) {
            size_t lo[1] = { s };
            size_t hi[1] = { s + width - 1 };
            size_t loff;

            loff = t * ce + (s - start);
            gds_op(buff + loff, ld, lo, hi, gds);
        }
    }

    GDS_wait(gds);
}

static size_t ms_transfer_bytes(size_t c, size_t width)
{
    /* TODO: this comparison should be made with param->significant_rank,
       but assuming that every process participates in the communication,
       this is valid. */
    if (my_rank == 0) {
        if (width < BLOCKSIZE)
            return sizeof(int) * c * (n_procs - 1) / (BLOCKSIZE / width);
        else
            return sizeof(int) * c * (n_procs - 1);
    } else
        return 0;
}

static void ms_get_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    ms_run(gds, count_each, buff, param, get_op);
}

/* Multi-server get */
static struct benchmark ms_r = {
    .desc = "ms_r",
    .init = local_init,
    .run = ms_get_run,
    .transfer_bytes = ms_transfer_bytes,
    .asymmetric = true,
    .do_retry = true,
};

static void ms_put_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    ms_run(gds, count_each, buff, param, put_op);
}

/* Multi-server put */
static struct benchmark ms_w = {
    .desc = "ms_w",
    .run = ms_put_run,
    .transfer_bytes = ms_transfer_bytes,
    .asymmetric = true,
    .do_retry = true,
};

static struct benchmark ms_ow = {
    .desc = "ms_ow",
    .init = local_seq_w_run,
    .run = ms_put_run,
    .transfer_bytes = ms_transfer_bytes,
    .asymmetric = true,
};

static void mc_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param, gds_op_fun_t gds_op)
{
    size_t width = param->access_size;
    size_t ce = count_each / n_procs;
    size_t ld[0] = {};
    size_t s, start, step = MAX(width, BLOCKSIZE);

    if (my_rank == param->significant_rank)
        return;

    start = param->significant_rank * count_each + ce * my_rank;
    for (s = start; s + width <= start + ce; s += step) {
        size_t lo[1] = { s };
        size_t hi[1] = { s + width - 1 };
        size_t loff;

        loff = param->significant_rank * ce + (s - start);
        gds_op(buff + loff, ld, lo, hi, gds);
    }

    GDS_wait(gds);
}

static size_t mc_transfer_bytes(size_t c, size_t width)
{
    /* TODO: this comparison should be made with param->significant_rank,
       but assuming that every process participates in the communication,
       this is valid. */
    if (my_rank == 0)
        return 0;

    if (width < BLOCKSIZE)
        return sizeof(int) * c / n_procs / (BLOCKSIZE / width);
    else
        return sizeof(int) * c / n_procs;
}

static void mc_put_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    mc_run(gds, count_each, buff, param, put_op);
}

/* Multi-client put */
static struct benchmark mc_w = {
    .desc = "mc_w",
    .run = mc_put_run,
    .transfer_bytes = mc_transfer_bytes,
    .asymmetric = true,
    .do_retry = true,
};

static struct benchmark mc_ow = {
    .desc = "mc_ow",
    .init = local_seq_w_run,
    .run = mc_put_run,
    .transfer_bytes = mc_transfer_bytes,
    .asymmetric = true,
};

static void mc_get_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    mc_run(gds, count_each, buff, param, get_op);
}

/* Multi-client get */
static struct benchmark mc_r = {
    .desc = "mc_r",
    .init = local_init,
    .run = mc_get_run,
    .transfer_bytes = mc_transfer_bytes,
    .asymmetric = true,
    .do_retry = true,
};

/* Random walk -- algorithm taken from LRDS bench_randomwalk.c */
static void random_init(GDS_gds_t gds, size_t count_each, int *buff,
    const struct param *param)
{
    size_t written = 0;
    int i;

    size_t start = my_rank * count_each;
    size_t ld[0] = {};

    while (written < count_each) {
        size_t to_copy = param->access_size;
        if (written + to_copy > count_each)
            to_copy = count_each - written;

        for (i = 0; i < param->access_size; i++)
            buff[i] = i + written;

        size_t lo[1] = { start + written };
        size_t hi[1] = { start + written + to_copy - 1 };

        GDS_put(buff, ld, lo, hi, gds);
        written += to_copy;
    }
    GDS_version_inc(gds, 1, NULL, 0);
}

#define LCG_MUL64 6364136223846793005ULL
#define LCG_ADD64 1
#define RANDOM_UPD_FRAC 4

static int random_nupdates(size_t count_each)
{
    return count_each / 2 / RANDOM_UPD_FRAC;
}

static void random_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    size_t ld[0] = {};
    uint64_t rn = my_rank + 1;
    int i = 0, nupdate = random_nupdates(count_each);
    int naccess = nupdate * 2;
    int r_cycle, w_cycle, rc, wc;
    size_t range = count_each * n_procs;
    uint64_t v;

    if (param->read_ratio < 0) {
        r_cycle = w_cycle = RWRATIO_TOTAL / 2;
    } else {
        r_cycle = param->read_ratio;
        w_cycle = RWRATIO_TOTAL - r_cycle;
    }

    rc = r_cycle;
    wc = w_cycle;

    while (i < naccess) {
        rn = LCG_MUL64 * rn + LCG_ADD64;
        size_t idx = (rn % (range / 2)) * 2;
        size_t lo[1] = { idx }, hi[1] = { idx + 1 };
        if (rc > 0) {
            GDS_get(&v, ld, lo, hi, gds);
            rc--;
            i++;
        }
        GDS_wait(gds);
        v ^= rn;
        if (wc > 0) {
            GDS_put(&v, ld, lo, hi, gds);
            wc--;
            i++;
        }
        if (rc == 0 && wc == 0) {
            rc = r_cycle;
            wc = w_cycle;
        }
    }
    GDS_wait(gds);
}

static size_t random_transfer_bytes(size_t c, size_t width)
{
    /* Intentionally ignore `width` */
    return sizeof(int) * 2 /* access width / rw */
        * 2 /* R+W */
        * random_nupdates(c);
}

static struct benchmark rnd = {
    .desc = "random",
    .width_min = 2,
    .width_max = 2,
    .init = random_init,
    .run = random_run,
    .transfer_bytes = random_transfer_bytes,
    .asymmetric = true,
};

#define MAXWIDTH (1024*2)

#define MIN_MSGS 8

#define N_TRY 2

static void print_banner(const char *msg)
{
    if (my_rank == 0)
        puts(msg);
}

const char *get_impl_str_local(int i)
{
    switch (i) {
    case GDS_LOG_GET_IMPL_RMA:
        return "rma";
    case GDS_LOG_GET_IMPL_MSG:
        return "msg";
    default:
        return "???";
    }
}

const char *put_impl_str_local(int i)
{
    switch (i) {
    case GDS_LOG_PUT_IMPL_RMA:
        return "rma";
    case GDS_LOG_PUT_IMPL_MSG:
        return "msg";
    case GDS_LOG_PUT_IMPL_HYB:
        return "hyb";
    default:
        return "???";
    }
}

struct array_flavor {
    const char *repr;
    int log_bs;
    int log_get_impl;
    int log_put_impl;
};

static MPI_Info info_setup(const struct array_flavor *flavor)
{
    MPI_Info info;

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_REPR_KEY, flavor->repr);
    if (strcmp(flavor->repr, GDS_REPR_LOG) == 0) {
        GDS_info_set_int(info, GDS_LOG_BS_KEY, flavor->log_bs);
        GDS_info_set_int(info, GDS_LOG_PREALLOC_VERS_KEY, 3);
        GDS_info_set_int(info, GDS_LOG_GET_IMPL_KEY, flavor->log_get_impl);
        GDS_info_set_int(info, GDS_LOG_PUT_IMPL_KEY, flavor->log_put_impl);
    } else if (strcmp(flavor->repr, GDS_REPR_FLAT) == 0) {
        GDS_info_set_int(info, GDS_FLAT_GET_IMPL_KEY, flavor->log_get_impl);
        GDS_info_set_int(info, GDS_FLAT_PUT_IMPL_KEY, flavor->log_put_impl);
    }

    return info;
}

static void run_bwbench(const struct array_flavor *flavor,
    const struct benchmark *benchmark)
{
    size_t width, wmin, wmax;
    int *buff = NULL, i;
    struct param param;
    MPI_Info info;
    char array_type_str[24];
    size_t count_each = MAXWIDTH;

    info = info_setup(flavor);

    if (strcmp(flavor->repr, GDS_REPR_LOG) == 0) {
        snprintf(array_type_str, sizeof(array_type_str),
            "%s-bs%zu-g%s-p%s", flavor->repr, flavor->log_bs * sizeof(int),
            get_impl_str_local(flavor->log_get_impl),
            put_impl_str_local(flavor->log_put_impl));
    } else
        snprintf(array_type_str, sizeof(array_type_str),
            "%s-g%s-p%s", flavor->repr,
            get_impl_str_local(flavor->log_get_impl),
            put_impl_str_local(flavor->log_put_impl));

    wmin = benchmark->width_min;
    wmax = benchmark->width_max;
    if (wmin == 0 && wmax == 0) {
        wmin = 1;
        wmax = MAXWIDTH;
    }

    print_banner("raw/avg, array type, test type, access width (B), time (s), bandwidth (KB/s), #procs");

    for (width = wmin; width <= wmax; width *= 2) {
        double td_sum = 0.0, bw_sum = 0.0, bw_agg_sum = 0.0;
        double td_sum_rt = 0.0, bw_sum_rt = 0.0, bw_agg_sum_rt = 0.0;
        int try_max = N_TRY;

        if (count_each < width * MIN_MSGS)
            count_each = width * MIN_MSGS;

        if (benchmark->asymmetric && width * MIN_MSGS > count_each / n_procs)
            count_each = width * n_procs * MIN_MSGS;

        buff = realloc(buff, count_each * sizeof(int));
        assert(buff);
        memset(buff, 0, count_each * sizeof(int));

        if (benchmark->asymmetric) {
            try_max = n_procs;
            if (try_max <= 2)
                try_max *= 2;
        }

        for (i = 0; i < try_max; i++) {
            GDS_status_t s;
            GDS_gds_t gds;
            size_t count[1] = { count_each * n_procs };
            double t0, t1, td, td_max, bw, ds, ds_agg;

            s = GDS_alloc(1, count, NULL, GDS_DATA_INT, GDS_PRIORITY_HIGH,
                GDS_COMM_WORLD, info, &gds);
            assert(s == GDS_STATUS_OK);

            if (benchmark->init) {
                param.access_size = wmax; /* for initialize */
                param.significant_rank = -1;
                benchmark->init(gds, count_each, buff, &param);
            }
            param.read_ratio = -1;
            GDS_counter_reset(gds);

            param.significant_rank = i % n_procs;
            param.access_size = width;

            MPI_Barrier(MPI_COMM_WORLD);

            t0 = MPI_Wtime();
            benchmark->run(gds, count_each, buff, &param);
            t1 = MPI_Wtime();
            td = t1 - t0;
            ds = benchmark->transfer_bytes(count_each, width);
            MPI_Reduce(&ds, &ds_agg, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
            MPI_Reduce(&td, &td_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

            if (benchmark->asymmetric) {
                if (my_rank == 0)
                    printf("raw, %s, %s, %zu, %f, %.3f, %d\n",
                        array_type_str,
                        benchmark->desc, width * sizeof(int), td_max,
                        ds_agg / 1000.0 / td_max,
                        n_procs);
            } else {
                if (my_rank == param.significant_rank) {
                    bw = ds / 1000.0 / td;
                    printf("raw, %s, %s, %zu, %f, %.3f, %d\n",
                        array_type_str,
                        benchmark->desc, width * sizeof(int), td, bw, n_procs);
                    if (my_rank != 0)
                        MPI_Send(&bw, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
                } else if (my_rank == 0)
                    MPI_Recv(&bw, 1, MPI_DOUBLE, param.significant_rank, 0,
                        MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                bw_sum += bw;
            }

            td_sum += td_max;
            bw_agg_sum += ds_agg / 1000.0 / td_max;

            if (!benchmark->do_retry)
                goto cont_free;

            MPI_Barrier(MPI_COMM_WORLD);

            t0 = MPI_Wtime();
            benchmark->run(gds, count_each, buff, &param);
            t1 = MPI_Wtime();
            td = t1 - t0;
            ds = benchmark->transfer_bytes(count_each, width);
            MPI_Reduce(&ds, &ds_agg, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
            MPI_Reduce(&td, &td_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

            if (benchmark->asymmetric) {
                if (my_rank == 0)
                    printf("raw, %s, %s_rt, %zu, %f, %.3f, %d\n",
                        array_type_str,
                        benchmark->desc, width * sizeof(int), td_max,
                        ds_agg / 1000.0 / td_max,
                        n_procs);
            } else {
                bw = 0.0;
                if (my_rank == param.significant_rank) {
                    bw = ds / 1000.0 / td;
                    printf("raw, %s, %s_rt, %zu, %f, %.3f, %d\n",
                        array_type_str,
                        benchmark->desc, width * sizeof(int), td, bw, n_procs);
                    if (my_rank != 0)
                        MPI_Send(&bw, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
                } else if (my_rank == 0)
                    MPI_Recv(&bw, 1, MPI_DOUBLE, param.significant_rank, 0,
                        MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                bw_sum_rt += bw;
            }

            td_sum_rt += td_max;
            bw_agg_sum_rt += ds_agg / 1000.0 / td_max;

        cont_free:
            if (!skip_gds_free)
                GDS_free(&gds);
        }

        if (my_rank == 0) {
            double td_avg, td_avg_rt;
            double bw_avg, bw_avg_rt;

            if (benchmark->asymmetric) {
                td_avg = td_sum / try_max;
                bw_avg = bw_agg_sum / try_max;
                td_avg_rt = td_sum_rt / try_max;
                bw_avg_rt = bw_agg_sum_rt / try_max;
            } else {
                td_avg = td_sum / try_max;
                bw_avg = bw_sum / try_max;
                td_avg_rt = td_sum_rt / try_max;
                bw_avg_rt = bw_sum_rt / try_max;
            }
            printf("avg, %s, %s, %zu, %f, %.3f, %d\n",
                array_type_str,
                benchmark->desc, width * sizeof(int), td_avg, bw_avg, n_procs);
            if (benchmark->do_retry)
                printf("avg, %s, %s_rt, %zu, %f, %.3f, %d\n",
                    array_type_str,
                    benchmark->desc, width * sizeof(int),
                    td_avg_rt, bw_avg_rt, n_procs);
        }
    }

    MPI_Info_free(&info);

    free(buff);
}

static void run_rndbench(const struct array_flavor *flavor,
    const struct benchmark *benchmark)
{
    size_t width, wmin, wmax;
    int *buff = NULL, i, rr;
    struct param param;
    MPI_Info info;
    char array_type_str[24];
    size_t count_each = MAXWIDTH;

    info = info_setup(flavor);

    if (strcmp(flavor->repr, GDS_REPR_LOG) == 0) {
        snprintf(array_type_str, sizeof(array_type_str),
            "%s-bs%zu-g%s-p%s", flavor->repr, flavor->log_bs * sizeof(int),
            get_impl_str_local(flavor->log_get_impl),
            put_impl_str_local(flavor->log_put_impl));
    } else
        snprintf(array_type_str, sizeof(array_type_str),
            "%s-g%s-p%s", flavor->repr,
            get_impl_str_local(flavor->log_get_impl),
            put_impl_str_local(flavor->log_put_impl));

    wmin = benchmark->width_min;
    wmax = benchmark->width_max;
    if (wmin == 0 && wmax == 0) {
        wmin = 1;
        wmax = MAXWIDTH;
    }

    print_banner("raw/avg, array type, test type, access width (B), read ratio (%), time (s), access speed (/s), #procs");

    for (width = wmin; width <= wmax; width *= 2) {
        int try_max = N_TRY;

        if (count_each < width * MIN_MSGS)
            count_each = width * MIN_MSGS;

        if (benchmark->asymmetric && width * MIN_MSGS > count_each / n_procs)
            count_each = width * n_procs * MIN_MSGS;

        buff = realloc(buff, count_each * sizeof(int));
        assert(buff);
        memset(buff, 0, count_each * sizeof(int));

        try_max = n_procs;
        if (try_max <= 2)
            try_max *= 2;

        for (rr = 0; rr <= RWRATIO_TOTAL; rr++) {
            double td_sum = 0.0, acc_agg_sum = 0.0;
            for (i = 0; i < try_max; i++) {
                GDS_status_t s;
                GDS_gds_t gds;
                size_t count[1] = { count_each * n_procs };
                double t0, t1, td, td_max, acc_agg;

                s = GDS_alloc(1, count, NULL, GDS_DATA_INT, GDS_PRIORITY_HIGH,
                    GDS_COMM_WORLD, info, &gds);
                assert(s == GDS_STATUS_OK);

                if (benchmark->init) {
                    param.access_size = wmax; /* for initialize */
                    param.significant_rank = -1;
                    benchmark->init(gds, count_each, buff, &param);
                }
                param.read_ratio = rr;
                GDS_counter_reset(gds);

                param.significant_rank = i % n_procs;
                param.access_size = width;

                MPI_Barrier(MPI_COMM_WORLD);

                t0 = MPI_Wtime();
                benchmark->run(gds, count_each, buff, &param);
                t1 = MPI_Wtime();
                td = t1 - t0;
                acc_agg = random_nupdates(count_each) * 2;
                MPI_Reduce(&td, &td_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

                if (my_rank == 0)
                    printf("raw, %s, %s, %zu, %d, %f, %.3f, %d\n",
                        array_type_str,
                        benchmark->desc, width * sizeof(int),
                        rr * 100 / RWRATIO_TOTAL,
                        td_max,
                        acc_agg / td_max,
                        n_procs);

                td_sum += td_max;
                acc_agg_sum += acc_agg / td_max;

                if (!skip_gds_free)
                    GDS_free(&gds);
            }

            if (my_rank == 0) {
                double td_avg;
                double acc_avg;

                td_avg = td_sum / try_max;
                acc_avg = acc_agg_sum / try_max;
                printf("avg, %s, %s, %zu, %d, %f, %.3f, %d\n",
                    array_type_str,
                    benchmark->desc, width * sizeof(int), rr * 100 / RWRATIO_TOTAL, td_avg, acc_avg, n_procs);
            }
        }
    }

    MPI_Info_free(&info);

    free(buff);
}

#define VERINC_COUNT_PER_PROC (1024*1024*512ULL) /* 2GB/proc */
#define N_VERINC 3
#define LOCAL_BUFSIZE (1024*1024)

static void run_verinc(const struct array_flavor *flavor)
{
    MPI_Info info;
    char array_type_str[24];
    int pct = 100, i;
    int *buff;
    size_t cnt_pp; /* count per proc */

    info = info_setup(flavor);
    if (strcmp(flavor->repr, GDS_REPR_LOG) == 0) {
        snprintf(array_type_str, sizeof(array_type_str),
            "%s-bs%zu", flavor->repr, flavor->log_bs * sizeof(int));
    } else
        strncpy(array_type_str, flavor->repr, sizeof(array_type_str));

    buff = calloc(LOCAL_BUFSIZE, sizeof(int));
    assert(buff);

    if (my_rank == 0)
        printf("raw/avg, nprocs, array_type, mod_pct, time\n");

    for (cnt_pp = 1024/sizeof(int); cnt_pp <= VERINC_COUNT_PER_PROC; cnt_pp *= 2) {
        double td_max_sum = 0.0;
        for (i = 0; i < N_VERINC; i++) {
            GDS_status_t s;
            GDS_gds_t gds;
            size_t count[1] = { cnt_pp * n_procs };
            size_t ld[0], lo[1], hi[1], put, to_put_max;
            double t0, t1, td, td_max;

            s = GDS_alloc(1, count, NULL, GDS_DATA_INT, GDS_PRIORITY_HIGH,
                GDS_COMM_WORLD, info, &gds);
            assert(s == GDS_STATUS_OK);

            put = my_rank * cnt_pp;
            to_put_max = put + cnt_pp * pct / 100;

            while (put < to_put_max) {
                size_t to_put = to_put_max - put;
                if (to_put > LOCAL_BUFSIZE)
                    to_put = LOCAL_BUFSIZE;

                lo[0] = put;
                hi[0] = put + to_put - 1;

                GDS_put(buff, ld, lo, hi, gds);

                put += to_put;
            }

            GDS_fence(gds);

            t0 = MPI_Wtime();
            GDS_version_inc(gds, 1, NULL, 0);
            t1 = MPI_Wtime();
            td = t1 - t0;

            MPI_Reduce(&td, &td_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
            if (my_rank == 0) {
                printf("raw, %d, %s, %zu, %f\n", n_procs, array_type_str,
                    cnt_pp * sizeof(int), td_max);
                td_max_sum += td_max;
            }
            if (!skip_gds_free)
                GDS_free(&gds);
        }
        if (my_rank == 0)
            printf("avg, %d, %s, %zu, %f\n", n_procs, array_type_str,
                cnt_pp * sizeof(int), td_max_sum / N_VERINC);
    }

    free(buff);
}

static void run_verinc2(const struct array_flavor *flavor)
{
    MPI_Info info;
    char array_type_str[24];
    int pct, i;
    int *buff;

    info = info_setup(flavor);
    if (strcmp(flavor->repr, GDS_REPR_LOG) == 0) {
        snprintf(array_type_str, sizeof(array_type_str),
            "%s-bs%zu", flavor->repr, flavor->log_bs * sizeof(int));
    } else
        strncpy(array_type_str, flavor->repr, sizeof(array_type_str));

    buff = calloc(LOCAL_BUFSIZE, sizeof(int));
    assert(buff);

    if (my_rank == 0)
        printf("raw/avg, nprocs, array_type, mod_pct, time\n");

    for (pct = 0; pct <= 100; pct += 20) {
        double td_max_sum = 0.0;

        for (i = 0; i < N_VERINC; i++) {
            GDS_status_t s;
            GDS_gds_t gds;
            size_t count[1] = { VERINC_COUNT_PER_PROC * n_procs };
            size_t ld[0], lo[1], hi[1], put, to_put_max;
            double t0, t1, td, td_max;

            s = GDS_alloc(1, count, NULL, GDS_DATA_INT, GDS_PRIORITY_HIGH,
                GDS_COMM_WORLD, info, &gds);
            assert(s == GDS_STATUS_OK);

            put = my_rank * VERINC_COUNT_PER_PROC;
            to_put_max = put + VERINC_COUNT_PER_PROC * pct / 100;

            while (put < to_put_max) {
                size_t to_put = to_put_max - put;
                if (to_put > LOCAL_BUFSIZE)
                    to_put = LOCAL_BUFSIZE;

                lo[0] = put;
                hi[0] = put + to_put - 1;

                GDS_put(buff, ld, lo, hi, gds);

                put += to_put;
            }

            GDS_fence(gds);

            t0 = MPI_Wtime();
            GDS_version_inc(gds, 1, NULL, 0);
            t1 = MPI_Wtime();
            td = t1 - t0;

            MPI_Reduce(&td, &td_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
            if (my_rank == 0) {
                printf("raw, %d, %s, %d, %f\n", n_procs, array_type_str,
                    pct, td_max);
                td_max_sum += td_max;
            }
            if (!skip_gds_free)
                GDS_free(&gds);
        }
        if (my_rank == 0)
            printf("avg, %d, %s, %d, %f\n", n_procs, array_type_str, pct,
                td_max_sum / N_VERINC);

    }

    free(buff);
}

static void lat_local_w_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    size_t width = param->access_size;
    size_t s = my_rank * count_each + param->start;
    size_t ld[0] = {};
    size_t lo[1] = { s };
    size_t hi[1] = { s + width - 1 };

    if (param->significant_rank == my_rank) {
        GDS_put(buff, ld, lo, hi, gds);
        GDS_wait(gds);
    }
}

struct benchmark lat_local_w = {
    .desc = "local_w",
    .do_retry = true,
    .run = lat_local_w_run,
};

struct benchmark lat_local_ow = {
    .desc = "local_ow",
    .init = neighbor_seq_w_run,
    .run = lat_local_w_run,
};

static void lat_local_r_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    size_t width = param->access_size;
    size_t s = my_rank * count_each + param->start;
    size_t ld[0] = {};
    size_t lo[1] = { s };
    size_t hi[1] = { s + width - 1 };

    if (param->significant_rank == my_rank) {
        GDS_get(buff, ld, lo, hi, gds);
        GDS_wait_local(gds);
    }
}

struct benchmark lat_local_r = {
    .desc = "local_r",
    .init = neighbor_seq_w_run,
    .do_retry = true,
    .run = lat_local_r_run,
};

static void lat_neighbor_w_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    size_t width = param->access_size;
    int neigh = (my_rank + 1) % n_procs;
    size_t s = neigh * count_each + param->start;
    size_t ld[0] = {};
    size_t lo[1] = { s };
    size_t hi[1] = { s + width - 1 };

    if (param->significant_rank == my_rank) {
        GDS_put(buff, ld, lo, hi, gds);
        GDS_wait(gds);
    }
}

struct benchmark lat_neighbor_w = {
    .desc = "neighbor_w",
    .do_retry = true,
    .run = lat_neighbor_w_run,
};

struct benchmark lat_neighbor_ow = {
    .desc = "neighbor_ow",
    .init = local_seq_w_run,
    .run = lat_neighbor_w_run,
};

struct benchmark lat_warmup = {
    .desc = "lat_warmup",
    .do_retry = true,
    .run = lat_neighbor_w_run,
};

static void lat_neighbor_a_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    size_t width = param->access_size;
    int neigh = (my_rank + 1) % n_procs;
    size_t s = neigh * count_each + param->start;
    size_t ld[0] = {};
    size_t lo[1] = { s };
    size_t hi[1] = { s + width - 1 };

    if (param->significant_rank == my_rank) {
        GDS_acc(buff, ld, lo, hi, MPI_SUM, gds);
        GDS_wait(gds);
    }
}

struct benchmark lat_neighbor_a = {
    .desc = "neighbor_a",
    .do_retry = true,
    .run = lat_neighbor_a_run,
};

static void lat_neighbor_r_run(GDS_gds_t gds, size_t count_each,
    int *buff, const struct param *param)
{
    size_t width = param->access_size;
    int neigh = (my_rank + 1) % n_procs;
    size_t s = neigh * count_each + param->start;
    size_t ld[0] = {};
    size_t lo[1] = { s };
    size_t hi[1] = { s + width - 1 };

    if (param->significant_rank == my_rank) {
        GDS_get(buff, ld, lo, hi, gds);
        GDS_wait_local(gds);
    }
}

struct benchmark lat_neighbor_r = {
    .desc = "neighbor_r",
    .init = local_seq_w_run,
    .do_retry = true,
    .run = lat_neighbor_r_run,
};

static void run_latbench(const struct array_flavor *flavor,
    const struct benchmark *benchmark)
{
    size_t width, stride, wmin, wmax, count_each;
    MPI_Info info;
    int *buff = NULL;
    char array_type_str[24];
    info = info_setup(flavor);

    if (strcmp(flavor->repr, GDS_REPR_LOG) == 0) {
        snprintf(array_type_str, sizeof(array_type_str),
            "%s-bs%zu-g%s-p%s", flavor->repr, flavor->log_bs * sizeof(int),
            get_impl_str_local(flavor->log_get_impl),
            put_impl_str_local(flavor->log_put_impl));
    } else
        snprintf(array_type_str, sizeof(array_type_str),
            "%s-g%s-p%s", flavor->repr,
            get_impl_str_local(flavor->log_get_impl),
            put_impl_str_local(flavor->log_put_impl));

    wmin = 1;
    wmax = MAXWIDTH;

    buff = malloc(MAXWIDTH * MIN_MSGS * sizeof(int));
    assert(buff);
    memset(buff, 1, MAXWIDTH * MIN_MSGS * sizeof(int));

    for (width = wmin; width <= wmax; width *= 2) {
        GDS_status_t s;
        GDS_gds_t gds;
        double lat_sum = 0.0, lat_sum_rt = 0.0;
        int i, n;

        stride = BLOCKSIZE;
        if (stride < width)
            stride = width;

        count_each = stride * MIN_MSGS;

        size_t count[1] = { count_each * n_procs };

        for (n = 0; n < N_TRY; n++) {
            int srank = n % n_procs;
            s = GDS_alloc(1, count, NULL, GDS_DATA_INT, GDS_PRIORITY_HIGH,
                GDS_COMM_WORLD, info, &gds);
            assert(s == GDS_STATUS_OK);

            memset(buff, 1, count_each * sizeof(int));

            if (benchmark->init) {
                struct param param = {
                    .access_size = count_each, /* for initialize */
                    .significant_rank = -1,
                };
                benchmark->init(gds, count_each, buff, &param);
            }

            GDS_fence(gds);

            for (i = 0; i < MIN_MSGS; i++) {
                double tb, te, usec, sum;
                struct param param = {
                    .access_size = width,
                    .start = i * stride,
                    .significant_rank = srank,
                };
                GDS_fence(gds);

                tb = MPI_Wtime();
                benchmark->run(gds, count_each, buff + i * stride, &param);
                te = MPI_Wtime();

                usec = 0.0;
                if (my_rank == srank) {
                    usec = (te - tb) * 1000000.0;
                    printf("raw, %s, %s, %zu, %f\n",
                        array_type_str, benchmark->desc, width * sizeof(int),
                        usec);
                }
                MPI_Reduce(&usec, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
                lat_sum += sum;
            }

            GDS_fence(gds);

            if (!benchmark->do_retry)
                goto skip;

            for (i = 0; i < MIN_MSGS; i++) {
                int srank = n % n_procs;
                double tb, te, usec, sum;
                struct param param = {
                    .access_size = width,
                    .start = i * stride,
                    .significant_rank = srank,
                };
                GDS_fence(gds);

                tb = MPI_Wtime();
                benchmark->run(gds, count_each, buff + i * stride, &param);
                te = MPI_Wtime();
                usec = 0.0;
                if (my_rank == srank) {
                    usec = (te - tb) * 1000000.0;
                    printf("raw, %s, %s_rt, %zu, %f\n",
                        array_type_str, benchmark->desc, width * sizeof(int),
                        usec);
                }
                MPI_Reduce(&usec, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
                lat_sum_rt += sum;
            }
            GDS_fence(gds);
        skip:
            if (!skip_gds_free)
                GDS_free(&gds);
        }

        if (my_rank == 0) {
            printf("avg, %s, %s, %zu, %f\n",
                array_type_str, benchmark->desc, width * sizeof(int),
                lat_sum / MIN_MSGS / N_TRY);
            if (benchmark->do_retry)
                printf("avg, %s, %s_rt, %zu, %f\n",
                    array_type_str, benchmark->desc, width * sizeof(int),
                    lat_sum_rt / MIN_MSGS / N_TRY);
        }
    }

    free(buff);
}

struct array_flavor fl_flat_rma = { .repr = GDS_REPR_FLAT,
                                    .log_get_impl = GDS_FLAT_GET_IMPL_RMA,
                                    .log_put_impl = GDS_FLAT_PUT_IMPL_RMA, };
struct array_flavor fl_flat_msg = { .repr = GDS_REPR_FLAT,
                                    .log_get_impl = GDS_FLAT_GET_IMPL_MSG,
                                    .log_put_impl = GDS_FLAT_PUT_IMPL_MSG, };
struct array_flavor fl_log_rma = { .repr = GDS_REPR_LOG,
                                   .log_bs = BLOCKSIZE,
                                   .log_get_impl = GDS_LOG_GET_IMPL_RMA,
                                   .log_put_impl = GDS_LOG_PUT_IMPL_RMA, };
struct array_flavor fl_log_msg = { .repr = GDS_REPR_LOG,
                                   .log_bs = BLOCKSIZE,
                                   .log_get_impl = GDS_LOG_GET_IMPL_MSG,
                                   .log_put_impl = GDS_LOG_PUT_IMPL_MSG, };
struct array_flavor fl_log_hyb = { .repr = GDS_REPR_LOG,
                                   .log_bs = BLOCKSIZE,
                                   .log_get_impl = GDS_LOG_GET_IMPL_RMA,
                                   .log_put_impl = GDS_LOG_PUT_IMPL_HYB, };

static void do_p2p_bandwidth(int n)
{
    run_bwbench(&fl_flat_rma, &bw_warmup);

    switch (n) {
    case 0:
        print_banner("---- Local sequential write (flat array) ----");
        run_bwbench(&fl_flat_rma, &local_seq_w);
        run_bwbench(&fl_flat_msg, &local_seq_w);

        print_banner("---- Local sequential write (log-structured array) ----");
        run_bwbench(&fl_log_rma, &local_seq_w);
        run_bwbench(&fl_log_msg, &local_seq_w);
        run_bwbench(&fl_log_hyb, &local_seq_w);

        break;

    case 1:
        print_banner("---- Local sequential overwrite (flat array) ----");
        run_bwbench(&fl_flat_rma, &local_seq_ow);
        run_bwbench(&fl_flat_msg, &local_seq_ow);

        print_banner("---- Local sequential overwrite (log-structured array) ----");
        run_bwbench(&fl_log_rma, &local_seq_ow);
        run_bwbench(&fl_log_msg, &local_seq_ow);
        run_bwbench(&fl_log_hyb, &local_seq_ow);

        break;

    case 2:
        print_banner("---- Local sequential read (flat array) ----");
        run_bwbench(&fl_flat_rma, &local_seq_r);
        run_bwbench(&fl_flat_msg, &local_seq_r);

        print_banner("---- Local sequential read (log-structured array) ----");
        run_bwbench(&fl_log_rma, &local_seq_r);
        run_bwbench(&fl_log_msg, &local_seq_r);
        run_bwbench(&fl_log_hyb, &local_seq_r);

        break;

    case 3:
        print_banner("---- Neighbor sequential write (flat array) ----");
        run_bwbench(&fl_flat_rma, &neighbor_seq_w);
        run_bwbench(&fl_flat_msg, &neighbor_seq_w);

        print_banner("---- Neighbor sequential write (log-structured array) ----");
        run_bwbench(&fl_log_rma, &neighbor_seq_w);
        run_bwbench(&fl_log_msg, &neighbor_seq_w);
        run_bwbench(&fl_log_hyb, &neighbor_seq_w);

        break;

    case 4:
        print_banner("---- Neighbor sequential overwrite (flat array) ----");
        run_bwbench(&fl_flat_rma, &neighbor_seq_ow);
        run_bwbench(&fl_flat_msg, &neighbor_seq_ow);

        print_banner("---- Neighbor sequential overwrite (log-structured array) ----");
        run_bwbench(&fl_log_rma, &neighbor_seq_ow);
        run_bwbench(&fl_log_msg, &neighbor_seq_ow);
        run_bwbench(&fl_log_hyb, &neighbor_seq_ow);

        break;

    case 5:
        print_banner("---- Neighbor sequential read (flat array) ----");
        run_bwbench(&fl_flat_rma, &neighbor_seq_r);
        run_bwbench(&fl_flat_msg, &neighbor_seq_r);

        print_banner("---- Neighbor sequential read (log-structured array) ----");
        run_bwbench(&fl_log_rma, &neighbor_seq_r);
        run_bwbench(&fl_log_msg, &neighbor_seq_r);
        run_bwbench(&fl_log_hyb, &neighbor_seq_r);
        break;
    }
}

static void do_asym_bandwidth(int n)
{
    switch (n) {
    case 0:
        print_banner("---- Multi-server write (flat array) ----");
        run_bwbench(&fl_flat_rma, &ms_w); run_bwbench(&fl_flat_msg, &ms_w);

        print_banner("---- Multi-server write (log-structured array) ----");
        run_bwbench(&fl_log_rma, &ms_w);
        run_bwbench(&fl_log_msg, &ms_w);
        run_bwbench(&fl_log_hyb, &ms_w);
        break;

    case 1:
        print_banner("---- Multi-server overwrite (flat array) ----");
        run_bwbench(&fl_flat_rma, &ms_ow); run_bwbench(&fl_flat_msg, &ms_ow);

        print_banner("---- Multi-server overwrite (log-structured array) ----");
        run_bwbench(&fl_log_rma, &ms_ow);
        run_bwbench(&fl_log_msg, &ms_ow);
        run_bwbench(&fl_log_hyb, &ms_ow);
        break;

    case 2:
        print_banner("---- Multi-server read  (flat array) ----");
        run_bwbench(&fl_flat_rma, &ms_r); run_bwbench(&fl_flat_msg, &ms_r);

        print_banner("---- Multi-server read (log-structured array) ----");
        run_bwbench(&fl_log_rma, &ms_r);
        run_bwbench(&fl_log_msg, &ms_r);
        run_bwbench(&fl_log_hyb, &ms_r);
        break;

    case 3:
        print_banner("---- Multi-client write (flat array) ----");
        run_bwbench(&fl_flat_rma, &mc_w); run_bwbench(&fl_flat_msg, &mc_w);

        print_banner("---- Multi-client write (log-structured array) ----");
        run_bwbench(&fl_log_rma, &mc_w);
        run_bwbench(&fl_log_msg, &mc_w);
        run_bwbench(&fl_log_hyb, &mc_w);
        break;

    case 4:
        print_banner("---- Multi-client overwrite (flat array) ----");
        run_bwbench(&fl_flat_rma, &mc_ow); run_bwbench(&fl_flat_msg, &mc_ow);

        print_banner("---- Multi-client overwrite (log-structured array) ----");
        run_bwbench(&fl_log_rma, &mc_ow);
        run_bwbench(&fl_log_msg, &mc_ow);
        run_bwbench(&fl_log_hyb, &mc_ow);
        break;

    case 5:
        print_banner("---- Multi-client read (flat array) ----");
        run_bwbench(&fl_flat_rma, &mc_r); run_bwbench(&fl_flat_msg, &mc_r);

        print_banner("---- Multi-client read (log-structured array) ----");
        run_bwbench(&fl_log_rma, &mc_r);
        run_bwbench(&fl_log_msg, &mc_r);
        run_bwbench(&fl_log_hyb, &mc_r);
        break;

    case 6:
        print_banner("---- Random walk (flat array) ----");
        run_bwbench(&fl_flat_rma, &rnd); run_bwbench(&fl_flat_msg, &rnd);

        print_banner("---- Random walk (log-structured array) ----");
        run_bwbench(&fl_log_rma, &rnd);
        run_bwbench(&fl_log_msg, &rnd);
        run_bwbench(&fl_log_hyb, &rnd);
        break;
    }
}

static void do_random(void)
{
    print_banner("---- Random walk (flat array) ----");
    run_rndbench(&fl_flat_rma, &rnd); run_rndbench(&fl_flat_msg, &rnd);

    print_banner("---- Random walk (log-structured array) ----");
    run_rndbench(&fl_log_rma, &rnd);
    run_rndbench(&fl_log_msg, &rnd);
    run_rndbench(&fl_log_hyb, &rnd);
}

static void do_verinc(void)
{
    if (my_rank == 0)
        printf("VERINC_COUNT_PER_PROC = %llu MB\n",
            VERINC_COUNT_PER_PROC * sizeof(int) / 1024 / 1024);

    print_banner("---- Version increment (flat array) ----");
    run_verinc(&fl_flat_rma);

    print_banner("---- Version increment (log-structured array) ----");
    run_verinc(&fl_log_msg);
}

static void do_p2p_latency(int n)
{
    run_latbench(&fl_flat_rma, &lat_warmup);

    switch (n) {
    case 0:
        print_banner("---- Local write latency (flat array) ----");
        run_latbench(&fl_flat_rma, &lat_local_w); run_latbench(&fl_flat_msg, &lat_local_w);

        print_banner("---- Local write latency (log-structured array) ----");
        run_latbench(&fl_log_rma, &lat_local_w);
        run_latbench(&fl_log_msg, &lat_local_w);
        run_latbench(&fl_log_hyb, &lat_local_w);
        break;

    case 1:
        print_banner("---- Local overwrite latency, cache miss (flat array) ----");
        run_latbench(&fl_flat_rma, &lat_local_ow); run_latbench(&fl_flat_msg, &lat_local_ow);

        print_banner("---- Local overwrite latency, cache miss (log-structured array) ----");
        run_latbench(&fl_log_rma, &lat_local_ow);
        run_latbench(&fl_log_msg, &lat_local_ow);
        run_latbench(&fl_log_hyb, &lat_local_ow);
        break;

    case 2:
        print_banner("---- Local read latency (flat array) ----");
        run_latbench(&fl_flat_rma, &lat_local_r); run_latbench(&fl_flat_msg, &lat_local_r);

        print_banner("---- Local read latency (log-structured array) ----");
        run_latbench(&fl_log_rma, &lat_local_r);
        run_latbench(&fl_log_msg, &lat_local_r);
        run_latbench(&fl_log_hyb, &lat_local_r);
        break;

    case 3:
        print_banner("---- Neighbor write latency (flat array) ----");
        run_latbench(&fl_flat_rma, &lat_neighbor_w); run_latbench(&fl_flat_msg, &lat_neighbor_w);

        print_banner("---- Neighbor write latency (log-structured array) ----");
        run_latbench(&fl_log_rma, &lat_neighbor_w);
        run_latbench(&fl_log_msg, &lat_neighbor_w);
        run_latbench(&fl_log_hyb, &lat_neighbor_w);
        break;

    case 4:
        print_banner("---- Neighbor overwrite latency, cache miss (flat array) ----");
        run_latbench(&fl_flat_rma, &lat_neighbor_ow); run_latbench(&fl_flat_msg, &lat_neighbor_ow);

        print_banner("---- Neighbor overwrite latency, cache miss (log-structured array) ----");
        run_latbench(&fl_log_rma, &lat_neighbor_ow);
        run_latbench(&fl_log_msg, &lat_neighbor_ow);
        run_latbench(&fl_log_hyb, &lat_neighbor_ow);
        break;

    case 5:
        print_banner("---- Neighbor read latency (flat array) ----");
        run_latbench(&fl_flat_rma, &lat_neighbor_r); run_latbench(&fl_flat_msg, &lat_neighbor_r);

        print_banner("---- Neighbor read latency (log-structured array) ----");
        run_latbench(&fl_log_rma, &lat_neighbor_r);
        run_latbench(&fl_log_msg, &lat_neighbor_r);
        run_latbench(&fl_log_hyb, &lat_neighbor_r);
        break;

    case 6:
        run_latbench(&fl_flat_rma, &lat_neighbor_a);
        break;
    }
}

int main(int argc, char *argv[])
{
    int opt, ret = 0, n = 0;
    int mode = 0;
    GDS_thread_support_t provd_support;

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support);

    GDS_comm_rank(GDS_COMM_WORLD, &my_rank);
    GDS_comm_size(GDS_COMM_WORLD, &n_procs);

    while ((opt = getopt(argc, argv, "a:b:cvl:rs")) != -1) {
        switch (opt) {
        case 'a':
            if (n_procs < 2) {
                fprintf(stderr,
                    "Asymmetric mode requires more than 1 process.\n");
                ret = 1;
                goto out;
            }
            mode = 'a';
            n = atoi(optarg);
            break;

        case 'b':
        case 'l':
            n = atoi(optarg);
            /* Fall through */
        case 'c':
        case 'v':
        case 'r':
            mode = opt;
            break;

        case 's':
            skip_gds_free = true;
            break;

        default:
            fprintf(stderr, "Unknown option: %c\n"
                "Usage: logstr_bench <options>\n"
                "\n"
                "Options:\n"
                "  -a    Asymmetric mode\n"
                "  -b    P2P bandwidth mode\n"
                "  -r    Random walk\n"
                "  -l    Latency mode\n"
                "  -s    Skip GDS_free\n"
                "  -v    Version increment mode\n",
                opt);
            ret = 1;
            goto out;
        }
    }

    switch (mode) {
    case 'a':
        do_asym_bandwidth(n);
        break;

    case 'b':
        do_p2p_bandwidth(n);
        break;

    case 'c':
        run_bwbench(&fl_flat_rma, &neighbor_seq_a);
        break;

    case 'v':
        do_verinc();
        break;

    case 'l':
        do_p2p_latency(n);
        break;

    case 'r':
        do_random();
        break;

    default:
        fprintf(stderr, "Unknown mode\n");
        break;
    }

out:
    GDS_finalize();

    return ret;
}
