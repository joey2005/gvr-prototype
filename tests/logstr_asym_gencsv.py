#
#  Global View Resilience (GVR)
#  http://gvr.cs.uchicago.edu
#
#  Copyright (C) 2014 University of Chicago.
#  See license.txt in top-level directory.
#

import sys
import numpy
import getopt

delim = ','

def fst(x):
    return x[0]

def snd(x):
    return x[1]

def draw(bench_type, width):
    dataset = {}

    for line in sys.stdin:
        strs = line.split(", ")

        if strs[0] != "raw":
            continue

        atype = strs[1]
        btype = strs[2]

        if btype[len(btype)-3:] == '_rt':
            btype = btype[0:len(btype)-3]
            atype += '-ch'

        if btype[len(btype)-3:] == '_ow':
            btype = btype[0:len(btype)-3] + '_w'
            atype += '-ow-cm'

        if btype !=bench_type:
            continue

        w = int(strs[3])

        if not w == width:
            continue

        bw = float(strs[5])
        np = int(strs[6])

        if not dataset.has_key(np):
            dataset[np] = {}

        if not dataset[np].has_key(atype):
            dataset[np][atype] = []

        # print "Appending %f to dataset[%d][%s]" % (bw, np, atype)
        dataset[np][atype].append(bw)

    write = sys.stdout.write
    first_line = True
    for np in sorted(dataset.keys()):
        if first_line:
            write("nodes");
            for ty in dataset[np]:
                write("%s%s" % (delim, ty))
            write("\n")
            first_line = False

        write("%d" % np)
        for ty in dataset[np]:
            write("%s%f" % (delim, numpy.mean(dataset[np][ty])))
        write("\n")

optlist, args = getopt.getopt(sys.argv[1:], 't')
for o, a in optlist:
    if o == "-t":
        delim = '\t'
    else:
        assert False, "Unknown option"

if len(args) < 2:
    print "Usage: asym_plot <btype> <width>"
    sys.exit(1)

draw(args[0], int(args[1]))
