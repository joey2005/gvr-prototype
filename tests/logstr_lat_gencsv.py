#
#  Global View Resilience (GVR)
#  http://gvr.cs.uchicago.edu
#
#  Copyright (C) 2014 University of Chicago.
#  See license.txt in top-level directory.
#

import sys
import numpy
import getopt

delim = ','

def draw(fp):
    dataset = {}

    for line in fp:
        strs = line.split()

        for i in range(len(strs)):
            strs[i] = strs[i].rstrip(',')

        # "raw, flat, local_seq_w, 16, 0.426334, 2459.518"
        if len(strs) == 0 or strs[0] != "raw":
            continue

        atype = strs[1]
        btype = strs[2]
        w = int(strs[3])
        lat = float(strs[4])

        if btype[len(btype)-3:] == '_rt':
            btype = btype[0:len(btype)-3]
            atype += '-ch'

        if btype[len(btype)-3:] == '_ow':
            btype = btype[0:len(btype)-3] + '_w'
            atype += '-ow-cm'

        atype = btype + ':' + atype

        if not dataset.has_key(w):
            dataset[w] = {}

        if not dataset[w].has_key(atype):
            dataset[w][atype] = []

        dataset[w][atype].append(lat)

    first_line = True

    write = sys.stdout.write
    for w in sorted(dataset.keys()):
        tys = dataset[w].keys()
        if first_line:
            write("width")
            for ty in tys:
                write("%s%s" % (delim, ty))
            first_line = False
            write("\n")

        write("%d" % w)
        for ty in tys:
            write("%s%f" % (delim, numpy.mean(dataset[w][ty])))
        write("\n")

optlist, args = getopt.getopt(sys.argv[1:], 't')
for o, a in optlist:
    if o == "-t":
        delim = '\t'
    else:
        assert False, "Unknown option"

draw(sys.stdin)
