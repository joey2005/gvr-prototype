/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  Test code that simulates OpenMC's cross section table and
  its recovery routine
*/

#include "common.h"

#undef  TEST_NAME
#define TEST_NAME "OpenMC mock error test"

struct openmcmock_xs {
    double absorption;
    double fission;
    unsigned checksum;
};

static void openmcmock_calc_xs(struct openmcmock_xs *xs, int i)
{
    /* Meaningless values, of course! */
    xs->absorption = 0.1*i;
    xs->fission = 0.2 + 0.01*i;
}

static unsigned openmcmock_calc_checksum(const struct openmcmock_xs *xs)
{
    int i;
    unsigned c = 0;
    union {
        struct openmcmock_xs xs;
        unsigned ui[sizeof(struct openmcmock_xs)/sizeof(unsigned)];
    } u;

    u.xs = *xs;

    for (i = 0; i < sizeof(*xs)/sizeof(unsigned); i++) {
        c ^= u.ui[i];
    }

    return c;
}

#define XS_SIZE 4096
#define XS_TARGET 1023

static void openmcmock_init(GDS_gds_t gds, int n)
{
    int i;
    GDS_size_t lo[1];
    GDS_size_t hi[1];
    GDS_size_t count = sizeof(struct openmcmock_xs);
    GDS_size_t ld[] = {};

    for (i = 0; i < n; i++) {
        struct openmcmock_xs xs = { .checksum = 0 };

        openmcmock_calc_xs(&xs, i);
        xs.checksum = openmcmock_calc_checksum(&xs);

        lo[0] = i*sizeof(xs);
        hi[0] = lo[0] + count - 1;
        if (i == XS_TARGET) {
            /* Fault injection */
            xs.absorption -= 3.14159;
        }
        ASSERT_STAT("GDS_put", GDS_put(&xs, ld, lo, hi, gds));
        GDS_wait(gds);
    }
}

static GDS_status_t openmcmock_error_handler(GDS_gds_t gds, GDS_error_t error_desc)
{
    int flag;
    GDS_size_t offset, count, len;
    struct openmcmock_xs xs;
    GDS_size_t hi[1];
    GDS_size_t ld[] = {};

    ASSERT_STAT("GDS_get_error_attr",
        GDS_get_error_attr(error_desc, GDS_EATTR_GDS_INDEX,
            &offset, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);
    ASSERT_STAT("GDS_get_error_attr_len",
                GDS_get_error_attr_len(error_desc, GDS_EATTR_GDS_INDEX, &len, &flag));
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(int64_t));

    ASSERT_STAT("GDS_get_error_attr", 
        GDS_get_error_attr(error_desc, GDS_EATTR_GDS_COUNT,
            &count, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);
    ASSERT_STAT("GDS_get_error_attr_len",
                GDS_get_error_attr_len(error_desc, GDS_EATTR_GDS_INDEX, &len, &flag));
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(int64_t));

    ASSERT_EQUAL_INT(offset, sizeof(xs)*XS_TARGET);
    ASSERT_EQUAL_INT(count, sizeof(xs));

    openmcmock_calc_xs(&xs, offset/sizeof(xs));
    xs.checksum = 0;
    xs.checksum = openmcmock_calc_checksum(&xs);

    hi[0] = offset + count - 1;
    ASSERT_STAT("GDS_put", 
        GDS_put(&xs, ld, &offset, hi, gds));
    GDS_wait(gds);

    ASSERT_STAT("GDS_resume_local", 
        GDS_resume_local(gds, error_desc));

    return GDS_STATUS_OK;
}

void test_openmcmock_error(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[1];
    int ndim = 1, i, rank;
    int nprocs;
    int ibegin, iend, per_proc;
    GDS_size_t count[] = {sizeof(struct openmcmock_xs)};
    GDS_size_t min_chunk[] = {0};
    GDS_size_t ld[] = {};

    cts[0] = XS_SIZE * sizeof(struct openmcmock_xs);

    ASSERT_TEST_START(TEST_NAME);

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &nprocs);

    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_BYTE, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    GDS_error_pred_t pred;
    GDS_error_pred_term_t term;
    GDS_size_t offset_range[] = {0, cts[0] - 1};
    GDS_size_t count_range[] = {0, cts[0]};

    ASSERT_STAT_FATAL("GDS_create_error_pred",
                      GDS_create_error_pred(&pred));

    ASSERT_STAT("GDS_create_error_pred_term",
                GDS_create_error_pred_term(GDS_EATTR_GDS_INDEX,
                                           GDS_EMEXP_RANGE,
                                           sizeof(offset_range), offset_range,
                                           &term));
    ASSERT_NOT_NULL_FATAL(term);
    ASSERT_STAT("GDS_add_error_pred_term",
                GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT("GDS_create_error_pred_term",
                GDS_create_error_pred_term(GDS_EATTR_GDS_COUNT,
                                           GDS_EMEXP_RANGE,
                                           sizeof(count_range), count_range,
                                           &term));
    ASSERT_NOT_NULL_FATAL(term);
    ASSERT_STAT("GDS_add_error_pred_term",
                GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT_FATAL("GDS_register_local_error_handler",
        GDS_register_local_error_handler(gds, pred,
                                         openmcmock_error_handler));

    GDS_free_error_pred(&pred);

    if (rank == 0)
        openmcmock_init(gds, XS_SIZE);

    GDS_fence(gds);

    per_proc = (XS_SIZE+1)/nprocs;
    ibegin   = rank * per_proc;
    iend     = (rank+1) * per_proc;
    if (iend > XS_SIZE)
        iend = XS_SIZE;

    for (i = ibegin; i < iend; i++) {
        struct openmcmock_xs xs;
        GDS_size_t offset[] = { i*sizeof(xs) };
        GDS_size_t hi[] = { (i + 1) * sizeof(xs) - 1};
        ASSERT_STAT("GDS_get", 
            GDS_get(&xs, ld, offset, hi, gds));
        GDS_wait(gds);
        if (i == XS_TARGET) {
            GDS_error_t err;
            GDS_size_t error_off = offset[0];
            GDS_size_t error_ct  = count[0];

            GDS_create_error_descriptor(&err);
            ASSERT_NOT_NULL_FATAL(err);
            GDS_add_error_attr(err, GDS_EATTR_GDS_INDEX,
                               sizeof(error_off), &error_off);
            GDS_add_error_attr(err, GDS_EATTR_GDS_COUNT,
                               sizeof(error_ct), &error_ct);

            ASSERT_NOT_EQUAL_INT(openmcmock_calc_checksum(&xs), 0);
            /* TODO: it is legal to return GDS_STATUS_NOMEM upon memory shortage */
            ASSERT_STAT("GDS_raise_local_error",
                        GDS_raise_local_error(gds, err));
        } else {
            ASSERT_EQUAL_INT(openmcmock_calc_checksum(&xs), 0);
        }
    }

    /* Now all the entries should be correct */
    for (i = ibegin; i < iend; i++) {
        struct openmcmock_xs xs;
        GDS_size_t offset[] = { i*sizeof(xs) };
        GDS_size_t hi[] = { (i + 1) *sizeof(xs) - 1};
        ASSERT_STAT("GDS_get", 
            GDS_get(&xs, ld, offset, hi, gds));
        GDS_wait(gds);
        ASSERT_EQUAL_INT(openmcmock_calc_checksum(&xs), 0);
    }

    GDS_fence(NULL);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}
