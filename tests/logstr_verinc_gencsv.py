#
#  Global View Resilience (GVR)
#  http://gvr.cs.uchicago.edu
#
#  Copyright (C) 2014 University of Chicago.
#  See license.txt in top-level directory.
#

import sys
import numpy

def draw(fp):
    dataset = {}

    for line in fp:
        strs = line.split()

        for i in range(len(strs)):
            strs[i] = strs[i].rstrip(',')

        # raw, 16, log-bs128, 2147483648, 0.42287
        if len(strs) == 0 or strs[0] != "avg":
            continue

        np = int(strs[1])
        atype = strs[2] + '-' + strs[1]
        asize = long(strs[3])
        t = float(strs[4])

        if not dataset.has_key(asize):
            dataset[asize] = {}

        if not dataset[asize].has_key(atype):
            dataset[asize][atype] = []

        dataset[asize][atype].append(t)

    first_line = True

    write = sys.stdout.write
    for s in sorted(dataset.keys()):
        tys = dataset[s].keys()
        if first_line:
            write("size")
            for ty in tys:
                write(", %s" % (ty))
            first_line = False
            write("\n")

        write("%ld" % s)
        for ty in tys:
            # make it microseconds
            write(", %f" % (numpy.mean(dataset[s][ty]) * 1000000))
        write("\n")

draw(sys.stdin)
