#
#  Global View Resilience (GVR)
#  http://gvr.cs.uchicago.edu
#
#  Copyright (C) 2014 University of Chicago.
#  See license.txt in top-level directory.
#

import sys
import matplotlib.pyplot as plt

def fst(x):
    return x[0]

def snd(x):
    return x[1]

def is_overwrite(s):
    return s[len(s)-3:] == '_ow'

def draw(btype, width):
    dataset = {}

    for line in sys.stdin:
        strs = line.split(", ")

        if strs[0] != "avg":
            continue

        atype = strs[1]

        bt = strs[2]
        if is_overwrite(bt):
            bt = bt[0:len(bt)-3]
            atype += '-ow'

        if btype != bt:
            continue

        if width != int(strs[3]):
            continue

        bw = float(strs[5])
        np = int(strs[6])

        if not dataset.has_key(atype):
            dataset[atype] = []

        dataset[atype].append((np, bw))

    for ty in dataset.keys():
        d = sorted(dataset[ty], key = fst)
        plt.loglog(map(fst, d), map(snd, d), label=ty, basex=2)
    plt.legend(loc='best')
    plt.xlabel("Number of Nodes")
    plt.ylabel("Aggregate Bandwidth (KB/s)")
    title = "%s-%d" % (btype, width)
    plt.title(title)
    plt.savefig(title + ".png")
    #plt.show()

if len(sys.argv) < 3:
    print "Usage: asym_plot <btype> <width>"
    sys.exit(1)

draw(sys.argv[1], int(sys.argv[2]))
