/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include "common.h"

/* api.c */
void test_gds_get_comm(void);
void test_gds_compare_and_swap(void);
void test_gds_version_label(void);
void test_gds_get_attr(void);
void test_gds_get_acc(void);
void test_gds_access_1d(void);

void test_gds_create_single(void);
void test_gds_create_small_rw(void);
void test_gds_create_small_acc(void);
void test_gds_create_ver_traverse(void);
void test_gds_create_vector(void); 
void test_gds_create_2d(void);
void test_gds_create_version_inc(void);
void test_local_error(void);
void test_gds_set_error_handler(void);

/* basic.c */
void test_allocation(void);
void test_array_rw(void);
void test_array_acc(void);
void test_subcomm(void);
void test_array_2d_rowm(void);
void test_array_2d_colm(void);
void test_array_3d_rowm(void);
void test_array_3d_colm(void);
void test_multi_alloc(void);
void test_single_rw(void);
void test_stride(void);
void test_iaxpy(void);
void test_ver_traverse(void);
void test_ver_restore(void);
void test_clone(void);
void test_issue18(void);
void test_gvr_2d_test(void);
void test_large_matrix(void);
void test_global_local_error_check_recovery(void);
void test_minchunk(void);
#ifdef GDS_CONFIG_USE_LRDS
void test_register_lrds_callback(void);
#endif

/* openmcmock.c */
void test_openmcmock_error(void);
   
/* particle.c */
void test_particle(void);
void test_particle_global(void);

/* dgemm.c */
void test_naive_dgemm(void);
void test_block_dgemm(void);

/* log_array_test.c */
void do_log_array_tests(void);

int dprint_level = 0;

int main(int argc, char **argv)
{
    int ret = 0;
    GDS_thread_support_t provd_support;
    
    ASSERT_INIT();
    
    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support);
    GDS_comm_size(GDS_COMM_WORLD, &nprocs);
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    /* List all tests here */

    /* API tests */
    test_gds_get_comm();
    test_gds_compare_and_swap();
    test_gds_version_label();
    test_gds_get_attr();
    test_gds_get_acc();
    test_gds_access_1d();
    test_gds_create_single();
    test_gds_create_small_rw();
    test_gds_create_small_acc();
    test_gds_create_ver_traverse();
    test_gds_create_vector(); 
    test_gds_create_2d();
    test_gds_create_version_inc();
    test_local_error();
    test_gds_set_error_handler();
 
    /* Functionality tests */
    test_allocation();
    test_array_rw();
    test_array_acc();
    test_subcomm();
    test_array_2d_rowm();
    test_array_2d_colm();
    test_array_3d_rowm();
    test_array_3d_colm();
    test_multi_alloc();
    test_single_rw();
    test_stride();
    test_iaxpy();
    test_ver_traverse();
    test_ver_restore();
    test_clone();
    test_issue18();
    test_gvr_2d_test();
    test_large_matrix();
    test_global_local_error_check_recovery();
    test_minchunk();

#ifdef GDS_CONFIG_USE_LRDS
    /* lrds callback test */
    test_register_lrds_callback();
#endif

    test_openmcmock_error();

    test_particle();
    test_particle_global();

    test_naive_dgemm();
    test_block_dgemm();

    /* Log-structured array tests */
    do_log_array_tests();

    ret = ASSERT_REPORT() == 0 ? 0 : 1;
    
    GDS_finalize();
    return ret;
}
