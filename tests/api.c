/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  APIs tests
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "common.h"

#undef	TEST_NAME
#define TEST_NAME "API: GDS_get_comm()"
void test_gds_get_comm(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_comm_t comm = -1;   /* Use -1 as test value */
    int cmp;
    
    ASSERT_TEST_START(TEST_NAME);
    
    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    ASSERT_STAT("GDS_get_comm", GDS_get_comm(gds, &comm));
    MPI_Comm_compare(comm, GDS_COMM_WORLD, &cmp);
    ASSERT_EQUAL_INT(cmp, MPI_CONGRUENT);

    /* TODO: Test with sub communicator */

    ASSERT_STAT("GDS_free", GDS_free(&gds));

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_compare_and_swap()"
void test_gds_compare_and_swap(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo[] = {100}, hi[] = {100}, offset[] = {100}, ld[] = {0};
    int put_buf[] = {432};
    int get_buf[1] = {0}; 
    int comp_buf = 432;
    int src_buf = 123;
    int res_buf = 0;
        
    ASSERT_TEST_START(TEST_NAME);
             
    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds)); 
    ASSERT_NOT_NULL_FATAL(gds);
    
    /* Each process should touch a separate cell */
    lo[0] = (GDS_size_t) myrank;
    hi[0] = (GDS_size_t) myrank;
    offset[0] = (GDS_size_t) myrank;
    ASSERT_STAT_FATAL("GDS_put", GDS_put(put_buf, ld, lo, hi, gds));
    GDS_fence(gds);
     
    ASSERT_STAT_FATAL("GDS_compare_and_swap",
        GDS_compare_and_swap(&comp_buf, &src_buf, &res_buf, offset, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(res_buf, 432);

    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buf, ld, lo, hi, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(get_buf[0], 123);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_version_inc() and GDS_get_version_label()"
void test_gds_version_label(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo[] = {0}, hi[] = {0}, ld[] = {0};
    int put_buf[] = {432};
    char *label;
    size_t label_size;
    char *ver1 = "version one";
    char *ver2 = "version two";

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    ASSERT_STAT_FATAL("GDS_put", GDS_put(put_buf, ld, lo, hi, gds));
    GDS_fence(gds);
    ASSERT_STAT("GDS_version_inc", GDS_version_inc(gds, 1, ver1, strlen(ver1)));

    put_buf[0] = 433;
    ASSERT_STAT_FATAL("GDS_put", GDS_put(put_buf, ld, lo, hi, gds));
    GDS_fence(gds);
    ASSERT_STAT("GDS_version_inc", GDS_version_inc(gds, 2, ver2, strlen(ver2)));

    ASSERT_STAT("GDS_get_version_label",
        GDS_get_version_label(gds, &label, &label_size));

    ASSERT_EQUAL_STR(label, ver2, strlen(ver2));

    ASSERT_STAT("GDS_move_to_prev", GDS_move_to_prev(gds));

    ASSERT_STAT("GDS_get_version_label",
        GDS_get_version_label(gds, &label, &label_size));

    ASSERT_EQUAL_STR(label, ver1, strlen(ver1));

    ASSERT_STAT("GDS_free", GDS_free(&gds));

    ASSERT_TEST_END();
}

/* Tests for create */
#define N_DIM_OPTS 3
size_t vec_len;
size_t mat_major_axis, mat_minor_axis, mat_dims[2];

GDS_status_t root_only_global_to_local(const GDS_size_t global_indices[], 
    int *local_rank, GDS_size_t *local_offset) 
{
    *local_rank = 0;
    *local_offset = global_indices[0];
    return GDS_STATUS_OK;
}


GDS_status_t root_only_local_to_global(GDS_size_t local_offset, 
    GDS_size_t *global_indices)
{
    global_indices[0] = local_offset; 
    return GDS_STATUS_OK;
}

GDS_status_t vector_global_to_local(const GDS_size_t global_indices[], 
    int *local_rank, GDS_size_t *local_offset) 
{
    int comm_size;
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    int cell_per_proc = vec_len / comm_size;
    *local_rank = global_indices[0] / cell_per_proc;
    *local_offset = global_indices[0] - ((*local_rank) * cell_per_proc);
    
    return GDS_STATUS_OK;
}


GDS_status_t vector_local_to_global(GDS_size_t local_offset, 
    GDS_size_t global_indices[])
{
    int comm_size; 
    int comm_rank;
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    GDS_comm_rank(GDS_COMM_WORLD, &comm_rank);
    int cell_per_proc = vec_len / comm_size;
    global_indices[0] = local_offset + comm_rank * cell_per_proc; 
    return GDS_STATUS_OK;
}

GDS_status_t block_simple_global_to_local(const GDS_size_t global_indices[], 
    int *local_rank, GDS_size_t *local_offset) 
{
    int comm_size;
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    int major_unit_per_proc = mat_dims[mat_major_axis] / comm_size;
    *local_rank = global_indices[mat_major_axis] / (major_unit_per_proc);
    *local_offset = global_indices[mat_major_axis] * mat_dims[mat_minor_axis] 
        + global_indices[mat_minor_axis] - (*local_rank) * major_unit_per_proc 
        * mat_dims[mat_minor_axis];
    return GDS_STATUS_OK;
}

GDS_status_t block_simple_local_to_global(GDS_size_t local_offset, 
    GDS_size_t *global_indices)
{
    int comm_size; 
    int comm_rank;
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    GDS_comm_rank(GDS_COMM_WORLD, &comm_rank);
    int major_unit_per_proc = mat_dims[mat_major_axis] / comm_size;
    global_indices[mat_major_axis] = local_offset / mat_dims[mat_minor_axis] 
        + comm_rank * major_unit_per_proc;
    global_indices[mat_minor_axis] = local_offset % mat_dims[mat_minor_axis];
    return GDS_STATUS_OK;
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_create() - Single create"
void test_gds_create_single(void)
{
    int rank, size; 
    GDS_comm_size(GDS_COMM_WORLD, &size);
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_gds_t gds = NULL;

    GDS_size_t len_local = 1024;
    vec_len = len_local * size;

    GDS_size_t ndim = 1;
    int local_buffer[len_local];
    MPI_Info info;
    MPI_Info_create(&info);
    
    ASSERT_TEST_START(TEST_NAME);
    
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);
    
    ASSERT_STAT_FATAL("GDS_create",
        GDS_create(ndim, &vec_len, GDS_DATA_INT, vector_global_to_local,
            vector_local_to_global, local_buffer, len_local,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    MPI_Info_free(&info);
    ASSERT_NOT_NULL_FATAL(gds);
    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_create() - Small read/write"
void test_gds_create_small_rw(void)
{

    int rank, size; 
    GDS_comm_size(GDS_COMM_WORLD, &size);
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_gds_t gds = NULL;
    GDS_size_t len_local = 1;
    vec_len = len_local * size;
    int local_buffer[len_local];
    int ndim = 1;
    GDS_size_t lo_ind[] = {rank}, hi_ind[] = {rank}, ld[] = {0};
    int put_buff[] = {432+rank};
    int get_buff[1];
    MPI_Info info;
    MPI_Info_create(&info);
    
    ASSERT_TEST_START(TEST_NAME);
    
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    ASSERT_STAT_FATAL("GDS_create",
        GDS_create(ndim, &vec_len, GDS_DATA_INT, vector_global_to_local,
            vector_local_to_global, local_buffer, len_local,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    MPI_Info_free(&info);
    ASSERT_NOT_NULL_FATAL(gds);

    ASSERT_STAT_FATAL("GDS_put",
        GDS_put(put_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    ASSERT_STAT_FATAL("GDS_get",
        GDS_get(get_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(put_buff[0], get_buff[0]);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_create() - Small accumulate"
void test_gds_create_small_acc(void)
{
    int rank, size; 
    GDS_comm_size(GDS_COMM_WORLD, &size);
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_gds_t gds = NULL;
    GDS_size_t len_local = 1;
    vec_len = len_local * size;
    int local_buffer[len_local];
    int ndim = 1;
    GDS_size_t lo_ind[] = {0}, hi_ind[] = {0}, ld[] = {0};
    int put_buff[] = {1};
    int acc_buff[] = {1};
    int get_buff[1];
    MPI_Info info;
    MPI_Info_create(&info);
    
    ASSERT_TEST_START(TEST_NAME);
    
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    ASSERT_STAT_FATAL("GDS_create",
        GDS_create(ndim, &vec_len, GDS_DATA_INT, vector_global_to_local, 
            vector_local_to_global, local_buffer, len_local,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    MPI_Info_free(&info);
    ASSERT_NOT_NULL_FATAL(gds);

    ASSERT_STAT_FATAL("GDS_put",
        GDS_put(put_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    
    ASSERT_STAT_FATAL("GDS_acc",
        GDS_acc(acc_buff, ld, lo_ind, hi_ind, MPI_SUM, gds));
    GDS_fence(gds);
    
    ASSERT_STAT_FATAL("GDS_get",
        GDS_get(get_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(size + 1, get_buff[0]);

    if (myrank == 0) {
        acc_buff[0] = 2;
        ASSERT_STAT_FATAL("GDS_acc",
            GDS_acc(acc_buff, ld, lo_ind, hi_ind, MPI_PROD, gds));
    }
    GDS_fence(gds);
    
    ASSERT_STAT_FATAL("GDS_get",
        GDS_get(get_buff, ld, lo_ind, hi_ind, gds));
    GDS_fence(gds);

    ASSERT_EQUAL_INT((size + 1) * 2, get_buff[0]);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}


#undef  TEST_NAME
#define TEST_NAME "API: GDS_create() - Version traverse"

#define NELEMS(a) (sizeof(a)/sizeof(a[0]))
void test_gds_create_ver_traverse(void)
{
    int rank, size; 
    GDS_comm_size(GDS_COMM_WORLD, &size);
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_gds_t gds = NULL;
    GDS_size_t len_local = 1024;
    vec_len = len_local * size;
    int local_buffer[len_local];
    int ndim = 1;
    MPI_Info info;
    MPI_Info_create(&info);
    GDS_size_t versions[] = {0, 1, 2, 5, 7, 10, 11};
    GDS_size_t lastver = versions[NELEMS(versions)-1];
    
    ASSERT_TEST_START(TEST_NAME);
    
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    ASSERT_STAT_FATAL("GDS_create",
        GDS_create(ndim, &vec_len, GDS_DATA_INT, vector_global_to_local,
            vector_local_to_global, local_buffer, len_local,
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
    MPI_Info_free(&info);
    ASSERT_NOT_NULL_FATAL(gds);

    /* create versions */
    int i;
    for (i = 1; i < NELEMS(versions); i++) {
        int inc = versions[i] - versions[i-1];
        ASSERT_STAT("GDS_version_inc",
            GDS_version_inc(gds, inc, NULL, 0));
        ASSERT_EQUAL_INT(GDS_version(gds), versions[i]);
    }

    ASSERT_STAT("GDS_version_dec", GDS_version_dec(gds, lastver));
    ASSERT_EQUAL_INT(GDS_version(gds), 0);

    ASSERT_STAT("GDS_move_to_newest", GDS_move_to_newest(gds));
    ASSERT_EQUAL_INT(GDS_version(gds), lastver);

    /* Go backward */
    for (i = NELEMS(versions)-1;i > 0;i--) {
        if (i > 0) {
            ASSERT_STAT("GDS_move_to_prev 1", GDS_move_to_prev(gds));
            ASSERT_EQUAL_INT(GDS_version(gds), versions[i-1]);
        }
        else {
            ASSERT_STAT("GDS_move_to_prev 2", GDS_move_to_prev(gds));
            ASSERT_EQUAL_INT(GDS_version(gds), 0);
            break;
        }
    }

    /* Go forward */
    for (i = 0; ;i++) {
        if (i < NELEMS(versions)-1) {
            ASSERT_STAT("GDS_move_to_next", GDS_move_to_next(gds));
            ASSERT_EQUAL_INT(GDS_version(gds), versions[i+1]);
        }
        else {
            ASSERT_STAT_INVALID("GDS_move_to_next",
                GDS_move_to_next(gds));
            ASSERT_EQUAL_INT(GDS_version(gds), lastver);
            break;
        }
    }

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_create() - vector"
void test_gds_create_vector(void)
{
    int i, celli;
    double *local_buffer, *src = NULL, *dst = NULL;
    MPI_Info info;
    GDS_gds_t gds;
    GDS_size_t len_local;
    GDS_size_t lo_ind[1] = {0};
    GDS_size_t hi_ind[1];
    GDS_size_t ld[0];
    GDS_size_t cells_per_proc[N_DIM_OPTS] = {2, 7, 1024};
    int comm_size; 
    int comm_rank;
    
    ASSERT_TEST_START(TEST_NAME);
    
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    GDS_comm_rank(GDS_COMM_WORLD, &comm_rank);
    for (celli = 0; celli < N_DIM_OPTS; ++celli) {
        len_local = cells_per_proc[celli];
        vec_len = len_local * comm_size;
        local_buffer = (double *)malloc(len_local * sizeof(*local_buffer));
        MPI_Info_create(&info);
        ASSERT_STAT_FATAL("GDS_create",
            GDS_create(1, &vec_len, GDS_DATA_DBL, vector_global_to_local,
                vector_local_to_global, local_buffer, len_local,
                GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds));
        ASSERT_NOT_NULL_FATAL(gds);
        if (comm_rank == 0) {
            src = (double *) malloc(vec_len * sizeof(*src));
            dst = (double *) calloc(vec_len, sizeof(*dst));
            for (i = 0; i < vec_len; ++i) {
                src[i] = 0.1 * (i + 1);
            }
            hi_ind[0] = vec_len - 1;
            ASSERT_STAT_FATAL("GDS_put",
                GDS_put(src, ld, lo_ind, hi_ind, gds));
            for (i = 0; i < len_local; ++i) {
                //printf("%d: %lf (%lf)\n", i, local_buffer[i], src[i]);
            }
            
        }
        GDS_fence(gds);
        if (comm_rank == 0) {
            ASSERT_STAT_FATAL("GDS_get",
                GDS_get(dst, ld, lo_ind, hi_ind, gds));
         }
         GDS_wait(gds);
         if (comm_rank == 0) {
            for (i = 0; i < vec_len; ++i) {
                //printf("%d: %lf (%lf)\n", i, dst[i], src[i]);
                ASSERT_EQUAL_INT(src[i], dst[i]);
            }
            free(src);
            free(dst);
        }
        ASSERT_STAT("GDS_free", GDS_free(&gds)); 
        free(local_buffer); 
        MPI_Info_free(&info);
    }
    
    ASSERT_TEST_END();
}

/**
 * Allocating a 2D gds.  that is 
 * square matrices 3 tests 1 * nproc row, 7 * nproc row, 1024 * nproc row
 */
#undef  TEST_NAME
#define TEST_NAME "API: GDS_create() - 2D array"
void test_gds_create_2d(void)
{
    int i, coli, rowi, orderi;
    double *local_buffer, *src = NULL, *dst = NULL;
    GDS_size_t local_count, global_count = 0;
    MPI_Info info;
    GDS_gds_t gds;
    GDS_size_t dims_local[2];
    GDS_size_t lo_ind[2] = {0, 0}; // starting at 0,0 for both tests
    GDS_size_t hi_ind[2]; //ending at some arbitrary index
    GDS_size_t ld[1];
    GDS_size_t dims_per_proc[N_DIM_OPTS] = {1, 7, 16};
    char * orders[2] = {GDS_ORDER_COL_MAJOR, GDS_ORDER_ROW_MAJOR};
    int comm_size; 
    int comm_rank;
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    GDS_comm_rank(GDS_COMM_WORLD, &comm_rank);

    ASSERT_TEST_START(TEST_NAME);

    for (orderi = 0; orderi <=1; ++orderi) {
        mat_major_axis = 1 - orderi;
        mat_minor_axis = 1 - mat_major_axis;
        for (rowi = 0; rowi < N_DIM_OPTS; ++rowi) {
            dims_local[0] = dims_per_proc[rowi];
            mat_dims[0] = orderi == 0 ? dims_local[0] :
                                        dims_local[0] * comm_size;
            for (coli = 0; coli < 2; ++coli) {
                dims_local[1] = dims_per_proc[coli];
                mat_dims[1] = orderi == 0 ? dims_local[1] * comm_size :
                                            dims_local[1];
                local_count = dims_local[0] * dims_local[1];
                local_buffer = (double *)malloc(local_count * 
                                                sizeof(*local_buffer));
                MPI_Info_create(&info);
                MPI_Info_set(info, GDS_ORDER_KEY, orders[orderi]);
                ASSERT_STAT_FATAL("GDS_create",
                    GDS_create(2, mat_dims, GDS_DATA_DBL, 
                    block_simple_global_to_local, 
                    block_simple_local_to_global, local_buffer, 
                    local_count, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, 
                    &gds));
                ASSERT_NOT_NULL_FATAL(gds);
                if (comm_rank == 0) {
                    global_count = mat_dims[0] * mat_dims[1];
                    src = (double *) malloc(global_count * sizeof(*src));
                    dst = (double *) calloc(global_count, sizeof(*dst));
                    for (i = 0; i < global_count; ++i) {
                        src[i] = 0.1 * i;
                    }
                    hi_ind[0] = mat_dims[0] - 1;
                    hi_ind[1] = mat_dims[1] - 1;
                    ld[0] = mat_dims[mat_minor_axis]; 
                    ASSERT_STAT_FATAL("GDS_put",
                        GDS_put(src, ld, lo_ind, hi_ind, gds));
                }

                GDS_fence(gds);
                if (comm_rank == 0) {
                    ASSERT_STAT_FATAL("GDS_get",
                        GDS_get(dst, ld, lo_ind, hi_ind, gds));
                    GDS_wait(gds);
                    for (i = 0; i < global_count; ++i) {
                        ASSERT_EQUAL_INT(src[i], dst[i]);
                    }
                    free(src);
                    free(dst);
                }
                GDS_wait(gds);
                ASSERT_STAT("GDS_free", GDS_free(&gds)); 
                MPI_Info_free(&info);
            }
        }
    }
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_create() - Version increment"
void test_gds_create_version_inc(void)
{
    int i, coli, rowi, orderi;
    double *local_buffer, *src = NULL, *dst = NULL;
    GDS_size_t local_count, global_count = 0;
    MPI_Info info;
    GDS_gds_t gds;
    GDS_size_t dims_local[2];
    GDS_size_t lo_ind[2] = {0, 0}; // starting at 0,0 for both tests
    GDS_size_t hi_ind[2]; //ending at some arbitrary index
    GDS_size_t ld[1];
    GDS_size_t dims_per_proc[N_DIM_OPTS] = {1, 7, 16};
    char * orders[2] = {GDS_ORDER_COL_MAJOR, GDS_ORDER_ROW_MAJOR};
    int comm_size; 
    int comm_rank;
    
    ASSERT_TEST_START(TEST_NAME);

    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    GDS_comm_rank(GDS_COMM_WORLD, &comm_rank);

    for (orderi = 0; orderi <=1; ++orderi) {
        mat_major_axis = 1 - orderi;
        mat_minor_axis = 1 - mat_major_axis;
        for (rowi = 0; rowi < N_DIM_OPTS; ++rowi) {
            dims_local[0] = dims_per_proc[rowi];
            mat_dims[0] = orderi == 0 ? dims_local[0] :
                                        dims_local[0] * comm_size;
            for (coli = 0; coli < 2; ++coli) {
                dims_local[1] = dims_per_proc[coli];
                mat_dims[1] = orderi == 0 ? dims_local[1] * comm_size :
                                            dims_local[1];
                local_count = dims_local[0] * dims_local[1];
                local_buffer = (double *) malloc(local_count *
                                                 sizeof(*local_buffer));
                MPI_Info_create(&info);
                MPI_Info_set(info, GDS_ORDER_KEY, orders[orderi]);
                ASSERT_STAT_FATAL("GDS_create",
                    GDS_create(2, mat_dims, GDS_DATA_DBL, 
                    block_simple_global_to_local, 
                    block_simple_local_to_global, local_buffer, 
                    local_count, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, 
                    &gds));
                ASSERT_NOT_NULL_FATAL(gds);
                if (comm_rank == 0) {
                    global_count = mat_dims[0] * mat_dims[1];
                    src = (double *) malloc(global_count * sizeof(*src));
                    dst = (double *) calloc(global_count, sizeof(*dst));
                    for (i = 0; i < global_count; ++i) {
                        src[i] = 0.1 * i;
                    }
                    hi_ind[0] = mat_dims[0] - 1;
                    hi_ind[1] = mat_dims[1] - 1;
                    ld[0] = mat_dims[mat_minor_axis]; 
                    ASSERT_STAT_FATAL("GDS_put",
                        GDS_put(src, ld, lo_ind, hi_ind, gds));
                }

                ASSERT_STAT_FATAL("GDS_version_inc",
                    GDS_version_inc(gds, 1, "", 0));
                if (comm_rank == 0) {
                    ASSERT_STAT_FATAL("GDS_get",
                        GDS_get(dst, ld, lo_ind, hi_ind, gds));
                    GDS_wait(gds);
                    for (i = 0; i < global_count; ++i) {
                        ASSERT_EQUAL_INT(src[i], dst[i]);
                    }
                    free(src);
                    free(dst);
                }
                GDS_wait(gds);
                ASSERT_STAT("GDS_free", GDS_free(&gds)); 
                MPI_Info_free(&info);
            }
        }
    }
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_get_attr()"
void test_gds_get_attr(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {3, 4, 3};
    GDS_size_t min_chunk[3] = {0, 0, 0};
    int ndim = 3, flag;
   
    ASSERT_TEST_START(TEST_NAME);
    
    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);
    
    GDS_datatype_t type;
    ASSERT_STAT("GDS_get_attr", GDS_get_attr(gds, GDS_TYPE, &type, &flag));
    ASSERT_EQUAL_INT(type, GDS_DATA_INT);
    ASSERT_EQUAL_INT(flag, 1);

    GDS_flavor_t flavor;
    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_CREATE_FLAVOR, &flavor, &flag));
    ASSERT_EQUAL_INT(flavor, GDS_FLAVOR_ALLOC);
    ASSERT_EQUAL_INT(flag, 1);
    
    GDS_size_t ndims;
    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_NUMBER_DIMENSIONS, &ndims, &flag));
    ASSERT_EQUAL_INT(ndims, 3);
    ASSERT_EQUAL_INT(flag, 1);
   
    GDS_size_t *counts = malloc(sizeof(GDS_size_t) * ndims);
    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_COUNT, counts, &flag));
    ASSERT_EQUAL_SIZE_ARR(counts, cts, 3);
    ASSERT_EQUAL_INT(flag, 1);
    free(counts);
    
    GDS_size_t *chunk_size;
    chunk_size = malloc(sizeof(GDS_size_t) * ndims);
    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_CHUNK_SIZE, chunk_size, &flag));
    ASSERT_EQUAL_INT(flag, 1);
    free(chunk_size);
    
    ASSERT_STAT("GDS_free", GDS_free(&gds));
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_get_acc()"
void test_gds_get_acc(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo[] = {100}, hi[] = {100}, ld[] = {0};
    int acc_buf[] = {432};
    int get_buf[1] = {0}; 
    int res_buf = 0;

    ASSERT_TEST_START(TEST_NAME);
             
    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds)); 
    ASSERT_NOT_NULL_FATAL(gds);
    
    /* Each process should touch a separate cell */
    lo[0] = (GDS_size_t) myrank;
    hi[0] = (GDS_size_t) myrank;
    ASSERT_STAT_FATAL("GDS_put", GDS_put(acc_buf, ld, lo, hi, gds));
    GDS_fence(gds);
     
    ASSERT_STAT_FATAL("GDS_get_acc",
        GDS_get_acc(acc_buf, ld, &res_buf, ld, lo, hi, MPI_SUM, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(res_buf, 432);

    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buf, ld, lo, hi, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(get_buf[0], 864);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: GDS_access() - 1D array"
void test_gds_access_1d(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {2048 + 32 * nprocs}; /* ensure focus does not exceed the array bound */
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    GDS_size_t lo[1], hi[1], ld[] = {0}, focus[1];
    int acc_buf[] = {432};
    int get_buf[1] = {0}; 
    void *access_buf;
    GDS_access_handle_t access_handle;
    int stripe_size = cts[0] / nprocs;
    int stripe_r = cts[0] % nprocs;

    ASSERT_TEST_START(TEST_NAME);
             
    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds)); 
    ASSERT_NOT_NULL_FATAL(gds);
  
    /* 
     * Calculte stripe size exactly as GVR's internal partition:
     * A size of N array evenly distributed on P processes, 
     * if there is a reminder, i.e., stripe_r, then the first stripe_r processes
     * will actually have a buffer of (stripe_size+1) size, the rest of processes
     * have a buffer of stripe_size. 
     */
    if (myrank < stripe_r) {    /* the first stripe_r processes */
        lo[0] = myrank + myrank * stripe_size;
        hi[0] = lo[0] + stripe_size;
    } else {                    /* from stripe_r+1 process */
        lo[0] = myrank * stripe_size + stripe_r; /* already shifted right to stripe_r offset */
        hi[0] = lo[0] + stripe_size - 1;
    }
    
    /* Each process should touch a separate cell */
    focus[0] = lo[0] + 5;
    ASSERT_STAT_FATAL("GDS_put", GDS_put(acc_buf, ld, focus, focus, gds));
    GDS_fence(gds);

    ASSERT_STAT("GDS_access", GDS_access(gds, lo, hi,
        GDS_ACCESS_BUFFER_DIRECT, &access_buf, &access_handle));
    
    int *ptr = (int *) access_buf;
    ASSERT_EQUAL_INT(*(ptr + 5), 432); /* read local buffer */
    *(ptr + 5) += 7; /* write local buffer */
    *(ptr + 17) = 433; /* write local buffer */
    ASSERT_STAT("GDS_release", GDS_release(access_handle));
    
    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buf, ld, focus, focus, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(get_buf[0], 439);
    
    focus[0] = lo[0] + 17;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buf, ld, focus, focus, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(get_buf[0], 433);
   
    ASSERT_STAT("GDS_access", GDS_access(gds, lo, hi,
        GDS_ACCESS_BUFFER_COPY, &access_buf, &access_handle));

    ptr = (int *) access_buf;
    ASSERT_EQUAL_INT(*(ptr + 5), 439); /* read local buffer */
    *(ptr + 5) += 1; /* write local buffer */
    *(ptr + 17) = 434; /* write local buffer */
    ASSERT_STAT("GDS_release", GDS_release(access_handle));
  
    focus[0] = lo[0] + 5;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buf, ld, focus, focus, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(get_buf[0], 440);
    
    focus[0] = lo[0] + 17;
    ASSERT_STAT_FATAL("GDS_get", GDS_get(get_buf, ld, focus, focus, gds));
    GDS_fence(gds);
    ASSERT_EQUAL_INT(get_buf[0], 434);
    
    ASSERT_STAT("GDS_free", GDS_free(&gds));

    ASSERT_TEST_END();
}

#undef TEST_NAME
#define TEST_NAME "API: local error"
#define LET_A 2948029
#define LET_STR "May the force be with you..."
static GDS_error_attr_key_t LET_ATTR_N;
static GDS_error_attr_key_t LET_ATTR_S;
static int let_shared_a;

static GDS_status_t local_error_recovery(GDS_gds_t gds, GDS_error_t desc)
{
    int flag;
    long a;
    GDS_size_t len;
    char buff[32];

    GDS_get_error_attr(desc, LET_ATTR_N, &a, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(a, LET_A);

    GDS_get_error_attr_len(desc, LET_ATTR_N, &len, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(int64_t));

    GDS_get_error_attr(desc, LET_ATTR_S, buff, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(strcmp(buff, LET_STR), 0);

    GDS_get_error_attr_len(desc, LET_ATTR_S, &len, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(LET_STR));

    let_shared_a = a;

    GDS_resume_local(gds, desc);

    return GDS_STATUS_OK;
}

void test_local_error(void)
{
    long a = LET_A;
    char str[] = LET_STR;
    GDS_gds_t gds;
    GDS_size_t count[] = { 128 * nprocs };
    GDS_error_pred_t pred;
    GDS_error_pred_term_t term;

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(1, count, NULL, GDS_DATA_INT, GDS_PRIORITY_LOW,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));

    GDS_error_t desc;

    ASSERT_STAT("GDS_define_error_attr_key",
                GDS_define_error_attr_key("LET_ATTR_S", strlen("LET_ATTR_S"),
                                          GDS_EAVTYPE_BYTE_ARRAY, &LET_ATTR_S));
    ASSERT_STAT("GDS_define_error_attr_key",
                GDS_define_error_attr_key("LET_ATTR_N", strlen("LET_ATTR_N"),
                                          GDS_EAVTYPE_INT, &LET_ATTR_N));

    ASSERT_STAT_FATAL("GDS_create_error_pred",
                      GDS_create_error_pred(&pred));

    ASSERT_STAT_FATAL("GDS_create_error_pred_term",
                      GDS_create_error_pred_term(LET_ATTR_S, GDS_EMEXP_ANY,
                                                 0, NULL, &term));
    ASSERT_NOT_NULL_FATAL(term);
    ASSERT_STAT("GDS_add_error_pred_term",
                GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT("GDS_create_error_pred_term",
                GDS_create_error_pred_term(LET_ATTR_N, GDS_EMEXP_ANY,
                                           0, NULL, &term));
    ASSERT_NOT_NULL_FATAL(term);
    ASSERT_STAT("GDS_add_error_pred_term",
                GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT("GDS_register_local_error_handler",
        GDS_register_local_error_handler(gds, pred, local_error_recovery));
    ASSERT_STAT("GDS_free_error_pred",
                GDS_free_error_pred(&pred));

    ASSERT_STAT_FATAL("GDS_create_error_descriptor",
                      GDS_create_error_descriptor(&desc));
    ASSERT_STAT("GDS_add_error_attr",
                GDS_add_error_attr(desc, LET_ATTR_S, sizeof(str), str));
    ASSERT_STAT("GDS_add_error_attr",
                GDS_add_error_attr(desc, LET_ATTR_N, sizeof(a), &a));

    ASSERT_STAT("GDS_raise_local_error",
                GDS_raise_local_error(gds, desc));

    ASSERT_EQUAL_INT(let_shared_a, LET_A);

    GDS_free(&gds);

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: local error hanlder"
static GDS_status_t local_error_recovery2(GDS_gds_t gds, GDS_error_t desc)
{
    int flag;
    long a;
    GDS_size_t len;
    char buff[32];

    GDS_get_error_attr(desc, LET_ATTR_N, &a, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(a, LET_A);

    GDS_get_error_attr_len(desc, LET_ATTR_N, &len, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(int64_t));

    GDS_get_error_attr(desc, LET_ATTR_S, buff, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(strcmp(buff, LET_STR), 0);

    GDS_get_error_attr_len(desc, LET_ATTR_S, &len, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(len, sizeof(LET_STR));

    let_shared_a = a;

    GDS_resume_local(gds, desc);

    return GDS_STATUS_OK;
}

void set_error_handler(GDS_gds_t gds)
{
    GDS_error_pred_t pred;
    GDS_error_pred_term_t term;

    ASSERT_STAT("GDS_define_error_attr_key",
                GDS_define_error_attr_key("LET_ATTR_S", strlen("LET_ATTR_S"),
                                          GDS_EAVTYPE_BYTE_ARRAY, &LET_ATTR_S));
    ASSERT_STAT("GDS_define_error_attr_key",
                GDS_define_error_attr_key("LET_ATTR_N", strlen("LET_ATTR_N"),
                                          GDS_EAVTYPE_INT, &LET_ATTR_N));

    ASSERT_STAT_FATAL("GDS_create_error_pred",
                      GDS_create_error_pred(&pred));

    ASSERT_STAT_FATAL("GDS_create_error_pred_term",
                      GDS_create_error_pred_term(LET_ATTR_S, GDS_EMEXP_ANY,
                                                 0, NULL, &term));
    ASSERT_NOT_NULL_FATAL(term);
    ASSERT_STAT("GDS_add_error_pred_term",
                GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT("GDS_create_error_pred_term",
                GDS_create_error_pred_term(LET_ATTR_N, GDS_EMEXP_ANY,
                                           0, NULL, &term));
    ASSERT_NOT_NULL_FATAL(term);
    ASSERT_STAT("GDS_add_error_pred_term",
                GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT("GDS_register_local_error_handler",
        GDS_register_local_error_handler(gds, pred, local_error_recovery2));
    ASSERT_STAT("GDS_free_error_pred",
                GDS_free_error_pred(&pred));
}

void test_gds_set_error_handler(void)
{
    long a = LET_A;
    char str[] = LET_STR;
    GDS_gds_t gds;
    GDS_size_t count[] = { 128 * nprocs };
    GDS_error_t desc;

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(1, count, NULL, GDS_DATA_INT, GDS_PRIORITY_LOW,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    set_error_handler(gds);

    ASSERT_STAT_FATAL("GDS_create_error_descriptor",
                      GDS_create_error_descriptor(&desc));
    ASSERT_STAT("GDS_add_error_attr",
                GDS_add_error_attr(desc, LET_ATTR_S, sizeof(str), str));
    ASSERT_STAT("GDS_add_error_attr",
                GDS_add_error_attr(desc, LET_ATTR_N, sizeof(a), &a));

    ASSERT_STAT("GDS_raise_local_error",
                GDS_raise_local_error(gds, desc));

    ASSERT_EQUAL_INT(let_shared_a, LET_A);

    GDS_free(&gds);
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "API: version traverse"
#define NELEMS(a) (sizeof(a)/sizeof(a[0]))
void test_gds_ver_traverse(void)
{
    GDS_gds_t gds;
    const size_t arraylen = 1024;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, i;
    int testdata[4], buf[4];
    GDS_size_t lo[] = {0}, hi[] = {3}, ld[] = {};

    GDS_size_t versions[] = {0, 1, 2, 5, 7, 10, 11};
    GDS_size_t lastver = versions[NELEMS(versions)-1];

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, 
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    /* create versions */
    for (i = 1; i < NELEMS(versions); i++) {
        int inc = versions[i] - versions[i-1];
        ASSERT_STAT("GDS_version_inc", GDS_version_inc(gds, inc, NULL, 0));
        ASSERT_EQUAL_INT(GDS_version(gds), versions[i]);
        if (myrank == 0) {
            testdata[0] = versions[i];
            testdata[1] = versions[i];
            testdata[2] = versions[i];
            testdata[3] = versions[i];
            ASSERT_STAT_FATAL("GDS_put", GDS_put(testdata, ld, lo, hi, gds));
        }
    }

    ASSERT_STAT("GDS_version_dec", GDS_version_dec(gds, lastver));
    ASSERT_EQUAL_INT(GDS_version(gds), 0);

    ASSERT_STAT("GDS_move_to_newest", GDS_move_to_newest(gds));
    ASSERT_EQUAL_INT(GDS_version(gds), lastver);

    /* Go backward */
    for (i = NELEMS(versions)-1;;i--) {
        if (i > 0) {
            ASSERT_STAT("GDS_move_to_prev", GDS_move_to_prev(gds));
            ASSERT_EQUAL_INT(GDS_version(gds), versions[i-1]);
            memset(buf, 0, sizeof(int) * 4);
            testdata[0] = versions[i-1];
            testdata[1] = versions[i-1];
            testdata[2] = versions[i-1];
            testdata[3] = versions[i-1];
            ASSERT_STAT_FATAL("GDS_get",
                    GDS_get(buf, ld, lo, hi, gds));
            GDS_fence(gds);
            ASSERT_EQUAL_INT(memcmp(buf, testdata, 4), 0);
            printf("b%d: buf[0]: %d, buf[1]: %d, buf[2]: %d, buf[3]: %d"
                   " vs. testdata[0]: %d, testdata[1]: %d, "
                   "testdata[2]: %d, testdata[3]: %d\n", i-1,
                   buf[0], buf[1], buf[2], buf[3], 
                   testdata[0], testdata[1], testdata[2], testdata[3]);
        }
        else {
            ASSERT_EQUAL_INT(GDS_move_to_prev(gds), GDS_STATUS_INVALID);
            ASSERT_EQUAL_INT(GDS_version(gds), 0);
            break;
        }
    }

    /* Go forward */
    for (i = 0; ;i++) {
        if (i < NELEMS(versions)-1) {
            ASSERT_STAT("GDS_move_to_next", GDS_move_to_next(gds));
            ASSERT_EQUAL_INT(GDS_version(gds), versions[i+1]);
            memset(buf, 0, sizeof(int) * 4);
            testdata[0] = versions[i+1];
            testdata[1] = versions[i+1];
            testdata[2] = versions[i+1];
            testdata[3] = versions[i+1];
            ASSERT_STAT_FATAL("GDS_get",
                    GDS_get(buf, ld, lo, hi, gds));
            GDS_fence(gds);
            ASSERT_EQUAL_INT(memcmp(buf, testdata, 4), 0);
        }
        else {
            ASSERT_EQUAL_INT(GDS_move_to_next(gds), GDS_STATUS_INVALID);
            ASSERT_EQUAL_INT(GDS_version(gds), lastver);
            break;
        }
    }

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    ASSERT_TEST_END();
}
