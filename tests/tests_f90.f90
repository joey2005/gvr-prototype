!
!  Global View Resilience (GVR)
!  http://gvr.cs.uchicago.edu
!
!  Copyright (C) 2014 University of Chicago.
!  See license.txt in top-level directory.
!
! Testsuite for GVR Fortran Binding
!

! Auxilary functions
MODULE GLOBAL
  INTEGER, PARAMETER :: NARRAYS = 10
  INTEGER :: provd_support, nprocs, rank
  INTEGER :: n_assert = 0, ok_assert = 0
  LOGICAL :: assert_ng_only = .TRUE.

  CONTAINS
  SUBROUTINE ASSERT_STAT(func, stat)
    USE GDS, ONLY : GDS_STATUS_OK
    IMPLICIT NONE
    CHARACTER(len=*), INTENT(IN) :: func
    INTEGER, INTENT(IN) :: stat

    n_assert = n_assert + 1

    IF ( stat == GDS_STATUS_OK ) THEN
      ok_assert = ok_assert + 1
      IF ( .NOT. assert_ng_only ) THEN
        WRITE(*, '(I4, A, A, A)') rank, ':', func, ':GDS_STATUS_OK:OK'
      END IF
    ELSE 
      WRITE(*, '(I4, A, A, A, I0, A)') rank, ':', func, &
        ':GDS_STATUS_OK:NG(', stat, ')'
    END IF
  END SUBROUTINE

  SUBROUTINE ASSERT_COND(func, test, cond)
    IMPLICIT NONE
    CHARACTER(len=*), INTENT(IN) :: func
    CHARACTER(len=*), INTENT(IN) :: test
    LOGICAL, INTENT(IN) :: cond

    n_assert = n_assert + 1
    IF ( cond ) THEN
      ok_assert = ok_assert + 1
      IF ( .NOT. assert_ng_only ) THEN
        WRITE(*, '(I4, A, A, A, A, A)') rank, ':', func, ':', test, ':OK'
      END IF
    ELSE
      WRITE(*, '(I4, A, A, A, A, A)') rank, ':', func, ':', test, ':NG'
    END IF
  END SUBROUTINE

  SUBROUTINE ASSERT_REPORT(nerr)
    USE MPI
    IMPLICIT NONE
    INTEGER, INTENT(OUT) :: nerr
    INTEGER, PARAMETER :: STDIN = 5
    INTEGER, PARAMETER :: STDOUT = 6
    INTEGER, PARAMETER :: STDERR = 0
    INTEGER :: ierr

    IF ( rank == 0 ) THEN
      WRITE(*, '(A)') '----------------------------------'
      WRITE(*, '(A, A10, A10, A10)') 'RANK', 'TOTAL', 'OK', 'NG'
    END IF
    
    flush(STDOUT)
    flush(STDERR)
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
   
    nerr = n_assert - ok_assert
    WRITE(*, '(I4, I10, I10, I10)') rank, n_assert, ok_assert, nerr
  END SUBROUTINE

  LOGICAL FUNCTION ARR_EQ(arr1, arr2, n)
    IMPLICIT NONE
    INTEGER, DIMENSION(*), INTENT(IN) :: arr1
    INTEGER, DIMENSION(*), INTENT(IN) :: arr2
    INTEGER, INTENT(IN) :: n
    INTEGER :: i

    DO i = 1, n
      ARR_EQ = arr1(i) == arr2(i)
      IF ( .NOT. ARR_EQ ) THEN
        RETURN
      END IF
    END DO
    RETURN
  END FUNCTION
  
  LOGICAL FUNCTION ARR_INT8_EQ(arr1, arr2, n)
    IMPLICIT NONE
    INTEGER(8), DIMENSION(*), INTENT(IN) :: arr1
    INTEGER(8), DIMENSION(*), INTENT(IN) :: arr2
    INTEGER(8), INTENT(IN) :: n
    INTEGER(8) :: i

    DO i = 1, n
      ARR_INT8_EQ = (arr1(i) == arr2(i))
      IF ( .NOT. ARR_INT8_EQ ) THEN
        RETURN
      END IF
    END DO
    RETURN
  END FUNCTION
  
  LOGICAL FUNCTION ARR_REAL8_EQ(arr1, arr2, n)
    IMPLICIT NONE
    REAL(8), DIMENSION(*), INTENT(IN) :: arr1
    REAL(8), DIMENSION(*), INTENT(IN) :: arr2
    INTEGER, INTENT(IN) :: n
    INTEGER :: i

    DO i = 1, n
      ARR_REAL8_EQ = arr1(i) == arr2(i)
      IF ( .NOT. ARR_REAL8_EQ ) THEN
        RETURN
      END IF
    END DO
    RETURN
  END FUNCTION
  
  LOGICAL FUNCTION STR_EQ(str1, str2, n)
    IMPLICIT NONE
    CHARACTER, DIMENSION(*), INTENT(IN) :: str1
    CHARACTER, DIMENSION(*), INTENT(IN) :: str2
    INTEGER(8), INTENT(IN) :: n
    INTEGER(8) :: i

    DO i = 1, n
      STR_EQ = str1(i) == str2(i)
      IF ( .NOT. STR_EQ ) THEN
        RETURN
      END IF
    END DO
    RETURN
  END FUNCTION

  SUBROUTINE BARRIER(msg)
    USE MPI
    IMPLICIT NONE
    INTEGER, PARAMETER :: STDIN = 5
    INTEGER, PARAMETER :: STDOUT = 6
    INTEGER, PARAMETER :: STDERR = 0
  
    CHARACTER(len=*), INTENT(IN) :: msg

    INTEGER :: ierr
  
    flush(STDOUT)
    flush(STDERR)
    call MPI_Barrier(MPI_COMM_WORLD, ierr)
    IF (rank == 0) THEN
      write(*, '(A)') msg
    END IF
  END SUBROUTINE
    
  INTEGER(C_INT) FUNCTION LOCAL_ERROR_ATTRS_RECOVERY(g, desc) BIND(C)
    USE, INTRINSIC :: ISO_C_BINDING
    USE GDS
    TYPE(C_PTR), VALUE, INTENT(IN) :: g
    TYPE(C_PTR), VALUE, INTENT(IN) :: desc
    INTEGER :: stat
        
    WRITE(*, *), "LOCAL ERROR ATTR RECOVERY"
    call GDS_RESUME_LOCAL(g, desc, stat)
    call ASSERT_STAT('GDS_RESUME_LOCAL', stat)
    LOCAL_ERROR_ATTRS_RECOVERY = 0
  END FUNCTION

  SUBROUTINE SET_HANDLER(g)
    USE, INTRINSIC :: ISO_C_BINDING
    USE GDS
    IMPLICIT NONE
    TYPE(C_PTR), VALUE, INTENT(IN) :: g
    TYPE(C_PTR) :: pred, term
    INTEGER :: stat

    call GDS_CREATE_ERROR_PRED(pred, stat)
    call ASSERT_STAT('GDS_CREATE_ERROR_PRED', stat)

    call GDS_CREATE_ERROR_PRED_TERM(GDS_EATTR_GDS_INDEX, GDS_EMEXP_ANY, 0_8, &
      C_NULL_PTR, term, stat)
    call ASSERT_STAT('GDS_CREATE_ERROR_PRED_TERM', stat)

    call GDS_ADD_ERROR_PRED_TERM(pred, term, stat)
    call ASSERT_STAT('GDS_ADD_ERROR_PRED_TERM', stat)
    call GDS_FREE_ERROR_PRED_TERM(term, stat)

    call GDS_REGISTER_LOCAL_ERROR_HANDLER(g, pred, &
      C_FUNLOC(LOCAL_ERROR_ATTRS_RECOVERY), stat)
    call ASSERT_STAT('GDS_REGISTER_LOCAL_ERROR_HANDLER', stat)
    call GDS_FREE_ERROR_PRED(pred, stat)
    call ASSERT_STAT('GDS_FREE_ERROR_PRED', stat)
  END SUBROUTINE
END MODULE

PROGRAM TESTS_F90
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE

  INTEGER :: stat, nerr
  
  call GDS_INIT(GDS_THREAD_SINGLE, provd_support, stat)
  call GDS_COMM_SIZE(GDS_COMM_WORLD, nprocs, stat)
  call GDS_COMM_RANK(GDS_COMM_WORLD, rank, stat)
  call ASSERT_STAT('GDS_COMM_RANK', stat)
  call BARRIER('Starting...')

  ! API Tests
  call BARRIER('API: GDS_GET_VERSION_LABEL() testing...')
  call TEST_GDS_GET_VERSION_LABEL()

  call BARRIER('API: GDS_CREATE testing...')
  call TEST_GDS_CREATE_SMALL_RW()

  call BARRIER('API: GDS_GET_ATTR testing...')
  call TEST_GDS_GET_ATTR()
  
  call BARRIER('API: GDS_GET_ACC testing...')
  call TEST_GDS_GET_ACC()

  call BARRIER('API: GDS_ACCESS testing...')
  call TEST_GDS_ACCESS()
  
  call BARRIER('API: Local error attrs testing...')
  call TEST_LOCAL_ERROR_ATTRS()
  
  call BARRIER('API: Local error testing...')
  call TEST_LOCAL_ERROR()

  call BARRIER('Single alloc testing...')
  call SINGLE_ALLOC_TEST()

  call BARRIER('Small array testing...')
  call SMALL_ARRAY_RW_TEST()

  call BARRIER('Small array accumulate testing...')
  call SMALL_ARRAY_ACC_TEST()
  
  call BARRIER('Basic 2D-array read/write testing...')
  call BASIC_2D_ARRAY_RW_TEST()
  
  call BARRIER('Array 2D row testing...')
  call ARRAY_2D_ROWM_TEST()
  
  call BARRIER('Array 2D column testing...')
  call ARRAY_2D_COLM_TEST()
  
  call BARRIER('Array 3D row testing...')
  call ARRAY_3D_ROWM_TEST()
  
  call BARRIER('Array 3D column testing...')
  call ARRAY_3D_COLM_TEST()
  
  call BARRIER('Multi alloc testing...')
  call MULTI_ALLOC_TEST()
  
  call BARRIER('Single RW testing...')
  call SINGLE_RW_TEST()
  
  call BARRIER('Stride testing...')
  call STRIDE_TEST()
  
  call BARRIER('IAXPY testing...')
  call IAXPY_TEST()
  
  call BARRIER('Version traverse testing...')
  call VER_TRAVERSE_TEST()

  call BARRIER('Clone testing...')
  call CLONE_TEST()
  
  call BARRIER('GVR 2D testing...')
  call GVR_2D_TEST()

  call BARRIER('Large matrix testing...')
  call LARGE_MATRIX_TEST()

  call BARRIER('MPI derived type testing...')
  call MPI_DERIVED_TYPE_TEST()
  
  call BARRIER('Reporting...')
  call ASSERT_REPORT(nerr)
  
  call BARRIER('Finalizing...')
  call GDS_FINALIZE(stat)

  IF ( nerr /= 0) THEN
    call EXIT(1)
  END IF
END PROGRAM TESTS_F90

! API Tests
SUBROUTINE TEST_GDS_GET_VERSION_LABEL()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  
  TYPE(C_PTR) :: g
  INTEGER(8) :: ndim, cts(1), min_chunk(1), lo(1), hi(1), ld(1)
  INTEGER, TARGET :: put_buf, get_buf
  CHARACTER(LEN=11) :: ver1, ver2
  CHARACTER, POINTER :: label(:)
  INTEGER(8) :: label_size
  INTEGER :: stat

  g = C_NULL_PTR
  cts(1) = 1024
  ndim = 1
  min_chunk = 0
  lo(1) = 0
  hi(1) = 0
  ld(1) = 0
  put_buf = 432
  get_buf = 0
  ver1 = "version one"
  ver2 = "version two"

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  call GDS_PUT(C_LOC(put_buf), ld, lo, hi, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call ASSERT_COND('GDS_PUT', 'put_buf', put_buf == 432)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  
  call GDS_VERSION_INC(g, 1_8, ver1, 11_8, stat)
  call ASSERT_STAT('GDS_VERSION_INC', stat)

  put_buf = 433
  call GDS_PUT(C_LOC(put_buf), ld, lo, hi, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  
  call GDS_VERSION_INC(g, 2_8, ver2, 11_8, stat)
  call ASSERT_STAT('GDS_VERSION_INC', stat)
  
  call GDS_GET_VERSION_LABEL(g, label, label_size, stat)
  call ASSERT_STAT('GDS_GET_VERSION_LABEL', stat)
  call ASSERT_COND('GDS_GET_VERSION_LABEL', 'LABEL', &
    STR_EQ(label, ver2, label_size))

  call GDS_MOVE_TO_PREV(g, stat)
  call ASSERT_STAT('GDS_MOVE_TO_PREV', stat)
  
  call GDS_GET_VERSION_LABEL(g, label, label_size, stat)
  call ASSERT_STAT('GDS_GET_VERSION_LABEL', stat)
  call ASSERT_COND('GDS_GET_VERSION_LABEL', 'LABEL', &
    STR_EQ(label, ver1, label_size))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE
  

SUBROUTINE TEST_GDS_CREATE_SMALL_RW()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  
  TYPE(C_PTR) :: g
  INTEGER(8) :: ndim, lo_idx(1), hi_idx(1), ld(1)
  INTEGER(8) :: len_local, vec_len(1)
  INTEGER, TARGET :: put_buf, get_buf
  INTEGER :: myrank, mysize, stat
  TYPE(C_FUNPTR) :: fptr_gl, fptr_lg
  INTEGER, ALLOCATABLE, TARGET :: local_buffer(:)
  
  g = C_NULL_PTR
  ndim = 1
  ld(1) = 0
  lo_idx(1) = 0
  hi_idx(1) = 0
  put_buf = 432
  get_buf = 0
  
  len_local = 1

  call GDS_COMM_RANK(GDS_COMM_WORLD, myrank, stat);
  call GDS_COMM_SIZE(GDS_COMM_WORLD, mysize, stat);
  
  lo_idx(1) = myrank
  hi_idx(1) = myrank
  put_buf = 432 + myrank
  vec_len = len_local * mysize
  ALLOCATE(local_buffer(len_local))

  fptr_gl = C_FUNLOC(VECTOR_GLOBAL_TO_LOCAL)
  fptr_lg = C_FUNLOC(VECTOR_LOCAL_TO_GLOBAL)
  call GDS_CREATE(ndim, vec_len, GDS_DATA_INT, fptr_gl, &
    fptr_lg, C_LOC(local_buffer), len_local, &
    GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_CREATE', stat)

  call GDS_PUT(C_LOC(put_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  
  call GDS_GET(C_LOC(get_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  call ASSERT_COND('GDS_GET', 'get_buf', put_buf == get_buf)

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
  
  CONTAINS
#if defined(__INTEL_COMPILER)
  INTEGER(C_INT) FUNCTION VECTOR_GLOBAL_TO_LOCAL(global_indices, local_rank, &
    local_offset)
#else
  INTEGER(C_INT) FUNCTION VECTOR_GLOBAL_TO_LOCAL(global_indices, local_rank, &
    local_offset) BIND(C)
#endif
    USE, INTRINSIC :: ISO_C_BINDING
    INTEGER(C_SIZE_T), DIMENSION(*), INTENT(IN) :: global_indices
    INTEGER(C_INT), INTENT(OUT) :: local_rank
    INTEGER(C_SIZE_T), INTENT(OUT) :: local_offset
    INTEGER :: comm_size, cell_per_proc, stat

    call GDS_COMM_SIZE(GDS_COMM_WORLD, comm_size, stat)
    cell_per_proc = INT(vec_len(1)) / comm_size
    local_rank = INT(global_indices(1)) / cell_per_proc
    local_offset = global_indices(1) - (local_rank * cell_per_proc)
    
    VECTOR_GLOBAL_TO_LOCAL = INT(GDS_STATUS_OK)
  END FUNCTION VECTOR_GLOBAL_TO_LOCAL
 
#if defined(__INTEL_COMPILER)
  INTEGER(C_INT) FUNCTION VECTOR_LOCAL_TO_GLOBAL(local_offset, global_indices)
#else
  INTEGER(C_INT) FUNCTION VECTOR_LOCAL_TO_GLOBAL(local_offset, global_indices) BIND(C)
#endif
    USE, INTRINSIC :: ISO_C_BINDING
    INTEGER(C_INT), VALUE, INTENT(IN) :: local_offset
    INTEGER(C_SIZE_T), DIMENSION(*), INTENT(OUT) :: global_indices
    INTEGER :: comm_size, comm_rank, cell_per_proc, stat

    call GDS_COMM_SIZE(GDS_COMM_WORLD, comm_size, stat)
    call GDS_COMM_RANK(GDS_COMM_WORLD, comm_rank, stat)
    cell_per_proc = INT(vec_len(1)) / comm_size
    global_indices(1) = local_offset + comm_rank * cell_per_proc
     
    VECTOR_LOCAL_TO_GLOBAL = INT(GDS_STATUS_OK)
  END FUNCTION VECTOR_LOCAL_TO_GLOBAL
END SUBROUTINE

SUBROUTINE TEST_GDS_GET_ATTR()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g
  INTEGER(8) :: cts(3), min_chunk(3), ndim = 3
  INTEGER, TARGET :: datatype, flavor
  INTEGER :: flag, stat
  INTEGER(8), TARGET :: ndims
  INTEGER(8), ALLOCATABLE, TARGET :: counts(:), chunk_size(:)

  g = C_NULL_PTR
  cts = (/3, 4, 3/)
  min_chunk = (/0, 0, 0/)
  ndim = 3

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  call GDS_GET_ATTR(g, GDS_TYPE, C_LOC(datatype), flag, stat)
  call ASSERT_STAT('GDS_GET_ATTR', stat)
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_TYPE', datatype == GDS_DATA_INT)
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_TYPE', flag == 1)
  
  call GDS_GET_ATTR(g, GDS_CREATE_FLAVOR, C_LOC(flavor), flag, stat)
  call ASSERT_STAT('GDS_GET_ATTR', stat)
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_CREATE_FLAVOR', &
    flavor == GDS_FLAVOR_ALLOC)
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_CREATE_FLAVOR', flag == 1)
  
  call GDS_GET_ATTR(g, GDS_NUMBER_DIMENSIONS, C_LOC(ndims), flag, stat)
  call ASSERT_STAT('GDS_GET_ATTR', stat)
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_NUMBER_DIMENSIONS', ndims == ndim)
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_NUMBER_DIMENSIONS', flag == 1)

  ALLOCATE(counts(ndims))
  call GDS_GET_ATTR(g, GDS_COUNT, C_LOC(counts), flag, stat)
  call ASSERT_STAT('GDS_GET_ATTR', stat)
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_COUNT', ARR_INT8_EQ(cts, counts, ndim))
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_COUNT FLAG', flag == 1)
  DEALLOCATE(counts)
  
  ALLOCATE(chunk_size(ndims))
  call GDS_GET_ATTR(g, GDS_CHUNK_SIZE, C_LOC(chunk_size), flag, stat)
  call ASSERT_STAT('GDS_GET_ATTR', stat)
  call ASSERT_COND('GDS_GET_ATTR', 'GDS_CHUNK_SIZE', flag == 1)
  DEALLOCATE(chunk_size)

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE TEST_GDS_GET_ACC()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g
  INTEGER(8) :: ndim, lo(1), hi(1), ld(1), cts(1), min_chunk(1)
  INTEGER, TARGET :: acc_buf, get_buf, res_buf
  INTEGER :: stat

  g = C_NULL_PTR
  cts = (/1024/)
  min_chunk = (/0/)
  ndim = 1
  ld(1) = 0
  acc_buf = 432
  get_buf = 0
  res_buf = 0

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  lo(1) = rank;
  hi(1) = rank;
  call GDS_PUT(C_LOC(acc_buf), ld, lo, hi, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_FENCE(g, stat)
  
  call GDS_GET_ACC(C_LOC(acc_buf), ld, C_LOC(res_buf), &
    ld, lo, hi, MPI_SUM, g, stat)
  call ASSERT_STAT('GDS_GET_ACC', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_COND('GDS_GET_ACC', 'res_buf', res_buf == 432)

  call GDS_GET(C_LOC(get_buf), ld, lo, hi, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_COND('GDS_GET', 'get_buf', get_buf == 864)

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE TEST_GDS_ACCESS()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g, access_handle, access_buf_cptr
  INTEGER(8) :: ndim, lo(1), hi(1), focus(1), ld(1), cts(1), min_chunk(1)
  INTEGER, TARGET :: acc_buf, get_buf, res_buf
  INTEGER(8) :: stripe_size, stripe_r
  INTEGER :: stat
  INTEGER, POINTER :: access_buf(:)

  g = C_NULL_PTR
  cts = (/1024/)
  min_chunk = (/0/)
  ndim = 1
  ld(1) = 0
  acc_buf = 432
  get_buf = 0
  res_buf = 0
  stripe_size = cts(1) / nprocs
  stripe_r = MOD(cts(1), nprocs)

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  IF (rank < stripe_r) THEN
    lo(1) = rank + rank * stripe_size
    hi(1) = lo(1) + stripe_size
  ELSE
    lo(1) = rank * stripe_size + stripe_r
    hi(1) = lo(1) + stripe_size - 1
  END IF
  
  focus(1) = lo(1) + 5
  call GDS_PUT(C_LOC(acc_buf), ld, focus, focus, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_FENCE(g, stat)

  call GDS_ACCESS(g, lo, hi, GDS_ACCESS_BUFFER_DIRECT, &
    access_buf_cptr, access_handle, stat)
  call ASSERT_STAT('GDS_ACCESS', stat)

  call C_F_POINTER(access_buf_cptr, access_buf, [stripe_size])
  call ASSERT_COND('GDS_ACCESS', 'access_buf', access_buf(6) == 432)

  access_buf(6) = access_buf(6) + 7
  access_buf(18) = 433

  call GDS_RELEASE(access_handle, stat)
  call ASSERT_STAT('GDS_RELEASE', stat)
 
  call GDS_GET(C_LOC(get_buf), ld, focus, focus, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_COND('GDS_GET', 'get_buf', get_buf == 439) 
  
  focus(1) = lo(1) + 17
  call GDS_GET(C_LOC(get_buf), ld, focus, focus, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_COND('GDS_GET', 'get_buf', get_buf == 433) 
  
  call GDS_ACCESS(g, lo, hi, GDS_ACCESS_BUFFER_COPY, &
    access_buf_cptr, access_handle, stat)
  call ASSERT_STAT('GDS_ACCESS', stat)

  call C_F_POINTER(access_buf_cptr, access_buf, [stripe_size])
  call ASSERT_COND('GDS_ACCESS', 'access_buf', access_buf(6) == 439)

  access_buf(6) = access_buf(6) + 1
  access_buf(18) = 434

  call GDS_RELEASE(access_handle, stat)
  call ASSERT_STAT('GDS_RELEASE', stat)
 
  focus(1) = lo(1) + 5
  call GDS_GET(C_LOC(get_buf), ld, focus, focus, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_COND('GDS_GET', 'get_buf', get_buf == 440) 
  
  focus(1) = lo(1) + 17
  call GDS_GET(C_LOC(get_buf), ld, focus, focus, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_COND('GDS_GET', 'get_buf', get_buf == 434) 

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE SINGLE_ALLOC_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  
  TYPE(C_PTR) :: g = C_NULL_PTR
  INTEGER(8) :: ndim, cts(1), min_chunk(1)
  INTEGER :: stat

  g = C_NULL_PTR
  cts(1) = 1024
  ndim = 1
  min_chunk(1) = 0

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  
  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE SMALL_ARRAY_RW_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  
  TYPE(C_PTR) :: g
  INTEGER(8) :: ndim, cts(1), min_chunk(1), lo_idx(1), hi_idx(1), ld(1)
  INTEGER, TARGET :: put_buf, get_buf
  INTEGER :: stat

  g = C_NULL_PTR
  ndim = 1
  cts(1) = 1
  min_chunk(1) = 0
  lo_idx(1) = 0
  hi_idx(1) = 0
  ld(1) = 0
  put_buf = 432
  get_buf = 0

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  call GDS_PUT(C_LOC(put_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call ASSERT_COND('GDS_PUT', 'put_buf', put_buf == 432)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  
  call GDS_GET(C_LOC(get_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  call ASSERT_COND('GDS_GET', 'get_buf', get_buf == 432)

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE SMALL_ARRAY_ACC_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  
  TYPE(C_PTR) :: g
  INTEGER(8) :: ndim, cts(1), min_chunk(1), lo_idx(1), hi_idx(1), ld(1)
  INTEGER, TARGET :: put_buf, acc_buf, get_buf
  INTEGER :: stat

  g = C_NULL_PTR
  ndim = 1
  cts(1) = 1
  min_chunk(1) = 0
  lo_idx(1) = 0
  hi_idx(1) = 0
  ld(1) = 0
  put_buf = 1
  acc_buf = 1
  get_buf = 0

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  call GDS_PUT(C_LOC(put_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call ASSERT_COND('GDS_PUT', 'put_buf', put_buf == 1)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_ACC(C_LOC(acc_buf), ld, lo_idx, hi_idx, MPI_SUM, g, stat)
  call ASSERT_STAT('GDS_ACC', stat)
  call ASSERT_COND('GDS_ACC', 'acc_buf', acc_buf == 1)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  
  call GDS_GET(C_LOC(get_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  call ASSERT_COND('GDS_GET', 'get_buf', get_buf == nprocs + 1)
 
  acc_buf = 2
  call GDS_ACC(C_LOC(acc_buf), ld, lo_idx, hi_idx, MPI_PROD, g, stat)
  call ASSERT_STAT('GDS_ACC', stat)
  call ASSERT_COND('GDS_ACC', 'acc_buf', acc_buf == 2)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  
  call GDS_GET(C_LOC(get_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  call ASSERT_COND('GDS_GET', 'get_buf', get_buf == (nprocs + 1) * (2 ** nprocs))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE BASIC_2D_ARRAY_RW_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  
  TYPE(C_PTR) :: g
  INTEGER(8) :: ndim, cts(2), min_chunk(2), lo_idx(2), hi_idx(2), ld(1)
  INTEGER, TARGET :: put_buf(4), get_buf(4)
  INTEGER :: stat

  g = C_NULL_PTR
  ndim = 2
  cts = (/2, 2/)
  min_chunk = (/0, 0/)
  lo_idx = (/0, 0/)
  hi_idx = (/1, 1/)
  ld(1) = 0
  put_buf = (/1, 2, 3, 4/)
  get_buf(4) = 0

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  call GDS_PUT(C_LOC(put_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  
  call GDS_GET(C_LOC(get_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  call ASSERT_COND('GDS_GET', 'ARR', ARR_EQ(put_buf, get_buf, 4))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE ARRAY_2D_ROWM_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g
  INTEGER(8), DIMENSION(1:2) :: cts
  INTEGER(8), DIMENSION(1:2) :: min_chunk 
  INTEGER(8) :: ndim = 2
  !INTEGER(8), DIMENSION(1:2) :: lo_idx, hi_idx, ld
  INTEGER(8) :: lo_idx(2), hi_idx(2)
  INTEGER(8) :: ld(2)
  INTEGER, TARGET :: buf(1:9) = 0
  INTEGER :: exp_buf(1:9)
  INTEGER :: i, j, info, ierr, stat

  g = C_NULL_PTR
  cts = (/4, 5/)
  min_chunk = (/0, 0/)
  ndim = 2

  call MPI_INFO_CREATE(info, ierr)
  call MPI_INFO_SET(info, GDS_ORDER_KEY, GDS_ORDER_ROW_MAJOR, ierr)
  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, info, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call MPI_INFO_FREE(info, ierr)

  ld(1) = 1
  DO j = 0, 4
    DO i = 0, 3
      buf(1) = 4 * j + i
      lo_idx = (/i, j/)
      hi_idx = (/i, j/)
      call GDS_PUT(C_LOC(buf), ld, lo_idx, hi_idx, g, stat)
      call ASSERT_STAT('GDS_PUT', stat)
      call GDS_WAIT_LOCAL(g, stat)
    END DO
  END DO
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  ld(1) = 3
  lo_idx = (/1, 1/)
  hi_idx = (/2, 3/)
  call GDS_GET(C_LOC(buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:6) = (/5, 9, 13, 6, 10, 14/)
  call ASSERT_COND('GDS_GET', 'ARRAY',  ARR_EQ(buf, exp_buf, 6))

  ld(1) = 2
  lo_idx = (/0, 3/)
  hi_idx = (/1, 4/)
  call GDS_GET(C_LOC(buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:4) = (/12, 16, 13, 17/)
  call ASSERT_COND('GDS_GET', 'ARRAY', ARR_EQ(buf, exp_buf, 4))

  ld(1) = 4
  lo_idx = (/1, 2/)
  hi_idx = (/2, 3/)
  buf(:) = -1
  call GDS_GET(C_LOC(buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:8) = (/9, 13, -1, -1, 10, 14, -1, -1/)
  call ASSERT_COND('GDS_GET', 'ARRAY', ARR_EQ(buf, exp_buf, 8))

  ld(1) = 5
  lo_idx = (/0, 0/)
  hi_idx = (/0, 4/)
  call GDS_GET(C_LOC(buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:5) = (/0, 4, 8, 12, 16/)
  call ASSERT_COND('GDS_GET', 'ARRAY', ARR_EQ(buf, exp_buf, 5))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE ARRAY_2D_COLM_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g = C_NULL_PTR
  INTEGER(8) :: cts(2), min_chunk(2), ndim, lo_idx(2), hi_idx(2), ld(1)
  INTEGER, TARGET :: buf(12)
  INTEGER :: exp_buf(12)
  TYPE(C_PTR) :: buf_addr
  INTEGER :: i, j, info, ierr, stat

  g = C_NULL_PTR
  cts = (/4, 5/)
  min_chunk = (/0, 0/)
  ndim = 2

  call MPI_INFO_CREATE(info, ierr)
  call MPI_INFO_SET(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR, ierr)
  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, info, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call MPI_INFO_FREE(info, ierr)

  buf_addr = C_LOC(buf)
  ld(1) = 1
  DO j = 0, 4
    DO i = 0, 3
      buf(1) = 4 * j + i
      lo_idx = (/i, j/)
      hi_idx = (/i, j/)
      call GDS_PUT(buf_addr, ld, lo_idx, hi_idx, g, stat)
      call ASSERT_STAT('GDS_PUT', stat)
      call GDS_WAIT_LOCAL(g, stat)
    END DO
  END DO
  call GDS_FENCE(g, stat)

  ld(1) = 2
  lo_idx = (/1, 1/)
  hi_idx = (/2, 3/)
  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:6) = (/5, 6, 9, 10, 13, 14/)
  call ASSERT_COND('GDS_GET', 'ARRAY',  ARR_EQ(buf, exp_buf, 6))

  lo_idx = (/0, 3/)
  hi_idx = (/1, 4/)
  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:4) = (/12, 13, 16, 17/)
  call ASSERT_COND('GDS_GET', 'ARRAY', ARR_EQ(buf, exp_buf, 4))

  ld(1) = 4
  lo_idx = (/1, 1/)
  hi_idx = (/2, 2/)
  buf(:) = -1
  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:8) = (/5, 6, -1, -1, 9, 10, -1, -1/)
  call ASSERT_COND('GDS_GET', 'ARRAY', ARR_EQ(buf, exp_buf, 8))

  ld(1) = 1
  lo_idx = (/0, 0/)
  hi_idx = (/0, 4/)
  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  exp_buf(1:5) = (/0, 4, 8, 12, 16/)
  call ASSERT_COND('GDS_GET', 'ARRAY', ARR_EQ(buf, exp_buf, 5))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE ARRAY_3D_COLM_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g
  INTEGER(8) :: cts(3), min_chunk(3), ndim = 3, lo_idx(3), hi_idx(3), ld(2)
  INTEGER, TARGET :: buf(48)
  INTEGER :: exp_buf(48)
  TYPE(C_PTR) :: buf_addr
  INTEGER :: i, j, k, n, info, ierr, stat

  g = C_NULL_PTR
  cts = (/4, 3, 3/)
  min_chunk = (/0, 0, 0/)
  ndim = 3

  call MPI_INFO_CREATE(info, ierr)
  call MPI_INFO_SET(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR, ierr)
  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, info, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call MPI_INFO_FREE(info, ierr)

  buf_addr = C_LOC(buf)
  ld = (/1, 1/)
  n = 0
  DO k = 0, 2
     DO j = 0, 2
       DO i = 0, 3
         buf(1) = n
         n = n + 1
         lo_idx = (/i, j, k/)
         hi_idx = (/i, j, k/)
         call GDS_PUT(buf_addr, ld, lo_idx, hi_idx, g, stat)
         call ASSERT_STAT('GDS_PUT', stat)
         call GDS_WAIT_LOCAL(g, stat)
       END DO
     END DO
  END DO
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  ld = (/2, 2/)
  lo_idx = (/1, 1, 0/)
  hi_idx = (/2, 2, 2/)
  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  exp_buf(1:12) = (/5, 6, 9, 10, 17, 18, 21, 22, 29, 30, 33, 34/)
  call ASSERT_COND('GDS_GET', 'ARRAY',  ARR_EQ(buf, exp_buf, 12))

  ld = (/4, 3/)
  lo_idx = (/1, 1, 0/)
  hi_idx = (/2, 2, 1/)
  buf(:) = -1
  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:24) = (/5, 6, -1, -1, 9, 10, -1, -1, -1, -1, -1, -1, &
                   17, 18, -1, -1, 21, 22, -1, -1, -1, -1, -1, -1/)
  call ASSERT_COND('GDS_GET', 'ARRAY', ARR_EQ(buf, exp_buf, 24))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE ARRAY_3D_ROWM_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g
  INTEGER(8) :: cts(1:3), min_chunk(3), ndim, lo_idx(3), hi_idx(3), ld(2)
  INTEGER, TARGET :: buf(48)
  INTEGER :: exp_buf(1:48)
  TYPE(C_PTR) :: buf_addr
  INTEGER :: i, j, k, n, stat ,info, ierr

  g = C_NULL_PTR
  cts = (/3, 4, 3/)
  min_chunk = (/0, 0, 0/)
  ndim = 3
  buf_addr = C_LOC(buf)

  call MPI_INFO_CREATE(info, ierr)
  call MPI_INFO_SET(info, GDS_ORDER_KEY, GDS_ORDER_ROW_MAJOR, ierr)
  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, info, g, stat)
  !call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
  !     GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call MPI_INFO_FREE(info, ierr)

  ld = (/1, 1/)
  n = 0
  DO k = 0, 2
     DO j = 0, 3
       DO i = 0, 2
         buf(1) = n
         n = n + 1
         lo_idx = (/k, j, i/)
         hi_idx = (/k, j, i/)
         call GDS_PUT(buf_addr, ld, lo_idx, hi_idx, g, stat)
         call ASSERT_STAT('GDS_PUT', stat)
         call GDS_WAIT_LOCAL(g, stat)
         call ASSERT_STAT('GDS_WAIT_LOCAL', stat)
       END DO
     END DO
  END DO
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  ld = (/3, 2/)
  lo_idx = (/0, 1, 1/)
  hi_idx = (/2, 3, 2/)
  buf(:) = -1
  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  exp_buf(1:18) = (/4, 5, 7, 8, 10, 11, 16, 17, 19, 20, 22, 23, &
                   28, 29, 31, 32, 34, 35/)
  call ASSERT_COND('GDS_GET', 'ARRAY',  ARR_EQ(buf, exp_buf, 18))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE MULTI_ALLOC_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g(NARRAYS)
  INTEGER(8) :: cts(1), min_chunk(1), ndim
  INTEGER :: i, stat

  g(:) = C_NULL_PTR
  ndim = 1

  DO i = 1, NARRAYS
    cts(1) = 1024
    min_chunk(1) = 0
    call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
      GDS_COMM_WORLD, MPI_INFO_NULL, g(i), stat)
    call ASSERT_STAT('GDS_ALLOC', stat)
  END DO

  DO i = 1, NARRAYS
    call GDS_FREE(g(i), stat)
    call ASSERT_STAT('GDS_FREE', stat)
  END DO
END SUBROUTINE

SUBROUTINE SINGLE_RW_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g
  INTEGER(8) :: cts(1), min_chunk(1), ndim
  INTEGER(8) :: lo_idx(1), hi_idx(1), ld(1)
  INTEGER, TARGET :: put_buf(3), get_buf(3)
  TYPE(C_PTR) :: put_buf_addr, get_buf_addr
  INTEGER :: i, stat

  g = C_NULL_PTR
  cts(1) = 1024
  min_chunk(1) = 0
  ndim = 1
  lo_idx(1) = 510
  hi_idx(1) = 512
  put_buf = (/55, 143, 2098/)
  put_buf_addr = C_LOC(put_buf)
  get_buf_addr = C_LOC(get_buf)
  
  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call GDS_PUT(put_buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  call GDS_GET(get_buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  DO i = 1, 3
    call ASSERT_COND('GDS_GET', 'PUT/GET', put_buf(i) == get_buf(i))
  END DO

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE STRIDE_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  INTEGER, PARAMETER :: ARRAY_SIZE = 64
  TYPE(C_PTR) :: g
  INTEGER(8) :: cts(1), min_chunk(1), ndim
  INTEGER(8) :: lo_idx(1), hi_idx(1), ld(1)
  INTEGER, TARGET :: put_buf(3), get_buf(3)
  TYPE(C_PTR) :: put_buf_addr, get_buf_addr
  INTEGER :: stat

  g = C_NULL_PTR
  cts(1) = ARRAY_SIZE
  min_chunk(1) = 0
  ndim = 1
  lo_idx(1) = 0
  hi_idx(1) = 0
  put_buf = (/0, 143, 2098/)

  IF ( nprocs < 2 ) RETURN
  
  put_buf_addr = C_LOC(put_buf)
  get_buf_addr = C_LOC(get_buf)
  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  IF ( rank == 0 ) THEN
    call GDS_PUT(put_buf_addr, ld, lo_idx, hi_idx, g, stat)
    call ASSERT_STAT('GDS_PUT', stat)
    call GDS_WAIT_LOCAL(g, stat)
    call ASSERT_STAT('GDS_WAIT_LOCAL', stat)
  END IF

  put_buf(1) = 55
  lo_idx(1) = ARRAY_SIZE / nprocs  - 1
  hi_idx(1) = lo_idx(1) + 3 - 1

  IF ( rank == 0 ) THEN
    call GDS_PUT(put_buf_addr, ld, lo_idx, hi_idx, g, stat)
    call ASSERT_STAT('GDS_PUT', stat)
  ENDIF

  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_VERSION_INC(g, 1_8, '', 0_8, stat)
  call ASSERT_STAT('GDS_VERSION_INC', stat)
  call GDS_GET(get_buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call ASSERT_COND('GDS_GET', 'PUT/GET', ARR_EQ(put_buf, get_buf, 3))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE IAXPY_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  INTEGER :: IAXPYM = 20
  TYPE(C_PTR) :: x, y
  INTEGER(8) :: cts(1), min_chunk(1), global_lo(1), global_hi(1), &
    ld(1), my_lo(1), my_hi(1), ndim, mysize
  INTEGER :: alpha, i, stat
  INTEGER, TARGET, ALLOCATABLE, DIMENSION(:) :: inbufx, inbufy, outbuf
  INTEGER, ALLOCATABLE, TARGET, DIMENSION(:) :: tmpbufx, tmpbufy
  TYPE(C_PTR) :: inbufx_addr, inbufy_addr, outbuf_addr, &
                 tmpbufx_addr, tmpbufy_addr
  
  IF ( IAXPYM < nprocs ) THEN
    IAXPYM = 2 * nprocs
  END IF

  x = C_NULL_PTR
  y = C_NULL_PTR
  cts(1) = IAXPYM
  min_chunk(1) = 0
  global_lo(1) = 0
  global_hi(1) = IAXPYM - 1
  ndim = 1
  alpha = 2

  ALLOCATE(inbufx(1:IAXPYM))
  ALLOCATE(inbufy(1:IAXPYM))
  ALLOCATE(outbuf(1:IAXPYM))

  DO i = 1, IAXPYM
    inbufx(i) = i
    inbufy(i) = i + IAXPYM
    outbuf(i) = 0
  END DO

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, MPI_INFO_NULL, x, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, MPI_INFO_NULL, y, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  mysize = IAXPYM / nprocs
  my_lo(1) = rank * mysize
  my_hi(1) = (rank + 1) * mysize - 1

  ALLOCATE(tmpbufx(1:mysize))
  ALLOCATE(tmpbufy(1:mysize))
  
  inbufx_addr = C_LOC(inbufx)
  inbufy_addr = C_LOC(inbufy)
  outbuf_addr = C_LOC(outbuf)
  tmpbufx_addr = C_LOC(tmpbufx)
  tmpbufy_addr = C_LOC(tmpbufy)

  IF ( rank == 0 ) THEN
    call GDS_PUT(inbufx_addr, ld, global_lo, global_hi, x, stat)
    call ASSERT_STAT('GDS_PUT', stat)
    call GDS_PUT(inbufy_addr, ld, global_lo, global_hi, y, stat)
    call ASSERT_STAT('GDS_PUT', stat)
  END IF

  call GDS_FENCE(C_NULL_PTR, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_GET(tmpbufx_addr, ld, my_lo, my_hi, x, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_GET(tmpbufy_addr, ld, my_lo, my_hi, y, stat)
  call ASSERT_STAT('GDS_GET', stat)
  
  call GDS_FENCE(C_NULL_PTR, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_VERSION_INC(x, 1_8, '', 0_8, stat)
  call ASSERT_STAT('GDS_VERSION_INC', stat)

  DO i = 1, INT(mysize)
    tmpbufx(i) = alpha * tmpbufx(i) + tmpbufy(i)
  END DO

  call GDS_PUT(tmpbufx_addr, ld, my_lo, my_hi, x, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  
  call GDS_FENCE(x, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  IF ( rank == 0 ) THEN
    call GDS_GET(outbuf_addr, ld, global_lo, global_hi, x, stat)
    call GDS_WAIT(x, stat)
    DO i = 1, INT(mysize * nprocs)
      call ASSERT_COND('GDS_GET', 'IN/OUTBUF', &
        outbuf(i) == (alpha * inbufx(i) + inbufy(i)))
    END DO
  END IF

  call GDS_FENCE(C_NULL_PTR, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_FREE(x, stat)
  call ASSERT_STAT('GDS_FREE', stat)
  call GDS_FREE(y, stat)
  call ASSERT_STAT('GDS_FREE', stat)
  
  DEALLOCATE(tmpbufx)
  DEALLOCATE(tmpbufy)
  DEALLOCATE(inbufx)
  DEALLOCATE(inbufy)
  DEALLOCATE(outbuf)
END SUBROUTINE

SUBROUTINE VER_TRAVERSE_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE

  INTEGER(8), PARAMETER :: ARRAYLEN = 1024
  TYPE(C_PTR) :: g
  INTEGER(8) :: cts(1), min_chunk(1), ndim, versions(7), lastver, ver
  INTEGER :: i, stat

  g = C_NULL_PTR
  cts(1) = ARRAYLEN
  min_chunk(1) = 0
  ndim = 1
  versions = (/0, 1, 2, 5, 7, 10, 11/)
  lastver = versions(SIZE(versions))

  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
       GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  ! create versions
  DO i = 2, SIZE(versions)
    call GDS_VERSION_INC(g, versions(i) - versions(i-1), '', 0_8, stat)
    call ASSERT_STAT('GDS_VERSION_INC', stat)
    call GDS_GET_VERSION_NUMBER(g, ver, stat)
    call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
    call ASSERT_COND('GDS_VERSION_INC', 'VER', ver == versions(i))
  END DO

  call GDS_VERSION_DEC(g, lastver, stat)
  call ASSERT_STAT('GDS_VERSION_DEC', stat)
  call GDS_GET_VERSION_NUMBER(g, ver, stat)
  call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
  call ASSERT_COND('GDS_VERSION_DEC', 'VER', ver == 0)

  call GDS_MOVE_TO_NEWEST(g, stat)
  call ASSERT_STAT('GDS_MOVE_TO_NEWEST', stat)
  call GDS_GET_VERSION_NUMBER(g, ver, stat)
  call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
  call ASSERT_COND('GDS_MOVE_TO_NEWEST', 'VER', ver == lastver)
 
  ! Go backward
  DO i = SIZE(versions), -1, -1
    IF ( i > 1 ) THEN
      call GDS_MOVE_TO_PREV(g, stat)
      call ASSERT_STAT('GDS_MOVE_TO_PREV', stat)
      call GDS_GET_VERSION_NUMBER(g, ver, stat)
      call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
      call ASSERT_COND('GDS_MOVE_TO_PREV', 'VER', ver == versions(i-1))
    ELSE
      call GDS_MOVE_TO_PREV(g, stat)
      call ASSERT_COND('GDS_MOVE_TO_PREV', 'GDS_STATUS_INVALID', &
        stat == GDS_STATUS_INVALID)
      call GDS_GET_VERSION_NUMBER(g, ver, stat)
      call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
      call ASSERT_COND('GDS_MOVE_TO_PREV', 'VER', ver == 0)
      EXIT
    END IF
  END DO

  ! Go foreward
  DO i = 1, SIZE(versions) + 1
    IF ( i < SIZE(versions) ) THEN
      call GDS_MOVE_TO_NEXT(g, stat)
      call ASSERT_STAT('GDS_MOVE_TO_NEXT', stat)
      call GDS_GET_VERSION_NUMBER(g, ver, stat)
      call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
      call ASSERT_COND('GDS_MOVE_TO_NEXT', 'VER', ver == versions(i+1))
    ELSE
      call GDS_MOVE_TO_NEXT(g, stat)
      call ASSERT_COND('GDS_MOVE_TO_NEXT', 'GDS_STATUS_INVALID', &
        stat == GDS_STATUS_INVALID)
      call GDS_GET_VERSION_NUMBER(g, ver, stat)
      call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
      call ASSERT_COND('GDS_MOVE_TO_NEXT', 'VER', ver == lastver)
      EXIT
    END IF
  END DO

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE CLONE_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  INTEGER(8), PARAMETER :: ARRAYLEN = 4
  TYPE(C_PTR) :: g, g1
  INTEGER, TARGET :: testdata(2,4),  buf(4)
  INTEGER(8) :: cts(1), min_chunk(1), ndim, lo_idx(1), hi_idx(1), ld(1), ver
  TYPE(C_PTR) :: testdata_addr, buf_addr
  INTEGER :: stat, info, ierr
  
  g = C_NULL_PTR
  g1 = C_NULL_PTR
  buf = 0
  cts(1) = ARRAYLEN
  min_chunk(1) = 0
  ndim = 1
  lo_idx(1) = 0
  hi_idx(1) = 3
  testdata = RESHAPE((/1, 2, 3, 4, 5, 6, 7, 8/), SHAPE(testdata))
  
  call MPI_INFO_CREATE(info, ierr)
  call MPI_INFO_SET(info, GDS_ORDER_KEY, GDS_ORDER_ROW_MAJOR, ierr)
  call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, info, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call MPI_INFO_FREE(info, ierr)

  buf_addr = C_LOC(buf)
  IF ( rank == 0 ) THEN
    testdata_addr = C_LOC(testdata(1, 1))
    call GDS_PUT(testdata_addr, ld, lo_idx, hi_idx, g, stat)
    call ASSERT_STAT('GDS_PUT', stat)
  END IF

  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_DESCRIPTOR_CLONE(g, g1, stat)
  call ASSERT_STAT('GDS_DESCRIPTOR_CLONE', stat)
  call ASSERT_COND('GDS_DESCRIPTOR_CLONE', 'GDS', .NOT. C_ASSOCIATED(g, g1))
  call GDS_VERSION_INC(g, 1_8, '', 0_8, stat)
  call ASSERT_STAT('GDS_VERSION_INC', stat)

  testdata_addr = C_LOC(testdata(1, 3))
  call GDS_PUT(testdata_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)

  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  
  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call ASSERT_COND('GDS_GET', 'BUF', ARR_EQ(buf, testdata(1, 3), 4))
  
  call GDS_GET_VERSION_NUMBER(g, ver, stat)
  call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
  call ASSERT_COND('GDS_VERSION_INC', 'VER', ver == 1)
  call GDS_GET_VERSION_NUMBER(g1, ver, stat)
  call ASSERT_STAT('GDS_GET_VERSION_NUMBER', stat)
  call ASSERT_COND('GDS_VERSION_INC', 'VER', ver == 0)

  call GDS_GET(buf_addr, ld, lo_idx, hi_idx, g1, stat)
  call ASSERT_STAT('GDS_GET', stat)
  
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  call ASSERT_COND('GDS_GET', 'BUF', ARR_EQ(buf, testdata(1, 1), 4))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
  call GDS_FREE(g1, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE GVR_2D_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g = C_NULL_PTR
  INTEGER(8) :: ndim, cts(2), chunk(2), ld(1), lo(2), hi(2)
  INTEGER :: stat
  INTEGER, TARGET :: a(128, 128 * nprocs)

  ndim = 2
  cts = (/128 * nprocs, 128 * nprocs/)
  chunk = (/0, 0/)
  ld(1) = 128

  call GDS_ALLOC(ndim, cts, chunk, GDS_DATA_INT, GDS_PRIORITY_LOW, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  
  a = 0

  lo = (/128 * rank, 0/)
  hi = (/128 * rank + 128 - 1, 128 * nprocs - 1/)
  call GDS_PUT(C_LOC(a), ld, lo, hi, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)

  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE TEST_LOCAL_ERROR_ATTRS()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  TYPE(C_PTR) :: g, desc
  INTEGER(8), TARGET :: a
  INTEGER(8) :: cnt(1)
  INTEGER :: stat

  cnt = 128 * nprocs
  call GDS_ALLOC(1_8, cnt, (/0_8/), GDS_DATA_INT, GDS_PRIORITY_LOW, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call SET_HANDLER(g)

  call GDS_CREATE_ERROR_DESCRIPTOR(desc, stat)
  call ASSERT_STAT('GDS_CREATE_ERROR_DESCRIPTOR', stat)

  a = 128
  call GDS_ADD_ERROR_ATTR(desc, GDS_EATTR_GDS_INDEX, 8_8, C_LOC(a), stat)
  call ASSERT_STAT('GDS_ADD_ERROR_ATTR', stat)

  call GDS_RAISE_LOCAL_ERROR(g, desc, stat)
  call ASSERT_STAT('GDS_RAISE_LOCAL_ERROR', stat)
  
  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)
END SUBROUTINE

SUBROUTINE TEST_LOCAL_ERROR()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  INTEGER, PARAMETER :: LET_A = 2948029
  CHARACTER, TARGET :: LET_STR(24) =  (/'M','a','y',' ','t','h','e',' ','f', &
    'o','c','e',' ','b','e',' ','w','i','t','h',' ','y','o','u'/)
  INTEGER :: stat
  INTEGER, TARGET :: a
  INTEGER(8) :: attr_lens(2)
  TYPE(C_PTR) :: g, attr_vals(2), pred, term, desc
  INTEGER(8) :: cnt(1)
  INTEGER, SAVE :: LET_ATTR_N, LET_ATTR_S, let_shared_a

  a = LET_A
  cnt(1) = 128 * nprocs
  attr_lens = (/INT(SIZE(LET_STR), 8), SIZEOF(a)/)
  attr_vals = (/C_LOC(LET_STR), C_LOC(a)/)

  call GDS_ALLOC(1_8, cnt, (/0_8/), GDS_DATA_INT, GDS_PRIORITY_LOW, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  call GDS_DEFINE_ERROR_ATTR_KEY('LET_ATTR_S', 10_8, &
    GDS_EAVTYPE_BYTE_ARRAY, LET_ATTR_S, stat)
  call ASSERT_STAT('GDS_DEFINE_ERROR_ATTR_KEY', stat)
  
  call GDS_DEFINE_ERROR_ATTR_KEY('LET_ATTR_N', 10_8, &
    GDS_EAVTYPE_BYTE_ARRAY, LET_ATTR_N, stat)
  call ASSERT_STAT('GDS_DEFINE_ERROR_ATTR_KEY', stat)

  call GDS_CREATE_ERROR_PRED(pred, stat)
  call ASSERT_STAT('GDS_CREATE_ERROR_PRED', stat)

  call GDS_CREATE_ERROR_PRED_TERM(LET_ATTR_N, GDS_EMEXP_ANY, 0_8, &
    C_NULL_PTR, term, stat)
  call ASSERT_STAT('GDS_CREATE_ERROR_PRED_TERM', stat)

  call GDS_ADD_ERROR_PRED_TERM(pred, term, stat)
  call ASSERT_STAT('GDS_ADD_ERROR_PRED_TERM', stat)

  call GDS_FREE_ERROR_PRED_TERM(term, stat)

  call GDS_REGISTER_LOCAL_ERROR_HANDLER(g, pred, &
    C_FUNLOC(LOCAL_ERROR_RECOVERY), stat)
  call ASSERT_STAT('GDS_REGISTER_LOCAL_ERROR_HANDLER', stat)
  call GDS_FREE_ERROR_PRED(pred, stat)
  call ASSERT_STAT('GDS_FREE_ERROR_PRED', stat)

  call GDS_CREATE_ERROR_DESCRIPTOR(desc, stat)
  call ASSERT_STAT('GDS_CREATE_ERROR_DESCRIPTOR', stat)
  call GDS_ADD_ERROR_ATTR(desc, LET_ATTR_S, 24_8, C_LOC(LET_STR), stat)
  call ASSERT_STAT('GDS_ADD_ERROR_ATTR', stat)
  call GDS_ADD_ERROR_ATTR(desc, LET_ATTR_N, 4_8, C_LOC(a), stat)
  call ASSERT_STAT('GDS_ADD_ERROR_ATTR', stat)

  call GDS_RAISE_LOCAL_ERROR(g, desc, stat)
  call ASSERT_STAT('GDS_RAISE_LOCAL_ERROR', stat)

  call ASSERT_COND('GDS_RAISE_LOCAL_ERROR', 'LET_SHARED', &
    let_shared_a == LET_A)

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)

  CONTAINS
#if defined(__INTEL_COMPILER)
  INTEGER(C_INT) FUNCTION LOCAL_ERROR_RECOVERY(gds, desc)
#else
  INTEGER(C_INT) FUNCTION LOCAL_ERROR_RECOVERY(gds, desc) BIND(C)
#endif
    USE, INTRINSIC :: ISO_C_BINDING
    TYPE(C_PTR), VALUE, INTENT(IN) :: gds
    TYPE(C_PTR), VALUE, INTENT(IN) :: desc
    INTEGER, TARGET :: a
    INTEGER :: flag, stat
    INTEGER(8) :: attr_len
    CHARACTER, TARGET :: buf(32)
    
    call GDS_GET_ERROR_ATTR(desc, LET_ATTR_N, C_LOC(a), flag, stat)
    call ASSERT_STAT('GDS_GET_ERROR_ATTR', stat)
    call ASSERT_COND('GDS_GET_ERROR_ATTR', 'FLAG', flag /= 0)
    call ASSERT_COND('GDS_GET_ERROR_ATTR', 'LET_A', a == LET_A)

    call GDS_GET_ERROR_ATTR_LEN(desc, LET_ATTR_N, attr_len, flag, stat)
    call ASSERT_COND('GDS_GET_ERROR_ATTR_LEN', 'FLAG', flag /= 0)
    call ASSERT_COND('GDS_GET_ERROR_ATTR_LEN', 'LET_ATTR_N', attr_len == 4)

    call GDS_GET_ERROR_ATTR(desc, LET_ATTR_S, C_LOC(buf), flag, stat)
    call ASSERT_STAT('GDS_GET_ERROR_ATTR', stat)
    call ASSERT_COND('GDS_GET_ERROR_ATTR', 'FLAG', flag /= 0)
    call ASSERT_COND('GDS_GET_ERROR_ATTR', 'LET_STR', &
      STR_EQ(buf, LET_STR, 24_8))

    call GDS_GET_ERROR_ATTR_LEN(desc, LET_ATTR_S, attr_len, flag, stat)
    call ASSERT_COND('GDS_GET_ERROR_ATTR_LEN', 'FLAG', flag /= 0)
    call ASSERT_COND('GDS_GET_ERROR_ATTR_LEN', 'LET_ATTR_S', attr_len == 24)

    let_shared_a = a

    call GDS_RESUME_LOCAL(gds, desc, stat)
    call ASSERT_STAT('GDS_RESUME_LOCAL', stat)
    LOCAL_ERROR_RECOVERY = 0
  END FUNCTION
END SUBROUTINE

SUBROUTINE LARGE_MATRIX_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  REAL(8), PARAMETER :: TOL = 1D-14
  INTEGER, PARAMETER :: LMNONZEROS = 13107200
  TYPE(C_PTR) :: gds_rows, gds_cols, gds_vals
  INTEGER(8) :: cnt(1), min_chunk(1), lo(1), hi(1), ld(1), eq
  INTEGER, ALLOCATABLE, TARGET :: rows(:), cols(:)
  REAL(8), ALLOCATABLE, TARGET :: vals(:)
  INTEGER, ALLOCATABLE, TARGET :: rows_result(:), cols_result(:)
  REAL(8), ALLOCATABLE, TARGET :: vals_result(:)
  INTEGER :: i, row, col, stat
  REAL(8) :: d

  cnt(1) = LMNONZEROS
  min_chunk(1) = 0

  ALLOCATE(rows(LMNONZEROS))
  ALLOCATE(cols(LMNONZEROS))
  ALLOCATE(vals(LMNONZEROS))
  ALLOCATE(rows_result(LMNONZEROS))
  ALLOCATE(cols_result(LMNONZEROS))
  ALLOCATE(vals_result(LMNONZEROS))

  row = 0
  col = 0

  DO i = 1, LMNONZEROS
    rows(i) = row
    cols(i) = col
    vals(i) = i / 3.0
    IF ( MOD(i, 3) == 0 ) THEN
      row = row + 1
    ELSE
      col = MOD(col + 1, 50000)
    END IF
  END DO

  call GDS_ALLOC(1_8, cnt, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, gds_rows, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call GDS_ALLOC(1_8, cnt, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, gds_cols, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  call GDS_ALLOC(1_8, cnt, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, gds_vals, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)
  
  lo(1) = 0
  hi(1) = LMNONZEROS - 1

  call GDS_PUT(C_LOC(rows), ld, lo, hi, gds_rows, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_PUT(C_LOC(cols), ld, lo, hi, gds_cols, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_PUT(C_LOC(vals), ld, lo, hi, gds_vals, stat)
  call ASSERT_STAT('GDS_PUT', stat)

  call GDS_FENCE(C_NULL_PTR, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_GET(C_LOC(rows_result), ld, lo, hi, gds_rows, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_GET(C_LOC(cols_result), ld, lo, hi, gds_cols, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_GET(C_LOC(vals_result), ld, lo, hi, gds_vals, stat)
  
  call GDS_FENCE(C_NULL_PTR, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  eq = 0
  DO i = 1, LMNONZEROS
    IF ( rows(i) == rows_result(i) ) eq = eq + 1
    IF ( cols(i) == cols_result(i) ) eq = eq + 1
    d = vals(i) - vals_result(i)
    IF ( d < 0 ) d = -d
    IF ( d < TOL ) eq = eq + 1
  END DO
  call ASSERT_COND('GDS_GET', 'EQ', eq == LMNONZEROS * 3)

  call GDS_FREE(gds_rows, stat)
  call ASSERT_STAT('GDS_FREE', stat)
  call GDS_FREE(gds_cols, stat)
  call ASSERT_STAT('GDS_FREE', stat)
  call GDS_FREE(gds_vals, stat)
  call ASSERT_STAT('GDS_FREE', stat)

  DEALLOCATE(rows)
  DEALLOCATE(cols)
  DEALLOCATE(vals)
  DEALLOCATE(rows_result)
  DEALLOCATE(cols_result)
  DEALLOCATE(vals_result)
END SUBROUTINE

SUBROUTINE MPI_DERIVED_TYPE_TEST()
  USE, INTRINSIC :: ISO_C_BINDING
  USE MPI
  USE GDS
  USE GLOBAL
  IMPLICIT NONE
  ! Borrowed from OpenMC data structures, 
  ! see bank_header.F90 and initialize.F90
  TYPE Bank
    SEQUENCE
    REAL(8) :: wgt
    REAl(8) :: xyz(3)
    REAL(8) :: uvw(3)
    REAL(8) :: E
  END TYPE Bank

  TYPE(C_PTR) :: g
  INTEGER(8) :: ndim, cts(1), min_chunk(1), lo_idx(1), hi_idx(1), ld(1)
  TYPE(Bank), TARGET :: put_buf, get_buf
  INTEGER :: MPI_BANK
  INTEGER(MPI_ADDRESS_KIND) :: bank_disp(4)
  INTEGER :: bank_blocks(4), bank_types(4)
  TYPE(Bank) :: b
  INTEGER :: ierr, stat

  call MPI_GET_ADDRESS(b % wgt, bank_disp(1), ierr)
  call MPI_GET_ADDRESS(b % xyz, bank_disp(2), ierr)
  call MPI_GET_ADDRESS(b % uvw, bank_disp(3), ierr)
  call MPI_GET_ADDRESS(b % E,   bank_disp(4), ierr)

  bank_disp = bank_disp - bank_disp(1)

  bank_blocks = (/1, 3, 3, 1/)
  bank_types = (/ MPI_REAL8, MPI_REAL8, MPI_REAL8, MPI_REAL8 /)
  call MPI_TYPE_CREATE_STRUCT(4, bank_blocks, bank_disp, & 
    bank_types, MPI_BANK, ierr)
  call MPI_TYPE_COMMIT(MPI_BANK, ierr)
  
  ndim = 1
  cts(1) = 1
  min_chunk(1) = 0
  lo_idx(1) = 0
  hi_idx(1) = 0
  ld(1) = 0

  call GDS_ALLOC(ndim, cts, min_chunk, MPI_BANK, GDS_PRIORITY_HIGH, &
    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
  call ASSERT_STAT('GDS_ALLOC', stat)

  put_buf % wgt = 1
  put_buf % xyz = 2
  put_buf % uvw = 3
  put_buf % E = 4

  call GDS_PUT(C_LOC(put_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_PUT', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)

  call GDS_GET(C_LOC(get_buf), ld, lo_idx, hi_idx, g, stat)
  call ASSERT_STAT('GDS_GET', stat)
  call GDS_FENCE(g, stat)
  call ASSERT_STAT('GDS_FENCE', stat)
  call ASSERT_COND('GDS_GET', 'get_buf', &
    get_buf % wgt == put_buf % wgt .AND. get_buf % E == put_buf % E .AND. &
    ARR_REAL8_EQ(get_buf % xyz, put_buf % xyz, 3) .AND. &
    ARR_REAL8_EQ(get_buf % uvw, put_buf % uvw, 3))

  call GDS_FREE(g, stat)
  call ASSERT_STAT('GDS_FREE', stat)

  call MPI_TYPE_FREE(MPI_BANK, ierr)
END SUBROUTINE
