/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  Particle code example motivated by ddcMC code

  This code is expected to be used with tests.c
*/

#include "common.h"

#undef TEST_NAME
#define TEST_NAME "particle"

struct particle {
    double px, py, pz; /* Current position */
    double vx, vy, vz; /* Current velocity vector */
    int state;
};

#define PT_SIZE 1024
#define PT_TARGET 298

int n_orig;

int recovery_mode=0;

int total_recovery_mode=0;

static void init_particles(GDS_gds_t gds, int n)
{
    int i;
    GDS_size_t lo[1];
    GDS_size_t hi[1];
    GDS_size_t ld[] = {};

    for (i = 0; i < n; i++) {	
        struct particle p;

        p.state = i % 5;

        lo[0] = i*sizeof(p);
        hi[0] = (i+1)*sizeof(p)-1;
        ASSERT_STAT("GDS_put",
            GDS_put(&p, ld, lo, hi, gds));
        GDS_wait(gds);
    }
}

static size_t count_particles(GDS_gds_t gds)
{
    int i, flag;
    GDS_size_t ndims, *count;
    GDS_size_t ld[] = {};
    size_t c = 0;
    size_t nelems;

    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_NUMBER_DIMENSIONS, &ndims, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);
    ASSERT_EQUAL_INT(ndims, 1);
  
    count = malloc(sizeof(GDS_size_t) * ndims);
    ASSERT_STAT("GDS_get_attr", 
        GDS_get_attr(gds, GDS_COUNT, count, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);

    nelems = count[0]/sizeof(struct particle);

    for (i = 0; i < nelems; i++) {
        struct particle p;
        GDS_size_t lo[] = { i*sizeof(p) };
        GDS_size_t hi[] = { (i+1)*sizeof(p) - 1 };
        ASSERT_STAT("GDS_get", GDS_get(&p, ld, lo, hi, gds));
        GDS_wait(gds);
        if (p.state == 1)
            c++;
    }
  
    free(count);
    return c;
}

static size_t count_particles_local(GDS_gds_t gds, int rank, int size)
{
    int i, flag;
    GDS_size_t ndims, *count;
    GDS_size_t ld[] = {};
    size_t c = 0;
    size_t nelems;
    size_t nelems_local;
    int start, end;

    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_NUMBER_DIMENSIONS, &ndims, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);
    ASSERT_EQUAL_INT(ndims, 1);
    
    count = malloc(sizeof(GDS_size_t) * ndims);
    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_COUNT, count, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);

    nelems = count[0] / sizeof(struct particle);

    nelems_local = nelems / size;

    start = rank * nelems_local;
    if (rank == size - 1) {
        end = nelems;
    }else{
        end = (rank + 1) * nelems_local;
    }

    for (i = start; i < end; i++) {
        struct particle p;
        GDS_size_t lo[] = { i*sizeof(p) };
        GDS_size_t hi[] = { (i+1)*sizeof(p) - 1 };
        ASSERT_STAT("GDS_get",
            GDS_get(&p, ld, lo, hi, gds));
        GDS_wait(gds);
        if (p.state == 1)
            c++;
    }
  
    free(count);
    return c;
}

#if 0
static GDS_status_t count_particles_parallel(GDS_gds_t gds, GDS_priority_t check_priority)
{
    int rank;
    int size;

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);
    
    int i, flag;
    GDS_size_t ndims, *count;
    GDS_size_t ld[] = {};
    size_t c = 0;
    size_t nelems;
    size_t nelems_local;
    int start, end;
    
    int local_n, n_reduced;

    ASSERT_STAT("GDS_get_attr", 
        GDS_get_attr(gds, GDS_NUMBER_DIMENSIONS, &ndims, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);
    ASSERT_EQUAL_INT(ndims, 1);

    ASSERT_STAT("GDS_get_attr", 
        GDS_get_attr(gds, GDS_COUNT, &count, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);

    nelems = count[0] / sizeof(struct particle);
    nelems_local = nelems/size;

    start = rank*nelems_local;
    if (rank == size - 1) {
        end = nelems;
    }else{
        end = (rank + 1) * nelems_local;
    }

    for (i = start; i < end; i++) {
        struct particle p;
        GDS_size_t lo[] = { i * sizeof(p) };
        GDS_size_t hi[] = { (i + 1) * sizeof(p) - 1 };
        ASSERT_STAT("GDS_get", 
            GDS_get(&p, ld, lo, hi, gds));
        GDS_wait_local(gds);
        if (p.state == 1)
            c++;
    }

    local_n=c;    
    MPI_Reduce(&local_n, &n_reduced, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0) {// rank 0 will raise the global error
        if (n_reduced != n_orig){
            GDS_error_t err;
            GDS_size_t error_off = 0;
            GDS_size_t error_ct  = PT_SIZE * sizeof(struct particle);
            GDS_error_attr_t attr_keys[] = 
                {GDS_ERROR_MEMORY_OFFSET, GDS_ERROR_MEMORY_COUNT};
            void *attr_vals[] = {&error_off, &error_ct};
        
            ASSERT_STAT("GDS_create_error_descriptor",
                GDS_create_error_descriptor(GDS_ERROR_MEMORY,
                    attr_keys, attr_vals, 2, &err));
            ASSERT_NOT_NULL_FATAL(err);
            ASSERT_STAT("GDS_raise_global_error",
                GDS_raise_global_error(gds, err));
            ASSERT_EQUAL_INT(count_particles(gds), n_orig);
        }
    }
    

    return GDS_STATUS_OK;
}
#endif

static void update_particles(GDS_gds_t gds)
{
    /* TODO: do something! */
}

static int particles_full_check(GDS_gds_t gds)
{
    int i, flag;
    GDS_size_t *count, ndims;
    size_t nelems;
    
    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_NUMBER_DIMENSIONS, &ndims, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);
    ASSERT_EQUAL_INT(ndims, 1);
    
    count = malloc(sizeof(GDS_size_t) * ndims);
    ASSERT_STAT("GDS_get_attr",
        GDS_get_attr(gds, GDS_COUNT, count, &flag));
    ASSERT_NOT_EQUAL_INT(flag, 0);

    nelems = count[0]/sizeof(struct particle);

    for (i = 0; i < nelems; i++) {
        struct particle p;
        GDS_size_t lo[] = { i*sizeof(p) };
        GDS_size_t ld[] = {};
        GDS_size_t hi[] = { (i+1)*sizeof(p)-1 };
        ASSERT_STAT("GDS_get", GDS_get(&p, ld, lo, hi, gds));
        GDS_wait(gds);
        if (p.state != i % 5)
            return 0;
    }
    
    free(count);
    return 1;
}

#define PT_BUFSIZE 64

static GDS_status_t particle_error_handler(GDS_gds_t gds, GDS_error_t error_desc)
{
    int i;
    GDS_gds_t gds_latest;
    ASSERT_STAT_FATAL("GDS_descriptor_clone",
        GDS_descriptor_clone(gds, &gds_latest));
    /* Find a good version */
    do {
        ASSERT_STAT("GDS_move_to_prev", GDS_move_to_prev(gds));
    } while (!particles_full_check(gds));

    /* Copy good data to resume */
    for (i = 0; i < PT_SIZE; i += PT_BUFSIZE) {
        struct particle ps[PT_BUFSIZE];
        GDS_size_t lo[] = { i * sizeof(struct particle) };
        GDS_size_t hi[1];
        GDS_size_t count;
        GDS_size_t ld[] = {};

        if (PT_SIZE - i < PT_BUFSIZE)
            count = sizeof(struct particle) * (PT_SIZE - i);
        else
            count = sizeof(struct particle) * PT_BUFSIZE;
        hi[0] = lo[0] + count - 1;
        ASSERT_STAT("GDS_get", 
            GDS_get(ps, ld, lo, hi, gds));
        GDS_wait(gds);
        ASSERT_STAT("GDS_put", 
            GDS_put(ps, ld, lo, hi, gds_latest));
        GDS_wait(gds_latest);
    }

    ASSERT_STAT("GDS_move_to_newest", GDS_move_to_newest(gds));
    ASSERT_STAT("GDS_free", GDS_free(&gds_latest));
    ASSERT_STAT("GDS_resume_local", 
        GDS_resume_local(gds, error_desc));

    return GDS_STATUS_OK;
}

static GDS_status_t particle_error_global_handler(GDS_gds_t gds, GDS_error_t error_desc)
{
    int my_rank;
    int size;

    ASSERT_STAT("GDS_comm_rank",
        GDS_comm_rank(GDS_COMM_WORLD, &my_rank));
    ASSERT_STAT("GDS_comm_rank",
      GDS_comm_size(GDS_COMM_WORLD, &size));

    recovery_mode=1;
   
    MPI_Allreduce(&recovery_mode, &total_recovery_mode, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD );
    ASSERT_EQUAL_INT(total_recovery_mode, size);

    if (my_rank == 0) {
        int i;
        GDS_gds_t gds_latest;
        ASSERT_STAT_FATAL("GDS_descriptor_clone",
            GDS_descriptor_clone(gds, &gds_latest));
        /* Find a good version */
        do {
            ASSERT_STAT("GDS_move_to_prev",
                GDS_move_to_prev(gds));
        } while (!particles_full_check(gds));

    /* Copy good data to resume */
       for (i = 0; i < PT_SIZE; i += PT_BUFSIZE) {
            struct particle ps[PT_BUFSIZE];
            GDS_size_t lo[] = { i*sizeof(struct particle) };
            GDS_size_t hi[1];
            GDS_size_t count;
            GDS_size_t ld[] = {};

            if (PT_SIZE - i < PT_BUFSIZE)
                count = sizeof(struct particle)*(PT_SIZE - i);
            else
                count = sizeof(struct particle)*PT_BUFSIZE;
            hi[0] = lo[0] + count - 1;
            ASSERT_STAT("GDS_get", 
                GDS_get(ps, ld, lo, hi, gds));
            GDS_wait_local(gds);
            ASSERT_STAT("GDS_put", 
                GDS_put(ps, ld, lo, hi, gds_latest));
            GDS_wait(gds_latest);
        }

        ASSERT_STAT("GDS_move_to_newest",
            GDS_move_to_newest(gds));

        ASSERT_STAT("GDS_free", 
            GDS_free(&gds_latest));
    }

    ASSERT_STAT("GDS_resume_global", 
        GDS_resume_global(gds, error_desc));

    return GDS_STATUS_OK;
}

void test_particle(void)
{
    int rank;
    int size;

    ASSERT_TEST_START(TEST_NAME);

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    GDS_gds_t gds_p = NULL;
    GDS_size_t cts[1];
    int ndim = 1, i;
    GDS_size_t min_chunk[] = {0};
    GDS_size_t lo[] = { PT_TARGET };
    GDS_size_t hi[1];
    GDS_size_t ld[] = {};
    int n_orig = 0;
    int local_n=0;
    int n_reduced=0;

    cts[0] = PT_SIZE * sizeof(struct particle);

    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_BYTE, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds_p));
    ASSERT_NOT_NULL_FATAL(gds_p);
    GDS_error_pred_t pred;
    GDS_create_error_pred(&pred);
    ASSERT_STAT_FATAL("GDS_register_local_error_handler",
        GDS_register_local_error_handler(gds_p, pred,
        particle_error_handler));
    GDS_free_error_pred(&pred);

    init_particles(gds_p, PT_SIZE);

    if (rank == 0) {
        n_orig = count_particles(gds_p);
    }

    for (i = 0; i < 8; i++) {
        update_particles(gds_p);

        /* Fault injection */
        if (i == 5) {
            struct particle p;
            lo[0] = PT_TARGET * sizeof(p);
            hi[0] = (PT_TARGET + 1) * sizeof(p) - 1;
            ASSERT_STAT("GDS_get", 
                GDS_get(&p, ld, lo, hi, gds_p));
            GDS_fence(gds_p);
            p.state = p.state == 1 ? 0 : 1;
            ASSERT_STAT("GDS_put", 
                GDS_put(&p, ld, lo, hi, gds_p));
            GDS_fence(gds_p);
        }

        /* If particles not conserved, error! */
        local_n = count_particles_local(gds_p,rank,size);

        MPI_Reduce(&local_n, &n_reduced, 1, MPI_INT, MPI_SUM, 
            0, MPI_COMM_WORLD);

        if (rank==0) {
        
            if (i!=5) {
                ASSERT_EQUAL_INT(n_reduced, n_orig);
            }

            if ( n_reduced != n_orig) {
                GDS_error_t err;
                GDS_size_t error_off = 0;
                GDS_size_t error_ct  = cts[0];
                GDS_create_error_descriptor(&err);
                ASSERT_NOT_NULL_FATAL(err);
                GDS_add_error_attr(err, GDS_EATTR_GDS_INDEX,
                                   sizeof(error_off), &error_off);
                GDS_add_error_attr(err, GDS_EATTR_GDS_COUNT,
                                   sizeof(error_ct), &error_ct);

                ASSERT_STAT("GDS_raise_local_error",
                    GDS_raise_local_error(gds_p, err));
                ASSERT_EQUAL_INT(count_particles(gds_p), n_orig);
            }
        }
        GDS_version_inc(gds_p, 1, NULL, 0);
    }

    ASSERT_STAT("GDS_free", GDS_free(&gds_p));
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "global particle"

void test_particle_global(void)
{
    int rank;
    int size;

    GDS_gds_t gds_p = NULL;
    GDS_size_t cts[1];
    int ndim = 1, i;
    GDS_size_t min_chunk[] = {0};
    GDS_size_t lo[] = { PT_TARGET };
    GDS_size_t hi[1];
    GDS_size_t ld[] = {};
    int n_orig = 0;
    int n_reduced = 0;

    cts[0] = PT_SIZE * sizeof(struct particle);

    ASSERT_TEST_START(TEST_NAME);
    
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    ASSERT_STAT_FATAL("GDS_alloc", 
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_BYTE, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds_p));
    ASSERT_NOT_NULL_FATAL(gds_p);
    GDS_error_pred_t pred;
    GDS_create_error_pred(&pred);
    ASSERT_STAT_FATAL("GDS_reigster_global_error_handler",
        GDS_register_global_error_handler(gds_p, pred,
            particle_error_global_handler));
    GDS_free_error_pred(&pred);
    init_particles(gds_p, PT_SIZE);

    GDS_version_inc(gds_p, 1, NULL, 0);

    if (rank == 0) {
        n_orig = count_particles(gds_p);
    }

    MPI_Bcast(&n_orig, 1, MPI_INT, 0, MPI_COMM_WORLD);

    for (i = 0; i < 8; i++) {
        update_particles(gds_p);

        /* Fault injection */
        if (rank == 0) {
            if (i == 5) {
                struct particle p;
                lo[0] = PT_TARGET * sizeof(p);
                hi[0] = (PT_TARGET + 1) * sizeof(p) - 1;
                ASSERT_STAT("GDS_get", 
                    GDS_get(&p, ld, lo, hi, gds_p));
                p.state = p.state == 1 ? 0 : 1;
                ASSERT_STAT("GDS_put", 
                    GDS_put(&p, ld, lo, hi, gds_p));
            }
        }
        
        if (rank == 0) {

            /* If particles not conserved, error! */               
            n_reduced= count_particles(gds_p);
            
            if (i!= 5){             
                ASSERT_EQUAL_INT(n_reduced, n_orig);
            }
            if ( n_reduced != n_orig) {
                GDS_error_t err;
                GDS_size_t error_off = 0;
                GDS_size_t error_ct  = cts[0];
                GDS_create_error_descriptor(&err);
                ASSERT_NOT_NULL_FATAL(err);
                GDS_add_error_attr(err, GDS_EATTR_GDS_INDEX,
                                   sizeof(error_off), &error_off);
                GDS_add_error_attr(err, GDS_EATTR_GDS_COUNT,
                                   sizeof(error_ct), &error_ct);

                ASSERT_STAT("GDS_raise_global_error", 
                    GDS_raise_global_error(gds_p, err));
            }

        }

        if (size > 1) {
            if (rank == 1)
            {
                GDS_error_t err;
                GDS_size_t error_off = 0;
                GDS_size_t error_ct  = cts[0];
                GDS_create_error_descriptor(&err);
                ASSERT_NOT_NULL_FATAL(err);
                GDS_add_error_attr(err, GDS_EATTR_GDS_INDEX,
                                   sizeof(error_off), &error_off);
                GDS_add_error_attr(err, GDS_EATTR_GDS_COUNT,
                                   sizeof(error_ct), &error_ct);

                ASSERT_STAT("GDS_raise_global_error", 
                    GDS_raise_global_error(gds_p, err));
            }
        }
        if (i % 2 ==0)  
            ASSERT_STAT("GDS_fence", GDS_fence(NULL));

        ASSERT_STAT("GDS_version_inc", 
            GDS_version_inc(gds_p, 1, NULL, 0));
    }

    ASSERT_EQUAL_INT(count_particles(gds_p), n_orig);

    ASSERT_STAT("GDS_free", GDS_free(&gds_p));
    
    ASSERT_TEST_END();
}
