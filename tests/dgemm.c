/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  DGEMM examples

  This code is expected to be used with tests.c
*/

#include <stdio.h>
#include "common.h"

#undef  TEST_NAME
#define TEST_NAME "DGEMM"

#define TOL 1e-14

static void local_dgemm(double *A, double *B, double *C, size_t M, 
    size_t N, size_t K) 
{
    int i, j, kidx;
    double r;
    for (i = 0; i < M; ++i) {
        for (j = 0; j < N; ++j) {
            r = 0.0;
            for (kidx = 0; kidx < K; ++kidx) {
                r += A[kidx * M + i] * B[j * K + kidx];
            }
            C[j * M + i] = r;
        }
    }
}

#undef  TEST_NAME
#define TEST_NAME "naive DGEMM"
void test_naive_dgemm(void) 
{
#define NDMATRIXM 16
#define NDMATRIXK 4
#define NDMATRIXN 8

    int rank, i, j, k;
    int size;
    double result;
    
    ASSERT_TEST_START(TEST_NAME);
    
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    GDS_gds_t gds_A,gds_B,gds_C;
    GDS_size_t Asize[2] = {NDMATRIXM, NDMATRIXK};
    GDS_size_t Ahi[2] = {NDMATRIXM - 1, NDMATRIXK - 1};
    GDS_size_t Bsize[2] = {NDMATRIXK, NDMATRIXN};
    GDS_size_t Bhi[2] = {NDMATRIXK - 1, NDMATRIXN - 1};
    GDS_size_t Csize[2] = {NDMATRIXM, NDMATRIXN};
    GDS_size_t Chi[2] = {NDMATRIXM - 1, NDMATRIXN - 1};
    GDS_size_t min_chunk[2] = {0, 0};
    MPI_Info info;

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    GDS_alloc(2, Asize, min_chunk, GDS_DATA_DBL, 
        GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_A);
    GDS_alloc(2, Bsize, min_chunk, GDS_DATA_DBL, 
        GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_B);
    GDS_alloc(2, Csize, min_chunk, GDS_DATA_DBL, 
        GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_C);
    MPI_Info_free(&info);

    double C[NDMATRIXM * NDMATRIXN];
    size_t C_ld[1] = {NDMATRIXM};
    GDS_size_t offsetzero[2] = {0, 0};

    double Clocal[NDMATRIXM * NDMATRIXN];
    if (rank == 0) {
        double A[NDMATRIXM * NDMATRIXK];
        double B[NDMATRIXK * NDMATRIXN];
        for (i = 0; i < NDMATRIXM * NDMATRIXK; ++i) A[i] = (2*(i + 1))%11;
        for (i = 0; i < NDMATRIXK * NDMATRIXN; ++i) B[i] = (3*(i + 1))%17;
        for (i = 0; i < NDMATRIXM * NDMATRIXN; ++i) C[i] = 0;
        for (i = 0; i < NDMATRIXM * NDMATRIXN; ++i) Clocal[i] = 0;
        dprintmatrix(DPRINT_INFO, NDMATRIXM, NDMATRIXK, A, "Initial A"); 
        dprintmatrix(DPRINT_INFO, NDMATRIXK, NDMATRIXN, B, "Initial B"); 
        dprintmatrix(DPRINT_INFO, NDMATRIXM, NDMATRIXN, C, "Initial C");
        dprintmatrix(DPRINT_INFO, NDMATRIXM, NDMATRIXN, Clocal, "Initial locally computed C");

        //expected results
        local_dgemm(A, B, Clocal, NDMATRIXM, NDMATRIXN, NDMATRIXK);
        dprintmatrix(DPRINT_INFO, NDMATRIXM, NDMATRIXN, Clocal, "Locally Computed C"); 

        size_t A_ld[1] = {NDMATRIXM};
        size_t B_ld[1] = {NDMATRIXK};
        GDS_put((void *)A, A_ld, offsetzero, Ahi, gds_A); 
        GDS_put((void *)B, B_ld, offsetzero, Bhi, gds_B); 
        GDS_put((void *)C, C_ld, offsetzero, Chi, gds_C); 

        GDS_wait(gds_A);
        GDS_wait(gds_B);
        GDS_wait(gds_C);

        double Agot[NDMATRIXM * NDMATRIXK];
        double Bgot[NDMATRIXK * NDMATRIXN];
        double Cgot[NDMATRIXM * NDMATRIXN];

        GDS_get((void *)Agot, A_ld, offsetzero, Ahi, gds_A); 
        GDS_get((void *)Bgot, B_ld, offsetzero, Bhi, gds_B); 
        GDS_get((void *)Cgot, C_ld, offsetzero, Chi, gds_C); 

        GDS_wait(gds_A);
        GDS_wait(gds_B);
        GDS_wait(gds_C);

        dprintmatrix(DPRINT_INFO, NDMATRIXM, NDMATRIXK, Agot, "A after put and get"); 
        dprintmatrix(DPRINT_INFO, NDMATRIXK, NDMATRIXN, Bgot, "B after put and get"); 
        dprintmatrix(DPRINT_INFO, NDMATRIXM, NDMATRIXN, Cgot, "C after put and get"); 
    }

    GDS_fence(NULL);

    int rowsperproc = NDMATRIXM / size;
    int remainder = NDMATRIXM % size;
    i = rank * rowsperproc;
    int hilimit = i + rowsperproc;
    if (rank == size - 1) hilimit += remainder;

    double rbuf[NDMATRIXK];
    double cbuf[NDMATRIXK];
    GDS_size_t Amylo[2] = {0,0};
    GDS_size_t Amyhi[2] = {0,NDMATRIXK - 1};
    GDS_size_t Bmylo[2] = {0,0};
    GDS_size_t Bmyhi[2] = {NDMATRIXK - 1,0};
    GDS_size_t Cmylo[2] = {0,0};
    GDS_size_t Cmyhi[2] = {0,0};
    size_t rbuf_ld[1] = {1};
    size_t cbuf_ld[1] = {NDMATRIXK};	
    size_t res_ld[1] = {1};	

    for (; i < hilimit; ++i) {
        Amylo[0] = i;
        Amyhi[0] = i;
        Cmylo[0] = i;
        Cmyhi[0] = i;

        GDS_get((void *)rbuf, rbuf_ld, Amylo, Amyhi, gds_A); 
        GDS_wait(gds_A);

        for (j = 0; j < NDMATRIXN; ++j) {
            Bmylo[1] = j;
            Bmyhi[1] = j;
            Cmylo[1] = j;
            Cmyhi[1] = j;
            GDS_get((void *)cbuf, cbuf_ld, Bmylo, Bmyhi, gds_B);
            GDS_wait(gds_B); 
            result = 0;

            for (k = 0; k < NDMATRIXK; ++k) {
                result += rbuf[k] * cbuf[k];
            }
            GDS_put((void *)&result, res_ld, Cmylo, Cmyhi, gds_C); 
            GDS_wait(gds_C);
        }

    }

    GDS_fence(NULL);

    if (rank == 0) {
        GDS_get((void *)C, C_ld, offsetzero, Chi, gds_C);
        GDS_wait(gds_C); 
        dprintmatrix(DPRINT_INFO, NDMATRIXM, NDMATRIXN, C, "C");
        int idx;
        for (i = 0; i < NDMATRIXM; ++i) {
            for (j = 0; j < NDMATRIXN; ++j) {
                idx = j * NDMATRIXM + i;
                ASSERT_EQUAL_DOUBLE(C[idx], Clocal[idx], TOL);
            }
        }
    }

    GDS_fence(NULL);

    GDS_free(&gds_A);
    GDS_free(&gds_B);
    GDS_free(&gds_C);
    
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "block DGEMM"
void test_block_dgemm(void) {
#define BDSUBM 2
#define BDSUBN 3	
#define BDSUBK 5
#define BDMATRIXM 4
#define BDMATRIXN 4
#define BDMATRIXK 4

    int rank, i, j, k;
    int size;

    ASSERT_TEST_START(TEST_NAME);

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    size_t fullM = BDMATRIXM * BDSUBM;
    size_t fullN = BDMATRIXN * BDSUBN;
    size_t fullK = BDMATRIXK * BDSUBK;
    size_t Act = fullM * fullK;
    size_t Bct = fullK * fullN;
    size_t Cct = fullM * fullN;

    GDS_gds_t gds_A,gds_B,gds_C;
    GDS_size_t Asize[2] = {fullM, fullK};
    GDS_size_t Ahi[2] = {fullM - 1, fullK - 1};
    GDS_size_t Bsize[2] = {fullK, fullN};
    GDS_size_t Bhi[2] = {fullK - 1, fullN - 1};
    GDS_size_t Csize[2] = {fullM, fullN};
    GDS_size_t Chi[2] = {fullM - 1, fullN - 1};
    GDS_size_t min_chunk[2] = {0, 0};
    MPI_Info info;

    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    GDS_alloc(2, Asize, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_A);
    GDS_alloc(2, Bsize, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_B);
    GDS_alloc(2, Csize, min_chunk, GDS_DATA_DBL, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_C);
    MPI_Info_free(&info);

    GDS_size_t offsetzero[2] = {0, 0};
    size_t A_ld[1] = {fullM};
    size_t B_ld[1] = {fullK};
    size_t C_ld[1] = {fullM};

    double A[Act];
    double B[Bct];
    double *C = NULL, *Clocal = NULL;
    if (rank == 0) {
        C = (double *)malloc(Cct * sizeof(double));
        Clocal = (double *)malloc(Cct * sizeof(double));
        for (i = 0; i < Act; ++i) A[i] = (2*(i + 2)/3)%11;
        for (i = 0; i < Bct; ++i) B[i] = (6*(i + 1)/7)%17;
        for (i = 0; i < Cct; ++i) C[i] = 0;
        for (i = 0; i < Cct; ++i) Clocal[i] = 0;
        dprintmatrix(DPRINT_INFO,  fullM, fullK, A, "Initial A"); 
        dprintmatrix(DPRINT_INFO,  fullK, fullN, B, "Initial B"); 
        dprintmatrix(DPRINT_INFO,  fullM, fullN, C, "Initial C");
        dprintmatrix(DPRINT_INFO,  fullM, fullN, Clocal, "Initial locally computed C");

        local_dgemm(A, B, Clocal, fullM, fullN, fullK);
        dprintmatrix(DPRINT_INFO, fullM, fullN, Clocal, "Locally Computed C"); 

        GDS_put((void *)A, A_ld, offsetzero, Ahi, gds_A); 
        GDS_put((void *)B, B_ld, offsetzero, Bhi, gds_B); 
        GDS_put((void *)C, C_ld, offsetzero, Chi, gds_C); 
    }

    GDS_fence(NULL);

    //For the sake of diversity, we will divide processes by columns of blocks in A
    int colblocksperproc = BDMATRIXK / size;
    int remainder = BDMATRIXK % size;
    int startk = rank * colblocksperproc;
    int hilimit = startk + colblocksperproc;
    if (rank == size - 1) hilimit += remainder;

    size_t Ablkct = BDSUBM * BDSUBK;
    size_t Bblkct = BDSUBK * BDSUBN;
    size_t Cblkct = BDSUBM * BDSUBN;

    double Ablkbuf[Ablkct];
    double Bblkbuf[Bblkct];
    double Cblkbuf[Cblkct];

    GDS_size_t Amylo[2] = {0,0};
    GDS_size_t Amyhi[2] = {0,0};
    GDS_size_t Bmylo[2] = {0,0};
    GDS_size_t Bmyhi[2] = {0,0};
    GDS_size_t Cmylo[2] = {0,0};
    GDS_size_t Cmyhi[2] = {0,0};
    size_t Ablk_ld[1] = {BDSUBM};
    size_t Bblk_ld[1] = {BDSUBK};	
    size_t Cblk_ld[1] = {BDSUBM};	

    for (i = 0; i < BDMATRIXM; ++i) {
        Amylo[0] = Cmylo[0] = i * BDSUBM;
        Amyhi[0] = Cmyhi[0] = (i + 1) * BDSUBM - 1;
        for (j = 0; j < BDMATRIXN; ++j) {
            Bmylo[1] = Cmylo[1] = j * BDSUBN;
            Bmyhi[1] = Cmyhi[1] = (j + 1) * BDSUBN - 1;
            for (k = startk; k < hilimit; ++k) {
                Amylo[1] = Bmylo[0] = k * BDSUBK;
                Amyhi[1] = Bmyhi[0] = (k + 1) * BDSUBK - 1;
                pr_info("\n(i,j,k)=(%d,%d,%d)\n\n", i, j, k);

                GDS_get((void *)Ablkbuf, Ablk_ld, Amylo, Amyhi, gds_A);
                GDS_get((void *)Bblkbuf, Bblk_ld, Bmylo, Bmyhi, gds_B);

                GDS_wait(gds_A);                
                dprintmatrix(DPRINT_INFO, BDSUBM, BDSUBK, Ablkbuf, "Ablock");

                GDS_wait(gds_B);
                dprintmatrix(DPRINT_INFO, BDSUBK, BDSUBN, Bblkbuf, "Bblock");

                local_dgemm(Ablkbuf, Bblkbuf, Cblkbuf, BDSUBM, BDSUBN, BDSUBK);
                dprintmatrix(DPRINT_INFO, BDSUBM, BDSUBN, Cblkbuf, "Cblock");
                GDS_acc((void *)Cblkbuf, Cblk_ld, Cmylo, Cmyhi, (GDS_op_t)MPI_SUM, gds_C);
                GDS_wait(gds_C);
            }
        }
    }

    GDS_fence(NULL);

    if (rank == 0) {
        GDS_get((void *)C, C_ld, offsetzero, Chi, gds_C);
        GDS_wait(gds_C); 
        dprintmatrix(DPRINT_INFO, fullM, fullN, C, "C");
        int idx;
        for (i = 0; i < fullM; ++i)
            for (j = 0; j < fullN; ++j) {
                idx = j * fullM + i;
                ASSERT_EQUAL_DOUBLE(C[idx], Clocal[idx], TOL);
            }
        free(C);
        free(Clocal);
    }

    GDS_fence(NULL);

    GDS_free(&gds_A);
    GDS_free(&gds_B);
    GDS_free(&gds_C);
    
    ASSERT_TEST_END();
}
