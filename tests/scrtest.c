/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdio.h>
#include <stdlib.h>
#include <gds.h>
#include <mpi.h>
#include <string.h>

int main(int argc, char *argv[])
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {1024};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1, i, k;
    GDS_size_t lo[] = {0}, hi[] = {0}, ld[] = {0};
    char *ver = "version one";
    int rank, size;

    GDS_thread_support_t provd_support;
    GDS_init(&argc, &argv, MPI_THREAD_MULTIPLE, &provd_support);
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    int *put_buf;
    int bufsize = 1024/size;
    put_buf = (int *)malloc(sizeof(int)*bufsize);
    lo[0] = rank*bufsize;
    hi[0] = (rank+1)*bufsize-1;

    GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds);

    for(k = 0; k < 4; k++) {

        for(i = 0; i < bufsize; i++) {
            put_buf[i] = rank;
        }

        GDS_put(put_buf, ld, lo, hi, gds);
        GDS_version_inc(gds, 1, ver, strlen(ver));

    }

    free(put_buf);
    GDS_free(&gds);
    GDS_finalize();
}
