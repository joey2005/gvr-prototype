/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  Functional test suite for Distributed Metadata Service

  Accesses to the internal structures may be allowed.
  However, such access shall be easily influenced by
  minor changes in the implementation details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "common.h"
#include <gds_internal.h>

int dprint_level = 0;

#undef  TEST_NAME
#define TEST_NAME "pca calculation test"
static void test_pca_cal(void)
{
    FILE *fp;
    int i, j, case_num;
    int owner_num, nonowner_num, version_num, chunk_number;
    int rank, pca_target_rank, correct_pca_rank;

    ASSERT_TEST_START(TEST_NAME);

    GDS_comm_rank(GDS_COMM_WORLD, &rank);

    if (rank == 0) {
        fp = fopen("pca_cal_cases", "r");
        if (fp == NULL) {
            printf("Can't open file pca_cal_cases.\n");
            return;
        }
        fscanf(fp, "%d", &case_num);

        for(i = 0; i < case_num; i++) {
            fscanf(fp, "%d,", &owner_num);
            fscanf(fp, "%d,", &nonowner_num);
            fscanf(fp, "%d,", &version_num);
            fscanf(fp, "%d", &chunk_number);

            for (j = 0; j < chunk_number; j++) {
                pca_target_rank = gdsi_get_pca_target_rank(j, owner_num,
                    nonowner_num, version_num);
                fscanf(fp, "%d,", &correct_pca_rank);
                ASSERT_EQUAL_INT(pca_target_rank, correct_pca_rank);
            }
        }

    }

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "coordinates flattening"
static void test_coord(void)
{
    size_t dims_a[] = {3, 5, 7};
    size_t dims_b[] = {4, 4, 8};
    size_t i, j, k, lr;

    ASSERT_TEST_START(TEST_NAME);

    lr = 0;
    for (k = 0; k < 3; k++) {
        for (j = 0; j < 5; j++) {
            for (i = 0; i < 7; i++) {
                size_t cr[] = {k, j, i};
                size_t c[3];
                size_t l = gdsi_linearize_index_impl(3, dims_a, cr);
                ASSERT_EQUAL_INT(l, lr);
                gdsi_decompose_index_impl(l, 3, dims_a, c);
                ASSERT_EQUAL_INT(memcmp(cr, c, sizeof(c)), 0);
                lr++;
            }
        }
    }

    lr = 0;
    for (k = 0; k < 4; k++) {
        for (j = 0; j < 4; j++) {
            for (i = 0; i < 8; i++) {
                size_t cr[] = {k, j, i};
                size_t c[3];
                size_t l = gdsi_linearize_index_impl(3, dims_b, cr);
                ASSERT_EQUAL_INT(l, lr);
                gdsi_decompose_index_impl(l, 3, dims_b, c);
                ASSERT_EQUAL_INT(memcmp(cr, c, sizeof(c)), 0);
                lr++;
            }
        }
    }

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "iterator"
static void test_iter(void)
{
    GDS_gds_t gds = NULL;
    const size_t arraylen = 1024;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    gdsi_chunk_iterator_t it;
    struct gdsi_chunk chunk;
    size_t total_size = 0;
    size_t index;

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    index = arraylen;
    ASSERT_EQUAL_INT(
        gdsi_find_first_chunkit(gds, &index, &it), GDS_STATUS_RANGE);
    index = arraylen + 1;
    ASSERT_EQUAL_INT(
        gdsi_find_first_chunkit(gds, &index, &it), GDS_STATUS_RANGE);
    index = arraylen + 2;
    ASSERT_EQUAL_INT(
        gdsi_find_first_chunkit(gds, &index, &it), GDS_STATUS_RANGE);

    index = 0;
    ASSERT_STAT("gdsi_find_first_chunkit",
        gdsi_find_first_chunkit(gds, &index, &it));

    for (; !gdsi_chunkit_end(&it); gdsi_chunkit_forward(&it)) {
        gdsi_chunkit_access(&it, &chunk);

        ASSERT_COND(chunk.size > 0);
        total_size += chunk.size;
    }
    ASSERT_EQUAL_INT(total_size, 1024);

    gdsi_chunkit_release(&it);

    ASSERT_STAT("GDS_free", GDS_free(&gds));

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "verion increment"
static void test_verinc(void)
{
    GDS_gds_t gds = NULL;
    const size_t arraylen = 1024;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    int ndim = 1;
    gdsi_chunk_iterator_t it;
    struct gdsi_chunk chunk;
    size_t total_size = 0;
    size_t index;

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim,cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    ASSERT_EQUAL_INT(gds->common->latest_version, 0);
    ASSERT_EQUAL_INT(gds->chunk_set->version, 0);
    ASSERT_STAT("GDS_version_inc",
        GDS_version_inc(gds, 0, NULL, 0));
    ASSERT_EQUAL_INT(gds->common->latest_version, 0);
    ASSERT_STAT("GDS_version_inc",
        GDS_version_inc(gds, 1, NULL, 0));
    ASSERT_EQUAL_INT(gds->chunk_set->version, 1);
    ASSERT_EQUAL_INT(gds->common->latest_version, 1);

    index = 0;
    ASSERT_STAT("gdsi_find_first_chunkit",
        gdsi_find_first_chunkit(gds, &index, &it));
    printf("find first chunkit\n");
    for (; !gdsi_chunkit_end(&it); gdsi_chunkit_forward(&it)) {
        gdsi_chunkit_access(&it, &chunk);

        ASSERT_COND(chunk.size > 0);
        total_size += chunk.size;
    }
    ASSERT_EQUAL_INT(total_size, 1024);

    gdsi_chunkit_release(&it);

    ASSERT_STAT("GDS_free", GDS_free(&gds));
}

#undef  TEST_NAME
#define TEST_NAME "indirect fetch"
static void test_fetch(void)
{
    GDS_gds_t gds, gds_firstver;
    const size_t arraylen = 4;
    GDS_size_t cts[] = {arraylen};
    GDS_size_t min_chunk[] = {0};
    GDS_size_t lo[] = {0};
    GDS_size_t hi[] = {3};
    GDS_size_t ld[] = {};
    int ndim = 1;
    int testdata[][4] = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}};
    int buff[4];
    gdsi_chunk_iterator_t it;
    struct gdsi_chunk chunk;
    size_t o = 0;
    size_t index;
    int i = 0;

    ASSERT_TEST_START(TEST_NAME);

    ASSERT_STAT_FATAL("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &gds));
    ASSERT_NOT_NULL_FATAL(gds);

    ASSERT_STAT("GDS_put",
        GDS_put((void *)testdata[0], ld, lo, hi, gds));
    GDS_fence(gds);

    index = 0;
    ASSERT_STAT("gdsi_find_first_chunkit",
        gdsi_find_first_chunkit(gds, &index, &it));

    for (o = 0, i = 0; !gdsi_chunkit_end(&it); gdsi_chunkit_forward(&it)) {
        GDS_status_t status;
        gdsi_chunkit_access(&it, &chunk);
        status = gdsi_indirect_fetch(gds, chunk.target_rank, buff, 0,
            chunk.size, chunk.chunk_number, NULL);
        ASSERT_STAT("gdsi_indirect_fetch", status);
        if (status != GDS_STATUS_OK)
            pr_err("status=%d\n", status);
        ASSERT_EQUAL_INT(
            memcmp(testdata[0]+o, buff, chunk.size), 0);
        o += chunk.size;
        i++;
    }

    gdsi_chunkit_release(&it);

    ASSERT_TEST_GOING();

    ASSERT_STAT_FATAL("GDS_descriptor_clone",
        GDS_descriptor_clone(gds, &gds_firstver));

    ASSERT_STAT("GDS_version_inc",
        GDS_version_inc(gds, 1, NULL, 0));
    ASSERT_STAT("GDS_put",
        GDS_put((void *)testdata[1], ld, lo, hi, gds));
    GDS_fence(gds);

    index = 0;
    ASSERT_STAT("gdsi_find_first_chunkit",
        gdsi_find_first_chunkit(gds, &index, &it));

    for (o = 0, i = 0; !gdsi_chunkit_end(&it); gdsi_chunkit_forward(&it)) {
        gdsi_chunkit_access(&it, &chunk);
        ASSERT_STAT("gdsi_indrect_fetch",
            gdsi_indirect_fetch(gds, chunk.target_rank, buff, 0,
                chunk.size, chunk.chunk_number, NULL));
        ASSERT_EQUAL_INT(memcmp(testdata[1]+o, buff, chunk.size), 0);
        o += chunk.size;
        i++;
    }
    gdsi_chunkit_release(&it);

    ASSERT_TEST_GOING();

    index = 0;
    ASSERT_STAT("gdsi_find_first_chunkit",
        gdsi_find_first_chunkit(gds_firstver, &index, &it));

    for (o = 0, i = 0; !gdsi_chunkit_end(&it); gdsi_chunkit_forward(&it)) {
        gdsi_chunkit_access(&it, &chunk);
        ASSERT_STAT("gdsi_indirect_fetch",
            gdsi_indirect_fetch(gds_firstver, chunk.target_rank,
                buff, 0, chunk.size, chunk.chunk_number, NULL));
        ASSERT_EQUAL_INT(memcmp(testdata[0]+o, buff, chunk.size), 0);
        o += chunk.size;
    }
    gdsi_chunkit_release(&it);

    ASSERT_TEST_GOING();

    ASSERT_STAT("GDS_free", GDS_free(&gds));
    ASSERT_TEST_GOING();
    ASSERT_STAT("GDS_free", GDS_free(&gds_firstver));

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "minimum chunk"
static void validate_minchunk_split(size_t np, size_t nd, size_t dims[],
    size_t min_chunks[], size_t ind_exp[np][nd], size_t len_exp[np][nd])
{
    size_t i;
    size_t ind[np][nd];
    size_t len[np][nd];
    gdsi_minchunk_t *mc;
    size_t max_size_exp = 0;

    mc = gdsi_minchunk_split(np, nd, dims, min_chunks);
    ASSERT_NOT_NULL_FATAL(mc);

    for (i = 0; i < np; i++) {
        int j;
        size_t tot_size_exp = 1;
        gdsi_minchunk_get(mc, i, ind[i], len[i]);
        for (j = 0; j < nd; j++) {
            ASSERT_EQUAL_INT(ind[i][j], ind_exp[i][j]);
            ASSERT_EQUAL_INT(len[i][j], len_exp[i][j]);
            tot_size_exp *= len_exp[i][j];
        }
        ASSERT_EQUAL_INT(tot_size_exp, gdsi_minchunk_size(mc, i));
        if (max_size_exp < tot_size_exp)
            max_size_exp = tot_size_exp;
    }

    ASSERT_EQUAL_INT(max_size_exp, gdsi_minchunk_maxsize(mc));

    gdsi_minchunk_free(mc);
}

static void test_minchunk(void)
{
    size_t np, nd = 2;

    ASSERT_TEST_START(TEST_NAME);

    {
        np = 5;

        size_t dims[] = {4, 5};
        size_t min_chunks[] = {4, 0};
        size_t ind_exp[5][2] = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}};
        size_t len_exp[5][2] = {{4, 1}, {4, 1}, {4, 1}, {4, 1}, {4, 1}};

        validate_minchunk_split(np, nd, dims, min_chunks, ind_exp, len_exp);
    }

    {
        np = 4;

        size_t dims[] = {4, 4};
        size_t min_chunks[] = {2, 2};
        size_t ind_exp[4][2] = {{0, 0}, {0, 2}, {2, 0}, {2, 2}};
        size_t len_exp[4][2] = {{2, 2}, {2, 2}, {2, 2}, {2, 2}};

        validate_minchunk_split(np, nd, dims, min_chunks, ind_exp, len_exp);
    }

    {
        np = 3;

        size_t dims[] = {4, 4};
        size_t min_chunks[] = {2, 2};
        size_t ind_exp[3][2] = {{0, 0}, {0, 2}, {2, 0}};
        size_t len_exp[3][2] = {{2, 2}, {2, 2}, {2, 4}};

        validate_minchunk_split(np, nd, dims, min_chunks, ind_exp, len_exp);
    }

    {
        np = 4;

        size_t dims[] = {6, 6};
        size_t min_chunks[] = {2, 2};
        size_t ind_exp[4][2] = {{0, 0}, {0, 4}, {2, 0}, {4, 0}};
        size_t len_exp[4][2] = {{2, 4}, {2, 2}, {2, 6}, {2, 6}};

        validate_minchunk_split(np, nd, dims, min_chunks, ind_exp, len_exp);
    }

    {
        np = 2;

        size_t dims[] = {1, 1};
        size_t min_chunks[] = {0, 0};
        size_t ind_exp[2][2] = {{0, 0}, {0, 0}};
        size_t len_exp[2][2] = {{0, 0}, {1, 1}};

        validate_minchunk_split(np, nd, dims, min_chunks, ind_exp, len_exp);
    }

    {
        np = 5;

        size_t dims[] = {2, 2};
        size_t min_chunks[] = {0, 0};
        size_t ind_exp[5][2] = {{0, 0}, {0, 0}, {0, 1}, {1, 0}, {1, 1}};
        size_t len_exp[5][2] = {{0, 0}, {1, 1}, {1, 1}, {1, 1}, {1, 1}};

        validate_minchunk_split(np, nd, dims, min_chunks, ind_exp, len_exp);
    }

    ASSERT_TEST_END();
}

static void test_minchunk_issue59(void)
{
    const int xcnt = 5, ycnt = 4;
    int np = 32, nd = 2, i;
    size_t dims[] = {ycnt, xcnt}, min_chunks[] = {0, 0};
    gdsi_minchunk_t *mc;
    size_t ind[2], len[2];

    ASSERT_TEST_START("minchunk test for Issue #59");

    mc = gdsi_minchunk_split(np, nd, dims, min_chunks);
    ASSERT_NOT_NULL_FATAL(mc);

    for (i = 0; i < np - ycnt*xcnt; i++) {
        gdsi_minchunk_get(mc, i, ind, len);
        ASSERT_EQUAL_INT(len[0], 0);
        ASSERT_EQUAL_INT(len[1], 0);
    }

    gdsi_minchunk_free(mc);

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "mutex use"
static void test_use_mutex(void)
{
    int IAXPYM = 20;
    if (IAXPYM < nprocs)
        IAXPYM = 2 * nprocs;

    GDS_gds_t x;

    int ndim = 1;
    int inbufx[IAXPYM];

    int i, rank;
    int size;

    struct gdsi_mutex gds_mtx;

    ASSERT_TEST_START(TEST_NAME);

    for (i=0;i<IAXPYM;++i) {
        inbufx[i] = i + 1;
    }

    GDS_size_t cts[] = {IAXPYM};
    GDS_size_t min_chunk[] = {0};
    size_t ld[] = {};

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    ASSERT_STAT("GDS_alloc",
        GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
            GDS_COMM_WORLD, MPI_INFO_NULL, &x));

    size_t mysize = (IAXPYM)/size;

    GDS_size_t mylo[] = {rank * mysize};
    GDS_size_t myhi[] = {(rank + 1) * mysize - 1};

    int tmpbufx[mysize];

    if (rank == 0) {
        ASSERT_STAT("GDS_put",
            GDS_put(inbufx, ld, mylo, myhi, x));
    }

    GDS_fence(x);

    ASSERT_STAT_FATAL("gdsi_mutex_init",
        gdsi_mutex_init(&gds_mtx, GDS_COMM_WORLD));

    ASSERT_STAT_FATAL("gdsi_mutex_lock",
        gdsi_mutex_lock(&gds_mtx));


    ASSERT_STAT("GDS_put",
        GDS_put((void *)tmpbufx, ld, mylo, myhi, x));

    ASSERT_STAT_FATAL("gdsi_mutex_unlock",
        gdsi_mutex_unlock(&gds_mtx));

    ASSERT_STAT_FATAL("gdsi_mutex_destroy",
        gdsi_mutex_destroy(&gds_mtx));

    GDS_free(&x);

    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "mutex sum"

#define MTX_SUM_TOTAL_LOOP 10000
static void test_mutex_sum(void)
{
    int i,rank;

    int size;
    GDS_gds_t x;
    int ndim = 1;
    int inbufx[1]={0};

    int loop_per_proc = MTX_SUM_TOTAL_LOOP / nprocs;
    if (loop_per_proc < 1)
        loop_per_proc = 1;

    int tmpbufx[1]={0};

    GDS_size_t cts[] = {1};
    GDS_size_t min_chunk[] = {0};
    GDS_size_t lo[] = {0};
    GDS_size_t hi[] = {0};

    size_t ld[] = {};

    struct gdsi_mutex gds_mtx;

    ASSERT_TEST_START(TEST_NAME);

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &size);

    ASSERT_STAT("GDS_alloc", GDS_alloc(ndim, cts, min_chunk,
        GDS_DATA_INT, GDS_PRIORITY_HIGH, GDS_COMM_WORLD, MPI_INFO_NULL, &x));

    ASSERT_STAT_FATAL("gdsi_mutex_init",
        gdsi_mutex_init(&gds_mtx,GDS_COMM_WORLD));

    if (rank == 0) {
        ASSERT_STAT("GDS_put",
            GDS_put((void *)inbufx, ld, lo, hi, x));
    }

    GDS_fence(x);

    for (i=0; i < loop_per_proc; i++) {
        /* read-modify-write loop */
        ASSERT_STAT_FATAL("gdsi_mutex_lock",
            gdsi_mutex_lock(&gds_mtx));

        ASSERT_STAT("GDS_get",
            GDS_get((void *)tmpbufx, ld, lo, hi, x));
        GDS_wait(x);

        inbufx[0]=++tmpbufx[0];

        ASSERT_STAT("GDS_put",
            GDS_put((void *)inbufx, ld, lo, hi, x));
        GDS_wait(x);

        ASSERT_STAT_FATAL("gdsi_mutex_unlock",
            gdsi_mutex_unlock(&gds_mtx));
    }

    ASSERT_STAT_FATAL("gdsi_mutex_destroy",
        gdsi_mutex_destroy(&gds_mtx));

    if (rank == 0) {
        ASSERT_STAT("GDS_get",
            GDS_get((void *)tmpbufx, ld, lo, hi, x));
        GDS_wait(x);

        ASSERT_EQUAL_INT(tmpbufx[0], loop_per_proc * size);
    }

    GDS_free(&x);
    ASSERT_TEST_END();
}

#undef  TEST_NAME
#define TEST_NAME "bitvec ffs"
static void test_bitvec_ffs(void)
{
    int exp[] = {0, 7, 8, 10, 20, 29, 30, 31, 32, 35, 36, 37, 100};
    int i, j, max = 120;
    struct gdsi_bitvec *bv;

    ASSERT_TEST_START(TEST_NAME);

    bv = gdsi_bitvec_new(max);
    ASSERT_NOT_NULL_FATAL(bv);

    for (i = 0; i < sizeof(exp)/sizeof(int); i++)
        gdsi_bitvec_set(bv, exp[i]);

    for (i = j = 0; i < sizeof(exp)/sizeof(int); i++) {
        int r = gdsi_bitvec_ffs(bv, j);
        pr_info("r=%d, j=%d, exp=%d\n", r, j, exp[i]);
        ASSERT_EQUAL_INT(r, exp[i]);
        j = r + 1;
    }
    ASSERT_COND(gdsi_bitvec_ffs(bv, j) < 0);

    ASSERT_TEST_END();
}

#undef TEST_NAME
#define TEST_NAME "vaddr to gds"
void assert_response(struct gdsi_vaddr_to_gds_response *response,
    GDS_gds_t gds, GDS_size_t ct, GDS_size_t start[],
    struct gdsi_vaddr_to_gds_response **next) {
    ASSERT_NOT_NULL_FATAL(response);
    struct gdsi_gds_common *gds_common = gds->common;
    ASSERT_EQUAL_PTR(gds_common, response->common);
    ASSERT_EQUAL_INT(ct, response->ct);
    size_t ndims = response->common->ndims;
    int dim;
    for (dim = 0; dim < ndims; ++dim) {
        ASSERT_EQUAL_INT(response->start[dim], start[dim]);
    }
    *next = response->next;
    free(response);
}

size_t mat_major_axis, mat_minor_axis, mat_dims[2];

GDS_status_t block_simple_global_to_local(const GDS_size_t global_indices[],
    int *local_rank, GDS_size_t *local_offset)
{
    int comm_size;
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    int major_unit_per_proc = mat_dims[mat_major_axis] / comm_size;
    *local_rank = global_indices[mat_major_axis] / (major_unit_per_proc);
    *local_offset = global_indices[mat_major_axis] * mat_dims[mat_minor_axis]
        + global_indices[mat_minor_axis] - (*local_rank) * major_unit_per_proc
        * mat_dims[mat_minor_axis];
    return GDS_STATUS_OK;
}

GDS_status_t block_simple_local_to_global(GDS_size_t local_offset,
    GDS_size_t *global_indices)
{
    int comm_size;
    int comm_rank;
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);
    GDS_comm_rank(GDS_COMM_WORLD, &comm_rank);
    int major_unit_per_proc = mat_dims[mat_major_axis] / comm_size;
    global_indices[mat_major_axis] = local_offset / mat_dims[mat_minor_axis]
        + comm_rank * major_unit_per_proc;
    global_indices[mat_minor_axis] = local_offset % mat_dims[mat_minor_axis];
    return GDS_STATUS_OK;
}

static void test_vaddr_to_gds(void) {
    struct gdsi_vaddr_to_gds_response *responses;
    size_t n_responses;
    GDS_size_t local_start_row, local_stop_row;

    ASSERT_TEST_START(TEST_NAME);
    int rank, comm_size;
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    GDS_comm_size(GDS_COMM_WORLD, &comm_size);

    //1D GDS
    GDS_gds_t gds1 = NULL;
    size_t gds1_ct = 4;
    GDS_size_t cts1[] = {gds1_ct * comm_size};
    GDS_size_t min_chunk1[] = {0};
    int ndim = 1;

    GDS_alloc(ndim,cts1,min_chunk1, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, MPI_INFO_NULL, &gds1);

    GDS_size_t local_start = gds1_ct * rank;
    GDS_size_t local_stop = gds1_ct * (rank + 1) - 1;
    GDS_size_t lo[] = {local_start};
    GDS_size_t hi[] = {local_stop};
    GDS_access_handle_t handle1;
    void *buffer1;
    GDS_access(gds1, lo, hi, GDS_ACCESS_BUFFER_DIRECT, &buffer1, &handle1);

    gdsi_vaddr_to_gds(buffer1, 1, &n_responses, &responses);
    size_t start1[] = {local_start};
    assert_response(responses, gds1, 1, start1, &responses);
    ASSERT_EQUAL_PTR(responses, NULL);

    // make sure we input number of bytes and output number of entries
    gdsi_vaddr_to_gds(buffer1+1, 2, &n_responses, &responses);
    size_t start2[] = {local_start};
    assert_response(responses, gds1, 1, start2, &responses);
    ASSERT_EQUAL_PTR(responses, NULL);

    // slightly more complicated offsets and lens
    gdsi_vaddr_to_gds(buffer1+5, 4, &n_responses, &responses);
    size_t start3[] = {local_start + 1};
    assert_response(responses, gds1, 2, start3, &responses);
    ASSERT_EQUAL_PTR(responses, NULL);


    //2D GDS
    GDS_gds_t gds2 = NULL;
    GDS_size_t gds2_rows = 4;
    local_start_row = gds2_rows * rank;
    local_stop_row = gds2_rows * (rank + 1) - 1;
    size_t cts2[] = {gds2_rows * comm_size, 5};
    GDS_size_t min_chunk2[] = {0, 0};
    int ndim2 = 2;

    GDS_alloc(ndim2,cts2,min_chunk2, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, MPI_INFO_NULL, &gds2);

    GDS_size_t lo2[] = {local_start_row, 0};
    GDS_size_t hi2[] = {local_stop_row, cts2[1] - 1};
    GDS_access_handle_t handle2;
    void *buffer2;
    GDS_access(gds2, lo2, hi2, GDS_ACCESS_BUFFER_DIRECT, &buffer2, &handle2);

    gdsi_vaddr_to_gds(buffer2 + 12, 20, &n_responses, &responses);
    size_t start4[] = {local_start_row + 1, 0};
    assert_response(responses, gds2, 3, start4, &responses);
    size_t start5[] = {local_start_row, 3};
    assert_response(responses, gds2, 2, start5, &responses);
    ASSERT_EQUAL_PTR(responses, NULL);


    /* 2 arrays. Unfortunately, this test is nondeterministic, since we don't
     * know for sure what our virtual addresses are. Probably,
     * buffer2 < buffer1, but we will test it both ways
     */
    void *err_start;
    size_t len;
    if ((size_t)buffer1 < (size_t)buffer2) {
        err_start = buffer1 + (gds1_ct - 1) * 4 ; // last elmt of gds1
        len = (size_t)buffer2 - (size_t)err_start + 1; // first elmt of gds2
        gdsi_vaddr_to_gds(err_start, len, &n_responses, &responses);
        size_t start6[] = {3 + local_start};
        assert_response(responses, gds1, 1, start6, &responses);
        size_t start7[] = {local_start_row, 0};
        assert_response(responses, gds2, 1, start7, &responses);
        ASSERT_EQUAL_PTR(responses, NULL);
    } else {
        err_start = buffer2 + (gds2_rows * cts2[1] -1 ) * 4; // last elmt of gds2
        len = (size_t)buffer1 - (size_t)err_start + 1; // first elmt of gds1
        gdsi_vaddr_to_gds(err_start, len, &n_responses, &responses);
        size_t start6[] = {3 + local_start_row, 4};
        assert_response(responses, gds2, 1, start6, &responses);
        size_t start7[] = {local_start_row};
        assert_response(responses, gds1, 1, start7, &responses);
        ASSERT_EQUAL_PTR(responses, NULL);
    }

    GDS_release(handle2);
    ASSERT_STAT("GDS_free", GDS_free(&gds2));

    GDS_release(handle1);

    ASSERT_STAT("GDS_free", GDS_free(&gds1));

    //create test
    double *local_buffer;
    GDS_size_t local_count;
    MPI_Info info;
    GDS_gds_t gds3;
    GDS_size_t dims_local[2];
    mat_major_axis = 0;
    mat_minor_axis = 1 - mat_major_axis;
    GDS_size_t rows_per_proc = 2;
    dims_local[0] = rows_per_proc;
    mat_dims[0] = rows_per_proc * comm_size;
    dims_local[1] = 3;
    mat_dims[1] = dims_local[1];
    local_count = dims_local[0] * dims_local[1];
    local_buffer = (double *)malloc(local_count * sizeof(*local_buffer));
    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_ROW_MAJOR);
    GDS_create(2, mat_dims, GDS_DATA_DBL, block_simple_global_to_local,
    block_simple_local_to_global, local_buffer, local_count,
    GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds3);
    MPI_Info_free(&info);
    local_start_row = rows_per_proc * rank;
    local_stop_row = rows_per_proc * (rank + 1) - 1;

    gdsi_vaddr_to_gds(((void *)local_buffer) + 16, 16, &n_responses, &responses);
    size_t start8[] = {local_start_row + 1, 0};
    assert_response(responses, gds3, 1, start8, &responses);
    size_t start9[] = {local_start_row, 2};
    assert_response(responses, gds3, 1, start9, &responses);
    ASSERT_EQUAL_PTR(responses, NULL);

    GDS_free(&gds3);

    //2D GDS column oriented
    GDS_gds_t gds4 = NULL;
    GDS_size_t gds4_cols = 4;
    GDS_size_t local_start_col = gds4_cols * rank;
    GDS_size_t local_stop_col = gds4_cols * (rank + 1) - 1;
    size_t cts4[] = {3, gds4_cols * comm_size};
    GDS_size_t min_chunk4[] = {0, 0};
    int ndim4 = 2;
    MPI_Info info2;
    MPI_Info_create(&info2);
    MPI_Info_set(info2, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    GDS_alloc(ndim4,cts4,min_chunk4, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, info2, &gds4);
    MPI_Info_free(&info2);

    GDS_size_t lo4[] = {0, local_start_col};
    GDS_size_t hi4[] = {cts4[0] - 1, local_stop_col};
    GDS_access_handle_t handle3;
    void *buffer4;
    GDS_access(gds4, lo4, hi4, GDS_ACCESS_BUFFER_DIRECT, &buffer4, &handle3);

    gdsi_vaddr_to_gds(buffer4 + 8, 12, &n_responses, &responses);
    size_t start10[] = {0, local_start_col + 1};
    assert_response(responses, gds4, 2, start10, &responses);
    size_t start11[] = {2, local_start_col};
    assert_response(responses, gds4, 1, start11, &responses);
    ASSERT_EQUAL_PTR(responses, NULL);

    GDS_release(handle3);
    ASSERT_STAT("GDS_free", GDS_free(&gds4));
    ASSERT_TEST_END();
}

int main(int argc, char **argv)
{
    int opt, ret = 0;
    GDS_thread_support_t support_prov;

    ASSERT_INIT();

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &support_prov);

    GDS_comm_size(GDS_COMM_WORLD, &nprocs);
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    while ((opt = getopt(argc, argv, "dx:")) != -1) {
        switch (opt) {
        case 'd':
            dprint_level++;
            break;

        default:
            fprintf(stderr,
                    "Usage: dms_test [options]\n"
                    "\n"
                    "Options:\n"
                    "  -d                  increase debug output level\n");
            ret = 1;
            goto out;
        }
    }

    /* list tests here */
    test_pca_cal();
    test_coord();
    test_iter();
    test_verinc();
    test_fetch();
    test_minchunk();
    test_minchunk_issue59();
    test_use_mutex();
    test_mutex_sum();
    test_bitvec_ffs();
    test_vaddr_to_gds();
out:
    ASSERT_REPORT();
    GDS_finalize();

    return ret;
}
