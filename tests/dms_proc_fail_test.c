/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdio.h>
#include <unistd.h>

#include <gds.h>

#include "../src/gds_internal.h"

#include "common.h"
#define FAILED_PROC 1

int dprint_level = 0;

#define COUNT_PER_PROC 32

static GDS_status_t handle_proc_failure(GDS_gds_t gds, GDS_error_t desc)
{
    GDS_comm_t comm_buff;
    MPI_Group group_buff, world_group, compare_group;
    int failed_proc = FAILED_PROC;
    int flag;
    int size;
    int64_t n_ranges;
    GDS_size_t count, offset;

    printf("[rank %d] handle_proc_failure() called\n", myrank);

    GDS_get_error_attr(desc, GDS_EATTR_LOST_COMMUNICATOR, &comm_buff, &flag);
    ASSERT_COND(flag);
    GDS_comm_size(comm_buff, &size);
    ASSERT_EQUAL_INT(size, nprocs);

    GDS_get_comm(gds, &comm_buff);
    GDS_comm_size(comm_buff, &size);
    ASSERT_EQUAL_INT(size, (myrank == FAILED_PROC) ? 1 : nprocs - 1);

    GDS_get_error_attr(desc, GDS_EATTR_LOST_PROCESSES,
        &group_buff, &flag);
    ASSERT_COND(flag);

    MPI_Comm_group(MPI_COMM_WORLD, &world_group);
    MPI_Group_incl(world_group, 1, &failed_proc, &compare_group);
    MPI_Group_compare(group_buff, compare_group, &flag);
    ASSERT_EQUAL_INT(flag, MPI_IDENT);
    MPI_Group_free(&world_group);
    MPI_Group_free(&compare_group);

    GDS_get_error_attr(desc, GDS_EATTR_MEMORY_N_RANGES,
        &n_ranges, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(n_ranges, 1);

    GDS_get_error_attr(desc, GDS_EATTR_GDS_INDEX,
        &offset, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(offset, COUNT_PER_PROC * FAILED_PROC);

    GDS_get_error_attr(desc, GDS_EATTR_GDS_COUNT,
        &count, &flag);
    ASSERT_COND(flag);
    ASSERT_EQUAL_INT(count, COUNT_PER_PROC);

    GDS_resume_global(gds, desc);

    return GDS_STATUS_OK;
}

static int new_owner(void)
{
    int new = FAILED_PROC + 1;
    if (new >= nprocs)
        new = 0;

    return new;
}

static int new_rank(int old_rank)
{
    if (old_rank > FAILED_PROC)
        return old_rank - 1;
    else
        return old_rank;
}

static void verify_metadata_consistency(struct gdsi_gds_common *common,
    struct gdsi_chunk_set *cset)
{
    const struct gdsi_chunk_desc *cd;
    int i, rank, size, next, prev;

    MPI_Comm_rank(common->comm, &rank);
    MPI_Comm_size(common->comm, &size);
    next = (rank + 1) % size;
    prev = (rank == 0) ? size - 1 : rank - 1;

    cd = cset->chunk_descs;

    for (i = 0; i < common->nchunk_descs; i++) {
        MPI_Request reqs[6];
        int n_reqs = 0;
        int my_trank, neigh_trank;
        size_t my_tidx, neigh_tidx;
        size_t my_size, neigh_size;

        my_trank = cd[i].target_rank;
        my_tidx  = cd[i].target_index;
        my_size  = cd[i].size_flat;

        MPI_Isend(&my_trank, 1, MPI_INT, next, 0, common->comm, &reqs[n_reqs++]);
        MPI_Irecv(&neigh_trank, 1, MPI_INT, prev, 0, common->comm, &reqs[n_reqs++]);
        MPI_Isend(&my_tidx, 1, MPI_UNSIGNED_LONG, next, 0, common->comm, &reqs[n_reqs++]);
        MPI_Irecv(&neigh_tidx, 1, MPI_UNSIGNED_LONG, prev, 0, common->comm, &reqs[n_reqs++]);
        MPI_Isend(&my_size, 1, MPI_UNSIGNED_LONG, next, 0, common->comm, &reqs[n_reqs++]);
        MPI_Irecv(&neigh_size, 1, MPI_UNSIGNED_LONG, prev, 0, common->comm, &reqs[n_reqs++]);

        ASSERT_EQUAL_INT(n_reqs, sizeof(reqs) / sizeof(reqs[0]));

        MPI_Waitall(n_reqs, reqs, MPI_STATUSES_IGNORE);

        ASSERT_EQUAL_INT(my_trank, neigh_trank);
        ASSERT_EQUAL_INT(my_tidx, neigh_tidx);
        ASSERT_EQUAL_INT(my_size, neigh_size);
    }
}

static void verify_old_metadata(struct gdsi_gds_common *common,
    struct gdsi_chunk_set *cset)
{
    int i;

    for (i = 0; i < common->nchunk_descs; i++) {
        int pca_target = gdsi_get_pca_target_rank(
            i, nprocs, 0, cset->version);
        printf("[rank %d] chunkno=%d ver=%zu pca_target=%d corrupted=%d\n",
            myrank, i, cset->version,
            pca_target, cset->chunk_descs[i].is_corrupted);
        if (pca_target == FAILED_PROC) {
            ASSERT_COND(cset->chunk_descs[i].is_corrupted);
        } else {
            int tr = cset->chunk_descs[i].target_rank;
            ASSERT_COND(!cset->chunk_descs[i].is_corrupted);
            ASSERT_EQUAL_INT(tr, new_rank(tr));
        }
    }
}

#define VERIFY_FAILED_MD()                                              \
    do {                                                                \
        if (myrank == new_owner()) {                                    \
            ASSERT_EQUAL_INT(gds->common->chunk_sets->chunk_descs[myrank].target_index, my_tidx_orig); \
            ASSERT_EQUAL_INT(gds->common->chunk_sets->chunk_descs[myrank].target_rank, \
                gds->common->chunk_sets->chunk_descs[FAILED_PROC].target_rank); \
            ASSERT_EQUAL_INT(gds->common->chunk_sets->chunk_descs[FAILED_PROC].size_flat, \
                my_tidx_orig + gds->common->chunk_sets->chunk_descs[myrank].size_flat); \
            ASSERT_EQUAL_INT(gds->common->chunk_sets->chunk_descs[FAILED_PROC].size_flat, \
                failed_size_orig);                                      \
        }                                                               \
    } while (0)

void basic_test(void)
{
    GDS_gds_t gds = NULL;
    GDS_size_t cts[] = {COUNT_PER_PROC * nprocs};
    GDS_size_t min_chunk[] = {1};
    int ndim = 1;
    GDS_error_pred_t pred;
    GDS_error_pred_term_t term;

    struct gdsi_chunk_set *cset[3];

    size_t my_tidx_orig, failed_size_orig;

    ASSERT_TEST_START("DMS process failure test");

    /* Allocating an array */

    GDS_alloc(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH,
        GDS_COMM_WORLD, MPI_INFO_NULL, &gds);

    my_tidx_orig = gds->common->chunk_sets->chunk_descs[myrank].target_index;
    failed_size_orig = gds->common->chunk_sets->chunk_descs[FAILED_PROC].size_flat;

    cset[0] = gds->common->chunk_sets;

    /* Setting up error handler */

    ASSERT_STAT_FATAL("GDS_create_error_pred",
        GDS_create_error_pred(&pred));

    ASSERT_STAT_FATAL("GDS_create_error_pred_term",
        GDS_create_error_pred_term(GDS_EATTR_LOST_PROCESSES,
            GDS_EMEXP_ANY, 0, NULL,
            &term));
    ASSERT_STAT_FATAL("GDS_add_error_pred_term",
        GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    ASSERT_STAT_FATAL("GDS_create_error_pred_term",
        GDS_create_error_pred_term(GDS_EATTR_LOST_COMMUNICATOR,
            GDS_EMEXP_ANY, 0, NULL,
            &term));
    ASSERT_STAT_FATAL("GDS_add_error_pred_term",
        GDS_add_error_pred_term(pred, term));
    GDS_free_error_pred_term(&term);

    GDS_register_global_error_handler(gds, pred, handle_proc_failure);
    GDS_free_error_pred(&pred);

    verify_metadata_consistency(gds->common, gds->common->chunk_sets);

    ASSERT_STAT_FATAL("GDS_version_inc",
        GDS_version_inc(gds, 1, "", 0));
    verify_metadata_consistency(gds->common, gds->common->chunk_sets);
    cset[1] = gds->common->chunk_sets;

    ASSERT_STAT_FATAL("GDS_version_inc",
        GDS_version_inc(gds, 1, "", 0));
    verify_metadata_consistency(gds->common, gds->common->chunk_sets);
    cset[2] = gds->common->chunk_sets;

    if (myrank == FAILED_PROC)
        printf("Simulating failure for rank %d...\n", FAILED_PROC);

    GDS_simulate_proc_failure(FAILED_PROC);

    printf("[rank %d] After injecting failure\n", myrank);

    VERIFY_FAILED_MD();
    verify_metadata_consistency(gds->common, gds->common->chunk_sets);

    ASSERT_COND(cset[0]->latest_crash_count < gdsi_crash_count);
    ASSERT_COND(cset[1]->latest_crash_count < gdsi_crash_count);

    int val;
    GDS_size_t lo[] = {0}, hi[] = {0};
    GDS_move_to_prev(gds);
    GDS_get(&val, NULL, lo, hi, gds);
    GDS_move_to_prev(gds);
    GDS_get(&val, NULL, lo, hi, gds);
    GDS_move_to_next(gds);
    GDS_move_to_next(gds);

    ASSERT_COND(cset[0]->latest_crash_count == gdsi_crash_count);
    ASSERT_COND(cset[1]->latest_crash_count == gdsi_crash_count);

    printf("[rank %d] Verifying metadata for version 0\n", myrank);
    verify_old_metadata(gds->common, cset[0]);
    verify_metadata_consistency(gds->common, cset[0]);
/*    printf("[rank %d] Verifying metadata for version 1\n", myrank);
    verify_old_metadata(gds->common, cset[1]);
    verify_metadata_consistency(gds->common, cset[1]);*/

    printf("[rank %d] Before version inc\n", myrank);
    ASSERT_STAT_FATAL("GDS_version_inc",
        GDS_version_inc(gds, 1, "", 0));
    printf("[rank %d] After version inc\n", myrank);

    VERIFY_FAILED_MD();
    verify_metadata_consistency(gds->common, gds->common->chunk_sets);

    printf("[rank %d] All tests done, freeing arrays\n", myrank);

    GDS_free(&gds);

    printf("[rank %d] Arrays freed\n", myrank);

    /* Comment out because this requires a collective call on MPI_COMM_WORLD */
    // ASSERT_TEST_END();
}

int main(int argc, char **argv)
{
    int ret = 0;
    GDS_thread_support_t provd_support;

    ASSERT_INIT();

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support);

    GDS_comm_size(GDS_COMM_WORLD, &nprocs);
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);

    basic_test();

    /* Comment out because this requires a collective call on MPI_COMM_WORLD */
    // ASSERT_REPORT();

    GDS_finalize();
    printf("[rank %d] Exiting\n", myrank);
    return ret;
}
