#!/bin/bash

# Tests on BG/Q

QUEUE_NAME=
PROG=

CURRENT_PWD=`pwd`
GDS_LIB=$CURRENT_PWD/../src/.libs:$CURRENT_PWD/../bindings/fortran/.libs

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GDS_LIB
export PATH=$CURRENT_PWD:$PATH

echo "Settings:"
echo "  LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
echo "  PATH=$PATH"
echo ""

echo "Executing:"
CMD="qsub -t 5 -n 8 --mode c1 -A $QUEUE_NAME $PROG"
echo $CMD
$CMD
