/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  Assert functions and counts
*/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "common.h"

#define MAX_TEST_NAME	96

double t_test_start;
double t_test_end;
int asserts_per_test_n;
int asserts_per_test_ok;
char _test[MAX_TEST_NAME];

void ASSERT_INIT(void)
{
    asserts_n = 0;
    asserts_ok = 0;
}

int ASSERT_REPORT(void)
{
    int asserts_diff, i;
    int *recv = NULL;
    
    asserts_diff = asserts_n - asserts_ok;
    if (myrank == 0) {
        recv = (int *) malloc(nprocs * sizeof(int));
        if (recv == NULL) {
            fprintf(stdout, "fatal: malloc failed\n");
            abort();
        }
    }

    MPI_Gather(&asserts_diff, 1, MPI_INT, recv, 1, MPI_INT,
        0, MPI_COMM_WORLD);
    
    if (myrank == 0) {
        for (i = 0; i < nprocs; i++) {
            if (recv[i])
                fprintf(stdout, "[%d] %d asserts failed\n", i, recv[i]);
            asserts_diff += recv[i];
        }

        fprintf(stdout, "\nTotal %d asserts on %d procs, OK: %d, NG: %d\n", 
            asserts_n * nprocs, nprocs, asserts_n * nprocs - asserts_diff,
            asserts_diff);
        fflush(stdout);
        free(recv);
    }

    return asserts_diff;
}

void ASSERT_TEST_START_COMM(const char *test, MPI_Comm comm)
{
    MPI_Barrier(comm);
    memset(_test, 0x0, MAX_TEST_NAME);
    strncpy(_test, test, MAX_TEST_NAME - 1);
    fflush(stdout);
    fflush(stderr);
    if (myrank == 0) {
        fprintf(stdout, "\nTesting %s...\n", _test);
    }
    asserts_per_test_n = asserts_n;
    asserts_per_test_ok = asserts_ok;
    t_test_start = MPI_Wtime();
}

void ASSERT_TEST_START(const char *test)
{
    ASSERT_TEST_START_COMM(test, MPI_COMM_WORLD);
}

void ASSERT_TEST_GOING(void)
{
    fprintf(stdout, "Test %s on going...\n", _test);
    fflush(stdout);
}

void ASSERT_TEST_END_COMM(MPI_Comm comm)
{
    int asserts_diff, i;
    int *recv = NULL;
    int nprocs;	/* Hide the global one */

    GDS_comm_size(comm, &nprocs);
    t_test_end = MPI_Wtime();
    asserts_per_test_n = asserts_n - asserts_per_test_n;
    asserts_per_test_ok = asserts_ok - asserts_per_test_ok;
    asserts_diff = asserts_per_test_n - asserts_per_test_ok;
    
    if (myrank == 0) {
        recv = (int *) malloc(nprocs * sizeof(int));
        if (recv == NULL) {
            fprintf(stdout, "fatal: malloc failed\n");
            abort();
        }
    }

    MPI_Gather(&asserts_diff, 1, MPI_INT, recv, 1, MPI_INT, 0, comm);

    if (myrank == 0) {
        for (i = 0; i < nprocs; i++) {
            if (recv[i])
                fprintf(stdout, "[%d] %d asserts failed\n", i, recv[i]);
            asserts_diff += recv[i];
        }

        if (asserts_diff == 0) {
            fprintf(stdout, "> OK, took %f seconds\n",
                t_test_end - t_test_start);
        } else {
            fprintf(stdout, "> NG, %d asserts failed, took %f seconds\n", 
                asserts_diff, t_test_end - t_test_start);
        }
        free(recv);
    }
}

void ASSERT_TEST_END(void)
{
    ASSERT_TEST_END_COMM(MPI_COMM_WORLD);
}

void ASSERT_STAT_(const char *func, int stat, 
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (stat == GDS_STATUS_OK) {
        asserts_ok++;
    } else {
        fprintf(stderr, "[%d:%s():%s:%d] %s:%s:STATUS:NG:%d\n", 
            myrank, infunc, infile, lineno, _test, func, stat);
    }
}

void ASSERT_STAT_FATAL_(const char *func, int stat,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (stat == GDS_STATUS_OK) {
        asserts_ok++;
    } else {
        fprintf(stderr, "[%d:%s():%s:%d] %s:%s:STATUS:NG:%d\n",
            myrank, infunc, infile, lineno, _test, func, stat);
        abort();
    }
}

void ASSERT_STAT_INVALID_(const char *func, int stat,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (stat == GDS_STATUS_INVALID) {
        asserts_ok++;
    } else {
        fprintf(stderr, "[%d:%s():%s:%d] %s:%s:STATUS:NG:%d\n", 
            myrank, infunc, infile, lineno, _test, func, stat);
    }
}

void ASSERT_NOT_NULL_FATAL_(const void *pointer,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (pointer == NULL) {
        fprintf(stderr, "[%d:%s():%s:%d] %s:POINTER IS NULL\n", 
            myrank, infunc, infile, lineno, _test);
        abort();
    } else
        asserts_ok++;
}

void ASSERT_EQUAL_INT_(int a, int b,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (a != b) {
        fprintf(stderr, "[%d:%s():%s:%d] %s:INT VALUES NOT EQUAL:%d vs. %d\n",
            myrank, infunc, infile, lineno, _test, a, b);
    } else
        asserts_ok++;
}

void ASSERT_NOT_EQUAL_INT_(int a, int b,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (a == b) {
        fprintf(stderr, "[%d:%s():%s:%d] %s:INT VALUES EQUAL:%d\n",
            myrank, infunc, infile, lineno, _test, a);
    } else
        asserts_ok++;
}

void ASSERT_EQUAL_DOUBLE_(double a, double b, double epsilon,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (fabs(a - b) > epsilon) {
        fprintf(stderr, "[%d:%s():%s:%d] %s:DBL VALUES NOT EQUAL\n",
            myrank, infunc, infile, lineno, _test);
    } else
        asserts_ok++;
}

void ASSERT_NOT_EQUAL_DOUBLE_(double a, double b, double epsilon,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (fabs(a - b) < epsilon) {
        fprintf(stderr, "[%d:%s():%s:%d] %s:DBL VALUES EQUAL\n",
            myrank, infunc, infile, lineno, _test);
    } else
        asserts_ok++;
}

void ASSERT_EQUAL_PTR_(void *a, void *b,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (a != b) {
        fprintf(stderr, "[%d:%s():%s:%d] %s:PTR VALUES NOT EQUAL\n",
            myrank, infunc, infile, lineno, _test);
    } else
        asserts_ok++;
}

void ASSERT_NOT_EQUAL_PTR_(void *a, void *b,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (a == b) {
        fprintf(stderr, "[%d:%s():%s:%d] %s:PTR VALUES EQUAL\n",
            myrank, infunc, infile, lineno, _test);
    } else
        asserts_ok++;
}

void ASSERT_EQUAL_STR_(const char *a, const char *b, const size_t len,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (strncmp(a, b, len) != 0) {
      fprintf(stderr, "[%d:%s():%s:%d] %s:STRINGS NOT EQUAL\n", 
        myrank, infunc, infile, lineno, _test);
    } else
      asserts_ok++;
}

void ASSERT_EQUAL_SIZE_ARR_(size_t *a, size_t *b, const size_t len,
    const char *infunc, const char *infile, int lineno)
{
    size_t i;
    asserts_n++;
    for (i = 0; i < len; i++) {
        if (a[i] != b[i]) {
            fprintf(stderr, "[%d:%s():%s:%d] %s:SIZE_ARR NOT EQUAL:%lu vs %lu\n", 
                myrank, infunc, infile, lineno, _test, a[i], b[i]);
            return;
        }
    }
    asserts_ok++;
}

void ASSERT_COND_(int cond,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (cond) {
        asserts_ok++;
    } else {
        fprintf(stderr, "[%d:%s():%s:%d] %s:COND:NG", 
            myrank, infunc, infile, lineno,  _test);
    }
}

void ASSERT_COND_FATAL_(int cond,
    const char *infunc, const char *infile, int lineno)
{
    asserts_n++;
    if (cond) {
        asserts_ok++;
        /* fprintf(stderr, "%s:OK", condname); */
    } else {
        fprintf(stderr, "[%d:%s():%s:%d] %s:COND:NG",
            myrank, infunc, infile, lineno, _test);
        abort();
    }
}

/* Wrapper */
GDS_size_t GDS_version(GDS_gds_t gds)
{
    GDS_size_t v;
    GDS_get_version_number(gds, &v);
    return v;
}
