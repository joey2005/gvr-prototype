#
#  Global View Resilience (GVR)
#  http://gvr.cs.uchicago.edu
#
#  Copyright (C) 2014 University of Chicago.
#  See license.txt in top-level directory.
#

import sys
import matplotlib.pyplot as plt

def fst(x):
    return x[0]

def snd(x):
    return x[1]

def is_retry(s):
    return s[len(s)-3:] == '_rt'

def draw(fp, bench_type):
    dataset = {}

    for line in fp:
        strs = line.split()

        for i in range(len(strs)):
            strs[i] = strs[i].rstrip(',')

        # "raw, flat, local_seq_w, 16, 0.426334, 2459.518"
        if len(strs) == 0 or strs[0] != "avg":
            continue

        atype = strs[1]
        btype = strs[2]
        w = int(strs[3])
        bw = float(strs[5])

        if is_retry(btype):
            btype = btype[0:len(btype)-3]
            atype += '-ch'

        if btype[len(btype)-3:] == '_ow':
            btype = btype[0:len(btype)-3] + '_w'
            atype += '-ow-cm'

        if btype != bench_type:
            continue

        if not dataset.has_key(atype):
            dataset[atype] = []

        p = (w, bw)

        dataset[atype].append(p)

    # http://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    ax = plt.subplot(111)

    for ty in dataset.keys():
        d = sorted(dataset[ty], key = fst)
        ax.loglog(map(fst, d), map(snd, d), label=ty, basex=2)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.2,
                     box.width, box.height * 0.8])
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
              fancybox=True, shadow=True, ncol=2)
    plt.xlabel("Access width (Bytes)")
    plt.ylabel("Bandwidth (KB/s)")
    plt.title(bench_type)
    plt.savefig(bench_type + ".png")

    # plt.clf()

    # for ty in widths.keys():
    #     wa  = [x for x in widths[ty] if x <= 256]
    #     bwa = bandwidths[ty][0:len(wa)]
    #     plt.semilogx(wa, bwa, label=ty, basex=2)
    # plt.legend()
    # plt.xlabel("Access width (Bytes)")
    # plt.ylabel("Bandwidth (KB/s)")
    # plt.title(bench_type + " - only for small widths")
    # plt.savefig(bench_type + "-semilog.png")

if len(sys.argv) < 2:
    print "Specify benchmark type"
    sys.exit(1)

draw(sys.stdin, sys.argv[1])
