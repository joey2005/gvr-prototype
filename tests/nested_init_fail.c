/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <mpi.h>
#include <gds.h>

#include <stdio.h>

int main(int argc, char *argv[])
{
    GDS_thread_support_t gvr_prov;
    int ret = 0;

    MPI_Init(&argc, &argv);

    GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &gvr_prov);

    GDS_finalize();

    MPI_Finalize();

    return ret;
}
