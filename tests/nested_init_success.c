/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <mpi.h>
#include <gds.h>

#include <stdio.h>

int main(int argc, char *argv[])
{
    int prov;
    GDS_thread_support_t gvr_prov;
    int ret = 0;

    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &prov);
    if (prov != MPI_THREAD_MULTIPLE) {
        fprintf(stderr, "MPI_Init_thread provided %d\n", prov);
        return 1;
    }

    if (GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &gvr_prov) != GDS_STATUS_OK) {
        fprintf(stderr, "GDS_init failed\n");
        ret = 1;
        goto out;
    }

    GDS_finalize();

out:
    MPI_Finalize();

    return ret;
}
