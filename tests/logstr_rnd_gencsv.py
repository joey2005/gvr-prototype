#
#  Global View Resilience (GVR)
#  http://gvr.cs.uchicago.edu
#
#  Copyright (C) 2014 University of Chicago.
#  See license.txt in top-level directory.
#

import sys
import numpy

def fst(x):
    return x[0]

def snd(x):
    return x[1]

def draw(bench_type, width, nprocs):
    dataset = {}

    for line in sys.stdin:
        strs = line.split(", ")

        if strs[0] != "raw":
            continue

        atype = strs[1]
        btype = strs[2]

        if btype[len(btype)-3:] == '_rt':
            btype = btype[0:len(btype)-3]
            atype += '-ch'

        if btype[len(btype)-3:] == '_ow':
            btype = btype[0:len(btype)-3] + '_w'
            atype += '-ow-cm'

        if btype != bench_type:
            continue

        w = int(strs[3])

        if not w == width:
            continue

        rr = int(strs[4])

        acc = float(strs[6])
        np = int(strs[7])

        if np != nprocs:
            continue

        if not dataset.has_key(rr):
            dataset[rr] = {}

        if not dataset[rr].has_key(atype):
            dataset[rr][atype] = []

        dataset[rr][atype].append(acc)

    write = sys.stdout.write
    first_line = True
    for r in sorted(dataset.keys()):
        if first_line:
            write("nodes");
            for ty in sorted(dataset[r]):
                write(",%s" % ty)
            write("\n")
            first_line = False

        write("%d" % r)
        for ty in sorted(dataset[r]):
            write(",%f" % numpy.mean(dataset[r][ty]))
        write("\n")

draw("random", 8, 16)
