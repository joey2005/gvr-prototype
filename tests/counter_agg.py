#
#  Global View Resilience (GVR)
#  http://gvr.cs.uchicago.edu
#
#  Copyright (C) 2014 University of Chicago.
#  See license.txt in top-level directory.
#

import sys
import numpy

def write_array(a):
    write = sys.stdout.write
    write("\t%d\t%f\t%d\t%f" % (min(a), numpy.mean(a), max(a), numpy.std(a)))

def aggregate(array_type, version_str):
    totals = {}
    counts = {}
    averages = {}

    for line in sys.stdin:
        strs = line[len('[  9] counter.c:49: '):].split(",")

        if strs[0] != array_type:
            continue

        if version_str != "":
            if strs[1] != version_str:
                continue
        else:
            if strs[1] == "total":
                continue

        key = strs[2]
        total = int(strs[3])
        count = int(strs[4])

        if not totals.has_key(key):
            totals[key] = []
            counts[key] = []
            averages[key] = []

        totals[key].append(total)
        counts[key].append(count)
        if count > 0:
            averages[key].append(float(total) / count)
        else:
            averages[key].append(0.0)

    write = sys.stdout.write
    print "key\ttotal_min\ttotal_ave\ttotal_max\ttotal_std\tcount_min\tcount_ave\tcount_max\tcount_std\tave_min\tave_ave\tave_max\tave_std"
    for k in sorted(totals.keys()):
        write("%s" % k)
        write_array(totals[k])
        write_array(counts[k])
        write_array(averages[k])
        write("\n")

if len(sys.argv) < 2:
    print "Usage: counter_agg.py <array_type> (version)"
    sys.exit(1)
elif len(sys.argv) == 2:
    ver = ""
else:
    ver = sys.argv[2]

aggregate(sys.argv[1], ver)
