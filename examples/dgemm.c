/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  DGEMM examples
 */

#include <stdio.h>
#include <gds.h>

#define NDMATRIXM 16
#define NDMATRIXK 4
#define NDMATRIXN 8

static void dprintmatrix(int _rows, int _cols, double *_buf, char *_fmt) 
{
    printf("%s\n", _fmt);                                                                                                              
    int _i, _j;                                                 
    for (_j = 0; _j < _cols; ++_j)                              
        fprintf(stderr, "------");                              
    fprintf(stderr, "\n");                                      
    for (_i = 0; _i < _rows; ++_i) {                            
        for (_j = 0; _j < _cols; ++_j)                          
            fprintf(stderr, "%5.2f ", _buf[_j * _rows + _i]);   
        fprintf(stderr, "\n");                                  
    }                                                           
    for (_j = 0; _j < _cols; ++_j)                              
        fprintf(stderr, "------");                              
    fprintf(stderr, "\n\n");                                    
} 

static void local_dgemm(double *A, double *B, double *C, size_t M, 
        size_t N, size_t K) 
{
    int i, j, kidx;
    double r;
    for (i = 0; i < M; ++i) {
        for (j = 0; j < N; ++j) {
            r = 0.0;
            for (kidx = 0; kidx < K; ++kidx) {
                r += A[kidx * M + i] * B[j * K + kidx];
            }
            C[j * M + i] = r;
        }
    }
}

int main(int argc, char **argv) 
{
    int rank, i, j, k;
    int size;
    double result;

    /* Initialize GVR */
    GDS_thread_support_t provd_support;
    GDS_init(&argc, &argv, MPI_THREAD_MULTIPLE, &provd_support);

    /* Get the rank number */
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    /* Get the size of the communicator */
    GDS_comm_size(GDS_COMM_WORLD, &size);

    /* Define three matrix/2-dimension global arrays: gds_A, gds_B, gds_C */
    GDS_gds_t gds_A,gds_B,gds_C;
    GDS_size_t Asize[2] = {NDMATRIXM, NDMATRIXK}; /* Size of array gds_A */
    GDS_size_t Ahi[2] = {NDMATRIXM - 1, NDMATRIXK - 1};
    GDS_size_t Bsize[2] = {NDMATRIXK, NDMATRIXN}; /* Size of array gds_B */
    GDS_size_t Bhi[2] = {NDMATRIXK - 1, NDMATRIXN - 1};
    GDS_size_t Csize[2] = {NDMATRIXM, NDMATRIXN}; /* Size of array gds_C */
    GDS_size_t Chi[2] = {NDMATRIXM - 1, NDMATRIXN - 1};
    GDS_size_t min_chunk[2] = {0, 0}; /* Minimum chunk size */
    MPI_Info info;

    /* Set the array as column major */
    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    /* Allocate arrays: gds_A, gds_B, gds_C */
    GDS_alloc(2, Asize, min_chunk, GDS_DATA_DBL, 
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_A);
    GDS_alloc(2, Bsize, min_chunk, GDS_DATA_DBL, 
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_B);
    GDS_alloc(2, Csize, min_chunk, GDS_DATA_DBL, 
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_C);
    MPI_Info_free(&info);

    double C[NDMATRIXM * NDMATRIXN];
    size_t C_ld[1] = {NDMATRIXM};
    GDS_size_t offsetzero[2] = {0, 0};

    double Clocal[NDMATRIXM * NDMATRIXN];
    if (rank == 0) {
        /* Initialize local matrices A, B, C*/
        double A[NDMATRIXM * NDMATRIXK];
        double B[NDMATRIXK * NDMATRIXN];
        for (i = 0; i < NDMATRIXM * NDMATRIXK; ++i) A[i] = 1;
        for (i = 0; i < NDMATRIXK * NDMATRIXN; ++i) B[i] = 2;
        for (i = 0; i < NDMATRIXM * NDMATRIXN; ++i) C[i] = 0;
        for (i = 0; i < NDMATRIXM * NDMATRIXN; ++i) Clocal[i] = 0;
        if (rank == 0) {
            dprintmatrix(NDMATRIXM, NDMATRIXK, A, "Initial A"); 
            dprintmatrix(NDMATRIXK, NDMATRIXN, B, "Initial B"); 
            dprintmatrix(NDMATRIXM, NDMATRIXN, C, "Initial C");
            dprintmatrix(NDMATRIXM, NDMATRIXN, Clocal, "Initial locally computed C");
        }

        //expected results
        local_dgemm(A, B, Clocal, NDMATRIXM, NDMATRIXN, NDMATRIXK);
        if (rank == 0)
            dprintmatrix(NDMATRIXM, NDMATRIXN, Clocal, "Locally Computed C"); 

        size_t A_ld[1] = {NDMATRIXM};
        size_t B_ld[1] = {NDMATRIXK};
        /* Put local value to global arrays gds_A, gds_B, gds_C */
        GDS_put((void *)A, A_ld, offsetzero, Ahi, gds_A); 
        GDS_put((void *)B, B_ld, offsetzero, Bhi, gds_B); 
        GDS_put((void *)C, C_ld, offsetzero, Chi, gds_C); 

        /* Wait for non-blocking put operations to be done */
        GDS_wait(gds_A);
        GDS_wait(gds_B);
        GDS_wait(gds_C);

        double Agot[NDMATRIXM * NDMATRIXK];
        double Bgot[NDMATRIXK * NDMATRIXN];
        double Cgot[NDMATRIXM * NDMATRIXN];

        /* Get values to local matrices from global arrays gds_A, gds_B, gds_C */
        GDS_get((void *)Agot, A_ld, offsetzero, Ahi, gds_A); 
        GDS_get((void *)Bgot, B_ld, offsetzero, Bhi, gds_B); 
        GDS_get((void *)Cgot, C_ld, offsetzero, Chi, gds_C); 

        /* Wait for non-blocking get operations to be done */
        GDS_wait(gds_A);
        GDS_wait(gds_B);
        GDS_wait(gds_C);

        if (rank == 0) {
            dprintmatrix(NDMATRIXM, NDMATRIXK, Agot, "A after put and get"); 
            dprintmatrix(NDMATRIXK, NDMATRIXN, Bgot, "B after put and get"); 
            dprintmatrix(NDMATRIXM, NDMATRIXN, Cgot, "C after put and get"); 
        }
    }

    /* Synchronize function */
    GDS_fence(NULL);

    /* Matrix computation */
    int rowsperproc = NDMATRIXM / size;
    int remainder = NDMATRIXM % size;
    i = rank * rowsperproc;
    int hilimit = i + rowsperproc;
    if (rank == size - 1) hilimit += remainder;

    double rbuf[NDMATRIXK];
    double cbuf[NDMATRIXK];
    GDS_size_t Amylo[2] = {0,0};
    GDS_size_t Amyhi[2] = {0,NDMATRIXK - 1};
    GDS_size_t Bmylo[2] = {0,0};
    GDS_size_t Bmyhi[2] = {NDMATRIXK - 1,0};
    GDS_size_t Cmylo[2] = {0,0};
    GDS_size_t Cmyhi[2] = {0,0};
    size_t rbuf_ld[1] = {1};
    size_t cbuf_ld[1] = {NDMATRIXK};	
    size_t res_ld[1] = {1};	

    for (; i < hilimit; ++i) {
        Amylo[0] = i;
        Amyhi[0] = i;
        Cmylo[0] = i;
        Cmyhi[0] = i;

        GDS_get((void *)rbuf, rbuf_ld, Amylo, Amyhi, gds_A); 
        GDS_wait(gds_A);

        for (j = 0; j < NDMATRIXN; ++j) {
            Bmylo[1] = j;
            Bmyhi[1] = j;
            Cmylo[1] = j;
            Cmyhi[1] = j;
            GDS_get((void *)cbuf, cbuf_ld, Bmylo, Bmyhi, gds_B);
            GDS_wait(gds_B); 
            result = 0;

            for (k = 0; k < NDMATRIXK; ++k) {
                result += rbuf[k] * cbuf[k];
            }
            GDS_put((void *)&result, res_ld, Cmylo, Cmyhi, gds_C); 
            GDS_wait(gds_C);
        }

    }

    GDS_fence(NULL);
    /* Computation end */

    if (rank == 0) {
        GDS_get((void *)C, C_ld, offsetzero, Chi, gds_C);
        GDS_wait(gds_C); 
        dprintmatrix(NDMATRIXM, NDMATRIXN, C, "C");
        int idx;
        for (i = 0; i < NDMATRIXM; ++i) {
            for (j = 0; j < NDMATRIXN; ++j) {
                idx = j * NDMATRIXM + i;
                printf("%d ", idx);
            }
            printf("\n");
        }
    }

    GDS_fence(NULL);

    /* Free global arrays */
    GDS_free(&gds_A);
    GDS_free(&gds_B);
    GDS_free(&gds_C);

    GDS_finalize();
    return 0;
}
