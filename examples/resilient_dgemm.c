/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.


  DGEMM example with resilience (version creation, version navigation,
  error signaling and handling)
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <gds.h>
#include <sys/time.h>
#include <string.h>

#define NDMATRIXM 16
#define NDMATRIXK 4
#define NDMATRIXN 8

static double C[NDMATRIXM * NDMATRIXN];

static void dprintmatrix(int _rows, int _cols, double *_buf, char *_fmt) 
{
    printf("%s\n", _fmt);                                                                                                              
    int _i, _j;                                                 
    for (_j = 0; _j < _cols; ++_j)                              
    fprintf(stderr, "------");                              
    fprintf(stderr, "\n");                                      
    for (_i = 0; _i < _rows; ++_i) {                            
        for (_j = 0; _j < _cols; ++_j)                          
        fprintf(stderr, "%5.2f ", _buf[_j * _rows + _i]);   
        fprintf(stderr, "\n");                                  
    }                                                           
    for (_j = 0; _j < _cols; ++_j)                              
    fprintf(stderr, "------");                              
    fprintf(stderr, "\n\n");                                    
} 

#define replicatematrix(_rows, _cols, _buf1, _buf2, ...)             \
    do {                                                                \
        int _i, _j;                                                 \
        for (_i = 0; _i < _rows; ++_i) {                            \
            for (_j = 0; _j < _cols; ++_j)                          \
            _buf1[_j * _rows + _i] = _buf2[_j * _rows + _i];   \
        }                                                           \
    } while (0)

static void local_dgemm(double *A, double *B, double *C, size_t M, 
        size_t N, size_t K) 
{
    int i, j, kidx;
    double r;
    for (i = 0; i < M; ++i) {
        for (j = 0; j < N; ++j) {
            r = 0.0;
            for (kidx = 0; kidx < K; ++kidx) {
                r += A[kidx * M + i] * B[j * K + kidx];
            }
            C[j * M + i] = r;
        }
    }
}

/* Artificial Error Injection Function -- corrupts some random locations */
static void error_injection(double *matrix, size_t M, size_t N, int n_errors) {
    int i, index;

    srand(time(NULL));
    index = rand() % (M*N);
    for (i = 0; i < n_errors; i++) {
        index = index + i > M*N ? index : index + i;
        matrix[index+i] = matrix[index+i] + 1;
    }
}

/* Artificial Error Detection Function
   -- compare a matrix with an expected value */
static int error_detection(double *matrix, double *expect, size_t M, size_t N)
{
    int rank;

    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    int i;
    for (i = 0; i < M*N; i++) {
        if (matrix[i] != expect[i]) {
            printf("rank %d detects error\n", rank);
            return 1;
        }
    }
    return 0;
}

/* Global Error Handler */
static GDS_status_t handle_dgemm_error(GDS_gds_t gds, GDS_error_t desc)
{
    GDS_size_t lo[2] = {0, 0}, hi[2] = { NDMATRIXM - 1, NDMATRIXN - 1 };
    GDS_size_t ld[1] = {NDMATRIXM};
    int n_errors, flag;

    printf("Recovering from an error.\n");

    /* Get number of errors embedded in the error descriptor */
    GDS_get_error_attr(desc, GDS_EATTR_APP, &n_errors, &flag);
    if (flag) {
        /* flag == true means the key exists */
        printf("%d errors reported.\n", n_errors);
    }

    /* Move to previous good version of data */
    GDS_move_to_prev(gds);

    /* Get the correct data from global array */
    GDS_get(C, ld, lo, hi, gds);
    GDS_wait(gds);

    /* Resume the gds handler */
    GDS_resume_global(gds, desc);

    return GDS_STATUS_OK;
}

int main(int argc, char **argv) 
{
    int rank, i, j, k;
    int size;
    double result;

    /* Initialize GVR */
    GDS_thread_support_t provd_support;
    GDS_init(&argc, &argv, MPI_THREAD_MULTIPLE, &provd_support);

    /* Get the rank number */
    GDS_comm_rank(GDS_COMM_WORLD, &rank);
    /* Get the size of the communicator */
    GDS_comm_size(GDS_COMM_WORLD, &size);

    /* Define three matrix/2-dimension global arrays: gds_A, gds_B, gds_C */
    GDS_gds_t gds_A,gds_B,gds_C;
    GDS_size_t Asize[2] = {NDMATRIXM, NDMATRIXK}; /* Size of array gds_A */
    GDS_size_t Ahi[2] = {NDMATRIXM - 1, NDMATRIXK - 1};
    GDS_size_t Bsize[2] = {NDMATRIXK, NDMATRIXN}; /* Size of array gds_B */
    GDS_size_t Bhi[2] = {NDMATRIXK - 1, NDMATRIXN - 1};
    GDS_size_t Csize[2] = {NDMATRIXM, NDMATRIXN}; /* Size of array gds_C */
    GDS_size_t Chi[2] = {NDMATRIXM - 1, NDMATRIXN - 1};
    GDS_size_t min_chunk[2] = {0, 0}; /* Minimum chunk size */
    MPI_Info info;

    /* Set the array as column major */
    MPI_Info_create(&info);
    MPI_Info_set(info, GDS_ORDER_KEY, GDS_ORDER_COL_MAJOR);

    /* Allocate arrays: gds_A, gds_B, gds_C */
    GDS_alloc(2, Asize, min_chunk, GDS_DATA_DBL, 
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_A);
    GDS_alloc(2, Bsize, min_chunk, GDS_DATA_DBL, 
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_B);
    GDS_alloc(2, Csize, min_chunk, GDS_DATA_DBL, 
            GDS_PRIORITY_HIGH, GDS_COMM_WORLD, info, &gds_C);
    MPI_Info_free(&info);

    GDS_error_pred_t pred;
    GDS_error_pred_term_t term;

    /* Create error predicator */
    GDS_create_error_pred(&pred);

    /* Creates a new error matching predicate term object, with attribute key
     * GDS_EATTR_APP (application error), matching expression GDS_EMEXP_ANY
     * (any value) */
    GDS_create_error_pred_term(GDS_EATTR_APP, GDS_EMEXP_ANY, 0, NULL, &term);

    /* Adds an error matching predicate term to the predicate */
    GDS_add_error_pred_term(pred, term);

    /* Frees a new error matching predicate term object */
    GDS_free_error_pred_term(&term);

    /* Creates a new error matching predicate term object, with attribute key
     * GDS_EATTR_DETECTED_BY (application error), matching expression GDS_EMEXP_ANY
     * (any value) */
    GDS_create_error_pred_term(GDS_EATTR_DETECTED_BY, GDS_EMEXP_ANY, 0, NULL, &term);

    /* Adds an error matching predicate term to the predicate */
    GDS_add_error_pred_term(pred, term);

    /* Frees a new error matching predicate term object */
    GDS_free_error_pred_term(&term);

    /* Register error handler, specify the recovery procedure for given gds */
    GDS_register_global_error_handler(gds_C, pred, handle_dgemm_error);

    /* Free predicator */
    GDS_free_error_pred(&pred);

    size_t C_ld[1] = {NDMATRIXM};
    GDS_size_t offsetzero[2] = {0, 0};

    double Clocal[NDMATRIXM * NDMATRIXN];
    if (rank == 0) {
        /* Initialize local matrices A, B, C*/
        double A[NDMATRIXM * NDMATRIXK];
        double B[NDMATRIXK * NDMATRIXN];
        for (i = 0; i < NDMATRIXM * NDMATRIXK; ++i) A[i] = 1;
        for (i = 0; i < NDMATRIXK * NDMATRIXN; ++i) B[i] = 2;
        for (i = 0; i < NDMATRIXM * NDMATRIXN; ++i) C[i] = 0;
        for (i = 0; i < NDMATRIXM * NDMATRIXN; ++i) Clocal[i] = 0;
        if (rank == 0) {
            dprintmatrix(NDMATRIXM, NDMATRIXK, A, "Initial A"); 
            dprintmatrix(NDMATRIXK, NDMATRIXN, B, "Initial B"); 
            dprintmatrix(NDMATRIXM, NDMATRIXN, C, "Initial C");
            dprintmatrix(NDMATRIXM, NDMATRIXN, Clocal, "Initial locally computed C");
        }

        //expected results
        local_dgemm(A, B, Clocal, NDMATRIXM, NDMATRIXN, NDMATRIXK);
        if (rank == 0)
            dprintmatrix(NDMATRIXM, NDMATRIXN, Clocal, "Locally Computed C"); 

        size_t A_ld[1] = {NDMATRIXM};
        size_t B_ld[1] = {NDMATRIXK};
        /* Put local value to global arrays gds_A, gds_B, gds_C */
        GDS_put((void *)A, A_ld, offsetzero, Ahi, gds_A); 
        GDS_put((void *)B, B_ld, offsetzero, Bhi, gds_B); 
        GDS_put((void *)C, C_ld, offsetzero, Chi, gds_C); 

        /* Wait for non-blocking put operations to be done */
        GDS_wait(gds_A);
        GDS_wait(gds_B);
        GDS_wait(gds_C);

        double Agot[NDMATRIXM * NDMATRIXK];
        double Bgot[NDMATRIXK * NDMATRIXN];
        double Cgot[NDMATRIXM * NDMATRIXN];

        /* Get values to local matrices from global arrays gds_A, gds_B, gds_C */
        GDS_get((void *)Agot, A_ld, offsetzero, Ahi, gds_A); 
        GDS_get((void *)Bgot, B_ld, offsetzero, Bhi, gds_B); 
        GDS_get((void *)Cgot, C_ld, offsetzero, Chi, gds_C); 

        /* Wait for non-blocking get operations to be done */
        GDS_wait(gds_A);
        GDS_wait(gds_B);
        GDS_wait(gds_C);

        if (rank == 0) {
            dprintmatrix(NDMATRIXM, NDMATRIXK, Agot, "A after put and get"); 
            dprintmatrix(NDMATRIXK, NDMATRIXN, Bgot, "B after put and get"); 
            dprintmatrix(NDMATRIXM, NDMATRIXN, Cgot, "C after put and get"); 
        }
    }

    /* Synchronize function */
    GDS_fence(NULL);

    /* Matrix computation */
    int rowsperproc = NDMATRIXM / size;
    int remainder = NDMATRIXM % size;
    i = rank * rowsperproc;
    int hilimit = i + rowsperproc;
    if (rank == size - 1) hilimit += remainder;

    double rbuf[NDMATRIXK];
    double cbuf[NDMATRIXK];
    GDS_size_t Amylo[2] = {0,0};
    GDS_size_t Amyhi[2] = {0,NDMATRIXK - 1};
    GDS_size_t Bmylo[2] = {0,0};
    GDS_size_t Bmyhi[2] = {NDMATRIXK - 1,0};
    GDS_size_t Cmylo[2] = {0,0};
    GDS_size_t Cmyhi[2] = {0,0};
    size_t rbuf_ld[1] = {1};
    size_t cbuf_ld[1] = {NDMATRIXK};	
    size_t res_ld[1] = {1};	

    for (; i < hilimit; ++i) {
        Amylo[0] = i;
        Amyhi[0] = i;
        Cmylo[0] = i;
        Cmyhi[0] = i;

        GDS_get((void *)rbuf, rbuf_ld, Amylo, Amyhi, gds_A); 
        GDS_wait(gds_A);

        for (j = 0; j < NDMATRIXN; ++j) {
            Bmylo[1] = j;
            Bmyhi[1] = j;
            Cmylo[1] = j;
            Cmyhi[1] = j;
            GDS_get((void *)cbuf, cbuf_ld, Bmylo, Bmyhi, gds_B);
            GDS_wait(gds_B); 
            result = 0;

            for (k = 0; k < NDMATRIXK; ++k) {
                result += rbuf[k] * cbuf[k];
            }
            GDS_put((void *)&result, res_ld, Cmylo, Cmyhi, gds_C); 
            GDS_wait(gds_C);
        }

    }

    GDS_fence(NULL);
    /* Computation end */

    if (rank == 0) {
        GDS_get((void *)C, C_ld, offsetzero, Chi, gds_C);
        GDS_wait(gds_C); 
        dprintmatrix(NDMATRIXM, NDMATRIXN, C, "C");
        int idx;
        for (i = 0; i < NDMATRIXM; ++i) {
            for (j = 0; j < NDMATRIXN; ++j) {
                idx = j * NDMATRIXM + i;
                printf("%d ", idx);
            }
            printf("\n");
        }
    }

    GDS_fence(NULL);

    /* Create a new version for global arrays */
    GDS_version_inc(gds_A, 1, NULL, 0);
    GDS_version_inc(gds_B, 1, NULL, 0);
    GDS_version_inc(gds_C, 1, NULL, 0);

    double C_check[NDMATRIXM * NDMATRIXN];

    /* replicate correct matrix C to C_check */
    replicatematrix(NDMATRIXM, NDMATRIXN, C_check, C);
    if (rank == 0) 
        dprintmatrix(NDMATRIXM, NDMATRIXN, C, "C_check");

    if (rank == 0)
        dprintmatrix(NDMATRIXM, NDMATRIXN, C, "Before injecting error to C");

    /* Inject 10 errors to C artifically */
    int n_errors = 10;
    error_injection(C, NDMATRIXM, NDMATRIXN, n_errors);
    if (rank == 0)
        dprintmatrix(NDMATRIXM, NDMATRIXN, C, "After injecting error to C");

    /* Put the corrupted value to global arrays */
    GDS_put((void *)C, C_ld, offsetzero, Chi, gds_C); 
    GDS_wait(gds_C);

    if (rank == 0) {
        /* rank 0 checks an error and raises an error if something
           goes wrong */

        /* Call error detection function */
        int flag = error_detection(C, C_check, NDMATRIXM, NDMATRIXN);
        if (!flag) {
            /* No errors found, skipping error signaling */
            goto skip;
        }

        GDS_error_t desc;
        /* Create error descriptor */
        GDS_create_error_descriptor(&desc);
        /* Add error attributes */
        /* Embed application-specific error attribute
           -- as an example, here we embed the number of errors injected */
        GDS_add_error_attr(desc, GDS_EATTR_APP, sizeof(n_errors), &n_errors);
        char detector[256] = "DETECTED BY APPLICATION ERROR DETECTOR";
        GDS_size_t len = strlen(detector);
        GDS_add_error_attr(desc, GDS_EATTR_DETECTED_BY, len, detector);

        /* Raise global error */
        GDS_raise_global_error(gds_C, desc);
    }
skip:
    /* Synchronization call triggers the global error handler */
    GDS_fence(gds_C);

    if (rank == 0)
        dprintmatrix(NDMATRIXM, NDMATRIXN, C, "After error recovery C");

    /* Free global arrays */
    GDS_free(&gds_A);
    GDS_free(&gds_B);
    GDS_free(&gds_C);

    GDS_finalize();
    return 0;
}
