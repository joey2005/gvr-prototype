/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#ifndef _GDS_H_
#define _GDS_H_

#include <stdlib.h>
#include <mpi.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
  Notes on prefix:
  GDS: user-visible GDS names
  gdsi: only for GDS library implementation internal use
*/

/*
  Data types
*/

typedef enum {
    GDS_STATUS_OK = 0,
    GDS_STATUS_INVALID,  /* Invalid argument */
    GDS_STATUS_NOMEM,    /* No memory */
    GDS_STATUS_RANGE,    /* Range exceeded */
    GDS_STATUS_EXISTS,   /* Already exists */
    GDS_STATUS_NOTEXIST, /* Not exist */
    GDS_STATUS_TOO_MANY_ENTS, /* Too many entries */
    GDS_STATUS_ERR_GENERAL, /* General error */
} GDS_status_t;

typedef int GDS_error_attr_key_t;
#define GDS_ERROR_ATTR_KEY_INVALID (-1)

/* Pre-defined error attributes */
extern GDS_error_attr_key_t GDS_EATTR_MEMORY_VADDR;
extern GDS_error_attr_key_t GDS_EATTR_MEMORY_SIZE;
extern GDS_error_attr_key_t GDS_EATTR_MEMORY_AFFECTED_SIZE;
extern GDS_error_attr_key_t GDS_EATTR_MEMORY_N_RANGES;
extern GDS_error_attr_key_t GDS_EATTR_MEMORY_DATA;
extern GDS_error_attr_key_t GDS_EATTR_MEMORY_CHECK_BITS;
extern GDS_error_attr_key_t GDS_EATTR_MEMORY_LOCATION_REUSABLE;

extern GDS_error_attr_key_t GDS_EATTR_APP;
extern GDS_error_attr_key_t GDS_EATTR_DETECTED_BY;

extern GDS_error_attr_key_t GDS_EATTR_GDS_INDEX;
extern GDS_error_attr_key_t GDS_EATTR_GDS_COUNT;
extern GDS_error_attr_key_t GDS_EATTR_GDS_AFFECTED_COUNT;
extern GDS_error_attr_key_t GDS_EATTR_GDS_VERSION;
extern GDS_error_attr_key_t GDS_EATTR_GDS_LATENCY;

extern GDS_error_attr_key_t GDS_EATTR_LOST_PROCESSES;
extern GDS_error_attr_key_t GDS_EATTR_LOST_COMMUNICATOR;
extern GDS_error_attr_key_t GDS_EATTR_REPLACED_COMMUNICATOR;
extern GDS_error_attr_key_t GDS_EATTR_LOST_COMPUTATION;
extern GDS_error_attr_key_t GDS_EATTR_MPI_RANK_UNRESPONSIVE;

/*
  Error matching expression types
  used to defined error matching predicate for error handlers
*/
typedef enum {
    /* Matches with any attribute value */
    GDS_EMEXP_ANY,

    /* Matches with exact value */
    GDS_EMEXP_VALUE,

    /* Matches with value range */
    GDS_EMEXP_RANGE,

    /* Matches if the key does not appear */
    GDS_EMEXP_NOT,
} GDS_error_match_expr_t;

typedef enum {
    GDS_EAVTYPE_BYTE,
    GDS_EAVTYPE_INT,
    GDS_EAVTYPE_FLOAT,
    GDS_EAVTYPE_BYTE_ARRAY,
    GDS_EAVTYPE_INT_ARRAY,
    GDS_EAVTYPE_FLOAT_ARRAY,
    GDS_EAVTYPE_MPIOBJ,
    GDS_EAVTYPE_MPIOBJ_ARRAY,
} GDS_error_attr_value_type_t;

struct GDS_error_pred;
typedef struct GDS_error_pred *GDS_error_pred_t;

struct GDS_error_pred_term;
typedef struct GDS_error_pred_term *GDS_error_pred_term_t;

typedef enum {
    GDS_TYPE,
    GDS_CREATE_FLAVOR,
    GDS_GLOBAL_LAYOUT,
    GDS_NUMBER_DIMENSIONS,
    GDS_COUNT,
    GDS_CHUNK_SIZE,
} GDS_attr_t;

typedef enum {
    GDS_THREAD_SINGLE,
    GDS_THREAD_FUNNELED,
    GDS_THREAD_SERIALIZED,
    GDS_THREAD_MULTIPLE,
} GDS_thread_support_t;

typedef enum {
    GDS_PRIORITY_HIGH,
    GDS_PRIORITY_MEDIUM,
    GDS_PRIORITY_LOW, 
    
    GDS_PRIORITY_MAX /* for implementation purpose and not be used by users */
} GDS_priority_t;

typedef enum {
    GDS_FLAVOR_CREATE,
    GDS_FLAVOR_ALLOC,
} GDS_flavor_t;

typedef enum {
    GDS_ACCESS_BUFFER_ANY,
    GDS_ACCESS_BUFFER_DIRECT,
    GDS_ACCESS_BUFFER_COPY,
} GDS_access_buffer_t;

struct GDS_access_handle;
typedef struct GDS_access_handle *GDS_access_handle_t;

/* Data ordering specifier */
#define GDS_ORDER_KEY "GDS_ORDER_KEY"
/* Default -- same as in GA,
   row major for C binding and col major for Fortran binding */
#define GDS_ORDER_DEFAULT "default"
/* Row major -- as in C language */
#define GDS_ORDER_ROW_MAJOR "rowmaj"
/* Column major -- as in Fortran language */
#define GDS_ORDER_COL_MAJOR "colmaj"

#define GDS_ORDER_VAL_MAX 7

/* Internal data representation */
#define GDS_REPR_KEY "GDS_REPR_KEY"
#define GDS_REPR_FLAT "flat"
#define GDS_REPR_LOG  "log"
#define GDS_REPR_LRDS "lrds"
#define GDS_REPR_VAL_MAX 4

/* Switch for data access methods */
#define GDS_GET_IMPL_KEY "GDS_GET_IMPL_KEY"
#define GDS_PUT_IMPL_KEY "GDS_PUT_IMPL_KEY"

/* Switch for get implementation */
enum GDS_get_impl {
    GDS_GET_IMPL_RMA, /* RMA based */
    GDS_GET_IMPL_MSG, /* Message (send-recv) based */

    GDS_GET_IMPL_MAX, /* end delimiter */
};

/* Switch for get implementation */
enum GDS_put_impl {
    GDS_PUT_IMPL_RMA, /* RMA based */
    GDS_PUT_IMPL_MSG, /* Message (send-recv) based */
    GDS_PUT_IMPL_HYB, /* Hybrid of RMA and MSG */

    GDS_PUT_IMPL_MAX, /* end delimiter */
};

/* Data access methods for flat array
   DEPRECATED -- left for compatibility.
   Use GDS_(GET|PUT)_IMPL_KEY instead. */
#define GDS_FLAT_GET_IMPL_KEY "GDS_GET_IMPL_KEY"
#define GDS_FLAT_PUT_IMPL_KEY "GDS_PUT_IMPL_KEY"

/* Switch for get implementation
   DEPRECATED -- only for compatibility */
enum GDS_flat_get_impl {
    GDS_FLAT_GET_IMPL_RMA = GDS_GET_IMPL_RMA, /* RMA based */
    GDS_FLAT_GET_IMPL_MSG = GDS_GET_IMPL_MSG, /* Message (send-recv) based */

    GDS_FLAT_GET_IMPL_MAX, /* end delimiter */
};

/* Switch for get implementation
   DEPRECATED -- only for compatibility */
enum GDS_flat_put_impl {
    GDS_FLAT_PUT_IMPL_RMA = GDS_PUT_IMPL_RMA, /* RMA based */
    GDS_FLAT_PUT_IMPL_MSG = GDS_PUT_IMPL_MSG, /* Message (send-recv) based */

    GDS_FLAT_PUT_IMPL_MAX, /* end delimiter */
};

/* Several additional flavors for log-structured array */
#define GDS_LOG_BS_KEY "GDS_LOG_BS_KEY"
#define GDS_LOG_PREALLOC_VERS_KEY "GDS_LOG_PREALLOC_VERS_KEY"
/* DEPRECATED -- only for compatibility */
#define GDS_LOG_GET_IMPL_KEY "GDS_GET_IMPL_KEY"
#define GDS_LOG_PUT_IMPL_KEY "GDS_PUT_IMPL_KEY"

/* Switch for get implementation
   DEPRECATED -- only for compatibility */
enum GDS_log_get_impl {
    GDS_LOG_GET_IMPL_RMA = GDS_GET_IMPL_RMA, /* RMA based */
    GDS_LOG_GET_IMPL_MSG = GDS_GET_IMPL_MSG, /* Message (send-recv) based */

    GDS_LOG_GET_IMPL_MAX, /* end delimiter */
};

/* Switch for put implementation
   DEPRECATED -- only for compatibility */
enum GDS_log_put_impl {
    GDS_LOG_PUT_IMPL_RMA = GDS_PUT_IMPL_RMA, /* RMA based */
    GDS_LOG_PUT_IMPL_MSG = GDS_PUT_IMPL_MSG, /* Message (send-recv) based */
    GDS_LOG_PUT_IMPL_HYB = GDS_PUT_IMPL_HYB, /* Hybrid */

    GDS_LOG_PUT_IMPL_MAX, /* end delimiter */
};

/* Several additional flavors for log-structured array */
#define GDS_LRDS_PROP_DIRTY_TRACKING_KEY "GDS_LRDS_PROP_DIRTY_TRACKING_KEY"
#define GDS_LRDS_PROP_VERSIONING_KEY "GDS_LRDS_PROP_VERSIONING_KEY"
#define GDS_LRDS_PROP_DIRTY_BLOCK_SIZE_KEY "GDS_LRDS_PROP_DIRTY_BLOCK_SIZE_KEY"

struct GDS_error; /* Forward declaration */
typedef struct GDS_error *GDS_error_t;

typedef int GDS_reliability_level_t; /* TODO: actual data type should be discussed later */
typedef size_t GDS_size_t; /* TODO: actual data type should be discussed later, but it will probably stay like this */

/*
  GDS communicator type

  gdsi_comm_to_MPI should be used to convert types in case of
  future design change.
*/
typedef MPI_Comm GDS_comm_t;

typedef MPI_Info GDS_info_t;

extern GDS_comm_t GDS_COMM_WORLD;

typedef MPI_Op GDS_op_t;

typedef MPI_Datatype GDS_datatype_t;

#define GDS_DATA_BYTE MPI_BYTE
#define GDS_DATA_INT MPI_INT
#define GDS_DATA_DBL MPI_DOUBLE

struct GDS_gds; /* Forward declaration */

typedef struct GDS_gds *GDS_gds_t;

#define GDS_NULL ((GDS_gds_t) NULL)
#define GDS_ROOT ((GDS_gds_t) 0x1)

typedef GDS_status_t (*GDS_recovery_func_t)(
    GDS_gds_t gds,
    GDS_error_t error_desc);

typedef GDS_status_t (*GDS_check_func_t)(
    GDS_gds_t gds,
    GDS_priority_t check_priority);

/* keep in mind that for both of these signatures, local offset is in terms of
   elements, not bytes */
typedef GDS_status_t (*GDS_global_to_local_func_t)(
    const GDS_size_t global_indices[],
    int *local_rank,
    GDS_size_t *local_offset);

/* keep in mind that for both of these signatures, local offset is in terms of
   elements, not bytes */
typedef GDS_status_t (*GDS_local_to_global_func_t)(
    GDS_size_t local_offset,
    GDS_size_t *global_indices);

GDS_status_t GDS_init(
    int *argc,
    char **argv[],
    GDS_thread_support_t requested_thread_support,
    GDS_thread_support_t *provided_thread_support);

GDS_status_t GDS_finalize();

GDS_status_t GDS_alloc(
    GDS_size_t ndim,
    const GDS_size_t count[],
    const GDS_size_t min_chunks[],
    GDS_datatype_t element_type,
    GDS_priority_t resilience_priority,
    GDS_comm_t users,
    GDS_info_t info,
    GDS_gds_t *gds);

GDS_status_t GDS_create(
    GDS_size_t ndims,
    const GDS_size_t count[],
    GDS_datatype_t element_type,
    GDS_global_to_local_func_t global_to_local_function,
    GDS_local_to_global_func_t local_to_global_function,
    void *local_buffer,
    GDS_size_t local_buffer_count,
    GDS_priority_t resilience_priority,
    GDS_comm_t users,
    GDS_info_t info,
    GDS_gds_t *gds);

GDS_status_t GDS_free(GDS_gds_t *gds);

GDS_status_t GDS_get_attr(
    GDS_gds_t gds,
    GDS_attr_t attribute_key,
    void *attribute_value,
    int *flag);

GDS_status_t GDS_get_comm(
    GDS_gds_t gds,
    GDS_comm_t *comm);

GDS_status_t GDS_info_set_int(MPI_Info info,
    const char *key,
    int val);

//ld is in number of elements
GDS_status_t GDS_get(
    void *origin_addr,
    GDS_size_t origin_ld[],
    GDS_size_t lo_index[],
    GDS_size_t hi_index[],
    GDS_gds_t gds);

GDS_status_t GDS_put(
    void *origin_addr,
    GDS_size_t origin_ld[],
    GDS_size_t lo_index[],
    GDS_size_t hi_index[],
    GDS_gds_t gds);

GDS_status_t GDS_acc(
    void *origin_addr,
    GDS_size_t origin_ld[],
    GDS_size_t lo_index[],
    GDS_size_t hi_index[],
    GDS_op_t accumulate_op,
    GDS_gds_t gds);

GDS_status_t GDS_get_acc(
    void *origin_addr,
    GDS_size_t origin_ld[],
    void *result_addr,
    GDS_size_t result_ld[],
    GDS_size_t lo_index[],
    GDS_size_t hi_index[],
    GDS_op_t accumulate_op,
    GDS_gds_t gds);

GDS_status_t GDS_compare_and_swap(
    void *comp_addr,
    void *swap_src_addr,
    void *swap_res_addr,
    GDS_size_t offset[],
    GDS_gds_t gds);

GDS_status_t GDS_access(
    GDS_gds_t gds,
    GDS_size_t lo_index[],
    GDS_size_t hi_index[],
    GDS_access_buffer_t buffer_type,
    void **access_buffer,
    GDS_access_handle_t *access_handle);

GDS_status_t GDS_get_access_buffer_type(
    GDS_access_handle_t access_handle,
    GDS_access_buffer_t *buffer_type);

GDS_status_t GDS_release(GDS_access_handle_t access_handle);

GDS_status_t GDS_fence(GDS_gds_t gds);

GDS_status_t GDS_wait(GDS_gds_t gds);

GDS_status_t GDS_wait_local(GDS_gds_t gds);

//label size is the number of bytes included in the label
GDS_status_t GDS_version_inc(
    GDS_gds_t gds,
    GDS_size_t increment,
    const char *label,
    size_t label_size);

GDS_status_t GDS_comm_rank(
    GDS_comm_t comm,
    int *rank);

GDS_status_t GDS_comm_size(
    GDS_comm_t comm,
    int *size);

#define GDS_ERROR_ATTR_NAME_MAX 31

GDS_status_t GDS_define_error_attr_key(
    const char *attr_name,
    size_t attr_name_size,
    GDS_error_attr_value_type_t value_type,
    GDS_error_attr_key_t *new_key);

GDS_status_t GDS_create_error_pred(GDS_error_pred_t *pred);

GDS_status_t GDS_duplicate_error_pred(
    const GDS_error_pred_t src,
    GDS_error_pred_t *dest
);

GDS_status_t GDS_create_error_pred_term(
    GDS_error_attr_key_t attr_key,
    GDS_error_match_expr_t match_expr,
    GDS_size_t value_len,
    const void *values,
    GDS_error_pred_term_t *term);

GDS_status_t GDS_free_error_pred_term(GDS_error_pred_term_t *term);

GDS_status_t GDS_add_error_pred_term(
    GDS_error_pred_t pred,
    const GDS_error_pred_term_t term);

GDS_status_t GDS_free_error_pred(GDS_error_pred_t *pred);

/* Reserved for future macro development */
struct gdsi_error_pred_term_input {
    GDS_error_attr_key_t attr_key;
    GDS_error_match_expr_t match_expr;
    GDS_size_t value_len;
    const void *values;
};

GDS_status_t GDS_register_local_error_handler(
    GDS_gds_t gds,
    const GDS_error_pred_t pred,
    GDS_recovery_func_t recovery_func);

GDS_status_t GDS_raise_local_error(
    GDS_gds_t gds,
    GDS_error_t error_desc);

GDS_status_t GDS_resume_local(
    GDS_gds_t gds,
    GDS_error_t error_desc);

GDS_status_t GDS_register_global_error_handler(
    GDS_gds_t gds,
    const GDS_error_pred_t pred,
    GDS_recovery_func_t recovery_func);

GDS_status_t GDS_raise_global_error(
    GDS_gds_t gds,
    GDS_error_t error_desc);

GDS_status_t GDS_resume_global(
    GDS_gds_t gds,
    GDS_error_t error_desc);

GDS_status_t GDS_invoke_global_error_handler(GDS_gds_t gds);

GDS_status_t GDS_invoke_local_error_handler(GDS_gds_t gds);

GDS_status_t GDS_register_global_error_check(GDS_gds_t gds,
    GDS_check_func_t check_func,
    GDS_priority_t check_priority);

GDS_status_t GDS_register_local_error_check(GDS_gds_t gds,
    GDS_check_func_t check_func,
    GDS_priority_t check_priority);

GDS_status_t GDS_check_all_errors(GDS_gds_t gds);

GDS_status_t GDS_create_error_descriptor(GDS_error_t *error_desc);
GDS_status_t GDS_free_error_descriptor(GDS_error_t *error_desc);

GDS_status_t GDS_add_error_attr(
    GDS_error_t error_desc,
    GDS_error_attr_key_t attr_key,
    GDS_size_t attr_len,
    const void *attr_val);

GDS_status_t GDS_get_error_attr(
    GDS_error_t error_desc,
    GDS_error_attr_key_t attr_key,
    void *attr_val,
    int *flag);

GDS_status_t GDS_get_error_attr_len(
    GDS_error_t error_desc,
    GDS_error_attr_key_t attr_key,
    GDS_size_t *attr_len,
    int *flag);

GDS_status_t GDS_get_version_number(
    GDS_gds_t gds, 
    GDS_size_t *version_number);

GDS_status_t GDS_get_version_label(
    GDS_gds_t gds, 
    char **label,
    size_t *label_size);

GDS_status_t GDS_move_to_newest(GDS_gds_t gds);

GDS_status_t GDS_move_to_next(GDS_gds_t gds);

GDS_status_t GDS_move_to_prev(GDS_gds_t gds);

GDS_status_t GDS_version_dec(GDS_gds_t gds, GDS_size_t dec);

GDS_status_t GDS_descriptor_clone(GDS_gds_t in_gds, GDS_gds_t *clone_gds);

/* Unofficial API */
GDS_status_t GDS_counter_reset(GDS_gds_t gds);

/*
 * A collective call to simulate a process failure
 * For demo purposes
 *
 * @param rank process to be killed
 */
GDS_status_t GDS_simulate_proc_failure(int rank);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _GDS_H_ */
