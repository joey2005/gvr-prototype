/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#ifndef _GDS_INTERNAL_H
#define _GDS_INTERNAL_H

#include <config.h>
#include <gds.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <pthread.h>

#ifdef GDS_CONFIG_USE_LRDS
#include <lrds.h>
#include <lrds_memory.h>
#endif

#include "mcs-mutex.h"
#include "dynconf.h"

#ifdef GDS_CONFIG_COUNTER
#include "counter.h"
#endif

#ifdef GDS_CONFIG_USE_SCR
#include <scr.h>
#ifndef FLUSH_to_SCR
#define FLUSH_to_SCR (5)
#endif
/* environmental variables */
extern int flush_to_scr_freq;
extern char *SCR_JOB_ID;
extern char SCR_CACHE_BASE[SCR_MAX_FILENAME];
extern char SCR_PREFIX[SCR_MAX_FILENAME];
extern bool SCR_SUPPORT; /* Disabled after proc failure */
#endif

/*
  gdsi_dprintf(level, fmt, args, ...)

  Print out a debug message to the standard error.
  level indicates the verbosity, defined in gdsi_debug_level

  Message verbosity can be specified with an environment variable
  GDS_DEBUG_LEVEL. Larger number means higher verbosity.

  Example:
  GDS_DEBUG_LEVEL=1 ./test
  This example specifies that gdsi_dprintf will output messages
  with GDS_DEBUG_ERR and GDS_DEBUG_WARN.
*/

enum gdsi_debug_level {
    GDS_DEBUG_ERR,  /* Severe error on an error path */
    GDS_DEBUG_WARN, /* Moderate error or warning */
    GDS_DEBUG_INFO, /* Verbose output, sometimes on regular path */
};

enum gdsi_ckpt_location_flag {
    GDS_CKPT_ON_MEMORY = 0x1,
    GDS_CKPT_ON_DISK   = 0x2,
};

extern int gdsi_dprintf_myrank;

#define gdsi_panic(fmt, ...)                                            \
    do {                                                                \
        fprintf(stderr,                                                 \
            "[%d] panic: %s:%d: " fmt, gdsi_dprintf_myrank,            \
            __FILE__, __LINE__, ##__VA_ARGS__);                         \
        abort();                                                        \
    } while (0)

#define gdsi_dprintf(_level, _fmt, ...)                                 \
    do {                                                                \
        if (_level <= gdsi_debug_level)                                 \
            fprintf(stderr, "[%3d] %s:%d: " _fmt, gdsi_dprintf_myrank,  \
                __FILE__, __LINE__, ##__VA_ARGS__);                     \
    } while (0)

#define GDSI_MPI_CALL(__e)                                     \
    do {                                                        \
    int __err, __rl;                                            \
        char __err_str[MPI_MAX_ERROR_STRING + 1];               \
        __err = (__e);                                          \
        if (__err != MPI_SUCCESS) {                             \
            MPI_Error_string(__err, __err_str, &__rl);          \
            gdsi_panic("%s returned %d (%s)\n", #__e, __err,    \
                __err_str);                                     \
        }                                                       \
    } while (0)

#define gdsi_assert(_e)                                         \
    do {                                                        \
        if (!(_e)) gdsi_panic("Assertion failed: %s\n", #_e);  \
    } while (0)

int gdsi_snprint_index(char *buff, size_t len, size_t ndims, const size_t dims[]);

#define GDSI_NBWORKQ_INFLIGHT_MAX 128 /* Max. number of inflight requests */
#define GDSI_OUTSTANDING_RMA_OPS_MAX_DEFAULT 32
#define GDSI_TARGET_BUFSIZE (1024*1024)
#define GDSI_LOG_RMA_GET_BATCH 8192 /* How many blocks to coalesce */
#define GDSI_LOG_RMA_PUT_BATCH 8192 /* How many blocks to coalesce */
#define GDSI_LOG_MD_PREFETCH_MAX_DEFAULT 16
#define GDSI_LRDS_DIRTY_BLOCK_SIZE_DEFAULT 64

#define GDSI_MIN(_A_, _B_)  ((_A_) < (_B_) ? (_A_) : (_B_))

extern int gdsi_thread_support;

/* Number of process crashes which this program encountered
   since start up */
extern int gdsi_crash_count;

/* Waiting schemes for target server thread */
enum {
    GDS_WAIT_SCHEME_DEFAULT,
    GDS_WAIT_SCHEME_SLEEP,
    GDS_WAIT_SCHEME_SPIN,
    GDS_WAIT_SCHEME_DOORBELL,

    GDS_WAIT_SCHEME_MAX,
};

GDS_status_t gdsi_target_init(void);
void gdsi_target_exit(void);
void gdsi_target_signal_thread(int target_rank);

void gdsi_target_global_lock(void);
void gdsi_target_global_unlock(void);

struct gdsi_gds_common;

struct gdsi_error_attr {
    struct gdsi_error_attr *next;
    GDS_error_attr_key_t attr_key;
    size_t value_len;
    char value[];
};

struct GDS_error_pred_term {
    struct GDS_error_pred_term *next;
    GDS_error_attr_key_t attr_key;
    GDS_error_match_expr_t match_expr;
    size_t value_len;
    char values[];
};

struct GDS_error_pred {
    int n_terms;
    size_t total_value_size;
    struct GDS_error_pred_term *terms;
};

struct gdsi_error_desc_flat {
    /* Total number of attributes */
    int n_attrs;

    /* Total size of this descriptor, including memory areas for
       attribute values */
    size_t total_size;

    /* Variable-length attributes: each field looks like:
       |key(GDS_error_attr_key_t)|value len(size_t)|value| */
    char attr_data[];
};

/*
  An opaque structure for descriptions of errors

  Memory allocation strategy:
  When allocating this object make sure to allocate additional memory
  area for storing attribute values and pointers to them.
*/
struct GDS_error {
    int n_attrs;

    /* Process rank that raised the error */
    int rank;

    /* Pointer to the next error desc in queue */
    struct GDS_error *next;

    /* Error desc counter */
    int counter;

    size_t total_value_size;

    struct gdsi_error_attr *attrs;
};

/*
  Chunk descriptor, a metadata for a chunk
*/
struct gdsi_chunk_desc {
    /* Rank of the target node which owns this chunk */
    int target_rank;
    int chunk_number;
    /* Index in the target memory window (by element-size unit)
       If this field is equal to GDSI_TARGET_INDEX_INVALID,
       it indicates that this chunk is not RMA accessible
       and therefore should use gdsi_gds_indirect_fetch. */
    MPI_Aint target_index;

    /* Length of the chunk (by element-size unit) 
       Length is flattend in a special way. If the array size is
       A x B x C x ..., length is encoded by calculating the index
       in the array of (A+1) x (B+1) x (C+1) x ...
       This is because the length in the first dimension l[0] may take
       0 <= l[0] <= A.
       To linearize/decompose the length, use linearize_size() and
       decompose_size(). */
    MPI_Aint size_flat;

    /* Index in the gds, flat (1-d) representation (by element-size unit) */
    MPI_Aint global_index_flat;

    /* For current version:
       Indicates if this chunk is lost and the content is
       initialized (undefined). Cleared when notified to the application.
       For old versions:
       Indicates if this chunk is lost and thus inaccessible.
       Never cleared. */
    bool is_corrupted;

    /* If the owner process of this chunk owns multiple chunks,
       this field indicates the chunk number of the "next".
       Negative number if there's no next chunk. */
    int next_sibling_chunk;

    /* The location of the chunk */
    int ckpt_location_flags;
};

MPI_Datatype gdsi_chunk_desc_type;

/*
  Chunk descriptor for users (GVS)
 */
struct gdsi_chunk {
    int target_rank;
    int chunk_number;
    MPI_Aint target_rma_index;
    MPI_Aint target_offset; /* Offset in target local version */
    size_t size;
    bool is_corrupted;
};

/* Log metadata type */
typedef uint32_t gdsi_log_md_t;

#define GDSI_LOG_USER_BLOCK_ELEMENTS_DEFAULT 32
#define GDSI_LOG_PREALLOC_DEFAULT 3

static inline size_t log_aligned_prev(size_t off, size_t block_size)
{
    size_t rem = off % block_size;
    return off - rem;
}

static inline size_t log_aligned_next(size_t off, size_t block_size)
{
    size_t prev = log_aligned_prev(off, block_size);
    if (prev < off)
        return prev + block_size;
    return off;
}

static inline
size_t log_required_blocks(size_t off, size_t len, size_t block_size)
{
    size_t pad_front = off - log_aligned_prev(off, block_size);
    size_t n = len + pad_front;
    size_t blks = n / block_size;
    if (n % block_size > 0)
        blks++;

    return blks;
}

static inline gdsi_log_md_t log_byteoff_to_md(size_t off, size_t block_size)
{
    return off / block_size;
}

static inline size_t log_md_to_byteoff(gdsi_log_md_t md, size_t block_size)
{
    return md * block_size;
}

static inline size_t log_pad_front(size_t off, size_t block_size)
{
    return off % block_size;
}

static inline size_t log_pad_back(size_t end_off, size_t block_size)
{
    return log_aligned_next(end_off, block_size) - end_off;
}

static inline size_t log_md_area_size(size_t singlever_size, size_t bs)
{
    size_t nblocks, padding;

    nblocks = log_required_blocks(0, singlever_size, bs);

    size_t md_true_size = sizeof(gdsi_log_md_t) * nblocks;

    /* Make the metadata blocks blocksize-aligned */
    padding = bs - md_true_size % bs;
    return md_true_size + padding;
}

static inline size_t log_memory_area_size(size_t singlever_size, size_t bs,
    int prealloc_vers)
{
    size_t md_size = log_md_area_size(singlever_size, bs);
    size_t nblocks;

    if (prealloc_vers <= 0)
        prealloc_vers = 1;

    nblocks = log_required_blocks(0, singlever_size, bs);

    return (md_size + bs * nblocks) * prealloc_vers
        + 2 * bs; /* 2 stands for the head and zero page */
}

bool gdsi_log_md_is_fresh(const struct gdsi_gds_common *common,
    int target_rank, gdsi_log_md_t md);
bool gdsi_log_md_is_fresh_any(const struct gdsi_gds_common *common,
    int target_rank, gdsi_log_md_t *mdc_start, int n_blocks);

gdsi_log_md_t *gdsi_log_get_md_cache(struct gdsi_gds_common *common,
    int target_rank, size_t target_offset);

size_t gdsi_log_md_index(struct gdsi_gds_common *common,
    int target_rank, size_t target_offset);

void gdsi_log_flush_enqueue(struct gdsi_gds_common *common,
    int target_rank, gdsi_log_md_t blk);

/**
   Decompose a flat (1d) index to multi-dimensional indices
 */
void gdsi_decompose_index_impl(size_t flat_index, size_t ndims, const size_t dims[], GDS_size_t indices[]);

void gdsi_decompose_index(GDS_size_t flat_index, const struct gdsi_gds_common *common, GDS_size_t indices[]);

size_t gdsi_linearize_index_impl(size_t ndims, const size_t dims[], const GDS_size_t indices[]);

void gdsi_decompose_size(size_t flat_size, const struct gdsi_gds_common *common, GDS_size_t sizes[]);

/**
   Linearize multi-dimensional indices to a flat index
 */
size_t gdsi_linearize_index(const struct gdsi_gds_common *common, const GDS_size_t indices[]);

/*
 * Rank and address translation
 */
int gdsi_map_owner_into_world(struct gdsi_gds_common *common, int rank);
int gdsi_get_array_id(const struct gdsi_gds_common *common, int trank);

GDS_status_t gdsi_clone_from_gds_common(struct gdsi_gds_common *common, GDS_gds_t *new_gds);

#define GDSI_TARGET_INDEX_INVALID ((MPI_Aint) -1)

/*
  Check if the chunk is directly accessible by remote memory access
*/
static inline
int gdsi_chunk_rma_accessible(size_t target_index)
{
    return target_index != GDSI_TARGET_INDEX_INVALID;
}

/* Chunk location info */
enum gdsi_chunk_location {
    GDSI_LOCATION_MEMORY, /* In memory */
    GDSI_LOCATION_LOCALFS, /* In local file system */
    GDSI_LOCATION_PFS, /* In PFS */
};

/*
  Chunk set - represents a set of chunk metadata for one version
*/
struct gdsi_chunk_set {
    struct gdsi_chunk_set *newer; /* To the newer version */
    struct gdsi_chunk_set *older; /* To the older version */
    GDS_size_t version; /* Version number */
    char   *label;      /* version label */
    size_t label_size;  /* version label size */

    /* Latest crash count number acknowledged by this version
       if latest_crash_count < gdsi_crash_count then this metadata
       could be obsolete and needs repairing */
    int latest_crash_count;

    MPI_Group world_group; /* Group view of gdsi_comm_world
                              at the moment of this version */
    struct gdsi_chunk_desc chunk_descs[];
};

enum gdsi_data_order {
    GDSI_ORDER_ROW_MAJOR, /* Row major -- C style */
    GDSI_ORDER_COL_MAJOR, /* Column major -- Fortran style */
};

struct gdsi_gds_flavor_alloc {
};

struct gdsi_gds_flavor_create {
    GDS_global_to_local_func_t global_to_local;
    GDS_local_to_global_func_t local_to_global;  
};

union gdsi_gds_flavor {
    struct gdsi_gds_flavor_alloc flavor_alloc;
    struct gdsi_gds_flavor_create flavor_create;
};

/*
  Internal representation of an error handler
*/
struct gdsi_error_handler {
    struct gdsi_error_handler *next;
    GDS_recovery_func_t recovery_func;
    int n_terms;
    struct GDS_error_pred_term *terms;
    char raw_buff[];
};

/*
    Function list - the linkedlist of error check function
*/
struct gdsi_check_fun {
    struct gdsi_check_fun *next; /* To the  next function*/
    GDS_check_func_t check_func;
    GDS_priority_t check_priority;
};

/* Functions related to error attribute predicates -- defined in gvs_error_handling.c */
GDS_error_attr_key_t gdsi_define_error_attr_key(
    const char *attr_name,
    GDS_error_attr_value_type_t value_type);
bool gdsi_validate_error_attr(GDS_error_attr_key_t key, size_t val_len);
size_t gdsi_error_desc_flat_size(const GDS_error_t desc);
struct gdsi_error_desc_flat *gdsi_flatten_error_desc(const GDS_error_t desc);
GDS_error_t gdsi_expand_error_desc(const struct gdsi_error_desc_flat *flat);
struct gdsi_error_attr *gdsi_find_error_attr(
    GDS_error_t error_desc,
    GDS_error_attr_key_t attr_key);
const struct gdsi_error_handler *gdsi_match_error_handler(
    struct gdsi_gds_common *common,
    const struct gdsi_error_handler *eh_queue,
    GDS_error_t error_desc);
void gdsi_free_error_desc(GDS_error_t desc);

enum gdsi_data_repr {
    GDSI_REPR_INVALID = -1, /* Invalid */

    GDSI_REPR_FLAT = 0, /* Flat array representation */
    GDSI_REPR_LOG,  /* Log-structured array representation */
    GDSI_REPR_LRDS, /* Flat array with LRDS versioning */
};

struct gdsi_chunk_ops;
struct gdsi_nbworkq_entry;

struct gdsi_log_inflight_req {
    struct gdsi_log_inflight_req *next;
    int target_rank;
    uint32_t block_id;
};

struct gdsi_log_stock_queue;

#define GDS_LOG_FLUSH_MAXENTS 128

struct gdsi_target_log_flush {
    int array_id;
    int tag; /* MPI tag for data */
};

struct gdsi_log_flush_queue {
    struct gdsi_target_log_flush lf;
    gdsi_log_md_t blocks[GDS_LOG_FLUSH_MAXENTS];
    int n_blocks;
    MPI_Request reqs[2];
};

#define GDSI_CUR_CHUNK_PER_PROC_MAX_DEFAULT 4

struct gdsi_gds_common {
    GDS_comm_t comm;
    int *array_ids;

    MPI_Group owner_group;
    MPI_Group non_owner_group; /* = comm_world - my_group */

    struct gdsi_gds_common *next;
    struct gdsi_gds_common *prev;

    GDS_size_t ndims; //number of dimensions for the array
    GDS_size_t *nelements; //number of elements in each dimension
    GDS_size_t *nelements_local; // number of elements in each dim on this proc
    GDS_size_t *offset_local;
    GDS_size_t nelements_local_flat; // cache number of bytes in local buffer

#ifdef GDS_CONFIG_USE_LRDS
    /* cached by register_lrds_callback so we can unregister later */
    void *lrds_buf; 
    size_t lrds_len;
#endif

    GDS_datatype_t type;
    enum gdsi_data_order order;
    enum gdsi_data_repr  representation;
    const struct gdsi_chunk_ops *ops;

    MPI_Win win;

    /*
      Mutable members below -- should be protected by the mutex lock

      This lock is to protect structure members in the multi-threaded
      execution on single node.
      It does nothing for multi-node mutual exclusions, therefore we
      definitely need another global mutex such as written in the
      "Using MPI-2" book.
    */
    pthread_mutex_t lock;

    /* reference counting rule:
       refcount represents the total number of GDS_gds_t objects
       which point to this common object. */
    int refcount;

    GDS_reliability_level_t reliability_level;
 
    struct gdsi_error_handler *global_error_handler_queue;
    struct gdsi_error_handler *local_error_handler_queue;

    GDS_error_t global_error_desc_queue;

    /*keep error desc from external events*/
    GDS_error_t local_error_desc_queue;

    /*global error raised by me in history*/
    int global_error_desc_counter;

    int local_error_desc_counter;

    int simulated_failed_proc;

    struct gdsi_check_fun *global_check_funs[GDS_PRIORITY_MAX];
    struct gdsi_check_fun *local_check_funs[GDS_PRIORITY_MAX];
    
    /*mode flag on if the error handler is invoked*/
    int recovery_mode;

    GDS_size_t latest_version;

    int nchunk_descs; /* Total number of chunks (per each version) */

    /* Doubly-linked list of chunk sets to represents version metadata
       The newest version comes first, the oldest comes last.
       Version number starts from zero, then is incremented monotonicly. */
    struct gdsi_chunk_set *chunk_sets;

    /*
      Set of chunk IDs that this process currently owns
      Usually there is only one chunk and chunk number is equal to
      client rank. However some process may own multiple chunks after
      process crash.
    */
    int *owning_chunks; /* Array of chunk IDs */
    int n_owning_chunks; /* Number of elements I currently have */
    int n_owning_chunks_max; /* Maximum # of elelements in owning_chunks */

    GDS_flavor_t flavor;
    union gdsi_gds_flavor flavor_attrs;
    GDS_global_to_local_func_t global_to_local_func;
    GDS_local_to_global_func_t local_to_global_func;

    /* Work queue of outstanding non-blockin remote operations */
    struct gdsi_nbworkq_entry *nbworkq;
    int nbworkq_n_ents;
    int nbworkq_n_ents_max;
    MPI_Request *nbworkq_reqs;
    unsigned nbworkq_counter; /* for debugging */

    int n_outstanding_rma_ops;

    enum GDS_get_impl get_impl;
    enum GDS_put_impl put_impl;

    /* LRDS-related members */
    int lrds_prop_dirty_tracking;
    size_t lrds_prop_dirty_block_size;
    MPI_Win lrds_dirty_win;

    size_t log_user_bs;
    size_t *log_size_limits;
    gdsi_log_md_t *log_md_cache;
    int *log_md_recvcounts;
    int *log_md_displs;
    gdsi_log_md_t *log_cur_md_heads;
    uint64_t *log_tail_cache;
    gdsi_log_md_t *log_tail_max; /* Known maximum of log tail */
    struct gdsi_log_flush_queue *log_flush_queues;

    struct gdsi_log_stock_queue **log_db_stocks;
    struct gdsi_log_inflight_req *log_preceding_alloc[GDSI_NBWORKQ_INFLIGHT_MAX];

#ifdef GDS_CONFIG_COUNTER
    struct gdsi_counter counter;
#endif

    /* MPI_Request for non-blocking gds_version_inc() */
    MPI_Request reqs[4];
    int n_reqs;

    /* Flag of this handle, indiciates whether the caller process has reached global GDS_version_inc() call or not */
    int version_inc_state;

    /* A linked-list to record all pending reads of the caller process */
    struct gdsi_get_info *get_info;

};

struct gdsi_get_info {
    void *origin_addr;
    GDS_size_t *origin_ld;
    GDS_size_t *lo_index;
    GDS_size_t *hi_index;

    struct gdsi_get_info *next;
};

void gdsi_append_get(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds);

void gdsi_destroy_get_info(struct gdsi_get_info **get_info);

/*
  GDS handle - an opaque structure to the users
*/
struct GDS_gds {
    struct gdsi_gds_common *common;

    /* Chunk set for the "current version" of this handle */
    struct gdsi_chunk_set *chunk_set;

};

/*
  Chunk iterator - iterates over a single row
  (in the column-major mode, read row as column)

  In the future, this might be extended to handle multiple columns/
  entire array.

  Should be opaque to the API layer
  Do not access the contents directly. Use accessor functions provided below.
*/
typedef struct gdsi_chunk_iterator {
    struct gdsi_gds_common *common;
    size_t lastdim_len; /* size of last dimension */
    size_t last2dim_begin; /* start index in 2nd last dimension */
    size_t global_index_flat;
    const struct gdsi_chunk_desc *cd_begin;
    const struct gdsi_chunk_desc *cd_cur;
    const struct gdsi_chunk_desc *cd_end;
} gdsi_chunk_iterator_t;

GDS_status_t gdsi_find_first_chunkit(GDS_gds_t gds,
    const size_t start[], gdsi_chunk_iterator_t *it);

/*
  Move forward the iterator so that it points to the next chunk
*/
void gdsi_chunkit_forward(gdsi_chunk_iterator_t *it);

/*
  Check if the iterator has reached the end
*/
int gdsi_chunkit_end(const gdsi_chunk_iterator_t *it);

void gdsi_chunkit_access(const gdsi_chunk_iterator_t *it, struct gdsi_chunk *chunk);

/* Releases resorces associated with the iterator */
static inline void gdsi_chunkit_release(gdsi_chunk_iterator_t *it)
{
    /* Do nothing - reserved for future extension */
}

/*
  Check and prepare buffer in the target process
  common: gds common structure
  target_rank: rank of target process
  target_index: 
  chunk_number:
  new_version: absolute version number to be checked
*/
GDS_status_t gdsi_check_new_version(struct gdsi_gds_common *common, int target_rank, 
        size_t target_index, int chunk_number, GDS_size_t new_version);

/*
  Synchronize metadata for all client processes
*/
void gdsi_metadata_sync(struct gdsi_gds_common *common);

/*
  Create a new version
  common: gds common structure
  new_version: absolute version number to be created

  MUST be called with common->lock held
 */
GDS_status_t gdsi_create_new_version(struct gdsi_gds_common *common,
    GDS_size_t new_version);

/**
 * Alocates a new gds and its backend buffers at the target side
 * arguments:
 *  count: number of elements to expose
 *  datatype: type of elements
 *  comm: The communicator that should be able to access this gds
 *  info: Hints for optimization which do not affect semantics.
 *  common: Handle to new gds common structure
 */
GDS_status_t gdsi_alloc(size_t ndims, const size_t count[],
    const size_t min_chunks[], GDS_datatype_t datatype,
    GDS_comm_t comm, MPI_Info info, struct gdsi_gds_common **common);

GDS_status_t gdsi_create(size_t ndims, const size_t counts[],
    GDS_datatype_t datatype, 
    GDS_global_to_local_func_t global_to_local_function,
    GDS_local_to_global_func_t local_to_global_function,
    void *local_buffer, size_t local_buffer_count, GDS_comm_t users, 
    MPI_Info info, struct gdsi_gds_common **common);

/*
 * frees gds. Replaces handle with an invalid pointer.
 * arguments:
 * common: gds common structure to be freed
 */
GDS_status_t gdsi_free(struct gdsi_gds_common **common);

/*
 * Retrieve data from a specified chunk
 * arguments:
 *  gds: handle to the gds to be accessed
 *  target_rank: rank of the target node
 *  buf: pointer to buffer into which data will be copied
 *  offset: element-wise offset from the beginning of the chunk to start reading
 *  count: number of elements to be copied
 *  wq: non-blocking workqueue entry (NULL if traditional blocking mode)
 */
GDS_status_t gdsi_indirect_fetch(GDS_gds_t gds, size_t target_rank,
    void *buf, size_t offset, size_t count, int chunk_number, struct gdsi_nbworkq_entry **wq);

/**
  Put data to the array
  Only applicable for log-structured array

 @param gds          handle to the gds to be accessed
 @param target_rank  rank of the target node
 @param buf          pointer to buffer from which data will be copied
 @param offset       element-wise offset from the beginning of the chunk to start reading
 @param count        number of elements to be put
 @param wqp          a pointer to a non-blocking workq object is returned
 */
GDS_status_t gdsi_indirect_put(GDS_gds_t gds, int target_rank,
    const void *buf, size_t offset, size_t count,
    bool acc_mode, MPI_Op acc_op, struct gdsi_nbworkq_entry **wqp);

GDS_status_t gdsi_log_alloc(GDS_gds_t gds, int target_rank,
    size_t offset, size_t count, struct gdsi_nbworkq_entry **wqp);

/* Replace communicator associated with GDS.
   Returns a number of newly-owned chunks by calling process */
int gdsi_replace_comm(struct gdsi_gds_common *common, MPI_Comm new_comm,
    MPI_Group new_comm_world);

/*
  Make sure that the array metadata for certain version
  is up-to-date (i.e. reflecting data loss after process
  crash)
  It is safe to apply this function to a version which is
  already repaired. In such a case this function does nothing
  and returns immediately.
*/
void gdsi_repair_old_metadata(struct gdsi_gds_common *common,
    struct gdsi_chunk_set *target_ver);

/* Sends a request message to target side service */
void gdsi_send_target_request(const void *buff, int count,
    MPI_Datatype datatype, int target_rank, int tag);

void gdsi_isend_target_request(const void *buff, int count,
    MPI_Datatype datatype, int target_rank, int tag,
    MPI_Request *req);

enum gdsi_target_message_tag {
    /*
      Fundamental rule here:
      GDS_TGTREQ_* should come at the beginning of the type definition
      (should begin with 0)
    */

    /* Requests to target */
    GDS_TGTREQ_CREATE,
    GDS_TGTREQ_INCVER,
    GDS_TGTREQ_STORE_CHUNK,
    GDS_TGTREQ_FLUSH_SCR,
    GDS_TGTREQ_FETCH,
    GDS_TGTREQ_PUT,
    GDS_TGTREQ_LOG_ALLOC,
    GDS_TGTREQ_LOG_FLUSH,
    GDS_TGTREQ_VADDR_TO_GDS,
    GDS_TGTREQ_SIM_FAILURE,
    GDS_TGTREQ_RECONSTRUCT,
    GDS_TGTREQ_FREE,
    GDS_TGTREQ_EXIT,

    GDS_TGTREQ_LAST, /* dummy entry -- only for a marker */

    /* Replies from target */
    GDS_TGTREP_GENERIC, /* Generic reply */
    GDS_TGTREP_CREATE,
    GDS_TGTREP_VADDR_TO_GDS_CT,
    GDS_TGTREP_VADDR_TO_GDS_RES,
    GDS_TGTREP_STORE_CHUNK,
    GDS_TGTREP_FLUSH_SCR,

    GDS_TGTREP_DYNAMIC_BEGIN,
};

struct gdsi_target_create {
    int disp_unit;
    GDS_flavor_t flavor;
    void *local_buffer;
    MPI_Aint  size; /* Array size in elements */
    enum gdsi_data_repr representation;

    /* Only used by log-structured array */
    size_t log_user_bs; /* Block size in bytes */
    int log_prealloc_vers; /* RMA buffer size (in # of versions) to allocate */

    /* Only used by LRDS versioning */
    int lrds_prop_dirty_tracking; /* LRDS_PROPERTY_DIRTY_TRACKING */
    int lrds_prop_versioning; /* LRDS_PROPERTY_VERSIONING */
    size_t lrds_prop_dirty_block_size; /* LRDS_PROPERTY_DIRTY_BLOCK_SIZE */
    bool lrds_expose_dirty; /* Expose dirty bit table through RMA? */
};

struct gdsi_target_alloc_result {
    int array_id;
    GDS_status_t status;
    MPI_Aint start_index;
};

struct gdsi_target_incver {
    int array_id;
    size_t version; /* number of version to be created */
    int target_rank;
    size_t store_version; /* version to be stored */
    int pca_target_rank; /* PCA target rank */
    int pca_array_id;
    int chunk_number;
    int chunk_size;
    int owner_flag;
    MPI_Aint target_index; /* offset in win */
};

struct gdsi_target_store_chunk {
    int array_id;
    size_t version; /* number of version to be created */
    size_t store_version; /* version to be stored */
    int pca_target_rank; /* PCA target rank */
    int pca_array_id;
    int chunk_number;
    size_t chunk_size;
    int owner_flag;
    MPI_Aint target_index; /* offset in win */
};

struct gdsi_target_flush_scr {
    int array_id;
    int unique_array_id; /* array_id on process 0 */
    GDS_size_t version;
    int chunk_number;
};

struct gdsi_target_fetch {
    int array_id;
    int unique_array_id; // array id on process 0
    size_t version;
    int chunk_number;
    int ckpt_location_flags;
    size_t offset; /* Offset in elements */
    size_t count;  /* Number of elements */
    int tag_status; /* MPI tag for return status */
    int tag_data;   /* MPI tag for return data */
};

struct gdsi_target_put {
    int array_id;
    size_t offset; /* Offset in elements */
    size_t count;  /* Number of elements */
    int tag_status; /* MPI tag for return status */
    int tag_data;   /* MPI tag for data */
    bool acc_mode;
    MPI_Op acc_op;
    MPI_Datatype acc_type; /* TODO: ugly, and is it correct?? */
};

/* request from client to translate a virtual address into a number of arrays */
struct gdsi_target_vaddr {
    void *vaddr;
    size_t len;
};

/* an array id and a linear offset in the array. Used to process 
   vaddr_to_gds requests */
struct gdsi_id_and_offset {
    int array_id;
    size_t offset;
    size_t len;

    /*used during construction in target, shouldn't be used in client */
    struct gdsi_id_and_offset *next; 
};

struct gdsi_target_reconstruct {
    int array_id;

    /* Number of elements to add in this target */
    MPI_Aint additional_count;
};

/* Forward declaration -- body is in target.c */
struct gdsi_target_checkpoint;
struct gdsi_target_fetch_priv;
struct gdsi_target_put_priv;

/* Array descriptor on target side */
struct gdsi_target_array_desc {
    struct gdsi_target_array_desc *prev, *next;
    enum gdsi_data_repr representation;
    int array_id;

    /* Operators depending on data representation */
    void (*fetch)(struct  gdsi_target_array_desc *desc,
        const struct gdsi_target_fetch *ft, int client_rank,
        struct gdsi_target_fetch_priv *priv);
    void (*put)(struct  gdsi_target_array_desc *desc,
        const struct gdsi_target_put *lw, int client_rank,
        struct gdsi_target_put_priv *priv);
    GDS_status_t (*increment_version)(struct gdsi_target_array_desc *desc);

    /* Buffer memory to be exposed to other processes through win */
    void *buff;
    MPI_Win win;

    /* TODO: should be obtained from win, but for some reason garbage
       value is returned */
    int disp_unit;
    size_t singlever_size; /* size (in bytes) */
    GDS_flavor_t flavor; /* GDS_alloc or GDS_create */

    int chunk_number;
    size_t current_version;

    /* Linear list of versions, newest version in the front (top) */
    struct gdsi_target_checkpoint *checkpoints;

#ifdef GDS_CONFIG_USE_LRDS
    /* Members needed by the LRDS representations */
    lrds_region_t lrds_region; /* region descriptor */
    int lrds_dirty_tracking; /* Scheme for dirty bit tracking */
    uint8_t *lrds_dirty_buff; /* Buffer for dirty bit tracking area */
    size_t lrds_dirty_buff_len;
    MPI_Win lrds_dirty_win; /* RMA window for dirty bit tracking area */
#endif

    /* Members needed by the log-structured representation */
    size_t log_area_size; /* Total size of log area */
    size_t log_bs; /* block size which the system sees */
    size_t log_user_bs; /* block size for the user contents */
    size_t log_cur_md_head;
    pthread_mutex_t log_lock;
    /* members below should be protected by log_lock */
    uint64_t *log_tailp;
};

/* A global communicator to be shared with client side and target side */
extern MPI_Comm gdsi_comm_world;
extern MPI_Group gdsi_group_world;

struct gdsi_target_array_desc *gdsi_find_array_desc(int array_id);

static inline int gdsi_pthread_mutex_lock(pthread_mutex_t *mtx) {
    if (gdsi_thread_support == GDS_THREAD_MULTIPLE)
        return pthread_mutex_lock(mtx);
    return 0;
}

static inline int gdsi_pthread_mutex_unlock(pthread_mutex_t *mtx) {
    if (gdsi_thread_support == GDS_THREAD_MULTIPLE)
        return pthread_mutex_unlock(mtx);
    return 0;
}

static inline void gdsi_common_lock(struct gdsi_gds_common *common)
{
    gdsi_pthread_mutex_lock(&common->lock);
}

static inline void gdsi_common_unlock(struct gdsi_gds_common *common)
{
    gdsi_pthread_mutex_unlock(&common->lock);
}

struct gdsi_mutex {
    MCS_Mutex mcs_mtx;
};

GDS_status_t gdsi_mutex_init(struct gdsi_mutex *gds_mtx, MPI_Comm comm);
GDS_status_t gdsi_mutex_lock(struct gdsi_mutex *gds_mtx);
GDS_status_t gdsi_mutex_trylock(struct gdsi_mutex *gds_mtx, int *success);
GDS_status_t gdsi_mutex_unlock(struct gdsi_mutex *gds_mtx);
GDS_status_t gdsi_mutex_destroy(struct gdsi_mutex *gds_mtx);

int gdsi_inc_global_index(size_t ndims, GDS_size_t global_indices[], 
    const GDS_size_t counts[]);
struct gdsi_vaddr_to_gds_response {
    //GDS_gds_t gds; TODO return gds instead of common
    struct gdsi_gds_common *common;
    GDS_size_t ct;
    struct gdsi_vaddr_to_gds_response *next;
    GDS_size_t start[];
};

GDS_status_t gdsi_get_common_by_array_id(const int array_id, 
    struct gdsi_gds_common **common);
GDS_status_t gdsi_vaddr_to_gds(void *vaddr, size_t err_len, 
    size_t *n_responses, struct gdsi_vaddr_to_gds_response **responses);

GDS_status_t gdsi_register_lrds_callback(struct gdsi_gds_common *common); 
GDS_status_t gdsi_unregister_lrds_callback(struct gdsi_gds_common *common); 

extern pthread_mutex_t gdsi_simfail_mtx;
extern pthread_cond_t gdsi_simfail_pause;

/*
  Minimum chunk decomposition
  defined in minchunk_split.c
*/
struct gdsi_minchunk;
typedef struct gdsi_minchunk gdsi_minchunk_t;

gdsi_minchunk_t *gdsi_minchunk_split(size_t nprocs,
    size_t ndims,
    const size_t dims[],
    const size_t min_chunks[]);
void gdsi_minchunk_get(const gdsi_minchunk_t *mc,
    size_t proc,
    size_t res_ind[],
    size_t res_len[]);
size_t gdsi_minchunk_size(const gdsi_minchunk_t *mc,
    size_t proc);
size_t gdsi_minchunk_maxsize(const gdsi_minchunk_t *mc);
void gdsi_minchunk_free(gdsi_minchunk_t *mc);

/*
  Dence bit vector
  defined in bitvec.c

  Assumptions:
  * 1 byte == 8 bits
*/

/* Low level APIs */

size_t gdsi_bitvecl_size(size_t max);
void gdsi_bitvecl_set_all(uint8_t *buf, size_t max);
void gdsi_bitvecl_clear_all(uint8_t *buf, size_t max);
void gdsi_bitvecl_set(uint8_t *buf, size_t n);
void gdsi_bitvecl_clear(uint8_t *buf, size_t n);
int gdsi_bitvecl_get(const uint8_t *buf, size_t n);

/* High level APIs */

struct gdsi_bitvec;

void gdsi_bitvec_set_all(struct gdsi_bitvec *bv);
void gdsi_bitvec_clear_all(struct gdsi_bitvec *bv);
struct gdsi_bitvec *gdsi_bitvec_new(size_t max);
void gdsi_bitvec_set(struct gdsi_bitvec *bv, size_t n);
void gdsi_bitvec_clear(struct gdsi_bitvec *bv, size_t n);
int gdsi_bitvec_get(const struct gdsi_bitvec *bv, size_t n);
/*
  Find first set bit
  Returns the first bit position from start where bit is set.
  If the start bit is set, start is returned. If no bit is set,
  returns a negative number.
*/
int gdsi_bitvec_ffs(const struct gdsi_bitvec *bv, size_t start);
uint8_t *gdsi_bitvec_raw_buffer(struct gdsi_bitvec *bv);
void gdsi_bitvec_delete(struct gdsi_bitvec *bv);

/* Workqueue infrastructure for non-blocking remote operations */
typedef int (*gdsi_nbworkq_test_t)(struct gdsi_nbworkq_entry *wq);
typedef MPI_Request * (*gdsi_nbworkq_request_t)(struct gdsi_nbworkq_entry *wq);
typedef int (*gdsi_nbworkq_ready_t)(struct gdsi_nbworkq_entry *wq, MPI_Status *status);

struct gdsi_nbworkq_entry {
    struct gdsi_nbworkq_entry *next;
    struct gdsi_nbworkq_entry *prev;
    GDS_gds_t gds;

    bool rma;
    gdsi_nbworkq_test_t test;
    gdsi_nbworkq_request_t request;
    gdsi_nbworkq_ready_t ready;

    unsigned id; /* for debugging */
};

void gdsi_nbworkq_init(struct gdsi_nbworkq_entry *wq, GDS_gds_t gds,
    gdsi_nbworkq_test_t test, gdsi_nbworkq_request_t request,
    gdsi_nbworkq_ready_t ready);

/* Dynamic tag system */
GDS_status_t gdsi_dyntag_init(MPI_Comm comm);
int gdsi_dyntag_alloc(MPI_Comm comm, int target_rank);
void gdsi_dyntag_free(MPI_Comm comm, int target_rank, int tag);
void gdsi_dyntag_finalize(MPI_Comm comm);

/* GDS_access */
struct GDS_access_handle {
    struct gdsi_gds_common *common;
    GDS_access_buffer_t buf_type;
    void *orig_buf;
    void *buf;
    size_t buf_size;
};

/* Calculate target rank for pca version placement */
int gdsi_get_pca_target_rank(int client_rank, int owner_num, int red_num,
GDS_size_t version_num);
/* Filenaming for utilizing SCR */
void gdsi_construct_filename(int array_id, int version_number, int
chunk_number, char *filename);
int gdsi_write_chunk_to_file(void *buf, char *filename, size_t size);
int gdsi_read_chunk_from_file(void *buf, char *filename, size_t offset, size_t size);
int gdsi_getfile(char *dir_name, const char *name, char *file);
int gdsi_get_base_path(char *dir_name);
int gdsi_get_pfs_path(char *pfs_name);
bool gdsi_check_pfs_available(int unique_array_id, int version_number, int
chunk_number);
/* Get environmental variables */
void gdsi_get_env_params();
#endif
