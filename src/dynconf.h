/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.

  Dynamic configuration infrastructure using environment variables
*/

#ifndef DYNCONF_H_INCLUDED
#define DYNCONF_H_INCLUDED

/* Declarations of dynamically configurable variables */
extern int gdsi_debug_level;
extern int gdsi_order_default;
extern int gdsi_show_info;
extern int gdsi_repr_default;
extern int gdsi_server_threads;
extern int gdsi_log_bs_default;
extern int gdsi_wait_scheme;
extern int gdsi_log_md_prefetch_max;
extern int gdsi_log_flush_on_padding;
extern int gdsi_log_prealloc_default;
extern int gdsi_omit_error_check_on_versioning;
extern int gdsi_enable_slow_memcpy;
extern int gdsi_ignore_versioning;
extern int gdsi_outstanding_rma_ops_max;
extern int gdsi_exit_on_free;
extern int gdsi_log_speculative_alloc;
extern int gdsi_wait_sleep_duration;
extern int gdsi_wait_spin_count;
extern int gdsi_dontcare_thread_multiple;
extern int gdsi_sanity_check;
extern int gdsi_disable_pca;
extern int gdsi_log_fetch_and_op;

/* Type definitions for the infrastructure */
struct gdsi_dynconf_entry;
typedef void (*gdsi_dynconf_parser_t)(const char *val_str, void *value);
typedef void (*gdsi_dynconf_printer_t)(const char *key, const void *value);

struct gdsi_dynconf_entry {
    const char *env_key;
    void *variable;
    gdsi_dynconf_parser_t parse;
    gdsi_dynconf_printer_t print;
};

void gdsi_dynconf_init(void);
void gdsi_dynconf_print(void);

#endif /* DYNCONF_H_INCLUDED */
