/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gds.h>

#include "gds_internal.h"

GDS_status_t gdsi_mutex_init(struct gdsi_mutex *gds_mtx, MPI_Comm comm){

    if (MCS_Mutex_create(0, comm, &gds_mtx->mcs_mtx)== MPI_SUCCESS)
        return GDS_STATUS_OK;

    return GDS_STATUS_ERR_GENERAL;
}

GDS_status_t gdsi_mutex_lock(struct gdsi_mutex *gds_mtx){

    if (MCS_Mutex_lock(gds_mtx->mcs_mtx) == MPI_SUCCESS)
        return GDS_STATUS_OK;

    return GDS_STATUS_ERR_GENERAL;
}

GDS_status_t gdsi_mutex_trylock(struct gdsi_mutex *gds_mtx, int * success){
    if (MCS_Mutex_trylock(gds_mtx->mcs_mtx, success) == MPI_SUCCESS)
        return GDS_STATUS_OK;

    return GDS_STATUS_ERR_GENERAL;
}

GDS_status_t gdsi_mutex_unlock(struct gdsi_mutex *gds_mtx){

    if (MCS_Mutex_unlock(gds_mtx->mcs_mtx) == MPI_SUCCESS)
        return GDS_STATUS_OK;

    return GDS_STATUS_ERR_GENERAL;
}

GDS_status_t gdsi_mutex_destroy(struct gdsi_mutex *gds_mtx){

    if (MCS_Mutex_free(&gds_mtx->mcs_mtx) == MPI_SUCCESS)
        return GDS_STATUS_OK;

    return GDS_STATUS_ERR_GENERAL;
}
