/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.

  This module implements the Distributed Metadata Service (DMS).
*/

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include <unistd.h>
#include "sglib.h"

#include "gds_internal.h"

extern struct gdsi_gds_common *gdsi_gds_common_list;
extern pthread_rwlock_t gdsi_gds_common_list_lock;

#define GetCeil(x) (int)ceil((double)x)
/*
 * Calculate the target rank for version placement which avoids process crash.
 */
int gdsi_get_pca_target_rank(int chunk_number, int owner_num, int nonowner_num, GDS_size_t version_num)
{
    int rank = chunk_number;
    int pca_target_rank = chunk_number;
    int left = 0, right = 0;
    /* GVR version starts from 0. But the pca target rank calculation expects
     * version number greater than 0. So we increase the version number by 1. */
    version_num += 1;

    if (nonowner_num == 0) { /* No redundant proc */

        /* Range of proc which will not store blocks in redundant procs */
        left = (version_num - 1)%owner_num;
        right = (GetCeil(owner_num/2) + version_num - 2)%owner_num;

        if (left <= right && rank > right) {
            rank = left + (rank - right) - 1;
        } else if (left <= right && rank < left) {
            // rank = rank;
        } else if (left > right && rank > right && rank < left) {
            rank = rank - right - 1;
        } else
            goto out;

        pca_target_rank = (left + rank)%owner_num;

    } else if (nonowner_num > 0 && nonowner_num < GetCeil(owner_num/2)) { /* # of redundant procs is less than half of owners */

        /* Range of proc which will not store blocks in redundant procs */
        left = (version_num - 1)%owner_num;
        right = (GetCeil(owner_num/2) - nonowner_num + version_num - 2)%owner_num;

        if (left <= right && rank > right) {
            rank = left + (rank - right) - 1;
        } else if (left <= right && rank < left) {
            // rank = rank;
        } else if (left > right && rank > right && rank < left) {
            rank = rank - right - 1;
        } else
            goto out;

        if (rank < 2*nonowner_num) /* Target proc is the redundant proc*/
            pca_target_rank = owner_num + rank/2;
        else
            pca_target_rank = (left + (rank - 2*nonowner_num))%owner_num;

    } else if (nonowner_num >= GetCeil(owner_num/2) && nonowner_num < owner_num) {
        /* # of redundant procs is greater than half of owners */
        pca_target_rank = owner_num + (rank/2 + version_num - 1)%nonowner_num;

    } else if (nonowner_num >= owner_num)
        pca_target_rank = owner_num + (rank + version_num - 1)%nonowner_num;

out:
    if (pca_target_rank >= owner_num + nonowner_num) {
        pca_target_rank = (chunk_number + version_num) % (owner_num + nonowner_num);
    }

    gdsi_dprintf(GDS_DEBUG_INFO, "chunk_number=%d version_number=%zu owner_num=%d nonowner_num=%d, pca_target_rank=%d\n",
        chunk_number, version_num, owner_num, nonowner_num, pca_target_rank);

    /* Basic sanity check: target rank number should be lower
       than the total number of processes in the program */
    gdsi_assert(pca_target_rank < owner_num + nonowner_num);

    return pca_target_rank;
}

static size_t linearize_count(size_t ndim, const size_t count[]);

static void print_chunk_desc(const struct gdsi_gds_common *common, int i, const struct gdsi_chunk_desc *desc)
{
    size_t ndims = common->ndims;
    size_t index[ndims], size[ndims];
    char index_s[16], size_s[16];

    gdsi_decompose_index(desc->global_index_flat, common, index);
    gdsi_decompose_size(desc->size_flat, common, size);

    gdsi_snprint_index(index_s, sizeof(index_s), ndims, index);
    gdsi_snprint_index(size_s, sizeof(size_s), ndims, size);

    gdsi_dprintf(GDS_DEBUG_INFO,
        "Chunk metadata[%d]:\n"
        "  Global index: [%s]\n"
        "  Target rank:   %d\n"
        "  Target index: %zu\n"
        "  Size: [%s]\n",
        i,
        index_s, desc->target_rank,
        desc->target_index, size_s);
}

static void print_log_md_cache(const struct gdsi_gds_common *common, int t)
{
    int i;
    int disp = common->log_md_displs[t], len = common->log_md_recvcounts[t];
    char buff[9*4 + 1], *p;

     gdsi_dprintf(GDS_DEBUG_INFO,
         "MD cache of target %d: disp=%d len=%d cur_md_head=%d\n",
         t, disp, len, common->log_cur_md_heads[t]);

     p = buff;
     for (i = disp; i < disp + len; i++) {
         int r = sprintf(p, "%08x ", common->log_md_cache[i]);
         gdsi_assert(r == 9);
         p += r;
         if ((p - buff) / 9 >= 4) {
             gdsi_dprintf(GDS_DEBUG_INFO, "%s\n", buff);
             p = buff;
         }
     }
     if (p > buff)
         gdsi_dprintf(GDS_DEBUG_INFO, "%s\n", buff);
}

static void print_array_ids(const struct gdsi_gds_common *common)
{
    int i, sz;
 
    MPI_Comm_size(gdsi_comm_world, &sz);

    for (i = 0; i < sz; i++)
        gdsi_dprintf(GDS_DEBUG_INFO,
            "array_ids[%d]=%d\n", i, common->array_ids[i]);
}

static int fits_in_chunk(const struct gdsi_gds_common *common,
    const struct gdsi_chunk_desc *cd, const size_t start[])
{
    size_t ndims = common->ndims;
    size_t chunk_lo[ndims];
    size_t chunk_len[ndims];
    size_t i;

    gdsi_decompose_index(cd->global_index_flat, common, chunk_lo);
    gdsi_decompose_size(cd->size_flat, common, chunk_len);

    for (i = 0; i < ndims; i++) {
        if (start[i] < chunk_lo[i])
            return 0;
        if (start[i] >= chunk_lo[i] + chunk_len[i])
            return 0;
    }

    return 1;
}

/* Do a binary search. Currently only works for 1-d array metadata. */
static int find_first_binsearch(const struct gdsi_chunk_desc *desc, int len,
MPI_Aint start)
{
    int s = 0;
    int l = len;
    while (l > 1) {
        int i = s + l/2;
        if (desc[i].global_index_flat == start)
            return i;
        else if (desc[i].global_index_flat < start) {
            s += l/2;
            l = (l+1)/2;
        } else
            l /= 2;
    }

    gdsi_assert(s >= 0 && s < len);
    gdsi_assert(desc[s].global_index_flat <= start);

    return s;
}

static int find_first_chunk_index(const struct gdsi_gds_common *common,
    const struct gdsi_chunk_desc *desc, int len, const size_t start[])
{
    int s = -1, i;

    if (common->ndims == 1) {
        return find_first_binsearch(desc, len, start[0]);
    }
    /* TODO: implement more efficient algorithm than linear search! */
    for (i = 0; i < len; i++) {
        if (fits_in_chunk(common, &desc[i], start)) {
            s = i;
            break;
        }
    }

    gdsi_assert(s >= 0 && s < len);

    return s;
}

void gdsi_send_target_request(const void *buff, int count,
    MPI_Datatype datatype, int target_rank, int tag)
{
    MPI_Send(buff, count, datatype, target_rank,
        tag, gdsi_comm_world);
    gdsi_target_signal_thread(target_rank);
}

void gdsi_isend_target_request(const void *buff, int count,
    MPI_Datatype datatype, int target_rank, int tag, MPI_Request *reqp)
{
    MPI_Isend(buff, count, datatype, target_rank,
        tag, gdsi_comm_world, reqp);
}

/*
  Get the target rank range

  @param rank specify the client rank. \
  If negative, this function will automatically get it from comm.

  The range is represented by [begin, end)
*/
static void get_target_rank_range(MPI_Comm comm, int rank, int *begin, int *end)
{
    int n_targets, n_clients;
    int cl_rank = rank;
    int n_per_client;
    int e;

    /* Know the total number of taget nodes */
    MPI_Comm_size(gdsi_comm_world /* TODO: is this correct? */,
                  &n_targets);

    MPI_Comm_size(comm, &n_clients);
    if (cl_rank < 0)
        MPI_Comm_rank(comm, &cl_rank);

    n_per_client = (n_targets+1)/n_clients;

    *begin = n_per_client * cl_rank;
    e = n_per_client * (cl_rank+1);
    *end = e > n_targets ? n_targets : e;

    /* TODO: this is an ad-hoc adjustment */
    if (cl_rank + 1 == n_clients && *end < n_targets)
        *end = n_targets;
}

/*
  Get non-owner rank range

  The range is represented by [begin, end)
*/
static void get_nonowner_rank_range(struct gdsi_gds_common *common,
    int cl_rank, int *begin, int *end)
{
    int n_targets, n_clients;
    int n_per_client;
    int e;

    /* Know the total number of taget nodes */
    MPI_Group_size(common->non_owner_group, &n_targets);
    if (n_targets == 0) {
        *begin = *end = -1;
        return;
    }

    MPI_Comm_size(common->comm, &n_clients);
    if (cl_rank < 0)
        MPI_Comm_rank(common->comm, &cl_rank);

    n_per_client = (n_targets+1)/n_clients;

    *begin = n_per_client * cl_rank;
    e = n_per_client * (cl_rank+1);
    *end = e > n_targets ? n_targets : e;

    /* TODO: this is an ad-hoc adjustment */
    if (cl_rank + 1 == n_clients && *end < n_targets)
        *end = n_targets;
}

/*
  Maps ranks in gds owners into comm_world
  If rank is negative, this function uses the caller's rank as an input
 */
int gdsi_map_owner_into_world(struct gdsi_gds_common *common, int rank)
{
    int r;

    if (rank < 0)
        MPI_Group_rank(common->owner_group, &rank);

    MPI_Group_translate_ranks(common->owner_group, 1, &rank, gdsi_group_world, &r);

    gdsi_dprintf(GDS_DEBUG_INFO, "owner mapping: %d -> %d\n", rank, r);

    return r;
}

static int map_nonowner_into_world(struct gdsi_gds_common *common, int rank)
{
    int r;

    MPI_Group_translate_ranks(common->non_owner_group, 1, &rank, gdsi_group_world, &r);

    gdsi_dprintf(GDS_DEBUG_INFO, "non-owner mapping: %d -> %d\n", rank, r);

    return r;
}

/* Returns a new owner rank in the new owner communicator */
static int find_new_owner(struct gdsi_gds_common *common,
                          MPI_Group old_owner_group, int lost_rank)
{
    int dist, size;

    MPI_Group_size(old_owner_group, &size);
    for (dist = 1; dist < size; dist++) {
        int t, r;
        t = (lost_rank + dist) % size;
        gdsi_dprintf(GDS_DEBUG_INFO, "Checking t=%d\n", t);
        MPI_Group_translate_ranks(old_owner_group, 1, &t,
            common->owner_group, &r);
        if (r != MPI_UNDEFINED)
            return r;
        gdsi_dprintf(GDS_DEBUG_INFO, "t=%d was undefined\n", t);
    }

    gdsi_panic("New owner not found!\n");
}

/* Retrieve an RMA window from target array descriptor */
static void set_rma_window(struct gdsi_gds_common *common)
{
    int trank;
    struct gdsi_target_array_desc *desc;

    trank = gdsi_map_owner_into_world(common, -1);
    desc = gdsi_find_array_desc(gdsi_get_array_id(common, trank));

    if (desc == NULL)
        gdsi_panic("array id %d not found -- something wrong!!",
            gdsi_get_array_id(common, trank));

    common->win = desc->win;
#ifdef GDS_CONFIG_USE_LRDS
    common->lrds_dirty_win = desc->lrds_dirty_win;
#endif
}

static size_t calc_total_count(const struct gdsi_gds_common *common,
    size_t size_flat)
{
    size_t counts[common->ndims];
    gdsi_decompose_size(size_flat, common, counts);
    return linearize_count(common->ndims, counts);
}

/* Update start index on the newly assigned chunk, and link it from
   a chunk in the same owner process */
static void repair_lost_chunk(struct gdsi_gds_common *common,
    struct gdsi_chunk_desc cds[], int chunkno, int new_owner)
{
    int cn, prev_cn = -1;
    size_t prev_count;

    for (cn = new_owner; cn >= 0; cn = cds[cn].next_sibling_chunk)
        prev_cn = cn;

    gdsi_assert(prev_cn >= 0);

    prev_count = calc_total_count(common, cds[prev_cn].size_flat);
    /* Make a link from the previous chunk */
    cds[prev_cn].next_sibling_chunk = chunkno;
    cds[chunkno].next_sibling_chunk = -1;
    cds[chunkno].target_index = cds[prev_cn].target_index + prev_count;
    cds[chunkno].is_corrupted = true;

    gdsi_dprintf(GDS_DEBUG_INFO,
        "cds[%d].target_index=%zu\n",
        chunkno, cds[chunkno].target_index);
}

#ifdef GDS_CONFIG_USE_SCR
bool SCR_SUPPORT;
#endif
/* Returns number of newly owned chunks by calling process */
static int repair_current_metadata(struct gdsi_gds_common *common,
    MPI_Group old_world_group, MPI_Group old_owner_group, MPI_Group new_world_group)
{
    int i;
    struct gdsi_chunk_desc *cd = common->chunk_sets->chunk_descs;
    MPI_Group group_world = new_world_group;
    int my_rank, my_target;
    int n_new_chunks = 0;
    GDS_status_t ret;

    gdsi_assert(group_world != MPI_GROUP_NULL);

    MPI_Comm_rank(common->comm, &my_rank);
    MPI_Group_translate_ranks(common->owner_group, 1, &my_rank,
        group_world, &my_target);

    for (i = 0; i < common->nchunk_descs; i++) {
        int r, orig_owner, new_owner, new_target;
        MPI_Group_translate_ranks(old_world_group, 1, &cd[i].target_rank,
            group_world, &r);
        if (r == MPI_UNDEFINED) {
            gdsi_dprintf(GDS_DEBUG_INFO, "chunk %d: target %d is dead\n",
                i, cd[i].target_rank);

            /* Find a new owner to take care of lost rank */
            MPI_Group_translate_ranks(old_world_group, 1, &cd[i].target_rank,
                old_owner_group, &orig_owner);
            gdsi_assert(orig_owner != MPI_UNDEFINED);
            new_owner = find_new_owner(common, old_owner_group, orig_owner);

            /* Translate new owner rank into  */
            MPI_Group_translate_ranks(common->owner_group, 1, &new_owner,
                group_world, &new_target);
            gdsi_assert(new_target != MPI_UNDEFINED);

            repair_lost_chunk(common, common->chunk_sets->chunk_descs,
                i, new_owner);

            if (new_target == my_target) {
                gdsi_dprintf(GDS_DEBUG_INFO,
                    "I'm taking care of chunk %d, was held in target %d\n",
                    i, cd[i].target_rank);
                common->owning_chunks[common->n_owning_chunks++] = i;
                n_new_chunks++;
                if (common->n_owning_chunks == common->n_owning_chunks_max) {
                    common->n_owning_chunks_max *= 2;
                    common->owning_chunks = realloc(common->owning_chunks,
                        sizeof(int)
                        * common->n_owning_chunks_max);
                    gdsi_assert(common->owning_chunks);
                }
            }
        } else {
            new_target = r;
        }

        gdsi_assert(new_target >= 0);
        cd[i].target_rank = new_target;
    }

    /* Reconstruct target memory buffer and window */

    MPI_Win_unlock_all(common->win);

    struct gdsi_target_reconstruct rc = {
        .array_id = gdsi_get_array_id(common, my_target),
    };
    size_t start_index = cd[common->owning_chunks[0]].target_index;

    for (i = 0; i < common->n_owning_chunks; i++) {
        int cn = common->owning_chunks[i];
        size_t lcount;
        lcount = calc_total_count(common, cd[cn].size_flat);
        if (i >= common->n_owning_chunks - n_new_chunks) {
            /* Update start index for newly owned chunks */
            rc.additional_count += lcount;
            cd[cn].target_index = start_index;
            gdsi_dprintf(GDS_DEBUG_INFO, "cd[%d].target_index=%zu\n",
                cn, start_index);
        }
        start_index += lcount;
    }

    /* Send the request to the target in the same process */
    gdsi_send_target_request(&rc, sizeof(rc), MPI_BYTE, my_target,
        GDS_TGTREQ_RECONSTRUCT);

    int nobegin, noend; /* Range of non-owners */
    get_nonowner_rank_range(common, -1, &nobegin, &noend);
    for (i = nobegin; i < noend; i++) {
        int tr = map_nonowner_into_world(common, i);
        rc.array_id = gdsi_get_array_id(common, tr);
        gdsi_send_target_request(&rc, sizeof(rc), MPI_BYTE, tr,
            GDS_TGTREQ_RECONSTRUCT);
    }

    /* Wait for servers to complete */
    MPI_Recv(&ret, 1, MPI_INT, my_target, GDS_TGTREP_GENERIC,
        gdsi_comm_world, MPI_STATUS_IGNORE);
    for (i = nobegin; i < noend; i++) {
        int tr = map_nonowner_into_world(common, i);
        MPI_Recv(&ret, 1, MPI_INT, tr, GDS_TGTREP_GENERIC,
            gdsi_comm_world, MPI_STATUS_IGNORE);
    }

    set_rma_window(common);

    MPI_Win_lock_all(0, common->win);

#ifdef GDS_CONFIG_USE_SCR 
    SCR_SUPPORT = false;
#endif

    return n_new_chunks;
}

static bool need_old_md_repairing(const struct gdsi_chunk_set *target_ver)
{
    return target_ver->latest_crash_count < gdsi_crash_count;
}

void gdsi_repair_old_metadata(struct gdsi_gds_common *common,
    struct gdsi_chunk_set *target_ver)
{
    if (!need_old_md_repairing(target_ver))
        return;

    int i, clrank, mytrank;
    MPI_Comm_rank(common->comm, &clrank);
    mytrank = gdsi_map_owner_into_world(common, clrank);

    for (i = 0; i < common->nchunk_descs; i++) {
        int cur_trank;
        int unique_array_id = gdsi_get_array_id(common, 0);
        MPI_Group_translate_ranks(target_ver->world_group,
            1, &target_ver->chunk_descs[i].target_rank,
            gdsi_group_world, &cur_trank);
        if (cur_trank != MPI_UNDEFINED) {
            gdsi_dprintf(GDS_DEBUG_INFO,
                "PCA target found: ver=%zu chunkno=%d\n",
                target_ver->version, i);

            gdsi_dprintf(GDS_DEBUG_INFO, "version %zu chunk %d pca %d, change to rank %d\n", target_ver->version,
                target_ver->chunk_descs[i].chunk_number,
                target_ver->chunk_descs[i].target_rank, cur_trank);
            target_ver->chunk_descs[i].target_rank = cur_trank;
            continue;
        }
        if (gdsi_check_pfs_available(unique_array_id,
                target_ver->version,
                target_ver->chunk_descs[i].chunk_number)) {
            gdsi_dprintf(GDS_DEBUG_INFO,
                "SCR PFS file found: ver=%zu chunkno=%d\n",
                target_ver->version, i);
            target_ver->chunk_descs[i].target_rank = mytrank;
            target_ver->chunk_descs[i].ckpt_location_flags = GDS_CKPT_ON_DISK;
            continue;
        }

        target_ver->chunk_descs[i].is_corrupted = true;
        gdsi_dprintf(GDS_DEBUG_INFO, "version %zu chunk %d pca %d be marked as corrupted\n",
            target_ver->version,
            target_ver->chunk_descs[i].chunk_number,
            target_ver->chunk_descs[i].target_rank);
    }

    target_ver->latest_crash_count = gdsi_crash_count;
}

int gdsi_replace_comm(struct gdsi_gds_common *common, MPI_Comm new_comm,
    MPI_Group new_group_world)
{
    MPI_Group old_owner_group = common->owner_group;
    MPI_Group old_world_group = common->chunk_sets->world_group;
    int my_new_rank, my_old_rank, old_size, new_size, n_new_chunks;

    MPI_Comm_rank(common->comm, &my_old_rank);
    MPI_Comm_size(common->comm, &old_size);
    MPI_Comm_size(new_comm, &new_size);

    gdsi_dprintf(GDS_DEBUG_INFO,
        "old_size=%d new_size=%d\n", old_size, new_size);

    common->comm = new_comm;

    MPI_Comm_rank(new_comm, &my_new_rank);

    MPI_Group_free(&common->non_owner_group);

    MPI_Comm_group(common->comm, &common->owner_group);
    MPI_Group_difference(new_group_world,
        common->owner_group, &common->non_owner_group);
    common->chunk_sets->world_group = new_group_world;

    n_new_chunks = repair_current_metadata(common, old_world_group, old_owner_group,
        new_group_world);

    MPI_Group_free(&old_owner_group);
    MPI_Group_free(&old_world_group);

    return n_new_chunks;
}

static inline size_t decompose_index_lastdim(size_t flat_index, size_t lastdim)
{
    return flat_index % lastdim;
}

static inline size_t decompose_size_lastdim(size_t flat_index, size_t lastdim)
{
    return flat_index % (lastdim + 1);
}

void gdsi_decompose_index_impl(size_t flat_index, size_t ndims, const size_t dims[], GDS_size_t indices[])
{
    GDS_size_t t = flat_index;
    GDS_size_t i;

    for (i = 0; i < ndims-1; i++) {
        size_t k = ndims-1-i;
        indices[k] = t % dims[k];
        t = (t - indices[k])/dims[k];
        //t = (t - indices[k])/dims[k] + 1;
    }
    indices[0] = t;
}

void gdsi_decompose_index(GDS_size_t flat_index, const struct gdsi_gds_common *common, GDS_size_t indices[])
{
    gdsi_decompose_index_impl(flat_index, common->ndims, common->nelements, indices);
}

void gdsi_decompose_size(size_t flat_size, const struct gdsi_gds_common *common, GDS_size_t sizes[])
{
    size_t ndims = common->ndims;
    size_t dims[ndims], i;

    for (i = 0; i < ndims; i++)
        dims[i] = common->nelements[i] + 1;

    gdsi_decompose_index_impl(flat_size, ndims, dims, sizes);
    if (common->flavor == GDS_FLAVOR_CREATE)
        sizes[0]++;
}

size_t gdsi_linearize_index_impl(size_t ndims, const size_t dims[], const GDS_size_t indices[])
{
    size_t i, l = 0, stride = 1;

    for (i = 0; i < ndims; i++) {
        size_t k = ndims - 1 - i;
        l += indices[k] * stride;
        stride *= dims[k];
    }

    return l;
}

size_t gdsi_linearize_index(const struct gdsi_gds_common *common, const GDS_size_t indices[])
{
    return gdsi_linearize_index_impl(common->ndims, common->nelements, indices);
}

static size_t linearize_size(const struct gdsi_gds_common *common, const GDS_size_t indices[])
{
    size_t ndims = common->ndims;
    size_t dims[ndims], i;

    for (i = 0; i < ndims; i++)
        dims[i] = common->nelements[i] + 1;

    return gdsi_linearize_index_impl(ndims, dims, indices);
}

static int receive_alloc_result(struct gdsi_chunk_desc *cd_buff,
    int *array_ids,
    gdsi_log_md_t *mds, const int *md_disps, const int *n_mds,
    gdsi_log_md_t *cur_md_heads, int chunkno, int trank,
    GDS_flavor_t flavor)
{
    int ret = 0;
    struct gdsi_target_alloc_result rep;

    /* TODO: define MPI datatype */
    gdsi_dprintf(GDS_DEBUG_INFO, "receive_alloc_result: begin to receive\n");
    GDSI_MPI_CALL(MPI_Recv(&rep, sizeof(rep), MPI_BYTE, trank,
        GDS_TGTREP_CREATE, gdsi_comm_world, MPI_STATUS_IGNORE));
    gdsi_dprintf(GDS_DEBUG_INFO, "receive_alloc_result: received\n");
    if (rep.status != GDS_STATUS_OK) {
        /* TODO: graceful recovery */
        ret = -1;
        gdsi_dprintf(GDS_DEBUG_ERR,
            "Target %d returned %d\n", trank, rep.status);
        goto out;
    }

    if (flavor == GDS_FLAVOR_ALLOC)
        cd_buff[chunkno].target_index = rep.start_index;

    if (array_ids != NULL) {
        array_ids[trank] = rep.array_id;
        gdsi_dprintf(GDS_DEBUG_INFO, "array_ids[%d]=%d\n",
            trank, array_ids[trank]);
    }

    if (cur_md_heads != NULL)
        GDSI_MPI_CALL(
            MPI_Recv(cur_md_heads + chunkno, 1, MPI_INT,
                trank, GDS_TGTREP_CREATE,
                gdsi_comm_world, MPI_STATUS_IGNORE)
            );

    if (mds != NULL)
        GDSI_MPI_CALL(
            MPI_Recv(mds + md_disps[chunkno], n_mds[chunkno], MPI_INT,
                trank, GDS_TGTREP_CREATE,
                gdsi_comm_world, MPI_STATUS_IGNORE)
            );

out:
    return ret;
}

#ifdef GDS_CONFIG_USE_SCR
static int receive_flush_scr_result(int trank)
{
    int ret = 0, rep = 1;

    GDSI_MPI_CALL(MPI_Recv(&rep, sizeof(int), MPI_BYTE, trank,
    GDS_TGTREP_FLUSH_SCR, gdsi_comm_world, MPI_STATUS_IGNORE));
    if (rep) {
        ret = -1;
        gdsi_dprintf(GDS_DEBUG_ERR,
            "FLUSH_SCR_RESULT: Target %d returned %d\n", trank, rep);
    }

    return ret;
}
#endif

static struct gdsi_chunk_set *
alloc_chunk_set(size_t n_chunks, GDS_size_t version)
{
    struct gdsi_chunk_set *cset;
    size_t size = sizeof(struct gdsi_chunk_set)
        + sizeof(struct gdsi_chunk_desc) * n_chunks;

    cset = malloc(size);
    if (cset != NULL) {
        memset(cset, 0, size);
        cset->version = version;
        cset->world_group = MPI_GROUP_NULL;
    }

    return cset;
}

int gdsi_get_array_id(const struct gdsi_gds_common *common, int target_rank)
{
    return common->array_ids[target_rank];
}

GDS_status_t gdsi_clone_from_gds_common(struct gdsi_gds_common *common, GDS_gds_t *new_gds)
{
    GDS_gds_t new;

    new = malloc(sizeof(struct GDS_gds));
    if (new == NULL)
        return GDS_STATUS_NOMEM;

    gdsi_common_lock(common);
    common->refcount++;
    gdsi_common_unlock(common);

    new->common = common;
    new->chunk_set = common->chunk_sets;

    *new_gds = new;

    return GDS_STATUS_OK;
}

static enum gdsi_data_order parse_order(MPI_Info info)
{
    int flag;
    char val[GDS_ORDER_VAL_MAX+1];

    if (info == MPI_INFO_NULL)
        return gdsi_order_default;

    GDSI_MPI_CALL(MPI_Info_get(info, GDS_ORDER_KEY, sizeof(val), val, &flag));
    if (!flag)
        return gdsi_order_default;

    if (strcmp(val, GDS_ORDER_ROW_MAJOR) == 0)
        return GDSI_ORDER_ROW_MAJOR;
    else if (strcmp(val, GDS_ORDER_COL_MAJOR) == 0)
        return GDSI_ORDER_COL_MAJOR;

    return gdsi_order_default;
}

static enum gdsi_data_repr parse_repr(MPI_Info info)
{
    int flag;
    char val[GDS_REPR_VAL_MAX+1];

    if (info == MPI_INFO_NULL)
        return gdsi_repr_default;

    GDSI_MPI_CALL(MPI_Info_get(info, GDS_REPR_KEY, sizeof(val), val, &flag));
    if (!flag)
        return gdsi_repr_default;

    if (strcmp(val, GDS_REPR_FLAT) == 0)
        return GDSI_REPR_FLAT;
    else if (strcmp(val, GDS_REPR_LOG) == 0)
        return GDSI_REPR_LOG;
#ifdef GDS_CONFIG_USE_LRDS
    else if (strcmp(val, GDS_REPR_LRDS) == 0)
        return GDSI_REPR_LRDS;
#endif

    return GDSI_REPR_INVALID;
}

static int parse_int_info(MPI_Info info, const char *key, int def)
{
    int flag;
    char str[2 * sizeof(int) + 1];
    int val;

    if (info == MPI_INFO_NULL)
        return def;

    GDSI_MPI_CALL(MPI_Info_get(info, key, sizeof(str), str, &flag));
    if (!flag)
        return def;

    sscanf(str, "%x", &val);

    return val;
}

const char *get_impl_str(int i)
{
    switch (i) {
    case GDS_GET_IMPL_RMA:
        return "rma";
    case GDS_GET_IMPL_MSG:
        return "msg";
    default:
        return "???";
    }
}

const char *put_impl_str(int i)
{
    switch (i) {
    case GDS_PUT_IMPL_RMA:
        return "rma";
    case GDS_PUT_IMPL_MSG:
        return "msg";
    case GDS_PUT_IMPL_HYB:
        return "hyb";
    default:
        return "???";
    }
}

#ifdef GDS_CONFIG_COUNTER
static char *array_type_string(const struct gdsi_gds_common *common, char *buff, size_t n)
{
    int r = 0;
    char *p = buff;

    switch (common->representation) {
    case GDSI_REPR_FLAT:
        r = snprintf(p, n, "flat-");
        break;

    case GDSI_REPR_LOG:
        r = snprintf(p, n, "log-bs%zu-", common->log_user_bs);
        break;

    case GDSI_REPR_LRDS:
        r = snprintf(p, n, "lrds-%d-",
            common->lrds_prop_dirty_tracking);
        break;

    default:
        snprintf(p, n, "???");
        return buff;
    }

    if (r >= n)
        return buff;
    p += r;

    r = snprintf(p, n - (p - buff), "g%s-", get_impl_str(common->get_impl));
    if (r >= n - (p - buff))
            return buff;
    p += r;
    r = snprintf(p, n - (p - buff), "p%s", put_impl_str(common->put_impl));

    return buff;
}
#endif /* GDS_CONFIG_COUNTER */

static size_t parse_log_blk_elms(MPI_Info info)
{
    return parse_int_info(info, GDS_LOG_BS_KEY, gdsi_log_bs_default);
}

static void free_common_bandled(struct gdsi_gds_common *common)
{
    free(common->log_size_limits);
    free(common->log_db_stocks);
    free(common->log_tail_max);
    free(common->log_tail_cache);
    free(common->log_cur_md_heads);
    free(common->log_md_displs);
    free(common->log_md_recvcounts);
    free(common->log_md_cache);
    free(common->log_flush_queues);
    free(common->owning_chunks);
    free(common->array_ids);
    free(common->nelements);
}

static struct gdsi_gds_common *alloc_common(size_t ndims,
    const size_t count[], int tsize, enum gdsi_data_order order,
    enum gdsi_data_repr repr, size_t log_user_blk_elms, int typelen)
{
    struct gdsi_gds_common *common;
    size_t i;

    common = calloc(sizeof(*common), 1);
    if (common == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "calloc() failed for common\n");
        return NULL;
    }

    common->comm = MPI_COMM_NULL;
    common->simulated_failed_proc = -1;

#ifdef GDS_CONFIG_USE_LRDS
    common->lrds_dirty_win = MPI_WIN_NULL;
#endif

    common->nelements = malloc(sizeof(size_t) * ndims);
    if (common->nelements == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to allocate common->nelements\n");
        goto out_fail;
    }

    common->array_ids = malloc(sizeof(int) * tsize);
    if (common->array_ids == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to allocate common->array_ids\n");
        goto out_fail;
    }

    common->n_owning_chunks_max = GDSI_CUR_CHUNK_PER_PROC_MAX_DEFAULT;
    common->owning_chunks = malloc(sizeof(int) * common->n_owning_chunks_max);
    if (common->owning_chunks == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to allocate common->owning_chunks\n");
        goto out_fail;
    }

    common->ndims = ndims;
    if (order == GDSI_ORDER_ROW_MAJOR) {
        for (i = 0; i < ndims; i++)
            common->nelements[i] = count[i];
    } else {
        for (i = 0; i < ndims; i++)
            common->nelements[i] = count[ndims-1-i];
    }

    common->order = order;
    common->representation = repr;

    if (repr == GDSI_REPR_LOG) {
        size_t c = linearize_count(ndims, count) * typelen;
        size_t log_user_bs = log_user_blk_elms * typelen;
        size_t nblocks = c / log_user_bs + tsize /* max. padding */;

        common->log_user_bs = log_user_bs;

        common->log_md_cache = malloc(sizeof(gdsi_log_md_t) * nblocks);
        if (common->log_md_cache == NULL)
            goto out_fail;

        common->log_md_recvcounts = malloc(sizeof(int) * tsize);
        if (common->log_md_recvcounts == NULL)
            goto out_fail;

        common->log_md_displs = malloc(sizeof(int) * tsize);
        if (common->log_md_displs == NULL)
            goto out_fail;

        common->log_cur_md_heads = malloc(sizeof(gdsi_log_md_t) * tsize);
        if (common->log_cur_md_heads == NULL)
            goto out_fail;

        common->log_tail_cache = malloc(sizeof(size_t) * tsize);
        if (common->log_tail_cache == NULL)
            goto out_fail;

        common->log_tail_max = calloc(sizeof(gdsi_log_md_t), tsize);
        if (common->log_tail_max == NULL)
            goto out_fail;

        common->log_db_stocks = calloc(sizeof(struct gdsi_log_stock_queue *), tsize);
        if (common->log_db_stocks == NULL)
            goto out_fail;

        common->log_size_limits = malloc(sizeof(size_t) * tsize);
        if (common->log_size_limits == NULL)
            goto out_fail;

        common->log_flush_queues
            = calloc(sizeof(struct gdsi_log_flush_queue), tsize);
        if (common->log_flush_queues == NULL)
            goto out_fail;
    }

    return common;

out_fail:
    free_common_bandled(common);
    free(common);

    return NULL;
}

static void free_common(struct gdsi_gds_common *common)
{
    if (common == NULL)
        return;

    int i;

    while (common->chunk_sets != NULL) {
        struct gdsi_chunk_set *set;
        set = common->chunk_sets;
        SGLIB_DL_LIST_DELETE(struct gdsi_chunk_set, common->chunk_sets,
                             set, newer, older);
        free(set->label);
        MPI_Group_free(&set->world_group);
        free(set);
    }

    while (common->global_error_desc_queue != NULL) {
        GDS_error_t error_desc_node;
        error_desc_node = common->global_error_desc_queue;
        SGLIB_LIST_DELETE(struct GDS_error,
            common->global_error_desc_queue, error_desc_node, next);
        free(error_desc_node);
    }

    while (common->global_error_handler_queue != NULL) {
        struct gdsi_error_handler *error_handler_node;
        error_handler_node = common->global_error_handler_queue;
        SGLIB_LIST_DELETE(struct gdsi_error_handler,
            common->global_error_handler_queue, error_handler_node, next);
        free(error_handler_node);
    }

    while (common->local_error_desc_queue != NULL) {
        GDS_error_t error_desc_node;
        error_desc_node = common->local_error_desc_queue;
        SGLIB_LIST_DELETE(struct GDS_error,
            common->local_error_desc_queue, error_desc_node, next);
        free(error_desc_node);
    }

    while (common->local_error_handler_queue != NULL) {
        struct gdsi_error_handler *error_handler_node;
        error_handler_node = common->local_error_handler_queue;
        SGLIB_LIST_DELETE(struct gdsi_error_handler,
            common->local_error_handler_queue, error_handler_node, next);
        free(error_handler_node);
    }

    for (i = 0; i < GDS_PRIORITY_MAX; i++) {
         while (common->global_check_funs[i] != NULL) {
            struct gdsi_check_fun *chk;
            chk = common->global_check_funs[i];
            SGLIB_LIST_DELETE(struct gdsi_check_fun, common->global_check_funs[i],
                chk, next);
            free(chk);
         }
    }

    for (i = 0; i < GDS_PRIORITY_MAX; i++) {
         while (common->local_check_funs[i] != NULL) {
            struct gdsi_check_fun *chk;
            chk = common->local_check_funs[i];
            SGLIB_LIST_DELETE(struct gdsi_check_fun, common->local_check_funs[i],
                chk, next);
            free(chk);
         }
    }

    if (common->log_db_stocks) {
        int tsize;
        /* TODO: is this correct? */
        MPI_Comm_size(gdsi_comm_world, &tsize);
        for (i = 0; i < tsize; i++)
            free(common->log_db_stocks[i]);
    }

    MPI_Group_free(&common->owner_group);
    MPI_Group_free(&common->non_owner_group);

    if (common->comm != MPI_COMM_NULL)
        MPI_Comm_free(&common->comm);

    free(common->nelements_local);
    free(common->offset_local);
    free_common_bandled(common);
    free(common);
}

static size_t linearize_count(size_t ndim, const size_t count[])
{
    size_t i, l = 1;
    for (i = 0; i < ndim; i++)
        l *= count[i];
    return l;
}

static void reverse_order(size_t a[], size_t n)
{
    size_t i;
    for (i = 0; i < n/2; i++) {
        size_t ri = n - i - 1;
        size_t l = a[i];
        size_t r = a[ri];

        a[i] = r;
        a[ri] = l;
    }
}

uint64_t estimate_log_tail(gdsi_log_md_t md_head, int n_mds, size_t bs)
{
    size_t padding;
    size_t md_true_size = sizeof(gdsi_log_md_t) * n_mds;

    padding = bs - md_true_size % bs;

    return log_md_to_byteoff(md_head, bs) + md_true_size + padding;
}

static void init_nonowner_assignments(struct gdsi_gds_common *common,
    int *displs, int *cnts)
{
    int i, clsize;
    MPI_Comm_size(common->comm, &clsize);

    for (i = 0; i < clsize; i++) {
        int tb, te;
        get_nonowner_rank_range(common, i, &tb, &te);
        displs[i] = tb;
        cnts[i] = te - tb;
        gdsi_dprintf(GDS_DEBUG_INFO, "%d: displ=%d, cnt=%d\n",
            i, tb, te - tb);
    }
}

/* gdsi_alloc and gdsi_create shard routines */
static struct gdsi_gds_common *ac_init_common(size_t ndims, GDS_datatype_t datatype,
    const size_t md_counts[], int tsize, int clsize, int clrank, int typelen,
    enum gdsi_data_repr repr, MPI_Comm comm, MPI_Info info, int *error_line)
{
    struct gdsi_gds_common *common;

    common = alloc_common(ndims, md_counts, tsize, parse_order(info),
        repr, parse_log_blk_elms(info), typelen);
    if (common == NULL) {
        *error_line = __LINE__;
        return NULL;
    }

    gdsi_assert(common->n_owning_chunks_max >= 1);
    common->owning_chunks[0] = clrank;
    common->n_owning_chunks = 1;

    if (common->representation == GDSI_REPR_LOG && tsize != clsize)
        gdsi_panic("Sub-communicator is not supported for log-structured "
            "array.\n");

    pthread_mutex_init(&common->lock, NULL);

    common->comm = comm;
    common->refcount = 0;
    MPI_Comm_group(common->comm, &common->owner_group);
    MPI_Group_difference(gdsi_group_world, common->owner_group,
        &common->non_owner_group);
    common->type = datatype;
    common->ndims = ndims;
    common->latest_version = 0;
    common->global_error_desc_counter = 0;
    common->get_impl = parse_int_info(info, GDS_GET_IMPL_KEY, 0);
    if (common->get_impl >= GDS_GET_IMPL_MAX) {
        *error_line = __LINE__;
        return NULL;
    }
    common->put_impl = parse_int_info(info, GDS_PUT_IMPL_KEY, 0);
    if (common->put_impl >= GDS_PUT_IMPL_MAX) {
        *error_line = __LINE__;
        return NULL;
    }
    common->lrds_prop_dirty_tracking = parse_int_info(info,
        GDS_LRDS_PROP_DIRTY_TRACKING_KEY, 0);
    common->lrds_prop_dirty_block_size = parse_int_info(info,
        GDS_LRDS_PROP_DIRTY_BLOCK_SIZE_KEY,
        GDSI_LRDS_DIRTY_BLOCK_SIZE_DEFAULT);
#ifdef GDS_CONFIG_COUNTER
    {
        char typestr[32];
        gdsi_counter_init(&common->counter,
            array_type_string(common, typestr, sizeof(typestr)));
    }
#endif

    return common;
}

static int ac_set_chunk_set(struct gdsi_gds_common *common, int size, int *error_line)
{
    struct gdsi_chunk_set *cset;

    common->nchunk_descs = size;
    cset = alloc_chunk_set(size, 0);
    if (cset == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to allocate chunk set\n");
        *error_line = __LINE__;
        return GDS_STATUS_NOMEM;
    }

    MPI_Comm_group(gdsi_comm_world, &cset->world_group);

    SGLIB_DL_LIST_ADD(struct gdsi_chunk_set, common->chunk_sets,
                      cset, newer, older);
    gdsi_assert(common->chunk_sets == cset);

    return GDS_STATUS_OK;
}

static void ac_set_target_cp(struct gdsi_gds_common *common, int typelen,
    struct gdsi_target_create *cp, GDS_flavor_t flavor,
    void *local_buffer, size_t local_buffer_count, MPI_Info info)
{
    cp->representation = common->representation;
    cp->disp_unit = typelen;
    cp->flavor = flavor;
    cp->local_buffer = local_buffer;
    cp->size = local_buffer_count;
    cp->log_user_bs = common->log_user_bs;
    cp->log_prealloc_vers = parse_int_info(info, GDS_LOG_PREALLOC_VERS_KEY,
        GDSI_LOG_PREALLOC_DEFAULT);
    cp->lrds_prop_dirty_tracking = common->lrds_prop_dirty_tracking;
    cp->lrds_prop_dirty_block_size = common->lrds_prop_dirty_block_size;
    cp->lrds_prop_versioning = parse_int_info(info,
        GDS_LRDS_PROP_VERSIONING_KEY, 0);
    cp->lrds_expose_dirty = (common->put_impl == GDS_PUT_IMPL_RMA
        || common->put_impl == GDS_PUT_IMPL_HYB);

}

GDS_status_t gdsi_alloc(size_t ndims, const size_t md_counts[],
    const size_t min_chunks[], GDS_datatype_t datatype,
    GDS_comm_t comm_user, MPI_Info info,
    struct gdsi_gds_common **common_retp)
{
    MPI_Comm comm;
    struct gdsi_gds_common *common = NULL;
    struct gdsi_target_create cp;
    int tsize, trank = -1, clsize, clrank, i;
    int typelen;
    int error_line = 0;
    enum gdsi_data_repr repr;
    int otbegin, otend;
    size_t min_chunks_r[ndims];
    gdsi_minchunk_t *mc = NULL;

    /* temporary array IDs */
    int *array_ids_tmp = NULL;

    /* Temporary buffers for non-owner assignments */
    int *no_displs = NULL, *no_cnts = NULL;

    MPI_Comm_dup(comm_user, &comm);
    MPI_Comm_size(gdsi_comm_world, &tsize);
    MPI_Comm_size(comm, &clsize);
    MPI_Comm_rank(comm, &clrank);

    gdsi_dprintf(GDS_DEBUG_INFO, "gdsi_alloc: %d/%d/%d\n",
        clrank, clsize, tsize);

    array_ids_tmp = malloc(sizeof(int) * tsize);
    no_displs = malloc(sizeof(int) * clsize);
    no_cnts = malloc(sizeof(int) * clsize);
    if (array_ids_tmp == NULL || no_displs == NULL || no_cnts == NULL) {
        free(array_ids_tmp);
        free(no_displs);
        free(no_cnts);
        return GDS_STATUS_NOMEM;
    }

    GDSI_MPI_CALL(MPI_Type_size(datatype, &typelen));

    repr = parse_repr(info);
    if (repr == GDSI_REPR_INVALID) {
        gdsi_dprintf(GDS_DEBUG_ERR, "invalid array represenation\n");
        error_line = __LINE__;
        goto reduce;
    }

    common = ac_init_common(ndims, datatype, md_counts, tsize, clsize, clrank,
                            typelen, repr, comm, info, &error_line);
    if (!common) { goto reduce; }

    trank = gdsi_map_owner_into_world(common, clrank);

    common->flavor = GDS_FLAVOR_ALLOC;

    init_nonowner_assignments(common, no_displs, no_cnts);

    ac_set_target_cp(common, typelen, &cp, GDS_FLAVOR_ALLOC, NULL, 0, info);

    if (ac_set_chunk_set(common, clsize, &error_line)) { goto reduce; }

    /* Reverse min_chunk order if column major */
    if (min_chunks != NULL) {
        memcpy(min_chunks_r, min_chunks, sizeof(min_chunks_r));
        if (common->order == GDSI_ORDER_COL_MAJOR)
            reverse_order(min_chunks_r, ndims);
    } else {
        memset(min_chunks_r, 0, sizeof(min_chunks_r));
    }

    mc = gdsi_minchunk_split(clsize,
        common->ndims, common->nelements, min_chunks_r);
    if (mc == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to split domain\n");
        error_line = __LINE__;
        goto reduce;
    }

    get_nonowner_rank_range(common, -1, &otbegin, &otend);
    gdsi_dprintf(GDS_DEBUG_INFO, "Non-owner target range: %d-%d\n",
        otbegin, otend);

reduce:
    GDSI_MPI_CALL(MPI_Allreduce(MPI_IN_PLACE, &error_line, 1, MPI_INT,
                                 MPI_MAX, comm));
    if (error_line != 0) {
        gdsi_minchunk_free(mc);
        free(array_ids_tmp);
        free(no_displs);
        free(no_cnts);
        if (common == NULL)
            MPI_Comm_free(&comm);
        else
            free_common(common);
        return GDS_STATUS_NOMEM;
    }

    {
        size_t chunk_idx[common->ndims];
        size_t chunk_size[common->ndims];

        gdsi_minchunk_get(mc, clrank, chunk_idx, chunk_size);

        // cache chunk_size and offset for error handling
        common->nelements_local = malloc(common->ndims *
            sizeof(*common->nelements_local));
        common->offset_local = malloc(common->ndims *
            sizeof(*common->offset_local));

        if (common->nelements_local == NULL || common->offset_local == NULL) {
            free(common->nelements_local);
            free(common->offset_local);
            return GDS_STATUS_NOMEM;
        }
        int dim;
        common->nelements_local_flat=1;
        for (dim = 0; dim < common->ndims; ++dim) {
            common->nelements_local[dim] = chunk_size[dim];
            common->nelements_local_flat *= chunk_size[dim];
            common->offset_local[dim] = chunk_idx[dim];
        }

        cp.size = linearize_count(common->ndims, chunk_size);

        gdsi_dprintf(GDS_DEBUG_INFO,
            "Sending TGTREQ_CREATE to %d\n", trank);
        gdsi_send_target_request(&cp,
            sizeof(cp), MPI_BYTE, trank, GDS_TGTREQ_CREATE);

#ifdef GDS_CONFIG_COUNTER
        /* TODO: not correct when tend - tbegin > 1 */
        gdsi_counter_add(&common->counter, GDS_COUNTER_ALLOC,
            cp.size * typelen);
#endif

        struct gdsi_chunk_desc *cd = common->chunk_sets->chunk_descs;
        cd[clrank].global_index_flat = gdsi_linearize_index(common, chunk_idx);
        cd[clrank].size_flat = linearize_size(common, chunk_size);
        cd[clrank].target_rank = trank;
        cd[clrank].next_sibling_chunk = -1;
        /* target_index will be set by receive_alloc_result */
    }

    for (i = otbegin; i < otend; i++) {
        cp.size = 0;
        int tr = map_nonowner_into_world(common, i);
        gdsi_dprintf(GDS_DEBUG_INFO,
            "Sending TGTREQ_CREATE to %d\n", tr);
        gdsi_send_target_request(&cp, sizeof(cp), MPI_BYTE, tr,
            GDS_TGTREQ_CREATE);
    }

    if (common->representation == GDSI_REPR_LOG) {
        int displs = 0;

        for (i = 0; i < tsize; i++) {
            int tb, te, t;
            int blocks = 0;
            size_t size;
            get_target_rank_range(comm, i, &tb, &te);
            for (t = tb; t < te; t++) {
                size = gdsi_minchunk_size(mc, t);
                /* Note: size is a number of elements */
                blocks += log_required_blocks(0, size * typelen,
                    common->log_user_bs);
            }
            size = gdsi_minchunk_size(mc, i);
            common->log_size_limits[i] = log_memory_area_size(size * typelen,
                common->log_user_bs, cp.log_prealloc_vers);
            common->log_md_recvcounts[i] = blocks;
            common->log_md_displs[i] = displs;
            gdsi_dprintf(GDS_DEBUG_INFO, "blocks=%d displs=%d\n",
                blocks, displs);
            displs += blocks;
        }
    }

    gdsi_minchunk_free(mc);

    /* Receiving allocation results from target side */

    if (receive_alloc_result(common->chunk_sets->chunk_descs,
            array_ids_tmp,
            common->log_md_cache, common->log_md_displs,
            common->log_md_recvcounts,
            common->log_cur_md_heads,
            clrank, trank,
            common->flavor) < 0) {
        free(array_ids_tmp);
        free(no_displs);
        free(no_cnts);
        free_common(common);

        return GDS_STATUS_NOMEM;
    }

    for (i = otbegin; i < otend; i++) {
        struct gdsi_target_alloc_result rep;
        int t = map_nonowner_into_world(common, i);

        GDSI_MPI_CALL(MPI_Recv(&rep, sizeof(rep), MPI_BYTE, t, GDS_TGTREP_CREATE,
                gdsi_comm_world, MPI_STATUS_IGNORE));
        array_ids_tmp[t] = rep.array_id;
        gdsi_dprintf(GDS_DEBUG_INFO, "common->array_ids[%d]=%d\n",
            t, array_ids_tmp[t]);
    }

    /*
      TODO: can we merge/overlap these allgathers for performance? (low prio)
    */

    MPI_Allgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL,
        array_ids_tmp, 1, MPI_INT, comm);
    if (clsize < tsize)
        MPI_Allgatherv(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL,
            array_ids_tmp + clsize, no_cnts, no_displs, MPI_INT, comm);

    for (i = 0; i < clsize; i++) {
        int t = gdsi_map_owner_into_world(common, i);
        common->array_ids[t] = array_ids_tmp[i];
    }
    for (i = 0; i < tsize - clsize; i++) {
        int t = map_nonowner_into_world(common, i);
        common->array_ids[t] = array_ids_tmp[i + clsize];
    }
    gdsi_dprintf(GDS_DEBUG_INFO, "Array IDs after Allgatherv()\n");
    print_array_ids(common);

    GDSI_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, gdsi_chunk_desc_type,
            common->chunk_sets->chunk_descs, 1, gdsi_chunk_desc_type,
            comm));

    if (common->representation == GDSI_REPR_LOG) {
        GDSI_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_INT,
                common->log_cur_md_heads, 1, MPI_INT,
                comm));
        GDSI_MPI_CALL(
            MPI_Allgatherv(MPI_IN_PLACE, 0, MPI_INT,
                common->log_md_cache, common->log_md_recvcounts,
                common->log_md_displs, MPI_INT, comm));
        for (i = 0; i < tsize; i++)
            common->log_tail_cache[i] = estimate_log_tail(
                common->log_cur_md_heads[i], common->log_md_recvcounts[i],
                common->log_user_bs);
    }

    set_rma_window(common);

    if (clrank == 0) {
        gdsi_dprintf(GDS_DEBUG_INFO, "Chunk metadata after MPI_Allgather:\n");
        for (i = 0; i < clsize; i++)
            print_chunk_desc(common, i, common->chunk_sets->chunk_descs+i);
        if (common->representation == GDSI_REPR_LOG) {
            for (i = 0; i < clsize; i++)
                print_log_md_cache(common, i);
        }
    }

    *common_retp = common;

    /* Cleanup temporary buffers */
    free(array_ids_tmp);
    free(no_displs);
    free(no_cnts);

    return GDS_STATUS_OK;
}

int gdsi_inc_global_index(size_t ndims, GDS_size_t global_indices[],
    const GDS_size_t counts[]) {
    int direction, start, stop;
    int i;
    int overflow = 1;

    start = ndims - 1;
    direction = -1;
    stop = -1;

    /* For user-specified configuarations,
       order shouldn't semenatically matter */
    for (i = start; i != stop; i += direction) {
        if (global_indices[i] < counts[i] - 1) {
            ++global_indices[i];
            overflow = 0;
            break;
        }
        global_indices[i] = 0;
    }

    return !overflow;
}

struct SL_chunk_desc {
    struct gdsi_chunk_desc this;
    struct SL_chunk_desc *next;
};

GDS_status_t gdsi_create(size_t ndims, const size_t md_counts[],
    GDS_datatype_t datatype, GDS_global_to_local_func_t global_to_local_func,
    GDS_local_to_global_func_t local_to_global_func, void *local_buffer,
    size_t local_buffer_count, GDS_comm_t comm_user, MPI_Info info,
    struct gdsi_gds_common **common_retp)
{
    MPI_Comm comm;
    struct gdsi_gds_common *common = NULL;
    struct gdsi_target_create cp;
    int tsize, trank = -1, clsize, clrank, i;
    int typelen;
    int error_line = 0;
    enum gdsi_data_repr repr;
    GDS_size_t global_ind[ndims];
    GDS_size_t global_ind_flat = 0;
    struct SL_chunk_desc *sl_chunk_desc, *sl_chunk_desc_root = NULL, \
        *sl_chunk_desc_next;
    int local_rank; /* last_local_rank; */
    GDS_size_t local_offset; /* last_local_offset; */
    size_t n_chunks = 1;

    /* temporary array IDs */
    int *array_ids_tmp = NULL;

    /* Temporary buffers for non-owner assignments */
    int *no_displs = NULL, *no_cnts = NULL;

    MPI_Comm_dup(comm_user, &comm);
    MPI_Comm_size(gdsi_comm_world, &tsize);
    MPI_Comm_size(comm, &clsize);
    MPI_Comm_rank(comm, &clrank);

    gdsi_dprintf(GDS_DEBUG_INFO, "gdsi_create: %d/%d/%d\n",
        clrank, clsize, tsize);

    array_ids_tmp = malloc(sizeof(int) * tsize);
    no_displs = malloc(sizeof(int) * clsize);
    no_cnts = malloc(sizeof(int) * clsize);
    if (array_ids_tmp == NULL || no_displs == NULL || no_cnts == NULL) {
        free(array_ids_tmp);
        free(no_displs);
        free(no_cnts);
        return GDS_STATUS_NOMEM;
    }

    GDSI_MPI_CALL(MPI_Type_size(datatype, &typelen));

    repr = parse_repr(info);
    if (repr == GDSI_REPR_INVALID) {
        gdsi_dprintf(GDS_DEBUG_ERR, "invalid array represenation\n");
        error_line = __LINE__;
        goto reduce;
    }

    common = ac_init_common(ndims, datatype, md_counts, tsize, clsize, clrank,
                            typelen, repr, comm, info, &error_line);
    if (!common) { goto reduce; }

    trank = gdsi_map_owner_into_world(common, clrank);

    common->flavor = GDS_FLAVOR_CREATE;
    common->global_to_local_func = global_to_local_func;
    common->local_to_global_func = local_to_global_func;
    common->nelements_local = NULL; // w/ create, we can't really know this
    common->offset_local = NULL;
    common->nelements_local_flat = local_buffer_count;

    init_nonowner_assignments(common, no_displs, no_cnts);

    ac_set_target_cp(common, typelen, &cp, GDS_FLAVOR_CREATE,
        local_buffer, local_buffer_count, info);

    for (i = 0; i < ndims; ++i) { global_ind[i] = 0; }
    global_to_local_func(global_ind, &local_rank, &local_offset);
    sl_chunk_desc = malloc(sizeof(*sl_chunk_desc));
    sl_chunk_desc_root = sl_chunk_desc;
    sl_chunk_desc->this.target_rank = local_rank;
    sl_chunk_desc->this.target_index = local_offset;
    sl_chunk_desc->this.size_flat = 1;
    sl_chunk_desc->this.global_index_flat = 0;
    sl_chunk_desc->this.next_sibling_chunk = -1; /* TODO: is this right? */
    sl_chunk_desc->next = NULL;
    print_chunk_desc(common, 0, &sl_chunk_desc_root->this);

    global_ind_flat = 1;

    while (gdsi_inc_global_index(ndims, global_ind, md_counts)) {
        global_to_local_func(global_ind, &local_rank, &local_offset);
        sl_chunk_desc->next = malloc(sizeof(*sl_chunk_desc));
        sl_chunk_desc = sl_chunk_desc->next;
        sl_chunk_desc->this.target_rank = local_rank;
        sl_chunk_desc->this.target_index = local_offset;
        sl_chunk_desc->this.size_flat = 1;
        sl_chunk_desc->this.global_index_flat = global_ind_flat;
        print_chunk_desc(common, n_chunks, &sl_chunk_desc->this);
        sl_chunk_desc->next = NULL;

        ++n_chunks;
        ++global_ind_flat;
    }

    if (ac_set_chunk_set(common, n_chunks, &error_line)) { goto reduce; }

reduce:
    GDSI_MPI_CALL(MPI_Allreduce(MPI_IN_PLACE, &error_line, 1, MPI_INT,
                                 MPI_MAX, comm));
    if (error_line != 0) {
        free(array_ids_tmp);
        free(no_displs);
        free(no_cnts);
        if (common == NULL)
            MPI_Comm_free(&comm);
        else
            free_common(common);
        return GDS_STATUS_NOMEM;
    }

    gdsi_dprintf(GDS_DEBUG_INFO,
        "Sending TGTREQ_CREATE to %d\n", trank);
    gdsi_send_target_request(&cp, sizeof(cp), MPI_BYTE, trank,
        GDS_TGTREQ_CREATE);

    struct gdsi_chunk_desc *cd = common->chunk_sets->chunk_descs;

    sl_chunk_desc = sl_chunk_desc_root;
    i = 0;
    while (sl_chunk_desc != NULL) {
        memcpy(&cd[i], &sl_chunk_desc->this, sizeof(sl_chunk_desc->this));
        sl_chunk_desc_next = sl_chunk_desc->next;
        free(sl_chunk_desc);
        sl_chunk_desc = sl_chunk_desc_next;
        ++i;
    }

    if (receive_alloc_result(common->chunk_sets->chunk_descs,
            common->array_ids,
            common->log_md_cache, common->log_md_displs,
            common->log_md_recvcounts,
            common->log_cur_md_heads,
            clrank, trank,
            common->flavor) < 0) {
        free(array_ids_tmp);
        free(no_displs);
        free(no_cnts);
        free_common(common);

        return GDS_STATUS_NOMEM;
    }

    set_rma_window(common);

    /*
      TODO: can we merge/overlap these allgathers for performance? (low prio)
    */

    GDSI_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_INT,
                                 common->array_ids,
                                 1,
                                 MPI_INT,
                                 comm));

    GDSI_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_BYTE,
                                 common->chunk_sets->chunk_descs,
                                 sizeof(struct gdsi_chunk_desc),
                                 MPI_BYTE,
                                 comm));

    if (common->representation == GDSI_REPR_LOG) {
        GDSI_MPI_CALL(MPI_Allgather(MPI_IN_PLACE, 0, MPI_INT,
                common->log_cur_md_heads, 1, MPI_INT,
                comm));
        GDSI_MPI_CALL(
            MPI_Allgatherv(MPI_IN_PLACE, 0, MPI_INT,
                common->log_md_cache, common->log_md_recvcounts,
                common->log_md_displs, MPI_INT, comm));
        for (i = 0; i < tsize; i++)
            common->log_tail_cache[i] = estimate_log_tail(
                common->log_cur_md_heads[i], common->log_md_recvcounts[i],
                common->log_user_bs);
    }

    if (clrank == 0) {
        gdsi_dprintf(GDS_DEBUG_INFO, "Chunk metadata after MPI_Allgather:\n");
        for (i = 0; i < tsize; i++)
            print_chunk_desc(common, i, common->chunk_sets->chunk_descs+i);
        if (common->representation == GDSI_REPR_LOG) {
            for (i = 0; i < tsize; i++)
                print_log_md_cache(common, i);
        }
    }

    *common_retp = common;

    /* Cleanup temporary buffers */
    free(array_ids_tmp);
    free(no_displs);
    free(no_cnts);

    return GDS_STATUS_OK;
}

GDS_status_t gdsi_free(struct gdsi_gds_common **commonp)
{
    struct gdsi_gds_common *common = *commonp;
    int nobegin, noend, i, myrank, mytrank, array_id;
    GDS_status_t rep;

    MPI_Comm_rank(common->comm, &myrank);
    mytrank = gdsi_map_owner_into_world(common, myrank);

    array_id = gdsi_get_array_id(common, mytrank);
    gdsi_dprintf(GDS_DEBUG_INFO, "Sending TGTREQ_FREE to rank=%d\n", mytrank);
    gdsi_send_target_request(&array_id, 1, MPI_INT, mytrank, GDS_TGTREQ_FREE);
    get_nonowner_rank_range(common, myrank, &nobegin, &noend);
    gdsi_dprintf(GDS_DEBUG_INFO, "non-owner rank range: %d-%d\n",
        nobegin, noend);
    for (i = nobegin; i < noend; i++) {
        int trank = map_nonowner_into_world(common, i);
        array_id = gdsi_get_array_id(common, trank);
        gdsi_dprintf(GDS_DEBUG_INFO, "Sending TGTREQ_FREE to rank=%d\n", trank);
        gdsi_send_target_request(&array_id, 1, MPI_INT, trank, GDS_TGTREQ_FREE);
    }

    gdsi_dprintf(GDS_DEBUG_INFO,
        "Receiving TGTREP_GENERIC from rank=%d\n", mytrank);
    GDSI_MPI_CALL(MPI_Recv(&rep, 1, MPI_INT, mytrank, GDS_TGTREP_GENERIC,
            gdsi_comm_world, MPI_STATUS_IGNORE));
    if (rep != GDS_STATUS_OK) {
        /* TODO: graceful recovery */
        gdsi_panic("Target process returned %d\n", rep);
    }

    for (i = nobegin; i < noend; i++) {
        int trank = map_nonowner_into_world(common, i);
        gdsi_dprintf(GDS_DEBUG_INFO, "Receiving TGTREP_GENERIC from rank=%d\n",
            trank);
        GDSI_MPI_CALL(MPI_Recv(&rep, 1, MPI_INT, trank, GDS_TGTREP_GENERIC,
                gdsi_comm_world, MPI_STATUS_IGNORE));
        if (rep != GDS_STATUS_OK) {
            /* TODO: graceful recovery */
            gdsi_panic("Target process returned %d\n", rep);
        }
    }

#ifdef GDS_CONFIG_COUNTER
    gdsi_counter_destroy(&common->counter);
#endif

    free_common(common);

    *commonp = NULL;

    gdsi_dprintf(GDS_DEBUG_INFO, "exit\n");

    return GDS_STATUS_OK;
}

static int fits_in_array(const struct gdsi_gds_common *common, const size_t start[])
{
    size_t dims = common->ndims;
    size_t i;

    for (i = 0; i < dims; i++)
        if (start[i] >= common->nelements[i])
            return 0;

    return 1;
}

static size_t offset_in_chunk(const struct gdsi_gds_common *common,
    const struct gdsi_chunk_desc *desc, size_t global_index_flat);

GDS_status_t gdsi_find_first_chunkit(GDS_gds_t gds, const size_t start_md[], gdsi_chunk_iterator_t *it)
{
    struct gdsi_gds_common *common = gds->common;
    int fst;
    size_t start = gdsi_linearize_index(common, start_md);

    if (!fits_in_array(common, start_md)) {
        char start_s[16], size_s[16];
        gdsi_snprint_index(start_s, sizeof(start_s), common->ndims, start_md);
        gdsi_snprint_index(size_s, sizeof(size_s), common->ndims,
            common->nelements);
        gdsi_dprintf(GDS_DEBUG_ERR, "Out of range: nelements:%s start_md:%s\n",
            size_s, start_s);
        return GDS_STATUS_RANGE;
    }

    fst = find_first_chunk_index(common, gds->chunk_set->chunk_descs,
                                 common->nchunk_descs, start_md);


    /* TODO: handle reference counting on common */
    it->common         = common;
    it->cd_begin       = it->cd_cur = gds->chunk_set->chunk_descs + fst;
    it->lastdim_len    = common->nelements[common->ndims-1];
    it->last2dim_begin = common->ndims > 1 ? start_md[common->ndims - 2] : 0;
    it->cd_end         = gds->chunk_set->chunk_descs + common->nchunk_descs;

    it->global_index_flat = start;

    /* Skip zero chunks */
    if (it->cd_cur->size_flat == 0)
        gdsi_chunkit_forward(it);

    return GDS_STATUS_OK;
}

void gdsi_chunkit_forward(gdsi_chunk_iterator_t *it)
{
    int typelen;

    MPI_Type_size(it->common->type, &typelen);

    do {
        size_t flat_off, flat_len, flat_rem;

        flat_off = decompose_index_lastdim(it->global_index_flat, it->lastdim_len)
            - decompose_index_lastdim(it->cd_cur->global_index_flat, it->lastdim_len);
        flat_len = decompose_size_lastdim(it->cd_cur->size_flat,
            it->lastdim_len);

        flat_rem = flat_len - flat_off;

        it->global_index_flat += flat_rem;
        it->cd_cur++;
    } while (it->cd_cur->size_flat == 0 && it->cd_cur < it->cd_end);
    /* Skip targets with zero data size */
}

int gdsi_chunkit_end(const gdsi_chunk_iterator_t *it)
{
    if (it->cd_cur >= it->cd_end)
        return 1;

    if (it->common->ndims > 1 && it->cd_cur > it->cd_begin) {
        size_t cur[it->common->ndims];

        /* If it goes to another row, terminate the iteration */

        gdsi_decompose_index(it->cd_cur->global_index_flat, it->common, cur);
        if (cur[it->common->ndims - 2] > it->last2dim_begin)
            return 1;
    }

    return 0;
}

static size_t offset_in_chunk(const struct gdsi_gds_common *common,
    const struct gdsi_chunk_desc *desc, size_t global_index_flat)
{
    size_t ndims = common->ndims, i;
    size_t gindex[ndims], chunk_lo[ndims], chunk_size[ndims], len[ndims];
    size_t offset = 0, stride = 1;

    gdsi_decompose_index(global_index_flat, common, gindex);
    gdsi_decompose_index(desc->global_index_flat, common, chunk_lo);
    gdsi_decompose_size(desc->size_flat, common, chunk_size);

    for (i = 0; i < ndims; i++)
        len[i] = gindex[i] - chunk_lo[i];

    for (i = 0; i < ndims; i++) {
        size_t j = ndims - i - 1;
        offset += len[j] * stride;
        stride *= chunk_size[j];
    }

    return offset;
}

void gdsi_chunkit_access(const gdsi_chunk_iterator_t *it, struct gdsi_chunk *c)
{
    int typelen;
    size_t offset_in_lastdim
        = decompose_index_lastdim(it->global_index_flat, it->lastdim_len)
        - decompose_index_lastdim(it->cd_cur->global_index_flat, it->lastdim_len);
    size_t size = decompose_size_lastdim(it->cd_cur->size_flat, it->lastdim_len)
        - offset_in_lastdim;

    MPI_Type_size(it->common->type, &typelen);

    c->target_rank      = it->cd_cur->target_rank;
    c->target_rma_index = it->cd_cur->target_index;
    c->target_offset    = offset_in_chunk(
        it->common, it->cd_cur, it->global_index_flat);
    c->size = size;
    c->chunk_number = it->cd_cur->chunk_number;
    c->is_corrupted = it->cd_cur->is_corrupted;
}

static void invalidate_rma(struct gdsi_chunk_desc *desc, int ndescs)
{
    int i;
    for (i = 0; i < ndescs; i++)
        desc[i].target_index = GDSI_TARGET_INDEX_INVALID;
}

GDS_status_t gdsi_check_new_version(struct gdsi_gds_common *common, int target_rank,
        size_t target_index, int chunk_number, GDS_size_t new_version)
{
    int clrank, clsize, trank, owner_num, nonowner_num;
    struct gdsi_chunk_set *cset = common->chunk_sets;
    int elem_size;
    MPI_Type_size(common->type, &elem_size);

    struct gdsi_target_incver *s = (struct gdsi_target_incver *)malloc(sizeof(struct gdsi_target_incver));
    MPI_Comm_rank(common->comm, &clrank);
    MPI_Comm_size(common->comm, &clsize);
    trank = gdsi_map_owner_into_world(common, clrank);
    owner_num = clsize;
    MPI_Group_size(common->non_owner_group, &nonowner_num);

    int pca_target_rank;

    if (common->flavor == GDS_FLAVOR_ALLOC) {
        pca_target_rank = common->chunk_sets->chunk_descs[chunk_number].target_rank;

        if (pca_target_rank < owner_num) {
            pca_target_rank = gdsi_map_owner_into_world(common, pca_target_rank);
            s->owner_flag = 1;
        }
        else {
            pca_target_rank -= owner_num; /* rank in nonowner group */
            pca_target_rank = map_nonowner_into_world(common, pca_target_rank);
            s->owner_flag = 0;
        }

        s->array_id = gdsi_get_array_id(common, trank);
        s->version  = new_version;
        s->store_version = common->chunk_sets->version;
        s->target_rank = trank;
        s->pca_target_rank = pca_target_rank;
        s->pca_array_id = gdsi_get_array_id(common, pca_target_rank);
        s->chunk_number = chunk_number;
        s->target_index = target_index;

        s->chunk_size = elem_size * calc_total_count(common, common->chunk_sets->chunk_descs[chunk_number].size_flat);

        gdsi_send_target_request((void *)s, sizeof(struct gdsi_target_incver), MPI_BYTE, trank, GDS_TGTREQ_INCVER);
        receive_alloc_result(cset->chunk_descs, NULL, NULL, NULL, NULL, common->log_cur_md_heads, chunk_number, trank, common->flavor);

    } else { /* common->flavor==GDS_CREATE */

        s->array_id = gdsi_get_array_id(common, trank);
        s->version  = new_version;
        s->store_version = common->chunk_sets->version;
        s->target_rank = trank;
        s->pca_target_rank = trank;
        s->pca_array_id = s->array_id;
        s->chunk_number = chunk_number;
        s->target_index = target_index;
        s->owner_flag = 1;
        s->chunk_size = elem_size * calc_total_count(common, common->chunk_sets->chunk_descs[chunk_number].size_flat);

        gdsi_send_target_request((void *)s, sizeof(struct gdsi_target_incver), MPI_BYTE, trank, GDS_TGTREQ_INCVER);
        receive_alloc_result(cset->chunk_descs, NULL, NULL, NULL, NULL, common->log_cur_md_heads, clrank, trank, common->flavor);
    }

    free(s);
}

void gdsi_metadata_sync(struct gdsi_gds_common *common)
{
    MPI_Waitall(common->n_reqs, common->reqs, MPI_STATUSES_IGNORE);
    common->n_reqs = 0;

    gdsi_dprintf(GDS_DEBUG_INFO, "after MPI_Waitall\n");
}

GDS_status_t gdsi_create_new_version(struct gdsi_gds_common *common,
                                      GDS_size_t new_version)
{
    int clrank, clsize, trank, i, j, owner_num, nonowner_num;
    struct gdsi_chunk_set *cset;
    int elem_size;
    MPI_Type_size(common->type, &elem_size);
    struct gdsi_target_incver *s;
    s = (struct gdsi_target_incver *)malloc(common->n_owning_chunks *
        sizeof(struct gdsi_target_incver));

    gdsi_dprintf(GDS_DEBUG_INFO, "called with new_version=%zu\n", new_version);

    MPI_Comm_rank(common->comm, &clrank);
    MPI_Comm_size(common->comm, &clsize);
    trank = gdsi_map_owner_into_world(common, clrank);
    owner_num = clsize;
    MPI_Group_size(common->non_owner_group, &nonowner_num);
    gdsi_dprintf(GDS_DEBUG_INFO, "owner size %d, nonowner size %d\n", owner_num,
    nonowner_num);
    cset = alloc_chunk_set(common->nchunk_descs, new_version);
    if (cset == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to allocate chunk set\n");
        return GDS_STATUS_NOMEM;
    }

    int chunk_number, pca_target_rank;
    /* For pca target storing chunk consideration, if pca
     * target is nonowner, it should lock the window.
     * =0: nonowner, =1: owner.
     */
    int owner_flag = 0;

    /* Record the world group view at this moment */
    MPI_Comm_group(gdsi_comm_world, &cset->world_group);

    if (common->flavor == GDS_FLAVOR_ALLOC) {
        /* update all chunk metadata */
        for (i = 0; i < common->nchunk_descs; ++i) {
            cset->chunk_descs[i] = common->chunk_sets->chunk_descs[i];
            cset->chunk_descs[i].chunk_number = i;

            chunk_number = i;

            if (common->representation == GDSI_REPR_FLAT
                && !gdsi_disable_pca)
                pca_target_rank = gdsi_get_pca_target_rank(chunk_number, owner_num,
                    nonowner_num, common->chunk_sets->version);
            else {
                /* Disable PCA and always create versions in the same server process */
                /* TODO: this may not work when communicator shrink (=process loss)
                   happens */
                pca_target_rank = chunk_number;
            }
            if (pca_target_rank < 0)
                gdsi_dprintf(GDS_DEBUG_ERR, "gdsi_create_new_version: pca target rank calculation error, returns %d\n",
                    pca_target_rank);
            if (pca_target_rank < owner_num) {
                pca_target_rank = gdsi_map_owner_into_world(common, pca_target_rank);
                owner_flag = 1;
            }
            else {
                pca_target_rank -= owner_num; /* rank in nonowner group */
                pca_target_rank = map_nonowner_into_world(common, pca_target_rank);
                owner_flag = 0;
            }
            common->chunk_sets->chunk_descs[i].target_rank = pca_target_rank;
            common->chunk_sets->chunk_descs[i].chunk_number = i;

            for (j = 0; j < common->n_owning_chunks; j++) {
                if (i == common->owning_chunks[j]) {
                    s[j].owner_flag = owner_flag;
                    break;
                }
            }
        }

        for (i = 0; i < common->n_owning_chunks; i++) {
            chunk_number = common->owning_chunks[i];
            pca_target_rank = common->chunk_sets->chunk_descs[chunk_number].target_rank;

            s[i].array_id = gdsi_get_array_id(common, trank);
            s[i].version  = new_version;
            s[i].store_version = common->chunk_sets->version;
            s[i].target_rank = trank;
            s[i].pca_target_rank = pca_target_rank;
            s[i].pca_array_id = gdsi_get_array_id(common, pca_target_rank);
            s[i].chunk_number = chunk_number;
            s[i].target_index =
                common->chunk_sets->chunk_descs[chunk_number].target_index;

            s[i].chunk_size = elem_size * calc_total_count(common,
                common->chunk_sets->chunk_descs[chunk_number].size_flat);

            gdsi_dprintf(GDS_DEBUG_INFO, "rank %d: version %zu chunk %d pca_target_rank %d" 
                " target_index %zu\n",
                clrank, s[i].store_version, s[i].chunk_number,
                s[i].pca_target_rank, s[i].target_index);
            gdsi_send_target_request(&s[i], sizeof(struct gdsi_target_incver),
                MPI_BYTE, trank, GDS_TGTREQ_INCVER);
            gdsi_dprintf(GDS_DEBUG_INFO, "gdsi target request sent\n");
            receive_alloc_result(cset->chunk_descs, NULL,
                NULL, NULL, NULL, common->log_cur_md_heads,
                chunk_number, trank, common->flavor);
            gdsi_dprintf(GDS_DEBUG_INFO, "gdsi target request received\n");
            gdsi_dprintf(GDS_DEBUG_INFO, "rank %d: receive response for version %zu chunk %d pca_target_rank %d"
                " target_index %zu\n",
                clrank, s[i].store_version, s[i].chunk_number, s[i].pca_target_rank,
                s[i].target_index);
        }

        /* Debug print */
        for (i = 0; i < common->n_owning_chunks; i++) {
            chunk_number = common->owning_chunks[i];
            gdsi_dprintf(GDS_DEBUG_INFO, "chunk_descs[%d] before MPI_Allgather\n",
                chunk_number);
            print_chunk_desc(common, chunk_number, cset->chunk_descs+chunk_number);
        }

        invalidate_rma(common->chunk_sets->chunk_descs, common->nchunk_descs);
        SGLIB_DL_LIST_ADD_BEFORE(struct gdsi_chunk_set, common->chunk_sets,
                             cset, newer, older);

        GDSI_MPI_CALL(MPI_Iallgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL,
            cset->chunk_descs, 1, gdsi_chunk_desc_type, common->comm,
            &common->reqs[common->n_reqs++]));

        gdsi_dprintf(GDS_DEBUG_INFO, "after MPI_Iallgatherv\n");

        if (common->representation == GDSI_REPR_LOG) {
            GDSI_MPI_CALL(MPI_Iallgather(MPI_IN_PLACE, 0, MPI_INT,
                common->log_cur_md_heads, 1, MPI_INT,
                common->comm, &common->reqs[common->n_reqs++]));

            GDSI_MPI_CALL(
                MPI_Iallgatherv(MPI_IN_PLACE, 0, MPI_INT,
                    common->log_md_cache, common->log_md_recvcounts,
                    common->log_md_displs, MPI_INT, common->comm,
                                &common->reqs[common->n_reqs++]));
        }

        if (common->representation == GDSI_REPR_LOG) {
            for (i = 0; i < clsize; i++)
                common->log_tail_cache[i] = estimate_log_tail(
                    common->log_cur_md_heads[i], common->log_md_recvcounts[i],
                    common->log_user_bs);
        }

    } else { /* common->flavor==GDS_CREATE */
        /* I don't think chunk_descs[rank] is meaningful here
        * if (common->chunk_sets->chunk_descs[rank].size_flat > 0) */
        for (i = 0; i < common->nchunk_descs; ++i) {
            cset->chunk_descs[i] = common->chunk_sets->chunk_descs[i];
            cset->chunk_descs[i].chunk_number = i;
        }

        for (i = 0; i < common->n_owning_chunks; i++) {
            chunk_number = common->owning_chunks[i];

            s[i].array_id = gdsi_get_array_id(common, trank);
            s[i].version  = new_version;
            s[i].store_version = common->chunk_sets->version;
            s[i].target_rank = trank;
            s[i].pca_target_rank = trank;
            s[i].pca_array_id = s[i].array_id;
            s[i].chunk_number = chunk_number;
            s[i].target_index =
                common->chunk_sets->chunk_descs[chunk_number].target_index;
            s[i].owner_flag = 1;
            
            s[i].chunk_size = elem_size * calc_total_count(common,
                common->chunk_sets->chunk_descs[chunk_number].size_flat);

            gdsi_dprintf(GDS_DEBUG_INFO,
                "Sending TGTREQ_INCVER to %d\n", trank);
            gdsi_send_target_request(&s[i], sizeof(struct gdsi_target_incver),
                MPI_BYTE, trank, GDS_TGTREQ_INCVER);
            receive_alloc_result(cset->chunk_descs, NULL,
                NULL, NULL, NULL, common->log_cur_md_heads,
                clrank, trank, common->flavor);
        }
        invalidate_rma(common->chunk_sets->chunk_descs, common->nchunk_descs);
        SGLIB_DL_LIST_ADD_BEFORE(struct gdsi_chunk_set, common->chunk_sets,
                                cset, newer, older);
    }

    common->latest_version = new_version;

#ifdef GDS_CONFIG_USE_SCR
    /* Whether flush version to scr? 1) control by user, set FLUSH_to_SCR
     * frequence; 2) control by SCR, call SCR_Need_checkpoint(&flag). If
     * flag=1, need checkpoint. Otherwise, don't need */
    /* int flag;
     * SCR_Need_checkpoint(&flag);
     * gdsi_dprintf(GDS_DEBUG_INFO, "need checkpoint\n");*/

    if (SCR_SUPPORT == true && common->chunk_sets->version % flush_to_scr_freq == 0) {
        struct gdsi_target_flush_scr t;
        gdsi_dprintf(GDS_DEBUG_INFO, "[rank %d] call SCR start checkpoint.\n", trank);
        SCR_Start_checkpoint();

        for (i = 0; i < common->n_owning_chunks; i++) {
            t.array_id = s[i].pca_array_id;
            t.unique_array_id = gdsi_get_array_id(common, 0); /* array id on process 0 */
            t.version = s[i].store_version;
            t.chunk_number = s[i].chunk_number;
            pca_target_rank = s[i].pca_target_rank;

            gdsi_dprintf(GDS_DEBUG_INFO, "[rank %d] send flush scr result"
                "from pca target rank %d\n", trank, s[i].pca_target_rank);
            gdsi_send_target_request(&t, sizeof(t), MPI_BYTE, pca_target_rank,
                    GDS_TGTREQ_FLUSH_SCR);
            receive_flush_scr_result(pca_target_rank);

            gdsi_dprintf(GDS_DEBUG_INFO, "[rank %d] receive flush scr result"
                "from pca target rank %d\n", trank, s[i].pca_target_rank);
        }

        MPI_Barrier(gdsi_comm_world);

        SCR_Complete_checkpoint(1);
    }
#endif

    common->chunk_sets = cset;
    common->latest_version = new_version;
    free(s);

#ifdef GDS_CONFIG_COUNTER
    gdsi_counter_add(&common->counter, GDS_COUNTER_VER_INC, 1);
#endif

    return GDS_STATUS_OK;
}

enum gdsi_fetch_state {
    GDSI_FETCH_ISSUED,
    GDSI_FETCH_REQ_SENT,
    GDSI_FETCH_GOT_STATUS,
    GDSI_FETCH_DONE,
};

struct fetch_workq {
    struct gdsi_nbworkq_entry wq; /* This must be at the head */

    struct gdsi_target_fetch ft;
    MPI_Request req_send;
    MPI_Request req_status;
    MPI_Request req_data;
    enum gdsi_fetch_state state;
    int reply;

    /* Data members to re-issue MPI_Irecv */
    void *orig_buff;
    size_t total_len;
    size_t rcvd_len;
    int target_rank;
};

static void fetch_release_tags(struct fetch_workq *wq)
{
    gdsi_dyntag_free(gdsi_comm_world, wq->target_rank, wq->ft.tag_status);
    gdsi_dyntag_free(gdsi_comm_world, wq->target_rank, wq->ft.tag_data);
}

static int fetch_ready(struct gdsi_nbworkq_entry *wqe, MPI_Status *status)
{
    struct fetch_workq *wq = (struct fetch_workq *) wqe;
    int flag = 0, r;

    switch (wq->state) {
    case GDSI_FETCH_ISSUED:
        gdsi_target_signal_thread(wq->target_rank);
        wq->state = GDSI_FETCH_REQ_SENT;
        GDSI_MPI_CALL(MPI_Test(&wq->req_status, &flag, status));
        if (!flag)
            return 0;

    case GDSI_FETCH_REQ_SENT:
        if (wq->reply != GDS_STATUS_OK) {
            gdsi_dprintf(GDS_DEBUG_ERR,
                "[%u] target_fetch from rank %d returned %d\n",
                wq->wq.id, wq->target_rank, wq->reply);
            /* TODO: how to deal with this? */
            GDSI_MPI_CALL(MPI_Cancel(&wq->req_data));
            fetch_release_tags(wq);
            wq->state = GDSI_FETCH_DONE;
            return 1;
        }

        wq->state = GDSI_FETCH_GOT_STATUS;
        GDSI_MPI_CALL(MPI_Test(&wq->req_data, &flag, status));
        if (!flag)
            return 0;

        /* Fall through, if Irecv is ready */
        gdsi_dprintf(GDS_DEBUG_INFO,
            "[%u] fall through to GDSI_FETCH_GOT_STATUS\n", wq->wq.id);
    case GDSI_FETCH_GOT_STATUS:
        gdsi_dprintf(GDS_DEBUG_INFO,
            "[%u] GDSI_FETCH_GOT_STATUS\n", wq->wq.id);
        do {
            GDSI_MPI_CALL(MPI_Get_count(status, MPI_BYTE, &r));

            gdsi_dprintf(GDS_DEBUG_INFO,
                "[%u] received %d bytes from rank %d\n",
                wq->wq.id, r, status->MPI_SOURCE);
            wq->rcvd_len += r;
            if (wq->rcvd_len >= wq->total_len) {
                gdsi_dprintf(GDS_DEBUG_INFO, "[%u] done\n", wq->wq.id);
                fetch_release_tags(wq);
                wq->state = GDSI_FETCH_DONE;
                return 1;
            }
            gdsi_dprintf(GDS_DEBUG_INFO, "[%u] Posting Irecv (again): "
                "target_rank=%d, total_len=%zu, rcvd_len=%zu\n", wq->wq.id,
                wq->target_rank, wq->total_len, wq->rcvd_len);
            GDSI_MPI_CALL(MPI_Irecv(wq->orig_buff + wq->rcvd_len,
                    wq->total_len - wq->rcvd_len, MPI_BYTE,
                    wq->target_rank, wq->ft.tag_data, gdsi_comm_world,
                    &wq->req_data));
            GDSI_MPI_CALL(MPI_Test(&wq->req_data, &flag, status));
        } while (flag);

        return 0;

    case GDSI_FETCH_DONE:
        gdsi_dprintf(GDS_DEBUG_ERR, "Hmm... why coming here??\n");
        return 1;
    };

    gdsi_panic("Should not reach here!\n");

    return 0;
}

static MPI_Request *fetch_request(struct gdsi_nbworkq_entry *wqe)
{
    struct fetch_workq *wq = (struct fetch_workq *) wqe;

    switch (wq->state) {
    case GDSI_FETCH_ISSUED:
        return &wq->req_send;
    case GDSI_FETCH_REQ_SENT:
        return &wq->req_status;
    case GDSI_FETCH_GOT_STATUS:
        return &wq->req_data;
    case GDSI_FETCH_DONE:
        break;
    }

    gdsi_panic("Should not reach here!\n");

    return NULL;
}

static int fetch_test(struct gdsi_nbworkq_entry *wqe)
{
    struct fetch_workq *wq = (struct fetch_workq *) wqe;
    MPI_Status status;
    int flag;

    if (wq->state == GDSI_FETCH_DONE)
        return 1;

    GDSI_MPI_CALL(MPI_Test(fetch_request(wqe), &flag, &status));
    if (flag)
        return fetch_ready(wqe, &status);

    return 0;
}

GDS_status_t gdsi_indirect_fetch(GDS_gds_t gds, size_t target_rank,
    void *buf, size_t offset, size_t count, int chunk_number, struct gdsi_nbworkq_entry **wqep)
{
    MPI_Datatype ty = gds->common->type;
    int unit_size;
    struct fetch_workq *wq;

    if (count == 0)
        return GDS_STATUS_OK;

    /* TODO: parameter validation */

    MPI_Type_size(ty, &unit_size);

    wq = malloc(sizeof(*wq));
    if (wq == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to allocate workqueue object.\n");
        return GDS_STATUS_NOMEM;
    }

    gdsi_nbworkq_init(&wq->wq, gds, fetch_test, fetch_request, fetch_ready);

    wq->ft.array_id   = gdsi_get_array_id(gds->common, target_rank);
    wq->ft.unique_array_id = gdsi_get_array_id(gds->common, 0);
    wq->ft.version    = gds->chunk_set->version;
    wq->ft.chunk_number = chunk_number;
    wq->ft.offset     = offset;
    wq->ft.count      = count;
    wq->ft.ckpt_location_flags =
        gds->chunk_set->chunk_descs[chunk_number].ckpt_location_flags;
    wq->ft.tag_status = gdsi_dyntag_alloc(gdsi_comm_world, target_rank);
    gdsi_assert(wq->ft.tag_status >= 0);
    wq->ft.tag_data   = gdsi_dyntag_alloc(gdsi_comm_world, target_rank);
    gdsi_assert(wq->ft.tag_data >= 0);

    gdsi_isend_target_request(&wq->ft, sizeof(wq->ft), MPI_BYTE, target_rank,
        GDS_TGTREQ_FETCH, &wq->req_send);
    gdsi_dprintf(GDS_DEBUG_INFO, "sent GDS_TGTREQ_FETCH"
        " {array_id=%d, version=%zu, chunk=%d, offset=%zu, count=%zu, "
        "tag_status=%d, tag_data=%d} to target %zu\n",
        wq->ft.array_id, wq->ft.version, wq->ft.chunk_number, wq->ft.offset, wq->ft.count,
        wq->ft.tag_status, wq->ft.tag_data, target_rank);

    wq->state       = GDSI_FETCH_ISSUED;
    wq->orig_buff   = buf;
    wq->total_len   = count * unit_size;
    wq->rcvd_len    = 0;
    wq->target_rank = target_rank;

    GDSI_MPI_CALL(MPI_Irecv(&wq->reply, 1, MPI_INT, target_rank,
            wq->ft.tag_status, gdsi_comm_world, &wq->req_status));
    gdsi_dprintf(GDS_DEBUG_INFO, "Posting Irecv: target_rank=%d, "
        "total_len=%zu, rcvd_len=%zu, tag_data=%d\n",
        wq->target_rank, wq->total_len, wq->rcvd_len, wq->ft.tag_data);
    GDSI_MPI_CALL(MPI_Irecv(wq->orig_buff, wq->total_len, MPI_BYTE,
            wq->target_rank, wq->ft.tag_data, gdsi_comm_world,
            &wq->req_data));

    if (wqep == NULL) {
        /* TODO: blocking implementation -- this should be removed
           in the future */
        MPI_Status status;
        int r;
        do {
            MPI_Wait(wq->wq.request(&wq->wq), &status);
            r = wq->wq.ready(&wq->wq, &status);
        } while (!r);
        free(wq);
    }
    else
        *wqep = &wq->wq;

    return GDS_STATUS_OK;
}

GDS_status_t gdsi_get_common_by_array_id(const int array_id,
    struct gdsi_gds_common **common) {
    struct gdsi_gds_common *current;
    *common = NULL;
    int rank;
    GDS_comm_rank(gdsi_comm_world, &rank);
    pthread_rwlock_rdlock(&gdsi_gds_common_list_lock);
    SGLIB_DL_LIST_GET_FIRST(struct gdsi_gds_common, gdsi_gds_common_list,
        prev, next, current);
    for(; current != NULL; current = current->next) {
        if (current->array_ids[rank] == array_id) {
            *common = current;
            break;
        }
    }
    pthread_rwlock_unlock(&gdsi_gds_common_list_lock);
    return GDS_STATUS_OK;
}

//We'll report everything in terms of an n-dimensional offset and a
//1-dimensional length, since I don't think we need to report 2-d blocks
//(for now)
static void reverse_idx(const size_t ndims, const size_t array_ct[], const size_t
    err_start_idx, const size_t err_ct, size_t *num_err_segments, size_t
    ***err_start_idxs, size_t **err_cts) {
    //Find start idx:
    size_t current_idx[ndims];
    gdsi_decompose_index_impl(err_start_idx, ndims, array_ct, current_idx);

    // find number of segments that we're dealing with. Each partial or complete
    // row is a new segment
    *num_err_segments = 0;
    int first_is_partial = 0;
    int last_is_partial = 0;
    size_t start_col = current_idx[ndims-1];
    size_t ncols = array_ct[ndims-1];

    size_t err_ct_remaining = err_ct;
    size_t elmts_in_first = GDSI_MIN(ncols - start_col, err_ct);
    if (elmts_in_first > 0) {
        ++(*num_err_segments);
        first_is_partial = 1;
        err_ct_remaining -= elmts_in_first;
    }
    size_t segments_in_middle = err_ct_remaining / ncols;
    (*num_err_segments) += segments_in_middle;
    err_ct_remaining -= segments_in_middle * ncols;
    size_t elmts_in_last = err_ct_remaining;
    if (elmts_in_last > 0) {
        ++(*num_err_segments);
        last_is_partial = 1;
    }
    *err_start_idxs = malloc(*num_err_segments * sizeof(**err_start_idxs));
    *err_cts = malloc(*num_err_segments * sizeof(*err_cts));

    int seg;
    for (seg = 0; seg < *num_err_segments; ++seg) {
        (*err_cts)[seg] = ncols;
        size_t array_sz = ndims * sizeof(***err_start_idxs);
        (*err_start_idxs)[seg] = malloc(array_sz);
        memcpy((*err_start_idxs)[seg], current_idx, array_sz);
        current_idx[ndims-1] = ncols - 1; // move to end of this row
        // use previously written code to move to the beginning of the next row
        gdsi_inc_global_index(ndims, current_idx, array_ct);
    }
    if (first_is_partial) {
        (*err_cts)[0] = elmts_in_first;
    }
    if (last_is_partial) {
        (*err_cts)[*num_err_segments - 1] = elmts_in_last;
    }
}

static void free_cts(const size_t num_err_segments, size_t **err_start_idxs,
    size_t *err_cts) {
    int seg;
    for (seg = 0; seg < num_err_segments; ++seg) {
        free(err_start_idxs[seg]);
    }
    free(err_start_idxs);
    free(err_cts);
}


GDS_status_t gdsi_vaddr_to_gds(void *vaddr, size_t err_len,
    size_t *n_responses, struct gdsi_vaddr_to_gds_response **responses) {
    int myrank;
    int n_hits;
    *n_responses = 0;
    *responses = NULL;
    GDS_comm_rank(GDS_COMM_WORLD, &myrank);
    struct gdsi_target_vaddr request = { .vaddr = vaddr, .len = err_len };
    gdsi_dprintf(GDS_DEBUG_INFO,
        "Sending TGTREQ_VADDR_TO_GDS to %d\n", myrank);
    gdsi_send_target_request(&request, sizeof(request), MPI_BYTE, myrank,
        GDS_TGTREQ_VADDR_TO_GDS);
    GDSI_MPI_CALL(MPI_Recv(&n_hits, sizeof(n_hits), MPI_INT, myrank,
        GDS_TGTREP_VADDR_TO_GDS_CT, gdsi_comm_world, MPI_STATUS_IGNORE));
    struct gdsi_id_and_offset hits[n_hits];
    GDSI_MPI_CALL(MPI_Recv(hits, sizeof(*hits) * n_hits, MPI_BYTE, myrank,
        GDS_TGTREP_VADDR_TO_GDS_RES, gdsi_comm_world, MPI_STATUS_IGNORE));
    int i;
    struct gdsi_gds_common *common;
    for (i = 0; i < n_hits; ++i) {
        int array_id = hits[i].array_id;
        size_t offset = hits[i].offset;
        size_t len = hits[i].len;
        gdsi_get_common_by_array_id(array_id, &common);
        int typelen;
        int dim;
        GDSI_MPI_CALL(MPI_Type_size(common->type, &typelen));
        GDS_size_t ndims = common->ndims;
        struct gdsi_vaddr_to_gds_response *resp;
        size_t flat_start_idx = offset / typelen;
        /* ceiling division! Check out
         * http://stackoverflow.com/questions/2745074/
         * fast-ceiling-of-an-integer-division-in-c-c */
        size_t flat_end_idx_plus_1 = (len + offset + typelen - 1) / typelen;
        size_t err_ct = flat_end_idx_plus_1 - flat_start_idx;
        size_t resp_size = sizeof(*resp) + ndims * sizeof(*resp->start);
        if (common->flavor == GDS_FLAVOR_ALLOC) {
            size_t num_err_segments;
            size_t **err_start_idxs;
            size_t *err_cts;
            reverse_idx(ndims, common->nelements_local, flat_start_idx, err_ct,
                &num_err_segments, &err_start_idxs, &err_cts);
            int seg;
            for (seg = 0; seg < num_err_segments; ++seg) {
                resp = malloc(resp_size);
                resp->common = common;
                resp->ct = err_cts[seg];
                if (common->order == GDSI_ORDER_ROW_MAJOR) {
                    for (dim = 0; dim < ndims; ++dim) {
                        resp->start[dim] = err_start_idxs[seg][dim] +
                            common->offset_local[dim];
                    }
                } else { // reverse indices returned if col major
                    for (dim = 0; dim < ndims; ++dim) {
                        resp->start[ndims - dim - 1] = err_start_idxs[seg][dim] +
                            common->offset_local[dim];
                    }
                }
                SGLIB_LIST_ADD(gdsi_vaddr_to_gds_response, *responses, resp, next);
                ++*n_responses;
            }
            free_cts(num_err_segments, err_start_idxs, err_cts);
        } else {
            int cell;
            for (cell = 0; cell < err_ct; ++cell) {
                resp = malloc(resp_size);
                resp->common = common;
                resp->ct = 1;
                GDS_size_t global_indices[ndims];
                common->local_to_global_func(flat_start_idx + cell, global_indices);
                for (dim = 0; dim < ndims; ++dim) {
                    resp->start[dim] = global_indices[dim];
                }
                SGLIB_LIST_ADD(gdsi_vaddr_to_gds_response, *responses, resp, next);
                ++*n_responses;
            }
        }
    }
    return GDS_STATUS_OK;
}

#ifdef GDS_CONFIG_USE_LRDS
static void lrds_cb(const struct lrds_fault_memory_desc* desc) {
    size_t n_responses;
    struct gdsi_vaddr_to_gds_response *responses;

    gdsi_vaddr_to_gds(desc->start, desc->size, &n_responses, &responses);
    if (n_responses == 0) { // error did not affect a gds
        return;
    }
    long is_reusable = (desc->type == LRDS_FAULT_MEMORY_SOFT);

    gdsi_dprintf(GDS_DEBUG_INFO, "LRDS invoked callback: start %p, size %zd\n",
        desc->start, desc->size);
    gdsi_dprintf(GDS_DEBUG_INFO, "n_responses: %zu\n", n_responses);

    int i;
    for (i = 0; i < n_responses; ++i) {
        int dim;
        struct gdsi_gds_common *common = responses->common;
        size_t ndims = common->ndims;

        int rank;
        GDS_comm_rank(common->comm, &rank);
        gdsi_dprintf(GDS_DEBUG_INFO, "array_id: %d, start: ",
            common->array_ids[rank]);
        for (dim = 0; dim < ndims; ++dim) {
            gdsi_dprintf(GDS_DEBUG_INFO, "%zu ", responses->start[dim]);
        }
        gdsi_dprintf(GDS_DEBUG_INFO, "ct: %zu\n", responses->ct);

        GDS_gds_t tmp_gds;
        GDS_error_t error_desc;
        GDS_create_error_descriptor(&error_desc);
        long memory_offset[ndims];
        long memory_ct[ndims];
        for (dim = 0; dim < ndims; ++dim) {
            memory_offset[dim] = responses->start[dim];
            memory_ct[dim] = 1;
        }
        if (common->order == GDSI_ORDER_ROW_MAJOR) {
            memory_ct[ndims - 1] = responses->ct;
        } else {
            memory_ct[0] = responses->ct;
        }
        GDS_add_error_attr(error_desc, GDS_EATTR_GDS_INDEX,
            sizeof(memory_offset), memory_offset);
        GDS_add_error_attr(error_desc, GDS_EATTR_GDS_COUNT,
            sizeof(memory_ct), memory_ct);
        GDS_add_error_attr(error_desc, GDS_EATTR_MEMORY_LOCATION_REUSABLE,
            sizeof(is_reusable), &is_reusable); 
        gdsi_clone_from_gds_common(common, &tmp_gds);
        GDS_raise_local_error(tmp_gds, error_desc);
        GDS_free(&tmp_gds);
        struct gdsi_vaddr_to_gds_response *next = responses->next;
        free(responses);
        responses = next;
    }
}

GDS_status_t gdsi_register_lrds_callback(struct gdsi_gds_common *common) {
    /* use GDS_access to find our internal buffer */
    common->lrds_buf = NULL;
    GDS_size_t ndims = common->ndims;
    GDS_size_t lo[ndims];
    GDS_size_t hi[ndims];
    if (common->flavor == GDS_FLAVOR_ALLOC) {
        int dim;
        if (common->order == GDSI_ORDER_ROW_MAJOR) {
            for (dim = 0; dim < ndims; ++dim) {
                lo[dim] = common->offset_local[dim];
                hi[dim] = common->offset_local[dim] + common->nelements_local[dim] - 1;
            }
        } else {
            for (dim = 0; dim < ndims; ++dim) {
                size_t target_dim = ndims - dim - 1;
                lo[dim] = common->offset_local[target_dim];
                hi[dim] = common->offset_local[target_dim] +
                    common->nelements_local[target_dim] - 1;
            }
        }
    }
    void *buf;
    GDS_gds_t tmp_gds;
    gdsi_clone_from_gds_common(common, &tmp_gds);
    GDS_access_handle_t handle;
    GDS_access(tmp_gds, lo, hi, GDS_ACCESS_BUFFER_DIRECT, &buf, &handle);
    if (buf == NULL) {
        /* Presumably, there is no buffer residing on this process, so we don't
         * register the handler */
        GDS_free(&tmp_gds);
        return GDS_STATUS_OK;
    }

    /* length of the local buffer is cached in common */
    int typelen;
    GDSI_MPI_CALL(MPI_Type_size(common->type, &typelen));
    size_t len = typelen * common->nelements_local_flat;

    common->lrds_buf = buf;
    common->lrds_len = len;
    lrds_memory_set_fault_cb(buf, len, lrds_cb);
    GDS_release(handle);
    GDS_free(&tmp_gds);
    return GDS_STATUS_OK;
}

GDS_status_t gdsi_unregister_lrds_callback(struct gdsi_gds_common *common) {
    if (common->lrds_buf != NULL) {
        lrds_memory_set_fault_cb(common->lrds_buf, common->lrds_len, NULL);
    }
    return GDS_STATUS_OK;
}

#endif /* GDS_CONFIG_USE_LRDS */

enum gdsi_logput_state {
    GDSI_LOGPUT_ISSUED,
    GDSI_LOGPUT_SENT_REQ,
    GDSI_LOGPUT_DONE,
};

struct logput_workq {
    struct gdsi_nbworkq_entry wq; /* This must be at the head */
    enum gdsi_logput_state state;

    struct gdsi_target_put lw;
    GDS_status_t reply;
    int target_rank;

    MPI_Request req_req;

    int total_reqs;
    int outstanding_reqs;
    int waiting_i;
    MPI_Request reqs_data[];
};

static int logput_ready(struct gdsi_nbworkq_entry *wqe, MPI_Status *status)
{
    struct logput_workq *wq = (struct logput_workq *) wqe;
    int i, flag;

    switch (wq->state) {
    case GDSI_LOGPUT_ISSUED:
        wq->state = GDSI_LOGPUT_SENT_REQ;
        wq->waiting_i = 0;
        goto test;

    case GDSI_LOGPUT_SENT_REQ:
    restart:
        wq->outstanding_reqs--;
        gdsi_assert(wq->outstanding_reqs >= 0);

        gdsi_assert(wq->waiting_i < wq->total_reqs);
        wq->reqs_data[wq->waiting_i] = MPI_REQUEST_NULL;
        if (wq->outstanding_reqs == 0) {
            wq->state = GDSI_LOGPUT_DONE;
            gdsi_dyntag_free(gdsi_comm_world, wq->target_rank,
                wq->lw.tag_status);
            gdsi_dyntag_free(gdsi_comm_world, wq->target_rank,
                wq->lw.tag_data);
            return 1;
        }
    test:
        for (i = wq->waiting_i; i < wq->total_reqs; i++) {
            if (wq->reqs_data[i] == MPI_REQUEST_NULL)
                continue;

            wq->waiting_i = i;
            GDSI_MPI_CALL(MPI_Test(&wq->reqs_data[i], &flag, status));
            if (flag)
                goto restart;
            else
                break;
        }

        return 0;

    case GDSI_LOGPUT_DONE:
        gdsi_dprintf(GDS_DEBUG_INFO, "Why coming here??\n");
        return 1;
    }

    gdsi_panic("Should not reach here!\n");

    return 0;
}

static MPI_Request *logput_request(struct gdsi_nbworkq_entry *wqe)
{
    struct logput_workq *wq = (struct logput_workq *) wqe;

    int i;

    switch (wq->state) {
    case GDSI_LOGPUT_ISSUED:
        gdsi_target_signal_thread(wq->target_rank);
        return &wq->req_req;

    case GDSI_LOGPUT_SENT_REQ:
        for (i = wq->waiting_i; i < wq->total_reqs; i++) {
            if (wq->reqs_data[i] != MPI_REQUEST_NULL) {
                wq->waiting_i = i;
                return &wq->reqs_data[i];
            }
        }
        gdsi_panic("Should not reach here!\n");
        return NULL;

    case GDSI_LOGPUT_DONE:
        gdsi_dprintf(GDS_DEBUG_INFO, "Why coming here?\n");
        return NULL;
    }

    gdsi_panic("Should not reach here!\n");

    return 0;
}

static int logput_test(struct gdsi_nbworkq_entry *wqe)
{
    struct logput_workq *wq = (struct logput_workq *) wqe;
    MPI_Request *req;
    MPI_Status status;
    int flag;

    switch (wq->state) {
    case GDSI_LOGPUT_ISSUED:
    case GDSI_LOGPUT_SENT_REQ:
        req = logput_request(wqe);
        GDSI_MPI_CALL(MPI_Test(req, &flag, &status));
        if (flag)
            return logput_ready(wqe, &status);
        return 0;

    case GDSI_LOGPUT_DONE:
        return 1;
    }

    gdsi_panic("Should not reach here!\n");

    return 0;
}

GDS_status_t gdsi_indirect_put(GDS_gds_t gds, int target_rank,
    const void *buf, size_t offset, size_t count,
    bool acc_mode, MPI_Op acc_op, struct gdsi_nbworkq_entry **wqp)
{
    int unit_size;
    MPI_Datatype mpi_type = (MPI_Datatype) gds->common->type;
    size_t n_blocks = 1, pad_front = 0, i, sent = 0, len;
    struct logput_workq *wq;
    size_t wq_len;

    gdsi_assert(wqp);

    MPI_Type_size(mpi_type, &unit_size);

    len = count * unit_size;

    if (gds->common->log_user_bs > 0) {
        n_blocks = log_required_blocks(offset * unit_size, len,
            gds->common->log_user_bs);
        pad_front = offset * unit_size
            - log_aligned_prev(offset * unit_size, gds->common->log_user_bs);
    }

    wq_len = sizeof(*wq) + sizeof(MPI_Request) * (n_blocks + 1);
    wq = malloc(wq_len);
    if (wq == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to allocate workqueue object\n");
        return GDS_STATUS_NOMEM;
    }

    gdsi_nbworkq_init(&wq->wq, gds, logput_test, logput_request, logput_ready);

    wq->state         = GDSI_LOGPUT_ISSUED;
    wq->lw.array_id   = gdsi_get_array_id(gds->common, target_rank);
    wq->lw.offset     = offset;
    wq->lw.count      = count;
    wq->lw.tag_status = gdsi_dyntag_alloc(gdsi_comm_world, target_rank);
    wq->lw.tag_data   = gdsi_dyntag_alloc(gdsi_comm_world, target_rank);
    wq->lw.acc_mode   = acc_mode;
    wq->lw.acc_op     = acc_op;
    wq->lw.acc_type   = mpi_type;
    wq->total_reqs    = 0;
    wq->outstanding_reqs = 0;
    wq->waiting_i     = 0;
    wq->target_rank   = target_rank;

    /* TODO: graceful recovery */
    gdsi_assert(wq->lw.tag_status >= 0);
    gdsi_assert(wq->lw.tag_data >= 0);

    gdsi_isend_target_request(&wq->lw, sizeof(wq->lw), MPI_BYTE, target_rank,
        GDS_TGTREQ_PUT, &wq->req_req);
    gdsi_dprintf(GDS_DEBUG_INFO, "sent GDS_TGTREQ_PUT"
        " {array_id=%d, offset=%zu, count=%zu, tag_status=%d,"
        " tag_data=%d} to target %d\n",
        wq->lw.array_id, wq->lw.offset, wq->lw.count, wq->lw.tag_status,
        wq->lw.tag_data, target_rank);

    /* Packetize subject to block boundary */
    for (i = 0; i < n_blocks && sent < len; i++) {
        size_t send_len;
        size_t rem = len - sent;

        if (gds->common->log_user_bs > 0 || acc_mode)
            /* Make it unit_size aligned */
            send_len = (GDSI_TARGET_BUFSIZE - pad_front) / unit_size * unit_size;
        else
            send_len = rem;

        if (rem < send_len)
            send_len = rem;

        gdsi_dprintf(GDS_DEBUG_INFO,
            "Posting Isend for block[%zu] to rank %d: %zu bytes\n",
            i, target_rank, send_len);

        GDSI_MPI_CALL(MPI_Isend(buf + sent, send_len, MPI_BYTE, target_rank,
                wq->lw.tag_data, gdsi_comm_world, &wq->reqs_data[i]));

        pad_front = 0;
        sent += send_len;
    }

    GDSI_MPI_CALL(MPI_Irecv(&wq->reply, 1, MPI_INT, target_rank,
            wq->lw.tag_status, gdsi_comm_world, &wq->reqs_data[i++]));

    gdsi_assert(n_blocks + 1 >= i);
    wq->total_reqs = wq->outstanding_reqs = i;

    *wqp = &wq->wq;

    return GDS_STATUS_OK;
}

enum gdsi_logalloc_state {
    GDS_LOGALLOC_ISSUED,
    GDS_LOGALLOC_SENT_REQ,
    GDS_LOGALLOC_RCVD_STATUS,
    GDS_LOGALLOC_DONE,
};

struct logalloc_workq {
    struct gdsi_nbworkq_entry wq; /* This must be at the head */
    enum gdsi_logalloc_state state;

    struct gdsi_target_put lw;
    GDS_status_t reply;
    int target_rank;

    MPI_Request req_send;
    MPI_Request reqs_recv[2];
};

static int logalloc_ready(struct gdsi_nbworkq_entry *wqe, MPI_Status *status)
{
    struct logalloc_workq *wq = (struct logalloc_workq *) wqe;

    switch (wq->state) {
    case GDS_LOGALLOC_ISSUED:
        wq->state = GDS_LOGALLOC_SENT_REQ;
        return 0;

    case GDS_LOGALLOC_SENT_REQ:
        wq->state = GDS_LOGALLOC_RCVD_STATUS;
        return 0;

    case GDS_LOGALLOC_RCVD_STATUS:
        wq->state = GDS_LOGALLOC_DONE;
        return 1;

    case GDS_LOGALLOC_DONE:
        gdsi_dprintf(GDS_DEBUG_INFO, "Why coming here??\n");
        return 1;
    }

    gdsi_panic("Should not reach here!\n");

    return 0;
}

static MPI_Request *logalloc_request(struct gdsi_nbworkq_entry *wqe)
{
    struct logalloc_workq *wq = (struct logalloc_workq *) wqe;

    switch (wq->state) {
    case GDS_LOGALLOC_ISSUED:
        gdsi_target_signal_thread(wq->target_rank);
        return &wq->req_send;

    case GDS_LOGALLOC_SENT_REQ:
        return &wq->reqs_recv[0];

    case GDS_LOGALLOC_RCVD_STATUS:
        return &wq->reqs_recv[1];

    case GDS_LOGALLOC_DONE:
        gdsi_dprintf(GDS_DEBUG_INFO, "Why coming here?\n");
        return NULL;
    }

    gdsi_panic("Should not reach here!\n");

    return 0;
}

static int logalloc_test(struct gdsi_nbworkq_entry *wqe)
{
    struct logalloc_workq *wq = (struct logalloc_workq *) wqe;
    MPI_Status status;
    int flag;

    if (wq->state == GDS_LOGALLOC_DONE)
        return 1;

    GDSI_MPI_CALL(MPI_Test(logalloc_request(wqe), &flag, &status));
    if (flag)
        return logalloc_ready(wqe, &status);
    else
        return 0;
}

GDS_status_t gdsi_log_alloc(GDS_gds_t gds, int target_rank,
    size_t offset, size_t count, struct gdsi_nbworkq_entry **wqp)
{
    int unit_size;
    MPI_Datatype mpi_type = (MPI_Datatype) gds->common->type;
    size_t n_blocks, len;
    struct logalloc_workq *wq;
    gdsi_log_md_t *mdc;

    gdsi_assert(wqp);

    MPI_Type_size(mpi_type, &unit_size);

    len = count * unit_size;

    n_blocks = log_required_blocks(offset * unit_size, len,
        gds->common->log_user_bs);

    wq = malloc(sizeof(*wq));
    if (wq == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to allocate workqueue object\n");
        return GDS_STATUS_NOMEM;
    }

    gdsi_nbworkq_init(&wq->wq, gds,
        logalloc_test, logalloc_request, logalloc_ready);

    wq->state         = GDS_LOGALLOC_ISSUED;
    wq->lw.array_id   = gdsi_get_array_id(gds->common, target_rank);
    wq->lw.offset     = offset;
    wq->lw.count      = count;
    wq->lw.tag_status = gdsi_dyntag_alloc(gdsi_comm_world, target_rank);
    wq->lw.tag_data   = gdsi_dyntag_alloc(gdsi_comm_world, target_rank);
    wq->target_rank   = target_rank;

    /* TODO: graceful recovery */
    gdsi_assert(wq->lw.tag_status >= 0);
    gdsi_assert(wq->lw.tag_data >= 0);


    gdsi_isend_target_request(&wq->lw, sizeof(wq->lw), MPI_BYTE, target_rank,
        GDS_TGTREQ_LOG_ALLOC, &wq->req_send);
    gdsi_dprintf(GDS_DEBUG_INFO, "sent GDS_TGTREQ_LOG_ALLOC"
        " {array_id=%d, offset=%zu, count=%zu, tag_status=%d"
        "} to target %d\n",
        wq->lw.array_id, wq->lw.offset, wq->lw.count, wq->lw.tag_status,
        target_rank);

    GDSI_MPI_CALL(MPI_Irecv(&wq->reply, 1, MPI_INT, target_rank,
            wq->lw.tag_status, gdsi_comm_world, &wq->reqs_recv[0]));
    mdc = gdsi_log_get_md_cache(gds->common, target_rank, offset);
    n_blocks = log_required_blocks(offset * unit_size, count * unit_size,
        gds->common->log_user_bs);
    GDSI_MPI_CALL(MPI_Irecv(mdc, n_blocks, MPI_UINT32_T, target_rank,
            wq->lw.tag_data, gdsi_comm_world, &wq->reqs_recv[1]));

    *wqp = &wq->wq;

    return GDS_STATUS_OK;
}

bool gdsi_log_md_is_fresh(const struct gdsi_gds_common *common,
    int target_rank, gdsi_log_md_t md)
{
    gdsi_dprintf(GDS_DEBUG_INFO,
        "target_rank=%d md_head=0x%08x md=0x%08x\n",
        target_rank, common->log_cur_md_heads[target_rank], md);

    return md > common->log_cur_md_heads[target_rank];
}

bool gdsi_log_md_is_fresh_any(const struct gdsi_gds_common *common,
    int target_rank, gdsi_log_md_t *mdc_start, int n_blocks)
{
    int i;
    for (i = 0 ; i < n_blocks; i++)
        if (gdsi_log_md_is_fresh(common, target_rank, mdc_start[i]))
            return true;
    return false;
}

gdsi_log_md_t *gdsi_log_get_md_cache(struct gdsi_gds_common *common,
    int target_rank, size_t target_offset)
{
    int unit_size;
    size_t blk_off;

    MPI_Type_size(common->type, &unit_size);

    blk_off = target_offset * unit_size / common->log_user_bs;

    gdsi_assert(common->log_md_cache);
    gdsi_assert(blk_off < common->log_md_recvcounts[target_rank]);

    return &common->log_md_cache[common->log_md_displs[target_rank] + blk_off];
}

size_t gdsi_log_md_index(struct gdsi_gds_common *common,
    int target_rank, size_t target_offset)
{
    int unit_size;
    size_t bs = common->log_user_bs;
    size_t blk_off;
    size_t mdh;

    MPI_Type_size(common->type, &unit_size);

    blk_off = target_offset * unit_size / common->log_user_bs;

    gdsi_assert(blk_off < common->log_md_recvcounts[target_rank]);

    /* Log-structured array uses byte addressing */
    mdh = log_md_to_byteoff(common->log_cur_md_heads[target_rank], bs);
    return mdh + blk_off * sizeof(gdsi_log_md_t);
}

void gdsi_log_flush_enqueue(struct gdsi_gds_common *common,
    int target_rank, gdsi_log_md_t blk)
{
    /* TODO: multi-threading */
    struct gdsi_log_flush_queue *q = &common->log_flush_queues[target_rank];

    if (q->n_blocks >= GDS_LOG_FLUSH_MAXENTS) {
        MPI_Waitall(2, q->reqs, MPI_STATUSES_IGNORE);
        q->n_blocks = 0;
        gdsi_dyntag_free(gdsi_comm_world, target_rank, q->lf.tag);
    }

    q->blocks[q->n_blocks++] = blk;
    if (q->n_blocks >= GDS_LOG_FLUSH_MAXENTS) {
        q->lf.array_id = gdsi_get_array_id(common, target_rank);
        q->lf.tag      = gdsi_dyntag_alloc(gdsi_comm_world, target_rank);
        gdsi_isend_target_request(&q->lf, sizeof(q->lf), MPI_BYTE, target_rank,
            GDS_TGTREQ_LOG_FLUSH, &q->reqs[0]);
        gdsi_isend_target_request(q->blocks, q->n_blocks, MPI_UINT32_T,
            target_rank, q->lf.tag, &q->reqs[1]);
        gdsi_target_signal_thread(target_rank);
    }
}

/* TODO: These structures should be per-communicator/target basis */
static pthread_mutex_t dyntag_lock = PTHREAD_MUTEX_INITIALIZER;
static struct gdsi_bitvec *dyntag_bv;
static unsigned long dyntag_max = 32767; /* TODO: should query MPI_TAG_UB */

GDS_status_t gdsi_dyntag_init(MPI_Comm comm)
{
    dyntag_bv = gdsi_bitvec_new(dyntag_max - GDS_TGTREP_DYNAMIC_BEGIN);
    if (dyntag_bv == NULL)
        return GDS_STATUS_NOMEM;

    gdsi_bitvec_set_all(dyntag_bv);

    return GDS_STATUS_OK;
}

int gdsi_dyntag_alloc(MPI_Comm comm, int target_rank)
{
    int r;
    pthread_mutex_lock(&dyntag_lock);
    r = gdsi_bitvec_ffs(dyntag_bv, 0);
    if (r < 0)
        goto out;
    gdsi_bitvec_clear(dyntag_bv, r);
    pthread_mutex_unlock(&dyntag_lock);
    r += GDS_TGTREP_DYNAMIC_BEGIN;
out:
    return r;
}

void gdsi_dyntag_free(MPI_Comm comm, int target_rank, int tag)
{
    int b;
    gdsi_assert(tag >= GDS_TGTREP_DYNAMIC_BEGIN);
    gdsi_assert(tag < dyntag_max);

    b = tag - GDS_TGTREP_DYNAMIC_BEGIN;
    pthread_mutex_lock(&dyntag_lock);
    gdsi_bitvec_set(dyntag_bv, b);
    pthread_mutex_unlock(&dyntag_lock);
}

void gdsi_dyntag_finalize(MPI_Comm comm)
{
    gdsi_bitvec_delete(dyntag_bv);
}

void gdsi_append_get(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds)
{
    struct gdsi_get_info *newinfo;
    int ndims, i;

    newinfo = calloc(sizeof(*newinfo), 1);
    ndims = gds->common->ndims;
    if (ndims == 1) {
        newinfo->origin_ld = NULL;
    } else {
        newinfo->origin_ld = calloc(sizeof(GDS_size_t), ndims - 1);
    }
    newinfo->lo_index = calloc(sizeof(GDS_size_t), ndims);
    newinfo->hi_index = calloc(sizeof(GDS_size_t), ndims);

    newinfo->origin_addr = origin_addr;
    for (i = 0; i < ndims - 1; ++i) {
        newinfo->origin_ld[i] = origin_ld[i];
        newinfo->lo_index[i] = lo_index[i];
        newinfo->hi_index[i] = hi_index[i];
    }
    newinfo->lo_index[ndims - 1] = lo_index[ndims - 1];
    newinfo->hi_index[ndims - 1] = hi_index[ndims - 1];

    newinfo->next = gds->common->get_info;
    gds->common->get_info = newinfo;
}

void gdsi_destroy_get_info(struct gdsi_get_info **get_info) 
{
    struct gdsi_get_info *next;
    
    while (*get_info != 0) {
        next = (*get_info)->next;
        free(*get_info);
        *get_info = next;
    }

}
