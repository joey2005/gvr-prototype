/*
   Global View Resilience (GVR)
http://gvr.cs.uchicago.edu

Copyright (C) 2014 University of Chicago.
See license.txt in top-level directory.

This module implements the Global View Service (GVS).
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <gds.h>

#include "sglib.h"
#include "gds_internal.h"
#include <config.h>
#include "version.h"

#include <assert.h>

#define local_error_poll_handle(_gds)                                   \
    do {                                                                \
        int _error_flag;                                                \
        poll_local_error(_gds->common, &_error_flag);                   \
        if (_error_flag) {                                              \
            gdsi_dprintf(GDS_DEBUG_INFO, "local error\n");              \
            GDS_invoke_local_error_handler(_gds);                       \
        }                                                               \
    } while (0)

int gdsi_thread_support;
int gdsi_crash_count;

GDS_comm_t GDS_COMM_WORLD;

static int need_to_cleanup_mpi = 1;

int gdsi_dprintf_myrank;
static bool skip_cleanup;

/*
   A list which holds all the live gds objects
 * should be protected by gdsi_gds_common_list_lock
 * reference from gdsi_gds_common_list do not increment common->refcount,
 as the lifetime of the common object matches the duration of reference
 */
struct gdsi_gds_common *gdsi_gds_common_list;
pthread_rwlock_t gdsi_gds_common_list_lock;

/* Pre-defined error attributes */
GDS_error_attr_key_t GDS_EATTR_MEMORY_VADDR;
GDS_error_attr_key_t GDS_EATTR_MEMORY_SIZE;
GDS_error_attr_key_t GDS_EATTR_MEMORY_AFFECTED_SIZE;
GDS_error_attr_key_t GDS_EATTR_MEMORY_N_RANGES;
GDS_error_attr_key_t GDS_EATTR_MEMORY_DATA;
GDS_error_attr_key_t GDS_EATTR_MEMORY_CHECK_BITS;
GDS_error_attr_key_t GDS_EATTR_MEMORY_LOCATION_REUSABLE;

GDS_error_attr_key_t GDS_EATTR_APP;
GDS_error_attr_key_t GDS_EATTR_DETECTED_BY;

GDS_error_attr_key_t GDS_EATTR_GDS_INDEX;
GDS_error_attr_key_t GDS_EATTR_GDS_COUNT;
GDS_error_attr_key_t GDS_EATTR_GDS_AFFECTED_COUNT;
GDS_error_attr_key_t GDS_EATTR_GDS_VERSION;
GDS_error_attr_key_t GDS_EATTR_GDS_LATENCY;

GDS_error_attr_key_t GDS_EATTR_LOST_PROCESSES;
GDS_error_attr_key_t GDS_EATTR_LOST_COMMUNICATOR;
GDS_error_attr_key_t GDS_EATTR_REPLACED_COMMUNICATOR;
GDS_error_attr_key_t GDS_EATTR_LOST_COMPUTATION;
GDS_error_attr_key_t GDS_EATTR_MPI_RANK_UNRESPONSIVE;

struct gdsi_log_stock_queue {
    int head;
    int len;
    int max_len;
    gdsi_log_md_t stocks[];
};

static void win_flush(struct gdsi_gds_common *common)
{
    gdsi_common_lock(common);
    common->n_outstanding_rma_ops = 0;
    gdsi_common_unlock(common);

    MPI_Win_flush_all(common->win);
}

static void win_flush_local(struct gdsi_gds_common *common)
{
    gdsi_common_lock(common);
    common->n_outstanding_rma_ops = 0;
    gdsi_common_unlock(common);

    MPI_Win_flush_local_all(common->win);
}

static void nbworkq_run(struct gdsi_gds_common *common, bool wait_ok, bool rma_flushed);

static void rma_issued(struct gdsi_gds_common *common)
{
    gdsi_common_lock(common);
    ++common->n_outstanding_rma_ops;
    gdsi_common_unlock(common);
}

static void rma_post_check(struct gdsi_gds_common *common)
{
    bool need_flush = false;

    gdsi_common_lock(common);
    if (common->n_outstanding_rma_ops >= gdsi_outstanding_rma_ops_max
            || common->nbworkq_n_ents >= GDSI_NBWORKQ_INFLIGHT_MAX) {
        need_flush = true;
        common->n_outstanding_rma_ops = 0;
    }
    gdsi_common_unlock(common);

    if (need_flush) {
        MPI_Win_flush_all(common->win);
#ifdef GDS_CONFIG_USE_LRDS
        if (common->lrds_dirty_win != MPI_WIN_NULL)
            MPI_Win_flush_all(common->lrds_dirty_win);
#endif
    }
    nbworkq_run(common, false, need_flush);
}

void gdsi_nbworkq_init(struct gdsi_nbworkq_entry *wq, GDS_gds_t gds,
        gdsi_nbworkq_test_t test, gdsi_nbworkq_request_t request,
        gdsi_nbworkq_ready_t ready)
{
    wq->next = wq->prev = NULL;
    wq->gds     = gds;
    wq->test    = test;
    wq->request = request;
    wq->ready   = ready;
    wq->rma     = false;
}

static void nbworkq_rma_init(struct gdsi_nbworkq_entry *wq, GDS_gds_t gds,
        gdsi_nbworkq_test_t test)
{
    gdsi_nbworkq_init(wq, gds, test, NULL, NULL);
    wq->rma = true;
}

static void nbworkq_enqueue(struct gdsi_nbworkq_entry *wq)
{
    struct gdsi_gds_common *common = wq->gds->common;

    gdsi_common_lock(common);
    gdsi_assert(common->nbworkq_n_ents >= 0);
    wq->id = common->nbworkq_counter++;
    SGLIB_DL_LIST_ADD_AFTER(struct gdsi_nbworkq_entry,
            common->nbworkq, wq, prev, next);
    gdsi_dprintf(GDS_DEBUG_INFO, "[%u] queued\n", wq->id);
    common->nbworkq_n_ents++;
    gdsi_common_unlock(common);
}

static void nbworkq_dequeue_nolock(struct gdsi_nbworkq_entry *wq)
{
    struct gdsi_gds_common *common = wq->gds->common;

    gdsi_assert(common->nbworkq_n_ents > 0);

    SGLIB_DL_LIST_DELETE(struct gdsi_nbworkq_entry,
            common->nbworkq, wq, prev, next);
    common->nbworkq_n_ents--;
    if (common->nbworkq_n_ents == 0)
        gdsi_assert(common->nbworkq == NULL);
}

static void nbworkq_run(struct gdsi_gds_common *common, bool wait_ok, bool rma_flushed)
{
    struct gdsi_nbworkq_entry *wq, *next;
    int reallocate, i, readyi;
    bool has_req;
    MPI_Status status;

rerun:
    has_req = false;
    i = 0;

    reallocate = common->nbworkq_n_ents > common->nbworkq_n_ents_max;

    gdsi_dprintf(GDS_DEBUG_INFO, "nbworkq_run: enter\n");

    if (common->nbworkq_n_ents == 0) {
        gdsi_dprintf(GDS_DEBUG_INFO, "nbworkq_run: nothing to do, return\n");
        return;
    }

    /*
TODO: how to deal with thread safety here?
One solution: allow this function being called only from stable points
     */
    for (wq = common->nbworkq; wq != NULL; wq = next) {
        int r = 0;

        if (!wq->rma || rma_flushed)
            r = wq->test(wq);
        next = wq->next;

        if (r) {
            nbworkq_dequeue_nolock(wq);
            free(wq);
        } else if (!reallocate) {
            gdsi_assert(common->nbworkq_reqs);
            if (wq->request) {
                common->nbworkq_reqs[i] = *wq->request(wq);
                has_req = true;
            } else
                common->nbworkq_reqs[i] = MPI_REQUEST_NULL;
            i++;
        }
        wq = next;
    }

    if (!reallocate) {
        if (i < common->nbworkq_n_ents) {
            /* New entries were prepended during the first run
               -- simply retry */
            rma_flushed = false;
            goto rerun;
        } else {
            gdsi_assert(i == common->nbworkq_n_ents);
        }
    }

    if (common->nbworkq_n_ents == 0) {
        gdsi_dprintf(GDS_DEBUG_INFO,
                "nbworkq_run: all the items were processed\n");
        return;
    }

    if (reallocate) {
        int max = common->nbworkq_n_ents * 2;
        common->nbworkq_reqs = realloc(common->nbworkq_reqs,
                sizeof(MPI_Request) * max);

        /* TODO: graceful recovery */
        gdsi_assert(common->nbworkq_reqs);

        common->nbworkq_n_ents_max = max;
        i = 0;
        for (wq = common->nbworkq; wq != NULL; wq = wq->next) {
            if (wq->request) {
                common->nbworkq_reqs[i] = *wq->request(wq);
                has_req = true;
            } else
                common->nbworkq_reqs[i] = MPI_REQUEST_NULL;
            i++;
        }
        gdsi_assert(i == common->nbworkq_n_ents);
    }

    if (!has_req) {
        gdsi_dprintf(GDS_DEBUG_INFO, "nbworkq_run: no wait needed\n");
        return;
    }

    gdsi_dprintf(GDS_DEBUG_INFO, "nbworkq_run: wait for up to %d reqs...\n",
            common->nbworkq_n_ents);
    readyi = -1;
    if (wait_ok)
        GDSI_MPI_CALL(MPI_Waitany(common->nbworkq_n_ents,
                    common->nbworkq_reqs, &readyi, &status));

    for (i = 0, wq = common->nbworkq; wq != NULL; i++, wq = next) {
        int r = 0;
        next = wq->next;

        if (wq->ready) {
            if (i == readyi)
                r = wq->ready(wq, &status);
            else
                r = wq->test(wq);

            if (r) {
                nbworkq_dequeue_nolock(wq);
                free(wq);
            }
        }

        wq = next;
    }

    gdsi_dprintf(GDS_DEBUG_INFO, "nbworkq_run: exit: %d wqs remaining\n",
            common->nbworkq_n_ents);
}

static void snprint_coord(char *buff, size_t len, int ndims, const GDS_size_t lo[], const GDS_size_t hi[])
{
    int i, r;

    for (i = 0; i < ndims; i++) {
        r = snprintf(buff, len, "[%zu]", lo[i]);
        if (len <= r)
            return;
        len -= r;
        buff += r;
    }
    r = snprintf(buff, len, "-");
    if (len <= r)
        return;
    len -= r;
    buff += r;
    for (i = 0; i < ndims; i++) {
        r = snprintf(buff, len, "[%zu]", hi[i]);
        if (len <= r)
            return;
        len -= r;
        buff += r;
    }
}

int gdsi_snprint_index(char *buff, size_t len, size_t ndims, const size_t dims[])
{
    size_t i;
    int r, ret = 0;

    for (i = 0; i < ndims; i++) {
        if (i != ndims - 1)
            r = snprintf(buff + ret, len, "%zu,", dims[i]);
        else
            r = snprintf(buff + ret, len, "%zu", dims[i]);

        ret += r;

        if (len <= r)
            return ret;
        len -= r;
    }

    return ret;
}

struct gdsi_op_args {
    /* Buffers are used by multiple routines */
    void *buf; /* put/get/acc: buf, cmp_&_swap: comp_buf */
    void *buf2; /* cmp_&_swap: swap_src_buf; get_acc: res_buf */
    void *buf3; /* cmp_&_swap: swap_res_buf */
    GDS_op_t op;
};

typedef void (*chunk_lambda_t)(GDS_gds_t gds, size_t local_offset,
        int target_rank, size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args);

typedef void (*chunk_lambda_get_acc_t)(GDS_gds_t gds,
        size_t origin_offset, size_t result_offset,
        int target_rank, size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args);

struct gdsi_chunk_ops {
    chunk_lambda_t get;
    chunk_lambda_t put;
    chunk_lambda_t acc;
    chunk_lambda_t compare_and_swap;
    chunk_lambda_get_acc_t get_acc;
};

static struct gdsi_chunk_ops flat_ops;
static struct gdsi_chunk_ops log_ops;

static void iterate_array_top(GDS_gds_t gds, const size_t lo_index[],
        const size_t hi_index[], const size_t ld[], chunk_lambda_t lambda,
        struct gdsi_op_args *lambda_args);

static void add_to_gdsi_gds_common_list(struct gdsi_gds_common *gds)
{
    pthread_rwlock_wrlock(&gdsi_gds_common_list_lock);
    SGLIB_DL_LIST_ADD_BEFORE(struct gdsi_gds_common, gdsi_gds_common_list,
            gds, prev, next);
    gdsi_gds_common_list = gds;
    pthread_rwlock_unlock(&gdsi_gds_common_list_lock);
}

static void remove_from_gdsi_gds_common_list(struct gdsi_gds_common *gds)
{
    pthread_rwlock_wrlock(&gdsi_gds_common_list_lock);
    SGLIB_DL_LIST_DELETE(struct gdsi_gds_common, gdsi_gds_common_list, gds, prev, next);
    pthread_rwlock_unlock(&gdsi_gds_common_list_lock);
}

static void error_finalize(void);

GDS_status_t GDS_define_error_attr_key(
        const char *attr_name,
        size_t attr_name_size,
        GDS_error_attr_value_type_t value_type,
        GDS_error_attr_key_t *new_keyp)
{
    if (attr_name_size > GDS_ERROR_ATTR_NAME_MAX)
        return GDS_STATUS_INVALID;

    GDS_error_attr_key_t nk = gdsi_define_error_attr_key(attr_name, value_type);
    *new_keyp = nk;
    if (nk == GDS_ERROR_ATTR_KEY_INVALID)
        return GDS_STATUS_TOO_MANY_ENTS;

    return GDS_STATUS_OK;
}

/* Initializes error-related data structures and set up pre-defined
   error categories */
static GDS_status_t error_init(void)
{
    GDS_define_error_attr_key("MEMORY_VADDR", 12, GDS_EAVTYPE_INT,
            &GDS_EATTR_MEMORY_VADDR);
    GDS_define_error_attr_key("MEMORY_SIZE", 11, GDS_EAVTYPE_INT,
            &GDS_EATTR_MEMORY_SIZE);
    GDS_define_error_attr_key("MEMORY_AFFECTED_SIZE", 20,
            GDS_EAVTYPE_INT,
            &GDS_EATTR_MEMORY_AFFECTED_SIZE);
    GDS_define_error_attr_key("MEMORY_N_RANGES", 15, GDS_EAVTYPE_INT,
            &GDS_EATTR_MEMORY_N_RANGES);
    GDS_define_error_attr_key("MEMORY_DATA", 11, GDS_EAVTYPE_BYTE_ARRAY,
            &GDS_EATTR_MEMORY_DATA);
    GDS_define_error_attr_key("MEMORY_CHECK_BITS", 17, GDS_EAVTYPE_BYTE_ARRAY,
            &GDS_EATTR_MEMORY_CHECK_BITS);
    GDS_define_error_attr_key("MEMORY_LOCATION_REUSABLE", 24, GDS_EAVTYPE_INT,
            &GDS_EATTR_MEMORY_LOCATION_REUSABLE);

    GDS_define_error_attr_key("APP", 3, GDS_EAVTYPE_BYTE_ARRAY, 
            &GDS_EATTR_APP);
    GDS_define_error_attr_key("DETECTED_BY", 11, GDS_EAVTYPE_BYTE_ARRAY, 
            &GDS_EATTR_DETECTED_BY);

    GDS_define_error_attr_key("GDS_INDEX", 9, GDS_EAVTYPE_INT_ARRAY, 
            &GDS_EATTR_GDS_INDEX);
    GDS_define_error_attr_key("GDS_COUNT", 9, GDS_EAVTYPE_INT_ARRAY, 
            &GDS_EATTR_GDS_COUNT);
    GDS_define_error_attr_key("GDS_AFFECTED_COUNT", 18,
            GDS_EAVTYPE_INT_ARRAY,
            &GDS_EATTR_GDS_AFFECTED_COUNT);
    GDS_define_error_attr_key("GDS_VERSION", 11, GDS_EAVTYPE_INT, 
            &GDS_EATTR_GDS_VERSION);
    GDS_define_error_attr_key("GDS_LATENCY", 11, GDS_EAVTYPE_INT,
            &GDS_EATTR_GDS_LATENCY);

    GDS_define_error_attr_key("LOST_PROCESSES", 14, GDS_EAVTYPE_MPIOBJ,
            &GDS_EATTR_LOST_PROCESSES);
    GDS_define_error_attr_key("LOST_COMMUNICATOR", 17, GDS_EAVTYPE_MPIOBJ,
            &GDS_EATTR_LOST_COMMUNICATOR);
    GDS_define_error_attr_key("REPLACED_COMMUNICATOR", 22, GDS_EAVTYPE_MPIOBJ,
            &GDS_EATTR_REPLACED_COMMUNICATOR);
    GDS_define_error_attr_key("LOST_COMPUTATION", 16, GDS_EAVTYPE_FLOAT,
            &GDS_EATTR_LOST_COMPUTATION);
    GDS_define_error_attr_key("MPI_RANK_UNRESPONSIVE", 21, GDS_EAVTYPE_INT,
            &GDS_EATTR_MPI_RANK_UNRESPONSIVE);

    return GDS_STATUS_OK;
}

static void error_finalize(void)
{
}

static int error_desc_comparator( GDS_error_t error_desc_1, GDS_error_t error_desc_2 )
{
    if (error_desc_1->rank > error_desc_2->rank)
        return -1;
    if (error_desc_1->rank < error_desc_2->rank)
        return 1;
    if (error_desc_1->rank == error_desc_2->rank) {
        if (error_desc_1->counter > error_desc_2->counter)
            return -1;
        if (error_desc_1->counter < error_desc_2->counter)
            return 1;
    }
    return 0;
}

static void poll_local_error (struct gdsi_gds_common *common, int *error_flag)
{
    gdsi_common_lock(common);

    if (common->local_error_desc_queue != NULL) {
        *error_flag = 1;
    } else {
        *error_flag = 0;
    }
    gdsi_common_unlock(common);
}

static void enqueue_global_error(struct gdsi_gds_common *common,
        GDS_error_t error_desc);

/* reverse the index array for column-major array */
void gdsi_reverse_array(GDS_size_t *array, int array_len)
{
    int lo, hi;
    GDS_size_t temp;

    for (lo = 0, hi = array_len - 1; lo < hi; lo++, hi--) {
        temp = array[hi];
        array[hi] = array[lo];
        array[lo] = temp;
    }
}

static GDS_status_t setup_procfail_edesc(struct gdsi_gds_common *common,
        MPI_Comm old_comm, MPI_Group failed_group)
{
    GDS_status_t ret;
    GDS_error_t desc;
    int64_t n_ranges = 0;
    int i;

    ret = GDS_create_error_descriptor(&desc);
    if (ret != GDS_STATUS_OK)
        return ret;

    ret = GDS_add_error_attr(desc, GDS_EATTR_LOST_PROCESSES,
            sizeof(MPI_Group), &failed_group);
    if (ret != GDS_STATUS_OK)
        goto out_free;

    ret = GDS_add_error_attr(desc, GDS_EATTR_LOST_COMMUNICATOR,
            sizeof(MPI_Comm), &old_comm);
    if (ret != GDS_STATUS_OK)
        goto out_free;

    /* Record lost regions */
    for (i = 0; i < common->nchunk_descs; i++)
        if (common->chunk_sets->chunk_descs[i].is_corrupted)
            n_ranges++;
    ret = GDS_add_error_attr(desc, GDS_EATTR_MEMORY_N_RANGES,
            sizeof(int64_t), &n_ranges);
    if (ret != GDS_STATUS_OK)
        goto out_free;

    {
        GDS_size_t offsets[common->ndims * n_ranges];
        GDS_size_t counts[common->ndims * n_ranges];
        int nc = 0;

        for (i = 0; i < common->nchunk_descs; i++) {
            const struct gdsi_chunk_desc *cd;

            cd = common->chunk_sets->chunk_descs + i;
            if (!cd->is_corrupted)
                continue;

            gdsi_dprintf(GDS_DEBUG_INFO,
                    "cn=%d: cd->global_index_flat=%zu, cd->size_flat=%zu\n",
                    i, cd->global_index_flat, cd->size_flat);
            gdsi_decompose_index(cd->global_index_flat,
                    common, &offsets[common->ndims * nc]);
            gdsi_decompose_size(cd->size_flat,
                    common, &counts[common->ndims * nc]);

            if (common->order == GDSI_ORDER_COL_MAJOR) {
                gdsi_reverse_array(&offsets[common->ndims * nc], common->ndims);
                gdsi_reverse_array(&counts[common->ndims * nc], common->ndims);
            }

            nc++;
        }

        gdsi_assert(nc == n_ranges);

        /* Debug print */
        for (i = 0; i < common->ndims * n_ranges; i++) {
            gdsi_dprintf(GDS_DEBUG_INFO,
                    "offsets[%d]=%zu, counts[%d]=%zu\n",
                    i, offsets[i], i, counts[i]);
        }

        ret = GDS_add_error_attr(desc, GDS_EATTR_GDS_INDEX,
                sizeof(GDS_size_t) * common->ndims * n_ranges, offsets);
        if (ret != GDS_STATUS_OK)
            goto out_free;
        ret = GDS_add_error_attr(desc, GDS_EATTR_GDS_COUNT,
                sizeof(GDS_size_t) * common->ndims * n_ranges, counts);
        if (ret != GDS_STATUS_OK)
            goto out_free;
    }

    /* Clear error flag for all chunks */
    for (i = 0; i < common->nchunk_descs; i++)
        common->chunk_sets->chunk_descs[i].is_corrupted = false;

    enqueue_global_error(common, desc);

    return GDS_STATUS_OK;

out_free:
    gdsi_dprintf(GDS_DEBUG_ERR, "Adding error attribute failed\n");
    GDS_free_error_descriptor(&desc);
    return ret;
}

/* Poll the global errors raised by different processes */
static GDS_status_t poll_global_error(struct gdsi_gds_common *common,
        int *error_flag)
{
    GDS_status_t ret = GDS_STATUS_OK;
    int nprocs;
    int myrank;
    int i, j;
    int n_global_errors;

    *error_flag = 0;

    GDS_comm_size(common->comm, &nprocs);
    GDS_comm_rank(common->comm, &myrank);

    if (nprocs == 1) {
        if (common->global_error_desc_queue != NULL)
            *error_flag = 1;

        return GDS_STATUS_OK;
    }

    gdsi_common_lock(common);

    /* get number of error desc in the queue */
    SGLIB_LIST_LEN(struct GDS_error, common->global_error_desc_queue,
            next, n_global_errors);

    gdsi_common_unlock(common);

    size_t error_desc_flat_size; /* Size of flattend representation
                                    of error descriptor */
    struct gdsi_error_desc_flat *flat = NULL;
    GDS_error_t error_desc_node = common->global_error_desc_queue;

    /* bit vector. bit i is set if process i raised error(s) */
    struct gdsi_bitvec *bv = gdsi_bitvec_new(nprocs);
    if (bv == NULL)
        return GDS_STATUS_NOMEM;
    uint8_t *bitvec_raw = gdsi_bitvec_raw_buffer(bv);
    size_t bitvec_size  = gdsi_bitvecl_size(nprocs);

    if (common->global_error_desc_queue != NULL)
        gdsi_bitvec_set(bv, myrank); /* myrank has raised error(s) */

    GDSI_MPI_CALL(MPI_Allreduce(MPI_IN_PLACE, bitvec_raw, bitvec_size,
                MPI_BYTE, MPI_BOR, common->comm));

    for (i = gdsi_bitvec_ffs(bv, 0); 0 <= i; i = gdsi_bitvec_ffs(bv, i+1)) {
        /* process i has raised error(s) */
        *error_flag = 1;

        /* other processes need to know the number of errors */
        GDSI_MPI_CALL(MPI_Bcast(&n_global_errors, 1, MPI_INT, i,
                    common->comm));

        for (j = 0; j < n_global_errors; j++) {
            if (myrank == i){
                error_desc_node = common->global_error_desc_queue;

                /* other processes need to know size of each error desc */
                flat = gdsi_flatten_error_desc(error_desc_node);
                if (flat == NULL) {
                    /* TODO: if we get out of here, what happens to other
                       processes? */
                    ret = GDS_STATUS_NOMEM;
                    goto out;
                }
                error_desc_flat_size = gdsi_error_desc_flat_size(error_desc_node);
                gdsi_dprintf(GDS_DEBUG_INFO, "flat->n_attrs=%d\n",
                        flat->n_attrs);
            }

            GDSI_MPI_CALL(MPI_Bcast(&error_desc_flat_size,
                        sizeof(size_t), MPI_BYTE, i,
                        common->comm));
            gdsi_dprintf(GDS_DEBUG_INFO, "error_desc_flat_size=%zu\n",
                    error_desc_flat_size);

            if (myrank != i) {
                flat = malloc(error_desc_flat_size);
                if (flat == NULL) {
                    ret = GDS_STATUS_NOMEM;
                    goto out;
                }
            }

            GDSI_MPI_CALL(MPI_Bcast(flat, error_desc_flat_size,
                        MPI_BYTE, i, common->comm));
            gdsi_dprintf(GDS_DEBUG_INFO, "flat->n_attrs=%d\n",
                    flat->n_attrs);

            if (myrank != i) {
                GDS_error_t desc = gdsi_expand_error_desc(flat);

                gdsi_common_lock(common);

                /* sorted by [rank, id], high value is on the head*/
                SGLIB_SORTED_LIST_ADD(struct GDS_error,
                        common->global_error_desc_queue,
                        desc, error_desc_comparator, next);

                gdsi_common_unlock(common);
            }

            free(flat);

            if (myrank == i)
                error_desc_node = error_desc_node->next;
        }
    }

    /* Special case to make proc failure work */
    if (common->simulated_failed_proc >= 0) {
        MPI_Group failed_group, new_owner_group;
        MPI_Comm old_comm = common->comm, new_comm;
        int myrank;

        MPI_Comm_rank(old_comm, &myrank);

        MPI_Group_incl(common->owner_group, 1,
                &common->simulated_failed_proc,
                &failed_group);
        MPI_Group_excl(common->owner_group, 1,
                &common->simulated_failed_proc,
                &new_owner_group);
        MPI_Comm_create(common->comm, new_owner_group, &new_comm);
        MPI_Group_free(&new_owner_group);

        if (myrank != common->simulated_failed_proc) {
            gdsi_replace_comm(common, new_comm, gdsi_group_world);
            gdsi_dprintf(GDS_DEBUG_INFO,
                    "setting up procfail error descriptor: failed proc=%d\n",
                    common->simulated_failed_proc);
            ret = setup_procfail_edesc(common, old_comm, failed_group);
            if (ret != GDS_STATUS_OK) {
                MPI_Group_free(&failed_group);
                goto out;
            }
            *error_flag = 1;
        } else {
            /* I'm the one who is going to die */
            MPI_Win_unlock_all(common->win);
            MPI_Win_free(&common->win);
#ifdef GDS_CONFIG_USE_LRDS
            if (common->lrds_dirty_win != MPI_WIN_NULL) {
                MPI_Win_unlock_all(common->lrds_dirty_win);
                MPI_Win_free(&common->lrds_dirty_win);
            }
#endif
        }
        common->simulated_failed_proc = -1;
    }

out:
    gdsi_bitvec_delete(bv);

    return ret;
}

/* This function itself runs as a non-collective call, but it may run in
   a collective context if called from global_error_check */
static GDS_status_t run_error_check(GDS_gds_t gds,
        struct gdsi_check_fun *check_funs[], bool all_check)
{
    int i;
    GDS_size_t version_num;
    GDS_status_t check_ret;
    GDS_get_version_number(gds, &version_num);

    for (i = 0; i < GDS_PRIORITY_MAX; i++) {

        /* Change frequency based on priority */
        if (!all_check && ((int)version_num % ((i + 1) * (i + 1)) != 0))
            continue;

        struct gdsi_check_fun *chk = check_funs[i];
        while (chk != NULL) {
            check_ret = chk->check_func(gds, chk->check_priority);
            if (check_ret != GDS_STATUS_OK)
                return check_ret;
            chk = chk->next;
        }
    }

    return GDS_STATUS_OK;
}

static GDS_status_t local_error_check(GDS_gds_t gds, bool all_check)
{
    return run_error_check(gds, gds->common->local_check_funs, all_check);
}

/* This is a collective call */
static GDS_status_t global_error_check(GDS_gds_t gds, bool all_check)
{
    return run_error_check(gds, gds->common->global_check_funs, all_check);
}

static void show_info(void)
{
    int my_rank, nprocs;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (my_rank != 0)
        return;

    printf("Global View Resilience (GVR)\n");
    printf("http://gvr.cs.uchicago.edu/\n");
    puts("");
    printf("    Version: %s\n", VERSION);
    printf("    Git description:   %s\n",
            strlen(GITDESC) > 0 ? GITDESC : "(N/A)");
    printf("    Git full revision: %s\n", GITID);
    printf("    Configure options: '%s'\n", CONFIG_OPTS);
#ifdef GDS_CONFIG_COUNTER
    printf("    GDS_CONFIG_COUNTER enabled\n");
#endif
#ifdef GDS_CONFIG_USE_SCR
    printf("    GDS_CONFIG_USE_SCR enabled\n");
#endif
#ifdef GDS_CONFIG_USE_LRDS
    printf("    GDS_CONFIG_USE_LRDS enabled\n");
#endif
    puts("");
    printf("    Number of processes: %d\n", nprocs);
    gdsi_dynconf_print();
}

#ifdef GDS_CONFIG_USE_SCR
int flush_to_scr_freq;
char *SCR_JOB_ID;
char SCR_CACHE_BASE[SCR_MAX_FILENAME];
char SCR_PREFIX[SCR_MAX_FILENAME];
bool SCR_SUPPORT;
/* For utilizing SCR purpose, get environmental variables */
void gdsi_get_env_params()
{
    /* FLUSH_to_SCR: frequency to flush version to SCR.
     * SCR_JOB_ID: job id.
     * SCR_CACHE_BASE: local filesystem prefix.
     * SCR_PREFIX: shared filesystem prefix.
     */
    char *value;
    if ((value = getenv("FLUSH_to_SCR")) != NULL)
        flush_to_scr_freq = atoi(value);
    else
        flush_to_scr_freq = FLUSH_to_SCR; /* default value 3 */

    if ((SCR_JOB_ID = getenv("SCR_JOB_ID")) == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "SCR_JOB_ID is not set.\n");
        gdsi_dprintf(GDS_DEBUG_INFO, "SCR_JOB_ID is %s.\n", SCR_JOB_ID);
    }

    if ((value = getenv("SCR_CACHE_BASE")) != NULL) {
        strncpy(SCR_CACHE_BASE, value, SCR_MAX_FILENAME);
        gdsi_dprintf(GDS_DEBUG_INFO, "SCR_CACHE_BASE is %s.\n", SCR_CACHE_BASE);
    } else
        strcpy(SCR_CACHE_BASE, "/tmp");

    if ((value = getenv("SCR_PREFIX")) != NULL) {
        strncpy(SCR_PREFIX, value, SCR_MAX_FILENAME);
        gdsi_dprintf(GDS_DEBUG_INFO, "SCR_PREFIX is set %s.\n", SCR_PREFIX);
    } else {
        getcwd(SCR_PREFIX, SCR_MAX_FILENAME);
        gdsi_dprintf(GDS_DEBUG_INFO, "Default SCR_PREFIX is %s.\n", SCR_PREFIX);
    }
}
#endif

GDS_status_t GDS_init(int *argc, char **argv[], GDS_thread_support_t requested_thread_support, GDS_thread_support_t *provided_thread_support) {
    int prov;
    int mpi_init;
    GDS_status_t ret;

    pthread_rwlock_init(&gdsi_gds_common_list_lock, NULL);

    gdsi_dynconf_init();

    ret = error_init();
    if (ret != GDS_STATUS_OK) {
        gdsi_dprintf(GDS_DEBUG_ERR, "error_init returned %d\n", ret);
        return ret;
    }

    MPI_Initialized(&mpi_init);
    if (mpi_init) {
        MPI_Query_thread(&prov);
        if (prov != MPI_THREAD_MULTIPLE && !gdsi_dontcare_thread_multiple)
            gdsi_panic("MPI library should be initialized with MPI_THREAD_MULTIPLE\n");
        need_to_cleanup_mpi = 0;
    } else {
        GDSI_MPI_CALL(MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &prov));
        if (prov != MPI_THREAD_MULTIPLE)
            gdsi_panic("MPI_Init_thread provided %d\n", prov);
    }

    if (gdsi_show_info)
        show_info();

    GDSI_MPI_CALL(MPI_Comm_dup(MPI_COMM_WORLD, &GDS_COMM_WORLD));
    GDSI_MPI_CALL(MPI_Comm_rank(GDS_COMM_WORLD, &gdsi_dprintf_myrank));

    if (gdsi_dyntag_init(GDS_COMM_WORLD) != GDS_STATUS_OK)
        gdsi_panic("Failed to initialize dynamic tag system\n");

    ret = gdsi_target_init();
    if (ret != GDS_STATUS_OK)
        gdsi_panic("gdsi_target_init failed: %d\n", ret);

    gdsi_thread_support = GDS_THREAD_SINGLE;
    *provided_thread_support = gdsi_thread_support;

    MPI_Type_contiguous(sizeof(struct gdsi_chunk_desc), MPI_BYTE,
            &gdsi_chunk_desc_type);
    MPI_Type_commit(&gdsi_chunk_desc_type);

#ifdef GDS_CONFIG_USE_SCR
    SCR_Init();
    SCR_SUPPORT = true;
    gdsi_get_env_params();
#endif

    return GDS_STATUS_OK;
}

GDS_status_t GDS_finalize() {
    if (skip_cleanup)
        return GDS_STATUS_OK;

#ifdef GDS_CONFIG_USE_SCR
    SCR_Finalize();
#endif

    gdsi_target_exit();

    gdsi_dyntag_finalize(GDS_COMM_WORLD);

    MPI_Type_free(&gdsi_chunk_desc_type);
    MPI_Comm_free(&GDS_COMM_WORLD);

    if (need_to_cleanup_mpi)
        MPI_Finalize();

    pthread_rwlock_rdlock(&gdsi_gds_common_list_lock); {
        size_t remaining = 0;
        struct gdsi_gds_common *c;

        for (c = gdsi_gds_common_list; c != NULL; c = c->next)
            remaining++;

        if (remaining > 0)
            gdsi_dprintf(GDS_DEBUG_ERR, "%zu arrays were not freed\n",
                    remaining);
    } pthread_rwlock_unlock(&gdsi_gds_common_list_lock);

    error_finalize();

    pthread_rwlock_destroy(&gdsi_gds_common_list_lock);

    return GDS_STATUS_OK;
}

static GDS_status_t gdsi_index_bound_check(GDS_size_t lo[], GDS_size_t hi[],
        GDS_gds_t gds)
{
    GDS_size_t ndims, *count;
    enum gdsi_data_order order;
    int i;

    ndims = gds->common->ndims;
    count = gds->common->nelements;
    order = gds->common->order;

    if (order == GDSI_ORDER_ROW_MAJOR) {
        for (i = 0; i < ndims; i++) {
            if (lo[i] < 0 || lo[i] >= count[i] ||
                    hi[i] < 0 || hi[i] >= count[i]) {
                gdsi_panic("Array bound check failed\n");
            }
        }
    } else {
        for (i = 0; i < ndims; i++) {
            if (lo[i] < 0 || lo[i] >= count[ndims-1-i] ||
                    hi[i] < 0 || hi[i] >= count[ndims-1-i]) {
                gdsi_panic("Array bound check failed\n");
            }
        }
    }

    return GDS_STATUS_OK;
}

GDS_status_t GDS_alloc(GDS_size_t ndims, const GDS_size_t count[], const GDS_size_t min_chunks[], GDS_datatype_t element_type, GDS_priority_t resilience_priority, GDS_comm_t users, GDS_info_t info, GDS_gds_t *gds)
{
    struct GDS_gds *gds_internal;
    GDS_status_t ret;
    int myrank, sanity_hash, buf, i;

    /* Sanity checking */
    if (gdsi_sanity_check) {
        sanity_hash = ndims + 13;
        for (i = 0; i < ndims; i++) {
            sanity_hash += 3 * count[i] + 17;
            if (min_chunks)
                sanity_hash += 5 * min_chunks[i] + 19;
        }
        sanity_hash += 7 * (element_type - MPI_CHAR) + 23;

        GDS_comm_rank(users, &myrank);
        if (myrank == 0) {
            buf = sanity_hash;
        }
        MPI_Bcast(&buf, 1, MPI_INT, 0, users);
        if (buf != sanity_hash) {
            gdsi_panic("GDS_alloc sanity checking failed on process %d\n", myrank);
        }
    }

    /* Allocation start */
    gds_internal = calloc(sizeof(*gds_internal), 1);
    if (gds_internal == NULL)
        return GDS_STATUS_NOMEM;

    if (info == 0) {
        gdsi_dprintf(GDS_DEBUG_ERR,
                "WARNING: NULL is no longer valid as the info argument for GDS_alloc. Use MPI_INFO_NULL instead.\n");
        info = MPI_INFO_NULL;
    }

    ret = gdsi_alloc(ndims, count, min_chunks, element_type, users, info, &gds_internal->common);
    if (ret != GDS_STATUS_OK)
        goto out_free_gds;

    gds_internal->common->version_inc_state = 0;
    gds_internal->common->get_info = 0;


    /* At this moment, no other thread is using this object,
       therefore we can touch the common structure without holding lock */
    gds_internal->common->n_reqs = 0;
    gds_internal->common->refcount = 1;
    gds_internal->chunk_set = gds_internal->common->chunk_sets;
    gds_internal->chunk_set->label = NULL;
    gds_internal->chunk_set->label_size = 0;
    if (gds_internal->common->representation == GDSI_REPR_LOG)
        gds_internal->common->ops = &log_ops;
    else
        gds_internal->common->ops = &flat_ops;
    *gds = gds_internal;
    add_to_gdsi_gds_common_list(gds_internal->common);

    MPI_Win_lock_all(MPI_MODE_NOCHECK, (*gds)->common->win);
#ifdef GDS_CONFIG_USE_LRDS
    gdsi_register_lrds_callback(gds_internal->common);
    if ((*gds)->common->lrds_dirty_win != MPI_WIN_NULL)
        MPI_Win_lock_all(0, (*gds)->common->lrds_dirty_win);
#endif

    return GDS_STATUS_OK;

out_free_gds:
    free(gds_internal);

    return ret;
}

GDS_status_t GDS_get_execute(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds);

static void finish_version_inc(GDS_gds_t gds) {
    if (gds->common->version_inc_state) {
        gdsi_metadata_sync(gds->common);

        struct gdsi_get_info *current = gds->common->get_info;
        while (current != 0) {
            GDS_get_execute(current->origin_addr, current->origin_ld, current->lo_index, current->hi_index, gds);
            current = current->next;
        }
        gds->common->version_inc_state = 0;
        gdsi_destroy_get_info(&(gds->common->get_info));

        MPI_Win win = gds->common->win;
        /* Flush all pending reads */
        do {
            win_flush(gds->common);
            MPI_Win_sync(win);
            nbworkq_run(gds->common, true, true);
        } while (gds->common->nbworkq != NULL);
    }
}

static void finish_version_inc_test(GDS_gds_t gds) {
    if (gds->common->version_inc_state) {
        int res;
        MPI_Testall(gds->common->n_reqs, gds->common->reqs, &res, MPI_STATUSES_IGNORE);
        if (res) {
            finish_version_inc(gds);
        }
    }
}

GDS_status_t GDS_free(GDS_gds_t *gds) {
    int refcount;

    if (*gds == NULL)
        goto out;

    /* Check whether the previous round of GDS_version_inc() has finished */
    finish_version_inc(*gds);

    gdsi_common_lock((*gds)->common);
    refcount = --(*gds)->common->refcount;
    gdsi_common_unlock((*gds)->common);

    if (refcount == 0) {
        gdsi_dprintf(GDS_DEBUG_INFO, "Freeing array...\n");
        if (gdsi_exit_on_free)
            exit(0);
        GDS_fence(*gds);
#ifdef GDS_CONFIG_USE_LRDS
        gdsi_unregister_lrds_callback((*gds)->common);
#endif
        gdsi_assert((*gds)->common->nbworkq == NULL);
        gdsi_dprintf(GDS_DEBUG_INFO, "Unlocking window...\n");
        MPI_Win_unlock_all((*gds)->common->win);
#ifdef GDS_CONFIG_USE_LRDS
        if ((*gds)->common->lrds_dirty_win != MPI_WIN_NULL)
            MPI_Win_unlock_all((*gds)->common->lrds_dirty_win);
#endif
        gdsi_dprintf(GDS_DEBUG_INFO, "Window unlocked\n");

        remove_from_gdsi_gds_common_list((*gds)->common);

        gdsi_free(&(*gds)->common);
    }

    free(*gds);
    *gds = NULL;

out:
    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_attr(GDS_gds_t gds, GDS_attr_t attribute_key,
        void *attribute_value, int *flag)
{
    GDS_size_t *cts_p = NULL;

    /* Check and handle local error first */
    local_error_poll_handle(gds);

    *flag = 1;

    switch (attribute_key) {
        case GDS_TYPE:
            *((GDS_datatype_t *) attribute_value) = gds->common->type;
            break;

        case GDS_CREATE_FLAVOR:
            *((GDS_flavor_t *) attribute_value) = gds->common->flavor;
            break;

        case GDS_GLOBAL_LAYOUT:
            /* TODO: support */
            *flag = 0;
            break;

        case GDS_NUMBER_DIMENSIONS:
            *((GDS_size_t *) attribute_value) = gds->common->ndims;
            break;

        case GDS_COUNT:;
                       cts_p = (GDS_size_t *) attribute_value;
                       int i;
                       for (i = 0; i < gds->common->ndims; i++)
                           cts_p[i] = gds->common->nelements[i];
                       break;

        case GDS_CHUNK_SIZE:
                       /* Chunk size may change after process crash, treat this case as
                        * GDS_create(), i.e., ignore */
                       if (gds->common->flavor == GDS_FLAVOR_ALLOC) {
                           struct gdsi_chunk_desc *cd;
                           int rank;
                           GDS_size_t *chunk_size = (GDS_size_t *) attribute_value;
                           GDS_comm_rank(gds->common->comm, &rank);
                           cd = gds->common->chunk_sets->chunk_descs;
                           gdsi_decompose_size(cd[rank].size_flat, gds->common, chunk_size);
                       } else {
                           attribute_value = NULL;
                           *flag = 0;
                       }
                       break;

        default:
                       *flag = 0;
                       break;
    }

    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_comm(GDS_gds_t gds, GDS_comm_t *comm)
{
    *comm = gds->common->comm;
    return GDS_STATUS_OK;
}

GDS_status_t GDS_info_set_int(MPI_Info info, const char *key, int val)
{
    char buff[2 * sizeof(int) + 1];

    sprintf(buff, "%x", val);
    GDSI_MPI_CALL(MPI_Info_set(info, key, buff));

    return GDS_STATUS_OK;
}

static void get_lambda_flat(GDS_gds_t gds,
        size_t local_offset, int target_rank,
        size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args)
{
    int mpi_type_size;
    MPI_Datatype mpi_type = (MPI_Datatype)gds->common->type;
    MPI_Type_size(mpi_type, &mpi_type_size);

    if (gdsi_chunk_rma_accessible(target_index)
            && gds->common->get_impl == GDS_GET_IMPL_RMA) {
        MPI_Get(args->buf + local_offset * mpi_type_size, count, mpi_type,
                target_rank, target_index + target_offset, count, mpi_type,
                gds->common->win);
        rma_issued(gds->common);
        gdsi_dprintf(GDS_DEBUG_INFO,
                "MPI_Get(buf+%zu, count=%zu, target_rank=%d, "
                "target_index+target_offset=%zu)\n",
                local_offset * mpi_type_size, count, target_rank,
                target_index + target_offset);
    }
    else {
        struct gdsi_nbworkq_entry *wq;
        gdsi_indirect_fetch(gds, target_rank,
                args->buf + local_offset * mpi_type_size,
                target_offset, count, chunk_number, &wq);
        nbworkq_enqueue(wq);
    }
}

#define log_check_limit(_common_, _target_rank_, _td_)                  \
    do {                                                                \
        if ((_td_) > (_common_)->log_size_limits[(_target_rank_)])      \
        gdsi_panic("Log limit overflow: "                          \
                "td=%zu target_rank=%d limit=%zu\n",                    \
                (_td_), (_target_rank_),                                \
                ((_common_)->log_size_limits[(_target_rank_)]));        \
    } while (0)

enum gdsi_log_get_state {
    GDS_LOG_GET_INIT,
    GDS_LOG_GET_DONE,
};

struct log_get_workq {
    struct gdsi_nbworkq_entry wq; /* This must be at the head */

    int n_blocks;
    gdsi_log_md_t mdc_orig[GDSI_LOG_RMA_GET_BATCH];
    enum gdsi_log_get_state state;

    /* Data members to re-issue MPI_Get */
    void *orig_buff;
    size_t count;
    int target_rank;
    size_t target_index;
    size_t target_offset;
};

static int log_get_test(struct gdsi_nbworkq_entry *wqe)
{
    struct log_get_workq *wq = (struct log_get_workq *) wqe;
    struct gdsi_gds_common *common = wq->wq.gds->common;
    MPI_Datatype mpi_type = common->type;
    int unit_size;
    gdsi_log_md_t *mdc;

    MPI_Type_size(mpi_type, &unit_size);

    mdc = gdsi_log_get_md_cache(common, wq->target_rank, wq->target_offset);

    switch (wq->state) {
        int i, ret;
        size_t td, len, pad_front, pad_back, end, local_offset, off;
        gdsi_log_md_t md_contig_start;
        int contig_len;
        case GDS_LOG_GET_INIT:
        /* MD should be ready */
        ret = 1;
        off = (wq->target_index + wq->target_offset) * unit_size;
        pad_front = log_pad_front(off, common->log_user_bs);
        end = (wq->target_index + wq->target_offset + wq->count) * unit_size;
        pad_back = log_pad_back(end, common->log_user_bs);

        local_offset = 0;
        contig_len = 1;
        md_contig_start = 0;

        gdsi_dprintf(GDS_DEBUG_INFO, "[%u] wq->n_blocks=%d\n",
                wqe->id, wq->n_blocks);

        for (i = 0; i < wq->n_blocks; i++) {
            if (mdc[i] == wq->mdc_orig[i]) {
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_GET_MDC_HIT, 1);
#endif
                continue;
            }
#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter, GDS_COUNTER_GET_MDC_MISS, 1);
#endif
            md_contig_start = mdc[i];
            if (i > 0)
                local_offset += common->log_user_bs * i - pad_front;
            break;
        }
        for (++i; i < wq->n_blocks; i++) {
            size_t pdf = 0;
            if (i - contig_len == 0)
                pdf = pad_front;

            if (mdc[i] != wq->mdc_orig[i] && mdc[i-1] + 1 == mdc[i]) {
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_GET_MDC_MISS, 1);
#endif
                contig_len++;
                continue;
            }

            if (md_contig_start != 0) {
                ret = 0;
                td = wq->target_index * unit_size + pdf
                    + log_md_to_byteoff(md_contig_start, common->log_user_bs);
                len = common->log_user_bs * contig_len - pdf;
                gdsi_dprintf(GDS_DEBUG_INFO, "[%u] local_offset=%zu "
                        "len=%zu count*unit_size=%zu\n",
                        wqe->id, local_offset, len, wq->count * unit_size);
                gdsi_assert(local_offset + len <= wq->count * unit_size);
                log_check_limit(common, wq->target_rank, td + len);
                gdsi_dprintf(GDS_DEBUG_INFO,
                        "[%u] (retry) MPI_Get(buf+%zu, len=%zu, target_rank=%d, "
                        "td=%zu)\n", wqe->id,
                        local_offset, len, wq->target_rank, td);
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_GET_RETRY, 1);
#endif
                log_check_limit(common, wq->target_rank, td + len);
                MPI_Get(wq->orig_buff + local_offset, len, MPI_BYTE,
                        wq->target_rank, td, len, MPI_BYTE, common->win);
                rma_issued(common);
                local_offset += len;
                contig_len = 0;
            }

            if (mdc[i] != wq->mdc_orig[i]) {
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_GET_MDC_MISS, 1);
#endif
                if (md_contig_start == 0) {
                    gdsi_dprintf(GDS_DEBUG_INFO, "[%u] local_offset=%zu "
                            "contig_len=%d\n",
                            wqe->id, local_offset, contig_len);
                    local_offset += contig_len * common->log_user_bs;
                    contig_len = 0;
                }
                md_contig_start = mdc[i];
            } else {
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_GET_MDC_HIT, 1);
#endif
                md_contig_start = 0;
            }

            contig_len++;
        }

        if (i > wq->n_blocks)
            i = wq->n_blocks;

        if (md_contig_start != 0) {
            size_t pdf = 0;

            if (i - contig_len == 0)
                pdf = pad_front;

            ret = 0;
            td = wq->target_index * unit_size
                + log_md_to_byteoff(md_contig_start, common->log_user_bs)
                + pdf;
            len = common->log_user_bs * contig_len - pad_back - pdf;
            gdsi_dprintf(GDS_DEBUG_INFO, "[%u] i=%d "
                    "pad_front=%zu pad_back=%zu "
                    "contig_len=%d\n", wqe->id, i, pad_front, pad_back, contig_len);
            gdsi_dprintf(GDS_DEBUG_INFO, "[%u] local_offset=%zu "
                    "len=%zu count*unit_size=%zu\n",
                    wqe->id, local_offset, len, wq->count * unit_size);
            gdsi_assert(local_offset + len <= wq->count * unit_size);
            log_check_limit(common, wq->target_rank, td + len);
            gdsi_dprintf(GDS_DEBUG_INFO,
                    "[%u] (retry) MPI_Get(buf+%zu, len=%zu, target_rank=%d, "
                    "td=%zu)\n", wqe->id,
                    local_offset, len, wq->target_rank, td);
#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter, GDS_COUNTER_GET_RETRY, 1);
#endif
            log_check_limit(common, wq->target_rank, td + len);
            MPI_Get(wq->orig_buff + local_offset, len, MPI_BYTE,
                    wq->target_rank, td, len, MPI_BYTE, common->win);
            rma_issued(common);
        }

        wq->state = GDS_LOG_GET_DONE;
        return ret;

        case GDS_LOG_GET_DONE:
        /* Nothing to do */
        return 1;
    }

    gdsi_panic("Should not reach here!\n");
}

typedef void (*log_batch_op_t)(void *buff, size_t len, int target_rank,
        size_t target_disp, MPI_Win win);

static void log_batch_get_op(void *buff, size_t len, int target_rank,
        size_t target_disp, MPI_Win win)
{
    MPI_Get(buff, len, MPI_BYTE, target_rank, target_disp, len, MPI_BYTE, win);
}

static void log_batch_put_op(void *buff, size_t len, int target_rank,
        size_t target_disp, MPI_Win win)
{
    MPI_Put(buff, len, MPI_BYTE, target_rank, target_disp, len, MPI_BYTE, win);
}

static void log_process_batch(struct gdsi_gds_common *common, void *buff,
        int target_rank, size_t target_disp_base, gdsi_log_md_t *mdc_start,
        size_t n_blocks, size_t pad_front, size_t pad_back,
        log_batch_op_t op)
{
    gdsi_log_md_t *mdc = mdc_start, md_contig_start;
    size_t i, contig_mds = 1, td, len, local_offset = 0;
    const size_t total_put_len = common->log_user_bs * n_blocks
        - pad_front - pad_back;

    md_contig_start = *mdc;
    for (i = 1; i < n_blocks; i++) {
        if (mdc[i-1] + 1 == mdc[i]) {
            contig_mds++;
            continue;
        }

        /* Non-contiguous */
        td = target_disp_base
            + log_md_to_byteoff(md_contig_start, common->log_user_bs)
            + pad_front;
        len = contig_mds * common->log_user_bs - pad_front;
        gdsi_dprintf(GDS_DEBUG_INFO,
                "1st MPI_Op(buf+%zu, len=%zu, target_rank=%d, td=%zu)\n",
                local_offset, len, target_rank, td);
        gdsi_assert(local_offset + len <= total_put_len);
        log_check_limit(common, target_rank, td + len);
        op(buff + local_offset, len, target_rank, td, common->win);
        rma_issued(common);

        md_contig_start = mdc[i];
        contig_mds = 1;
        pad_front = 0;
        local_offset += len;
    }
    td = target_disp_base
        + log_md_to_byteoff(md_contig_start, common->log_user_bs)
        + pad_front;
    len = total_put_len - local_offset;
    gdsi_dprintf(GDS_DEBUG_INFO,
            "1st MPI_Op(buf+%zu, len=%zu, target_rank=%d, td=%zu)\n",
            local_offset, len, target_rank, td);
    gdsi_assert(local_offset + len <= total_put_len);
    log_check_limit(common, target_rank, td + len);
    op(buff + local_offset, len, target_rank, td, common->win);
    rma_issued(common);
}

static void log_get_process_batch(GDS_gds_t gds, void *local_buff,
        int target_rank, size_t target_index, size_t target_offset, size_t count)
{
    struct gdsi_gds_common *common = gds->common;
    struct log_get_workq *wq;
    gdsi_log_md_t *mdc, md_prev;
    int i, n_blocks, unit_size, n_blk_pf;
    bool all_fresh, all_contig;
    size_t pad_front, pad_back;
    size_t mdi = gdsi_log_md_index(gds->common, target_rank, target_offset);

    gdsi_dprintf(GDS_DEBUG_INFO, "log_get_process_batch: "
            "local_buff=%p, target_rank=%d, target_index=%zu, target_offset=%zu, "
            "count=%zu\n",
            local_buff, target_rank, target_index, target_offset, count);

    MPI_Type_size(common->type, &unit_size);

    pad_front = log_pad_front(target_offset * unit_size, common->log_user_bs);
    n_blocks = log_required_blocks(target_offset * unit_size,
            count * unit_size, common->log_user_bs);
    pad_back = n_blocks * common->log_user_bs - count * unit_size - pad_front;

    gdsi_dprintf(GDS_DEBUG_INFO, "n_blocks=%d\n", n_blocks);

    gdsi_assert(n_blocks <= GDSI_LOG_RMA_GET_BATCH);

    mdc = gdsi_log_get_md_cache(common, target_rank, target_offset);

    all_fresh = gdsi_log_md_is_fresh(common, target_rank, mdc[0]);
    all_contig = true;
    md_prev = mdc[0];
    for (i = 1; i < n_blocks; i++) {
        if (!gdsi_log_md_is_fresh(common, target_rank, mdc[i]))
            all_fresh = false;
        if (mdc[i] != md_prev + 1)
            all_contig = false;
        md_prev = mdc[i];
    }

    /*
       Condition for speculative data fetch
       1. all md caches are fresh -- in this case we can directly access
       the latest data, or
       2. all data blocks are contiguous (according to the current md cache)
       in this case, the cost of speculative fetching should be low
     */
    if (all_fresh || all_contig) {
#ifdef GDS_CONFIG_COUNTER
        gdsi_counter_add(&common->counter, GDS_COUNTER_GET_SPECULATIVE, 1);
        if (all_fresh)
            gdsi_counter_add(&common->counter, GDS_COUNTER_GET_MDC_HIT, n_blocks);
#endif
        log_process_batch(common, local_buff, target_rank,
                target_index * unit_size, mdc, n_blocks, pad_front, pad_back,
                log_batch_get_op);
    }

    if (all_fresh)
        return;

    wq = malloc(sizeof(*wq));
    /* TODO: graceful recovery */
    if (wq == NULL)
        gdsi_panic("GDS_get: failed to allocate internal memory object.\n");

    nbworkq_rma_init(&wq->wq, gds, log_get_test);

    /* If we did not issue speculative fetching, fill 0 in mdc_orig
       so that we'll always get a new MD index */
    if (all_fresh || all_contig)
        memcpy(wq->mdc_orig, mdc, sizeof(gdsi_log_md_t) * n_blocks);
    else
        memset(wq->mdc_orig, 0, sizeof(gdsi_log_md_t) * n_blocks);

    n_blk_pf = n_blocks;
    if (n_blocks < gdsi_log_md_prefetch_max) {
        size_t bs = common->log_user_bs;
        size_t n = (common->log_md_recvcounts[target_rank] * bs
                - log_aligned_prev(target_offset * unit_size, bs)) / bs;
        if (n >= gdsi_log_md_prefetch_max)
            n_blk_pf = gdsi_log_md_prefetch_max;
        else
            n_blk_pf = n;
    }
    log_check_limit(common, target_rank,
            mdi + n_blk_pf * sizeof(gdsi_log_md_t));
    MPI_Get(mdc, n_blk_pf, MPI_UINT32_T, target_rank,
            mdi, n_blk_pf, MPI_UINT32_T, common->win);
    rma_issued(common);

    wq->n_blocks      = n_blocks;
    wq->state         = GDS_LOG_GET_INIT;
    wq->orig_buff     = local_buff;
    wq->count         = count;
    wq->target_index  = target_index;
    wq->target_offset = target_offset;
    wq->target_rank   = target_rank;

    nbworkq_enqueue(&wq->wq);
}

static void get_lambda_log(GDS_gds_t gds,
        size_t local_offset, int target_rank,
        size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args)
{
    int unit_size;
    MPI_Datatype mpi_type = (MPI_Datatype) gds->common->type;
    gdsi_log_md_t *md = gdsi_log_get_md_cache(gds->common,
            target_rank, target_offset);
    size_t pad_front, batch_start = target_offset;

    gdsi_dprintf(GDS_DEBUG_INFO, "get_lambda_log: local_offset=%zu, "
            "target_rank=%d, target_index=%zu, target_offset=%zu, count=%zu, "
            "args->buf=%p\n",
            local_offset, target_rank, target_index, target_offset, count,
            args->buf);

    gdsi_assert(md);

    MPI_Type_size(mpi_type, &unit_size);

    if (gds->common->get_impl == GDS_GET_IMPL_MSG
            || !gdsi_chunk_rma_accessible(target_index)) {
        struct gdsi_nbworkq_entry *wq;
        gdsi_indirect_fetch(gds, target_rank,
                args->buf + local_offset * unit_size, target_offset, count,
                chunk_number, &wq);
        nbworkq_enqueue(wq);
        return;
    }

    pad_front = log_pad_front((target_index + target_offset) * unit_size,
            gds->common->log_user_bs);

    while (batch_start < target_offset + count) {
        size_t batch_count_max = (gds->common->log_user_bs
                * GDSI_LOG_RMA_GET_BATCH - pad_front) / unit_size;
        size_t batch_count = batch_count_max;
        void *local_buff = args->buf + local_offset * unit_size;
        if (target_offset + count - batch_start < batch_count_max)
            batch_count = target_offset + count - batch_start;

        gdsi_assert(batch_start + batch_count <= target_offset + count);

        log_get_process_batch(gds,
                local_buff + (batch_start - target_offset) * unit_size,
                target_rank, target_index, batch_start, batch_count);
        batch_start += batch_count;
        pad_front = 0;
    }
}

GDS_status_t GDS_get_execute(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds)
{
#ifdef GDS_CONFIG_COUNTER
    {
        int typelen;
        MPI_Type_size(gds->common->type, &typelen);
        size_t len = typelen;
        int i;
        /* TODO: consider ld correctly */
        for (i = 0; i < gds->common->ndims; i++)
            len *= hi_index[i] - lo_index[i] + 1;
        gdsi_counter_add(&gds->common->counter, GDS_COUNTER_GET, len);
    }
#endif

    struct gdsi_op_args args;
    args.buf = origin_addr;
    iterate_array_top(gds, lo_index, hi_index, origin_ld,
            gds->common->ops->get, &args);

    rma_post_check(gds->common);

    return GDS_STATUS_OK;
}

//in get, put, and acc, ld is in number of elements
GDS_status_t GDS_get(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds)
{
    /* Check and handle local error first */
    local_error_poll_handle(gds);

    /* Check and repair old metadata */
    gdsi_repair_old_metadata(gds->common, gds->chunk_set);

    /* Sanity checking */
    if (gdsi_sanity_check)
        gdsi_index_bound_check(lo_index, hi_index, gds);

    char dbg_buff[128];
    snprint_coord(dbg_buff, sizeof(dbg_buff), gds->common->ndims, lo_index, hi_index);
    gdsi_dprintf(GDS_DEBUG_INFO, "GDS_get: %s\n", dbg_buff);

    /* GDS_version_inc() completion checking */
    finish_version_inc_test(gds);

    if (gds->common->version_inc_state) {
        /* has called GDS_version_inc() before and version_inc_nb has not finished */
        gdsi_append_get(origin_addr, origin_ld, lo_index, hi_index, gds);
    } else {
        /* has not called GDS_version_inc() or in normal mode */
        GDS_get_execute(origin_addr, origin_ld, lo_index, hi_index, gds);
    }

    return GDS_STATUS_OK;
}

#ifdef GDS_CONFIG_USE_LRDS
struct lrds_put_workq {
    struct gdsi_nbworkq_entry wq;
    uint8_t dirty_buff[0];
};

static int lrds_put_test(struct gdsi_nbworkq_entry *wqe)
{
    return 1;
}

static void lrds_update_dirty(GDS_gds_t gds,
        int mpi_type_size,
        int target_rank, size_t target_index, size_t target_offset, size_t count)
{
    struct gdsi_gds_common *common = gds->common;
    struct lrds_put_workq *wq;
    size_t buflen; /* for dirty buffer */
    size_t off_tgt = (target_index + target_offset) * mpi_type_size;
    size_t aligned_off = log_aligned_prev(off_tgt, common->lrds_prop_dirty_block_size * 8);
    size_t len = count * mpi_type_size;
    int first_bit, last_bit, i;
    size_t off_buff = off_tgt - aligned_off;

    first_bit = off_buff / common->lrds_prop_dirty_block_size;
    last_bit = (off_buff + len - 1) / common->lrds_prop_dirty_block_size;
    buflen = last_bit / 8 + 1;

    wq = malloc(sizeof(*wq) + buflen);
    gdsi_assert(wq);

    nbworkq_rma_init(&wq->wq, gds, lrds_put_test);

    memset(wq->dirty_buff, 0xff, buflen);
    for (i = 0; i < first_bit; i++)
        wq->dirty_buff[0] &= ~(1 << i);
    for (i = last_bit % 8 + 1; i < 8; i++)
        wq->dirty_buff[buflen-1] &= ~(1 << i);

    MPI_Accumulate(wq->dirty_buff, buflen, MPI_BYTE, target_rank,
            off_tgt / (common->lrds_prop_dirty_block_size * 8),
            buflen, MPI_BYTE, MPI_BOR, common->lrds_dirty_win);
    rma_issued(common);

    nbworkq_enqueue(&wq->wq);
}
#endif

static void put_lambda_flat(GDS_gds_t gds,
        size_t local_offset, int target_rank,
        size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args)
{
    /* Checking the buffer of newest version */
    if (gds->common->version_inc_state) {
        gdsi_check_new_version(gds->common, target_rank, target_index, chunk_number, gds->common->latest_version);
    }

    int mpi_type_size;
    MPI_Datatype mpi_type = (MPI_Datatype)gds->common->type;

    MPI_Type_size(mpi_type, &mpi_type_size);

    if (gds->common->put_impl == GDS_PUT_IMPL_RMA
            || gds->common->put_impl == GDS_PUT_IMPL_HYB) {
        MPI_Put(args->buf + local_offset * mpi_type_size, count,
                mpi_type, target_rank, target_index + target_offset,
                count, mpi_type, gds->common->win);
        rma_issued(gds->common);
        gdsi_dprintf(GDS_DEBUG_INFO,
                "MPI_Put(buf+%zu, count=%zu, target_rank=%d, "
                "target_index+target_offset=%zu)\n",
                local_offset * mpi_type_size, count, target_rank,
                target_index + target_offset);
#ifdef GDS_CONFIG_USE_LRDS
        if (gds->common->lrds_dirty_win != MPI_WIN_NULL) {
            lrds_update_dirty(gds, mpi_type_size,
                    target_rank, target_index, target_offset, count);
        }
#endif
    } else {
        struct gdsi_nbworkq_entry *wqe;
        gdsi_indirect_put(gds, target_rank,
                args->buf + local_offset * mpi_type_size, target_offset,
                count, false, MPI_NO_OP, &wqe);
        nbworkq_enqueue(wqe);
    }
}

static int inflight_req_comparator(struct gdsi_log_inflight_req *l,
        struct gdsi_log_inflight_req *r)
{
    if (l->target_rank < r->target_rank)
        return -1;
    else if (l->target_rank > r->target_rank)
        return 1;
    else if (l->block_id < r->block_id)
        return -1;
    else if (l->block_id > r->block_id)
        return 1;
    else
        return 0;
}

enum log_put_state {
    GDS_LOG_PUT_INIT,
    GDS_LOG_PUT_ALLOCATING,
    GDS_LOG_PUT_ALLOCATED,
    GDS_LOG_PUT_BUFFER_WAITING,
    GDS_LOG_PUT_BUFFER_READY,
    GDS_LOG_PUT_BUFFER_PADDING,
    GDS_LOG_PUT_MD_UPDATING,
    GDS_LOG_PUT_OVERWRITE,
    GDS_LOG_PUT_SPEC_ALLOCATING,
    GDS_LOG_PUT_DONE,
};

struct log_put_workq {
    struct gdsi_nbworkq_entry wq;
    enum log_put_state state;
    uint64_t log_tail_target;
    uint64_t log_tail_estimate;
    uint64_t log_tail_actual;
    uint64_t log_tail_diff; /* for fetch_and_op */
    gdsi_log_md_t md_target;
    gdsi_log_md_t md_estimate;
    gdsi_log_md_t md_actual;
    bool allocating;

    int target_rank;
    size_t target_offset;
    size_t target_index;
    size_t count;
    size_t pad_front;
    size_t pad_back;
    MPI_Datatype pad_stride_type;
    bool acc_mode;
    MPI_Op acc_op;
    const void *local_buff;
    const void *send_buf;
    char pad_buf[];
};

struct log_put_m_workq {
    struct gdsi_nbworkq_entry wq;
    enum log_put_state state;
    uint64_t log_tail_target;
    uint64_t log_tail_estimate;
    uint64_t log_tail_actual;
    uint64_t log_tail_diff; /* for fetch_and_op */
    gdsi_log_md_t md_targets[GDSI_LOG_RMA_PUT_BATCH];
    gdsi_log_md_t md_estimates[GDSI_LOG_RMA_PUT_BATCH];
    gdsi_log_md_t md_actuals[GDSI_LOG_RMA_PUT_BATCH];
    bool allocating;

    int target_rank;
    size_t target_offset;
    size_t target_index;
    size_t n_blocks;
    void *buff;
};

enum log_put_hyb_state {
    GDS_LOG_HYB_ISSUED,
    GDS_LOG_HYB_BUF_READY,
    GDS_LOG_HYB_DONE,
};

struct log_put_hyb_workq {
    struct gdsi_nbworkq_entry wq;
    enum log_put_hyb_state state;

    struct gdsi_nbworkq_entry *alloc_wq;
    int target_rank;
    size_t target_offset;
    size_t target_index;
    size_t n_blocks;
    size_t pad_front;
    size_t pad_back;
    void *buff;
};

static inline int log_put_preceding_hash(int target_rank, uint32_t block_id)
{
    return (target_rank * 17 + block_id * 11) % GDSI_NBWORKQ_INFLIGHT_MAX;
}

static inline uint32_t log_block_id(struct gdsi_gds_common *common, size_t target_offset)
{
    int unit_size;

    MPI_Type_size(common->type, &unit_size);

    return target_offset * unit_size / common->log_user_bs;
}

static gdsi_log_md_t log_put_fetch_stock(struct gdsi_gds_common *common,
        int target_rank);

/* Should be called under gdsi_common_lock held */
static bool log_put_preceding_exists(struct gdsi_gds_common *common,
        int target_rank, size_t target_offset)
{
    uint32_t block_id = log_block_id(common, target_offset);
    struct gdsi_log_inflight_req *ifr, ifr_ref = { .target_rank = target_rank,
        .block_id = block_id};
    int hash;

    hash = log_put_preceding_hash(target_rank, block_id);
    SGLIB_LIST_FIND_MEMBER(struct gdsi_log_inflight_req,
            common->log_preceding_alloc[hash], &ifr_ref,
            inflight_req_comparator, next, ifr);

    return ifr != NULL;
}

/* Should be called under gdsi_common_lock held */
static void log_put_preceding_register(struct gdsi_gds_common *common,
        int target_rank, size_t target_offset)
{
    uint32_t block_id = log_block_id(common, target_offset);
    struct gdsi_log_inflight_req *ifr;
    int hash = log_put_preceding_hash(target_rank, block_id);;

    ifr = malloc(sizeof(*ifr));
    if (ifr == NULL)
        return;

    ifr->target_rank = target_rank;
    ifr->block_id = block_id;
    SGLIB_LIST_ADD(struct gdsi_log_inflight_req,
            common->log_preceding_alloc[hash], ifr, next);

    return;
}

/* Check if remote memory allocation is needed, and, if so,
   mark that allocation is in progress */
static enum log_put_state log_put_need_allocation(
        struct gdsi_gds_common *common,
        int target_rank, size_t target_offset, gdsi_log_md_t *stockp)
{
    enum log_put_state ret;
    gdsi_log_md_t *mdc = gdsi_log_get_md_cache(common, target_rank,
            target_offset);

    gdsi_common_lock(common);

    if (gdsi_log_md_is_fresh(common, target_rank, *mdc)) {
        ret = GDS_LOG_PUT_BUFFER_WAITING;
        goto out_unlock;
    }

    if (log_put_preceding_exists(common, target_rank, target_offset)) {
        ret = GDS_LOG_PUT_BUFFER_WAITING;
        goto out_unlock;
    }

    *stockp = log_put_fetch_stock(common, target_rank);
    if (gdsi_log_md_is_fresh(common, target_rank, *stockp)) {
        gdsi_dprintf(GDS_DEBUG_WARN,
                "Stock fetched for target rank=%d\n", target_rank);
        ret = GDS_LOG_PUT_ALLOCATED;
        goto out_unlock;
    }

    ret = GDS_LOG_PUT_INIT;
    log_put_preceding_register(common, target_rank, target_offset);

out_unlock:
    gdsi_common_unlock(common);

    return ret;
}

static void log_put_allocation_complete(struct gdsi_gds_common *common,
        int target_rank, size_t target_offset)
{
    uint32_t block_id = log_block_id(common, target_offset);
    struct gdsi_log_inflight_req *ifr, ifr_ref = { .target_rank = target_rank,
        .block_id = block_id};
    int hash = log_put_preceding_hash(target_rank, block_id);

    gdsi_common_lock(common);

    SGLIB_LIST_DELETE_IF_MEMBER(struct gdsi_log_inflight_req,
            common->log_preceding_alloc[hash], &ifr_ref,
            inflight_req_comparator, next, ifr);

    gdsi_common_unlock(common);

    free(ifr);
}

static enum log_put_state log_put_need_allocation_range(
        struct gdsi_gds_common *common, int target_rank, size_t target_offset,
        const size_t cont_blks, size_t *cont_lenp, gdsi_log_md_t *stockp)
{
    enum log_put_state ret;
    gdsi_log_md_t *mdc = gdsi_log_get_md_cache(common, target_rank,
            target_offset);
    gdsi_log_md_t stock;
    size_t i, cont_len = 0;
    int unit_size;

    gdsi_assert(cont_lenp != NULL);
    gdsi_assert(stockp != NULL);

    MPI_Type_size(common->type, &unit_size);
    gdsi_assert(target_offset * unit_size % common->log_user_bs == 0);

    gdsi_common_lock(common);

    if (gdsi_log_md_is_fresh(common, target_rank, *mdc)) {
        gdsi_log_md_t md_prev;
        ret = GDS_LOG_PUT_OVERWRITE;

        do {
            md_prev = *mdc;
            cont_len++;
            mdc++;
        } while (cont_len < cont_blks
                && gdsi_log_md_is_fresh(common, target_rank, *mdc)
                && *mdc == md_prev + 1);

        *cont_lenp = cont_len;

        goto out_unlock;
    }

    /*
       If there are multiple preceding allocations, it would be difficult
       to tell if they would become contiguous.
       To simplify the implementation, for now assume all pceceding
       allocations will become non-contiguous.
       I believe this would be a corner case so it does not affect performance
       in most usual cases.
     */
    if (log_put_preceding_exists(common, target_rank, target_offset)) {
        ret = GDS_LOG_PUT_BUFFER_WAITING;
        *cont_lenp = 1;
        goto out_unlock;
    }

    stock = log_put_fetch_stock(common, target_rank);
    if (gdsi_log_md_is_fresh(common, target_rank, stock)) {
        gdsi_log_md_t st = stock, st_prev;
        ret = GDS_LOG_PUT_ALLOCATED;
        do {
            st_prev = st;
            st = log_put_fetch_stock(common, target_rank);
            cont_len++;
        } while (cont_len < cont_blks
                && st == st_prev + 1
                && gdsi_log_md_is_fresh(common, target_rank, st));
        *cont_lenp = cont_len;
        *stockp = stock;
        goto out_unlock;
    }

    ret = GDS_LOG_PUT_INIT;
    log_put_preceding_register(common, target_rank, target_offset);
    cont_len = 1;
    for (i = 1; i < cont_blks; i++) {
        target_offset += common->log_user_bs / unit_size;
        mdc++;
        if (gdsi_log_md_is_fresh(common, target_rank, *mdc))
            break;
        if (log_put_preceding_exists(common, target_rank, target_offset))
            break;
        cont_len++;
    }

    *cont_lenp = cont_len;

out_unlock:
    gdsi_common_unlock(common);

    return ret;
}

static void log_put_direct(const struct log_put_workq *wq)
{
    struct gdsi_gds_common *common = wq->wq.gds->common;
    int unit_size;
    size_t ti;
    size_t bs = common->log_user_bs;
    gdsi_log_md_t *md = gdsi_log_get_md_cache(common,
            wq->target_rank, wq->target_offset);

    MPI_Type_size(common->type, &unit_size);
    ti = wq->target_index * unit_size + wq->pad_front
        + log_md_to_byteoff(*md, bs);
    gdsi_dprintf(GDS_DEBUG_INFO,
            "[%u] %s via fresh MD cache (2): target_rank=%d md=0x%08x "
            "target_disp=%zu count=%zu\n",
            wq->wq.id, wq->acc_mode ? "Acc" : "Put",
            wq->target_rank, *md, ti, wq->count);
    if (wq->count * unit_size >= 4) {
        const char *b = wq->send_buf + wq->pad_front;
        gdsi_dprintf(GDS_DEBUG_INFO, "first 4 bytes of send buf: "
                "%02x %02x %02x %02x\n", b[0], b[1], b[2], b[3]);
    }

    log_check_limit(common, wq->target_rank, ti + wq->count * unit_size);

    if (wq->acc_mode)
        MPI_Accumulate(wq->send_buf + wq->pad_front, wq->count, common->type,
                wq->target_rank, ti, wq->count, common->type, wq->acc_op,
                common->win);
    else
        MPI_Put(wq->send_buf + wq->pad_front, wq->count, common->type,
                wq->target_rank, ti, wq->count, common->type, common->win);

    rma_issued(common);
}

static void log_put_init_remotebuf(struct log_put_workq *wq)
{
    struct gdsi_gds_common *common = wq->wq.gds->common;
    int unit_size;
    size_t ti;
    size_t bs = common->log_user_bs;
    gdsi_log_md_t *md = gdsi_log_get_md_cache(common,
            wq->target_rank, wq->target_offset);
    size_t mdi = gdsi_log_md_index(common, wq->target_rank, wq->target_offset);

    MPI_Type_size(common->type, &unit_size);
    wq->state = GDS_LOG_PUT_BUFFER_READY;

    ti = wq->target_index * unit_size + log_md_to_byteoff(*md, bs);

    if (wq->acc_mode) {
        log_check_limit(common, wq->target_rank, ti + bs);
        GDSI_MPI_CALL(MPI_Get(wq->pad_buf, common->log_user_bs, MPI_BYTE,
                    wq->target_rank, ti, common->log_user_bs, MPI_BYTE,
                    common->win));
        rma_issued(common);
        return;
    }

    if (wq->pad_front > 0 && wq->pad_back > 0) {
        log_check_limit(common, wq->target_rank, ti + bs);
        GDSI_MPI_CALL(MPI_Get(wq->pad_buf, 1, wq->pad_stride_type,
                    wq->target_rank, ti, 1, wq->pad_stride_type, common->win));
        rma_issued(common);
    } else if (wq->pad_front > 0) {
        log_check_limit(common, wq->target_rank, ti + wq->pad_front);
        GDSI_MPI_CALL(MPI_Get(wq->pad_buf, wq->pad_front, MPI_BYTE,
                    wq->target_rank, ti, wq->pad_front, MPI_BYTE, common->win));
        rma_issued(common);
    } else if (wq->pad_back > 0) {
        /* pad_front == 0 */
        size_t offset = common->log_user_bs - wq->pad_back;
        log_check_limit(common, wq->target_rank, ti + offset + wq->pad_back);
        GDSI_MPI_CALL(MPI_Get(wq->pad_buf + offset, wq->pad_back, MPI_BYTE,
                    wq->target_rank, ti + offset, wq->pad_back, MPI_BYTE,
                    common->win));
        rma_issued(common);
    } else {
        /* No padding */
        wq->md_target = log_byteoff_to_md(wq->log_tail_target - bs, bs);
        wq->md_estimate = *md;
        gdsi_dprintf(GDS_DEBUG_INFO, "[%u] (no padding) "
                "Trying to update MD: target=0x%x compare=0x%x "
                "target_rank=%d mdi=%zu\n",
                wq->wq.id, wq->md_target, wq->md_estimate, wq->target_rank, mdi);
        log_check_limit(common, wq->target_rank, mdi + sizeof(gdsi_log_md_t));
        GDSI_MPI_CALL(MPI_Compare_and_swap(&wq->md_target,
                    &wq->md_estimate, &wq->md_actual, MPI_UINT32_T,
                    wq->target_rank, mdi, common->win));
        rma_issued(common);
        ti = wq->target_index * unit_size + wq->log_tail_target - bs;
        gdsi_dprintf(GDS_DEBUG_INFO, "[%u] Speculative MPI_Put: "
                "bytes=%zu target_rank=%d target_disp=%zu\n",
                wq->wq.id, wq->count * unit_size, wq->target_rank, ti);
        log_check_limit(common, wq->target_rank, ti + wq->count * unit_size);
        GDSI_MPI_CALL(MPI_Put(wq->send_buf, wq->count * unit_size,
                    MPI_BYTE, wq->target_rank, ti, wq->count * unit_size,
                    MPI_BYTE, common->win));
        rma_issued(common);
        wq->state = GDS_LOG_PUT_MD_UPDATING;
    }
}

static void log_put_clear_stock(struct gdsi_gds_common *common,
        int target_rank)
{
    struct gdsi_log_stock_queue *q;

    q = common->log_db_stocks[target_rank];
    if (q == NULL)
        return;

    if (q->len > 0) {
#ifdef GDS_CONFIG_COUNTER
        gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_STOCK_DROP, q->len);
#endif
        q->len = 0;
    }
}

static void log_put_store_stock(struct gdsi_gds_common *common,
        int target_rank, gdsi_log_md_t md)
{
    struct gdsi_log_stock_queue *q;

    gdsi_dprintf(GDS_DEBUG_INFO, "Storing stock for target_rank=%d: "
            "md=0x%x\n", target_rank, md);

#ifdef GDS_CONFIG_COUNTER
    gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_STOCK_ENQUEUE, 1);
#endif

    q = common->log_db_stocks[target_rank];
    if (q == NULL) {
        int max_len = GDSI_NBWORKQ_INFLIGHT_MAX;
        q = malloc(sizeof(*q) + sizeof(gdsi_log_md_t) * max_len);
        if (q == NULL) {
            gdsi_dprintf(GDS_DEBUG_WARN, "Failed to allocate storage queue\n");
            return;
        }
        q->head = q->len = 0;
        q->max_len = max_len;
        common->log_db_stocks[target_rank] = q;
    }

    if (q->len == q->max_len) {
        gdsi_log_md_t old = q->stocks[q->head];
        q->stocks[q->head] = md;
        if (++q->head >= q->max_len)
            q->head = 0;
        if (gdsi_log_md_is_fresh(common, target_rank, old)) {
            gdsi_dprintf(GDS_DEBUG_WARN,
                    "Dropping stock data block for target rank=%d\n",
                    target_rank);
#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_STOCK_DROP, 1);
#endif
        }
    } else {
        int pos = (q->head + q->len) % q->max_len;
        q->stocks[pos] = md;
        q->len++;
    }
}

static gdsi_log_md_t log_put_fetch_stock(struct gdsi_gds_common *common,
        int target_rank)
{
    struct gdsi_log_stock_queue *q;
    gdsi_log_md_t md;

    q = common->log_db_stocks[target_rank];
    if (q == NULL || q->len == 0)
        return 0;

#ifdef GDS_CONFIG_COUNTER
    gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_STOCK_DEQUEUE, 1);
#endif

    do {
        gdsi_assert(q->head < q->max_len);
        md = q->stocks[q->head];

        gdsi_dprintf(GDS_DEBUG_INFO, "Loading stock from target_rank=%d, "
                "md=0x%x q->head=%d q->len=%d\n",
                target_rank, md, q->head, q->len);

        q->head = (q->head + 1) % q->max_len;
        q->len--;
    } while (!gdsi_log_md_is_fresh(common, target_rank, md)
            && q->len > 0);

    return md;
}

static gdsi_log_md_t log_put_stock_len(struct gdsi_gds_common *common,
        int target_rank)
{
    struct gdsi_log_stock_queue *q;

    q = common->log_db_stocks[target_rank];
    if (q == NULL)
        return 0;
    return q->len;
}

    static inline
uint64_t log_put_estimate_tail(const struct gdsi_gds_common *common,
        int target_rank)
{
    uint64_t m = common->log_tail_max[target_rank];
    if (m < common->log_tail_cache[target_rank])
        m = common->log_tail_cache[target_rank];

    return m;
}

static inline void log_put_update_tail_max(struct gdsi_gds_common *common,
        int target_rank, uint64_t max)
{
    if (common->log_tail_max[target_rank] < max)
        common->log_tail_max[target_rank] = max;
}

#define LOG_PUT_RETRY_ALLOC(_wq_, _len_)                                \
    do {                                                                \
        gdsi_dprintf(GDS_DEBUG_INFO,                                   \
                "[%u] We lost: target=%" PRIu64 " actual=%" PRIu64          \
                " target_rank=%d\n", (_wq_)->wq.id,                         \
                (_wq_)->log_tail_target, (_wq_)->log_tail_actual,           \
                (_wq_)->target_rank);                                       \
        \
        log_put_update_tail_max((_wq_)->wq.gds->common,                 \
                (_wq_)->target_rank,                                        \
                (_wq_)->log_tail_actual);                                   \
        \
        /* Retry */                                                     \
        (_wq_)->log_tail_estimate = log_put_estimate_tail(              \
                (_wq_)->wq.gds->common, (_wq_)->target_rank);               \
        (_wq_)->log_tail_target = (_wq_)->log_tail_estimate + (_len_);  \
        log_put_update_tail_max((_wq_)->wq.gds->common,                 \
                (_wq_)->target_rank,                                        \
                (_wq_)->log_tail_target);                                   \
        gdsi_dprintf(GDS_DEBUG_INFO,                                   \
                "[%u] (re) allocating data buffer:"                         \
                " target=%" PRIu64 " compare=%" PRIu64 " target_rank=%d\n", \
                (_wq_)->wq.id,                                              \
                (_wq_)->log_tail_target, (_wq_)->log_tail_estimate,         \
                (_wq_)->target_rank);                                       \
        GDSI_MPI_CALL(MPI_Compare_and_swap(&(_wq_)->log_tail_target,   \
                    &(_wq_)->log_tail_estimate, &(_wq_)->log_tail_actual,   \
                    MPI_UINT64_T, (_wq_)->target_rank, 0 /* target disp */, \
                    (_wq_)->wq.gds->common->win));                          \
        rma_issued((_wq_)->wq.gds->common);                             \
    } while (0)

static void log_put_before_done(struct log_put_workq *wq)
{
    struct gdsi_gds_common *common = wq->wq.gds->common;

    if (!gdsi_log_speculative_alloc
            || log_put_stock_len(common, wq->target_rank) == 0) {
        wq->state = GDS_LOG_PUT_DONE;
        return;
    }

    wq->log_tail_estimate = log_put_estimate_tail(common, wq->target_rank);
    wq->log_tail_target = wq->log_tail_estimate + common->log_user_bs;
    log_put_update_tail_max(common, wq->target_rank, wq->log_tail_target);
    gdsi_dprintf(GDS_DEBUG_INFO, "[%u] speculatively allocating data buffer:"
            " target=%" PRIu64 " compare=%" PRIu64 " target_rank=%d\n",
            wq->wq.id, wq->log_tail_target, wq->log_tail_estimate, wq->target_rank);
    GDSI_MPI_CALL(MPI_Compare_and_swap(&wq->log_tail_target,
                &wq->log_tail_estimate, &wq->log_tail_actual,
                MPI_UINT64_T, wq->target_rank, 0 /* target disp */, common->win));
    rma_issued(common);
    wq->state = GDS_LOG_PUT_SPEC_ALLOCATING;
}

static int log_put_test(struct gdsi_nbworkq_entry *wqe)
{
    struct log_put_workq *wq = (struct log_put_workq *) wqe;
    struct gdsi_gds_common *common = wqe->gds->common;
    int unit_size;
    size_t bs = common->log_user_bs;
    size_t mdi = gdsi_log_md_index(common, wq->target_rank, wq->target_offset);
    gdsi_log_md_t *md = gdsi_log_get_md_cache(common,
            wq->target_rank, wq->target_offset);

    log_check_limit(common, wq->target_rank, mdi + sizeof(gdsi_log_md_t));

    MPI_Type_size(common->type, &unit_size);

    switch (wq->state) {
        size_t ti;
        case GDS_LOG_PUT_ALLOCATING:
        if (wq->log_tail_actual % bs != 0)
            gdsi_panic("[%u] target=%" PRIu64 " actual=%" PRIu64
                    " target_rank=%d\n", wqe->id,
                    wq->log_tail_target, wq->log_tail_actual, wq->target_rank);

        common->log_tail_cache[wq->target_rank] = wq->log_tail_actual;
        if (gdsi_log_fetch_and_op) {
            wq->log_tail_target   = wq->log_tail_actual + wq->log_tail_diff;
            wq->log_tail_estimate = wq->log_tail_actual;
        }
        /* Fall through */
        case GDS_LOG_PUT_ALLOCATED:
        if (gdsi_log_md_is_fresh(common, wq->target_rank, *md)) {
#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_OW_MISS, 1);
#endif
            /* MD updated and already became fresh on remote node */
            log_put_direct(wq);

            /* Store the stock */
            if (wq->log_tail_actual == wq->log_tail_estimate)
                log_put_store_stock(common, wq->target_rank,
                        log_byteoff_to_md(wq->log_tail_target - bs, bs));

            if (wq->allocating)
                log_put_allocation_complete(common,
                        wq->target_rank, wq->target_offset);

            log_put_before_done(wq);
            return 0;
        }

        if (wq->log_tail_actual != wq->log_tail_estimate) {
#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_ALLOC_RETRY, 1);
#endif
            LOG_PUT_RETRY_ALLOC(wq, bs);
            return 0;
        }
        gdsi_dprintf(GDS_DEBUG_INFO,
                "[%u] We won: target=%" PRIu64 " actual=%" PRIu64
                " target_rank=%d\n", wqe->id,
                wq->log_tail_target, wq->log_tail_actual, wq->target_rank);
        common->log_tail_cache[wq->target_rank] = wq->log_tail_target;

        log_put_init_remotebuf(wq);
        return 0;

        case GDS_LOG_PUT_BUFFER_READY:
        wq->md_target = log_byteoff_to_md(wq->log_tail_target - bs, bs);
        wq->md_estimate = *md;

        if (gdsi_log_md_is_fresh(common, wq->target_rank, *md)) {
            gdsi_dprintf(GDS_DEBUG_INFO, "[%u] (BUFFER_READY) "
                    "MD became fresh: md=0x%x target_rank=%d mdi=%zu\n",
                    wqe->id, *md, wq->target_rank, mdi);
#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_OW_MISS, 1);
#endif
            log_put_direct(wq);
            log_put_store_stock(common, wq->target_rank, wq->md_target);
            if (wq->allocating)
                log_put_allocation_complete(common, wq->target_rank,
                        wq->target_offset);

            log_put_before_done(wq);
            return 0;
        }

        if (wq->acc_mode)
            MPI_Reduce_local(wq->local_buff, wq->pad_buf + wq->pad_front,
                    wq->count, common->type, wq->acc_op);

        if (wq->acc_mode || wq->pad_front > 0 || wq->pad_back > 0) {
            ti = wq->target_index * unit_size
                + log_md_to_byteoff(wq->md_target, bs);
            log_check_limit(common, wq->target_rank, ti + common->log_user_bs);
            GDSI_MPI_CALL(MPI_Put(wq->pad_buf, common->log_user_bs, MPI_BYTE,
                        wq->target_rank, ti, common->log_user_bs, MPI_BYTE,
                        common->win));
            wq->state = GDS_LOG_PUT_BUFFER_PADDING;
            if (gdsi_log_flush_on_padding)
                win_flush(common);
            else {
                rma_issued(common);
                return 0;
            }
        }

        /* Fall through */
        case GDS_LOG_PUT_BUFFER_PADDING:
        gdsi_dprintf(GDS_DEBUG_INFO, "[%u] (BUFFER_READY) "
                "Trying to update MD: target=0x%x compare=0x%x target_rank=%d "
                "mdi=%zu\n",
                wqe->id, wq->md_target, wq->md_estimate, wq->target_rank, mdi);
#ifdef GDS_CONFIG_COUNTER
        gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_MDUPDATE, 1);
#endif
        log_check_limit(common, wq->target_rank, mdi + sizeof(gdsi_log_md_t));
        GDSI_MPI_CALL(MPI_Compare_and_swap(&wq->md_target, &wq->md_estimate,
                    &wq->md_actual, MPI_UINT32_T, wq->target_rank, mdi,
                    common->win));
        rma_issued(common);

        wq->state = GDS_LOG_PUT_MD_UPDATING;
        return 0;

        case GDS_LOG_PUT_MD_UPDATING:
        if (wq->md_estimate != wq->md_actual) {
            /* We lost */
            gdsi_dprintf(GDS_DEBUG_INFO,
                    "[%u] We lost: MD: "
                    "cache=0x%x actual=0x%x fresh=%d target_rank=%d\n",
                    wqe->id, wq->md_estimate, wq->md_actual,
                    gdsi_log_md_is_fresh(common, wq->target_rank, wq->md_actual),
                    wq->target_rank);
            *md = wq->md_estimate = wq->md_actual;
            if (!gdsi_log_md_is_fresh(common, wq->target_rank, wq->md_estimate)
               ) {
                /* Retry */
                gdsi_dprintf(GDS_DEBUG_INFO, "[%u] (Retry) "
                        "Trying to update MD: target=0x%x compare=0x%x "
                        "target_rank=%d mdi=%zu\n",
                        wqe->id, wq->md_target, wq->md_estimate, wq->target_rank,
                        mdi);
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter,
                        GDS_COUNTER_PUT_MDUPDATE_RETRY, 1);
#endif
                log_check_limit(common, wq->target_rank,
                        mdi + sizeof(gdsi_log_md_t));
                GDSI_MPI_CALL(MPI_Compare_and_swap(&wq->md_target,
                            &wq->md_estimate, &wq->md_actual, MPI_UINT32_T,
                            wq->target_rank, mdi, common->win));
                rma_issued(common);
                return 0;
            }

#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_OW_MISS, 1);
#endif
            log_put_direct(wq);
            log_put_store_stock(common, wq->target_rank, wq->md_target);
            if (wq->allocating)
                log_put_allocation_complete(common, wq->target_rank,
                        wq->target_offset);
            log_put_before_done(wq);
            return 0;
        }

        gdsi_dprintf(GDS_DEBUG_INFO,
                "[%u] We won: MD: target=0x%x actual=0x%x target_rank=%d\n",
                wqe->id, wq->md_target, wq->md_actual, wq->target_rank);
        gdsi_log_flush_enqueue(common, wq->target_rank, wq->md_actual);
#ifdef GDS_CONFIG_COUNTER
        gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_FIRST, 1);
#endif
        *md = wq->md_target;
        gdsi_dprintf(GDS_DEBUG_INFO, "[%u] md=%p\n", wqe->id, md);

        if (wq->allocating)
            log_put_allocation_complete(common,
                    wq->target_rank, wq->target_offset);

        /* In this case we already put the user buffer contents to
           the remote target. */
        if (wq->pad_front > 0 || wq->pad_back > 0) {
            log_put_before_done(wq);
            if (wq->state == GDS_LOG_PUT_DONE)
                goto done_exit;
            else
                return 0;
        }

        log_put_direct(wq);

        log_put_before_done(wq);
        return 0;

        case GDS_LOG_PUT_BUFFER_WAITING:
        if (!gdsi_log_md_is_fresh(common, wq->target_rank, *md)) {
            gdsi_dprintf(GDS_DEBUG_INFO, "[%u] md=%p\n", wqe->id, md);
            gdsi_dprintf(GDS_DEBUG_INFO, "[%u] Not ready\n", wqe->id);
            return 0;
        }
#ifdef GDS_CONFIG_COUNTER
        gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_OW_HIT, 1);
#endif
        log_put_direct(wq);
        log_put_before_done(wq);
        return 0;

        case GDS_LOG_PUT_DONE:
done_exit:
        if (wq->pad_stride_type != MPI_DATATYPE_NULL)
            GDSI_MPI_CALL(MPI_Type_free(&wq->pad_stride_type));
        gdsi_dprintf(GDS_DEBUG_INFO, "[%u] done\n", wqe->id);
        return 1;

        case GDS_LOG_PUT_SPEC_ALLOCATING:
        /* Store the stock */
        common->log_tail_cache[wq->target_rank] = wq->log_tail_actual;
        if (wq->log_tail_actual == wq->log_tail_estimate)
            log_put_store_stock(common, wq->target_rank,
                    log_byteoff_to_md(wq->log_tail_target - bs, bs));
        wq->state = GDS_LOG_PUT_DONE;
        return 1;

        default:
        gdsi_panic("[%u] should not reach here!\n", wqe->id);
    }


    return 0;
}

/* Issue non-blocking operation for single block.
   Handles partial block as well */
static void log_put_issue_single(GDS_gds_t gds, const char *buff,
        int target_rank, size_t target_index, size_t target_offset, size_t count,
        size_t pad_front, size_t pad_back, bool acc_mode, MPI_Op acc_op)
{
    struct gdsi_gds_common *common = gds->common;
    struct log_put_workq *wq;
    size_t ti;
    size_t bs = common->log_user_bs;
    size_t n_blk_pf; /* no. of blocks to prefetch */
    gdsi_log_md_t *md = gdsi_log_get_md_cache(common,
            target_rank, target_offset);
    gdsi_log_md_t stock = 0;
    size_t mdi = gdsi_log_md_index(common, target_rank, target_offset);
    size_t pad_buf_size = (pad_front > 0 || pad_back > 0 || acc_mode)
        ? common->log_user_bs : 0;
    size_t off;
    int unit_size;

    MPI_Type_size(common->type, &unit_size);
    off = (target_index + target_offset) * unit_size;

    if (gdsi_log_md_is_fresh(common, target_rank, *md)) {
        size_t off_bytes = off
            - log_aligned_prev(off, common->log_user_bs);
        /* We already know the fresh memory location, just directly
           write to it */
        ti = target_index * unit_size + off_bytes
            + log_md_to_byteoff(*md, bs);
        gdsi_dprintf(GDS_DEBUG_INFO,
                "%s via fresh MD cache: target_rank=%d md=0x%08x "
                "target_disp=%zu count=%zu\n",
                acc_mode ? "Acc" : "Put",
                target_rank, *md, ti, count);
        log_check_limit(common, target_rank, ti + count * unit_size);
#ifdef GDS_CONFIG_COUNTER
        gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_OW_HIT, 1);
#endif
        if (acc_mode)
            MPI_Accumulate(buff, count, common->type, target_rank,
                    ti, count, common->type, acc_op, common->win);
        else
            MPI_Put(buff, count, common->type, target_rank, ti, count,
                    common->type, common->win);
        rma_issued(common);
        return;
    }

    gdsi_dprintf(GDS_DEBUG_INFO, "stale MD cache: target_rank=%d md=0x%08x\n",
            target_rank, *md);

    wq = malloc(sizeof(*wq) + pad_buf_size);
    gdsi_assert(wq); /* TODO: graceful recovery */

    nbworkq_rma_init(&wq->wq, gds, log_put_test);

    wq->local_buff = buff;
    wq->allocating = false;
    wq->target_rank = target_rank;
    wq->target_offset = target_offset;
    wq->target_index = target_index;
    wq->count = count;
    wq->pad_stride_type = MPI_DATATYPE_NULL;
    wq->pad_front = pad_front;
    wq->pad_back = pad_back;
    wq->acc_mode = acc_mode;
    wq->acc_op   = acc_op;

    n_blk_pf = (common->log_md_recvcounts[target_rank] * bs
            - log_aligned_prev(target_offset * unit_size, bs)) / bs;
    if (n_blk_pf > gdsi_log_md_prefetch_max)
        n_blk_pf = gdsi_log_md_prefetch_max;
    log_check_limit(common, wq->target_rank, mdi + sizeof(gdsi_log_md_t));
    GDSI_MPI_CALL(MPI_Get(md, n_blk_pf, MPI_UINT32_T, wq->target_rank, mdi,
                n_blk_pf, MPI_UINT32_T, common->win));
    rma_issued(common);

    if (wq->pad_front > 0 || wq->pad_back > 0) {
        wq->send_buf = wq->pad_buf;
        memcpy(wq->pad_buf + pad_front, buff, count * unit_size);
        if (wq->pad_front > 0 && wq->pad_back > 0) {
            int blens[2] = {wq->pad_front, wq->pad_back};
            MPI_Aint displs[2] = {0, common->log_user_bs - pad_back};
            MPI_Datatype types[2] = {MPI_BYTE, MPI_BYTE};
            GDSI_MPI_CALL(MPI_Type_create_struct(2, blens, displs, types,
                        &wq->pad_stride_type));
            GDSI_MPI_CALL(MPI_Type_commit(&wq->pad_stride_type));
        }
    } else
        wq->send_buf = buff;

    wq->state = log_put_need_allocation(common, target_rank, target_offset,
            &stock);
    switch (wq->state) {
        case GDS_LOG_PUT_INIT:
            gdsi_dprintf(GDS_DEBUG_INFO,
                    "need_allocation(rank=%d, offset=%zu) returned INIT\n",
                    target_rank, target_offset);

            wq->log_tail_estimate = log_put_estimate_tail(common, target_rank);
            wq->log_tail_target = wq->log_tail_estimate + bs;
            log_check_limit(common, wq->target_rank, wq->log_tail_target);
            log_put_update_tail_max(common, target_rank, wq->log_tail_target);
            gdsi_dprintf(GDS_DEBUG_INFO, "allocating data buffer: "
                    "target=%" PRIu64 " compare=%" PRIu64 " target_rank=%d\n",
                    wq->log_tail_target, wq->log_tail_estimate, target_rank);

            if (gdsi_log_fetch_and_op) {
                wq->log_tail_diff = bs;
                MPI_Fetch_and_op(&wq->log_tail_diff, &wq->log_tail_actual,
                        MPI_UINT64_T, target_rank, 0 /* target disp */,
                        MPI_SUM, common->win);
            } else
                GDSI_MPI_CALL(MPI_Compare_and_swap(&wq->log_tail_target,
                            &wq->log_tail_estimate, &wq->log_tail_actual,
                            MPI_UINT64_T, target_rank, 0 /* target disp */, common->win));
            rma_issued(common);
            wq->state = GDS_LOG_PUT_ALLOCATING;
            wq->allocating = true;
#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_ALLOC, 1);
#endif
            break;

        case GDS_LOG_PUT_ALLOCATED:
            gdsi_dprintf(GDS_DEBUG_INFO,
                    "need_allocation(rank=%d, offset=%zu) returned ALLOCATED\n",
                    target_rank, target_offset);

            wq->log_tail_actual = wq->log_tail_estimate = log_md_to_byteoff(stock, bs);
            wq->log_tail_target = wq->log_tail_actual + bs;
            log_put_test(&wq->wq);
            break;

        case GDS_LOG_PUT_BUFFER_WAITING:
            gdsi_dprintf(GDS_DEBUG_INFO,
                    "need_allocation(rank=%d, offset=%zu) returned WAITING\n",
                    target_rank, target_offset);

            log_put_test(&wq->wq);
            break;

        default:
            gdsi_panic("Should not reach here: state=%d\n", wq->state);
    }

    nbworkq_enqueue(&wq->wq);
}

static void log_put_m_direct(const struct log_put_m_workq *wq)
{
    struct gdsi_gds_common *common = wq->wq.gds->common;
    int unit_size;
    gdsi_log_md_t *mdc = gdsi_log_get_md_cache(common,
            wq->target_rank, wq->target_offset);

    MPI_Type_size(common->type, &unit_size);

    gdsi_dprintf(GDS_DEBUG_INFO, "*mdc=0x%x\n", *mdc);

    log_process_batch(common, wq->buff, wq->target_rank,
            wq->target_index * unit_size, mdc, wq->n_blocks, 0, 0,
            log_batch_put_op);
}

static void log_put_m_complete_allocation(struct log_put_m_workq *wq)
{
    struct gdsi_gds_common *common = wq->wq.gds->common;
    size_t i;
    int unit_size;

    if (!wq->allocating)
        return;

    MPI_Type_size(common->type, &unit_size);

    for (i = 0; i < wq->n_blocks; i++) {
        size_t off = common->log_user_bs / unit_size * i;
        log_put_allocation_complete(common,
                wq->target_rank, wq->target_offset + off);
    }

    wq->allocating = false;
}

static void log_put_issue_multiple(GDS_gds_t gds, void *buff,
        int target_rank, size_t target_index, size_t target_offset,
        size_t cont_blks);

static int log_put_m_test(struct gdsi_nbworkq_entry *wqe)
{
    struct log_put_m_workq *wq = (struct log_put_m_workq *) wqe;
    struct gdsi_gds_common *common = wqe->gds->common;
    int unit_size;
    size_t bs = common->log_user_bs;
    size_t mdi;
    gdsi_log_md_t *mdc = gdsi_log_get_md_cache(common,
            wq->target_rank, wq->target_offset);
    size_t i;

    MPI_Type_size(common->type, &unit_size);

    switch (wq->state) {
        bool all_fresh;
        case GDS_LOG_PUT_ALLOCATING:
        gdsi_assert(wq->log_tail_actual % bs == 0);
        common->log_tail_cache[wq->target_rank] = wq->log_tail_actual;
        if (gdsi_log_fetch_and_op) {
            wq->log_tail_target   = wq->log_tail_actual + wq->log_tail_diff;
            wq->log_tail_estimate = wq->log_tail_actual;
        }
        /* Fall through */
        case GDS_LOG_PUT_ALLOCATED:
        if (gdsi_log_md_is_fresh_any(common, wq->target_rank, mdc,
                    wq->n_blocks)) {
            gdsi_dprintf(GDS_DEBUG_WARN, "[%u] fresh_any(rank=%d,"
                    " target_offset=%zu, n_blocks=%zu)\n",
                    wqe->id, wq->target_rank, wq->target_offset, wq->n_blocks);

            if (wq->log_tail_actual == wq->log_tail_estimate)
                for (i = 0; i < wq->n_blocks; i++)
                    log_put_store_stock(common, wq->target_rank,
                            log_byteoff_to_md(wq->log_tail_actual, bs) + i);
            log_put_m_complete_allocation(wq);

            log_put_issue_multiple(wqe->gds, wq->buff, wq->target_rank,
                    wq->target_index, wq->target_offset, wq->n_blocks);
            wq->state = GDS_LOG_PUT_DONE;
            return 1;
        }

        if (wq->log_tail_actual != wq->log_tail_estimate) {
#ifdef GDS_CONFIG_COUNTER
            gdsi_counter_add(&common->counter,
                    GDS_COUNTER_PUT_ALLOC_RETRY, wq->n_blocks);
#endif
            LOG_PUT_RETRY_ALLOC(wq, bs * wq->n_blocks);
            return 0;
        }

        gdsi_dprintf(GDS_DEBUG_INFO,
                "[%u] We won: target=%" PRIu64 " actual=%" PRIu64
                " target_rank=%d\n", wqe->id,
                wq->log_tail_target, wq->log_tail_actual, wq->target_rank);
        common->log_tail_cache[wq->target_rank] = wq->log_tail_target;
        wq->state = GDS_LOG_PUT_BUFFER_READY;
        /* Fall through */
        case GDS_LOG_PUT_BUFFER_READY:
        all_fresh = true;
        for (i = 0; i < wq->n_blocks; i++) {
            wq->md_targets[i] = log_byteoff_to_md(
                    wq->log_tail_target - bs * (wq->n_blocks - i), bs);
            wq->md_estimates[i] = mdc[i];
            if (!gdsi_log_md_is_fresh(common, wq->target_rank, mdc[i])) {
                all_fresh = false;
                mdi = gdsi_log_md_index(common,
                        wq->target_rank,
                        wq->target_offset) + sizeof(gdsi_log_md_t) * i;
                gdsi_dprintf(GDS_DEBUG_INFO, "[%u] (BUFFER_READY) "
                        "Trying to update MD: target=0x%x compare=0x%x "
                        "target_rank=%d mdi=%zu\n",
                        wqe->id, wq->md_targets[i], wq->md_estimates[i],
                        wq->target_rank, mdi);
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_MDUPDATE, 1);
#endif
                log_check_limit(common, wq->target_rank,
                        mdi + sizeof(gdsi_log_md_t));
                GDSI_MPI_CALL(MPI_Compare_and_swap(&wq->md_targets[i],
                            &wq->md_estimates[i], &wq->md_actuals[i],
                            MPI_UINT32_T, wq->target_rank, mdi, common->win));
                rma_issued(common);
            } else {
                /* Someone else has installed a new block */
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_OW_MISS, 1);
#endif
                log_put_store_stock(common, wq->target_rank,
                        log_byteoff_to_md(wq->log_tail_actual, bs) + i);
            }
        }

        if (all_fresh) {
            log_put_m_direct(wq);
            log_put_m_complete_allocation(wq);
            wq->state = GDS_LOG_PUT_DONE;
            return 1;
        }
        wq->state = GDS_LOG_PUT_MD_UPDATING;
        return 0;

        case GDS_LOG_PUT_MD_UPDATING:
        all_fresh = true;
        for (i = 0; i < wq->n_blocks; i++) {
            if (wq->md_estimates[i] == wq->md_actuals[i]) {
                /* We won */
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_FIRST, 1);
#endif
                mdc[i] = wq->md_targets[i];
                gdsi_dprintf(GDS_DEBUG_INFO,
                        "[%u] We won: MD[%zu]: actual=0x%x target_rank=%d\n",
                        wqe->id, i, wq->md_actuals[i], wq->target_rank);
                gdsi_log_flush_enqueue(common, wq->target_rank, wq->md_actuals[i]);
            } else {
                /* We lost */
                gdsi_dprintf(GDS_DEBUG_INFO,
                        "[%u] We lost: MD[%zu]: "
                        "cache=0x%x actual=0x%x fresh=%d target_rank=%d\n",
                        wqe->id, i, wq->md_estimates[i], wq->md_actuals[i],
                        gdsi_log_md_is_fresh(common, wq->target_rank,
                            wq->md_actuals[i]),
                        wq->target_rank);
                mdc[i] = wq->md_estimates[i] = wq->md_actuals[i];
                if (!gdsi_log_md_is_fresh(common, wq->target_rank, mdc[i])) {
                    /* Retry */
                    all_fresh = false;
                    mdi = gdsi_log_md_index(common,
                            wq->target_rank,
                            wq->target_offset) + sizeof(gdsi_log_md_t) * i;
                    gdsi_dprintf(GDS_DEBUG_INFO, "[%u] (Retry) "
                            "Trying to update MD: target=0x%x compare=0x%x "
                            "target_rank=%d mdi=%zu\n",
                            wqe->id, wq->md_targets[i], wq->md_estimates[i],
                            wq->target_rank, mdi);
#ifdef GDS_CONFIG_COUNTER
                    gdsi_counter_add(&common->counter,
                            GDS_COUNTER_PUT_MDUPDATE, 1);
#endif
                    log_check_limit(common, wq->target_rank,
                            mdi + sizeof(gdsi_log_md_t));
                    GDSI_MPI_CALL(MPI_Compare_and_swap(&wq->md_targets[i],
                                &wq->md_estimates[i], &wq->md_actuals[i],
                                MPI_UINT32_T, wq->target_rank, mdi, common->win));
                    rma_issued(common);
                    continue;
                }
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_OW_MISS, 1);
#endif
                log_put_store_stock(common, wq->target_rank,
                        log_byteoff_to_md(wq->log_tail_actual, bs) + i);
            }
        }
        log_put_m_complete_allocation(wq);
        if (all_fresh) {
            log_put_m_direct(wq);
            wq->state = GDS_LOG_PUT_DONE;
            return 1;
        }
        return 0;

        case GDS_LOG_PUT_BUFFER_WAITING:
        for (i = 0; i < wq->n_blocks; i++) {
            if (!gdsi_log_md_is_fresh(common, wq->target_rank, mdc[i])) {
                gdsi_dprintf(GDS_DEBUG_INFO, "[%u] Not ready\n", wqe->id);
                return 0;
            }
        }
        /* Fall through */
        case GDS_LOG_PUT_OVERWRITE:
#ifdef GDS_CONFIG_COUNTER
        gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_OW_HIT, wq->n_blocks);
#endif
        log_put_m_direct(wq);
        wq->state = GDS_LOG_PUT_DONE;
        return 0;

        case GDS_LOG_PUT_DONE:
        gdsi_dprintf(GDS_DEBUG_INFO, "[%u] done\n", wqe->id);
        return 1;

        default:
        gdsi_panic("[%u] should not reach here!\n", wqe->id);
    }
}

static void log_put_hyb_alloc_complete(struct log_put_hyb_workq *wq)
{
    struct gdsi_gds_common *common = wq->wq.gds->common;
    gdsi_log_md_t *mdc = gdsi_log_get_md_cache(common, wq->target_rank,
            wq->target_index + wq->target_offset);
    int unit_size;

    MPI_Type_size(common->type, &unit_size);

    log_process_batch(common, wq->buff, wq->target_rank,
            wq->target_index * unit_size, mdc, wq->n_blocks,
            wq->pad_front, wq->pad_back,
            log_batch_put_op);
    wq->state = GDS_LOG_HYB_BUF_READY;
    wq->wq.request = NULL; wq->wq.ready = NULL;

    free(wq->alloc_wq);
    wq->alloc_wq = NULL;
}

static int log_put_hyb_ready(struct gdsi_nbworkq_entry *wqe,
        MPI_Status *status)
{
    struct log_put_hyb_workq *wq = (struct log_put_hyb_workq *) wqe;
    int r;

    switch (wq->state) {
        case GDS_LOG_HYB_ISSUED:
            r = wq->alloc_wq->ready(wq->alloc_wq, status);
            if (r)
                log_put_hyb_alloc_complete(wq);
            return 0;

        case GDS_LOG_HYB_BUF_READY:
            gdsi_dprintf(GDS_DEBUG_ERR, "[%u] Why came here?\n", wqe->id);
            return 0;

        case GDS_LOG_HYB_DONE:
            return 1;
    }

    gdsi_panic("[%u] Should not reach here!\n", wqe->id);

    return 0;
}

GDS_status_t GDS_create(GDS_size_t ndims, const GDS_size_t count[],
        GDS_datatype_t element_type,
        GDS_global_to_local_func_t global_to_local_function,
        GDS_local_to_global_func_t local_to_global_function, void *local_buffer,
        GDS_size_t local_buffer_count, GDS_priority_t resilience_priority,
        GDS_comm_t users, GDS_info_t info, GDS_gds_t *gds)
{
    struct GDS_gds *gds_internal;

    GDS_status_t ret;

    gds_internal = calloc(sizeof(*gds_internal), 1);
    if (gds_internal == NULL)
        return GDS_STATUS_NOMEM;

    if (info == 0) {
        gdsi_dprintf(GDS_DEBUG_ERR,
                "WARNING: NULL is no longer valid as the info argument for GDS_create. Use MPI_INFO_NULL instead.\n");
        info = MPI_INFO_NULL;
    }

    ret = gdsi_create(ndims, count, element_type, global_to_local_function,
            local_to_global_function, local_buffer, local_buffer_count, users,
            info, &gds_internal->common);
    if (ret != GDS_STATUS_OK)
        goto out_free_gds;
    gds_internal->common->version_inc_state = 0;
    gds_internal->common->get_info = 0;

    /* At this moment, no other thread is using this object,
       therefore we can touch the common structure without holding lock */
    gds_internal->common->n_reqs = 0;
    gds_internal->common->refcount = 1;
    gds_internal->chunk_set = gds_internal->common->chunk_sets;
    gds_internal->chunk_set->label = NULL;
    gds_internal->chunk_set->label_size = 0;
    if (gds_internal->common->representation == GDSI_REPR_LOG)
        gds_internal->common->ops = &log_ops;
    else
        gds_internal->common->ops = &flat_ops;
    *gds = gds_internal;
    add_to_gdsi_gds_common_list(gds_internal->common);

    MPI_Win_lock_all(0, (*gds)->common->win);
#ifdef GDS_CONFIG_USE_LRDS
    gdsi_register_lrds_callback(gds_internal->common);
    if ((*gds)->common->lrds_dirty_win != MPI_WIN_NULL)
        MPI_Win_lock_all(0, (*gds)->common->lrds_dirty_win);
#endif

    return GDS_STATUS_OK;

out_free_gds:
    free(gds_internal);

    return ret;
}

static MPI_Request *log_put_hyb_request(struct gdsi_nbworkq_entry *wqe)
{
    struct log_put_hyb_workq *wq = (struct log_put_hyb_workq *) wqe;

    if (wq->state == GDS_LOG_HYB_ISSUED) {
        gdsi_assert(wq->alloc_wq);
        return wq->alloc_wq->request(wq->alloc_wq);
    }

    gdsi_dprintf(GDS_DEBUG_ERR,
            "[%u] log_put_hyb_request called for state %d\n", wqe->id, wq->state);

    return NULL;
}

static int log_put_hyb_test(struct gdsi_nbworkq_entry *wqe)
{
    struct log_put_hyb_workq *wq = (struct log_put_hyb_workq *) wqe;
    struct gdsi_gds_common *common = wqe->gds->common;
    int unit_size;
    int flag, r;
    MPI_Status status;
    MPI_Request *req;

    MPI_Type_size(common->type, &unit_size);

    switch (wq->state) {
        case GDS_LOG_HYB_ISSUED:
            req = wq->alloc_wq->request(wq->alloc_wq);
            MPI_Test(req, &flag, &status);
            if (flag) {
                r = wq->alloc_wq->ready(wq->alloc_wq, &status);
                if (r)
                    log_put_hyb_alloc_complete(wq);
            }
            return 0;

        case GDS_LOG_HYB_BUF_READY:
            wq->state = GDS_LOG_HYB_DONE;
        case GDS_LOG_HYB_DONE:
            return 1;
    }

    gdsi_panic("[%u] Should not reach here!\n", wqe->id);
    return 0;
}

static void log_put_issue_multiple(GDS_gds_t gds, void *buff,
        int target_rank, size_t target_index, size_t target_offset,
        size_t cont_blks)
{
    struct gdsi_gds_common *common = gds->common;
    size_t cont_len, bs;
    int unit_size;

    if (cont_blks == 0)
        return;

    MPI_Type_size(common->type, &unit_size);
    bs = common->log_user_bs;

    while (cont_blks > 0) {
        struct log_put_m_workq *wq;
        gdsi_log_md_t stock = 0;
        size_t cb = cont_blks;

        wq = malloc(sizeof(*wq));
        gdsi_assert(wq); /* TODO: graceful recovery */

        nbworkq_rma_init(&wq->wq, gds, log_put_m_test);

        if (cb > GDSI_LOG_RMA_PUT_BATCH)
            cb = GDSI_LOG_RMA_PUT_BATCH;

        wq->allocating = false;
        wq->target_rank = target_rank;
        wq->target_offset = target_offset;
        wq->target_index = target_index;
        wq->state = log_put_need_allocation_range(common, target_rank,
                target_offset, cb, &cont_len, &stock);
        wq->n_blocks = cont_len;
        wq->buff = buff;

        switch (wq->state) {
            case GDS_LOG_PUT_INIT:
                gdsi_dprintf(GDS_DEBUG_INFO,
                        "need_allocation_range(rank=%d, offset=%zu) returned INIT"
                        " for %zu blocks\n",
                        target_rank, target_offset, cont_len);

                /* Remote memory allocation */
                wq->log_tail_estimate = log_put_estimate_tail(common, target_rank);
                wq->log_tail_target = wq->log_tail_estimate + cont_len * bs;
                log_check_limit(common, wq->target_rank, wq->log_tail_target);
                log_put_update_tail_max(common, target_rank, wq->log_tail_target);
                gdsi_dprintf(GDS_DEBUG_INFO, "allocating data buffer: "
                        "target=%" PRIu64 " compare=%" PRIu64 " target_rank=%d\n",
                        wq->log_tail_target, wq->log_tail_estimate, target_rank);
#ifdef GDS_CONFIG_COUNTER
                gdsi_counter_add(&common->counter, GDS_COUNTER_PUT_ALLOC, cont_len);
#endif
                if (gdsi_log_fetch_and_op) {
                    wq->log_tail_diff = cont_len * bs;
                    MPI_Fetch_and_op(&wq->log_tail_diff, &wq->log_tail_actual,
                            MPI_UINT64_T, target_rank, 0 /* target disp */,
                            MPI_SUM, common->win);
                } else
                    GDSI_MPI_CALL(MPI_Compare_and_swap(&wq->log_tail_target,
                                &wq->log_tail_estimate, &wq->log_tail_actual,
                                MPI_UINT64_T, target_rank, 0 /* target disp */, common->win));
                rma_issued(common);
                wq->state = GDS_LOG_PUT_ALLOCATING;
                wq->allocating = true;
                break;

            case GDS_LOG_PUT_ALLOCATED:
                gdsi_dprintf(GDS_DEBUG_INFO,
                        "need_allocation_range(rank=%d, offset=%zu) returned ALLOCATED"
                        " for %zu blocks\n",
                        target_rank, target_offset, cont_len);
                wq->log_tail_actual = wq->log_tail_estimate
                    = log_md_to_byteoff(stock, bs);
                wq->log_tail_target = wq->log_tail_actual + bs * cont_len;
                log_put_m_test(&wq->wq);
                break;

            case GDS_LOG_PUT_OVERWRITE:
                gdsi_dprintf(GDS_DEBUG_INFO,
                        "need_allocation_range(rank=%d, offset=%zu) returned OVERWRITE"
                        " for %zu blocks\n",
                        target_rank, target_offset, cont_len);
                log_put_m_test(&wq->wq);
                break;

            case GDS_LOG_PUT_BUFFER_WAITING:
                gdsi_dprintf(GDS_DEBUG_INFO,
                        "need_allocation_range(rank=%d, offset=%zu) returned WAITING"
                        " for %zu blocks\n",
                        target_rank, target_offset, cont_len);
                log_put_m_test(&wq->wq);
                break;

            default:
                gdsi_panic("Should not reach here: state=%d\n", wq->state);
        }

        buff += cont_len * common->log_user_bs;
        target_offset += cont_len * common->log_user_bs / unit_size;
        cont_blks -= cont_len;

        nbworkq_enqueue(&wq->wq);
    }
}

static void log_put_issue_hybrid(GDS_gds_t gds, void *buff,
        int target_rank, size_t target_index, size_t target_offset,
        size_t count)
{
    struct log_put_hyb_workq *wq;
    struct gdsi_gds_common *common = gds->common;
    size_t n_blocks, i, pad_front, pad_back;
    gdsi_log_md_t *mdc = gdsi_log_get_md_cache(common, target_rank,
            target_index + target_offset);
    int unit_size;
    bool all_fresh = true;

    MPI_Type_size(common->type, &unit_size);

    wq = malloc(sizeof(*wq));
    gdsi_assert(wq); /* TODO: graceful recovery */

    n_blocks = log_required_blocks(target_offset * unit_size,
            count * unit_size, common->log_user_bs);
    pad_front = log_pad_front(target_offset * unit_size, common->log_user_bs);
    pad_back  = log_pad_back((target_offset + count) * unit_size, common->log_user_bs);

    for (i = 0; i < n_blocks; i++)
        if (!gdsi_log_md_is_fresh(common, target_rank, mdc[i])) {
            all_fresh = false;
            break;
        }

    if (all_fresh) {
        log_process_batch(common, buff, target_rank,
                target_index * unit_size, mdc, n_blocks, pad_front, pad_back,
                log_batch_put_op);
        return;
    }

    gdsi_nbworkq_init(&wq->wq, gds,
            log_put_hyb_test, log_put_hyb_request, log_put_hyb_ready);
    gdsi_log_alloc(gds, target_rank, target_offset, count, &wq->alloc_wq);
    wq->state = GDS_LOG_HYB_ISSUED;
    wq->target_rank = target_rank;
    wq->target_index = target_index;
    wq->target_offset = target_offset;
    wq->n_blocks = n_blocks;
    wq->pad_front = pad_front;
    wq->pad_back = pad_back;
    wq->buff = buff;

    gdsi_assert(wq->alloc_wq != NULL);

    nbworkq_enqueue(&wq->wq);
}

static void put_lambda_log(GDS_gds_t gds, size_t local_offset,
        int target_rank, size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args)
{
    /* Checking the buffer of newest version */
    if (gds->common->version_inc_state) {
        gdsi_check_new_version(gds->common, target_rank, target_index, chunk_number, gds->common->latest_version);
    }

    struct gdsi_gds_common *common = gds->common;
    int unit_size;
    MPI_Datatype mpi_type = (MPI_Datatype) common->type;
    char *buff;
    size_t off, end, pad_front, pad_back, n_blocks, i, ct, pdb, cont_blks;

    MPI_Type_size(mpi_type, &unit_size);

    buff = args->buf + local_offset * unit_size;

    if (common->put_impl == GDS_PUT_IMPL_MSG
            || !gdsi_chunk_rma_accessible(target_index)) {
        struct gdsi_nbworkq_entry *wqe;
        gdsi_indirect_put(gds, target_rank, buff, target_offset, count,
                false, MPI_NO_OP, &wqe);
        nbworkq_enqueue(wqe);
        return;
    } else if (common->put_impl == GDS_PUT_IMPL_HYB) {
        log_put_issue_hybrid(gds, buff,
                target_rank, target_index, target_offset, count);
        return;
    }

    off = (target_index + target_offset) * unit_size;
    end = off + count * unit_size;
    pad_front = log_pad_front(off, common->log_user_bs);
    pad_back = log_pad_back(end, common->log_user_bs);

    n_blocks = log_required_blocks(off, count * unit_size, common->log_user_bs);

    i = 0;
    pdb = n_blocks == 1 ? pad_back : 0;
    if (pad_front > 0 || pdb > 0) {
        ct = (common->log_user_bs - pad_front) / unit_size;
        if (count < ct)
            ct = count;

        log_put_issue_single(gds, buff, target_rank, target_index,
                target_offset, ct, pad_front, pdb, false, MPI_NO_OP);
        buff += ct * unit_size;
        target_offset += ct;
        i++;
    }
    cont_blks = n_blocks - i;
    if (n_blocks > 1 && pad_back > 0)
        cont_blks--;

    ct = common->log_user_bs / unit_size;
    if (cont_blks > 0) {
        log_put_issue_multiple(gds, buff, target_rank, target_index,
                target_offset, cont_blks);
        buff += ct * unit_size * cont_blks;
        target_offset += ct * cont_blks;
    }

    if (n_blocks - cont_blks - i > 0) {
        ct = (common->log_user_bs - pad_back) / unit_size;
        log_put_issue_single(gds, buff, target_rank, target_index,
                target_offset, ct, 0, pad_back, false, MPI_NO_OP);
    }
}

/* 
   For version_inc_nb: consider the correct version to put.

   case 1: put to previous version
   case 2: put to current version
 */
GDS_status_t GDS_put(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds)
{
    /* Check and handle local error first */
    local_error_poll_handle(gds);

    /* GDS_version_inc() completion checking */
    finish_version_inc_test(gds);

    /* Forbid put to old version */
    if (gds->common->latest_version > gds->chunk_set->version) {
        /* Current version is smaller than latest version, indicating it's an
         * old version */
        return GDS_STATUS_INVALID;
    }

    /* Sanity checking */
    if (gdsi_sanity_check) {
        gdsi_index_bound_check(lo_index, hi_index, gds);
    }

    char dbg_buff[128];
    snprint_coord(dbg_buff, sizeof(dbg_buff), gds->common->ndims, lo_index, hi_index);
    gdsi_dprintf(GDS_DEBUG_INFO, "GDS_put: %s\n", dbg_buff);


#ifdef GDS_CONFIG_COUNTER
    {
        int typelen;
        MPI_Type_size(gds->common->type, &typelen);
        size_t len = typelen;
        int i;
        /* TODO: consider ld correctly */
        for (i = 0; i < gds->common->ndims; i++)
            len *= hi_index[i] - lo_index[i] + 1;
        gdsi_counter_add(&gds->common->counter, GDS_COUNTER_PUT, len);
    }
#endif

    struct gdsi_op_args args;
    args.buf = origin_addr;
    iterate_array_top(gds, lo_index, hi_index, origin_ld,
            gds->common->ops->put, &args);

    rma_post_check(gds->common);

    return GDS_STATUS_OK;
}

static void acc_lambda_flat(GDS_gds_t gds,
        size_t local_offset, int target_rank,
        size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args)
{
    int mpi_type_size;
    MPI_Datatype mpi_type = (MPI_Datatype)gds->common->type;
    MPI_Type_size(mpi_type, &mpi_type_size);
    const void *buff = args->buf + local_offset * mpi_type_size;

    if (gds->common->put_impl == GDS_PUT_IMPL_MSG
            || !gdsi_chunk_rma_accessible(target_index)) {
        struct gdsi_nbworkq_entry *wqe;
        gdsi_indirect_put(gds, target_rank, buff, target_offset, count,
                true, args->op, &wqe);
        nbworkq_enqueue(wqe);
    } else {
        MPI_Accumulate(buff, count, mpi_type,
                target_rank, target_index + target_offset,
                count, mpi_type, (MPI_Op)args->op, gds->common->win);
        rma_issued(gds->common);
    }
}

static void acc_lambda_log(GDS_gds_t gds,
        size_t local_offset, int target_rank,
        size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args)
{
    struct gdsi_gds_common *common = gds->common;
    int unit_size;
    MPI_Datatype mpi_type = (MPI_Datatype) common->type;
    char *buff;
    size_t off, end, pad_front, pad_back = 0, n_blocks, i, ct;

    MPI_Type_size(mpi_type, &unit_size);

    buff = args->buf + local_offset * unit_size;

    if (common->put_impl == GDS_PUT_IMPL_MSG
            || !gdsi_chunk_rma_accessible(target_index)) {
        struct gdsi_nbworkq_entry *wqe;
        gdsi_indirect_put(gds, target_rank, buff, target_offset, count,
                true, args->op, &wqe);
        nbworkq_enqueue(wqe);
        return;
    }

    off = (target_index + target_offset) * unit_size;
    end = off + count * unit_size;

    pad_front = log_pad_front(off, common->log_user_bs);
    n_blocks = log_required_blocks(off, count * unit_size, common->log_user_bs);

    for (i = 0; i < n_blocks; i++) {
        ct = (common->log_user_bs - pad_front) / unit_size;
        if (count < ct)
            ct = count;
        if (i == n_blocks - 1)
            pad_back = log_pad_back(end, common->log_user_bs);

        log_put_issue_single(gds, buff, target_rank, target_index,
                target_offset, ct, pad_front, pad_back, true, args->op);
        buff += ct * unit_size;
        target_offset += ct;
        pad_front = 0;
    }
}

GDS_status_t GDS_acc(void *origin_addr, GDS_size_t origin_ld[], GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_op_t accumulate_op, GDS_gds_t gds)
{
    struct gdsi_op_args args;

    /* Check and handle local error first */
    local_error_poll_handle(gds);

    /* Forbidden write to old versions */
    if (gds->common->latest_version != gds->chunk_set->version) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Can't put to old version %zu. Latest version %zu\n", gds->chunk_set->version, gds->common->latest_version);
        return GDS_STATUS_INVALID;
    }

    /* Sanity checking */
    if (gdsi_sanity_check)
        gdsi_index_bound_check(lo_index, hi_index, gds);

#ifdef GDS_CONFIG_COUNTER
    {
        int typelen;
        MPI_Type_size(gds->common->type, &typelen);
        size_t len = typelen;
        int i;
        /* TODO: consider ld correctly */
        for (i = 0; i < gds->common->ndims; i++)
            len *= hi_index[i] - lo_index[i] + 1;
        gdsi_counter_add(&gds->common->counter, GDS_COUNTER_ACC, len);
    }
#endif

    args.buf = origin_addr;
    args.op = accumulate_op;
    iterate_array_top(gds, lo_index, hi_index, origin_ld,
            gds->common->ops->acc, &args);

    rma_post_check(gds->common);

    return GDS_STATUS_OK;
}

static void compare_and_swap_lambda_flat(GDS_gds_t gds, size_t local_offset,
        int target_rank, size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args)
{
    MPI_Datatype mpi_type = (MPI_Datatype) gds->common->type;

    MPI_Compare_and_swap(args->buf2, args->buf, args->buf3, mpi_type,
            target_rank, target_index + target_offset, gds->common->win);
    rma_issued(gds->common);
}

static void compare_and_swap_lambda_log(GDS_gds_t gds, size_t local_offset,
        int target_rank, size_t target_index, size_t target_offset,
        size_t count, int chunk_number, struct gdsi_op_args *args)
{
    /* TODO */
    gdsi_panic("compare_and_swap_lambda_log not implemented.\n");
}

GDS_status_t GDS_compare_and_swap(void *comp_addr, void *swap_src_addr,
        void *swap_res_addr, GDS_size_t offset[], GDS_gds_t gds)
{
    struct gdsi_op_args args;
    GDS_size_t ld[8] = {1,1,1,1,1,1,1,1}; /* TODO: max 8-dimension, or use malloc? */
    GDS_size_t *lo_idx, *hi_idx;

    local_error_poll_handle(gds);

    /* GDS_version_inc() completion checking */
    finish_version_inc_test(gds);

    /* Forbid put to old version */
    if (gds->common->latest_version > gds->chunk_set->version) {
        /* Current version is smaller than latest version, indicating it's an
         * old version */
        return GDS_STATUS_INVALID;
    }


    /* comp_addr, swap_src_arr, swap_res_addr are all local buffers in one
     * element size, thus calculate target address only */

    /* synthesize arguments for iterate_array_top */
    args.buf = comp_addr;
    args.buf2 = swap_src_addr;
    args.buf3 = swap_res_addr;
    lo_idx = offset;
    hi_idx = offset;

    iterate_array_top(gds, lo_idx, hi_idx, ld,
            gds->common->ops->compare_and_swap, &args);
    rma_post_check(gds->common);

    return GDS_STATUS_OK;
}

static void get_acc_lambda_flat(GDS_gds_t gds,
        size_t orig_offset, size_t res_offset,
        int target_rank, size_t target_index,
        size_t target_offset, size_t count, int chunk_number, struct gdsi_op_args *args)
{
    int mpi_type_size;
    MPI_Datatype mpi_type = (MPI_Datatype) gds->common->type;
    MPI_Type_size(mpi_type, &mpi_type_size);
    const void *orig_buf = args->buf + orig_offset * mpi_type_size;
    void *res_buf = args->buf2 + res_offset * mpi_type_size;

    if (gds->common->put_impl == GDS_PUT_IMPL_MSG
            || !gdsi_chunk_rma_accessible(target_index)) {
        struct gdsi_nbworkq_entry *wq;
        gdsi_indirect_fetch(gds, target_rank, res_buf,
                target_offset, count, chunk_number, &wq);
        nbworkq_enqueue(wq);
        gdsi_indirect_put(gds, target_rank, orig_buf,
                target_offset, count, true, args->op, &wq);
        nbworkq_enqueue(wq);
    } else {
        MPI_Get_accumulate(orig_buf, count, mpi_type,
                res_buf, count, mpi_type,
                target_rank, target_index + target_offset,
                count, mpi_type, (MPI_Op) args->op, gds->common->win);
        rma_issued(gds->common);
    }
}

static void get_acc_lambda_log(GDS_gds_t gds,
        size_t origin_offset, size_t result_offset,
        int target_rank, size_t target_index,
        size_t target_offset, size_t count, int chunk_number, struct gdsi_op_args *args)
{
    /* TODO */
    gdsi_panic("get_acc_lambda_log() not implemented.\n");
}

static void iterate_array_get_acc(GDS_gds_t gds, size_t dim,
        size_t *orig_buf_offset, size_t *res_buf_offset, size_t focus[],
        const size_t lo_index[], const size_t hi_index[],
        const size_t orig_ld[], const size_t res_ld[],
        chunk_lambda_get_acc_t lambda, struct gdsi_op_args *lambda_args)
{
    size_t i, hi;
    const int ndims = gds->common->ndims;

    if (dim > 0) {
        hi = GDSI_MIN(hi_index[dim], lo_index[dim] + orig_ld[dim-1] - 1);
        hi = GDSI_MIN(hi, lo_index[dim] + res_ld[dim-1] - 1);
    } else
        hi = hi_index[dim];

    if (dim == ndims - 1) {
        gdsi_chunk_iterator_t it;
        GDS_status_t s;

        focus[dim] = i = lo_index[dim];
        s = gdsi_find_first_chunkit(gds, focus, &it);
        assert(s == GDS_STATUS_OK);

        for (;;) {
            struct gdsi_chunk chunk;

            gdsi_chunkit_access(&it, &chunk);

            gdsi_dprintf(GDS_DEBUG_INFO, "chunk."
                    "{target_rma_index=%zu, target_offset=%zu, size=%zu}\n",
                    chunk.target_rma_index, chunk.target_offset, chunk.size);

            size_t contig_len = GDSI_MIN(chunk.size, hi+1-i);

            lambda(gds, *orig_buf_offset, *res_buf_offset,
                    chunk.target_rank, chunk.target_rma_index,
                    chunk.target_offset, contig_len, chunk.chunk_number, lambda_args);
            i += contig_len;
            *orig_buf_offset += contig_len;
            *res_buf_offset += contig_len;

            if (i > hi) break;

            if (gdsi_chunkit_end(&it))
                gdsi_panic("chunkit ran out of elements with %zu elements"
                        "remaining\n", hi-i);
            else
                gdsi_chunkit_forward(&it);
        }

        if (0 < dim
                && lo_index[dim] + orig_ld[dim-1] > hi_index[dim] + 1) {
            size_t skip = lo_index[dim] + orig_ld[dim-1] - hi_index[dim] - 1;
            *orig_buf_offset += skip;
        }
        if (0 < dim
                && lo_index[dim] + res_ld[dim-1] > hi_index[dim] + 1) {
            size_t skip = lo_index[dim] + res_ld[dim-1] - hi_index[dim] - 1;
            *res_buf_offset += skip;
        }
    } else {
        for (i = lo_index[dim]; i <= hi; i++) {
            focus[dim] = i;
            iterate_array_get_acc(gds, dim+1, orig_buf_offset,
                    res_buf_offset, focus, lo_index, hi_index,
                    orig_ld, res_ld, lambda, lambda_args);
        }
        if (0 < dim
                && lo_index[dim] + orig_ld[dim-1] > hi_index[dim] + 1) {
            size_t skip = lo_index[dim] + orig_ld[dim-1] - hi_index[dim] - 1;
            for (i = dim+1; i < ndims; i++)
                skip *= gds->common->nelements[i];
            *orig_buf_offset += skip;
        }
        if (0 < dim
                && lo_index[dim] + res_ld[dim-1] > hi_index[dim] + 1) {
            size_t skip = lo_index[dim] + res_ld[dim-1] - hi_index[dim] - 1;
            for (i = dim+1; i < ndims; i++)
                skip *= gds->common->nelements[i];
            *res_buf_offset += skip;
        }
    }
}

static void iterate_array_top_get_acc(GDS_gds_t gds, const size_t lo_index[],
        const size_t hi_index[], const size_t orig_ld[], const size_t res_ld[],
        chunk_lambda_get_acc_t lambda, struct gdsi_op_args *lambda_args)
{
    size_t ndims = gds->common->ndims;
    size_t focus[ndims];
    size_t orig_buf_offset = 0, res_buf_offset = 0;

    if (gds->common->order != GDSI_ORDER_COL_MAJOR)
        iterate_array_get_acc(gds, 0, &orig_buf_offset, &res_buf_offset,
                focus, lo_index, hi_index, orig_ld, res_ld, lambda, lambda_args);
    else {
        size_t ndims = gds->common->ndims;
        size_t lot[ndims], hit[ndims];
        size_t orig_ldt[ndims-1], res_ldt[ndims-1];
        size_t i;
        for (i = 0; i < ndims-1; i++) {
            lot[i] = lo_index[ndims-1-i];
            hit[i] = hi_index[ndims-1-i];
            orig_ldt[i] = orig_ld[ndims-2-i];
            res_ldt[ndims-1] = res_ld[ndims-2-i];
        }
        lot[i] = lo_index[ndims-1-i];
        hit[i] = hi_index[ndims-1-i];

        iterate_array_get_acc(gds, 0, &orig_buf_offset, &res_buf_offset,
                focus, lot, hit, orig_ldt, res_ldt, lambda, lambda_args);
    }
}

GDS_status_t GDS_get_acc(void *origin_addr, GDS_size_t origin_ld[],
        void *result_addr, GDS_size_t result_ld[], GDS_size_t lo_index[],
        GDS_size_t hi_index[], GDS_op_t accumulate_op, GDS_gds_t gds)
{
    struct gdsi_op_args args;

    /* Check and handle local error first */
    local_error_poll_handle(gds);

    /* GDS_version_inc() completion checking */
    finish_version_inc_test(gds);

#ifdef GDS_CONFIG_COUNTER
    {
        int typelen;
        MPI_Type_size(gds->common->type, &typelen);
        size_t len = typelen;
        int i;
        /* TODO: consider ld correctly */
        for (i = 0; i < gds->common->ndims; i++)
            len *= hi_index[i] - lo_index[i] + 1;
        gdsi_counter_add(&gds->common->counter, GDS_COUNTER_GET_ACC, len);
    }
#endif

    args.buf = origin_addr;
    args.op = accumulate_op;
    args.buf2 = result_addr;

    iterate_array_top_get_acc(gds, lo_index, hi_index, origin_ld,
            result_ld, gds->common->ops->get_acc, &args);
    rma_post_check(gds->common);

    return GDS_STATUS_OK;
}

static struct gdsi_chunk_ops flat_ops = {
    .get = get_lambda_flat,
    .put = put_lambda_flat,
    .acc = acc_lambda_flat,
    .compare_and_swap = compare_and_swap_lambda_flat,
    .get_acc = get_acc_lambda_flat,
};

static struct gdsi_chunk_ops log_ops = {
    .get = get_lambda_log,
    .put = put_lambda_log,
    .acc = acc_lambda_log,
    .compare_and_swap = compare_and_swap_lambda_log,
    .get_acc = get_acc_lambda_log,
};

#if 0
/*
 * Find maximum contiguous local address section, using iterate_array-like
 * approach:
 * lo[]: array of starting indices for array section
 * hi[]: array of ending indices for array section
 * addr: starting address of local buffer
 * len: length of contiguous local buffer
 */

static int max_cont_local_addr(GDS_gds_t gds,
        GDS_size_t lo[], GDS_size_t hi[], void *addr, size_t len)
{
    const int ndims = gds->common->ndims;
    struct gdsi_gds_common *common;
    gdsi_chunk_iterator_t it;
    struct gdsi_chunk chunk;
    size_t focus[ndims], i;
    int clrank, trank;

    common = gds->common;
    MPI_Comm_rank(common->comm, &clrank);
    trank = gdsi_map_owner_into_world(common, clrank);

    /* Check contiguous chunk one by one, until the hi[] falls in range or
     * chunk is not in range */
    focus[0] = lo[0];
    if (gdsi_find_first_chunkit(gds, focus, &it) != GDS_STATUS_OK)
        gdsi_panic("failed to find first chunk iterator\n");
    gdsi_chunkit_access(&it, &chunk);
    if (chunk.target_rank != trank)
        return GDS_STATUS_RANGE;

    return GDS_STATUS_OK;
}
#endif

GDS_status_t GDS_access(GDS_gds_t gds,
        GDS_size_t lo_index[], GDS_size_t hi_index[],
        GDS_access_buffer_t buffer_type, void **access_buffer,
        GDS_access_handle_t *access_handle)
{
    /* GDS_version_inc() completion checking */
    finish_version_inc_test(gds);

    struct gdsi_gds_common *common;
    GDS_access_handle_t h;
    int ndims, clrank, trank, array_id;
    struct gdsi_target_array_desc *array_desc;

    *access_buffer = NULL;
    common = gds->common;
    MPI_Comm_rank(common->comm, &clrank);
    trank = gdsi_map_owner_into_world(common, clrank);
    h = (GDS_access_handle_t) malloc(sizeof(struct GDS_access_handle));
    if (access_handle == NULL)
        return GDS_STATUS_NOMEM;
    h->common = common;
    h->buf = NULL;
    h->buf_size = 0;
    h->buf_type = buffer_type;
    ndims = common->ndims;

    if (common->flavor == GDS_FLAVOR_ALLOC) {
        /* Only check for array created by GDS_alloc */
        size_t lo[ndims], hi[ndims], i;
        if (common->order == GDSI_ORDER_ROW_MAJOR) {
            for (i = 0; i < ndims; i++) {
                lo[i] = common->offset_local[i];
                hi[i] = common->offset_local[i] + common->nelements_local[i] - 1;
                if (lo[i] != lo_index[i] || hi[i] != hi_index[i])
                    return GDS_STATUS_RANGE;
            }
        } else {
            for (i = 0; i < ndims; i++) {
                size_t target_dim = ndims - i - 1;
                lo[i] = common->offset_local[target_dim];
                hi[i] = common->offset_local[target_dim] +
                    common->nelements_local[target_dim] - 1;
                if (lo[i] != lo_index[i] || hi[i] != hi_index[i])
                    return GDS_STATUS_RANGE;
            }
        }
    }

    /* Range matches, continue with buffer setting */
    array_id = gdsi_get_array_id(common, trank);
    array_desc = gdsi_find_array_desc(array_id);
    h->orig_buf = array_desc->buff;
    if (h->buf_type == GDS_ACCESS_BUFFER_DIRECT ||
            h->buf_type == GDS_ACCESS_BUFFER_ANY) {
        h->buf = h->orig_buf;
        h->buf_size = array_desc->singlever_size;
    } else if (h->buf_type == GDS_ACCESS_BUFFER_COPY) {
        h->buf_size = array_desc->singlever_size;
        h->buf = malloc(h->buf_size);
        if (h->buf == NULL)
            return GDS_STATUS_NOMEM;
        memcpy(h->buf, h->orig_buf, h->buf_size);
    }
    *access_handle = h;
    *access_buffer = h->buf;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_access_buffer_type(GDS_access_handle_t access_handle,
        GDS_access_buffer_t *buffer_type)
{
    *buffer_type = access_handle->buf_type;
    return GDS_STATUS_OK;
}

GDS_status_t GDS_release(GDS_access_handle_t access_handle)
{
    if (access_handle->buf_type == GDS_ACCESS_BUFFER_COPY) {
        memcpy(access_handle->orig_buf, access_handle->buf,
                access_handle->buf_size);
        free(access_handle->buf);
    }

    MPI_Win_sync(access_handle->common->win);
    free(access_handle);
    return GDS_STATUS_OK;
}

GDS_status_t GDS_fence(GDS_gds_t gds)
{
    struct gdsi_gds_common *c;
    GDS_gds_t tmp_gds;

    int local_error_flag, global_error_flag;

    gdsi_dprintf(GDS_DEBUG_INFO, "GDS_fence()\n");

    if (gds == NULL) /* for backward compatibility */
        gds = GDS_ROOT;
    else {
        /* Check whether previous round of GDS_version_inc() has finished */

        finish_version_inc(gds);
    }

    if (gds == GDS_ROOT) {
        pthread_rwlock_rdlock(&gdsi_gds_common_list_lock);
        for (c = gdsi_gds_common_list; c != NULL; c = c->next) {
            win_flush(c);
            MPI_Win_sync(c->win);

            /* check and handle local error */
            poll_local_error(c, &local_error_flag);
            if (local_error_flag) {
                gdsi_dprintf(GDS_DEBUG_INFO, "local error\n");

                gdsi_clone_from_gds_common(c, &tmp_gds);
                GDS_invoke_local_error_handler(tmp_gds);
                GDS_free(&tmp_gds);
            }

            /*check and handle global error*/
            poll_global_error(c, &global_error_flag);
            if (global_error_flag) {
                gdsi_dprintf(GDS_DEBUG_INFO, "global error\n");
                gdsi_clone_from_gds_common(c, &tmp_gds);
                GDS_invoke_global_error_handler(tmp_gds);
                GDS_free(&tmp_gds);
            }
        }
        pthread_rwlock_unlock(&gdsi_gds_common_list_lock);

        if (gdsi_comm_world != MPI_COMM_NULL) {
            /* Special workaround for simulated proc failure */
            GDSI_MPI_CALL(MPI_Barrier(gdsi_comm_world));
        }
    } else {
        GDS_wait(gds);

        /* check and handle local error */
        poll_local_error(gds->common, &local_error_flag);
        if (local_error_flag) {
            gdsi_dprintf(GDS_DEBUG_INFO, "local error\n");

            GDS_invoke_local_error_handler(gds);
        }

        /*check and handle global error*/
        poll_global_error(gds->common, &global_error_flag);
        if (global_error_flag) {
            gdsi_dprintf(GDS_DEBUG_INFO, "global error\n");

            GDS_invoke_global_error_handler(gds);
        }
    }

    return GDS_STATUS_OK;
}

GDS_status_t GDS_wait(GDS_gds_t gds)
{
    MPI_Win win = gds->common->win;

    gdsi_dprintf(GDS_DEBUG_INFO, "GDS_wait()\n");

    do {
        win_flush(gds->common);
        MPI_Win_sync(win);
        nbworkq_run(gds->common, true, true);
    } while (gds->common->nbworkq != NULL);

    /* Check whether previous round of GDS_version_inc() has finished */
    finish_version_inc(gds);

    /* Check and handle local error first */
    local_error_poll_handle(gds);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_wait_local(GDS_gds_t gds)
{
    gdsi_dprintf(GDS_DEBUG_INFO, "GDS_wait_local()\n");

    do {
        win_flush_local(gds->common);
        nbworkq_run(gds->common, true, true);
    } while (gds->common->nbworkq != NULL);

    /* GDS_version_inc() completion checking */
    finish_version_inc_test(gds);

    /* Check and handle local error first */
    local_error_poll_handle(gds);

    return GDS_STATUS_OK;
}

static GDS_status_t move_version(GDS_gds_t gds, GDS_size_t new_version);

//label size is the number of bytes included in the label
GDS_status_t GDS_version_inc(GDS_gds_t gds, GDS_size_t increment,
        const char *label, size_t label_size)
{
    /* Check whether the previous round of GDS_version_inc() has finished */
    finish_version_inc(gds);
    assert(gds->common->version_inc_state == 0);
    gdsi_dprintf(GDS_DEBUG_INFO, "GDS_version_inc: enter\n");

    /* Check local error. If raise local error, handle it immediately */
    local_error_check(gds, false);

    /* Poll and handle local error in case of external events */
    local_error_poll_handle(gds);

    GDS_status_t ret;
    GDS_size_t newver = gds->chunk_set->version + increment;

    GDS_wait(gds);

#ifdef GDS_CONFIG_USE_LRDS
    if (gds->common->lrds_dirty_win != MPI_WIN_NULL)
        MPI_Win_flush_all(gds->common->lrds_dirty_win);
#endif

    /* Set the flag for the caller process */
    gds->common->version_inc_state = 1;

    if (gdsi_ignore_versioning) {
        GDSI_MPI_CALL(MPI_Barrier(gds->common->comm));
        gdsi_dprintf(GDS_DEBUG_INFO, "GDS_version_inc: return\n");
        return GDS_STATUS_OK;
    }

    if (gdsi_omit_error_check_on_versioning) {
        GDSI_MPI_CALL(MPI_Barrier(gds->common->comm));
        goto skip_gerror;
    }

    /* On regular path, poll_global_error does a global synchronization
       which works as a barrier, so we don't have to do a barrier here. */

    /* Check global error, handle global error before increasing the version */
    global_error_check(gds, false);

    int global_error_flag;
    poll_global_error(gds->common, &global_error_flag);
    if (global_error_flag) {
        gdsi_dprintf(GDS_DEBUG_INFO, "global error\n");

        GDS_invoke_global_error_handler(gds);
    }

skip_gerror:
    if (increment == 0)
        return GDS_STATUS_OK;
    else if (newver < gds->chunk_set->version)
        return GDS_STATUS_RANGE; /* overflow */

    gdsi_common_lock(gds->common);
    ret = move_version(gds, newver);
    gdsi_common_unlock(gds->common);

    /* Setup label */
    gds->chunk_set->label_size = label_size;
    gds->chunk_set->label = malloc(sizeof(char) * (label_size + 1));
    if (!gds->chunk_set->label)
        return GDS_STATUS_NOMEM;

    strncpy(gds->chunk_set->label, label, label_size);
    gds->chunk_set->label[label_size] = '\0';

#ifdef GDS_CONFIG_COUNTER
    gdsi_counter_ver_inc(&gds->common->counter);
#endif
    gdsi_dprintf(GDS_DEBUG_INFO, "GDS_version_inc: return\n");

    return ret;
}

GDS_status_t GDS_version_dec(GDS_gds_t gds, GDS_size_t dec) {

    GDS_status_t ret;
    GDS_size_t newver = gds->chunk_set->version - dec;

    /* TODO: only allowed in error-recovery mode? */

    if (dec == 0)
        return GDS_STATUS_OK;
    else if (newver > gds->chunk_set->version)
        return GDS_STATUS_RANGE; /* underflow */

    gdsi_common_lock(gds->common);
    ret = move_version(gds, newver);
    gdsi_common_unlock(gds->common);

    return ret;
}

static void iterate_array(GDS_gds_t gds, size_t dim, size_t *lbuf_offset, size_t focus[], const size_t lo_index[], const size_t hi_index[], const size_t ld[], chunk_lambda_t lambda, struct gdsi_op_args *lambda_args)
{
    size_t i;
    size_t hi;
    const int ndims = gds->common->ndims;
    int k;

    if (dim > 0)
        hi = GDSI_MIN(hi_index[dim], lo_index[dim] + ld[dim-1] - 1);
    else
        hi = hi_index[dim];

    if (dim == ndims - 1) {
        gdsi_chunk_iterator_t it;
        GDS_status_t s;

        focus[dim] = i = lo_index[dim];
        s = gdsi_find_first_chunkit(gds, focus, &it);
        assert(s == GDS_STATUS_OK);

        for (;;) {
            struct gdsi_chunk chunk;

            gdsi_chunkit_access(&it, &chunk);

            gdsi_dprintf(GDS_DEBUG_INFO, "chunk."
                    "{target_rma_index=%zu, target_offset=%zu, size=%zu}\n",
                    chunk.target_rma_index, chunk.target_offset, chunk.size);

            size_t contig_len = GDSI_MIN(chunk.size, hi+1-i);

            /* Check if the chunk is corrupted for getting old versions. Other
             * operations upon old versions, such as put, acc, compare_and_swap, get_acc are
             * already forbiden inside the calling function. */
            if (gds->common->latest_version != gds->chunk_set->version &&
                    chunk.is_corrupted == true) {
                /* If old chunk is corrupted, raise error */
                gdsi_dprintf(GDS_DEBUG_INFO, "iterate_array: chunk is corrupted.\n");
                GDS_error_t desc;
                GDS_create_error_descriptor(&desc);
                GDS_size_t error_offset[ndims];
                GDS_size_t error_count[ndims];

                for (k = 0; k < ndims-1; k++) {
                    error_offset[k] = focus[k];
                }
                error_offset[k] = i;

                for (k = 0; k < ndims-1; k++) {
                    error_count[k] = 1;
                }

                if (gds->common->order == GDSI_ORDER_COL_MAJOR) {
                    gdsi_reverse_array(error_offset, ndims);
                    gdsi_reverse_array(error_count, ndims);
                }
                error_count[i] = contig_len;

                GDS_size_t error_n_ranges = 1;
                GDS_add_error_attr(desc, GDS_EATTR_GDS_INDEX,
                        sizeof(GDS_size_t), &error_offset);
                GDS_add_error_attr(desc, GDS_EATTR_GDS_COUNT,
                        sizeof(GDS_size_t), &error_count);
                GDS_add_error_attr(desc, GDS_EATTR_MEMORY_N_RANGES,
                        sizeof(GDS_size_t), &error_n_ranges);

                GDS_raise_local_error(gds, desc);
                break;
            }

            lambda(gds, *lbuf_offset,
                    chunk.target_rank, chunk.target_rma_index,
                    chunk.target_offset, contig_len, chunk.chunk_number, lambda_args);
            i += contig_len;
            *lbuf_offset += contig_len;

            if (i > hi) {
                break;
            }

            if (gdsi_chunkit_end(&it))
                gdsi_panic("chunkit ran out of elements with %zu elements remaining\n", hi-i);
            else
                gdsi_chunkit_forward(&it);
        }

        if (0 < dim
                && lo_index[dim] + ld[dim-1] > hi_index[dim] + 1) {
            size_t skip = lo_index[dim] + ld[dim-1] - hi_index[dim] - 1;
            *lbuf_offset += skip;
        }
    } else {
        for (i = lo_index[dim]; i <= hi; i++) {
            focus[dim] = i;
            iterate_array(gds, dim+1, lbuf_offset, focus, lo_index, hi_index, ld, lambda, lambda_args);
        }
        if (0 < dim
                && lo_index[dim] + ld[dim-1] > hi_index[dim] + 1) {
            size_t skip = lo_index[dim] + ld[dim-1] - hi_index[dim] - 1;
            for (i = dim+1; i < ndims; i++)
                skip *= gds->common->nelements[i];
            *lbuf_offset += skip;
        }
    }
}

static void iterate_array_top(GDS_gds_t gds, const size_t lo_index[], const size_t hi_index[], const size_t ld[], chunk_lambda_t lambda, struct gdsi_op_args *lambda_args)
{
    size_t ndims = gds->common->ndims;
    size_t focus[ndims];
    size_t lbuf_offset = 0;

    if (gds->common->order != GDSI_ORDER_COL_MAJOR)
        iterate_array(gds, 0, &lbuf_offset, focus,
                lo_index, hi_index, ld, lambda, lambda_args);
    else {
        size_t ndims = gds->common->ndims;
        size_t lot[ndims], hit[ndims], ldt[ndims-1];
        size_t i;
        for (i = 0; i < ndims-1; i++) {
            lot[i] = lo_index[ndims-1-i];
            hit[i] = hi_index[ndims-1-i];
            ldt[i] = ld[ndims-2-i];
        }
        lot[i] = lo_index[ndims-1-i];
        hit[i] = hi_index[ndims-1-i];

        iterate_array(gds, 0, &lbuf_offset, focus,
                lot, hit, ldt, lambda, lambda_args);
    }
}

//TODO for both of these, we assume there is only one thread
GDS_status_t GDS_comm_rank(GDS_comm_t comm, int *rank) {
    MPI_Comm_rank(comm, rank);
    return GDS_STATUS_OK;
}

GDS_status_t GDS_comm_size(GDS_comm_t comm, int *size) {
    MPI_Comm_size(comm, size);
    return GDS_STATUS_OK;
}

GDS_status_t GDS_create_error_pred(GDS_error_pred_t *predp)
{
    GDS_error_pred_t pred;

    pred = calloc(sizeof(*pred), 1);
    if (pred == NULL)
        return GDS_STATUS_NOMEM;

    *predp = pred;
    return GDS_STATUS_OK;
}

GDS_status_t GDS_create_error_pred_term(
        GDS_error_attr_key_t attr_key,
        GDS_error_match_expr_t match_expr,
        size_t value_len,
        const void *values,
        GDS_error_pred_term_t *termp)
{
    struct GDS_error_pred_term *term;

    /* Sanity checks */
    switch (match_expr) {
        case GDS_EMEXP_VALUE:
        case GDS_EMEXP_RANGE:
            if (!gdsi_validate_error_attr(attr_key, value_len))
                return GDS_STATUS_INVALID;
            break;

        default:
            break;
    }

    term = malloc(sizeof(*term) + value_len);
    if (term == NULL)
        return GDS_STATUS_NOMEM;

    term->attr_key   = attr_key;
    term->match_expr = match_expr;
    term->value_len  = value_len;
    memcpy(term->values, values, value_len);

    *termp = term;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_free_error_pred_term(GDS_error_pred_term_t *term)
{
    free(*term);
    *term = NULL;
    return GDS_STATUS_OK;
}

GDS_status_t GDS_add_error_pred_term(GDS_error_pred_t pred,
        const GDS_error_pred_term_t term_orig)
{
    struct GDS_error_pred_term *term;
    size_t total_len = sizeof(*term) + term_orig->value_len;

    term = malloc(total_len);
    if (term == NULL)
        return GDS_STATUS_NOMEM;

    memcpy(term, term_orig, total_len);

    pred->n_terms++;
    pred->total_value_size += term->value_len;

    SGLIB_LIST_ADD(struct GDS_error_pred_term,
            pred->terms, term, next);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_free_error_pred(GDS_error_pred_t *predp)
{
    GDS_error_pred_t pred = *predp;
    while (pred->terms != NULL) {
        struct GDS_error_pred_term *p_next = pred->terms->next;
        free(pred->terms);
        pred->terms = p_next;
    }

    free(pred);
    *predp = NULL;

    return GDS_STATUS_OK;
}

static struct gdsi_error_handler *setup_error_handler(
        struct gdsi_gds_common *common,
        const GDS_error_pred_t pred,
        GDS_recovery_func_t recovery_func)
{
    size_t offset = 0;
    struct GDS_error_pred_term *p, *q;
    size_t eh_real_size = sizeof(struct gdsi_error_handler)
        + pred->total_value_size
        + sizeof(struct GDS_error_pred_term) * pred->n_terms;
    struct gdsi_error_handler *eh =  malloc(eh_real_size);
    if (eh == NULL)
        return NULL;

    eh->recovery_func = recovery_func;
    eh->next = NULL;
    eh->n_terms = pred->n_terms;
    if (eh->n_terms == 0) {
        eh->terms = NULL;
        return eh;
    }
    eh->terms = (struct GDS_error_pred_term *) eh->raw_buff;

    p = pred->terms;
    q = eh->terms;
    while (p != NULL) {
        size_t s = sizeof(*p) + p->value_len;
        memcpy(q, p, s);
        offset += s;
        q->next = p->next == NULL ?
            NULL
            : (struct GDS_error_pred_term *) (eh->raw_buff + offset);
        p = p->next;
        q = q->next;
    }

    return eh;
}

GDS_status_t GDS_register_local_error_handler(
        GDS_gds_t gds,
        const GDS_error_pred_t pred,
        GDS_recovery_func_t recovery_func)
{
    struct gdsi_error_handler *eh = setup_error_handler(gds->common, pred,
            recovery_func);
    if (eh == NULL)
        return GDS_STATUS_NOMEM;

    gdsi_common_lock(gds->common);
    SGLIB_LIST_ADD(struct gdsi_error_handler,
            gds->common->local_error_handler_queue, eh, next);
    gdsi_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

static void enqueue_local_error(struct gdsi_gds_common *common,
        GDS_error_t error_desc)
{
    int myrank;
    GDS_comm_rank(common->comm, &myrank);
    error_desc->rank = myrank;

    gdsi_common_lock(common);

    /* high process id and counter is on the head of the linked list */
    SGLIB_LIST_ADD(struct GDS_error,
            common->local_error_desc_queue, error_desc, next);

    error_desc->counter = ++common->local_error_desc_counter;

    gdsi_common_unlock(common);
}

GDS_status_t GDS_raise_local_error(GDS_gds_t gds, GDS_error_t error_desc)
{
    GDS_status_t ret = GDS_STATUS_OK;

    if (error_desc == NULL)
        return GDS_STATUS_INVALID;

    enqueue_local_error(gds->common, error_desc);

    /* After raising local error, it invokes the error handler immediately */
    ret = GDS_invoke_local_error_handler(gds);

    return ret;
}

GDS_status_t GDS_resume_local(GDS_gds_t gds, GDS_error_t error_desc)
{
    gdsi_free_error_desc(error_desc);

    gdsi_common_lock(gds->common);
    gds->common->recovery_mode = 0;
    gdsi_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_register_global_error_handler(
        GDS_gds_t gds,
        GDS_error_pred_t pred,
        GDS_recovery_func_t recovery_func)
{
    struct gdsi_error_handler *eh = setup_error_handler(gds->common, pred,
            recovery_func);
    if (eh == NULL)
        return GDS_STATUS_NOMEM;

    gdsi_common_lock(gds->common);
    SGLIB_LIST_ADD(struct gdsi_error_handler_queue,
            gds->common->global_error_handler_queue, eh, next);
    gdsi_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

static void enqueue_global_error(struct gdsi_gds_common *common,
        GDS_error_t error_desc)
{
    int myrank;
    GDS_comm_rank(common->comm, &myrank);
    error_desc->rank = myrank;

    gdsi_common_lock(common);

    /*high process id and counter is on the head of the linkedlist*/
    SGLIB_LIST_ADD(struct GDS_error,
            common->global_error_desc_queue, error_desc, next);

    error_desc->counter = ++common->global_error_desc_counter;

    gdsi_common_unlock(common);
}

/*
   Raises a global error.
   This function does not trigger the recovery function.
   Instead, trigger it via GDS_invoke_global_error_handler at a stable point.
   Maybe more than one error in the queue.
 */
GDS_status_t GDS_raise_global_error(GDS_gds_t gds, GDS_error_t error_desc)
{
    if (error_desc == NULL)
        return GDS_STATUS_INVALID;

    enqueue_global_error(gds->common, error_desc);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_resume_global(GDS_gds_t gds, GDS_error_t error_desc)
{
    gdsi_free_error_desc(error_desc);

    gdsi_common_lock(gds->common);
    gds->common->recovery_mode = 0;
    gdsi_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_create_error_descriptor(GDS_error_t *edp)
{
    GDS_error_t desc;

    desc = calloc(sizeof(*desc), 1);
    if (desc == NULL)
        return GDS_STATUS_NOMEM;

    desc->rank = -1;

    *edp = desc;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_add_error_attr(
        GDS_error_t error_desc,
        GDS_error_attr_key_t attr_key,
        size_t attr_len,
        const void *attr_val)
{
    struct gdsi_error_attr *attr;

    if (!gdsi_validate_error_attr(attr_key, attr_len))
        return GDS_STATUS_INVALID;

    attr = malloc(sizeof(*attr) + attr_len);
    if (attr == NULL)
        return GDS_STATUS_NOMEM;

    attr->next = NULL;
    attr->attr_key = attr_key;
    attr->value_len = attr_len;
    memcpy(attr->value, attr_val, attr_len);

    SGLIB_LIST_ADD(struct GDS_error_attr,
            error_desc->attrs, attr, next);
    error_desc->n_attrs++;
    error_desc->total_value_size += attr->value_len;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_error_attr(
        GDS_error_t error_desc,
        GDS_error_attr_key_t attr_key,
        void *attr_val,
        int *flag)
{
    struct gdsi_error_attr *attr;

    *flag = 0;

    attr = gdsi_find_error_attr(error_desc, attr_key);
    if (attr == NULL)
        return GDS_STATUS_OK;

    memcpy(attr_val, attr->value, attr->value_len);
    *flag = 1;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_error_attr_len(
        GDS_error_t error_desc,
        GDS_error_attr_key_t attr_key,
        GDS_size_t *attr_len,
        int *flag)
{
    struct gdsi_error_attr *attr;

    *flag = 0;

    attr = gdsi_find_error_attr(error_desc, attr_key);
    if (attr == NULL)
        return GDS_STATUS_OK;

    *attr_len = attr->value_len;
    *flag = 1;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_free_error_descriptor(GDS_error_t *descp)
{
    gdsi_free_error_desc(*descp);
    *descp = NULL;
    return GDS_STATUS_OK;
}

GDS_status_t GDS_invoke_local_error_handler(GDS_gds_t gds)
{
    gdsi_common_lock(gds->common);

    /*if the recovery mode is on, just return*/
    if (gds->common->recovery_mode) {
        gdsi_common_unlock(gds->common);
        return GDS_STATUS_OK;
    } else {
        gds->common->recovery_mode = 1;
        gdsi_common_unlock(gds->common);
    }

    GDS_recovery_func_t recovery_func = NULL;
    GDS_error_t error_desc;
    GDS_status_t ret = GDS_STATUS_OK;

    int myrank;
    GDS_comm_rank(gds->common->comm, &myrank);

    while (1) {
        const struct gdsi_error_handler *eh_match;

        gdsi_common_lock(gds->common);

        error_desc = gds->common->local_error_desc_queue;
        if (error_desc == NULL)
            goto out_unlock;

        eh_match = gdsi_match_error_handler(gds->common,
                gds->common->local_error_handler_queue,
                error_desc);
        if (eh_match)
            recovery_func = eh_match->recovery_func;
        else
            gdsi_dprintf(GDS_DEBUG_ERR, "No match: n_attrs=%d\n",
                    error_desc->n_attrs);

        if (recovery_func != NULL && error_desc != NULL) {
            SGLIB_LIST_DELETE(struct GDS_error,
                    gds->common->local_error_desc_queue, error_desc, next);
        } else {
            ret = GDS_STATUS_INVALID;
            goto out_unlock;
        }

        gdsi_common_unlock(gds->common);

        if (recovery_func != NULL && error_desc != NULL) {
            ret = recovery_func(gds, error_desc);

            /* TODO: how we deal with the failed of recovery_func? */
            if (ret != GDS_STATUS_OK) {
                if (error_desc != NULL) {
                    gdsi_common_lock(gds->common);
                    SGLIB_LIST_ADD(struct GDS_error,
                            gds->common->local_error_desc_queue, error_desc, next);
                    gdsi_common_unlock(gds->common);
                }
                break;
            }
        }
    }

    return ret;

out_unlock:
    gdsi_common_unlock(gds->common);
    return ret;
}

/*collective operation*/
GDS_status_t GDS_invoke_global_error_handler(GDS_gds_t gds)
{
    GDS_status_t ret = GDS_STATUS_OK;

    gdsi_common_lock(gds->common);

    /* if the recovery mode is on,
       just return maybe unnessary for stable point */
    if (gds->common->recovery_mode) {
        gdsi_common_unlock(gds->common);
        return GDS_STATUS_OK;
    } else {
        gds->common->recovery_mode = 1;
        gdsi_common_unlock(gds->common);
    }

    GDS_recovery_func_t recovery_func = NULL;
    GDS_error_t error_desc;

    int myrank;
    GDS_comm_rank(gds->common->comm, &myrank);

    while (1) {
        const struct gdsi_error_handler *eh_match;

        gdsi_common_lock(gds->common);

        error_desc = gds->common->global_error_desc_queue;
        if (error_desc == NULL)
            goto out_unlock;

        eh_match = gdsi_match_error_handler(gds->common,
                gds->common->global_error_handler_queue,
                error_desc);
        if (eh_match)
            recovery_func = eh_match->recovery_func;

        if (recovery_func != NULL && error_desc != NULL) {
            SGLIB_LIST_DELETE(struct GDS_error,
                    gds->common->global_error_desc_queue, error_desc, next);
        } else {
            ret = GDS_STATUS_INVALID;
            goto out_unlock;
        }

        gdsi_common_unlock(gds->common);

        if (recovery_func != NULL && error_desc != NULL) {
            ret = recovery_func(gds, error_desc);
            /* error_desc and flag will be freed by calling GDS_resume */

            /* TODO: how we deal with the failure of recovery_func? */
            if (ret != GDS_STATUS_OK) {
                if (error_desc != NULL) {
                    gdsi_common_lock(gds->common);
                    SGLIB_LIST_ADD(struct GDS_error,
                            gds->common->global_error_desc_queue, error_desc, next);
                    gdsi_common_unlock(gds->common);
                }
                break;
            }
        }
    }
    return ret;

out_unlock:
    gdsi_common_unlock(gds->common);
    return ret;
}

GDS_status_t GDS_register_global_error_check(GDS_gds_t gds,
        GDS_check_func_t check_func, GDS_priority_t check_priority)
{
    struct gdsi_check_fun *chk = malloc(sizeof(struct gdsi_check_fun));
    if (chk == NULL)
        return GDS_STATUS_NOMEM;

    chk->check_func = check_func;
    chk->check_priority = check_priority;
    chk->next = NULL;

    gdsi_common_lock(gds->common);
    SGLIB_LIST_ADD(struct gdsi_check_fun,
            gds->common->global_check_funs[check_priority], chk, next);
    gdsi_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_register_local_error_check(GDS_gds_t gds,
        GDS_check_func_t check_func, GDS_priority_t check_priority)
{
    struct gdsi_check_fun *chk = malloc(sizeof(struct gdsi_check_fun));
    if (chk == NULL)
        return GDS_STATUS_NOMEM;

    chk->check_func = check_func;
    chk->check_priority = check_priority;
    chk->next = NULL;

    gdsi_common_lock(gds->common);
    SGLIB_LIST_ADD(struct gdsi_check_fun,
            gds->common->local_check_funs[check_priority], chk, next);
    gdsi_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_check_all_errors(GDS_gds_t gds)
{
    /* Forcely invoke all the registered error checks */
    local_error_check(gds, true);
    global_error_check(gds, true);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_version_number(GDS_gds_t gds, GDS_size_t *version_number)
{
    /* Check and handle local error first */
    local_error_poll_handle(gds);

    *version_number = gds->chunk_set->version;
    return GDS_STATUS_OK;
}

GDS_status_t GDS_get_version_label(GDS_gds_t gds, char **label, size_t *label_size)
{
    /* Check and handle local error first */
    local_error_poll_handle(gds);

    *label = gds->chunk_set->label;
    *label_size = gds->chunk_set->label_size;
    return GDS_STATUS_OK;
}

/*
   Must be called with gds->common->lock held.
 */
static GDS_status_t move_version(GDS_gds_t gds, GDS_size_t new_version)
{
    GDS_status_t ret = GDS_STATUS_INVALID;;
    struct gdsi_chunk_set *cset;
    if (new_version > gds->common->latest_version) {
        ret = gdsi_create_new_version(gds->common, new_version);
        if (ret == GDS_STATUS_OK) {
            gds->chunk_set = gds->common->chunk_sets;
            if (gds->common->representation == GDSI_REPR_LOG) {
                int i, tsize;
                /* TODO: is this a right way to obtain the size of target nodes? */
                MPI_Comm_size(MPI_COMM_WORLD, &tsize);
                for (i = 0; i < tsize; i++)
                    log_put_clear_stock(gds->common, i);
            }
        }
    } else if (new_version > gds->chunk_set->version) {
        /* Go to newer version */
        for (cset = gds->chunk_set; cset != NULL; cset = cset->newer) {
            if (cset->version == new_version) {
                /* Found */
                gds->chunk_set = cset;
                ret = GDS_STATUS_OK;
                break;
            } else if (cset->version > new_version) {
                /* There was no such version */
                break;
            }
        }
    } else {
        /* Go to older version */
        for (cset = gds->chunk_set; cset != NULL; cset = cset->older) {
            if (cset->version == new_version) {
                /* Found */
                gds->chunk_set = cset;
                ret = GDS_STATUS_OK;
                break;
            } else if (cset->version < new_version) {
                /* There was no such version */
                break;
            }
        }
    }

    return ret;
}

GDS_status_t GDS_move_to_newest(GDS_gds_t gds)
{
    gdsi_common_lock(gds->common);
    move_version(gds, gds->common->latest_version);
    gdsi_common_unlock(gds->common);

    return GDS_STATUS_OK;
}

GDS_status_t GDS_move_to_prev(GDS_gds_t gds)
{
    GDS_status_t ret;

    gdsi_common_lock(gds->common);
    if (gds->chunk_set->older == NULL)
        ret = GDS_STATUS_INVALID;
    else
        ret = move_version(gds, gds->chunk_set->older->version);
    gdsi_common_unlock(gds->common);

    return ret;
}

GDS_status_t GDS_move_to_next(GDS_gds_t gds)
{
    GDS_status_t ret;

    gdsi_common_lock(gds->common);
    if (gds->chunk_set->newer == NULL)
        ret = GDS_STATUS_INVALID;
    else
        ret = move_version(gds, gds->chunk_set->newer->version);
    gdsi_common_unlock(gds->common);

    return ret;
}

GDS_status_t GDS_descriptor_clone(GDS_gds_t old, GDS_gds_t *newp)
{
    /* Check and handle local error first */
    local_error_poll_handle(old);

    GDS_gds_t new;

    new = malloc(sizeof(struct GDS_gds));
    if (new == NULL)
        return GDS_STATUS_NOMEM;

    gdsi_common_lock(old->common);
    old->common->refcount++;
    gdsi_common_unlock(old->common);

    new->common = old->common;
    new->chunk_set = old->chunk_set;

    *newp = new;

    return GDS_STATUS_OK;
}

GDS_status_t GDS_counter_reset(GDS_gds_t gds)
{
#ifdef GDS_CONFIG_COUNTER
    gdsi_counter_init(&gds->common->counter, NULL);
#endif

    return GDS_STATUS_OK;
}

GDS_status_t GDS_simulate_proc_failure(int rank)
{
    int my_rank;
    struct gdsi_gds_common *c;

    GDS_comm_rank(gdsi_comm_world, &my_rank);

    gdsi_crash_count++;

    pthread_rwlock_rdlock(&gdsi_gds_common_list_lock);
    for (c = gdsi_gds_common_list; c != NULL; c = c->next) {
        int r;

        MPI_Group_translate_ranks(gdsi_group_world, 1, &rank,
                c->owner_group, &r);
        if (r == MPI_UNDEFINED)
            continue;

        c->simulated_failed_proc = r;
    }
    pthread_rwlock_unlock(&gdsi_gds_common_list_lock);

    skip_cleanup = true;

    pthread_mutex_lock(&gdsi_simfail_mtx);
    gdsi_send_target_request(&rank, 1, MPI_INT, my_rank, GDS_TGTREQ_SIM_FAILURE);
    pthread_cond_wait(&gdsi_simfail_pause, &gdsi_simfail_mtx);
    pthread_mutex_unlock(&gdsi_simfail_mtx);

    GDS_fence(NULL);

    if (rank != my_rank)
        MPI_Comm_group(gdsi_comm_world, &gdsi_group_world);

    if (rank == my_rank) {
        gdsi_dprintf(GDS_DEBUG_INFO, "before pause()\n");
        for (;;)
            pause();
    }

    return GDS_STATUS_OK;
}
