/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#ifndef GDS_COUNTER_H_INCLUDED
#define GDS_COUNTER_H_INCLUDED

#include "counter_def.h"

#define GDS_COUNTER_IDSTR_MAX 128

struct gdsi_counter_entry {
    int count; /* count since last version inc */
    int sum;
    int count_total;
    int sum_total;
};

struct gdsi_counter {
    struct gdsi_counter_entry ents[GDS_COUNTER_MAX];
    char idstr[GDS_COUNTER_IDSTR_MAX];
};

void gdsi_counter_init(struct gdsi_counter *ct, const char *id);
void gdsi_counter_add(struct gdsi_counter *ct, int key, int val);
void gdsi_counter_ver_inc(struct gdsi_counter *ct);
void gdsi_counter_destroy(struct gdsi_counter *ct);

#endif
