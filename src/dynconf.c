/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "dynconf.h"
#include "gds_internal.h"

/* Definitions of dynamically configurable variables */
int gdsi_debug_level = 0;
int gdsi_order_default = GDSI_ORDER_ROW_MAJOR;
int gdsi_show_info = 0;
int gdsi_repr_default  = GDSI_REPR_FLAT;
int gdsi_server_threads = 1;
int gdsi_log_bs_default = GDSI_LOG_USER_BLOCK_ELEMENTS_DEFAULT;
#ifdef GDS_BUILD_BGQ
int gdsi_wait_scheme = GDS_WAIT_SCHEME_SPIN;
#else
int gdsi_wait_scheme = GDS_WAIT_SCHEME_DOORBELL;
#endif
int gdsi_log_md_prefetch_max = GDSI_LOG_MD_PREFETCH_MAX_DEFAULT;
int gdsi_log_flush_on_padding = 0;
int gdsi_log_prealloc_default = GDSI_LOG_PREALLOC_DEFAULT;
int gdsi_omit_error_check_on_versioning = 0;
int gdsi_enable_slow_memcpy = 0;
int gdsi_ignore_versioning = 0;
int gdsi_outstanding_rma_ops_max = GDSI_OUTSTANDING_RMA_OPS_MAX_DEFAULT;
int gdsi_exit_on_free = 0;
int gdsi_log_speculative_alloc = 0;
int gdsi_wait_sleep_duration = 1;
int gdsi_wait_spin_count = 100000;
int gdsi_dontcare_thread_multiple = 0;
int gdsi_sanity_check = 0;
int gdsi_disable_pca = 0;
int gdsi_log_fetch_and_op = 0;

static void int_parser(const char *val_str, void *value)
{
    int v = atoi(val_str);
    int *pv = (int *) value;

    *pv = v;
}

static void int_printer(const char *key, const void *value)
{
    int v = *((int *) value);

    printf("    %s: %d\n", key, v);
}

static void bool_printer(const char *key, const void *value)
{
    int v = *((int *) value);

    printf("    %s: %s\n", key, v ? "true" : "false");
}

static void order_parser(const char *val_str, void *value)
{
    int *pv = (int *) value;

    if (strlen(val_str) == 0)
        return;

    switch (toupper(val_str[0])) {
    case 'C':
        gdsi_dprintf(GDS_DEBUG_INFO, "Default ordering set as column-major\n");
        *pv = GDSI_ORDER_COL_MAJOR;
        break;
    case 'R':
        gdsi_dprintf(GDS_DEBUG_INFO, "Default ordering set as row-major\n");
        *pv = GDSI_ORDER_ROW_MAJOR;
        break;
    }
}

static void order_printer(const char *key, const void *value)
{
    int v = *((int *) value);

    if (v == GDSI_ORDER_COL_MAJOR)
        printf("    Default ordering: column-major\n");
    else
        printf("    Default ordering: row-major\n");
}

void gdsi_set_order_col_major(void)
{
    gdsi_order_default = GDSI_ORDER_COL_MAJOR;
}

void gdsi_set_order_row_major(void)
{
    gdsi_order_default = GDSI_ORDER_ROW_MAJOR;
}

static void repr_parser(const char *val_str, void *value)
{
    int *pv = (int *) value;

    if (strlen(val_str) == 0)
        return;

    switch (toupper(val_str[0])) {
    case 'F':
        gdsi_dprintf(GDS_DEBUG_INFO, "Default representation set as flat\n");
        *pv = GDSI_REPR_FLAT;
        break;
    case 'L':
        gdsi_dprintf(GDS_DEBUG_INFO, "Default representation set as log-structured\n");
        *pv = GDSI_REPR_LOG;
        break;
    }
}

static void repr_printer(const char *key, const void *value)
{
    int *pv = (int *) value;

    if (*pv == GDSI_REPR_LOG)
        printf("    Default array representation: Log-structured\n");
    else
        printf("    Default array representation: Flat\n");
}

static const char *wait_scheme_string(void)
{
    switch (gdsi_wait_scheme) {
    case GDS_WAIT_SCHEME_DEFAULT:
        return "MPI default";

    case GDS_WAIT_SCHEME_SLEEP:
        return "sleep";

    case GDS_WAIT_SCHEME_SPIN:
        return "spin wait";

    case GDS_WAIT_SCHEME_DOORBELL:
        return "doorbell";

    default:
        return "unknown";
    }
}

static void wait_scheme_printer(const char *key, const void *value)
{
    printf("    GDS_WAIT_SCHEME (Server thread wait scheme): %d (%s)\n",
           gdsi_wait_scheme, wait_scheme_string());
}

/* The main registry of dynamic configurable variables */
static struct gdsi_dynconf_entry dynconf_entries[] = {
    { "GDS_DEBUG_LEVEL", &gdsi_debug_level, int_parser, int_printer },
    { "GDS_ORDER_DEFAULT", &gdsi_order_default, order_parser, order_printer },
    { "GDS_SHOW_INFO", &gdsi_show_info, int_parser, bool_printer },
    { "GDS_REPR_DEFAULT", &gdsi_repr_default, repr_parser, repr_printer },
    { "GDS_SERVER_THREADS", &gdsi_server_threads, int_parser, int_printer },
    { "GDS_LOG_BS_DEFAULT", &gdsi_log_bs_default, int_parser, int_printer },
    { "GDS_WAIT_SCHEME", &gdsi_wait_scheme, int_parser, wait_scheme_printer },
    { "GDS_LOG_MD_PREFETCH_MAX", &gdsi_log_md_prefetch_max,
      int_parser, int_printer },
    { "GDS_LOG_FLUSH_ON_PADDING", &gdsi_log_flush_on_padding,
      int_parser, bool_printer },
    { "GDS_LOG_PREALLOC_DEFAULT", &gdsi_log_prealloc_default,
      int_parser, int_printer },
    { "GDS_OMIT_ERROR_CHECK_ON_VERSIONING",
      &gdsi_omit_error_check_on_versioning, int_parser, bool_printer },
    { "GDS_ENABLE_SLOW_MEMCPY", &gdsi_enable_slow_memcpy,
      int_parser, int_printer },
    { "GDS_IGNORE_VERSIONING", &gdsi_ignore_versioning,
      int_parser, bool_printer },
    { "GDS_OUTSTANDING_RMA_OPS_MAX", &gdsi_outstanding_rma_ops_max,
      int_parser, int_printer },
    { "GDS_EXIT_ON_FREE", &gdsi_exit_on_free,
      int_parser, bool_printer },
    { "GDS_LOG_SPECULATIVE_ALLOC", &gdsi_log_speculative_alloc,
      int_parser, bool_printer },
    { "GDS_WAIT_SLEEP_DURATION", &gdsi_wait_sleep_duration,
      int_parser, int_printer },
    { "GDS_WAIT_SPIN_COUNT", &gdsi_wait_spin_count,
      int_parser, int_printer },
    { "GDS_DONTCARE_THREAD_MULTIPLE", &gdsi_dontcare_thread_multiple,
      int_parser, bool_printer },
    { "GDS_SANITY_CHECK", &gdsi_sanity_check,
      int_parser, bool_printer },
    { "GDS_DISABLE_PCA", &gdsi_disable_pca,
      int_parser, bool_printer },
    { "GDS_LOG_FETCH_AND_OP", &gdsi_log_fetch_and_op,
      int_parser, bool_printer },
};

void gdsi_dynconf_print(void)
{
    int nents = sizeof(dynconf_entries) / sizeof(struct gdsi_dynconf_entry);
    int i;

    for (i = 0; i < nents; i++) {
        struct gdsi_dynconf_entry *ent = dynconf_entries+i;
        ent->print(ent->env_key, ent->variable);
    }
}

void gdsi_dynconf_init(void)
{
    int i;

    for (i = 0; i < sizeof(dynconf_entries)/sizeof(dynconf_entries[0]); i++) {
        struct gdsi_dynconf_entry *ent = dynconf_entries+i;
        const char *valstr = getenv(ent->env_key);
        if (valstr == NULL)
            continue;
        ent->parse(valstr, ent->variable);
    }
}
