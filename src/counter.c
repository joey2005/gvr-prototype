/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <string.h>

#define GDS_COUNTER_IMPL
#include "gds_internal.h"
#include "counter.h"

void gdsi_counter_init(struct gdsi_counter *ct, const char *id)
{
    int i;
    for (i = 0; i < GDS_COUNTER_MAX; i++)
        memset(&ct->ents[i], 0, sizeof(ct->ents[i]));

    if (id)
        strncpy(ct->idstr, id, GDS_COUNTER_IDSTR_MAX - 1);
}

void gdsi_counter_add(struct gdsi_counter *ct, int key, int val)
{
    gdsi_assert(key >= 0);
    gdsi_assert(key < GDS_COUNTER_MAX);

    ct->ents[key].count++;
    ct->ents[key].sum += val;
}

void gdsi_counter_ver_inc(struct gdsi_counter *ct)
{
    int i;
    int ver = ct->ents[GDS_COUNTER_VER_INC].count_total;

    for (i = 0; i < GDS_COUNTER_MAX; i++) {
        gdsi_dprintf(GDS_DEBUG_ERR, "%s,%d,%s,%d,%d\n",
            ct->idstr, ver, gdsi_counter_key_strs[i], ct->ents[i].sum, ct->ents[i].count);
        ct->ents[i].count_total += ct->ents[i].count;
        ct->ents[i].count = 0;
        ct->ents[i].sum_total += ct->ents[i].sum;
        ct->ents[i].sum = 0;
    }
}

void gdsi_counter_destroy(struct gdsi_counter *ct)
{
    int i;

    gdsi_counter_ver_inc(ct);

    for (i = 0; i < GDS_COUNTER_MAX; i++)
        gdsi_dprintf(GDS_DEBUG_ERR, "%s,total,%s,%d,%d\n",
            ct->idstr, gdsi_counter_key_strs[i], ct->ents[i].sum_total,
            ct->ents[i].count_total);
}
