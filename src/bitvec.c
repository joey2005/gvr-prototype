/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <strings.h>

#include "gds_internal.h"

struct gdsi_bitvec {
    size_t max;
    uint8_t buf[];
};

size_t gdsi_bitvecl_size(size_t max)
{
    return (max + 7) / 8;
}

void gdsi_bitvecl_clear_all(uint8_t *buf, size_t max)
{
    size_t i, nb = gdsi_bitvecl_size(max);

    for (i = 0; i < nb; i++)
        buf[i] = 0;
}

void gdsi_bitvecl_set_all(uint8_t *buf, size_t max)
{
    size_t i, nb = gdsi_bitvecl_size(max);

    for (i = 0; i < nb; i++)
        buf[i] = ~0;
}

void gdsi_bitvecl_set(uint8_t *buf, size_t n)
{
    size_t n_byte = n / 8;
    size_t n_bit  = n % 8;

    buf[n_byte] |= (1 << n_bit);
}

void gdsi_bitvecl_clear(uint8_t *buf, size_t n)
{
    size_t n_byte = n / 8;
    size_t n_bit  = n % 8;

    buf[n_byte] &= ~((uint8_t) 1 << n_bit);
}

int gdsi_bitvecl_get(const uint8_t *buf, size_t n)
{
    size_t n_byte = n / 8;
    size_t n_bit  = n % 8;

    return buf[n_byte] & (1 << n_bit);
}

void gdsi_bitvec_set_all(struct gdsi_bitvec *bv)
{
    gdsi_bitvecl_set_all(bv->buf, bv->max);
}

void gdsi_bitvec_clear_all(struct gdsi_bitvec *bv)
{
    gdsi_bitvecl_clear_all(bv->buf, bv->max);
}

struct gdsi_bitvec *gdsi_bitvec_new(size_t max)
{
    struct gdsi_bitvec *bv;
    size_t nb = gdsi_bitvecl_size(max);

    bv = malloc(sizeof(*bv) + sizeof(uint8_t) * nb);
    if (bv == NULL)
        return bv;

    bv->max = max;
    gdsi_bitvec_clear_all(bv);

    return bv;
}

void gdsi_bitvec_set(struct gdsi_bitvec *bv, size_t n)
{
    gdsi_assert(n < bv->max);

    gdsi_bitvecl_set(bv->buf, n);
}

void gdsi_bitvec_clear(struct gdsi_bitvec *bv, size_t n)
{
    gdsi_assert(n < bv->max);

    gdsi_bitvecl_clear(bv->buf, n);
}

int gdsi_bitvec_get(const struct gdsi_bitvec *bv, size_t n)
{
    gdsi_assert(n < bv->max);

    return gdsi_bitvecl_get(bv->buf, n);
}

uint8_t *gdsi_bitvec_raw_buffer(struct gdsi_bitvec *bv)
{
    return bv->buf;
}

int gdsi_bitvec_ffs(const struct gdsi_bitvec *bv, size_t start)
{
    size_t byte_start = start / 8;

    if (start >= bv->max)
        return -1;

    if (start % 8 != 0) {
        size_t bs = start % 8;
        size_t bi;
        for (bi = bs; bi < 8; bi++)
            if (bv->buf[byte_start] & (1 << bi))
                return byte_start * 8 + bi;
        byte_start++;
    }

    size_t by, by_max = gdsi_bitvecl_size(bv->max);
    int ret = byte_start * 8;

    for (by = byte_start; by < by_max; by++) {
        int r = ffs(bv->buf[by]);
        if (r > 0)
            return ret + r - 1;
        ret += 8;
    }

    return -1;
}

void gdsi_bitvec_delete(struct gdsi_bitvec *bv)
{
    free(bv);
}
