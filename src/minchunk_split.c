/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.
*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "gds_internal.h"

struct gdsi_minchunk {
    size_t nprocs;
    size_t ndims;
    size_t **n_chunk_sizes;
    size_t *n_chunk_elms_d; /* per dim */
    size_t *n_chunk_elms_p; /* per proc */
};

static struct gdsi_minchunk *alloc(size_t nprocs, size_t ndims)
{
    struct gdsi_minchunk *mc;

    mc = calloc(sizeof(*mc), 1);
    if (mc == NULL)
        goto out_fail;

    mc->nprocs = nprocs;
    mc->ndims  = ndims;
    mc->n_chunk_sizes = calloc(sizeof(size_t *), ndims);
    if (mc->n_chunk_sizes == NULL)
        goto out_fail;

    mc->n_chunk_elms_d = malloc(sizeof(size_t) * ndims);
    if (mc->n_chunk_elms_d == NULL)
        goto out_fail;

    mc->n_chunk_elms_p = malloc(sizeof(size_t) * nprocs);
    if (mc->n_chunk_elms_p == NULL)
        goto out_fail;

    return mc;

out_fail:
    gdsi_minchunk_free(mc);

    return NULL;
}

static void decompose(size_t flat_index, size_t ndims, const size_t dims[], size_t indices[])
{
    gdsi_decompose_index_impl(flat_index, ndims, dims, indices);
}

static void increment(size_t ndims, const size_t n_chunk_elms[], size_t ind[])
{
    size_t i = ndims;

    do {
        i--;
        if (++ind[i] < n_chunk_elms[i])
            break;
        /* carry */
        ind[i] = 0;
    } while (i > 0);
}

/*
 if l < r?
*/
static bool less(size_t ndims, const size_t lhs[], const size_t rhs[])
{
    size_t i;
    for (i = 0; i < ndims; i++)
        if (lhs[i] < rhs[i])
            return true;

    return false;
}

static bool is_cuboid(size_t ndims, const size_t n_chunk_elms[], size_t start_chunk, size_t n_chunks)
{
    size_t start_ind[ndims], max_ind[ndims], end_ind[ndims];
    size_t ind[ndims];
    size_t i;

    decompose(start_chunk, ndims, n_chunk_elms, start_ind);
    decompose(start_chunk + n_chunks - 1, ndims, n_chunk_elms, end_ind);
    for (i = 0; i < ndims; i++) {
        ind[i] = start_ind[i];
        max_ind[i] = start_ind[i];
    }

    for (i = 0; i < n_chunks; i++) {
        if (less(ndims, ind, start_ind))
            return false;
        if (less(ndims, max_ind, ind)) {
            memcpy(max_ind, ind, sizeof(ind));
            if (less(ndims, end_ind, ind))
                return false;
        }
        increment(ndims, n_chunk_elms, ind);
    }

    return true;
}

static size_t shrink_to_cuboid(size_t ndims, const size_t n_chunk_elms[], size_t start_chunk, size_t n_chunks)
{
    do {
        n_chunks--;
        if (is_cuboid(ndims, n_chunk_elms, start_chunk, n_chunks))
            return n_chunks;
    } while (n_chunks > 0);

    /* 1 should success always */
    gdsi_assert(false);

    return 1;
}

/*
  Overall algorithm of minchunk split:

  1. split each dimension so that it satisfies min_chunk constraint in
  that dimension.
  As a result, the array is divided into several chunks. Each chunk
  meets the min_chunk constraints
  2. assign contiguous chunks to each process, under the condition that
  the assigned region must form cuboid (i.e. rectangle in 2-d array case).
*/
gdsi_minchunk_t *gdsi_minchunk_split(size_t nprocs, size_t ndims, const size_t dims[], const size_t min_chunks[])
{
    struct gdsi_minchunk *mc;
    size_t tot_chunk_elms = 1;
    size_t d;
    char dims_s[32], min_chunk_s[32];

    gdsi_snprint_index(dims_s, sizeof(dims_s), ndims, dims);
    if (min_chunks != NULL)
        gdsi_snprint_index(min_chunk_s, sizeof(min_chunk_s), ndims, min_chunks);
    else
        strncpy(min_chunk_s, "(null)", sizeof(min_chunk_s));

    gdsi_dprintf(GDS_DEBUG_INFO, "minchunk_split("
        "nprocs=%zu ndims=%zu, dims=[%s], min_chunk=[%s], ...)\n",
        nprocs, ndims, dims_s, min_chunk_s);

    mc = alloc(nprocs, ndims);
    if (mc == NULL)
        return NULL;

    for (d = 0; d < ndims; d++) {
        size_t m = (min_chunks == NULL || min_chunks[d] == 0) ?
            1 : min_chunks[d];
        size_t i, l, rem, nc;

        nc = dims[d] / m;
        if (nc > nprocs)
            nc = nprocs;

        mc->n_chunk_sizes[d] = malloc(sizeof(size_t) * nc);
        if (mc->n_chunk_sizes[d] == NULL)
            goto out_fail;

        tot_chunk_elms *= nc;
        mc->n_chunk_elms_d[d] = nc;

        /* Split this direction into n_chunk_elms_d[d] parts */
        l = dims[d] / nc;
        rem = dims[d] - l * nc;
        gdsi_assert(nc <= nprocs);
        for (i = 0; i < nc; i++) {
            size_t r = 0;
            if (rem > 0) {
                r = 1;
                rem--;
            }
            mc->n_chunk_sizes[d][i] = l + r;
        }
    }

    size_t celm_per_proc = tot_chunk_elms;
    size_t consumed_chunks = 0, p;

    for (p = 0; p < nprocs; p++) {
        celm_per_proc = (tot_chunk_elms - consumed_chunks) / (nprocs - p);

        if (!is_cuboid(ndims, mc->n_chunk_elms_d,
                consumed_chunks, celm_per_proc)) {
            celm_per_proc = shrink_to_cuboid(ndims,
                mc->n_chunk_elms_d, consumed_chunks, celm_per_proc);
        }
        mc->n_chunk_elms_p[p] = celm_per_proc;
        consumed_chunks += celm_per_proc;
    }

    /* TODO: is this always achievable?? We haven't proven this! */
    gdsi_assert(consumed_chunks == tot_chunk_elms);

    return mc;

out_fail:
    gdsi_minchunk_free(mc);

    return NULL;
}

void gdsi_minchunk_get(const gdsi_minchunk_t *mc, size_t proc, size_t res_ind[], size_t res_len[])
{
    size_t p, d, consumed_chunks = 0;
    size_t start_ind[mc->ndims], end_ind[mc->ndims];

    for (p = 0; p < proc; p++)
        consumed_chunks += mc->n_chunk_elms_p[p];

    decompose(consumed_chunks, mc->ndims, mc->n_chunk_elms_d, start_ind);

    for (d = 0; d < mc->ndims; d++) {
        size_t j, len = 0;

        gdsi_assert(start_ind[d] <= mc->nprocs);

        for (j = 0; j < start_ind[d]; j++)
            len += mc->n_chunk_sizes[d][j];
        res_ind[d] = len;
    }

    if (mc->n_chunk_elms_p[proc] == 0) {
        memset(res_len, 0, sizeof(res_len[0]) * mc->ndims);
        return;
    }

    decompose(consumed_chunks + mc->n_chunk_elms_p[proc] - 1,
        mc->ndims, mc->n_chunk_elms_d, end_ind);

    for (d = 0; d < mc->ndims; d++) {
        size_t j, len = 0;

        gdsi_assert(end_ind[d] <= mc->nprocs);

        for (j = start_ind[d]; j <= end_ind[d]; j++)
            len += mc->n_chunk_sizes[d][j];
        res_len[d] = len;
    }
}

static size_t get_chunk_size(const gdsi_minchunk_t *mc,
    size_t consumed_chunks, size_t proc)
{
    size_t start_ind[mc->ndims], end_ind[mc->ndims];
    size_t d, ret = 1;

    decompose(consumed_chunks, mc->ndims, mc->n_chunk_elms_d, start_ind);
    decompose(consumed_chunks + mc->n_chunk_elms_p[proc] - 1,
        mc->ndims, mc->n_chunk_elms_d, end_ind);

    for (d = 0; d < mc->ndims; d++) {
        size_t j, len = 0;

        gdsi_assert(end_ind[d] <= mc->nprocs);

        for (j = start_ind[d]; j <= end_ind[d]; j++)
            len += mc->n_chunk_sizes[d][j];
        ret *= len;
    }

    return ret;
}

size_t gdsi_minchunk_size(const gdsi_minchunk_t *mc, size_t proc)
{
    size_t p, consumed_chunks = 0;

    if (mc->n_chunk_elms_p[proc] == 0)
        return 0;

    for (p = 0; p < proc; p++)
        consumed_chunks += mc->n_chunk_elms_p[p];

    return get_chunk_size(mc, consumed_chunks, proc);
}

size_t gdsi_minchunk_maxsize(const gdsi_minchunk_t *mc)
{
    size_t p, consumed_chunks = 0;
    size_t ret = 0;

    for (p = 0; p < mc->nprocs; p++) {
        size_t size_p;

        if (mc->n_chunk_elms_p[p] == 0)
            continue;

        size_p = get_chunk_size(mc, consumed_chunks, p);

        consumed_chunks += mc->n_chunk_elms_p[p];

        if (ret < size_p)
            ret = size_p;
    }

    return ret;
}

void gdsi_minchunk_free(gdsi_minchunk_t *mc)
{
    if (mc == NULL)
        return;

    free(mc->n_chunk_elms_p);
    free(mc->n_chunk_elms_d);

    if (mc->n_chunk_sizes != NULL) {
        size_t d;
        for (d = 0; d < mc->ndims; d++)
            free(mc->n_chunk_sizes[d]);
        free(mc->n_chunk_sizes);
    }

    free(mc);
}
