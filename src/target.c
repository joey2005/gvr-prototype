/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.

  This module implements the target-side services.
*/
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <dirent.h>
#include <unistd.h>

#include <limits.h>
#include "sglib.h"

#include "gds_internal.h"

#ifdef GDS_CONFIG_USE_LRDS
#include <lrds.h>
#endif

struct gdsi_target_param {
    enum gdsi_target_message_tag tags[GDS_TGTREQ_LAST];
    int n_tags;
};

struct gdsi_target_checkpoint
{
    struct gdsi_target_checkpoint *newer, *older;
    size_t version_number;

    /* Flags to show the location of checkpoint
       -- combination of gdsi_ckpt_location_flag */
    int ckpt_location_flags;
    void *buffer; /* pointer to memory buffer */
    int array_id; /* unique array_id on process 0, for filenaming */
#ifdef GDS_CONFIG_USE_LRDS
    uint32_t lrds_version_id; /* version ID in LRDS internal */
#endif
    int chunk_number;
    size_t chunk_size;
    off64_t offset; /* offset in file */
};

struct gdsi_target_fetch_priv {
    char *send_buf;
};

static pthread_t *target_threads;
static void *target_thread_entry(void *);

/* Single, world-wide communicator to be used between clients and targets */
MPI_Comm gdsi_comm_world;
MPI_Group gdsi_group_world;

MPI_Comm gdsi_comm_target;

static volatile int target_doorbell_area;
static int target_doorbell_prev;
static MPI_Win target_doorbell_win;

/* TODO: should be hash table */
static struct gdsi_target_array_desc *array_descs = NULL;
static pthread_mutex_t array_descs_lock = PTHREAD_MUTEX_INITIALIZER;

static int array_id_max;

static void flat_fetch(struct  gdsi_target_array_desc *desc,
    const struct gdsi_target_fetch *ft, int client_rank,
    struct gdsi_target_fetch_priv *priv);
static void log_fetch(struct  gdsi_target_array_desc *desc,
    const struct gdsi_target_fetch *ft, int client_rank,
    struct gdsi_target_fetch_priv *priv);

static void flat_put(struct  gdsi_target_array_desc *desc,
    const struct gdsi_target_put *lw, int client_rank,
    struct gdsi_target_put_priv *priv);
static void log_put(struct  gdsi_target_array_desc *desc,
    const struct gdsi_target_put *lw, int client_rank,
    struct gdsi_target_put_priv *priv);

static GDS_status_t flat_increment_version(struct gdsi_target_array_desc *desc);
static GDS_status_t log_increment_version(struct gdsi_target_array_desc *desc);
#ifdef GDS_CONFIG_USE_LRDS
static GDS_status_t lrds_increment_version(struct gdsi_target_array_desc *desc);
#endif

#if defined(__GCC__) && defined(__i386__)
/*
  http://stackoverflow.com/questions/14783782/which-inline-assembly-code-is-correct-for-rdtscp
*/
static inline uint64_t rdtscp(void)
{
    uint64_t a, d;
    __asm__ __volatile__ ("rdtscp" : "=a"(a), "=d"(d) :: "%rcx");
    return (d << 32) + a;
}
#else
static inline uint64_t rdtscp(void)
{
    return 0;
}
#endif

/* TODO: this is only effective on Midway */
static size_t wait_table[] = {
    629, /* 16 */
    495, /* 32 */
    469, /* 64 */
    479, /* 128 */
    527, /* 256 */
    565, /* 512 */
    603, /* 1024 */
    1037, /* 2048 */
    1279, /* 4096 */
    1663, /* 8192 */
    2931, /* 16384 */
    4863, /* 32768 */
    8553, /* 65536 */
    15719, /* 131072 */
    30973, /* 262144 */
    63017, /* 524288 */
    166175, /* 1048576 */
    406187, /* 2097152 */
    886841, /* 4194304 */
    1867775, /* 8388608 */
    3780101, /* 16777216 */
    7627009, /* 33554432 */
    15328503, /* 67108864 */
    30709199, /* 134217728 */
    61429699, /* 268435456 */
    122787515, /* 536870912 */
    245481521, /* 1073741824 */
};

size_t wait_table2[] = {
    2047, /* 16 */
    2047, /* 32 */
    2047, /* 64 */
    2047, /* 128 */
    2047, /* 256 */
    2047, /* 512 */
    1535, /* 1024 */
    3071, /* 2048 */
    6143, /* 4096 */
    12287, /* 8192 */
    24575, /* 16384 */
    47203, /* 32768 */
    82863, /* 65536 */
    155647, /* 131072 */
    316775, /* 262144 */
    663551, /* 524288 */
    1572863, /* 1048576 */
    3145727, /* 2097152 */
    6291455, /* 4194304 */
    12582911, /* 8388608 */
    25165823, /* 16777216 */
    50331647, /* 33554432 */
    100663295, /* 67108864 */
    201326591, /* 134217728 */
    402653183, /* 268435456 */
    805306367, /* 536870912 */
    1610612735, /* 1073741824 */
};

static void *slow_memcpy_impl(void *dest, const void *src, size_t n, size_t wait)
{
    size_t i;
    memcpy(dest, src, n);
    for (i = 0; i < wait; i++)
        rdtscp();
    return dest;
}

/* Emulates a slow memory copy to NVRAM devices */
void *slow_memcpy(void *dest, const void *src, size_t n)
{
    /* Assuming that # of table entries are the same for all wait tables */
    size_t s, w, wt_max = sizeof(wait_table) / sizeof(wait_table[0]);
    const size_t *wt;
    int i;

    switch (gdsi_enable_slow_memcpy) {
    case 1:
        wt = wait_table;
        break;

    case 2:
        wt = wait_table2;
        break;

    default:
        return memcpy(dest, src, n);
    }

    if (n <= 16)
        w = wt[0];
    else {
        for (s = 32, i = 1; i < wt_max; i++, s *= 2) {
            if (n < s) {
                w = (2*(s-n)*wt[i-1] + (2*n-s)*wt[i]) / s;
                goto found;
            }
        }
        w = n * wt[wt_max - 1] / (s / 2);
    }
found:
    return slow_memcpy_impl(dest, src, n, w);
}

pthread_mutex_t gdsi_simfail_mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t gdsi_simfail_pause = PTHREAD_COND_INITIALIZER;

static void add_array_desc(struct gdsi_target_array_desc *desc)
{
    pthread_mutex_lock(&array_descs_lock);
    SGLIB_DL_LIST_ADD(struct gdsi_target_array_desc, array_descs, desc, prev, next);
    pthread_mutex_unlock(&array_descs_lock);
}

static struct gdsi_target_array_desc *find_array_desc(int array_id)
{

    struct gdsi_target_array_desc *desc;

    pthread_mutex_lock(&array_descs_lock);

    SGLIB_DL_LIST_GET_FIRST(struct gdsi_target_array_desc,
                            array_descs, prev, next, desc);
    for (; desc != NULL; desc = desc->next) {
        if (desc->array_id == array_id)
            break;
    }

    pthread_mutex_unlock(&array_descs_lock);

    return desc;
}

struct gdsi_target_array_desc *gdsi_find_array_desc(int array_id)
{
    return find_array_desc(array_id);
}

static int array_desc_comparator(const struct gdsi_target_array_desc *l,
                                 const struct gdsi_target_array_desc *r)
{
    return l->array_id - r->array_id;
}

static ssize_t ckpt_callback(int full, const void* src_buf,
                             size_t src_relative, size_t src_size,
                             void* shadow_buf,
                             void* priv)
{
    gdsi_dprintf(GDS_DEBUG_INFO,
                  "full=%d src_relative=%zu src_size=%zu shadow_buf=%p\n",
                  full, src_relative, src_size, shadow_buf);

    struct gdsi_target_array_desc *desc = priv;
    struct gdsi_target_checkpoint *ckpt;
    void *bufhead;
    const size_t alloc_size = desc->singlever_size;

    gdsi_assert(full);

    gdsi_assert(alloc_size >= src_relative + src_size);

    if (src_relative > 0)
        gdsi_dprintf(GDS_DEBUG_ERR,
                      "ckpt_callback: src_relative=%zu\n", src_relative);

    ckpt = malloc(sizeof(*ckpt));
    if (ckpt == NULL)
        return -1;

    ckpt->buffer = malloc(alloc_size);
    if (ckpt->buffer == NULL) {
        free(ckpt);
        return -1;
    }

    ckpt->ckpt_location_flags = GDS_CKPT_ON_MEMORY;
    ckpt->version_number = desc->current_version;
    ckpt->chunk_number = desc->chunk_number;
    ckpt->chunk_size = desc->singlever_size;
    gdsi_dprintf(GDS_DEBUG_INFO, "recording version: %zu\n", ckpt->version_number);

    bufhead = ckpt->buffer;

    slow_memcpy(bufhead+src_relative, src_buf, src_size);

    SGLIB_DL_LIST_ADD(struct gdsi_target_checkpoint,
                      desc->checkpoints, ckpt, newer, older);
    desc->checkpoints = ckpt;

    gdsi_dprintf(GDS_DEBUG_INFO, "ckpt_callback: version %zu chunk %d\n",
    ckpt->version_number, ckpt->chunk_number);

    return alloc_size;
}

static void target_param_fill_all(struct gdsi_target_param *p)
{
    int i;
    p->n_tags = GDS_TGTREQ_LAST;

    for (i = 0; i < p->n_tags; i++)
        p->tags[i] = i;
}

static void target_param_fill_worker(struct gdsi_target_param *p)
{
    int i = 0;

    p->tags[i++] = GDS_TGTREQ_FETCH;
    p->tags[i++] = GDS_TGTREQ_PUT;
    p->tags[i++] = GDS_TGTREQ_EXIT;
    p->n_tags = i;
}

GDS_status_t gdsi_target_init(void)
{
    int t;
    const int n_threads = gdsi_server_threads;
    GDS_status_t ret;
#ifdef GDS_CONFIG_USE_LRDS
    int lrds_thread_level;
#endif

    if (n_threads < 1) {
        gdsi_dprintf(GDS_DEBUG_ERR,
            "Number of server threads must be greater than zero!\n");
        return GDS_STATUS_INVALID;
    }

    target_threads = malloc(sizeof(pthread_t) * n_threads);
    if (target_threads == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR,
            "Failed to allocate target thread structures\n");
        ret = GDS_STATUS_NOMEM;
        goto fail;
    }

#ifdef GDS_CONFIG_USE_LRDS
    /* TODO: target server may be multi-threaded */
    if (lrds_init(LRDS_THREAD_SINGLE, &lrds_thread_level) != LRDS_SUCCESS) {
        gdsi_dprintf(GDS_DEBUG_ERR, "Failed to initialize LRDS\n");
        ret = GDS_STATUS_NOMEM; /* TODO: consider return code */
        goto fail;
    }
#endif /* GDS_CONFIG_USE_LRDS */

    GDSI_MPI_CALL(MPI_Comm_dup(MPI_COMM_WORLD, &gdsi_comm_world));
    GDSI_MPI_CALL(MPI_Comm_dup(MPI_COMM_WORLD, &gdsi_comm_target));

    MPI_Comm_group(gdsi_comm_world, &gdsi_group_world);

    for (t = 0; t < n_threads; t++) {
        struct gdsi_target_param *param;
        param = malloc(sizeof(*param));
        gdsi_assert(param);

        if (t == 0)
            target_param_fill_all(param);
        else
            target_param_fill_worker(param);

        int e = pthread_create(&target_threads[t], NULL,
            target_thread_entry, param);

        if (e != 0)
            gdsi_panic("Failed to create target thread: %s\n", strerror(e));
    }
    /* TODO: cleanup seriously on error-exit case */

    MPI_Win_create((void *) &target_doorbell_area, sizeof(int), sizeof(int),
        MPI_INFO_NULL, gdsi_comm_world, &target_doorbell_win);
    MPI_Win_lock_all(MPI_MODE_NOCHECK, target_doorbell_win);

    return GDS_STATUS_OK;

fail:
    free(target_threads);
    return ret;
}

void gdsi_target_exit(void)
{
    int dummy;
    int rank;
    int t;
    const int n_threads = gdsi_server_threads;

    gdsi_dprintf(GDS_DEBUG_INFO, "Requesting exit to threads...\n");
    GDSI_MPI_CALL(MPI_Comm_rank(gdsi_comm_world, &rank));

    for (t = 0; t < n_threads; t++)
        GDSI_MPI_CALL(MPI_Send(&dummy, 1, MPI_INT, rank, GDS_TGTREQ_EXIT,
                gdsi_comm_world));
    gdsi_target_signal_thread(rank);
    for (t = 0; t < n_threads; t++)
        pthread_join(target_threads[t], NULL);

    MPI_Win_unlock_all(target_doorbell_win);
    MPI_Win_free(&target_doorbell_win);

#ifdef GDS_CONFIG_USE_LRDS
    lrds_finalize();
#endif

    MPI_Group_free(&gdsi_group_world);
    MPI_Comm_free(&gdsi_comm_world);
    MPI_Comm_free(&gdsi_comm_target);
}

static int alloc_array_id(void)
{
    int id;

    pthread_mutex_lock(&array_descs_lock);
    /* TODO: cyclic, array ID reuse */
    id = array_id_max++;
    pthread_mutex_unlock(&array_descs_lock);

    return id;
}

static size_t calc_alloc_bytes(const struct gdsi_target_array_desc *desc,
    int log_prealloc_vers)
{
    switch (desc->representation) {
    case GDSI_REPR_LOG:
        return log_memory_area_size(desc->singlever_size, desc->log_bs,
            log_prealloc_vers);

    default:
        return desc->singlever_size;
    }
}

/*
  Array raw data structure at the head:
  |this region| represents one block

  |[log_tail][empty][...]|zero page|MD for ver. 0|data written in ver .0|...

  Currently, the first block is only used for log tail pointer.
 */
static void log_init_structure(struct gdsi_target_array_desc *desc)
{
    size_t nblocks, i;

    pthread_mutex_init(&desc->log_lock, NULL);

    nblocks = log_required_blocks(0, desc->singlever_size, desc->log_user_bs);

    desc->log_tailp = (size_t *) desc->buff;
    *desc->log_tailp = desc->log_bs;

    /* Create a "zero page" */
    memset(desc->buff + desc->log_bs, 0, desc->log_bs);

    desc->log_cur_md_head = *desc->log_tailp = desc->log_bs * 2;

    gdsi_log_md_t *md = (gdsi_log_md_t *) (desc->buff + *desc->log_tailp);
    for (i = 0; i < nblocks; i++) {
        /* Let every metadata block point to the initial zero block,
           which is considered to be "version -1" */
        md[i] = log_byteoff_to_md(desc->log_bs, desc->log_bs);
    }

    *desc->log_tailp += log_md_area_size(desc->singlever_size, desc->log_bs);

    gdsi_dprintf(GDS_DEBUG_INFO,
        "log_init_structure: nblocks=%zu, cur_md_head=%zu, log_tail=%zu\n",
        nblocks, desc->log_cur_md_head, *desc->log_tailp);
}

static inline
int log_md_updated(gdsi_log_md_t md, size_t log_bs, size_t cur_md_head)
{
    return log_md_to_byteoff(md, log_bs) > cur_md_head;
}

static gdsi_log_md_t *log_md_ptr(struct gdsi_target_array_desc *desc,
    size_t offset)
{
    gdsi_log_md_t *md;
    size_t off_aligned = log_aligned_prev(offset, desc->log_user_bs);
    size_t mdi;

    md = (gdsi_log_md_t *)(desc->buff + desc->log_cur_md_head);
    mdi = off_aligned / desc->log_user_bs;

    return md + mdi;
}

static void log_flush(struct gdsi_target_array_desc *desc,
    gdsi_log_md_t *blocks, int n_blocks)
{
    int i;
    char tmp_buf[desc->log_user_bs];

    for (i = 0; i < n_blocks; i++) {
        size_t byteoff = log_md_to_byteoff(blocks[i], desc->log_user_bs);
        slow_memcpy(tmp_buf, desc->buff + byteoff, desc->log_user_bs);
    }
}

static void log_alloc_data_buffer(struct gdsi_target_array_desc *desc,
    size_t offset, size_t len, bool copy_all)
{
    size_t i, n_blks, start_mdi, last_mdi;
    size_t off_aligned = log_aligned_prev(offset, desc->log_user_bs);
    size_t req_blks = 0, free_blks;
    size_t pad_front, pad_back;
    gdsi_log_md_t *md, md_head_orig, md_tail_orig;

    n_blks = log_required_blocks(offset, len, desc->log_user_bs);

    pad_front = log_pad_front(offset, desc->log_user_bs);
    pad_back  = log_pad_back(offset + len, desc->log_user_bs);

    start_mdi = off_aligned / desc->log_user_bs;
    last_mdi  = start_mdi + n_blks;

    pthread_mutex_lock(&desc->log_lock);

    free_blks = (desc->log_area_size - *desc->log_tailp) / desc->log_bs;

    gdsi_dprintf(GDS_DEBUG_INFO, "log_alloc_data_buffer: log_tail=%zu\n",
        *desc->log_tailp);

    md = log_md_ptr(desc, 0);

    md_head_orig = md[start_mdi];
    md_tail_orig = md[last_mdi - 1];

    for (i = start_mdi; i < last_mdi; i++) {
        gdsi_dprintf(GDS_DEBUG_INFO, "md[%zu](%p)=%u\n", i, &md[i], md[i]);
        if (!log_md_updated(md[i], desc->log_bs, desc->log_cur_md_head)) {
            size_t byteoff = *desc->log_tailp + desc->log_bs * req_blks;
            if (copy_all)
                memcpy(desc->buff + byteoff,
                    desc->buff + log_md_to_byteoff(md[i], desc->log_bs),
                    desc->log_bs);
            log_flush(desc, &md[i], 1);
            md[i] = log_byteoff_to_md(byteoff, desc->log_bs);
            req_blks++;

            if (req_blks > free_blks)
                gdsi_panic("Out of log area! Implement log cleaning!\n");

            gdsi_dprintf(GDS_DEBUG_INFO, "md[%zu]<=%u\n", i, md[i]);
        }
    }
    /* Fill both ends in front and back */
    if (md[start_mdi] != md_head_orig && pad_front > 0) {
        void *from = desc->buff
            + log_md_to_byteoff(md_head_orig, desc->log_bs);
        void *to = desc->buff
            + log_md_to_byteoff(md[start_mdi], desc->log_bs);
        gdsi_assert(to + pad_front <= desc->buff + desc->log_area_size);
        memcpy(to, from, pad_front);
    }
    if (md[last_mdi - 1] != md_tail_orig && pad_back > 0) {
        void *from = desc->buff
            + log_md_to_byteoff(md_tail_orig, desc->log_bs)
            + desc->log_bs - pad_back;
        void *to = desc->buff
            + log_md_to_byteoff(md[last_mdi - 1], desc->log_bs)
            + desc->log_bs - pad_back;
        gdsi_assert(to + pad_back <= desc->buff + desc->log_area_size);
        memcpy(to, from, pad_back);
    }

    *desc->log_tailp += req_blks * desc->log_bs;
    gdsi_assert(*desc->log_tailp <= desc->log_area_size);

    pthread_mutex_unlock(&desc->log_lock);
}

static void log_iterate_buffer_at(struct gdsi_target_array_desc *desc,
    gdsi_log_md_t *md, size_t offset, size_t len, void *priv,
    void (*op)(void *array_buf, size_t offset, size_t len, void *priv))
{
    size_t start_mdi, n_blks;
    size_t off_aligned = log_aligned_prev(offset, desc->log_user_bs);
    size_t pad_front;
    size_t i;

    start_mdi = off_aligned / desc->log_user_bs;
    n_blks = log_required_blocks(offset, len, desc->log_user_bs);
    pad_front = log_pad_front(offset, desc->log_user_bs);

    size_t copied = 0;
    for (i = start_mdi; i < start_mdi + n_blks; i++) {
        void *buf = desc->buff + log_md_to_byteoff(md[i], desc->log_bs)
            + pad_front;
        size_t copy_len = desc->log_user_bs - pad_front;

        if (len - copied < copy_len)
            copy_len = len - copied;

        gdsi_assert(buf + copy_len <= desc->buff + desc->log_area_size);
        op(buf, copied, copy_len, priv);

        copied += copy_len;
        pad_front = 0;
    }
}

static void log_iterate_buffer(struct gdsi_target_array_desc *desc,
    size_t offset, size_t len, void *priv,
    void (*op)(void *array_buf, size_t offset, size_t len, void *priv))
{
    gdsi_log_md_t *md = log_md_ptr(desc, 0);
    log_iterate_buffer_at(desc, md, offset, len, priv, op);
}

static void do_write_op(void *array_buf, size_t offset, size_t len, void *priv)
{
    void *user_buf = priv + offset;
    memcpy(array_buf, user_buf, len);
}

struct gdsi_log_acc {
    const void *buff;
    int count;
    MPI_Datatype type;
    MPI_Op op;
};

static void do_acc_op(void *array_buf, size_t offset, size_t len, void *priv)
{
    struct gdsi_log_acc *acc = priv;
    const void *user_buf = acc->buff + offset;
    int unit_size;

    MPI_Type_size(acc->type, &unit_size);
    MPI_Reduce_local(user_buf, array_buf, len/unit_size, acc->type, acc->op);
}

static void log_acc_data(struct gdsi_target_array_desc *desc,
    size_t elm_off, void *inbuf, size_t count, MPI_Datatype type, MPI_Op op)
{
    struct gdsi_log_acc acc = { .buff  = inbuf,
                                 .count = count,
                                 .type  = type,
                                 .op    = op, };
    size_t offset = desc->disp_unit * elm_off;
    size_t len    = desc->disp_unit * count;

    log_iterate_buffer(desc, offset, len, (void *) &acc, do_acc_op);
}

static void log_write_data(struct gdsi_target_array_desc *desc,
    size_t offset, size_t len, const void *data)
{
    log_alloc_data_buffer(desc, offset, len, false);
    log_iterate_buffer(desc, offset, len, (void *) data, do_write_op);
}

static void do_read_op(void *array_buf, size_t offset, size_t len, void *priv)
{
    void *user_buf = priv + offset;
    memcpy(user_buf, array_buf, len);
}

static void log_read_data(struct gdsi_target_array_desc *desc,
    size_t offset, size_t len, void *data)
{
    log_iterate_buffer(desc, offset, len, data, do_read_op);
}

static void log_read_data_at(struct gdsi_target_array_desc *desc,
    gdsi_log_md_t *md, size_t offset, size_t len, void *data)
{
    log_iterate_buffer_at(desc, md, offset, len, data, do_read_op);
}

struct gdsi_log_send {
    int client_rank;
    int tag;
};

static void do_send_op(void *array_buf, size_t offset, size_t len, void *priv)
{
    struct gdsi_log_send *ls = priv;

    gdsi_dprintf(GDS_DEBUG_INFO, "sending %zu bytes to rank %d, tag=%d\n",
        len, ls->client_rank, ls->tag);
    GDSI_MPI_CALL(MPI_Send(array_buf, len, MPI_BYTE, ls->client_rank,
            ls->tag, gdsi_comm_world));
    gdsi_dprintf(GDS_DEBUG_INFO, "sent %zu bytes to rank %d, tag=%d\n",
        len, ls->client_rank, ls->tag);
}

static void log_send_data(struct gdsi_target_array_desc *desc,
    size_t elm_off, size_t count, int client_rank, int tag)
{
    struct gdsi_log_send ls = { .client_rank = client_rank, .tag = tag, };
    size_t offset = desc->disp_unit * elm_off;
    size_t len    = desc->disp_unit * count;

    log_iterate_buffer(desc, offset, len, &ls, do_send_op);
}

struct gdsi_log_recv {
    int client_rank;
    MPI_Request *reqs;
    int tag;
};

static void do_recv_op(void *array_buf, size_t offset, size_t len, void *priv)
{
    struct gdsi_log_recv *lr = priv;

    gdsi_assert(len > 0);

    GDSI_MPI_CALL(MPI_Recv(array_buf, len, MPI_BYTE, lr->client_rank,
            lr->tag, gdsi_comm_world, MPI_STATUS_IGNORE));
}

static void log_recv_data(struct gdsi_target_array_desc *desc,
    size_t elm_off, size_t count, int client_rank, int tag)
{
    struct gdsi_log_recv lr = {
        .client_rank = client_rank,
        .tag = tag,
    };
    size_t offset = desc->disp_unit * elm_off;
    size_t len    = desc->disp_unit * count;

    log_iterate_buffer(desc, offset, len, &lr, do_recv_op);
}

static GDS_status_t log_increment_version(struct gdsi_target_array_desc *desc)
{
    size_t md_area_size = log_md_area_size(desc->singlever_size, desc->log_bs);
    gdsi_log_md_t *md = log_md_ptr(desc, 0);
    struct gdsi_target_checkpoint *ckpt;

    ckpt = malloc(sizeof(*ckpt));
    if (ckpt == NULL)
        gdsi_panic("Failed to allocate checkpoint structure.\n");
    ckpt->buffer = md;
    ckpt->ckpt_location_flags = GDS_CKPT_ON_MEMORY;
    ckpt->version_number = desc->current_version;
    ckpt->chunk_number = desc->chunk_number; /* TODO: I don't understand the
                                                rationale behind associating
                                                desc with chunk_number, but just
                                                follow the way as in flat array,
                                                for now. */
    SGLIB_DL_LIST_ADD(struct gdsi_target_checkpoint,
                      desc->checkpoints, ckpt, newer, older);
    desc->checkpoints = ckpt;

    pthread_mutex_lock(&desc->log_lock);

    if (desc->log_area_size - *desc->log_tailp < md_area_size)
        gdsi_panic("Out of log area! Implement log cleaning!\n");

    void *from = desc->buff + desc->log_cur_md_head;
    void *to   = desc->buff + *desc->log_tailp;
    memcpy(to, from, md_area_size);

     gdsi_dprintf(GDS_DEBUG_INFO,
         "log_increment_version: copied %zu bytes of md from %p to %p\n",
         md_area_size, from, to);

    desc->log_cur_md_head = *desc->log_tailp;
    *desc->log_tailp += md_area_size;

    gdsi_dprintf(GDS_DEBUG_INFO,
        "log_increment_version: buff=%p cur_md_head=%zu, log_tail=%zu\n",
        desc->buff, desc->log_cur_md_head, *desc->log_tailp);

    pthread_mutex_unlock(&desc->log_lock);

    return GDS_STATUS_OK;
}

/*
  TODO: this should work in another thread to exploit concurrency
*/
static void target_create(void *priv, const void *param, int client_rank)
{
    const struct gdsi_target_create *cr = param;
    struct gdsi_target_array_desc *desc = NULL;
    struct gdsi_target_alloc_result ret;
    size_t alloc_bytes = 0;
    int target_rank;
    int array_id;
    int local_failure = 0, global_failure;
    int win_disp_unit = -1;
    size_t alignment;

    gdsi_dprintf(GDS_DEBUG_INFO, "enter cr->disp_unit=%d\n", cr->disp_unit);

    /*
      Align main buffer to page boundary
      This is primarily for LRDS change tracking
     */
    int page_size = sysconf(_SC_PAGESIZE);
    if (page_size == -1) {
        gdsi_dprintf(GDS_DEBUG_ERR,
            "sysconf(_SC_PAGESIZE) failed, setting manually to 4096\n");
        page_size = 4096;
    }
    alignment = page_size;

    GDSI_MPI_CALL(MPI_Comm_rank(gdsi_comm_target, &target_rank));
    array_id = alloc_array_id();

    if (array_id < 0) {
        ret.status = GDS_STATUS_NOMEM;
        local_failure = 1;
        goto reduce_result;
    }

    desc = calloc(sizeof(*desc), 1);
    if (desc == NULL) {
        local_failure = 2;
        goto reduce_result;
    }

    desc->representation = cr->representation;

    switch (desc->representation) {
    case GDSI_REPR_FLAT:
        desc->fetch = flat_fetch;
        desc->put = flat_put;
        desc->increment_version = flat_increment_version;
        win_disp_unit = cr->disp_unit;
        break;

    case GDSI_REPR_LOG:
        desc->fetch = log_fetch;
        desc->put = log_put;
        desc->increment_version = log_increment_version;
        /* In the log-structured mode, allow byte addressing from
         the target side */
        win_disp_unit = 1;
        break;

#ifdef GDS_CONFIG_USE_LRDS
    case GDSI_REPR_LRDS:
        desc->fetch = flat_fetch;
        desc->put = flat_put;
        desc->increment_version = lrds_increment_version;
        win_disp_unit = cr->disp_unit;
        break;
#endif

    default:
        gdsi_panic("Unknown representation: %d\n", desc->representation);
    }

    desc->array_id = array_id;
    desc->flavor = cr->flavor;
    desc->disp_unit = cr->disp_unit;
    desc->singlever_size = cr->size * cr->disp_unit;
    desc->log_user_bs = cr->log_user_bs;
    desc->log_bs = desc->log_user_bs;
    alloc_bytes = calc_alloc_bytes(desc, cr->log_prealloc_vers);
#ifdef GDS_CONFIG_USE_LRDS
    desc->lrds_dirty_win = MPI_WIN_NULL;
#endif

    if (cr->flavor == GDS_FLAVOR_ALLOC) {
        if (alloc_bytes == 0) {
            gdsi_dprintf(GDS_DEBUG_INFO, "not allocating any memory\n");
            goto reduce_result;
        }

        gdsi_dprintf(GDS_DEBUG_INFO, "allocating %zu bytes\n", alloc_bytes);

        if (posix_memalign(&desc->buff, alignment, alloc_bytes) != 0)
            desc->buff = NULL;
    } else {
        desc->buff = cr->local_buffer; //passed in  null
    }
    if (desc->buff == NULL) {
        alloc_bytes = 0;
        local_failure = 3;
        goto reduce_result;
    }

    switch (desc->representation) {
    case GDSI_REPR_FLAT:
        /* Zero clear the buffer area so that get() without put()
           returns zero */
        memset(desc->buff, 0, alloc_bytes);
        break;

#ifdef GDS_CONFIG_USE_LRDS
    case GDSI_REPR_LRDS:
        gdsi_dprintf(GDS_DEBUG_INFO, "creating LRDS region: "
            "ptr=%p size=%zu dirty_tracking=%d versioning=%d\n",
            desc->buff, desc->singlever_size, cr->lrds_prop_dirty_tracking,
            cr->lrds_prop_versioning);
        lrds_property_set(LRDS_PROPERTY_DIRTY_TRACKING, &cr->lrds_prop_dirty_tracking);
        desc->lrds_dirty_tracking = cr->lrds_prop_dirty_tracking;
        lrds_property_set(LRDS_PROPERTY_VERSIONING, &cr->lrds_prop_versioning);
        lrds_property_set(LRDS_PROPERTY_DIRTY_BLOCK_SIZE, &cr->lrds_prop_dirty_block_size);
        if (cr->lrds_expose_dirty) {
            size_t n_blks = desc->singlever_size / cr->lrds_prop_dirty_block_size;
            size_t bytes;
            if (desc->singlever_size % cr->lrds_prop_dirty_block_size > 0)
                n_blks++;
            bytes = n_blks / 8;
            if (n_blks % 8 > 0)
                bytes++;
            MPI_Win_allocate(bytes, 1, MPI_INFO_NULL, gdsi_comm_world,
                &desc->lrds_dirty_buff, &desc->lrds_dirty_win);
            memset(desc->lrds_dirty_buff, 0, bytes);
            desc->lrds_dirty_buff_len = bytes;
        }
        int r = lrds_region_create(desc->buff, desc->singlever_size,
            &desc->lrds_region);
        if (r != LRDS_SUCCESS) {
            gdsi_dprintf(GDS_DEBUG_ERR, "LRDS region creation failed: r=%d\n", r);
            if (cr->lrds_expose_dirty)
                MPI_Win_free(&desc->lrds_dirty_win);
            local_failure = 4;
            goto reduce_result;
        }
#ifdef GDS_CONFIG_COUNTER
        size_t overhead;
        r = lrds_region_get_property(desc->lrds_region,
            LRDS_PROPERTY_OVERHEAD, &overhead);
        if (r != LRDS_SUCCESS)
            gdsi_dprintf(GDS_DEBUG_ERR, "LRDS region property get failed: r=%d\n", r);
        else
            gdsi_dprintf(GDS_DEBUG_ERR, "LRDS region overhead %zd\n", overhead);
#endif
        break;
#endif /* GDS_CONFIG_USE_LRDS */

    default:
        break;
    }

reduce_result:
    GDSI_MPI_CALL(MPI_Allreduce(&local_failure, &global_failure,
                                 1, MPI_INT, MPI_MAX, gdsi_comm_target));
    if (global_failure) {
        if (target_rank == 0) {
            gdsi_dprintf(GDS_DEBUG_ERR, "Global failure detected: %d\n",
                          global_failure);
        }

        if (desc != NULL) {
            if (desc->buff != NULL)
                free(desc->buff);
            free(desc);
        }
        ret.status = GDS_STATUS_NOMEM;
        goto out;
    }

    gdsi_dprintf(GDS_DEBUG_INFO, "creating MPI window\n");
    GDSI_MPI_CALL(MPI_Win_create(desc->buff, alloc_bytes, win_disp_unit,
                                  MPI_INFO_NULL, gdsi_comm_world, &desc->win));

    if (desc->representation == GDSI_REPR_LOG) {
        desc->log_area_size = alloc_bytes;
        log_init_structure(desc);
    }

    gdsi_dprintf(GDS_DEBUG_INFO, "adding desc: array_id=%d\n", desc->array_id);
    add_array_desc(desc);

    gdsi_dprintf(GDS_DEBUG_INFO, "exit\n");

    ret.array_id = array_id;
    ret.status = GDS_STATUS_OK;
    ret.start_index  = 0;

out:
     GDSI_MPI_CALL(
         MPI_Send(&ret, sizeof(ret), MPI_BYTE, client_rank,
             GDS_TGTREP_CREATE, gdsi_comm_world));

     if (ret.status == GDS_STATUS_OK
         && desc->representation == GDSI_REPR_LOG) {
         int n_mds = log_required_blocks(0, desc->singlever_size,
             desc->log_user_bs);
         gdsi_log_md_t cur_head = desc->log_cur_md_head / desc->log_bs;

         /* Send current metadata head position */
         GDSI_MPI_CALL(
             MPI_Send(&cur_head, 1, MPI_INT,
                 client_rank, GDS_TGTREP_CREATE, gdsi_comm_world)
             );
         /* Send log metadata cache to the client */
         GDSI_MPI_CALL(
             MPI_Send(desc->buff + desc->log_cur_md_head, n_mds, MPI_INT,
                 client_rank, GDS_TGTREP_CREATE, gdsi_comm_world)
             );
     }
}

static void target_free(void *priv, const void *param, int client_rank)
{
    int array_id = *((int *) param);
    struct gdsi_target_array_desc *desc, to_del;
    GDS_status_t ret;

    gdsi_dprintf(GDS_DEBUG_INFO, "enter: id=%d\n", array_id);

    to_del.array_id = array_id;

    pthread_mutex_lock(&array_descs_lock);
    SGLIB_DL_LIST_DELETE_IF_MEMBER(struct gdsi_target_array_desc,
                                   array_descs, &to_del,
                                   array_desc_comparator,
                                   prev, next,
                                   desc);
    pthread_mutex_unlock(&array_descs_lock);

    if (desc == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "array desc not found: id: %d", array_id);
        ret = GDS_STATUS_NOTEXIST;
        goto out;
    }

    while (desc->checkpoints != NULL) {
        struct gdsi_target_checkpoint *tmp;
        tmp = desc->checkpoints;
        gdsi_dprintf(GDS_DEBUG_INFO, "removing old version %zu\n",
                      tmp->version_number);
        SGLIB_DL_LIST_DELETE(struct gdsi_target_checkpoint,
                             desc->checkpoints, desc->checkpoints,
                             newer, older);
        if (desc->representation == GDSI_REPR_FLAT && tmp->ckpt_location_flags
                == GDS_CKPT_ON_MEMORY)
            free(tmp->buffer);
        free(tmp);
    }

    gdsi_dprintf(GDS_DEBUG_INFO, "before MPI_Win_free\n");
    GDSI_MPI_CALL(MPI_Win_free(&desc->win));
    gdsi_dprintf(GDS_DEBUG_INFO, "after MPI_Win_free\n");

#ifdef GDS_CONFIG_USE_LRDS
    if (desc->representation == GDSI_REPR_LRDS) {
        if (desc->lrds_dirty_buff != NULL)
            MPI_Win_free(&desc->lrds_dirty_win);
        lrds_region_free(&desc->lrds_region);
    }
#endif

    if (desc->flavor == GDS_FLAVOR_ALLOC)
        free(desc->buff);
    free(desc);

    ret = GDS_STATUS_OK;

out:
    gdsi_dprintf(GDS_DEBUG_INFO, "Sending TGTREP_GENERIC to rank=%d\n",
        client_rank);
    GDSI_MPI_CALL(
        MPI_Send(&ret, 1, MPI_INT, client_rank,
            GDS_TGTREP_GENERIC, gdsi_comm_world)
        );
    gdsi_dprintf(GDS_DEBUG_INFO, "exit\n");
}

static GDS_status_t flat_increment_version(struct gdsi_target_array_desc *desc)
{
    if (ckpt_callback(1 /*full*/, desc->buff, 0, desc->singlever_size,
            NULL, desc) < 0)
        return GDS_STATUS_NOMEM;

    return GDS_STATUS_OK;
}

#ifdef GDS_CONFIG_USE_LRDS
static GDS_status_t lrds_increment_version(struct gdsi_target_array_desc *desc)
{
    struct gdsi_target_checkpoint *ckpt;

    if (desc->lrds_dirty_buff)
        lrds_region_set_dirty_bitmap(desc->lrds_region, desc->lrds_dirty_buff,
            desc->lrds_dirty_buff_len);

    ckpt = calloc(sizeof(*ckpt), 1);
    if (ckpt == NULL)
        return GDS_STATUS_NOMEM;

    ckpt->ckpt_location_flags = GDS_CKPT_ON_MEMORY;
    ckpt->version_number = desc->current_version;
    ckpt->chunk_number = desc->chunk_number;
    ckpt->chunk_size = desc->singlever_size;

    if (lrds_region_version_commit(desc->lrds_region, &ckpt->lrds_version_id)
        != LRDS_SUCCESS) {
        free(ckpt);
        return GDS_STATUS_NOMEM;
    }
#ifdef GDS_CONFIG_COUNTER
    size_t size;
    int r = lrds_region_version_get_size(desc->lrds_region, ckpt->lrds_version_id, &size);
    if (r != LRDS_SUCCESS)
        gdsi_dprintf(GDS_DEBUG_ERR, "LRDS version get size failed: r=%d\n", r);
    else
        gdsi_dprintf(GDS_DEBUG_ERR, "LRDS version %d, size %zd\n", ckpt->lrds_version_id, size);
#endif

    if (desc->lrds_dirty_buff)
        memset(desc->lrds_dirty_buff, 0, desc->lrds_dirty_buff_len);

    SGLIB_DL_LIST_ADD(struct gdsi_target_checkpoint,
                      desc->checkpoints, ckpt, newer, older);
    desc->checkpoints = ckpt;

    return GDS_STATUS_OK;
}
#endif

static void target_alloc_new_version(void *priv, const void *param, int client_rank)
{
    const struct gdsi_target_incver *incver = param;
    struct gdsi_target_array_desc *desc;
    struct gdsi_target_alloc_result res;
    int rank, store_chunk_res = 0;

    res.start_index = 0;

    desc = find_array_desc(incver->array_id);
    if (desc == NULL) {
        res.status = GDS_STATUS_NOTEXIST;
        goto out;
    }

    /* already allocated chunk */
    if (desc->current_version >= incver->version) {
        res.start_index = incver->target_index;
        res.status = GDS_STATUS_OK;
        goto out;
    }

    MPI_Comm_rank(gdsi_comm_world, &rank);

    /* pca_target_rank == my_rank? */
    if (incver->pca_target_rank == incver->target_rank) { /* The pca target is itself */
        desc->chunk_number = incver->chunk_number;
        res.status = desc->increment_version(desc);
        if (res.status != GDS_STATUS_OK)
            goto out;
    } else {
        /* Send store_chunk request */
        struct gdsi_target_store_chunk sc;
        sc.array_id = incver->array_id;
        sc.version = incver->version;
        sc.store_version = incver->store_version;
        sc.pca_target_rank = incver->pca_target_rank;
        sc.pca_array_id = incver->pca_array_id;
        sc.chunk_number = incver->chunk_number;
        sc.chunk_size = incver->chunk_size;
        sc.owner_flag = incver->owner_flag;
        sc.target_index = incver->target_index;

        gdsi_dprintf(GDS_DEBUG_INFO, "Sending TGTREQ_STORE_CHUNK to %d\n",
            sc.pca_target_rank);
        gdsi_target_signal_thread(sc.pca_target_rank);
        //gdsi_dprintf(GDS_DEBUG_INFO, "Alive.\n");
        GDSI_MPI_CALL(MPI_Send(&sc, sizeof(sc),
        MPI_BYTE, sc.pca_target_rank, GDS_TGTREQ_STORE_CHUNK, gdsi_comm_world));
        //gdsi_dprintf(GDS_DEBUG_INFO, "Alive..\n");

        /* Receive store chunk result */
        GDSI_MPI_CALL(MPI_Recv(&store_chunk_res, sizeof(int), MPI_BYTE,
        sc.pca_target_rank, GDS_TGTREP_STORE_CHUNK, gdsi_comm_world,
        MPI_STATUS_IGNORE));
        //gdsi_dprintf(GDS_DEBUG_INFO, "Alive...\n");
        if (!store_chunk_res)
            goto out;
    }

    desc->current_version = incver->version;

    res.start_index = incver->target_index;
    res.status = GDS_STATUS_OK;

out:
    GDSI_MPI_CALL(MPI_Send(&res, sizeof(res), MPI_BYTE, client_rank,
            GDS_TGTREP_CREATE, gdsi_comm_world));

    //gdsi_dprintf(GDS_DEBUG_INFO, "Alive\n");
     if (res.status == GDS_STATUS_OK
         && desc->representation == GDSI_REPR_LOG) {
         gdsi_log_md_t cur_head = desc->log_cur_md_head / desc->log_bs;
         /* Send current metadata head position */
         GDSI_MPI_CALL(
             MPI_Send(&cur_head, 1, MPI_INT,
                 client_rank, GDS_TGTREP_CREATE, gdsi_comm_world)
             );
     }
    //gdsi_dprintf(GDS_DEBUG_INFO, "Alive\n");
}

static void target_store_chunk(void *priv, const void *param, int client_rank)
{
    const struct gdsi_target_store_chunk *sc = param;
    struct gdsi_target_array_desc *desc;
    desc = find_array_desc(sc->pca_array_id);
    if (desc == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "target_store_chunk: failed to find array descriptor.\n");
        return;
    }

    /* Follow callback function */
    struct gdsi_target_checkpoint *ckpt;
    const size_t alloc_size = sc->chunk_size;

    ckpt = malloc(sizeof(*ckpt));
    if(ckpt == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "target_store_chunk: failed to allocate target_checkpoint.\n");
        return;
    }

    ckpt->buffer = (char *)malloc(alloc_size);
    if(ckpt->buffer == NULL) {
        free(ckpt);
        gdsi_dprintf(GDS_DEBUG_ERR, "target_store_chunk: failed to allocate buffer");
        return;
    }

    ckpt->ckpt_location_flags = GDS_CKPT_ON_MEMORY;
    ckpt->version_number = sc->store_version;
    ckpt->chunk_number = sc->chunk_number;
    ckpt->chunk_size = sc->chunk_size;

    gdsi_dprintf(GDS_DEBUG_INFO, "target store chunk.\n");
    /* Copy the actual data */
    int mpi_type_size;
    MPI_Datatype mpi_type = MPI_BYTE;
    MPI_Type_size(mpi_type, &mpi_type_size);
    //MPI_Aint target_offset = 0; /* The offset is always 0 ? */
    MPI_Aint target_offset = desc->disp_unit * sc->target_index; /* The offset is always 0 ? */

    if (!sc->owner_flag) {
        /* nonowners should lock the window */
        MPI_Win_lock_all(MPI_MODE_NOCHECK, desc->win);
        gdsi_dprintf(GDS_DEBUG_INFO, "Nonowners lock the window\n");
    }

    MPI_Get(ckpt->buffer, alloc_size, mpi_type, client_rank, target_offset,
    alloc_size, mpi_type, desc->win);
    MPI_Win_flush(client_rank, desc->win);

    gdsi_dprintf(GDS_DEBUG_INFO, "target store chunk..\n");
    if (!sc->owner_flag) {
        /* nonowners should unlock the window */
        MPI_Win_unlock_all(desc->win);
    }

    gdsi_dprintf(GDS_DEBUG_INFO, "target store chunk...\n");
    SGLIB_DL_LIST_ADD(struct gdsi_target_checkpoint, desc->checkpoints, ckpt, newer, older);
    desc->checkpoints = ckpt;

    gdsi_dprintf(GDS_DEBUG_INFO, "store_chunk: version %zu chunk %d\n",
    sc->store_version, sc->chunk_number);

    int store_chunk_res = 1;
    GDSI_MPI_CALL(MPI_Send(&store_chunk_res, sizeof(int), MPI_BYTE, client_rank,
    GDS_TGTREP_STORE_CHUNK, gdsi_comm_world));
}

static void send_current_data(struct gdsi_target_array_desc *desc,
    int client_rank, size_t offset, size_t count, int tag)
{
    /* TODO: use actual data type instead of MPI_BYTE ?? */
    GDSI_MPI_CALL(MPI_Send(desc->buff + desc->disp_unit*offset,
                            desc->disp_unit*count,
                            MPI_BYTE, client_rank,
                            tag, gdsi_comm_world));
}

#define EXTRACT_BUFSIZE 4096

static void send_version_data(struct gdsi_target_array_desc *desc,
    int client_rank, struct gdsi_target_checkpoint *ckpt,
    size_t offset, size_t count, int tag,
    struct gdsi_target_fetch_priv *priv)
{
    size_t start = offset*desc->disp_unit, extract_size;
    const size_t data_size = count*desc->disp_unit;
    char *send_ptr;
    const size_t end_off = start + data_size;

    while (start < end_off) {
        extract_size = end_off - start;
        if (extract_size > GDSI_TARGET_BUFSIZE)
            extract_size = GDSI_TARGET_BUFSIZE;

        gdsi_dprintf(GDS_DEBUG_INFO,
            "sending version to %d: "
            "start=%zu end_off=%zu extract_size=%zu tag=%d\n",
            client_rank, start, end_off, extract_size, tag);

#ifdef GDS_CONFIG_USE_LRDS
        if (desc->representation == GDSI_REPR_LRDS) {
            int r;
            r = lrds_region_version_rollback(desc->lrds_region,
                ckpt->lrds_version_id, start, extract_size, priv->send_buf);
            if (r != LRDS_SUCCESS)
                gdsi_dprintf(GDS_DEBUG_ERR, "lrds_region_version_rollback"
                    " failed: r=%d\n", r);
            send_ptr = priv->send_buf;
        } else
#endif
            send_ptr = ckpt->buffer + start;

        start += extract_size;

        /* TODO: use actual data type instead of MPI_BYTE ?? */
        GDSI_MPI_CALL(MPI_Send(send_ptr, extract_size,
                                MPI_BYTE, client_rank,
                                tag, gdsi_comm_world));
    }

}

static struct gdsi_target_checkpoint
*find_ckpt(struct  gdsi_target_array_desc *desc, size_t version, int
chunk_number)
{
    struct gdsi_target_checkpoint *ckpt;

    for (ckpt = desc->checkpoints; ckpt != NULL; ckpt = ckpt->older) {
        if (ckpt->version_number == version && ckpt->chunk_number ==
        chunk_number)
            break;
    }
    return ckpt;
}

void gdsi_construct_filename(int array_id, int version_number, int chunk_number, char *filename)
{
    size_t l1 = sizeof(array_id); /* length of array id string */
    size_t l2 = sizeof(version_number); /* length of version number string */
    size_t l3 = sizeof(chunk_number); /* length of chunk number string */

    char *array_id_str = (char *)malloc(l1);
    snprintf(array_id_str, l1, "%d", array_id);

    char *version_number_str = (char *)malloc(l2);
    snprintf(version_number_str, l2, "%d", version_number);

    char *chunk_number_str = (char *)malloc(l3);
    snprintf(chunk_number_str, l3, "%d", chunk_number);

    strcpy(filename, "array");
    strcat(filename, array_id_str);
    strcat(filename, "_v");
    strcat(filename, version_number_str);
    strcat(filename, "_chunk");
    strcat(filename, chunk_number_str);
}

int gdsi_write_chunk_to_file(void *buf, char *filename, size_t size)
{
    FILE *fs = fopen(filename, "w");
    size_t n;

    if (fs == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "gdsi_write_chunk_to_file: failed to open the file %s.\n", filename);
        return 1;
    }

    n = fwrite(buf, 1, size, fs);
    if (n != size) {
        gdsi_dprintf(GDS_DEBUG_ERR, "gdsi_write_chunk_to_file: write %ld bytes to file instead of %ld bytes\n", n, size);
        fclose(fs);
        return 1;
    }
    int status = fclose(fs);
    if(status)
        gdsi_dprintf(GDS_DEBUG_ERR, "gdsi_write_chunk_to_file: close file %s\n", filename);
    return 0;
}

int gdsi_read_chunk_from_file(void *buf, char *filename, size_t offset, size_t size)
{
    FILE *fs = fopen(filename, "r");
    size_t n;

    if (fs == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR, "gdsi_read_chunk_from_file: failed to open the file %s.\n", filename);
        return 1;
    }

    fseek(fs, offset, SEEK_SET);
    n = fread(buf, 1, size, fs);
    if (n != size) {
        gdsi_dprintf(GDS_DEBUG_ERR, "gdsi_read_chunk_from_file: read %ld bytes to file instead of %ld bytes\n", n, size);
        fclose(fs);
        return 1;
    }

    fclose(fs);
    return 0;
}

#ifdef GDS_CONFIG_USE_SCR
int gdsi_get_base_path(char *dir_name)
{
    memset(dir_name, '\0', SCR_MAX_FILENAME);
    strcpy(dir_name, SCR_CACHE_BASE);
    strcat(dir_name, "/");

    char *username;
    if ((username = getenv("SCR_USER_NAME")) != NULL)
        gdsi_dprintf(GDS_DEBUG_INFO, "SCR_USER_NAME is %s.\n", username);
    else if ((username = getenv("USER")) == NULL)
        gdsi_dprintf(GDS_DEBUG_ERR, "Neither SCR_USER_NAME or USER are set.\n");

    strcat(dir_name, username);
    strcat(dir_name, "/scr.");
    strcat(dir_name, SCR_JOB_ID);
    return 0;
}

int gdsi_get_pfs_path(char *pfs_name)
{
    memset(pfs_name, '\0', SCR_MAX_FILENAME);
    strncpy(pfs_name, SCR_PREFIX, SCR_MAX_FILENAME);
    return 0;
}

int gdsi_getfile(char *dir_name, const char *name, char *file)
{
    struct dirent *ent;
    DIR *dir;
    int res = 1;

    if ((dir = opendir(dir_name)) != NULL) {
        while ((ent = readdir (dir)) != NULL) {
            gdsi_dprintf(GDS_DEBUG_INFO, "file %s\n", ent->d_name);
            if (!strcmp(ent->d_name, name)) {
                strcat(file, dir_name);
                strcat(file, "/");
                strcat(file, ent->d_name);
                res = 0;
                break;
            }
            if (ent->d_type == DT_DIR && strcmp(ent->d_name, ".") && strcmp(ent->d_name, "..")) {
                char path[SCR_MAX_FILENAME];
                memset(path, '\0', SCR_MAX_FILENAME);
                strcat(path, dir_name);
                strcat(path, "/");
                strcat(path, ent->d_name);
                res = gdsi_getfile(path, name, file);
                if (!res) break;
            }
        }
        closedir(dir);
    } else {
        /* could not open directory */
        gdsi_dprintf(GDS_DEBUG_ERR, "Fail to open directory %s.\n", dir_name);
        return 1;
    }
    return res;
}
#endif

bool gdsi_check_pfs_available(int unique_array_id, int version_number, int chunk_number)
{
#ifdef GDS_CONFIG_USE_SCR
    char name[SCR_MAX_FILENAME];
    gdsi_construct_filename(unique_array_id, version_number, chunk_number, name);

    char file[SCR_MAX_FILENAME];
    memset(file, '\0', SCR_MAX_FILENAME);

    /* Get file from PFS */
    char pfs_name[SCR_MAX_FILENAME];
    gdsi_get_pfs_path(pfs_name);

    int status = gdsi_getfile(pfs_name, name, file);
    if (status) {
        gdsi_dprintf(GDS_DEBUG_ERR, "flat_fetch: Failed to find file %s.\n", name);
        return false;
    }
    return true;
#else
    return false;
#endif
}

static void target_flush_scr(void *priv, const void *param, int client_rank)
{
#ifdef GDS_CONFIG_USE_SCR

    const struct gdsi_target_flush_scr *fl = param;
    struct gdsi_target_array_desc *desc;

    desc = find_array_desc(fl->array_id);
    if (desc == NULL) {
        return;
    }

    struct gdsi_target_checkpoint *ckpt;

    ckpt = find_ckpt(desc, fl->version, fl->chunk_number);
    if (ckpt == NULL) {
        /* Not found */
        gdsi_dprintf(GDS_DEBUG_ERR,
                      "specified version not found: version=%zu\n",
                      fl->version);
        return;
    }
    char name[SCR_MAX_FILENAME];
    char file[SCR_MAX_FILENAME];
    gdsi_construct_filename(fl->unique_array_id, fl->version, fl->chunk_number, name);

    SCR_Route_file(name, file);
    gdsi_dprintf(GDS_DEBUG_INFO, "file name is %s\n", file);

    int status = gdsi_write_chunk_to_file(ckpt->buffer, file, desc->singlever_size);
    gdsi_dprintf(GDS_DEBUG_INFO, "target_flush_scr: status %d, 1=fail\n", status);

    if (status) {
        gdsi_dprintf(GDS_DEBUG_ERR, "target_flush_scr: Failed to write chunk to file %s.\n", file);
    }
    else {
        ckpt->ckpt_location_flags = GDS_CKPT_ON_DISK;
        GDSI_MPI_CALL(MPI_Send(&status, sizeof(int), MPI_BYTE, client_rank,
        GDS_TGTREP_FLUSH_SCR, gdsi_comm_world));
        gdsi_dprintf(GDS_DEBUG_INFO, "target_flush_scr: Write chunk to file %s.\n", file);
        free(ckpt->buffer);
    }

#endif
}

static void flat_fetch(struct  gdsi_target_array_desc *desc,
    const struct gdsi_target_fetch *ft, int client_rank,
    struct gdsi_target_fetch_priv *priv)
{
    struct gdsi_target_checkpoint *ckpt = NULL;
    GDS_status_t ret = GDS_STATUS_OK;

    /* Find the chunk by version number and also chunk number */
    if (ft->version == desc->current_version)
        goto out;

    ckpt = find_ckpt(desc, ft->version, ft->chunk_number);
    /* In proc failure case, the target may not hold the chunk but can still
     * find the chunk on PFS */
    if (ckpt == NULL && ft->ckpt_location_flags != GDS_CKPT_ON_DISK) {
        /* Not found */
        gdsi_dprintf(GDS_DEBUG_ERR,
                      "specified ckpt not found: version=%zu chunk=%d\n",
                      ft->version, ft->chunk_number);
        ret = GDS_STATUS_NOTEXIST;
        goto out;
    }

    if ((ft->offset + ft->count)*desc->disp_unit > ckpt->chunk_size) {
        gdsi_dprintf(GDS_DEBUG_ERR,
                      "request out of range: offset=%zu count=%zu "
                      "disp_unit=%d chunk_size=%zu\n",
                      ft->offset, ft->count, desc->disp_unit,
                      ckpt->chunk_size);
        ret = GDS_STATUS_RANGE;
        goto out;
    }

out:
    GDSI_MPI_CALL(MPI_Send(&ret, 1, MPI_INT, client_rank, ft->tag_status,
            gdsi_comm_world));

    if (ret != GDS_STATUS_OK)
        return;

    /* In proc failure case, the target may not hold the chunk but can still
     * find the chunk on PFS */
    if (ft->ckpt_location_flags == GDS_CKPT_ON_DISK ||
            ckpt->ckpt_location_flags == GDS_CKPT_ON_DISK) {
        /* Check where is chunk: in memory or in disk */
        /* If in disk, partial rematerilization */
#ifdef GDS_CONFIG_USE_SCR 
        gdsi_dprintf(GDS_DEBUG_INFO, "SCR PFS file should be on disk: ver=%zu chunkno=%d\n", ft->version, ft->chunk_number);
        char name[SCR_MAX_FILENAME];
        gdsi_construct_filename(ft->unique_array_id, ft->version,
            ft->chunk_number, name);

        /* Construct the path dir_name to the file */
        char dir_name[SCR_MAX_FILENAME];
        gdsi_get_base_path(dir_name);
        char file[SCR_MAX_FILENAME];
        memset(file, '\0', SCR_MAX_FILENAME);

        int status = gdsi_getfile(dir_name, name, file);
        if (status) {
            gdsi_dprintf(GDS_DEBUG_INFO, "flat_fetch: Failed to find file %s.\n", name);

            /* Get file from PFS */
            char pfs_name[SCR_MAX_FILENAME];
            gdsi_get_pfs_path(pfs_name);
            status = gdsi_getfile(pfs_name, name, file);
            if (status) {
                gdsi_dprintf(GDS_DEBUG_ERR, "flat_fetch: Failed to find file %s.\n", name);
                return;
            }
        }

        gdsi_dprintf(GDS_DEBUG_INFO, "flat_fetch: Find file %s.\n", file);
        size_t offset = desc->disp_unit * ft->offset;
        size_t size = desc->disp_unit * ft->count;

        if (offset + size > desc->singlever_size) {
            gdsi_dprintf(GDS_DEBUG_ERR, "flat_fetch: request data out of range.\n");
            return;
        }

        void *buffer = malloc(size);

        status = gdsi_read_chunk_from_file(buffer, file, offset, size);

        if (status) {
            gdsi_dprintf(GDS_DEBUG_ERR, "flat_fetch: Failed to read file %s, name %s\n", file, name);
            return;
        }

        GDSI_MPI_CALL(MPI_Send(buffer, size, MPI_BYTE, client_rank,
                    ft->tag_data, gdsi_comm_world));

        gdsi_dprintf(GDS_DEBUG_INFO, "flat_fetch: Partial rematerialization.\n");
        free(buffer);
        return;
#endif
    }

    if (ckpt == NULL) {
        send_current_data(desc, client_rank, ft->offset, ft->count,
            ft->tag_data);
        return;
    } else {
        /* Data in memory */
        gdsi_dprintf(GDS_DEBUG_INFO, "flat_fetch: Data in memory\n");
        send_version_data(desc, client_rank, ckpt, ft->offset, ft->count, ft->tag_data, priv);
    }
}

static void log_fetch(struct  gdsi_target_array_desc *desc,
    const struct gdsi_target_fetch *ft, int client_rank,
    struct gdsi_target_fetch_priv *priv)
{
    GDS_status_t ret = GDS_STATUS_OK;
    size_t start_offset = desc->disp_unit * ft->offset;
    size_t total_len = desc->disp_unit * ft->count;
    size_t n_blocks = log_required_blocks(start_offset, total_len,
        desc->log_user_bs);
    struct gdsi_target_checkpoint *ckpt = NULL;

    if (ft->version == desc->current_version)
        goto out;

    ckpt = find_ckpt(desc, ft->version, ft->chunk_number);
    if (ckpt == NULL) {
        /* Not found */
        gdsi_dprintf(GDS_DEBUG_ERR,
                      "specified version not found: version=%zu\n",
                      ft->version);
        ret = GDS_STATUS_NOTEXIST;
    }

out:

    GDSI_MPI_CALL(MPI_Send(&ret, 1, MPI_INT, client_rank,
            ft->tag_status, gdsi_comm_world));

    if (ret != GDS_STATUS_OK)
        return;

    if (n_blocks == 1 && ckpt == NULL)
        log_send_data(desc, ft->offset, ft->count, client_rank, ft->tag_data);
    else {
        size_t sent = 0;

        while (sent < total_len) {
            size_t len = GDSI_TARGET_BUFSIZE;
            if (len > total_len - sent)
                len = total_len - sent;

            if (ckpt == NULL)
                log_read_data(desc, start_offset + sent, len, priv->send_buf);
            else
                log_read_data_at(desc, ckpt->buffer, start_offset + sent,
                    len, priv->send_buf);
            MPI_Send(priv->send_buf, len, MPI_BYTE, client_rank, ft->tag_data,
                gdsi_comm_world);
            sent += len;
        }
    }
}

static void target_fetch(void *privp, const void *param, int client_rank)
{
    const struct gdsi_target_fetch *ft = param;
    struct gdsi_target_fetch_priv *priv = privp;
    struct gdsi_target_array_desc *desc;
    GDS_status_t ret = GDS_STATUS_OK;

    desc = find_array_desc(ft->array_id);
    if (desc == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR,
            "array desc not found: id=%d\n", ft->array_id);
        ret = GDS_STATUS_NOTEXIST;
        goto out;
    }
    
    gdsi_dprintf(GDS_DEBUG_INFO, "Received fetch req from rank %d: "
        "array_id=%d, version=%zu chunk_number=%d offset=%zu count=%zu, "
        "tag_status=%d, tag_data=%d\n",
        client_rank, ft->array_id, ft->version, ft->chunk_number, ft->offset, ft->count,
        ft->tag_status, ft->tag_data);

    desc->fetch(desc, ft, client_rank, priv);

    return;

out:
    GDSI_MPI_CALL(MPI_Send(&ret, 1, MPI_INT, client_rank,
            ft->tag_status, gdsi_comm_world));
}

struct gdsi_target_put_priv {
    char *recv_buf;
};

static void flat_put(struct  gdsi_target_array_desc *desc,
    const struct gdsi_target_put *lw, int client_rank,
    struct gdsi_target_put_priv *priv)
{
    size_t start_offset, total_len, rcvd = 0;
    MPI_Status status;
    int r;
    bool indirect = false; /* Do we need indirect receiving? */
#ifdef GDS_CONFIG_USE_LRDS
    bool lrds_dirty_user = false; /* Using LRDS user-level change tracking */

    if (desc->representation == GDSI_REPR_LRDS) {
        switch (desc->lrds_dirty_tracking) {
        case LRDS_DIRTY_TRACKING_USER:
            lrds_dirty_user = true;
            break;
        case LRDS_DIRTY_TRACKING_KERNEL:
            /* Kernel-level change tracking requires indirect
               receiving, as applying MPI_Recv to the LRDS-registered
               region may cause EFAULT. */
            indirect = true;
            break;
        default:
            break;
        }
    }
#endif

    start_offset = desc->disp_unit * lw->offset;
    total_len = desc->disp_unit * lw->count;

    if (lw->acc_mode) {
        while (rcvd < total_len) {
            /* TODO: length check */
            GDSI_MPI_CALL(MPI_Recv(priv->recv_buf, GDSI_TARGET_BUFSIZE,
                    MPI_BYTE, client_rank, lw->tag_data,
                    gdsi_comm_world, &status));
            MPI_Get_count(&status, MPI_BYTE, &r);

            gdsi_dprintf(GDS_DEBUG_INFO, "Received %d bytes from rank %d\n",
                r, client_rank);

            MPI_Reduce_local(priv->recv_buf, desc->buff + start_offset + rcvd,
                lw->count, lw->acc_type, lw->acc_op);

            rcvd += r;
        }
        return;
    }

    while (rcvd < total_len) {
        if (indirect) {
            size_t rcv_len = GDSI_TARGET_BUFSIZE;
            if (rcv_len > total_len - rcvd)
                rcv_len = total_len - rcvd;
            MPI_Recv(priv->recv_buf, rcv_len,
                MPI_BYTE, client_rank, lw->tag_data,
                gdsi_comm_world, &status);
        } else {
            MPI_Recv(desc->buff + start_offset + rcvd,
                total_len - rcvd, MPI_BYTE, client_rank, lw->tag_data,
                gdsi_comm_world, &status);
        }
        MPI_Get_count(&status, MPI_BYTE, &r);
        if (indirect)
            memcpy(desc->buff + start_offset + rcvd, priv->recv_buf, r);
#ifdef GDS_CONFIG_USE_LRDS
        if (lrds_dirty_user)
            lrds_region_set_dirty(desc->lrds_region, start_offset + rcvd, r);
#endif
        rcvd += r;
    }
}

static void log_put(struct  gdsi_target_array_desc *desc,
    const struct gdsi_target_put *lw, int client_rank,
    struct gdsi_target_put_priv *priv)
{
    size_t n_blocks, start_offset, total_len, rcvd = 0;
    MPI_Status status;

    start_offset = desc->disp_unit * lw->offset;
    total_len = desc->disp_unit * lw->count;
    n_blocks = log_required_blocks(start_offset, total_len, desc->log_user_bs);

    /* TODO: check buffer overrun */
    log_alloc_data_buffer(desc, start_offset, total_len, lw->acc_mode);

    if (n_blocks == 1 && !lw->acc_mode)
        log_recv_data(desc, lw->offset, lw->count, client_rank, lw->tag_data);
    else {
        while (rcvd < total_len) {
            int r;
            /* TODO: length check */
            GDSI_MPI_CALL(MPI_Recv(priv->recv_buf, GDSI_TARGET_BUFSIZE,
                    MPI_BYTE, client_rank, lw->tag_data,
                    gdsi_comm_world, &status));
            MPI_Get_count(&status, MPI_BYTE, &r);

            gdsi_dprintf(GDS_DEBUG_INFO, "Received %d bytes from rank %d\n",
                r, client_rank);

            if (lw->acc_mode) {
                size_t elm_off = lw->offset + rcvd / desc->disp_unit;
                log_acc_data(desc, elm_off, priv->recv_buf, lw->count,
                    lw->acc_type, lw->acc_op);
            } else
                log_write_data(desc, start_offset + rcvd, r, priv->recv_buf);

            rcvd += r;
        }
    }
}

static void target_put(void *privp, const void *param, int client_rank)
{
    const struct gdsi_target_put *lw = param;
    struct gdsi_target_array_desc *desc;
    GDS_status_t ret;
    struct gdsi_target_put_priv *priv = privp;
    size_t start_offset;
    size_t total_len;

    desc = find_array_desc(lw->array_id);
    if (desc == NULL)
        gdsi_panic("array desc not found: id=%d\n", lw->array_id);

    start_offset = desc->disp_unit * lw->offset;
    total_len = desc->disp_unit * lw->count;

    if (start_offset + total_len > desc->singlever_size) {
        gdsi_dprintf(GDS_DEBUG_ERR,
                      "request out of range: offset=%zu count=%zu "
                      "disp_unit=%d singlever_size=%zu\n",
                      lw->offset, lw->count, desc->disp_unit,
                      desc->singlever_size);
        ret = GDS_STATUS_RANGE;
        goto fail_drain;
    }

    desc->put(desc, lw, client_rank, priv);

    ret = GDS_STATUS_OK;

out:
    GDSI_MPI_CALL(MPI_Send(&ret, 1, MPI_INT, client_rank,
        lw->tag_status, gdsi_comm_world));

    return;

fail_drain:
    /* Drain sent data */
    {
        size_t i;
        size_t n_blocks = (total_len + GDSI_TARGET_BUFSIZE - 1)
            / GDSI_TARGET_BUFSIZE;

        for (i = 0; i < n_blocks; i++)
            GDSI_MPI_CALL(MPI_Recv(priv->recv_buf, GDSI_TARGET_BUFSIZE,
                    MPI_BYTE, client_rank, lw->tag_data,
                    gdsi_comm_world, MPI_STATUS_IGNORE));
    }

    goto out;
}

static void target_log_alloc(void *privp, const void *param, int client_rank)
{
    const struct gdsi_target_put *lw = param;
    struct gdsi_target_array_desc *desc;
    size_t start_offset, total_len, n_blocks;
    GDS_status_t ret;
    gdsi_log_md_t *md;

    desc = find_array_desc(lw->array_id);
    if (desc == NULL)
        gdsi_panic("array desc not found: id=%d\n", lw->array_id);

    start_offset = desc->disp_unit * lw->offset;
    total_len = desc->disp_unit * lw->count;

    if (start_offset + total_len > desc->singlever_size) {
        gdsi_dprintf(GDS_DEBUG_ERR,
                      "request out of range: offset=%zu count=%zu "
                      "disp_unit=%d singlever_size=%zu\n",
                      lw->offset, lw->count, desc->disp_unit,
                      desc->singlever_size);
        ret = GDS_STATUS_RANGE;
        goto out;
    }

    /* TODO: check buffer overrun */
    log_alloc_data_buffer(desc, start_offset, total_len, false);

    ret = GDS_STATUS_OK;
out:
    GDSI_MPI_CALL(MPI_Send(&ret, 1, MPI_INT, client_rank,
        lw->tag_status, gdsi_comm_world));
    if (ret != GDS_STATUS_OK)
        return;

    /* Send metadata */
    md = log_md_ptr(desc, start_offset);
    n_blocks = log_required_blocks(start_offset, total_len, desc->log_user_bs);
    GDSI_MPI_CALL(MPI_Send(md, n_blocks, MPI_UINT32_T, client_rank,
        lw->tag_data, gdsi_comm_world));
}

static void target_log_flush(void *privp, const void *param, int client_rank)
{
    const struct gdsi_target_log_flush *lf = param;
    gdsi_log_md_t blocks[GDS_LOG_FLUSH_MAXENTS];
    int n;
    MPI_Status status;
    struct gdsi_target_array_desc *desc;

    MPI_Recv(blocks, GDS_LOG_FLUSH_MAXENTS, MPI_UINT32_T, client_rank,
        lf->tag, gdsi_comm_world, &status);

    desc = find_array_desc(lf->array_id);
    if (desc == NULL) {
        gdsi_dprintf(GDS_DEBUG_ERR,
            "array desc not found: id=%d\n", lf->array_id);
        return;
    }

    MPI_Get_count(&status, MPI_UINT32_T, &n);

    log_flush(desc, blocks, n);
}

static void target_vaddr_to_gds(void *priv, const void *param,
    int client_rank) {
    const struct gdsi_target_vaddr *vaddr = param;
    int n_hits = 0; // number of GDSs found
    size_t err_len_remaining = vaddr->len;
    size_t err_current_idx = (size_t)vaddr->vaddr;
    /* for all registered arrays, look at start location and length and build
       a list */
    struct gdsi_target_array_desc *desc = array_descs;
    struct gdsi_id_and_offset *hits = NULL;
    struct gdsi_id_and_offset *current_hit, *next_hit;
    pthread_mutex_lock(&array_descs_lock);

    // TODO it's pretty silly to loop over ever gds for every byte of error--
    // to be optimized when we have time
    int found = 0;
    while (err_len_remaining > 0) {
        SGLIB_DL_LIST_GET_FIRST(struct gdsi_target_array_desc,
                                array_descs, prev, next, desc);
        for (; desc != NULL; desc = desc->next) {
            size_t buf_start = (size_t)desc->buff;
            size_t buf_len = desc->singlever_size;
            size_t buf_end = buf_start + buf_len - 1;
            if (buf_start <= err_current_idx && err_current_idx <= buf_end) {
                ++n_hits;
                size_t this_len = GDSI_MIN(buf_end - err_current_idx + 1,
                    err_len_remaining);
                current_hit = malloc(sizeof(*current_hit));
                current_hit->array_id = desc->array_id;
                current_hit->offset = err_current_idx - buf_start;
                current_hit->len = this_len;
                SGLIB_LIST_ADD(struct gdsi_id_and_offset, hits, current_hit, next);
                err_len_remaining -= this_len;
                err_current_idx += this_len;
                if (err_len_remaining <= 0) {
                    found = 1;
                    break;
                }
            }
        }
        if (found) {
            found = 0;
        } else {
            ++err_current_idx;
            --err_len_remaining;
        }
    }

    /* serialize our list for MPI */
    struct gdsi_id_and_offset hits_serial[n_hits];
    int i = 0;
    current_hit = hits;
    while (current_hit != NULL) {
        memcpy(hits_serial + i, current_hit, sizeof(*current_hit));
        ++i;
        next_hit = current_hit->next;
        free(current_hit);
        current_hit = next_hit;
    }
    pthread_mutex_unlock(&array_descs_lock);

    GDSI_MPI_CALL(MPI_Send(&n_hits, 1, MPI_INT, client_rank,
        GDS_TGTREP_VADDR_TO_GDS_CT, gdsi_comm_world));
    GDSI_MPI_CALL(MPI_Send(hits_serial, sizeof(hits_serial), MPI_BYTE,
        client_rank, GDS_TGTREP_VADDR_TO_GDS_RES, gdsi_comm_world));
}

static void target_reconstruct(void *privp, const void *param, int client_rank)
{
    const struct gdsi_target_reconstruct *rc = param;
    struct gdsi_target_array_desc *desc;
    GDS_status_t ret = GDS_STATUS_OK;

    desc = find_array_desc(rc->array_id);
    if (desc == NULL)
        gdsi_panic("array desc not found: id=%d, client_rank=%d\n", rc->array_id, client_rank);

    MPI_Win_free(&desc->win);

    if (rc->additional_count > 0) {
        char *tmp_buff;
        size_t new_sv_size = desc->singlever_size + desc->disp_unit * rc->additional_count;
        gdsi_dprintf(GDS_DEBUG_INFO,
            "allocating new buffer: additional_count=%zu\n",
            rc->additional_count);
        MPI_Alloc_mem(new_sv_size, MPI_INFO_NULL, &tmp_buff);
        gdsi_assert(tmp_buff);
        memcpy(tmp_buff, desc->buff, desc->singlever_size);
        gdsi_dprintf(GDS_DEBUG_INFO, "freeing old buffer\n");
        free(desc->buff);
        desc->buff = tmp_buff;
        desc->singlever_size = new_sv_size;
    }

    MPI_Win_create(desc->buff, desc->singlever_size, desc->disp_unit,
                   MPI_INFO_NULL, gdsi_comm_world, &desc->win);

    /* Return result to the caller */
    MPI_Send(&ret, 1, MPI_INT, client_rank,
        GDS_TGTREP_GENERIC, gdsi_comm_world);
}

struct gdsi_target_recv_param {
    void * const buf;
    const int count;
    const MPI_Datatype datatype;
    void *priv;
    void (*handler)(void *priv, const void *param, int client_rank);
};

void gdsi_target_signal_thread(int target_rank)
{
    static int a = 1;
    if (gdsi_wait_scheme == GDS_WAIT_SCHEME_DOORBELL) {
        MPI_Accumulate(&a, 1, MPI_INT, target_rank, 0, 1, MPI_INT, MPI_SUM,
            target_doorbell_win);
        MPI_Win_flush(target_rank, target_doorbell_win);
    }
}

static void do_waitany(int count, MPI_Request reqs[], int *idx, MPI_Status *status)
{
    int flag, prev;

    switch (gdsi_wait_scheme) {
    default:
        gdsi_dprintf(GDS_DEBUG_ERR, "Unknown wait scheme provided: %d:"
            " falling back to default scheme\n", gdsi_wait_scheme);
    case GDS_WAIT_SCHEME_DEFAULT:
        MPI_Waitany(count, reqs, idx, status);
        break;

    case GDS_WAIT_SCHEME_SLEEP:
        for (;;) {
            /*
              This loop essentially does the same thing as MPI_Waitany(),
              but tries not to disturb main thread by a busy loop inside
              MPI_Waitany().
              (yes... this hack totally depends on MPI implementation...
              it works well with MVAPICH2-2.0b+IB, but not clear for other
              env...)
            */
            MPI_Testany(count, reqs, idx, &flag, status);
            if (flag)
                break;
            usleep(gdsi_wait_sleep_duration);
        }
        break;

    case GDS_WAIT_SCHEME_SPIN:
        for (;;) {
            int i;
            MPI_Testany(count, reqs, idx, &flag, status);
            if (flag)
                break;
            for (i = 0; i < gdsi_wait_spin_count; i++)
                __asm__ __volatile__("");
        }
        break;

    case GDS_WAIT_SCHEME_DOORBELL:
        prev = target_doorbell_prev;
        MPI_Testany(count, reqs, idx, &flag, status);
        if (flag)
            break;
        while ((flag = target_doorbell_area) == prev)
            __asm__ __volatile__("");
        target_doorbell_prev = flag;
        MPI_Waitany(count, reqs, idx, status);
        break;
    }
}

/* Post Irecvs for all the target request types */
static void post_irecvs(const struct gdsi_target_param *param,
    const struct gdsi_target_recv_param *rparams,
    MPI_Request requests[])
{
    int i;

    for (i = 0; i < param->n_tags; i++) {
        int tag = param->tags[i];
        const struct gdsi_target_recv_param *rp = rparams + tag;

        gdsi_assert(tag < GDS_TGTREQ_LAST);

        MPI_Irecv(rp->buf, rp->count, rp->datatype, MPI_ANY_SOURCE,
            tag, gdsi_comm_world, requests + i);
    }
}

static void cancel_irecvs(int n, MPI_Request requests[])
{
    int i;
    for (i = 0; i < n; i++) {
        if (requests[i] != MPI_REQUEST_NULL) {
            MPI_Cancel(requests + i);
            MPI_Request_free(requests + i);
        }
    }
}

static void *target_thread_entry(void *arg)
{
    struct gdsi_target_param *param = (struct gdsi_target_param *) arg;
    MPI_Request requests[GDS_TGTREQ_LAST];
    struct gdsi_target_create create_param;
    struct gdsi_target_incver incver;
    struct gdsi_target_store_chunk store_chunk;
    struct gdsi_target_flush_scr flush_scr;
    struct gdsi_target_fetch fetch;
    struct gdsi_target_fetch_priv fetch_priv = { .send_buf = NULL };
    struct gdsi_target_put put;
    struct gdsi_target_put_priv put_priv = { .recv_buf = NULL };
    struct gdsi_target_log_flush flush;
    struct gdsi_target_vaddr vaddr;
    struct gdsi_target_reconstruct reconstruct;

    int dummy, simfail_rank;
    int array_id;

    struct gdsi_target_recv_param rparams[] = {
        /* NOTE: order MATTERS! DO NOT SHUFFLE! */

        /* GDS_TGTREQ_CREATE */
        { .buf      = &create_param,
          .count    = sizeof(create_param),
          .datatype = MPI_BYTE,
          .handler  = target_create, },

        /* GDS_TGTREQ_INCVER */
        { .buf      = &incver,
          .count    = sizeof(incver),
          .datatype = MPI_BYTE,
          .handler  = target_alloc_new_version, },

        /* GDS_TGTREQ_STORE_CHUNK */
        { .buf      = &store_chunk,
          .count    = sizeof(store_chunk),
          .datatype = MPI_BYTE,
          .handler  = target_store_chunk, },

        /* GDS_TGTREQ_FLUSH_SCR */
        { .buf      = &flush_scr,
          .count    = sizeof(flush_scr),
          .datatype = MPI_BYTE,
          .handler  = target_flush_scr, },

        /* GDS_TGTREQ_FETCH */
        { .buf      = &fetch,
          .count    = sizeof(fetch),
          .datatype = MPI_BYTE,
          .priv     = &fetch_priv,
          .handler  = target_fetch, },

        /* GDS_TGTREQ_PUT */
        { .buf      = &put,
          .count    = sizeof(put),
          .datatype = MPI_BYTE,
          .priv     = &put_priv,
          .handler  = target_put, },

        /* GDS_TGTREQ_LOG_ALLOC */
        { .buf      = &put,
          .count    = sizeof(put),
          .datatype = MPI_BYTE,
          .handler  = target_log_alloc, },

        /* GDS_TGTREQ_LOG_FLUSH */
        { .buf      = &flush,
          .count    = sizeof(flush),
          .datatype = MPI_BYTE,
          .handler  = target_log_flush, },

        /* GDS_TGTREQ_VADDR_TO_GDS */
        { .buf      = &vaddr,
          .count    = sizeof(vaddr),
          .datatype = MPI_BYTE,
          .handler  = target_vaddr_to_gds, },

        /* GDS_TGTREQ_SIM_FAILURE */
        { .buf      = &simfail_rank,
          .count    = 1,
          .datatype = MPI_INT,
          /* no handler */ },

        /* GDS_TGTREQ_RECONSTRUCT */
        { .buf      = &reconstruct,
          .count    = sizeof(reconstruct),
          .datatype = MPI_BYTE,
          .handler  = target_reconstruct, },

        /* GDS_TGTREQ_FREE */
        { .buf      = &array_id,
          .count    = 1,
          .datatype = MPI_INT,
          .handler  = target_free, },

        /* GDS_TGTREQ_EXIT */
        { .buf      = &dummy,
          .count    = 1,
          .datatype = MPI_INT,
          /* No handler*/ },
    };

    gdsi_assert(param->n_tags <= GDS_TGTREQ_LAST);

    fetch_priv.send_buf = calloc(GDSI_TARGET_BUFSIZE, 1);
    if (fetch_priv.send_buf == NULL)
        gdsi_panic("Could not allocate send buffer for fetch operation\n");

    put_priv.recv_buf = calloc(GDSI_TARGET_BUFSIZE, 1);
    if (put_priv.recv_buf == NULL)
        gdsi_panic("Could not allocate receive buffer for log-structured array\n");

    post_irecvs(param, rparams, requests);

    /* Main message loop */
    for (;;) {
        int ri;
        MPI_Status status;

        do_waitany(param->n_tags, requests, &ri, &status);

        if (status.MPI_TAG < 0 || status.MPI_TAG >= GDS_TGTREQ_LAST) {
            int myrank;
            MPI_Comm_rank(gdsi_comm_world, &myrank);
            gdsi_dprintf(GDS_DEBUG_ERR, "My rank in gdsi_comm_world: %d\n",
                myrank);
            gdsi_panic("Unknown tag received: %d\n", status.MPI_TAG);
        }

        if (status.MPI_TAG == GDS_TGTREQ_SIM_FAILURE) {
            int myrank;
            MPI_Group tmp_group_world;
            MPI_Comm tmp_comm_world;

            MPI_Comm_rank(gdsi_comm_world, &myrank);

            gdsi_dprintf(GDS_DEBUG_INFO, "Simulating failure request received.\n");

            cancel_irecvs(param->n_tags, requests);
            gdsi_dprintf(GDS_DEBUG_INFO, "Before barrier.\n");
            MPI_Barrier(gdsi_comm_world);

            /* Replace comm_world */
            MPI_Group_excl(gdsi_group_world, 1, &simfail_rank, &tmp_group_world);
            MPI_Comm_create(gdsi_comm_world, tmp_group_world, &tmp_comm_world);
            MPI_Group_free(&gdsi_group_world);
            MPI_Group_free(&tmp_group_world);
            MPI_Comm_free(&gdsi_comm_world);
            gdsi_comm_world = tmp_comm_world;
            if (simfail_rank != myrank)
                MPI_Comm_group(gdsi_comm_world, &gdsi_group_world);

            gdsi_dprintf(GDS_DEBUG_INFO, "After barrier.\n");
            pthread_mutex_lock(&gdsi_simfail_mtx);
            pthread_cond_signal(&gdsi_simfail_pause);
            pthread_mutex_unlock(&gdsi_simfail_mtx);
            if (simfail_rank == myrank)
                goto exit_loop;
            gdsi_dprintf(GDS_DEBUG_INFO, "Before post_irecvs\n");
            post_irecvs(param, rparams, requests);
            gdsi_dprintf(GDS_DEBUG_INFO, "After post_irecvs\n");
            continue;
        } else if (status.MPI_TAG == GDS_TGTREQ_EXIT) {
            gdsi_dprintf(GDS_DEBUG_INFO, "Exit request received.\n");
            /* Cleanup all pending requests, as we are getting out of here. */
            cancel_irecvs(param->n_tags, requests);
            goto exit_loop;
        }

        struct gdsi_target_recv_param *rp = rparams + status.MPI_TAG;

        rp->handler(rp->priv, rp->buf, status.MPI_SOURCE);

        /* Re-post Irecv for the tag type which we just processed */
        GDSI_MPI_CALL(
            MPI_Irecv(rp->buf, rp->count, rp->datatype, MPI_ANY_SOURCE,
                status.MPI_TAG, gdsi_comm_world, requests + ri)
            );
    }
exit_loop:

    gdsi_dprintf(GDS_DEBUG_INFO, "Target side thread exiting.\n");

    free(put_priv.recv_buf);
    free(fetch_priv.send_buf);

    free(param);
    return NULL;
}
