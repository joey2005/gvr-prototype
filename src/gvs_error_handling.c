/*
  Global View Resilience (GVR)
  http://gvr.cs.uchicago.edu

  Copyright (C) 2014 University of Chicago.
  See license.txt in top-level directory.

  Error handling service in Global View Services (GVS) layer
*/

#include <stdlib.h>
#include <memory.h>
#include <sglib.h>
#include <pthread.h>

#include "gds_internal.h"

struct gdsi_error_attr_type {
    GDS_error_attr_key_t attr_key;
    char attr_name[GDS_ERROR_ATTR_NAME_MAX+1];
    GDS_error_attr_value_type_t value_type;
};

/* TODO: should allow arbitrary number of error attributes */
#define GDSI_MAX_ERROR_ATTRS 1024
static pthread_rwlock_t error_attr_key_lock = PTHREAD_RWLOCK_INITIALIZER;
static struct gdsi_error_attr_type error_attr_types[GDSI_MAX_ERROR_ATTRS];
static int error_attr_key_max;

GDS_error_attr_key_t gdsi_define_error_attr_key(
    const char *attr_name,
    GDS_error_attr_value_type_t value_type)
{
    GDS_error_attr_key_t new_key;

    pthread_rwlock_wrlock(&error_attr_key_lock);
    if (error_attr_key_max >= GDSI_MAX_ERROR_ATTRS) {
        pthread_rwlock_unlock(&error_attr_key_lock);
        return GDS_ERROR_ATTR_KEY_INVALID;
    }
    new_key = error_attr_key_max++;
    pthread_rwlock_unlock(&error_attr_key_lock);

    error_attr_types[new_key].attr_key  = new_key;
    strncpy(error_attr_types[new_key].attr_name,
            attr_name, GDS_ERROR_ATTR_NAME_MAX+1);
    error_attr_types[new_key].value_type = value_type;

    return new_key;
}

static size_t eavtype_elm_size(GDS_error_attr_value_type_t t)
{
    size_t elm_size = 0;

    switch (t) {
    case GDS_EAVTYPE_BYTE_ARRAY:
    case GDS_EAVTYPE_BYTE:
        elm_size = 1;
        break;

    case GDS_EAVTYPE_INT_ARRAY:
    case GDS_EAVTYPE_INT:
        elm_size = sizeof(int64_t);
        break;

    case GDS_EAVTYPE_FLOAT_ARRAY:
    case GDS_EAVTYPE_FLOAT:
        elm_size = sizeof(double);
        break;

    case GDS_EAVTYPE_MPIOBJ_ARRAY:
    case GDS_EAVTYPE_MPIOBJ:
        elm_size = sizeof(MPI_Group);
        break;
    }

    return elm_size;
}

static bool eavtype_is_array(GDS_error_attr_value_type_t t)
{
    switch (t) {
    case GDS_EAVTYPE_BYTE_ARRAY:
    case GDS_EAVTYPE_INT_ARRAY:
    case GDS_EAVTYPE_FLOAT_ARRAY:
    case GDS_EAVTYPE_MPIOBJ_ARRAY:
        return true;

    default:
        return true;
    }
}

bool gdsi_validate_error_attr(GDS_error_attr_key_t key, size_t val_len)
{
    bool ret = true;
    size_t elm_size;
    bool is_array = false;

    /* Check existence of the key */
    pthread_rwlock_rdlock(&error_attr_key_lock);
    if (key >= error_attr_key_max)
        ret = false;
    pthread_rwlock_unlock(&error_attr_key_lock);
    if (!ret)
        return ret;

    /* Check validity of data type */
    elm_size = eavtype_elm_size(error_attr_types[key].value_type);
    is_array = eavtype_is_array(error_attr_types[key].value_type);

    gdsi_dprintf(GDS_DEBUG_INFO,
                 "val_len=%zu, elm_size=%zu, is_array=%d\n",
                 val_len, elm_size, is_array);

    if (is_array)
        return val_len > 0 && val_len % elm_size == 0;
    else
        return val_len == elm_size;
}

size_t gdsi_error_desc_flat_size(const GDS_error_t desc)
{
    size_t total_attr_size, total_size;

    total_attr_size = desc->total_value_size
        + (sizeof(GDS_error_attr_key_t) + sizeof(size_t))
        * desc->n_attrs;
    total_size = sizeof(struct gdsi_error_desc_flat) + total_attr_size;

    return total_size;
}

struct gdsi_error_desc_flat *gdsi_flatten_error_desc(const GDS_error_t desc)
{
    struct gdsi_error_desc_flat *flat;
    size_t offset = 0, total_size;
    const struct gdsi_error_attr *attr;

    total_size = gdsi_error_desc_flat_size(desc);
    flat = malloc(total_size);
    if (flat == NULL)
        return NULL;

    flat->total_size = total_size;
    flat->n_attrs = desc->n_attrs;

    attr = desc->attrs;
    while (attr != NULL) {
        memcpy(flat->attr_data + offset, &attr->attr_key, sizeof(GDS_error_attr_key_t));
        offset += sizeof(GDS_error_attr_key_t);
        memcpy(flat->attr_data + offset, &attr->value_len, sizeof(size_t));
        offset += sizeof(size_t);
        memcpy(flat->attr_data + offset, attr->value, attr->value_len);
        offset += attr->value_len;
        attr = attr->next;
        gdsi_assert(offset <= total_size - sizeof(*flat));
    }

    return flat;
}

GDS_error_t gdsi_expand_error_desc(const struct gdsi_error_desc_flat *flat)
{
    int n;
    size_t offset = 0;
    GDS_error_t desc = NULL;

    if (GDS_create_error_descriptor(&desc) != GDS_STATUS_OK)
        return NULL;

    for (n = 0; n < flat->n_attrs; n++) {
        GDS_error_attr_key_t key;
        size_t len;
        memcpy(&key, flat->attr_data + offset, sizeof(key));
        offset += sizeof(key);
        memcpy(&len, flat->attr_data + offset, sizeof(len));
        offset += sizeof(len);
        gdsi_dprintf(GDS_DEBUG_INFO,
                     "adding attr : key=%d len=%zu\n",
                     key, len);
        if (GDS_add_error_attr(desc, key, len, flat->attr_data + offset) != GDS_STATUS_OK) {
            gdsi_dprintf(GDS_DEBUG_ERR,
                         "add_error_attr failed: key=%d len=%zu\n",
                         key, len);
            goto out_error;
        }
        offset += len;
        gdsi_assert(offset <= flat->total_size - sizeof(*flat));
    }

    return desc;

out_error:
    gdsi_free_error_desc(desc);
    return NULL;
}

void gdsi_free_error_desc(GDS_error_t desc)
{
    while (desc->attrs != NULL) {
        struct gdsi_error_attr *p_next = desc->attrs->next;
        free(desc->attrs);
        desc->attrs = p_next;
    }

    free(desc);
}

struct gdsi_error_attr *gdsi_find_error_attr(GDS_error_t error_desc,
                                      GDS_error_attr_key_t attr_key)
{
    struct gdsi_error_attr *attr = error_desc->attrs;
    while (attr != NULL) {
        if (attr->attr_key == attr_key)
            return attr;
        attr = attr->next;
    }

    return NULL;
}

enum gdsi_match_idx {
    MATCH_TOTAL,
    MATCH_EXACT,
    MATCH_RANGE,

    MATCH_MAX, /* sentinel. */
};

struct gdsi_attr_pred_match {
    int match_counts[MATCH_MAX];
};

/* if l > r? */
static bool match_sperior(const struct gdsi_attr_pred_match *l,
                          const struct gdsi_attr_pred_match *r)
{
    int i;

    for (i = 0; i < MATCH_MAX; i++) {
        if (l->match_counts[i] > r->match_counts[i])
            return true;
        else if (l->match_counts[i] < r->match_counts[i])
            return false;
    }

    return false;
}

static int compare_value(GDS_error_attr_value_type_t eavtype,
                         const void *v0, const void *v1)
{
    int64_t int64d;
    int32_t int32d;
    double dbld;

    switch (eavtype) {
    case GDS_EAVTYPE_BYTE_ARRAY:
    case GDS_EAVTYPE_BYTE:
        return *((char *) v0) - *((char *) v1);

    case GDS_EAVTYPE_INT_ARRAY:
    case GDS_EAVTYPE_INT:
        int64d = *((int64_t *) v0) - *((int64_t *) v1);
        if (int64d > 0)
            return 1;
        else if (int64d < 0)
            return -1;
        else
            return 0;

    case GDS_EAVTYPE_FLOAT_ARRAY:
    case GDS_EAVTYPE_FLOAT:
        dbld = *((double *) v0) - *((double *) v1);
        if (dbld > 0)
            return 1;
        else if (dbld < 0)
            return -1;
        else
            return 0;

    case GDS_EAVTYPE_MPIOBJ_ARRAY:
    case GDS_EAVTYPE_MPIOBJ:
        int32d = *((int32_t *) v0) - *((int32_t *) v1);
        if (int32d > 0)
            return 1;
        else if (int32d < 0)
            return -1;
        else
            return 0;
    }

    gdsi_panic("should not reach here!\n");

    return 0;
}

/* TODO: refinement to allow arbitrary number of dimensions */
#define MAX_DIMS 7

/* Test if pred matches given error attribute */
static bool predicate_match(const struct GDS_error_pred_term *term,
                            GDS_error_attr_key_t key,
                            size_t len, const void *val,
                            struct gdsi_attr_pred_match *m)
{
    int i, ndims;
    size_t elm_size;
    const void *min_ptr, *max_ptr;

    switch (term->match_expr) {
    case GDS_EMEXP_ANY:
        if (term->attr_key == key) {
            m->match_counts[MATCH_TOTAL]++;
            return true;
        }
        break;

    case GDS_EMEXP_VALUE:
        if (term->attr_key != key || term->value_len != len)
            break;

        if (memcmp(term->values, val, len) == 0) {
            m->match_counts[MATCH_TOTAL]++;
            m->match_counts[MATCH_EXACT]++;
            return true;
        }
        break;

    case GDS_EMEXP_RANGE:
        if (term->attr_key != key || len * 2 != term->value_len)
            break;

        elm_size = eavtype_elm_size(error_attr_types[key].value_type);

        ndims = len / elm_size;
        if (ndims > MAX_DIMS) {
            gdsi_dprintf(GDS_DEBUG_ERR,
                         "Currently maximum number of dimension is limited to %d. "
                         "Contact developers for fix!\n", MAX_DIMS);
            break;
        }
        min_ptr = term->values;
        max_ptr = term->values + len;
        for (i = 0; i < ndims; i++) {
            size_t offset = i * elm_size;

            /* !(min[i] > val[i]) */
            if (compare_value(error_attr_types[key].value_type,
                              min_ptr + offset, val + offset) > 0)
                return false;
            /* !(val[i] > max[i]) */
            if (compare_value(error_attr_types[key].value_type,
                              val + offset, max_ptr + offset) > 0)
                return false;
        }

        /* Range matches! */
        m->match_counts[MATCH_TOTAL]++;
        m->match_counts[MATCH_RANGE]++;
        return true;

    case GDS_EMEXP_NOT:
        if (term->attr_key != key) {
            m->match_counts[MATCH_TOTAL]++;
            return true;
        }
        break;
    }

    return false;
}

static bool count_match(
    struct gdsi_gds_common *common,
    const struct gdsi_error_handler *eh,
    GDS_error_t error_desc,
    struct gdsi_attr_pred_match *match)
{
    int i;
    struct gdsi_error_attr *attr;

    gdsi_assert(match->match_counts[MATCH_TOTAL] == 0);

    attr = error_desc->attrs;
    for (i = 0; i < error_desc->n_attrs; i++) {
        struct GDS_error_pred_term *term;

        gdsi_assert(attr != NULL);

        term = eh->terms;
        while (term != NULL) {
            predicate_match(term, attr->attr_key, attr->value_len,
                            attr->value, match);
            term = term->next;
        }
        attr = attr->next;
    }

    return match->match_counts[MATCH_TOTAL] == eh->n_terms;
}

const struct gdsi_error_handler *gdsi_match_error_handler(
    struct gdsi_gds_common *common,
    const struct gdsi_error_handler *eh_queue,
    GDS_error_t error_desc)
{
    const struct gdsi_error_handler *eh, *eh_match = NULL;
    struct gdsi_attr_pred_match best_match;

    for (eh = eh_queue; eh != NULL; eh = eh->next) {
        struct gdsi_attr_pred_match cur_match = { {0, }, };
        bool matched = count_match(common, eh, error_desc, &cur_match);
        if (!matched)
            continue;

        if (eh_match == NULL
            || match_sperior(&cur_match, &best_match)) {
            best_match = cur_match;
            eh_match = eh;
        }
    }

    return eh_match;
}
