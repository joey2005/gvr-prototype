# How to build GVR library

After cloning the code from a repository, first you need to run `autoreconf --install` to generate the `configure` script. However, if automake or libtool is upgraded, you may need to run `libtoolize` first.

    $ libtoolize # run this when automake/libtool is upgraded
    $ autoreconf --install # autoreconf -i

The rest of the step will be the same for end users. For details, please refer to docs/release-notes.md.

In general `autoreconf --install` is required only once. After that, you may have to run `autoreconf` when you pull some updates from upstream which modify one of `configure.ac` or `Makefile.am`.
