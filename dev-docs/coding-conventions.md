# Coding conventions

This document provides a set of rules about coding in the GVR project.

# Space and indentations

All indentations must have 4-character width. No hard tabs (\t) must not be used. All indentations must be represented by blank characters.

# Indent style

We generally follow the [K&R style](http://en.wikipedia.org/wiki/Indent_style#K.26R_style).

# Editor settings

## Emacs

Add the following code snippet to your `~/.emacs`.

    (defun default-c-mode-hook ()
      (c-set-style "k&r")
      (setq tab-width 8)
      (setq c-basic-offset 4)
      (setq indent-tabs-mode nil)
      (c-set-offset 'inextern-lang 0)
      (c-set-offset 'arglist-cont-nonempty '+))
    (add-hook 'c-mode-hook 'default-c-mode-hook)

## Vim

Add the following line to your `~/.vimrc`.

    autocmd BufEnter *.c set textwidth=79 autoindent expandtab tabstop=4

## gedit

On the bottom of the editor screen, set "Tab Width:" to 4 and check the "Use Spaces" menu.

# Spacing rules

In formulas, use spaces before and after an operator (except for unary operators (e.g. '-' or '^')).

    int a = 0; /* Preferable */
    int b = -a + 2; /* Preferable */
    int c=1; /* Not preferable */

Use a space after comma, but not before.

    f(a, b, c); /* Preferable */
    f(a,b,c); /* Not preferable */
    f(a , b , c); /* Not preferable */
    f(a ,b ,c); /* Not preferable */

Use a space after a _keyword_ (e.g. if, for, while, switch, ...) and a parenthesis.

    for (i = 0; i < 10; i++) { /* Preferable */
    if (a > 0) {/* Not preferable; need a space after '{' */
    if(a > 0){ /* Not preferable */

_Do not_ use a space after a function name.

    y = fun(x); /* Preferable */
    y = fun (x); /* Not preferable */

Do not leave unnecessary spaces at the end of the line. This can be checked automatically by renaming `gvr-prototype/.git/hooks/pre-commit.sample` to `pre-commit`. Or you can check it manually by `git diff --check` before making a commit.

Place single empty line between logical group of sentences. Do not place more than one empty lines in one location.

Do not place a space before a semicolon.

    y = x; /* Preferable */
    y = x ; /* Not preferable */

# Line and function length

* In general each line should be within 79 characters so that it can be displayed in 80-character-width editor or terminal.
* Try to keep each block or function as short as possible. Consider dividing too long function into multiple functions.

# Naming conventions

* Function names and variable names should be in lower case (except for `GDS_`) and connected via underscores `_`. (e.g. `some_function_name`)
* Constants names should be in upper case and connected via underscores. (e.g. `SOME_CONSTANT_NAME`)
* User-visible global identifiers should begin with a prefix `GDS_`. GVR-internal global identifiers should begin with a prefix `gdsi_` (for function and variable names) or `GDSI_` (for macro names).
* Give a long, descriptive names for global identifiers. Local identifiers can be short.
* Use typedef only when the details of the data type is totally opaque to users. For example, structures can be typedef'ed if the members of the structure are completely hidden from users. If users need to access the members, it should not be typedef'ed. Do not use typedef just for shortening the name.
    * Typedef'ed name should ends with `_t`

# Control flow style

Use [early exit](http://llvm.org/docs/CodingStandards.html#use-early-exits-and-continue-to-simplify-code).

    /* Preferable */
    int f() {
        if (check() != OK)
            return ERR;

        /* function body */
    }

    /* Not preferable */    
    int f() {
        if (check() == OK) {
            /* function body */
        } else
            return ERR;
    }

# Miscellaneous

* Auxiliary functions which are only referenced from the same file should be declared as a static function.
* Unnecessary code should be removed from the file. Do not leave these code blocks commented out.
