# Before making the final commit

## Update version numbers

Documents need update:

* configure.ac (AC_INIT([gvr], [X.Y.Z], [gvr-support@cs.uchicago.edu]))
* docs/src
    * conf.py
        * release
        * version
    * release.rst

### About versioning scheme

A version number looks like X.Y.Z.

    * X: Major version. Incremented on major feature release.
    * Y: Minor version. Incremented on minor feature update. (If there is a new feature in the release, increment this.)
    * Z: Revision. Incremented on minor bug fixes or performance improvements.

Each counter does not wrap; version 1.0.10 could be possible.

# After the final commit

## Testing

	# Build
    $ make dist
    $ tar xzf gvr-x.y.z.tar.gz # in some other dir
    $ cd gvr-x.y.z
    $ ./configure --prefix=$HOME
    $ make
    $ make -C tests check

	# Tests
	$ cd tests
	# In smaller platform, maximum number of processes can be reduced
	$ for n in 1 2 4 8 9 16 25 32; do
	  mpiexec -n $n ./tests
	  mpiexec -n $n ./tests_f90
	  mpiexec -n $n ./dms
	done
	# for process tests, smaller number would be fine
	$ for n in 2 4 8 9; do
	  mpiexec -n $n ./proc_fail_cur_ver_test
	  mpiexec -n $n ./proc_fail_old_ver_test
      mpiexec -n $n ./dms_proc_fail_test
	done
	$ cd ..

	# Installation
    $ make install

## Tagging

    $ git tag -a vX.Y.Z # Always annotate with tag, so that git-describe works
    $ git push <somewhere>
	# This two-step process will make sure fast-forward pusing
	$ git push <somewhere> --tag

## Building release tarball

	# Re-make tarball to reflect the version tag
	$ make dist
