# Making good commits

This documents describes general rules & guidelines for making a good commit in the GVR project (as well as many other open source projects).

# Good articles to read

* http://who-t.blogspot.de/2009/12/on-commit-messages.html
* https://github.com/erlang/otp/wiki/Writing-good-commit-messages

# General rules

* When start working on some enhancement (e.g. adding a feature) or bug fix, create a new branch with self-descriptive name (e.g. "issueXX" or "ulfm-utilization")
    * Make sure that you are creating a new branch from the latest master. Before creating a branch, do `git pull` on the master branch.
* Each commit should be as small as possible
* Each commit should have a small, usually single intention, for instance: "fix bug #XX", "introduce YY API", or "introduce structure ZZ"
* Each commit must form a "consistent state", meaning that at any point of the commit, the code must compile and pass the basic functionality tests(*1).

Sounds too hard to accomplish? Maybe it's time to learn about ["git add
-p" and "git rebase"](http://git-scm.com/book/en/Git-Tools-Rewriting-History).
This documents describes an addictive feature of git.

(*1: clear exception to this principle maybe the first phase of
introducing new functionality, where you may only have a stub code and
test cases. In such a case it's okay for the test code to fail, but
still the code should compile.)

# Commit message

Each commit should have a self-descriptive commit message.

* Commit message has two parts, short summary in the first line, and the body of the message.
* There is a single empty line between the summary and the body.
* Summary should begin with present tense (e.g. "Fix bug XX", not "Fixed bug XX"), and ideally shorter than 50 characters.

# Tips

If you figure out that you made a mistake in a latest commit and have not pushed it out, you can easily modify the latest commit by doing `git commit --amend`.
