.. GVR documentation master file, created by
   sphinx-quickstart on Fri Jul 12 13:38:19 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GVR documentation
===============================

.. toctree::
   :maxdepth: 2
   
   release.rst
   intro.rst
   start.rst
   openresilience.rst
   usecases.rst
   api.rst
