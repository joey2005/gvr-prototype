***********************
The GVR Interface 1.0.0
***********************

Initialization
==============

**GDS_init** (`argc, argv, requested_thread_support, provided_thread_support`)

  Initializes GDS library. Must be called before any other GDS functions. The `requested_thread_support` argument should be passed --- and the `provided_thread_support` argument will return --- one of the following, predefined values, which are inherited from MPI.

  * GDS_THREAD_SINGLE Each process has exactly one thread.
  * GDS_THREAD_FUNNELED Each process may have multiple threads, but only the main thread may call the GVR runtime system functions.
  * GDS_THREAD_SERIALIZED Each process may have multiple threads, and any thread may call the GVR runtime system functions. However, the application should arbitrate among the threads to ensure that at most one thread calls the GVR runtime system at the same time.
  * GDS_THREAD_MULTIPLE Each process may have multiple threads and any thread may call the GVR runtime system functions. Multiple threads may call the GVR runtime system functions concurrently.
  
  **Parameters:**
    * **argc**: the number of command line arguments (IN::integer)
    * **argv**: command line arguments (INOUT::array of string)
    * **requested_thread_support**: the thread concurrency level requested by the program (IN::GDS_thread_support_t)
    * **provided_thread_support**: actual thread concurrency level provided by the runtime system. Might be lower than the requested level depending on the runtime system's implementation (OUT::GDS_thread_support_t)

  C::

    GDS_status_t GDS_init(int argc, char **argv[], 
      GDS_thread_support_t requested_thread_support, 
      GDS_thread_support_t *provided_thread_support)
  
  Fortran::

    GDS_INIT(requested_thread_support, provided_thread_support, status)
      INTEGER, INTENT(IN) :: requested_thread_support
      INTEGER, INTENT(OUT) :: provided_thread_support
      INTEGER, INTENT(OUT) :: status


**GDS_finalize** ()

  Cleans up GDS library. Must be called after all other GDS procedures have completed.

  C::

    GDS_status_t GDS_finalize()


  Fortran::
    
    GDS_FINALIZE(status)
      INTEGER, INTENT(OUT) :: status


**GDS_comm_rank** (`comm, rank`)

  Get the calling process's rank within a given communicator.
  
  **Parameters:**
    * **comm**: the communicator (IN::GDS_comm_t)
    * **rank**: the rank of calling process (OUT::integer)

  C::

    GDS_status_t GDS_comm_rank(GDS_comm_t comm, int *rank)

  Fortran::
    
    GDS_COMM_RANK(comm, rank, status)
      INTEGER, INTENT(IN) :: comm
      INTEGER, INTENT(OUT) :: rank
      INTEGER, INTENT(OUT) :: status


**GDS_comm_size** (`comm, size`)

  Get the size of the given communicator.
  
  **Parameters:**
    * **comm**: the communicator (IN::GDS_comm_t)
    * **size**: the size of the communicator (OUT::integer)

  C::

    GDS_status_t GDS_comm_size(GDS_comm_t comm, int *size)

  Fortran::
    
    GDS_COMM_SIZE(comm, size, status)
      INTEGER, INTENT(IN) :: comm
      INTEGER, INTENT(OUT) :: size
      INTEGER, INTENT(OUT) :: status


Creating a Global Data Structure
================================

**GDS_create** (`ndims, count, element_type, global_to_local_func, local_to_global_func, local_buffer, local_buffer_count, resilience_priority, users, info, gds`)

  Creates a GDS object. A collective call that fuses a set of local memory buffers into a GDS object, enabling the use of said buffers as a global data structure by all processes specified in the users argument. A process can supply a null memory buffer.
  
  **Parameters:**
    * **ndims**: the number of dimensions in the global array (IN::GDS_size_t)
    * **count**: the number of elements in each dimension of the global array (IN::GDS_size_t)
    * **element_type**: type of elements in the GDS (IN::GDS_datatype_t)
    * **global_to_local_function**: function mapping global indices to local indices. `global_indices` is an input value. It is an ndims-length array signifying a single location in the global address space. `local_rank` is an output value. It is a scaler that signifies the process rank associated with the given global location. `local_offset` is an output value. It is a scalar that signifies the offset in the buffer provided by the process specified in the rank argument that corresponds with the given global indices. This argument is expressed in terms of elements. The function passed here must return the same results for every process. That is, every process should pass the same function pointer for this argument (IN::GDS_status_t (\*global_to_local_func) (GDS_size_t global_indices[], GDS_size_t \*local_rank, GDS_size_t \*local_offset))
    * **local_to_global_function**: function mapping local indices to global indices. local_offset is an input value. It is a scalar signifying an offset in the buffer provided by the calling process. This offset is specified in terms of number of elements. global_indices is an output value. It should output an ndims-length array signifying the global indices corresponding to `local_offset` for the calling process. This function is expected to return different results for every process. The calling process should provide global indices corresponding to its own provided buffer. (IN::GDS_status_t (\*local_to_global_func) (GDS_size_t local_offset, GDS_size_t \*global_indices))
    * **local_buffer**: base address of local buffer (IN::void*)
    * **local_buffer_count**: size of the local buffer in terms of the number of elements of the type defined by the `element_type` argument (IN::GDS_size_t)
    * **resilience_priority**: desired resilience priority for the GDS (IN::GDS_priority_t)
    * **users**: communicator of processes that can access the GDS object (IN::GDS_comm_t)
    * **info**: hints. `GDS_info_t` is a direct mapping of `MPI_Info`. See the MPI document for functions to manipulate an `MPI_Info` object. `GDS_create` will recognize `GDS_ORDER_KEY` (IN::GDS_info_t)
    * **gds**: returned handle to created GDS (OUT::GDS_gds_t)
  
  C::

    GDS_status_t GDS_create(GDS_size_t ndims, const GDS_size_t count[],
      GDS_datatype_t element_type,
      (GDS_status_t (*global_to_local_func)
          (const GDS_size_t global_indices[], int *local_rank, GDS_size_t *local_offset)),
      (GDS_status_t (*local_to_global_func)
          (GDS_size_t local_offset, GDS_size_t *global_indices)),
      void* local_buffer, GDS_size_t local_buffer_count, 
      GDS_priority_t resilience_priority, GDS_comm_t users, 
      GDS_info_t info, GDS_gds_t *gds)
  
  Fortran::

    GDS_CREATE(ndims, count, element_type, &
      global_to_local_func, local_to_global_func, &
      local_buffer, local_buffer_count, &
      resilience_priority, users, info, gds, status)
      INTEGER(8), INTENT(IN) :: ndims
      INTEGER(8), DIMENSION(*), INTENT(IN) :: count
      INTEGER, INTENT(IN) :: element_type
      TYPE(C_FUNPTR), VALUE, INTENT(IN) :: global_to_local_func
      TYPE(C_FUNPTR), VALUE, INTENT(IN) :: local_to_global_func
      TYPE(C_PTR), VALUE, INTENT(IN) :: local_buffer
      INTEGER(8), INTENT(IN) :: local_buffer_count
      INTEGER, INTENT(IN) :: resilience_priority
      INTEGER, INTENT(IN) :: users
      INTEGER, INTENT(IN) :: info
      TYPE(C_PTR), INTENT(OUT) :: gds
      INTEGER, INTENT(OUT) :: status
  
  **Function Type for global_to_local_function:**
  
  C::

    GDS_status_t global_to_local_func(const GDS_size_t global_indices[], 
      int *local_rank, GDS_size_t *local_offset) 
  
  Fortran::

    INTEGER(C_INT) FUNCTION VECTOR_GLOBAL_TO_LOCAL(global_indices, local_rank, &
      local_offset) BIND(C)
      USE, INTRINSIC :: ISO_C_BINDING
      INTEGER(C_SIZE_T), DIMENSION(*), INTENT(IN) :: global_indices
      INTEGER(C_INT), INTENT(OUT) :: local_rank
      INTEGER(C_SIZE_T), INTENT(OUT) :: local_offset
  
  **Function Type for local_to_global_function:**
  
  C::

    GDS_status_t local_to_global_func(GDS_size_t local_offset, 
      GDS_size_t global_indices[])
  
  Fortran::

    INTEGER(C_INT) FUNCTION VECTOR_LOCAL_TO_GLOBAL(local_offset, global_indices) BIND(C)
      USE, INTRINSIC :: ISO_C_BINDING
      INTEGER(C_INT), VALUE, INTENT(IN) :: local_offset
      INTEGER(C_SIZE_T), DIMENSION(*), INTENT(OUT) :: global_indices

  **Predefined Constants for Type: GDS_info_t**
    * **GDS_ORDER_KEY**: specifies data ordering of a multi-dimensional array. The value should be one of: `GDS_ORDER_DEFAULT`, `GDS_ORDER_ROW_MAJOR`, `GDS_ORDER_COL_MAJOR`. `GDS_ORDER_DEFAULT` is translated to language-dependent default: row major ordering in C binding and column major in Fortran binding.

**GDS_alloc** (`ndims, count, min_chunks, element_type, resilience_priority, users, info, gds`)

  A collective call that creates a GDS object that is accessible to all the users. Allocates the needed memory with layout as specified by the arguments.

  **Parameters:**
    * **ndims**: the number of dimensions in array (IN::GDS_size_t)
    * **count**: the number of elements in each dimension (IN::array with ndims elements of GDS_size_t)
    * **min_chunks**: the minimum chunk size in each dimension in number of elements (IN::array with ndims elements of GDS_size_t)
    * **element_type**: type of elements in the GDS (IN::GDS_datatype_t)
    * **resilience_priority**: desired resilience priority for the gds (IN::GDS_priority_t)
    * **users**: communicator of processes that can access the gds object (IN::GDS_comm_t)
    * **info**: hints. See the description of the info argument of ``GDS_create`` for details (IN::GDS_info_t)
    * **gds**: returned handle to created gds (OUT::GDS_gds_t)

  C::

    GDS_status_t GDS_alloc(GDS_size_t ndims, const GDS_size_t count, const 
      GDS_size_t min_chunks, GDS_datatype_t element_type,
      GDS_priority_t resilience_priority, GDS_comm_t users, GDS_info_t info,
      GDS_gds_t gds)

  Fortran::

    GDS_ALLOC(ndims, count, min_chunks, element_type, &
      resilience_priority, users, info, gds, status)
      INTEGER(8), INTENT(IN) :: ndims
      INTEGER(8), DIMENSION(*), INTENT(IN) :: count
      INTEGER(8), DIMENSION(*), INTENT(IN) :: min_chunks
      INTEGER, INTENT(IN) :: element_type
      INTEGER, INTENT(IN) :: resilience_priority
      INTEGER, INTENT(IN) :: users
      INTEGER, INTENT(IN) :: info
      TYPE(C_PTR), INTENT(OUT) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_free** (`gds`)

  Frees the GDS object gds and returns a null handle (equal to `GDS_NULL`). This is a collective call executed by all of the processes that can access the gds. If created with `GDS_alloc`, `GDS_free` will free corresponding allocated memory.
  
  **Parameters:**
    * **gds**: gds handle to free (INOUT:: GDS_gds_t)

  C::

    GDS_status_t GDS_free(GDS_gds_t *gds)

  Fortran::
    
    GDS_FREE(gds, status)
      TYPE(C_PTR), INTENT(OUT) :: gds
      INTEGER, INTENT(OUT) :: status


**GDS_get_attr** (`gds, attribute_key, attribute_value, flag`)

  Queries attributes for the given GDS object. If the specified attribute is present, the flag argument is set to true. Returns an error if an invalid attribute key is specified. The following attributes are predefined and are required to be present:

  * GDS_TYPE
  * GDS_CREATE_FLAVOR
  * GDS_GLOBAL_LAYOUT
  * GDS_NUMBER_DIMENSIONS
  * GDS_COUNT
  * GDS_CHUNK_SIZE
  
  **Parameters:**
    * **gds**: GDS object to query (IN::GDS_gds_t)
    * **attribute_key**: Attribute key (IN::GDS_attr_t)
    * **attribute_value**: attribute value (OUT::arbitrary)
    * **flag**: boolean indicating if the attribute is present (OUT::boolean)

  C::

    GDS_status_t GDS_get_attr(GDS_gds_t gds, GDS_attr_t attribute_key, 
      void *attribute_value, int *flag)

  Fortran::
    
    GDS_GET_ATTR(gds, attr_key, attr_val, flag, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, VALUE, INTENT(IN) :: attr_key
      TYPE(C_PTR), VALUE, INTENT(IN) :: attr_val
      INTEGER, INTENT(OUT) :: flag
      INTEGER, INTENT(OUT) :: status

  **Predefined Constants for Type: GDS_attr_t**
    * **GDS_TYPE**: the datatype of the GDS object. (GDS_datatype_t)
    * **GDS_CREATE_FLAVOR**: indicates how the GDS object was created. One of: GDS_FLAVOR_CREATE, GDS_FLAVOR_ALLOC (GDS_flavor_t)
    * **GDS_GLOBAL_LAYOUT**: the global layout of the GDS. Not supported yet.
    * **GDS_NUMBER_DIMENSIONS**: a GDS_size_t signifying the number of dimensions of the GDS. (GDS_size_t)
    * **GDS_COUNT**: an array of GDS_size_t containing a number of elements equal to the number of dimensions of the GDS. Signifies the size of every dimension in the GDS in terms of number of elements. User should be responsible to provide array buffer with sufficient length. (GDS_size_t [])
    * **GDS_CHUNK_SIZE**: an array of GDS_size_t containing a number of elements equal to the number of dimensions of the GDS. Signifies the size of every dimension in a single chunk of the GDS in terms of number of elements. Defined only if GDS was created with GDS_alloc. User should be responsible to provide array buffer with sufficient length. (GDS_size_t [])

**GDS_get_comm** (`gds, comm`)
  Returns a duplicate of the communicator used to create the GDS object gds.

  **Parameters:**
    * **gds**: GDS object (IN::GDS_gds_t)
    * **comm**: GDS communicator (OUT::GDS_comm_t)
   
  C::
   
    GDS_status_t GDS_get_comm(GDS_gds_t gds, GDS_comm_t *comm)

  Fortran::

    GDS_GET_COMM(gds, comm, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: comm
      INTEGER, INTENT(OUT) :: status

Using a Global Data Structure
=============================

**GDS_put** (`origin_addr, origin_ld, lo_index, hi_index, gds`)

  Puts data in the GDS memory. Transfers a block of entries from the origin, starting at origin_addr, to the segment of the GDS object specified by ``lo_index`` and ``hi_index``. Multiple ``GDS_put`` operations to the same gds memory location can lead to undefined output at the target location. Further, there is no ordering of GDS_put operations whatsoever, unless additional synchronization is used to explicitly order them. In general, put returns immediately --- it is nonblocking. Contents of the buffer pointed by `origin_addr` should not be modified until the operation is completed by a synchronization function (e.g. `GDS_wait`). This function returns an error if applied to a version of the gds that is not the current version.
  
  **Parameters:**
    * **origin_addr**: address of the local buffer from which data will be copied to the gds (IN::void*)
    * **origin_ld**: defines the shape of local buffer in units of the element_type (IN::array with ndims-1 elements of GDS_size_t)
    * **lo_index**: starting element of remote buffer (IN::array with ndims elements of GDS_size_t)
    * **hi_index**: ending element of remote buffer (IN::array with ndims elements of GDS_size_t)
    * **gds**: the gds in which data will be put (IN::GDS_gds_t)

  C::

    GDS_status_t GDS_put(void *origin_addr, GDS_size_t origin_ld[], 
      GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds);

  Fortran::
    
    GDS_PUT(orig_addr, orig_ld, lo_idx, hi_idx, gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: orig_addr
      INTEGER(8), DIMENSION(*), INTENT(IN) :: orig_ld
      INTEGER(8), DIMENSION(*), INTENT(IN) :: lo_idx
      INTEGER(8), DIMENSION(*), INTENT(IN) :: hi_idx
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_get** (`origin_addr, origin_ld, lo_index, hi_index, gds`)

  Gets data from the GDS memory. Similar to `GDS_put`, except that the direction of data transfer is reversed. Data is copied from the target memory to the origin. The copied data must fit, without truncation, in the origin buffer.  In general, get returns immediately --- it is nonblocking. A synchronization function `GDS_wait`, `GDS_wait_local` or `GDS_fence` should be used before the buffer is considered valid.

  **Parameters:**
    * **origin_addr**: address of the local buffer to which data will be copied from the gds (IN::void*)
    * **origin_ld**: defines the shape of local buffer in units of the element type (IN::array with ndims-1 elements of GDS_size_t)
    * **lo_index**: starting element of remote buffer (IN::array with ndims elements of GDS_size_t)
    * **hi_index**: ending element of remote buffer (IN::array with ndims elements of GDS_size_t)
    * **gds**: the gds from which data will be copied (IN::GDS_gds_t)

  C::

    GDS_status_t GDS_get(void *origin_addr, GDS_size_t origin_ld[], 
      GDS_size_t lo_index[], GDS_size_t hi_index[], GDS_gds_t gds);

  Fortran::
    
    GDS_GET(orig_addr, orig_ld, lo_idx, hi_idx, gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: orig_addr
      INTEGER(8), DIMENSION(*), INTENT(IN) :: orig_ld
      INTEGER(8), DIMENSION(*), INTENT(IN) :: lo_idx
      INTEGER(8), DIMENSION(*), INTENT(IN) :: hi_idx
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_acc** (`origin_addr, origin_ld, lo_index, hi_index, accumulate_op, gds`)

  Accumulates the contents of the origin buffer (as defined by `origin_addr` and `origin_ld`) to the specified segment of the GDS object, using the operation op. This is like `GDS_put` except that data are combined into the target area instead of the data in the target area being overwritten. This function is a non-blocking operation.
  
  **Parameters:**
    * **origin_addr**: address of the local buffer to which data will be accumulated to the gds (IN::void*)
    * **origin_ld**: defines the shape of local buffer in units of the element type (IN::array with ndims-1 elements of GDS_size_t)
    * **lo_index**: starting element of remote buffer (IN::array with ndims elements of GDS_size_t)
    * **hi_index**: ending element of remote buffer (IN::array with ndims elements of GDS_size_t)
    * **accumulate_op**: GDS accumulate operation used to accumulate date into the gds object (IN::GDS_op_t)
    * **gds**: the gds from which data will be copied (IN::GDS_gds_t)
  
  **Returns:** an error if applied to a version of the GDS object that is not the current version.

  C::

    GDS_status_t GDS_acc(void *origin_addr, GDS_size_t origin_ld[], 
      GDS_size_t lo_index[], GDS_size_t hi_index[], 
      GDS_op_t accumulate_op, GDS_gds_t gds);

  Fortran::

    GDS_ACC(orig_addr, orig_ld, lo_idx, hi_idx, acc_op, gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: orig_addr
      INTEGER(8), DIMENSION(*), INTENT(IN) :: orig_ld
      INTEGER(8), DIMENSION(*), INTENT(IN) :: lo_idx
      INTEGER(8), INTENT(IN) :: hi_idx
      INTEGER, VALUE, INTENT(IN) :: acc_op
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status


**GDS_get_acc** (`origin_addr, origin_ld, result_addr, result_ld, lo_index, hi_index, accumulate_op, gds`)

  Gets data from GDS memory and performs an operation on the target. Accumulates elements from the origin buffer `origin_addr` to the specified region of the GDS object using the operation op, and returns, in the result buffer `result_addr`, the content of the target region of the gds before the accumulation. The origin and result buffers (`origin_addr` and `result_addr`) must be disjoint. The result of the get operation must fit in the memory region pointed to by `result_addr`. This function is a non-blocking operation.
  
  **Parameters:**
    * **origin_addr**: address of the local buffer to which data will be accumulated to the gds (IN::void*)
    * **origin_ld**: defines the shape of local buffer in units of the element type (IN::void*)
    * **result_addr**: address of the local buffer to which current state of the gds region is written (IN::void*)
    * **result_ld**: shape of local result buffer (IN::array with ndims-1 elements of GDS_size_t)
    * **lo_index**: starting element of remote buffer (IN::array with ndims elements of GDS_size_t)
    * **hi_index**: ending element of remote buffer (IN::array with ndims elements of GDS_size_t)
    * **accumulate_op**: GDS accumulate operation used to accumulate date into the gds object (IN::GDS_op_t)
    * **gds**: the gds from which data will be copied (IN::GDS_gds_t)

  **Returns:** an error if applied to a version of the GDS object that is not the current version.

  C::

    GDS_status_t GDS_get_acc(void *origin_addr, GDS_size_t origin_ld[], 
      void *result_addr, GDS_size_t result_ld[], 
      GDS_size_t lo_index[], GDS_size_t hi_index[], 
      GDS_op_t accumulate_op, GDS_gds_t gds);

  Fortran::
  
    SUBROUTINE GDS_GET_ACC(origin_addr, origin_ld, result_addr, ressult_ld,
      lo_index, hi_index, accumulate_op, gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: origin_addr
      INTEGER(8), DIMENSION(*), INTENT(IN) :: origin_ld
      TYPE(C_PTR), VALUE, INTENT(IN) :: result_addr
      INTEGER(8), DIMENSION(*), INTENT(IN) :: result_ld
      INTEGER(8), DIMENSION(*), INTENT(IN) :: lo_index
      INTEGER(8), DIMENSION(*), INTENT(IN) :: hi_index
      INTEGER, VALUE, INTENT(IN) :: accumulate_op
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_compare_and_swap** (`compare_addr, swap_source_addr, swap_result_addr, gds_offset, gds`)

  Performs a compare and swap operation. This function compares one element in the compare buffer `compare_addr` with the target element of the GDS `gds_offset` and replaces the value at the target with the value in the origin buffer `swap_source_addr` if the compare buffer and the target element in the target gds are identical. The original value at the target gds is returned in the buffer `swap_result_addr`. All local buffers must be disjoint. This function is a non-blocking operation.
  

  **Parameters:**
    * **compare_addr**: address of the buffer which contains an element which will be compared to the corresponding element in the target gds (IN::void*)
    * **swap_source_addr**: address of the buffer which contains an element which may be written into the corresponding element in the target gds (IN::void*)
    * **swap_result_addr**: address of the buffer to which the previous value of the target gds will be written (IN::void*)
    * **gds_offset**: index of the target element in the target gds (IN::array with ndims elements of GDS_size_t)
    * **gds**: the target GDS object (IN::GDS_gds_t)

  C::

   GDS_status_t GDS_compare_and_swap(void *compare_addr, void *swap_source_addr, 
     void *swap_result_addr, GDS_size_t gds_offset[], GDS_gds_t gds);
  
  Fortran::

    GDS_COMPARE_AND_SWAP(orig_addr, orig_ld, lo_idx, hi_idx, acc_op, gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: compare_addr
      TYPE(C_PTR), VALUE, INTENT(IN) :: swap_source_addr
      TYPE(C_PTR), VALUE, INTENT(IN) :: swap_result_addr
      INTEGER(8), DIMENSION(*), INTENT(IN) :: gds_offset
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status
  
  **Notes**
    Due to the limitation of MPI, The datatype of GDS array using this function must belong to one of the following categories of predefined datatypes: C integer, Fortran integer, Logical, Multi-language types or Byte.

**GDS_access** (`gds, lo_index, hi_index, buffer_type, access_buffer, access_handle`)

  Depending on the runtime, the environment, and the value of the buffer_type argument, this function will return a pointer to either:

  * the buffer that contains the portion of the current version of the GDS that is stored locally, or,
  * a buffer that contains a duplicate of the data that is contained in the portion of the current version of the GDS that is stored locally.

  All operations on the specified GDS region are blocked until `gds_release` is called on `access_handle`.

  **Parameters:**
    * **gds**: the GDS object (IN::GDS_gds_t)
    * **lo_index**: the index of the first element from which the buffer will acquire its data (IN::array with ndims elements of `GDS_size_t`)
    * **hi_index**: the index of the last element from which the buffer will acquire its data (IN::array with ndims elements of `GDS_size_t`)
    * **buffer_type**: specifies the requested nature of the returned buffer. The value passed should be one of: `GDS_ACCESS_BUFFER_DIRECT` if the pointer should point directly to the buffer used by the GDS to store the local portion of its data, or, `GDS_ACCESS_BUFFER_COPY` if the pointer should point to a buffer containing a copy of the portion of the GDS stored locally, or, `GDS_ACCESS_BUFFER_ANY` to let the runtime decide between the above choices.
    * **access_buffer**: buffer pointing to the address specified in the buffer_type argument (OUT::void*)
    * **access_handle**: handle to track this GDS_access operation. This will be a required argument for GDS_release (OUT::GDS_access_handle_t)
  
  **Notes**
    If the array is created by `GDS_alloc`, then `lo_index` and `hi_index` is checked against the actual global array range. In case of check failure, the return value is `GDS_STATUS_RANGE`, indicating an error. For array created by `GDS_create`, this function returns the buffer provided by user during the call of `GDS_create`.

  C::

    GDS_status_t GDS_access(GDS_gds_t *gds, GDS_size_t lo_index[], 
      GDS_size_t hi_index[], GDS_access_buffer_t buffer_type,
      void *access_buffer, GDS_access_handle_t *access_handle)

  Fortran::

    GDS_ACCESS(gds, lo_index, hi_index, buf_type, access_buf, access_handle, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER(8), DIMENSION(*), INTENT(IN) :: lo_index
      INTEGER(8), DIMENSION(*), INTENT(IN) :: hi_index
      INTEGER, VALUE, INTENT(IN) :: buf_type
      TYPE(C_PTR), INTENT(OUT) :: access_buf
      TYPE(C_PTR), INTENT(OUT) :: access_handle
      INTEGER, INTENT(OUT) :: status

**GDS_get_access_buffer_type** (`access_handle, buffer_type`)

  Returns a value signifying the nature of the buffer returned by `GDS_access`.
  
  **Parameters:**
    * **access_handle**: handle for the access operation that should be queried (IN::GDS_access_handle_t)
    * **buffer_type**: the nature of the buffer returned in the call to access. Should be one of: `GDS_ACCESS_BUFFER_DIRECT` or `GDS_ACCESS_BUFFER_COPY` (OUT::GDS_access_buffer_t)

  C::
    
    GDS_status_t GDS_get_access_buffer_type(GDS_access_handle_t access_handle, 
      GDS_access_buffer_t *buffer_type);

  Fortran::

    GDS_GET_ACCESS_BUFFER_TYPE(access_handle, buffer_type, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: access_handle
      INTEGER, INTENT(OUT) :: buffer_type
      INTEGER, INTENT(OUT) :: status

**GDS_release** (`access_handle`)

  Puts the changes made to the contents of the access_buffer into the gds, and ensure that all subsequent read operations will see the new data.
  
  **Parameters:**
    * **access_handle**: handle that was output by a particular call to GDS_access (INOUT::GDS_access_handle_t)
  
  C::

    GDS_status_t GDS_release(GDS_access_handle_t access_handle);

  Fortran::
    GDS_RELEASE(access_handle, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: access_handle
      INTEGER, INTENT(OUT) :: status

**GDS_fence** (`gds`)

  A collective operation for memory consistency. All read operations following the fence will reflect all of the write operations preceding it. All read operations preceding the fence will not reflect any of the write operations following it. If `GDS_ROOT` is specified, this function performs fence operation for all gds arrays.
  
  **Parameters:**
    * **gds**: the GDS object on which the fence takes place (IN::GDS_gds_t)
  
  C::
    
    GDS_status_t GDS_fence(GDS_gds_t *gds);

  Fortran::

    GDS_FENCE(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status
    

**GDS_wait** (`gds`)

  Blocks until all operations on the gds are completed. The return of a call to GDS_wait signifies that, for the given gds, get, get_acc, and compare_and_swap (all operations returning values) have completed and their values are in the local buffers. In addition, put operations, and other one-sided operations, are completed locally.
  
  **Parameters:**
    * **gds**: the GDS object on which the wait takes place (IN::GDS_gds_t)
  
  C::
   
    GDS_status_t GDS_wait(GDS_gds_t *gds);

  Fortran::

    GDS_WAIT(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_wait_local** (`gds`)

  Blocks until all operations on the gds that impact data stored on the calling process are completed. Similar to GDS_wait, except only waits on reads from, and writes to, buffers local to the calling process.
  
  **Parameters:**
    * **gds**: the GDS object on which the wait takes place (IN::GDS_gds_t)

  C::
    
    GDS_status_t GDS_wait_local(GDS_gds_t *gds)

  Fortran::

    GDS_WAIT_LOCAL(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

Versioning, Error-Signaling, Error-Checking, and Error-Recovery
===============================================================

Demarcating Versions
--------------------

**GDS_version_inc** (`gds, increment, label, label_size`)

  Advances the version for the given GDS object. The runtime may choose whether or not to actually create a new version of the GDS object.
  Calling this function semantically equivalents to all clients calling GDS_wait() followed by GDS_fence() whether or not the runtime chooses to actually create a new version of the GDS object.

  **Parameters:**
    * **gds**: the GDS object (IN::GDS_gds_t)
    * **increment**: the number of versions by which the current version number is incremented (IN::GDS_size_t)
    * **label**: additional information that should be associated with the version to which the GDS object is advanced. Labels need not be unique (IN::string)
    * **label_size**: the size of version label (IN::size_t)


  C::
    
    GDS_status_t GDS_version_inc(GDS_gds_t gds, GDS_size_t increment, 
      const char *label, size_t label_size);

  Fortran::

    GDS_VERSION_INC(gds, incre, label, label_size, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER(8), VALUE, INTENT(IN) :: incre
      CHARACTER, DIMENSION(*), INTENT(IN) :: label
      INTEGER(8), VALUE, INTENT(IN) :: label_size
      INTEGER, INTENT(OUT) :: status


Error Signaling, Checking and Recovery
--------------------------------------

Terminologies
`````````````

**Local**
  only one process is involved.
**Global**
  all the processes using GDS object are involved.
**Stable point**
  a specific point when no process accesses the data via GDS operations and the array contents are in consistent status.

Error Attributes
````````````````

In GVR, errors are described with *error attributes*.
Each attribute is a pair of *error attribute key* and *error attribute value*, which
gives a parameter to describe the details of the error. For example, `GDS_EATTR_MEMORY_OFFSET` and `GDS_EATTR_MEMORY_COUNT` are two error attribute keys which describe damaged memory region.
An attribute value has a type, specified by *GDS_error_attr_value_type_t*, so that all the components around errors can agree on the interpretation of the value data.

**Predefined Constants for Type: GDS_error_attr_value_type_t**

  * **GDS_EAVTYPE_BYTE**: single 1-byte integer
  * **GDS_EAVTYPE_INT**: single 64-bit integer
  * **GDS_EAVTYPE_FLOAT**: single 64-bit floating point value
  * **GDS_EAVTYPE_BYTE_ARRAY**: an array of 1-byte integer
  * **GDS_EAVTYPE_INT_ARRAY**: an array of 64-bit integer
  * **GDS_EAVTYPE_FLOAT_ARRAY**: an array of 64-bit floating point value
  * **GDS_EAVTYPE_MPIOBJ**: single handle to an MPI object
  * **GDS_EAVTYPE_MPIOBJ_ARRAY**: an array of MPI objects

A set of error attributes form an *error descriptor*.
In the event of an error, an error descriptor is generated and passed across components which are involved in the error handling path.

Pre-defined Error Attributes
''''''''''''''''''''''''''''

GVR predefines the following error attributes. Type for each attribute is one of `GDS_error_attr_value_type_t`. The prefix `GDS_EAVTYPE_` is omitted for conciseness.
]
**Error Attributes Related to Data Corruption Errors**

  * **GDS_EATTR_MEMORY_VADDR**: starting virtual address of an affected region (INT)
  * **GDS_EATTR_MEMORY_SIZE**: size of affected region in bytes (INT)
  * **GDS_EATTR_MEMORY_AFFECTED_SIZE**: size of the region actually affected by an error in bytes(INT)
  * **GDS_EATTR_MEMORY_N_RANGES**: number of array index ranges affected by an error (INT)
  * **GDS_EATTR_MEMORY_DATA**: remaining (possibly corrupted) data (BYTE_ARRAY)
  * **GDS_EATTR_MEMORY_CHECK_BITS**: check bits information regarding corrupted region, provided by hardware or other lower component (BYTE_ARRAY)
  * **GDS_EATTR_MEMORY_LOCATION_REUSABLE**: if nonzero, memory is corrupted, but new data can be written into the same location. If zero, data cannot be written into the corrupted location again (INT)

  * **GDS_EATTR_APP**: attribute to signify an application-semantic error. Value has meaning only to the application (BYTE_ARRAY)
  * **GDS_EATTR_DETECTED_BY**: name or other identification of component that detected the error (BYTE_ARRAY)

  * **GDS_EATTR_GDS_INDEX**: start index of an affected region in a GDS (INT_ARRAY)
  * **GDS_EATTR_GDS_COUNT**: size and shape of affected region in number of elements (INT_ARRAY)
  * **GDS_EATTR_GDS_AFFECTED_COUNT**: size and shape of the region actually affected by an error in number of elements (INT_ARRAY)
  * **GDS_EATTR_GDS_VERSION**: version number of the corrupted version (INT)
  * **GDS_EATTR_GDS_LATENCY**: latency between actual data corruption and detection (INT)

**Error Attributes Related to Resource Loss Errors**
  * **GDS_EATTR_LOST_PROCESSES**: an `MPI_Group` object which describes lost processes (MPIOBJ)
  * **GDS_EATTR_LOST_COMMUNICATOR**: an `MPI_Comm` object which describes the old communcator before an error happens (MPIOBJ)
  * **GDS_EATTR_REPLACED_COMMUNICATOR**: an `MPI_Comm` object which describes the new communicator after an error happens (MPIOBJ)
  * **GDS_EATTR_LOST_COMPUTATION**: amount of lost computation (FLOAT)
  * **GDS_EATTR_MPI_RANK_UNRESPONSIVE**: specifies an mpi rank that is not responding to communication (INT)

Error Handlers
``````````````

Applications can define their own error handlers to deal with errors.
Each error handler is associated with an *error predicate* to describe what kind of error attributes can be handled.
Error predicate is a set of *terms*, which is a tuple of (attribute key, match expression type, value(s)...).

Match expression type must be one of the followings.

**Predefined Constants for Type: GDS_error_match_expr_t**

  * **GDS_EMEXP_ANY**: matches if the specified attribute key appears in the error descriptor, regardless of its value.
  * **GDS_EMEXP_VALUE**: matches if the specified attribute key appears in the error descriptor, and its value equals to the specified value
  * **GDS_EMEXP_RANGE**: matches if the specified attribute key appears in the error descriptor, and its value fits within a certain value range. A user specifies (min, max) values and it matches if an error descriptor has a value v which satisfies min <= v <= max. If a value is an array, the term matches if an error descriptor has a value v[N] which satisfies min[i] <= v[i] <= max[i] for all i < N.
  * **GDS_EMEXP_NOT**: matches if the specified attribute key does not appear in the error descriptor.

Applications can register multiple error handlers.
When an error is raised, GVR invokes one error handler all of whose error matching predicates are satisfied.
If there are more than one such an error handler, GVR chooses one according to the following rule.

1. A handler with the biggest number of predicate terms will be chosen
2. If there are still multiple candidates with the above rule, pick one with the biggest number of value matching (`GDS_EMEXP_VALUE`)
3. If there are still multiple candidates with the above rules, pick one with the biggest number of range matching (`GDS_EMEXP_RANGE`).
4. Further rules may be added in future, but currently it is erroneous to have multiple error handlers which cannot be narrowed down to a single handler with above rules.

API Functions
`````````````

**GDS_define_error_attr_key** (`attr_name, attr_name_size, value_type, new_key`)

  Defines a new error attribute key. This function is a collective call across the entire process in the program, and the parameters should be consistent across different processes.

  **Parameters:**
    * **attr_name**: name of the new attribute key. (IN::string)
    * **value_type**: attribute value type associated with (IN::GDS_error_attr_value_type_t)
    * **new_key**: the newly defined attribute key type (OUT::GDS_error_attr_key_t)

  C::

    GDS_status_t GDS_define_error_attr_key(const char *attr_name, GDS_size_t attr_name_size,
      GDS_error_attr_value_type_t value_type, GDS_error_attr_key_t *new_key);

  Fortran::

    GDS_DEFINE_ERROR_ATTR_KEY(attr_name, attr_name_size, value_type, new_key, status)
      CHARACTER, DIMENSION(*), INTENT(IN) :: attr_name
      INTEGER(8), VALUE, INTENT(IN) :: attr_name_size
      INTEGER, VALUE, INTENT(IN) :: value_type
      INTEGER, INTENT(OUT) :: new_key
      INTEGER, INTENT(OUT) :: status

**GDS_create_error_pred** (`pred`)

  Creates a new error matching predicate object.

  **Parameters**
    * **pred**: a newly created error matching predicate object (OUT::GDS_error_pred_t)

  C::

    GDS_status_t GDS_create_error_pred(GDS_error_pred_t *pred);

  Fortran::

    GDS_CREATE_ERROR_PRED(pred, status)
      TYPE(C_PTR), INTENT(OUT) :: pred
      INTEGER, INTENT(OUT) :: status

**GDS_free_error_pred** (`pred`)

  Frees a new error matching predicate object.

  **Parameters**
    * **pred**: a error matching predicate object to be freed (INOUT::GDS_error_pred_t)

  C::

    GDS_status_t GDS_free_error_pred(GDS_error_pred_t *pred);

  Fortran::

    GDS_FREE_ERROR_PRED(pred, status)
      TYPE(C_PTR), INTENT(OUT) :: pred
      INTEGER, INTENT(OUT) :: status

**GDS_create_error_pred_term** (`attr_key, match_expr, value_len, values, term`)

  Creates a new error matching predicate term object.

  **Parameters**
    * **attr_key**: An error attribute key associated with an error matching expression represented in this term (IN::GDS_error_attr_key_t)
    * **match_expr**: An error matching expression type (IN::GDS_error_match_expr_t)
    * **value_len**: Length of value (in bytes) associated with an error matching expression represented in this term (IN::GDS_size_t)
    * **values**: A pointer to a memory buffer which contains one or more values associated with an error matching expression represented in this term (IN::void \*).
    * **term**: a newly created error matching predicate term object (OUT::GDS_error_pred_term_t)

  **Notes on Values**
    `value_len` and `values` parameters will be used only if `match_expr` equals to `GDS_EMEXP_VALUE` or `GDS_EMEXP_RANGE`. When specifying ranges (`GDS_EMEXP_RANGE`), `values` should point to a memory buffer which contains two elements, minimum (first element) and maximum (second element). `value_len` shall be the total size of two elements. When specifying array range, the first half of the buffer is for the minimum and the second half is for the maximum.

  C::

    GDS_status_t GDS_create_error_pred_term(GDS_error_attr_key_t attr_key,
      GDS_error_match_expr_t match_expr, GDS_size_t value_len, const void *values,
      GDS_error_pred_term_t *term);

  Fortran::

    GDS_CREATE_ERROR_PRED_TERM(attr_key, match_expr, value_len,
      values, term, status)
      INTEGER, VALUE, INTENT(IN) :: attr_key
      INTEGER, VALUE, INTENT(IN) :: match_expr
      INTEGER(8), VALUE, INTENT(IN) :: value_len
      TYPE(C_PTR), VALUE, INTENT(IN) :: values
      TYPE(C_PTR), INTENT(OUT) :: term
      INTEGER, INTENT(OUT) :: status

**GDS_free_error_pred_term** (`term`)

  Frees a new error matching predicate term object.

  **Parameters**
    * **pred**: a error matching predicate term object to be freed (INOUT::GDS_error_pred_term_t)

  C::

    GDS_status_t GDS_free_error_pred_term(GDS_error_pred_t *pred);

  Fortran::

    GDS_FREE_ERROR_PRED_TERM(term, status)
      TYPE(C_PTR), INTENT(OUT) :: term
      INTEGER, INTENT(OUT) :: status

**GDS_add_error_pred_term** (`pred, term`)

  Adds an error matching predicate term to the predicate.

  **Parameters**
    * **pred**: An error matching predicate object (IN::GDS_error_pred_t)
    * **term**: An error matching predicate term object to be added to `pred` (IN::GDS_error_pred_term_t)

  **Notes**
    It is safe to free the `term` object by calling `GDS_free_error_pred_term` when this function returns.

  C::

    GDS_status_t GDS_add_error_pred_term(GDS_error_pred_t pred, const GDS_error_pred_term_t term);

  Fortran::

    GDS_ADD_ERROR_PRED_TERM(pred, term, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: pred
      TYPE(C_PTR), VALUE, INTENT(IN) :: term
      INTEGER, INTENT(OUT) :: status

**GDS_create_error_descriptor** (`error_desc`)

  Allocates a new empty error descriptor. The returned error descriptor should eventually be passed to either one of `GDS_raise_local_error` or `GDS_raise_global_error`.

  **Parameters:**
    * **error_desc**: returned handle to created error descriptor (OUT::GDS_error_t)

  C::

    GDS_status_t GDS_create_error_descriptor(GDS_error_t *error_desc)

  Fortran::

    GDS_CREATE_ERROR_DESCRIPTOR(error_desc, status)
      TYPE(C_PTR), INTENT(OUT) :: error_desc
      INTEGER, INTENT(OUT) :: status

**GDS_free_error_descriptor** (`error_desc`)

  Frees an error descriptor. Please note that this function is only for cleaning up purpose, for example error descriptor initialization is aborted for some reason. Usually an error descriptor will be passed to either one of `GDS_raise_local_error` or `GDS_raise_global_error`, and will be freed at the end of an error handling path (by either `GDS_resume_local` or `GDS_resume_global`), so users do not have to free an error descriptor by `GDS_free_error_descriptor`.

  **Parameters:**
    * **error_desc**: error descriptor to be freed (INOUT::GDS_error_t)

  C::

    GDS_status_t GDS_free_error_descriptor(GDS_error_t *error_desc)

  Fortran::

    GDS_FREE_ERROR_DESCRIPTOR(error_desc, status)
      TYPE(C_PTR), INTENT(OUT) :: error_desc
      INTEGER, INTENT(OUT) :: status

**GDS_add_error_attr** (`error_desc, attr_key, attr_len, attr_val`)

  Adds an error attribute to error descriptor object.

  **Parameters:**
    * **error_desc**: the error descriptor (IN::GDS_error_t)
    * **attr_key**: attribute key to be added (IN::GDS_error_attr_key_t)
    * **attr_len**: size of attribute value in bytes (IN::GDS_size_t)
    * **attr_val**: attribute value (IN::arbitrary)

  C::

    GDS_status_t GDS_add_error_attr(GDS_error_t error_desc,
      GDS_error_attr_t attr_key, GDS_size_t attr_len, const void *attr_val);

  Fortran::

    GDS_ADD_ERROR_ATTR(err_desc, attr_key, attr_len, attr_val, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: err_desc
      INTEGER, VALUE, INTENT(IN) :: attr_key
      INTEGER(8), VALUE, INTENT(IN) :: attr_len
      TYPE(C_PTR), VALUE, INTENT(IN) :: attr_val
      INTEGER, INTENT(OUT) :: status

**GDS_get_error_attr** (`error_desc, attribute_key, attribute_value, flag`)

  Queries attributes of the given error descriptor object. If the specified attribute is present, the flag argument is set to true.
  
  **Parameters:**
    * **error_desc**: the error descriptor (IN::GDS_error_t)
    * **attribute_key**: attribute key (IN::GDS_error_attr_t)
    * **attribute_value**: attribute value (OUT::arbitrary)
    * **flag**: boolean indicating if the attribute is present (OUT::boolean)

  C::
    
    GDS_status_t GDS_get_error_attr(GDS_error_t error_desc, 
      GDS_error_attr_t attr_key, void *attr_val, int *flag);

  Fortran::

    GDS_GET_ERROR_ATTR(err_desc, attr_key, attr_val, flag, status)
      INTEGER, VALUE, INTENT(IN) :: err_desc
      INTEGER, INTENT(IN) :: attr_key
      TYPE(C_PTR), VALUE, INTENT(IN) :: attr_val
      INTEGER, INTENT(OUT) :: flag
      INTEGER, INTENT(OUT) :: status

**GDS_get_error_attr_len** (`error_desc, attribute_key, attribute_len, flag`)

  Queries attribute value length of the given error descriptor object. If the specified attribute is present, the flag argument is set to true.

  **Parameters:**
    * **error_desc**: the error descriptor (IN::GDS_error_t)
    * **attribute_key**: attribute key (IN::GDS_error_attr_t)
    * **attribute_len**: attribute value length (OUT::GDS_size_t)
    * **flag**: boolean indicating if the attribute is present (OUT::boolean)

  C::

    GDS_status_t GDS_get_error_attr_len(GDS_error_t error_desc,
      GDS_error_attr_t attr_key, GDS_size_t *attr_len, int *flag);

  Fortran::

    GDS_GET_ERROR_ATTR_LEN(err_desc, attr_key, attr_len, flag, status)
      INTEGER, VALUE, INTENT(IN) :: err_desc
      INTEGER, INTENT(IN) :: attr_key
      INTEGER(8), INTENT(OUT) :: attr_len
      INTEGER, INTENT(OUT) :: flag
      INTEGER, INTENT(OUT) :: status

**GDS_register_local_error_check** (`gds, error_check_function, check_priority`)

  User programs can implement whatever error checking they would like, and then signal errors to the GVR system using raise_error. The `register_local_error_check()` function allows users to register a local checking function to the calling process. It is assumed these checking routines are uncoordinated with other processes. It will be triggered and run by the GVR system for the registered process. It is assumed these checking routines make appropriate calls to `raise_error()` when they find them. During an error-check, the application must not write to the passed GDS.

  **Parameters:**
    * **gds**: GDS on which this error checking routine is being registered (IN::GDS_gds_t)
    * **error_check_function**: the checking function, which takes a GDS, the priority of the check. The function is expected to check the given GDS for faults. If a fault is located, this function should call GDS_raise_error with the appropriate arguments (IN::GDS_status_t (\*check_func) (GDS_gds_t gds, GDS_priority_t check_priority))
    * **check_priority**: desired priority for the error check. High priority means run first (low cost, finds common errors). Medium and low priorities mean run as last resort (may be high cost, finds obscure errors) (IN::GDS_priority_t)

  C::

    GDS_status_t GDS_register_local_error_check(GDS_gds_t gds,
      GDS_check_func_t check_func,
      GDS_priority_t check_priority);

  Fortran::

    GDS_REGISTER_LOCAL_ERROR_CHECK(gds, check_func, check_priority, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      TYPE(C_FUNPTR), VALUE, INTENT(IN) :: check_func
      INTEGER, VALUE, INTENT(IN) :: check_priority
      INTEGER, INTENT(OUT) :: status

**GDS_register_global_error_check** (`gds, error_check_function, check_priority`)

  User programs can implement whatever error checking they would like, and then signal errors to the GVR system using raise_error. The `register_global_error_check()` function is a collective call, which allows users to register checking function to all the processes for the given GDS. It is assumed these checking routines are coordinated with all the processes. It will be triggered and run by the GVR system for each processes with synchronization. It is assumed these checking routines make appropriate calls to `raise_error()` when they find them. During an error-check, the application must not write to the passed GDS.
    
  **Parameters:**
    * **gds**: GDS on which this error checking routine is being registered (IN::GDS_gds_t)
    * **error_check_function**: the checking function, which takes a GDS, the priority of the check, and a flag specifying whether or not the check is coordinated. The function is expected to check the given GDS for faults. If a fault is located, this function should call GDS_raise_error with the appropriate arguments. If this error check is coordinated, every process in the communicator for the given GDS must pass the same value for this argument (IN::GDS_status_t (\*check_func) (GDS_gds_t gds, GDS_priority_t check_priority))
    * **check_priority**: desired priority for the error check.  High priority means run first (low cost, finds common errors). Medium and low priorities mean run as last resort (may be high cost, finds obscure errors) (IN::GDS_priority_t)

  C::

    GDS_status_t GDS_register_global_error_check(GDS_gds_t gds,
      GDS_check_func_t check_func,
      GDS_priority_t check_priority);

  Fortran::

    GDS_REGISTER_GLOBAL_ERROR_CHECK(gds, check_func, check_priority, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      TYPE(C_FUNPTR), VALUE, INTENT(IN) :: check_func
      INTEGER, VALUE, INTENT(IN) :: check_priority
      INTEGER, INTENT(OUT) :: status

**GDS_raise_local_error** (`gds, error_desc`)

  Indicates to the runtime that a fault has occurred in the calling process. It will interrupt the calling process and trigger the local error handler until a `GDS_resume` is called.
    * **gds**: the GDS object (IN::GDS_gds_t)
    * **error_desc**: description of the error raised. In general, system-raised error types will be enumerated and available to all programs being run at initialization time. Application programs must declare and then handle any errors that they raise (IN::GDS_error_t)

  C::

    GDS_status_t GDS_raise_local_error(GDS_gds_t gds, GDS_error_t error_desc);

  Fortran::

    GDS_RAISE_LOCAL_ERROR(gds, err_desc, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      TYPE(C_PTR), VALUE, INTENT(IN) :: err_desc
      INTEGER, INTENT(OUT) :: status

**GDS_raise_global_error** (`gds, error_desc`)

  Indicates to the runtime that a fault has occurred that involves all the processes for the given GDS object. It will interrupt all the processes and trigger the global error handler. It is not a collective call, either local check or global check can raise global error. The processes using the given GDS object are prevented from performing GDS library operations on the GDS object until a `GDS_resume_global` is called.

    * **gds**: the GDS object (IN::GDS_gds_t)
    * **error_desc**: description of the error raised.  In general, system-raised error types will be enumerated and available to all programs being run at initialization time. Application programs must declare and then handle any errors that they raise (IN::GDS_error_t)

  **Notes**
    This function raises an global error, but the corresponding error handler will not be invoked immediately. The handler will be invoked at the next stable point (*i.e.* `GDS_fence` or `GDS_version_inc`).

  C::

    GDS_status_t GDS_raise_global_error(GDS_gds_t gds, GDS_error_t error_desc);

  Fortran::

    GDS_RAISE_GLOBAL_ERROR(gds, err_desc, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      TYPE(C_PTR), VALUE, INTENT(IN) :: err_desc
      INTEGER, INTENT(OUT) :: status

**GDS_register_local_error_handler** (`gds, pred, recovery_func`)

  Specifies a local recovery procedure in the calling process. If a local error is detected, the runtime will execute the given function on the involved process.

  **Parameters:**
    * **gds**: a handle of the gds object for which the error handler will receive notifications (IN::GDS_gds_t)
    * **pred**: indicates an error matching predicates to describe what kind of errors `recovery_func` will handle (IN::GDS_error_pred_t)
    * **recovery_func**: the specified recovery function. The function is expected to take a GDS object, a boolean specifying whether or not this error recovery is coordinated, and an object describing the error, and return a status signifying whether or not the recovery has succeeded. Component-specific error information, such as location information for a memory error, shall be encapsulated in error_desc. The recover_func may assume that the application has been halted, and is expected to call GDS_resume on its given GDS after successful recovery (IN::GDS_status_t (\*recover_func)(GDS_gds_t gds, boolean is_coordinated, GDS_error_t error_desc))

  **Notes**
    It is safe to free the `pred` object by calling `GDS_free_error_pred` when this function returns.
  
  C::

    GDS_status_t GDS_register_local_error_handler(GDS_gds_t gds, 
      GDS_error_pred_t pred, GDS_recovery_func_t recovery_func);

  Fortran::

    GDS_REGISTER_LOCAL_ERROR_HANDLER(gds, pred, recovery_func, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, VALUE, INTENT(IN) :: pred
      TYPE(C_FUNPTR), INTENT(IN) :: recovery_func
      INTEGER, INTENT(OUT) :: status

**GDS_register_global_error_handler** (`gds, pred, recovery_func`)

  Specifies a globally coordinated recovery procedure in all the processes for a given GDS. It is a collective call. If an error is detected within the given component, then the runtime will execute the given function in coordinated manner at the future stable point.
    
  **Parameters:**
    * **gds**: a handle of the gds object for which the error handler will receive notifications (IN::GDS_gds_t)
    * **pred**: indicates an error matching predicates to describe what kind of errors `recovery_func` will handle (IN::GDS_error_pred_t)
    * **recovery_func**: the specified recovery function. The function is expected to take a GDS object, a boolean specifying whether or not this error recovery is coordinated, and an object describing the error, and return a status signifying whether or not the recovery has succeeded. Component-specific error information, such as location information for a memory error, shall be encapsulated in error_desc. The recover_func may assume that the application has been halted, and is expected to call GDS_resume on its given GDS after successful recovery (IN::GDS_status_t (\*recover_func)(GDS_gds_t gds, boolean is_coordinated, GDS_error_t error_desc))

  **Notes**
    It is safe to free the `pred` object by calling `GDS_free_error_pred` when this function returns.

  C::

    GDS_status_t GDS_register_global_error_handler(GDS_gds_t gds, 
      GDS_error_pred_t pred, GDS_recovery_func_t recovery_func);

  Fortran::

    GDS_REGISTER_GLOBAL_ERROR_HANDLER(gds, pred, recovery_func, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      TYPE(C_PTR), VALUE, INTENT(IN) :: pred
      TYPE(C_FUNPTR), VALUE, INTENT(IN) :: recovery_func
      INTEGER, INTENT(OUT) :: status

**GDS_resume_local** (`gds, error_desc`)

  Resumes the process if the local error handler succeeds.
  
  **Parameters:**
    * **gds**: the GDS this error handler is resuming (IN::GDS_gds_t)
    * **error_desc**: the error descriptor passed to the error handler function (IN::GDS_error_t)

  C::

    GDS_status_t GDS_resume_local(GDS_gds_t gds, GDS_error_t error_desc);

  Fortran::
  
    GDS_RESUME_LOCAL(gds, err_desc, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      TYPE(C_PTR), VALUE, INTENT(IN) :: err_desc
      INTEGER, INTENT(OUT) :: status

**GDS_resume_global** (`gds, error_desc`)

  Resumes the processes if the global error handler succeeds.
  
  **Parameters:**
    * **gds**: the GDS this error handler is resuming (IN::GDS_gds_t)
    * **error_desc**: the error descriptor passed to the error handler function (IN::GDS_error_t)

  C::

    GDS_status_t GDS_resume_global(GDS_gds_t gds, GDS_error_t error_desc);

  Fortran::

    GDS_RESUME_GLOBAL(gds, err_desc, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      TYPE(C_PTR), VALUE, INTENT(IN) :: err_desc
      INTEGER, INTENT(OUT) :: status

**GDS_check_all_errors** (`gds`)

  Trigger all of the registered global/local error check functions and gather the checking results from all processes. It is a collective function.
    
  **Parameters:**
    * **gds**: the GDS on which error checking routine is registered (IN::GDS_gds_t)

  C::

    GDS_status_t GDS_check_all_errors(GDS_gds_t gds);

  Fortran::

    GDS_CHECK_ALL_ERRORS(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_invoke_local_error_handler** (`gds`)

  Invoke the registered local error handler.
    
  **Parameters:**
    * **gds**: the GDS on which error handler is registered (IN::GDS_gds_t)

  C::

    GDS_status_t GDS_invoke_local_error_handler(GDS_gds_t gds);

  Fortran::
    
    GDS_INVOKE_LOCAL_ERROR_HANDLER(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_invoke_global_error_handler** (`gds`)

  Invoke the registered global error handler from all processes. By calling this function, application defines a stable point. It is a collective function.
    
  **Parameters:**
    * **gds**: the GDS on which error handler is registered (IN::GDS_gds_t)

  C::

    GDS_status_t GDS_invoke_global_error_handler(GDS_gds_t gds);

  Fortran::
    
    GDS_INVOKE_GLOBAL_ERROR_HANDLER(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

Version Navigation
------------------

**GDS_get_version_number** (`gds, version_number`)

  Returns version number for the given GDS object.
  
  **Parameters:**
    * **gds**: the GDS object whose version that should be queried (IN::GDS_gds_t)
    * **version_number**: the version number of the given GDS object (OUT::GDS_size_t)

  C::
    
    GDS_status_t GDS_get_version_number(GDS_gds_t gds, GDS_size_t *version_number);

  Fortran::

    GDS_GET_VERSION_NUMBER(gds, ver_num, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER(8), INTENT(OUT) :: ver_num
      INTEGER, INTENT(OUT) :: status

**GDS_get_version_label** (`gds, label, label_size`)

  Returns version label and its size for the given GDS object.
  
  **Parameters:**
    * **gds**: the GDS object whose version that should be queried (IN::GDS_gds_t)
    * **label**: the version label of the given GDS object (OUT::string)
    * **label_size**: the size of version label (OUT::size_t)

  C::
    
    GDS_status_t GDS_get_version_label(GDS_gds_t gds, char **label, size_t *label_size);

  Fortran::

    GDS_GET_VERSION_LABEL(gds, label, label_size, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      CHARACTER, POINTER, INTENT(OUT) :: label(:)
      INTEGER(8), INTENT(OUT) :: label_size
      INTEGER, INTENT(OUT) :: status

**GDS_move_to_newest** (`gds`)

  Sets the version of the GDS object to its most recent available version.
  
  **Parameters:**
    * **gds**: the GDS object (INOUT::GDS_gds_t)
  
  C::
    
    GDS_status_t GDS_move_to_newest(GDS_gds_t gds);

  Fortran::
    
    GDS_MOVE_TO_NEWEST(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_move_to_next** (`gds`)

  Sets the version of the given GDS object to the version of that GDS that is both further forward in time than the given version, and is the oldest available version that is forward in time.
  
  **Parameters:**
    * **gds**: the GDS object (INOUT::GDS_gds_t)

  **Returns:** an error if the given version of the GDS is already the newest version.

  C::
    
    GDS_status_t GDS_move_to_next(GDS_gds_t gds);

  Fortran::

    GDS_MOVE_TO_NEXT(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status


**GDS_move_to_prev** (`gds`)

  Sets the version of the given GDS object to the version of that GDS that is both backward in time from the given version, and the newest available version that is backward in time.
  
  **Parameters:**
    * **gds**: the GDS object (INOUT::GDS_gds_t)

  Returns an error if the given version of the GDS is already the oldest version.
  
  C::
    
    GDS_status_t GDS_move_to_prev(GDS_gds_t gds);

  Fortran::

    GDS_MOVE_TO_PREV(gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER, INTENT(OUT) :: status

**GDS_version_dec** (`gds, dec`)

  Sets the GDS_handle to point to current minus dec. If the requested version doesn't exist, returns an error.
    
  **Parameters:**
    * **gds**: the GDS handle where the version will be decremented (IN::GDS_gds_t)
    * **dec**: number by which to decrement the version number (IN::nonnegative integer)

  C::
    
    GDS_status_t GDS_version_dec(GDS_gds_t gds, GDS_size_t dec);

  Fortran::

    GDS_VERSION_DEC(gds, dec, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: gds
      INTEGER(8), VALUE, :: dec
      INTEGER, INTENT(OUT) :: status

**GDS_descriptor_clone** (`in_gds, clone_gds`)

  Returns a copy of the given GDS descriptor. The returned descriptor should be freed by GDS_free when no longer needed.
    
  **Parameters:**
    * **in_gds**: the GDS descriptor to be cloned (IN::GDS_gds_t)
    * **clone_gds**: GDS descriptor identical to in_gds (OUT::GDS_gds_t)
  
  C::
    
    GDS_status_t GDS_descriptor_clone(GDS_gds_t in_gds, GDS_gds_t *clone_gds);

  Fortran::
    
    GDS_DESCRIPTOR_CLONE(in_gds, clone_gds, status)
      TYPE(C_PTR), VALUE, INTENT(IN) :: in_gds
      TYPE(C_PTR), INTENT(OUT) :: clone_gds
      INTEGER, INTENT(OUT) :: status

GDS Types
=========

**Predefined Constants for Type: GDS_comm_t**
  * **GDS_COMM_WORLD**: communicator containing all processes using the GDS library.

**Predefined Constants for Type: GDS_priority_t**
  * **GDS_PRIORITY_HIGH** or **GDS_PRIORITY_CRITICAL**: these structures are critical for recovery, and may not be reconstructible. If lost, the computation will need to be restarted.
  * **GDS_PRIORITY_MEDIUM** or **GDS_PRIORITY_EXPENSIVE**: these structures are costly to diagnose and reconstruct. It is worth expending redundancy effort to protect these structures.
  * **GDS_PRIORITY_LOW** or **GDS_PRIORITY_CHEAP**: these structures can be restored inexpensively. Perhaps they are replicated across nodes, constants loaded in to the program, or can be reconstructed easily via symmetry or other semantics or with inexpensive computation.

  Because error handlers can be defined by applications and registered with the GVR runtime, it is straightforward to implement varied error reporting/filtering.  Lower level error handling routines can simply filter the requests. For example, one could register promiscuous filters for critical structures, and more opaque filters for lower priority structures.

**Predefined Constants for Type: GDS_gds_t**
  * **GDS_ROOT**

  Special GDS object which can be passed only to error-checking, error-handling, and synchronization functions. Specifies that the operation in question should be applied to all GDS objects currently registered with the library.

GDS types corresponding to MPI types
------------------------------------

Elements of the following GDS datatypes can be safely cast to elements of their corresponding MPI datatypes and visa versa.
  .. list-table::
    :header-rows: 1

    * - GDS type
      - MPI type
    * - GDS_comm_t 
      - MPI_Comm
    * - GDS_datatype_t 
      - MPI_Datatype
    * - GDS_op_t 
      - MPI_Op
    * - GDS_info_t 
      - MPI_Info

Scraps
======

**Placeholder: Distribution-Related Operations**

These operations will manipulate objects of type GDS_distrib_t, which express
global data layout (including global gds size?)

Revision History
================

API 1.0.1
---------

* Now `GDS_fence` does not imply synchronization, indead it just ensures memory consistency. If synchronization is necessary, applications need to call `MPI_Barrier` or similar functions.

API 1.0.0
---------

* Introduces new Open Resilience error handling APIs

  * "Error category" was deprecated
  * A notion of "error matching predicate" was introduced, and `GDS_register_*_error_handler` now takes an error matching predicate object.
  * `GDS_create_error_descriptor` was changed. Attributes are now added via `GDS_add_error_attr` function.

* API changes

  * `GDS_create` now takes `GDS_info_t info` instead of `const char *hint`
  * `GDS_BASE` was removed from `GDS_attr_t`
  * Supported datatypes are clarified for `GDS_compare_and_swap`
  * `GDS_check_all_error` was renamed to `GDS_check_all_errors`
  * A new API `GDS_get_version_label` was introduced
  * `GDS_enumerate_all_versions` was removed

API 0.7.5
---------
* Adds Fortran APIs documentation

API 0.7.4
---------

* Introduces pre-defined error categories
* Introduces ``GDS_create_error_descriptor`` and ``GDS_extend_error_category`` functions
* ``GDS_resume_*`` now takes an error descriptor as an argument
* ``GDS_get_error_attr`` now takes ``GDS_error_attr_t`` as an attribute key type instead of ``GDS_attr_t``.

API 0.7.3
---------

* Introduces GDS_info_t.
* Type of the info argument for GDS_create and GDS_alloc has been changed from string to GDS_info_t.
* GDS_create and GDS_alloc now supports both row-major and column-major order.
