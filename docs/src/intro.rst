******************
Introduction
******************

Concepts
========
* **Cooperative Resilience** Programmer cooperates with the system to manage application reliability. This includes providing information to guide resilience implementation. The programmer provides checking and recovery functions.

* **Versioned Arrays** Key abstraction is versions of arrays which represent the history of state evolution of the arrays. Versions should correspond to a stable (consistent) application point.

* **Resilience Priorities** Labelling of arrays as high, medium, or low resilience priority, which indicates to the system where effort/space should be expended to maximize return on application resilience.

* **Application Error Checks** Programmer-supplied routines that analyze arrays based on application semantics and, as appropriate, raise errors.

* **Application Error Handling**  Programmer-supplied routines that analyze the error and the available array versions, and repair application and resume execution.

Objectives
==========

* Enable programmers to express application-controlled resilience.
* Exploiting application semantics, multi-version arrays, and tolerating errors from hardware, software, unknown sources to continue execution.
* Application programmers able to control overhead of resilience and direct runtime effort with resilience priorities, and expression of version boundaries.

Open Issues
-----------
* Error-handling

  * Fault recovery asynchronous (error handler invoked asynch) or synchronous (error handler involved only at poll for faults).
  * Do we need any additional capabilities for error handling routines?

* Task-oriented parallelism

  * How to support unstructured task parallelism?

Design Requirements
===================

* It should be possible to derive GVR code from traditional global array programs using only incremental translation. "Incremental translation" should be understood to mean local rewrites, such as macros.

* GVR is only guaranteed to work as expected on well synchronized programs. The semantics of GVR do not support race conditions.

* There should be no additional overhead *required* to use versioned GVR structures. That is, the semantics of GVR should support a single-version implementation.

* The application is expected to access older versions in the context of an error-recovery. It is assumed that applications will rarely be in this context.
