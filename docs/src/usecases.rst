******************
Use Cases
******************

Introduction
============
GVR provides data-oriented resilience based on global view data and multi-version. It enables an application to incorporate resilience incrementally, expressing resilience proportionally to the application need. As a result, GVR fits for various types of parallel programs, including regular applications (SPMD) and irregular applications (task parallelism). Furthermore, GVR supports flexible usages, including direct application programming interface and co-existence with other runtime and programming model such as MPI.

In the use cases document, we first describe the typical parallel program structure from a data access point of view in order to build efficient global, reliable data structures. Then we present the use cases of general computation and multi-version checkpointing, followed by the examples of error check, signaling, and recovery.

Parallel program structure
==========================
Extensive research has studied design “patterns” for sequential computer programs and more recently for parallel programs. In general, study of design patterns has focused on patterns of interaction and composition from a program (generally function and control-centric) point of view. In sequential programs that presume a shared memory, these patterns may have little to do with program data access. In parallel programs, they are more closely related, but typically the design patterns are completely correlated (message passing interaction patterns and all other memory access local) or completely orthogonal.

We are interested in typical parallel program structure from a data access point of view in order to build efficient global, reliable data structures. In particular, we assume that the major application data structures are:

#. the primary basis of the parallelism
#. capture the critical sharing and coordination

We start from the perspective of models that have achieved high scalability and efficiency in large-scale parallel machines, working from simple cases to more complex ones. Global data structures which are read- only can also be important for large-scale parallelism, and their optimization can be performance critical. However, because they are simpler from a consistency and update point-of-view, we neglect them here.

A. Static, Regular Data Decomposition, Owner Computes
-----------------------------------------------------
This model includes simple stencil, finite differences, SOR, etc. Computations with and without ghost regions. The regular data is decomposed for parallelism and remains in that configuration. The basic computational steps are:

#. Copy read set for computation to local 
#. Compute (w/o communication)
#. Update write set locally (owner computes) 4. Synchronize globally (or with neighbors)

B. Static Irregular Data, Data Decomposition, Owner Computes
------------------------------------------------------------
This model includes iterative methods on sparse structures. For example, irregular finite element graphs, sparse matrix iteration, irregular meshes, and includes specific examples such as NEK and OpenMC (codes important to CESAR). The irregular data is intelligently decomposed for parallelism, perhaps by a sophisticated graph-partitioning algorithm, and remains in that configuration. The basic computational steps are:

#. Copy read set for computation to local (irregular) 
#. Compute (w/o communication)
#. Update write set locally (owner computes)
#. Synchronize globally (or with neighbors)

C. Irregular, Predictable Computation across Data (regular or irregular)
------------------------------------------------------------------------
This model includes adaptive, iterative methods on sparse structures. For example, adaptive finite element graphs, adaptive irregular meshes, multigrid, and sophisticated n-body algorithms such as FMM and Barnes- Hut. The irregular data is intelligently decomposed for parallelism, perhaps by a sophisticated graph- partitioning algorithm. This decomposition may be a compromise across multiple phases (e.g. with multigrid or FMM), and after a number of iterations may be reorganized for load-balance/parallelism. The basic computational steps are:
#. Copy read set for computation to local (irregular) 
#. Compute (w/o communication)
#. Update write set locally (owner computes)
#. Synchronize globally (or with neighbors)
#. Check balance, and if necessary rebalance

D. Irregular, Unpredictable Computation across Data (regular or irregular)
--------------------------------------------------------------------------
This model includes unpredictable computational structures such as direct solvers. The challenge for these models is that the simpler data decompositions do not work well because either the relationship between data and computation are unpredictable (even if there is little), and the parallelism is irregular and dynamic, requiring ongoing rebalancing of computation across the machine. Typical structures used to manage are task parallelism, and a range of dynamic, adaptive load-balancing techniques. Here there are two classes of problems global data structures for managing the irregular parallelism (task queues, etc.) and to manage the shared data. Examples that fit here are NWChem Tensor Contraction Engine (TCE) that lies behind dozens of single- and multi-reference coupled-cluster methods such as CCSD.
The elements of managing parallelism include:

#. Creation of tasks (enqueue)
#. Creation of workers
#. Claiming of tasks for execution
#. Completion of tasks and registration
#. Load-balancing (work-stealing, etc.)
#. Detecting termination of the phase (if well-defined)

Within each task, each worker typically undertakes the following steps with respect to the parallel work and shared data:

#. Copy read set for computation from global to local (regular or irregular)
#. Compute
#. Add tasks to the work pool(s)
#. Register completion and write results to global structures (regular or irregular)

What are important patterns of data sharing on the read/write sets?

E. Irregular, Unpredictable Computation across rapidly changing data (global shared memory)
-------------------------------------------------------------------------------------------
The majority of computations at ALCF (and presumably other high-end computing centers)are dominated by classes A, B, and C. Computational chemistry methods are also an important class, so if they fall into category D, then all four of A-D are required for good coverage.

Lots of the new computing models for irregular problems are attacking things that are in category E. What fraction of the interesting scalable computations do these uncovered correspond to?

Case Studies: general computation and multi-version
===================================================
Note: All algorithms are written from the perspective of a single process. This process has the index "me".

Case A 1: Multidimensional Finite Difference
--------------------------------------------
Assume that, for each process i, we are given D(i), which is a function that returns the set of global view data object indices required to calculate the gradient on dimension i. Also, we are given nabla(i, values), which is a function that returns the gradient on dimension i, given the value of the global view data object elements specified by D(i).

Implicit
````````
::

  for time := 0 to T-1 step h do
    GDS_get(grad_data_buf, D(me), ..., gds_grad)
    GDS_get(position_buf, me, ..., gds_position)
    my_grad := nabla(me, my_data)
    position_buf += my_grad
    GDS_fence(gds_position)
    GDS_put(position_buf, me, ..., gds_position)
    GDS_fence(gds_position)
  end for

Monotonic Implicit
``````````````````
::

  for time := 0 to T-1 step h do
    GDS_get(grad_data_buf, D(me), ..., gds_grad)
    GDS_get(position_buf, me, ..., gds_position)
    my_grad := nabla(me, my_data)
    position_buf += my_grad
    GDS_put(position_buf, me, ..., gds_position)
    GDS_version_inc(gds_position, 1, ...)
  end for

Case A 2: PGAS Matrix Addition
------------------------------
The PGAS program::

  shared int A[N][N], B[N][N], C[N][N]
  ...
  forall i := 0 to N-1 do
    for j := 0 to N-1 do
      C[i][j] := A[i][j] + B[i][j]
    end for
  end forall

Is equivalent to the GVR::

  GDS_gds_create([N,N], GDS_INT, GDS_WORLD, gds_A)
  GDS_gds_create([N,N], GDS_INT, GDS_WORLD, gds_B)
  GDS_gds_create([N,N], GDS_INT, GDS_WORLD, gds_C)
  ...
  GDS_get(my_A, [me, [0, N-1]], ..., gds_A)
  GDS_get(my_B, [me, [0, N-1]], ..., gds_B)
  for j := 0 to N-1 do
    my_C[j] := my_A[j] + my_B[j]
  end for
  GDS_put(my_c, [me, [0, N-1]], ..., gds_C)
  GDS_fence(gds_C)
  /* free gds_A, gds_B, and gds_C */
  ...

Case B: Iterative Method (SD) on a Sparse Matrix
------------------------------------------------
We iteratively solve Ax = b for x.

    We use two global view data objects:
    The first, gds x, stores the current approximate solution.
    The second, gds r, is used to store the residual for the reduction step.
    Assume there exists a function partition(i), which, given a process i, returns the set of rows of our global view data object that process i will be responsible for updating. It may also copy said rows to storage local to process i.
    Let A(rows) and b(rows) return the specified rows of A and b, respectively.
    Let cols(rows) return the set of columns for which at least one entry is nonzero given a set of rows. Let norm1(gds) return the Manhattan norm of gds.

Implicit
````````
::

  my_rows := partition(me) 
  repeat
    GDS_get(resid_data_buf, cols(my_rows), ..., gds_r)
    GDS_get(position_buf, my_rows, ..., gds_x)
    my_r := A(my_rows) * position_buf - b(my_rows)
    my_grad := STEP_SIZE * 2 * transpose(A(my_rows)) * my_r
    position_buf -= my_grad
    GDS_fence(gds_x)
    GDS_put(position_buf, my_rows, ..., gds_x)
    GDS_fence(gds_x)
    GDS_put(my_r, my_rows, ..., gds_r)
    GDS_fence(gds_r)
    norm = norm1(gds_r) / NUM_ROWS
  until norm < TOL

Monotonic Implicit
``````````````````
::

  my_rows := partition(me)
  repeat
    GDS_get(resid_data_buf, cols(my_rows), ..., gds_r)
    GDS_get(position_buf, my_rows, ..., gds_x)
    my_r := A(my_rows) * column_buf - b(my_rows)
    my_grad := STEP_SIZE * 2 * transpose(A(my_rows)) * my_r
    position_buf -= my_grad
    GDS_put(position_buf, my_rows, ..., gds_x)
    GDS_version_inc(gds_x, 1, ...)
    GDS_put(my_r, my_rows, ..., gds_r)
    GDS_version_inc(gds_r, 1, ...)
    norm = norm1(gds_r) / NUM_ROWS
  until norm < TOL

Case C: Parallel Barnes-Hut in 1 dimension
------------------------------------------
Assume we have a set of particles labeled 0 to N-1.

  In this example, we have four global view data objects:
  The first, gds m, is a vector of length N, which signifies the mass of each particle. This array is constant, and never needs synchronization, although it does need to be globally available.
  The second, gds x, is a vector of length N, which signifies the position of each particle.
  The third, gds tree, is a matrix with a number of rows on the order of NlogN. This array signifies the structure of trees through some unspecified means.
  The fourth, gds info, has the same number of rows as gds tree, and specifies each node’s center of mass, total length, and total mass.
  We define a function partition(i), which returns the particles for which process i is responsible.

Implicit
````````
::

  /*
   * Calculate initial values for:
   *   my_particles, mass_buf, position_buf, tree_buf, gds_m, gds_x, and 
   *   gds_tree.
   */
  ...
  for time := 0 to T-1 step h do
    /*
     * Calculate local interactions of particles in my_particles using values 
     * stored in local buffers.
     */
    info_buf := calculate_local_info(my_particles, position_buf, mass_buf, 
                tree_buf)
    GDS_put(info_buf, get_local_info_bounds(my_particles), ... , gds_info)
    GDS_fence(gds_info)
    current_node := parent(local_clusters_node)
    while current_node != root do
      GDS_get(info_buf, info_pertaining_to(current_node), ..., gds_info)
      info_buf := calculate_node_info(current_node, info_buf)
      GDS_fence(gds_info)
      GDS_put(info_buf, info_pertaining_to(current_node), ..., gds_info)
      GDS_fence(gds_info)
      current_node := parent(current_node)
    end while
    /*
     * Calculate global interactions of all particles in my_particles using 
     * calls to GDS_get(gds_tree), and calls to GDS_get(gds_info).
     */
    ...
    /*
     * Mutate position_buf based on calculated forces.
     */
    ...
    if repartition_is_required() then
      GDS_put(position_buf, my_particles, ..., gds_x)
      GDS_fence(gds_x)
      my_particles := partition(me)
      GDS_get(mass_buf, my_particles, ..., gds_m)
      GDS_get(position_buf, my_particles, ..., gds_x)
      tree_buf := calculate_local_tree(my_particles, position_buf, mass_buf)
      GDS_put(tree_buf, get_local_tree_bounds(my_particles), ..., gds_tree)
      GDS_fence(gds_tree)
      while global_root_node_does_not_exist() do
        GDS_get(tree_buf, current_least_deep_nodes, ..., gds_tree)
        merge_with_neighbor(tree_buf)
        GDS_fence(gds_tree)
        GDS_put(tree_buf, current_least_deep_nodes, ..., gds_tree)
        GDS_fence(gds_tree)
      end while
      GDS_fence(gds_info)
    end if
  end for

Monotonic Implicit
``````````````````
::

  /* 
   * Calculate initial values for: 
   *   my_particles, mass_buf, position_buf, tree_buf, gds_m, gds_x, and 
   *   gds_tree.
   */
   ...
   for time := 0 to T-1; step h do
    /*
     * Calculate local interactions of particles in my particles using values 
     * stored in local buffers.
     */
    info_buf := calculate_local_info(my_particles, position_buf, mass_buf, 
                tree_buf)
    GDS_put(info_buf, get_local_info_bounds(my_particles), ..., gds_info)
    GDS_fence(gds_info)
    current_node := parent(local_clusters_node)
    while current_node != root do
      GDS_get(info_buf, info_pertaining_to(current_node), ..., gds_info)
      info_buf := calculate_node_info(current_node, info_buf)
      GDS_put(gds_info, info_pertaining_to(current_node), info_buf)
      GDS_version_inc(gds_info, 1, ...)
      current_node := parent(current_node)
    end while
    /*
     * Calculate global interactions of all particles in my_particles using 
     * calls to GDS_get(gds_tree), and calls to GDS_get(gds_info).
     */
    ...
    /*
     * Mutate position_buf based on calculated forces.
     */
    ...
    if repartition_is_required() then
      GDS_put(position_buf, my_particles, ..., gds_x)
      GDS_fence(gds_x)
      my_particles := partition(me)
      GDS_get(mass_buf, my_particles, ..., gds_m)
      GDS_get(position_buf, my_particles, ..., gds_x)
      tree_buf := calculate_local_tree(my_particles, position_buf, mass_buf) 
      GDS_put(tree_buf, get_local_tree_bounds(my_particles), ..., gds_tree) 
      GDS_fence(gds_tree)
      while global_root_node_does_not_exist() do
        GDS_get(tree_buf, current_least_deep_nodes, ..., gds_tree) 
        merge_with_neighbor(tree_buf)
        GDS_put(tree_buf, current_least_deep_nodes, ..., gds_tree) 
        GDS_version_inc(gds_tree, 1, ...)
      end while
      GDS_fence(gds_info) 
    end if
  end for

Case D: 1-Dimensional DMC Task-Oriented Implicit
------------------------------------------------
::

  x := me * WIDTH/nprocs - WIDTH/2 
  task_collection_create(GDS_WORLD, tc) 
  for t := 0 to T-1 step TIMESTEP do
    for i := 0 to N-1 do
      x’ := random_number_in_range(-WIDTH/2, WIDTH/2)
      y’ := random_number_in_range(0, HEIGHT)
      task_create(t)
      /* Register gdss to which get will be applied */
      t.get_gdss := [gds_psi] 
      /* Bounds of gdss specified in previous line */
      t.get_bounds := [segment containing(x’)] 
      /* Register gdss to which acc will be applied */
      t.acc_gdss := [gds_sum] 
      /* Bounds of gdss specified in previous line */
      t.acc bounds := [me]
      t.func_to_exec := LESS_THAN_INTEGRAND
      t.func_args := (me, x, x’, y’)
      t.affinity := me
      task_enqueue(t, tc)
    end for
    task_collection_sync(tc) 
    /* 
     * Either ensures that all tasks currently in the collection are complete, 
     * or puts the burden on the scheduler to ensure that the result of the 
     * computation is identical to the result that would have occured if all 
     * the tasks in the collection had been complete at this point 
     */
    GDS_get(sum, me, ... , gds_sum)
    area := (sum / N) * WIDTH * HEIGHT 
    GDS_put(gds_psi, me, area)
    GDS_version_inc(gds_psi, 1, ...)
  end for

With auxiliary function::

  function LESS_THAN_INTEGRAND([gds_psi], [gds_sum], (me, x, x’, y’))
    GDS_get(psi_vals, segment_containing(x’), ..., gds_psi) 
    psi_val := interpolate(psi_vals)
    if y’ < G(x, x’) * psi_val then
      GDS_acc(gds_sum, me, 1, GDS_SUM) 
    end if
  end function

Case studies: Error Handling Scoping
====================================
GVR allows applications to create error recovery handlers.  In general, these
error handlers (EH),

#. EH’s need access to some application state for recovery
#. EH’s must be visible to the GVR error handling dispatch to be called

So, the critical question is the relationship of the “scopes” of the error
recovery state (ERS) and the error handler (EH).

There are several common solutions:

#. Case 1: define ERS and EH globally, allowing the EH to access the required state. This approach requires changing the visibility in the program, thus changing program structure.
#. Case 2: define variable that hold pointers to the ERS in the global scope (root or dictionary) that an EH can reference and then extract and modify the
required recovery state. This approach requires minimum changing of program.

Note that, In GVR, the ERS is typically GDS’s, thereby providing access to a
full set of versions. It is the responsibility of the programmer to ensure that
the ERS stays accessible (not freed explicitly or automatically) and up to
date. If the ERS is a GDS, the version system makes this relatively simple.

In ddcMD, the application has a collection of global distributed data structures. The program utilizes GDS global arrays to store data. In simulation procedure, data are captured in versions periodically.

Programmers define a global error handler `recovery_func()` with handles application semantic erros and can be generalized to memory errors. The handler is registered with the global data structures, i.e., the data is globally visible to the error handler. When errors are captured by error checks, the application sends error information to GVR, which therefore invokes the global error handler to restore data.
An example of case 1. 

::
    
  /* ddcMD example for scoping error handler */

  /* Global data structure */
  struct state {
    GDS_gds_t _position;
    GDS_gds_t _velocity;
    GDS_gds_t _domain;
    GDS_gds_t _time;
    ...
  } State;

  /* User defined error handler */
  recovery_func(gds, error_descriptor) {
    GDS_get(local_data_structure, gds);
    GDS_resume_global(gds, error_desc);
  }

  main() {
    /* Create global array data structure */
    GDS_alloc(State._time);
    GDS_alloc(State._position);
    ...

    /* Register the specific error handler */
    GDS_register_global_error_handler(State._position, recovery_func);

    /* Molecular Dynamics Simulation Loop */
    simulation_loop() {
    /* Actual computation work */
    computation();

    /* Error detection */
    error = detection_func();
    /* Error handling */
    if (error) {
      /* Create error descriptor for the error */
      error_descriptor = GDS_create_error_descriptor();
      /* Raise the global error */
      GDS_raise_global_error(gds, error_descriptor);
      continue;
    }
    GDS_put(local_data_structure, State._position);
    ...
    /* Take snapshot of correct states */
    if (snapshot point) {
      GDS_version_inc(State._position);
      ...
    }
  }
}


An example of case 2,

::

  // Pointers to state used for recovery
  ERS_global_dictionary {
    void* recovery_state_1;
    void* recovery_state_2;
  };

  main_user_program(){
    float* my_3d_array [1024][256][128];
    gds* my_versioned_gds = gds_alloc(...);

    register_error_handler(my_error_handler...);

    // Store pointers in global recovery state
    ERS_global_dictionary->recovery_state_1 = my_3d_array;	
    ERS_global_dictionary->recovery_state_2 = my_versioned_gds;

    // Do the real Computation
    ...my_versioned_gds...
    ...my_3d_array...
    ...signal an error that leads to error handler invocation...
    ...after handler recovers error, go on with the
    computation...
  }
  
  // User-defined error handler
  my_error_handler(state) {
    float* temp1 = ERS_global_dictionary->
    recovery_state_1;
    gds* temp2 = ERS_global_dictionary->
    recovery_state_2;
    ...do the recovery using temp1, temp2, state
    ...
  }

Case studies: error check, signaling, and recovery
==================================================
We list two case studies to present the local/global error check, signaling, and recovery.

miniFE
-------
::

  function MAIN
    /* create A and b */
    ...
    /* initialize x */
    ...
    GDS_alloc(..., &gds_x)

    /* Define my own error attribute */
    attr_name = "LOCAL_BUFFER_KEY"
    GDS_define_error_attr_key(attr_name, strlen(attr_name),
        GDS_EAVTYPE_INT, &LOCAL_BUFFER_KEY)

    /* Create error matching predicate */
    GDS_create_error_pred(&pred);
    GDS_create_error_pred_term(LOCAL_BUFFER_KEY,
        GDS_EPRED_ANY, /* Any attribute value will be allowed */
        0, NULL, /* No additional parameter for this predicate */
        &term)
    /* Add a term to the predicate */
    GDS_add_error_pred(pred, term)
    GDS_free_error_pred_term(&term)

    /* Register local error handler */
    GDS_register_local_error_handler(gds_x, pred, reload)

    GDS_free_error_pred(&pred)

    return iter_solve(A, b, x, gds x)
  end function

  function reload(gds, err_desc) /* Reload the previous version */
    GDS_get_error_attr(err_desc, LOCAL_BUFFER_KEY, local_buffer, ...) 
    GDS_descriptor_clone(gds)
    GDS_move_to_prev(gds_copy)
    GDS_get(local_buffer, ..., gds_copy)
    GDS_resume_local(gds)
  end function

  function iter_solve(A, b, x, gds_x)
    repeat
      old_normr := normr
      r := Ax-b
      normr := norm(r)
      if (old_normr - normr) / old normr > TOL_1 then
        /* Initialize err_desc */
        GDS_create_error_descriptor(&err_desc)
        GDS_add_error_attr(err_desc, LOCAL_BUFFER_KEY, x, sizeof(x))
        /* Trigger the local error handler */
        GDS_raise_local_error(gds_x, err_desc)
        /* err_desc will be freed by GDS_resume_local() */
        do_necessary_recalculation() 
      end if
      if iteration % CP_INTERVAL == 0 then
        /* Make a snapshot */
        GDS_put(x, ..., gds_x)
        GDS_version_inc(gds_x, ...)
      end if
      x := do_calculation(A, b, x) 
    until normr < TOL_2
    return x
  end function

miniMD
--------
::

  function MAIN 
    GDS_alloc(GDS_PRIORITY_HIGH, &gds)
    /* Initialize predicate */
    ...
    /* Register global error handler */
    GDS_register_global_error_handler(gds, pred, rollback)
    /* Register global error check for rollback */
    GDS_register_global_error_check(gds, full_check) 
    return iter_computation()
  end function

  /* Rollback to the correct version */
  function rollback(gds, err_desc)   
    GDS_move_to_prev(gds)
    while GDS_check_global_error(gds) != OK do
      GDS_move_to_prev(gds)
    end while
    GDS_get(atoms, gds)
    GDS_resume_global(gds, err_desc)
  end function
    
  function iter_computation
    repeat
      do_comp_and_comm(atoms)
      if atoms_out_of_box(atoms) then
        /* Initialize error descriptor */
        GDS_create_error_descriptor(&global_error_desc)
        GDS_add_error_attr(global_error_desc, ...)
        ...
        /* Raised global error will be triggered at the next
           stable point (i.e. GDS_version_inc or GDS_fence) */
        GDS_raise_global_error(gds, global_error_desc)
      end if
      GDS_put(atoms, gds)
      GDS_version_inc(gds)
    until converge()
  end function 
