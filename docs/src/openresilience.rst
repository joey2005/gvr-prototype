***********************
Open Resilience
***********************

Introduction
==============

A computer system may encounter various kinds of errors during execution.
Process or node crash is a typical example of such errors but also there can be a lots of other errors like data corruption in memory, disk, or network transaction.
In addition to the large variety of the kinds of errors, errors come from various different layers.
Errors could happen in and be detected by hardware, operating system, middleware for I/O or communication, numerical libraries, or applications.
So in order to effectively deal with the real system, we need a framework to treat various kinds of errors from various layers.

One requirement for such a comprehensive error handling is a **cross-layer error handling**, which means multiple components in different layers coordinate together to handle errors. This is important because generally lower layer (e.g., hardware or operating system) is agnostic to higher-level (e.g., application) knowledge about data structures or properties, whereas these higher-level knowledge is a key for efficient error checking and recovery.

.. figure:: or_traditional.png
   :scale: 50 %
   :alt: Traditional approach

   A traditional cross-layer error handling approach.

Traditional way of dealing with these many different errors in the cross-layer manner is the "siloed" approach, in which each error is tightly coupled with a specific error handler.
Under an assumption that there are so many different types of errors,
such approach is prohibitively expensive in terms of development effort.
It also prohibits any reuse or generalization of existing error handlers. If an error handler is tightly coupled with a certain kind of error, it can not handle another kind of error which is quite similar to the originally expected one.
In fact, some sort of errors can be treated in a similar way. For example, data corruption error can be recovered by a single handler regardless of the actual source of an error.
This kind of **generalization** of an error handler can maximize the return of an investment to the development of error handlers.
This will also encourage hardware/software vendors to invest more on rich error reporting.
On the other hand, if a developer wants to have a new error handler very specific to a particular type of error events, one should be able to add the new one without removing or changing an existing, more generic handler. In other words, there's also a need for **specialization**.

.. figure:: or_openresilience.png
   :scale: 50 %
   :alt: Open Resilience approach.

   The Open Resilience approach for error signaling & handling.

GVR implements the **Open Resilience** framework to enable and encourage a synergy between error detecting side and error handling side, through the **unified error signaling & handling interface**.
This section describes a general ideas and designs of the Open Resilience framework in GVR,
followed by several motivating use case examples.

Design
======

Under Open Resilience, **error detectors**, which detect and report errors, and **error handler**, which receive error signals and handle errors, are first decoupled.
Then GVR dynamically glues an error detector and an error handler through its unified interface, based on template matching of an error to handler.
When an error detector detects an error, it signals an error by raising an error through the GVR interface.
GVR looks up the most appropriate error handler among the registered error handlers, then invokes the chosen one.
This approach improves composability and generality of error handlers, thus increasing leverage of error reporting and error handlers.

Error Attributes and Descriptor
-------------------------------------

An error needs several descriptive information, such as the size and location of a corrupted region or a list of failed processes.
Errors in GVR are represented as a set of error attributes.
Each error attribute is a pair of a key and a value.
Some common error attributes are pre-defined by GVR but users can also define a new one.

A set of error attributes is called **error descriptor**.
An error descriptor is a core object on error handling which is passed along different components to describe the details about an error.
An error descriptor is created by `GDS_create_error_descriptor`, followed by `GDS_add_error_attr` to add error attributes.

Error Signaling and Handling
---------------------------------

Local and Global Error Signaling & Handling
''''''''''''''''''''''''''''''''''''''''''''

.. figure:: or_signaling_handling.png
   :scale: 50 %
   :alt: Global/local error signaling and handling.

   Global/local error signaling and handling.

Some errors can be handled locally only by one process, while others may require coordinated error handling with multiple processes.
In order to capture this difference, GVR introduces a notion of **local and global error signaling & handling**.
Local error handling involves only one process, whereas global error handling involves all the processes in a group. The group usually is a set of users of one GVR array. If an error is not associated with any array, the group consists of all processes in the application.

Signaling Errors
''''''''''''''''
When an error detector find an error, it first generates an error descriptor and store available information regarding the error as a set of error attributes.
Then it raises an error using either of `GDS_raise_local_error` or `GDS_raise_global_error`, with passing the error descriptor.

Handling Errors
'''''''''''''''
Errors are handled by error handlers. An error handler is a function which takes an error descriptor as an argument.
An error handler is registered to a GVR array via either `GDS_register_local_error_handler` or `GDS_register_global_error_handler`.
Each array can have multiple error handlers registered.
When an error is raised by an error detector, GVR looks up the most appropriate error handler and invokes it.

Library-invoked Error Checking
''''''''''''''''''''''''''''''
.. figure:: or_checking.png
   :scale: 50 %
   :alt: Global/local error checking.

   Global/local error checking.

A user can register his or her own error checking functions to GVR using `GDS_register_local_error_check` or `GDS_register_global_error_check`.
GVR will invoke these functions based on at certain timing based on **error checking priority**.
As in error signaling and handling, error checking also has a notion of local and global error checking.
Local error checking is run by a single process whereas global error checking involves all processes which use a GVR array.
Global error checking function is invoked in each process in a synchronized manner.

Matching between Errors and Error Handlers
-------------------------------------------------

Upon error event, GVR chooses the most appropriate error handler for a given error descriptor.
This is because if multiple handlers were called, these handlers must have been designed carefully to make sure that they would not interfere each other, thus loosing generality and flexibility.

In order to choose the best handler automatically, each handler expresses its capability on error handling.
More specifically, each handler is associated with an **error attribute predicate** to characterize what kind of error attributes it expects/handles.
A predicate expresses a constraint on acceptable error descriptors.

A predicate is a set of terms (rules), where each term looks like one of the following.

1. **<KEY:*>**: An error attribute key `KEY` must appear in the descriptor, but its associated attribute value can be anything.
2. **<KEY:VAL>**: An error attribute key `KEY` must appear in the descriptor with a value `VAL`.
3. **<KEY:MIN-MAX>**: An error attribute key `KEY` must appear in the descriptor, and the value must be within the range between `MIN` and `MAX`.
4. **<KEY:!>**: An error attribute key `KEY` must *not* appear in the descriptor.

.. figure:: or_matchexample.png
   :scale: 50 %
   :alt: Matching example.

   A matching example of errors and error handlers.

The above figure shows an example of matching between error descriptors and error handlers.
When error descriptor A is raised, it can match with all the error handlers in the right-hand side,
because all the rules in each handler are satisfied with the descriptor A.
Then GVR has to choose the best one among these three.
GVR regards the one with the most specific match as the best match.
In this case `handler_1` has the most specific rules among three, so it will be invoked for the descriptor A.

To register an error handler, first a user has to prepare an error predicate.
An error predicate object is created by `GDS_create_error_pred`.
Then a user can call `GDS_create_error_pred_term` to create terms which correspond to matching rules.
`GDS_add_error_pred_term` adds a term to an existing predicate object.
Once a predicate object becomes ready, it will be passed to either of `GDS_register_local_error_handler` or `GDS_register_global_error_handler` with a function pointer to the handler.

Application Lifecycle
------------------------

.. figure:: or_workflow.png
   :scale: 50 %
   :alt: A GVR application lifecycle.

The above figure shows the typical application lifecycle regarding error handling.
During the normal run (top left, "Running"), an application performs computation and communication. Also it stores critical data to a GVR array and creates versions occasionally.

During program execution, several error checks will be performed (bottom left, "Error Check").
Some checks are done by application itself, while others may be performed by other layers, such as hardware, operating system, communication middleware, numerical library, or GVR library.
Once an error is found, an error is raised through GVR's unified error signaling interface.
This error event (descriptor) is queued inside GVR and delivered to an appropriate handler at the next available timing (bottom right, "Error Pending").
For local errors, queued error will be delivered at the next GDS function, where as for global errors, queued error will be delivered at the next synchronization point (*i.e.* `GDS_fence` or `GDS_version_inc`).

Once an error descriptor is delivered to an error handler, it performs an error recovery based on its own strategy (top right, "Error Recovery").
During the recovery the handler can utilize data stored in the GVR array, not only in the latest version but also in an old version.
Once a recovery is done, the handler calls `GDS_resume_local` or `GDS_resume_global` to tell GVR that error handling is done.

Example Scenarios
==================

.. figure:: or_chomboexample.png
   :scale: 60 %
   :alt: Error handling scenario

   An error handling example scenario.

This figure shows some examples of generalization of error handlers under the Open Resilience error handling framework.
The first example is the low-level memory error handler that handles a data corruption in a small region in a GVR array. This is a local error handler, meaning that it doesn't coordinate with other processes.
It can handle two kinds of errors, one is a local memory error originally detected by a hardware and signaled through OS and GVR, and the other is a data corruption error detected by an application itself, using checksum for example. While these two errors come from different error detectors and possibly different causes, the handler can treat them in the same manner because both errors describes the corrupted location information with the same attributes, `GDS_EATTR_GDS_INDEX` (describes the location of the corruption) and `GDS_EATTR_GDS_COUNT` (describes the size of the corrupted region).

The second example shown here is the handler which performs recomputation or approximation to recover array data for data loss.
The first use case of this handler is a case where the low-level memory error handler in the previous example figures out that it requires global coordination to complete the recovery.
The second case is an MPI communication failure, typically caused by process or node crash.
Both errors manifest as a data loss in a certain region, therefore this handler is able to handle both errors in the same fashion.
