************************************************
Release Notes
************************************************

What is GVR?
============

GVR (Global View Resilience) is a user-level library that enables portable, efficient, application-controlled resilience.  The primary target of GVR is HPC applications that require both extreme scalability and performance as well as resilience.  GVR's key approaches include independent versioning of application arrays, efficient partial or whole restoration, open resilience to maximize the number of errors that can be handled (minimize fail-stop occurrences).  Application knowledge can be exploited to control overhead, maximize error coverage, and maximize recoverable errors.

More information of GVR project is available at http://gvr.cs.uchicago.edu/

GVR has been developed by University of Chicago and Argonne National Laboratory, under the lead of Prof. Andrew A. Chien and Dr. Pavan Balaji. It has been supported by the U.S. Department of Energy, Office of Science / ASCR under awards DE-SC0008603/57K68-00-145.

Features
--------------

* Portable application-controlled resilience and recovery with incremental code change
* Versioned distributed arrays with global naming (a portable abstraction)
* Reliable storage of the versioned arrays in memory, local disk/SSD, or global file system
* Whole version navigation and efficient restoration
* Partial version efficient restoration (incremental "materialization")
* Independent array versioning (each at its own pace)
* Open Resilience framework to maximize cross-layer error handling

  * application-defined error handling
  * unified application and system error descriptors
  * attribute based composition for easy extensibility at application, operating system, and hardware levels
* C native APIs and Fortran bindings

Software and Platform Requirements
------------------------------------------------

* Requires only an MPI library which is compatible with MPI-3 standard.
* Standard "autotools" preparation
* Requires no root privilege
* Runs on several platforms including x86-64 Linux cluster, Cray XC30 and IBM Blue Gene/Q

.. * Process/node crash tolerance

What's new in this version
==========================

1.0.1
-----

* Implemented the "doorbell" waiting scheme for target server thread. Set an environment variable ``GDS_WAIT_SCHEME`` to ``3``.

    * Now this is the default scheme for x86-based systems. This scheme does not support Blue Gene Q platform at this momemnt.

* Minor bug fixes and performance improvements.

1.0.0
-----

* Introduced new Open Resilience error signaling & handling framework
* Implemented persistent data support, both in memory and on persistent storages (via the Scalable Checkpoint/Restart library)
* In addition to Linux/x86, also supported Cray XC30 and Blue Gene/Q platforms.
* Implemented several APIs

  * Implemented `GDS_create` function.
  * Implemented `GDS_get_acc` function.
  * Implemented `GDS_compare_and_swap` function.
  * Implemented `GDS_access`, `GDS_get_access_buffer_type`, `GDS_release` functions.
  * Supported `label` and `label_size` arguments in `GDS_version_inc` function.

* Updated documentation and added program examples.

0.9.1
-----
* Sub-communicators (communicators which are subset of `MPI_COMM_WORLD`) are now supported in `GDS_alloc`.

0.9.0
-----
* Removed dependency to CUnit from test suite.
* Size type of a communicator became `int` (originally `size_t`).
* Implemented `GDS_get_comm` function.
* Implemented `GDS_compare_and_swap` function.
* Implemented parameter sanity checking for basic functions. Set an environment variable `GDS_SANITY_CHECK` to 1 to activate.

0.8.2-rc0
---------
* Introduced sleep scheme switching infrastructure (`GDS_WAIT_SCHEME`)
* Fixed unnecessary memory consumption around dynamic tag system
* Target buffer is now zero-cleared on allocation
* Several improvements to log-structured array

    * `GDS_acc` support

0.8.1-rc0
---------
* Added log-structured array implementation
* Several performance improvements in the UChicago RCC Midway cluster (MVAPICH2-2.0b + Infiniband).

0.8.0
-------------
* Added Fortran APIs, and related documentation.
* Add `GDS_register_global_error_check` and `GDS_register_local_error_check`, and the test case.
* Separate the error descriptor enqueue function from raise error function.
* Supported `min_chunks` argument in `GDS_alloc`.
* Supported self version description. To show the version of the GVR library, set the environment variable `GDS_SHOW_INFO` to 1.

0.7.1
-----
* Fixed a bug that `tests/dprint.h` was not included in the release tarball.


0.7.0
-------------------
* Add local error polling and handling for the APIs including `GDS_get/put`, `GDS_acc`, `GDS_wait`, `GDS_fence`, `GDS_descriptor_clone`, and `GDS_version_inc`.
* Set a flag of recovery mode. If the flag is on, they will not reentrant the error handling function.
* For each GDS object, global error recovery functions are invoked one by one when `GDS_fence` is called with `NULL`.

0.6.0
--------------------

* Implements API changes in GVR API 0.7.3 and 0.7.4

    * Supports both row-major ordering and column-major ordering for multi-dimensional arrays. Default ordering for C binding is now row-major (same as in GA and C language itself).
    * Supports functionality to create error descriptor and error category.

* Supports functionality for global error handling, including global error handler registration, raising global error, and invoking global error handler.
* Supports functionality for local error handling, including local error handler registration, raising local error, and invoking local error handler.
* Array accessing primitives (get/put/acc) now become non-blocking, as defined in the API document. This may break some existing applications which do synchronize properly.
* New documentation system

0.5.3
-----

* Fixed a misconfiguration that internal header files were missing from the tarball

0.5.2
-----

* Fixed a bug that led to data corruption when restoring data bigger than 4KB from an old version (Issue #18)

0.5.1
-----

* Fixed a bug that `gds.h` included unnecessary file, which led to a build failure

0.5
---

* Initial internal milestone
* Basic Functionality including ability to create global data structures, put/get, create versions, signal and handle errors, subject to restrictions listed below
* Implements GVR API version 0.72

Prerequisites
=============

To install and use the GVR library, you need to have the following things properly installed and configured.

* Standard software development tools such as `make` or `gcc`. In Ubuntu, you can install them by doing `sudo apt-get install build-essential`.
* MPI library which supports MPI 3.0 standard. The `mpicc` command must be found in a directory included in the `PATH` environment variable.

We have been testing GVR on the following platforms.
* Ubuntu 12.04 AMD64, GCC 4.6.3, and MPICH 3.1.
* Midway at University of Chicago RCC (Scientific Linux 6.5, GCC 4.8.2, MVAPICH2-2.1a)
* Edison at NERSC (Cray XC30)
* Vesta at Argonne National Lab. (IBM Blue Gene/Q)

How to build & install
======================

GVR can utilize LRDS (Local Reliable Data Storage) as an external memory error event reporter. Use of LRDS is optional. In order to configure GVR to use LRDS, first build and install LRDS.

Then, the installation process of GVR is straightforward by,

::

    $ ./configure [OPTIONS]
    $ make && make install

Some important parameters for the ``configure`` script:

* ``--prefix=`` specifies the installation target directory under which several sub-directories such as ``include/``, ``bin/``, and ``lib/`` are created.
* ``--with-lrds=`` specifies the installation directory for LRDS. If you specified a directory to ``--prefix`` when configuring LRDS, specify the same directory here. If ``--with-lrds`` is not specified, GVR will be configured and built not to use LRDS.

Platform support: Blue Gene/Q
-----------------------------
On BG/Q machine, to build GCC toolchain and latest MPICH, please follow the instructions at https://wiki.mpich.org/mpich/index.php/BGQ.

To build GVR, specify host as `powerpc64-bgq-linux` during the configuration.

::

    $ ./configure --host=powerpc64-bgq-linux [OPTIONS]

Platform support: Cray XC30
---------------------------
On Cray machine, to build GVR, specify host as `x86-cray-linux` during the configuration.

::

    $ ./configure --host=x86-cray-linux [OPTIONS]

Optional library: SCR
---------------------

GVR supports persistent data with SCR (Scalable Checkpoint Restart) library (http://sourceforge.net/projects/scalablecr/) developed by Lawrence Livermore National Laboratory
(https://computation-rnd.llnl.gov/scr/). GVR utilizes SCR to store data across
storage hierarchy (local file system, parallel file system, etc.), therefore provides stronger resilience towards severe failure cases such as process failures.

SCR installation is optional. GVR works with latest SCR version 1.1-7. To install SCR, please follow the install instructions inside the package.

Install GVR with SCR:

::

    $ ./configure [OPTIONS] --with-scr=$(SCR_PATH)
    $ make && make install

Before running GVR programs with SCR, you need to configure SCR. There are
several environmental parameters.

* `SCR_CACHE_BASE` specifies the location in local file system for storing data. Please make sure there are enough space for storage.
* `SCR_PREFIX` specifies the location in shared file system for storing data.
* `SCR_CONF_FILE` specifies the detail configuration for SCR utilization.
* `SCR_JOB_ID` specifies the unique job number. Each instance of program should be associated with a unique number.
* `FLUSH_to_SCR` specifies the frequency to flush data to SCR.

Following example shows a configuration file and environmental variables settings for SCR.

::

    $ cat myscr.conf
    SCR_FLUSH=3
    CACHEDESC=0 BASE=/tmp SIZE=12
    SCR_COPY_TYPE=LOCAL
    SCR_PREFIX=/home/DIR
    SCR_LOG_ENABLE=0

    $ export SCR_CONF_FILE=myscr.conf
    $ export SCR_CACHE_BASE=/tmp
    $ export SCR_PREFIX=/home/DIR
    $ export SCR_JOB_ID=1
    $ export FLUSH_to_SCR=10

Known issues and restrictions
=============================

Currently we have the following restrictions.

* Currently `GDS_THREAD_SINGLE` is the only supported thread execution model.
* Due to a potential bug, GVR may not work correctly on MVAPICH2 + Infiniband environment.
* `GDS_get_attr` does not support `GDS_GLOBAL_LAYOUT` attribute yet.
