******************
Getting Started
******************

Initialization
==============

The simplest GVR program looks like this::

    #include <gds.h>

    int main(int argc, char *argv[])
    {
        GDS_thread_support_t provd_support;
        GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support);
        ...
        GDS_finalize();

        return 0;
    }

Initialization with MPI library
===============================

If your program already uses MPI, you should call ``GDS_init`` **after**
``MPI_Init_thread`` and ``GDS_finalize`` **before** ``MPI_Finalize``. MPI library must be initialized with ``MPI_Init_thread`` and ``MPI_THREAD_MULTIPLE`` for the thread support level.

::

    #include <mpi.h>
    #include <gds.h>

    int main(int argc, char *argv[])
    {
        int mpi_prov;
        GDS_thread_support_t provd_support;
        MPI_Init_thread(MPI_THREAD_MULTIPLE, &mpi_prov);
        GDS_init(&argc, &argv, GDS_THREAD_SINGLE, &provd_support);
        ...
        GDS_finalize();
        MPI_Finalize();

        return 0;
    }

How to build your program
=========================

You need to link your GVR-applied program with ``libgds`` by specifying proper linking options, such as ``-L`` and ``-lgds`` options.

::

    $ mpicc ... -I$GVR_PATH/include -L$GVR_PATH/lib <gvr-program>.c -lgds

For Fortran programs, you need to link with both ``libgdsf90`` and ``libgds``.
Make sure to specify -L, -lgdsf90, and -lgds options.

::

    $ mpif90 ... -I$GVR_PATH/include -L$GVR_PATH/lib <gvr-program>.f90 -lgdsf90 -lgds

Note for static linking
-----------------------

If you want to link ``libgds.a`` statically to your program, you also need to link ``lrds_dummy.a``. Note that ``lrds.a`` requires a special kernel feature and is unlikely to work on most of the systems.

::

    $ mpicc ... -I$GVR_PATH/include -L$GVR_PATH/lib -static <gvr-program>.c -lgds -llrds_dummy

How to run your program
=======================

Current GVR implementation is built on top of MPI library, so you can run your
GVR-enabled program just like an MPI application. You may have to add the
library path to ``LD_LIBRARY_PATH`` where ``libgds.so`` is located.

::

    $ LD_LIBRARY_PATH=$GVR_LIB_PATH mpiexec -n 4 <gvr-program>

Runtime Options
---------------
GVR library provides following environment variables to control the runtime behavior:

For debugging/problem reporting

  * ``GDS_DEBUG_LEVEL``: An integer variable to set verbosity of the debug output from the GVR library. Default is zero (only critical errors are reported) and the current maximum is 2 (all the debug messages will be shown). Debug output is written to the standard error.
  * ``GDS_SHOW_INFO``: If set to non-zero value, the GVR library will dump a banner at the beginning of the execution, including version information and all the compile time/runtime configurations. This will be useful for error reporting. Default is zero (disabled).
  * ``GDS_SANITY_CHECK``: If set to non-zero value, the GVR library will perform more careful check on input parameters. Default is zero (disabled).

For performance tuning

  * ``GDS_WAIT_SCHEME``: The GVR library uses a server thread to handle some of the internal service requests. This option controls how the server thread waits for incoming requests.

      * `0`: Use ``MPI_Waitany``. This option is known to have a performance issue in many platforms.
      * `1`: Use a loop of ``MPI_Testany`` + short ``usleep``.
      * `2`: Use a loop of ``MPI_Testany`` + "CPU spinning" (empty loop).
      * `3`: Use the "doorbell" method. When there is some message for server thread, client writes a value in a small remote memory region that is shared between clients and servers. Server thread usually spins on this region to see if there is any updates on value. Since MPI call is issued only when absolutely necessary on target side, this will avoid lock contention between client and server threads. Currently only x86 platforms are supported (may not work in a platform with weak memory consistency between CPU and devices).
  * ``GDS_WAIT_SLEEP_DURATION``: Set the sleep duration in microseconds if ``GDS_WAIT_SCHEME`` is set to 1.
  * ``GDS_WAIT_SPIN_COUNT``: Set the number of spin loop count if ``GDS_WAIT_SCHEME`` is set to 2.
  * ``GDS_OUTSTANDING_RMA_OPS_MAX``: In some platforms it is known that the large number of outstanding RMA operation is harmful for performance. This variable sets the maximum number of outstanding RMA operation per RMA window (=GVR array).


Code example
============

The ``examples`` directory contains two examples (in C).

* ``dgemm.c``: A simple DGEMM program with GVR array creation and data access.
* ``resilient_dgemm.c``: In addition to the above program, this also covers version creation, version navigation, error signaling and handling.

Writing Fortran programs requires users having basic knowledge of
interoperability with C, especially dealing with C pointer and function
pointer. For more details, please refer to `GCC Interoperability with C
<http://gcc.gnu.org/onlinedocs/gfortran/Interoperability-with-C.html>`_.
Note that the internal memory ordering of array is set to `column-major` by
default for GVR Fortran library, which is different from `row-major` in GVR C library.

Following Fortran90 code shows an example of using GVR Fortran APIs. For more
examples, please refer to ``tests/tests_f90.f90`` in GVR source directory.

::

    PROGRAM FORTRAN_EXAMPLE()
      USE, INTRINSIC :: ISO_C_BINDING
      USE GDS
      IMPLICIT NONE
      
      TYPE(C_PTR) :: g ! GDS handler with C_PTR type
      INTEGER(8) :: ndim, cts(1), min_chunk(1), lo_idx(1), hi_idx(1), ld(1)
      INTEGER, TARGET :: put_buf, acc_buf, get_buf ! TARGET attribute is required for C_LOC
      INTEGER :: stat

      g = C_NULL_PTR
      ndim = 1
      cts(1) = 1
      min_chunk(1) = 0
      lo_idx(1) = 0
      hi_idx(1) = 0
      ld(1) = 0
      put_buf = 1
      acc_buf = 1
      get_buf = 0

      call GDS_ALLOC(ndim, cts, min_chunk, GDS_DATA_INT, GDS_PRIORITY_HIGH, &
	    GDS_COMM_WORLD, MPI_INFO_NULL, g, stat)
	  
      ! Use C_LOC to get buffer address
      call GDS_PUT(C_LOC(put_buf), ld, lo_idx, hi_idx, g, stat) 
      call GDS_FENCE(g, stat)

      call GDS_ACC(C_LOC(acc_buf), ld, lo_idx, hi_idx, MPI_SUM, g, stat)
      call GDS_FENCE(g, stat)
      
      call GDS_GET(C_LOC(get_buf), ld, lo_idx, hi_idx, g, stat)
      call GDS_FENCE(g, stat)
     
      acc_buf = 2
      call GDS_ACC(C_LOC(acc_buf), ld, lo_idx, hi_idx, MPI_PROD, g, stat)
      call GDS_FENCE(g, stat)
      
      call GDS_GET(C_LOC(get_buf), ld, lo_idx, hi_idx, g, stat)
      call GDS_FENCE(g, stat)

      call GDS_FREE(g, stat)
    END PROGRAM
